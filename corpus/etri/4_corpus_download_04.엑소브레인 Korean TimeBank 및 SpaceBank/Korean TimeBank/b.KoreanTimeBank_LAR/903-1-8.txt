{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "이 학교는 1907년 12월 평북 정주에 민족정신의 고취와 인재양성에 뜻을 두고 설립한 학교이다.  ",
	"morp" : [
		{"id" : 0, "lemma" : "이", "type" : "MM", "position" : 0, "weight" : 0.0020222 },
		{"id" : 1, "lemma" : "학교", "type" : "NNG", "position" : 4, "weight" : 0.73717 },
		{"id" : 2, "lemma" : "는", "type" : "JX", "position" : 10, "weight" : 0.0287565 },
		{"id" : 3, "lemma" : "1907", "type" : "SN", "position" : 14, "weight" : 1 },
		{"id" : 4, "lemma" : "년", "type" : "NNB", "position" : 18, "weight" : 0.414343 },
		{"id" : 5, "lemma" : "12", "type" : "SN", "position" : 22, "weight" : 1 },
		{"id" : 6, "lemma" : "월", "type" : "NNB", "position" : 24, "weight" : 0.408539 },
		{"id" : 7, "lemma" : "평북", "type" : "NNP", "position" : 28, "weight" : 0.9 },
		{"id" : 8, "lemma" : "정주", "type" : "NNP", "position" : 35, "weight" : 0.0571978 },
		{"id" : 9, "lemma" : "에", "type" : "JKB", "position" : 41, "weight" : 0.0823628 },
		{"id" : 10, "lemma" : "민족", "type" : "NNG", "position" : 45, "weight" : 0.9 },
		{"id" : 11, "lemma" : "정신", "type" : "NNG", "position" : 51, "weight" : 0.184812 },
		{"id" : 12, "lemma" : "의", "type" : "JKG", "position" : 57, "weight" : 0.0694213 },
		{"id" : 13, "lemma" : "고취", "type" : "NNG", "position" : 61, "weight" : 0.9 },
		{"id" : 14, "lemma" : "와", "type" : "JC", "position" : 67, "weight" : 0.0169714 },
		{"id" : 15, "lemma" : "인재", "type" : "NNG", "position" : 71, "weight" : 0.754407 },
		{"id" : 16, "lemma" : "양성", "type" : "NNG", "position" : 77, "weight" : 0.183692 },
		{"id" : 17, "lemma" : "에", "type" : "JKB", "position" : 83, "weight" : 0.153364 },
		{"id" : 18, "lemma" : "뜻", "type" : "NNG", "position" : 87, "weight" : 0.9 },
		{"id" : 19, "lemma" : "을", "type" : "JKO", "position" : 90, "weight" : 0.129611 },
		{"id" : 20, "lemma" : "두", "type" : "VV", "position" : 94, "weight" : 0.192556 },
		{"id" : 21, "lemma" : "고", "type" : "EC", "position" : 97, "weight" : 0.416679 },
		{"id" : 22, "lemma" : "설립", "type" : "NNG", "position" : 101, "weight" : 0.9 },
		{"id" : 23, "lemma" : "하", "type" : "XSV", "position" : 107, "weight" : 0.0001 },
		{"id" : 24, "lemma" : "ㄴ", "type" : "ETM", "position" : 107, "weight" : 0.392321 },
		{"id" : 25, "lemma" : "학교", "type" : "NNG", "position" : 111, "weight" : 0.658588 },
		{"id" : 26, "lemma" : "이", "type" : "VCP", "position" : 117, "weight" : 0.0177525 },
		{"id" : 27, "lemma" : "다", "type" : "EF", "position" : 120, "weight" : 0.353579 },
		{"id" : 28, "lemma" : ".", "type" : "SF", "position" : 123, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이/MM", "target" : "이", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "학교/NNG+는/JX", "target" : "학교는", "word_id" : 1, "m_begin" : 1, "m_end" : 2},
		{"id" : 2, "result" : "1907/SN+년/NNB", "target" : "1907년", "word_id" : 2, "m_begin" : 3, "m_end" : 4},
		{"id" : 3, "result" : "12/SN+월/NNB", "target" : "12월", "word_id" : 3, "m_begin" : 5, "m_end" : 6},
		{"id" : 4, "result" : "평북/NNG", "target" : "평북", "word_id" : 4, "m_begin" : 7, "m_end" : 7},
		{"id" : 5, "result" : "정주/NNG+에/JKB", "target" : "정주에", "word_id" : 5, "m_begin" : 8, "m_end" : 9},
		{"id" : 6, "result" : "민족정신/NNG+의/JKG", "target" : "민족정신의", "word_id" : 6, "m_begin" : 10, "m_end" : 12},
		{"id" : 7, "result" : "고취/NNG+와/JC", "target" : "고취와", "word_id" : 7, "m_begin" : 13, "m_end" : 14},
		{"id" : 8, "result" : "인재양성/NNG+에/JKB", "target" : "인재양성에", "word_id" : 8, "m_begin" : 15, "m_end" : 17},
		{"id" : 9, "result" : "뜻/NNG+을/JKO", "target" : "뜻을", "word_id" : 9, "m_begin" : 18, "m_end" : 19},
		{"id" : 10, "result" : "두/VV+고/EC", "target" : "두고", "word_id" : 10, "m_begin" : 20, "m_end" : 21},
		{"id" : 11, "result" : "설립하/VV+ㄴ/ETM", "target" : "설립한", "word_id" : 11, "m_begin" : 22, "m_end" : 24},
		{"id" : 12, "result" : "학교/NNG+이/VCP+다/EF+./SF", "target" : "학교이다.", "word_id" : 12, "m_begin" : 25, "m_end" : 28}
	],
	"WSD" : [
		{"id" : 0, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "학교", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 4, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "1907", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 14, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 18, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "12", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 22, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "월", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 24, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "평북", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 28, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "정주", "type" : "NNP", "scode" : "01", "weight" : 1, "position" : 35, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 41, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "민족정신", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 45, "begin" : 10, "end" : 11},
		{"id" : 11, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 57, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "고취", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 61, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "와", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 67, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "인재", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 71, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "양성", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 77, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 83, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "뜻", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 87, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 90, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "두", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 94, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 97, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : "설립하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 101, "begin" : 22, "end" : 23},
		{"id" : 22, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 107, "begin" : 24, "end" : 24},
		{"id" : 23, "text" : "학교", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 111, "begin" : 25, "end" : 25},
		{"id" : 24, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 117, "begin" : 26, "end" : 26},
		{"id" : 25, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 120, "begin" : 27, "end" : 27},
		{"id" : 26, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 123, "begin" : 28, "end" : 28}
	],
	"word" : [
		{"id" : 0, "text" : "이", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "학교는", "type" : "", "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "1907년", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 3, "text" : "12월", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 4, "text" : "평북", "type" : "", "begin" : 7, "end" : 7},
		{"id" : 5, "text" : "정주에", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 6, "text" : "민족정신의", "type" : "", "begin" : 10, "end" : 12},
		{"id" : 7, "text" : "고취와", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 8, "text" : "인재양성에", "type" : "", "begin" : 15, "end" : 17},
		{"id" : 9, "text" : "뜻을", "type" : "", "begin" : 18, "end" : 19},
		{"id" : 10, "text" : "두고", "type" : "", "begin" : 20, "end" : 21},
		{"id" : 11, "text" : "설립한", "type" : "", "begin" : 22, "end" : 24},
		{"id" : 12, "text" : "학교이다.", "type" : "", "begin" : 25, "end" : 28}
	],
	"NE" : [
		{"id" : 0, "text" : "1907년 12월", "type" : "DT_OTHERS", "begin" : 3, "end" : 6, "weight" : 0.608927, "common_noun" : 0},
		{"id" : 1, "text" : "평북", "type" : "LCP_PROVINCE", "begin" : 7, "end" : 7, "weight" : 0.445567, "common_noun" : 0},
		{"id" : 2, "text" : "정주", "type" : "LCP_COUNTY", "begin" : 8, "end" : 8, "weight" : 0.204181, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이", "head" : 1, "label" : "DP", "mod" : [], "weight" : 0.894951 },
		{"id" : 1, "text" : "학교는", "head" : 10, "label" : "NP_SBJ", "mod" : [0], "weight" : 0.542809 },
		{"id" : 2, "text" : "1907년", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.992668 },
		{"id" : 3, "text" : "12월", "head" : 10, "label" : "NP_AJT", "mod" : [2], "weight" : 0.675988 },
		{"id" : 4, "text" : "평북", "head" : 5, "label" : "NP", "mod" : [], "weight" : 0.986333 },
		{"id" : 5, "text" : "정주에", "head" : 10, "label" : "NP_AJT", "mod" : [4], "weight" : 0.821654 },
		{"id" : 6, "text" : "민족정신의", "head" : 8, "label" : "NP_MOD", "mod" : [], "weight" : 0.84077 },
		{"id" : 7, "text" : "고취와", "head" : 8, "label" : "NP_CNJ", "mod" : [], "weight" : 0.698797 },
		{"id" : 8, "text" : "인재양성에", "head" : 10, "label" : "NP_AJT", "mod" : [6, 7], "weight" : 0.876696 },
		{"id" : 9, "text" : "뜻을", "head" : 10, "label" : "NP_OBJ", "mod" : [], "weight" : 0.952379 },
		{"id" : 10, "text" : "두고", "head" : 11, "label" : "VP", "mod" : [1, 3, 5, 8, 9], "weight" : 0.737574 },
		{"id" : 11, "text" : "설립한", "head" : 12, "label" : "VP_MOD", "mod" : [10], "weight" : 0.541703 },
		{"id" : 12, "text" : "학교이다.", "head" : -1, "label" : "VNP", "mod" : [11], "weight" : 0.0312822 }
	],
	"SRL" : [
		{"verb" : "두", "sense" : 1, "word_id" : 10, "weight" : 0.248209,
			"argument" : [
				{"type" : "ARG2", "word_id" : 7, "text" : "고취와", "weight" : 0.309517 },
				{"type" : "ARG2", "word_id" : 8, "text" : "인재양성에", "weight" : 0.178745 },
				{"type" : "ARG1", "word_id" : 9, "text" : "뜻을", "weight" : 0.256365 }
			] },
		{"verb" : "설립", "sense" : 1, "word_id" : 11, "weight" : 0.203116,
			"argument" : [
				{"type" : "ARG1", "word_id" : 1, "text" : "학교는", "weight" : 0.19372 },
				{"type" : "ARGM-TMP", "word_id" : 3, "text" : "12월", "weight" : 0.324224 },
				{"type" : "ARGM-LOC", "word_id" : 5, "text" : "정주에", "weight" : 0.162208 },
				{"type" : "ARGM-CAU", "word_id" : 10, "text" : "두고", "weight" : 0.132313 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 12, "ant_sid" : 0, "ant_wid" : 1, "type" : "s", "istitle" : 0, "weight" : 0.6064 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "도산 안창호의 강연에 감명을 받은 남강 이승훈이 세운것으로 알려진 민족 사립학교는 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "도산", "type" : "NNG", "position" : 126, "weight" : 0.356847 },
		{"id" : 1, "lemma" : "안창호", "type" : "NNP", "position" : 133, "weight" : 0.9 },
		{"id" : 2, "lemma" : "의", "type" : "JKG", "position" : 142, "weight" : 0.0987295 },
		{"id" : 3, "lemma" : "강연", "type" : "NNG", "position" : 146, "weight" : 0.9 },
		{"id" : 4, "lemma" : "에", "type" : "JKB", "position" : 152, "weight" : 0.153364 },
		{"id" : 5, "lemma" : "감명", "type" : "NNG", "position" : 156, "weight" : 0.9 },
		{"id" : 6, "lemma" : "을", "type" : "JKO", "position" : 162, "weight" : 0.129611 },
		{"id" : 7, "lemma" : "받", "type" : "VV", "position" : 166, "weight" : 0.765648 },
		{"id" : 8, "lemma" : "은", "type" : "ETM", "position" : 169, "weight" : 0.109632 },
		{"id" : 9, "lemma" : "남강", "type" : "NNP", "position" : 173, "weight" : 0.0272582 },
		{"id" : 10, "lemma" : "이승훈", "type" : "NNP", "position" : 180, "weight" : 0.9 },
		{"id" : 11, "lemma" : "이", "type" : "JKS", "position" : 189, "weight" : 0.0234517 },
		{"id" : 12, "lemma" : "세운것", "type" : "NNP", "position" : 193, "weight" : 0.6 },
		{"id" : 13, "lemma" : "으로", "type" : "JKB", "position" : 202, "weight" : 0.0823853 },
		{"id" : 14, "lemma" : "알려지", "type" : "VV", "position" : 209, "weight" : 0.9 },
		{"id" : 15, "lemma" : "ㄴ", "type" : "ETM", "position" : 215, "weight" : 0.304215 },
		{"id" : 16, "lemma" : "민족", "type" : "NNG", "position" : 219, "weight" : 0.9 },
		{"id" : 17, "lemma" : "사립", "type" : "NNG", "position" : 226, "weight" : 0.184325 },
		{"id" : 18, "lemma" : "학교", "type" : "NNG", "position" : 232, "weight" : 0.184769 },
		{"id" : 19, "lemma" : "는", "type" : "JX", "position" : 238, "weight" : 0.0287565 },
		{"id" : 20, "lemma" : "무엇", "type" : "NP", "position" : 242, "weight" : 0.9 },
		{"id" : 21, "lemma" : "이", "type" : "VCP", "position" : 248, "weight" : 0.0175768 },
		{"id" : 22, "lemma" : "ㄹ까", "type" : "EF", "position" : 248, "weight" : 0.258243 },
		{"id" : 23, "lemma" : "?", "type" : "SF", "position" : 254, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "도산/NNG", "target" : "도산", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "안창호/NNG+의/JKG", "target" : "안창호의", "word_id" : 1, "m_begin" : 1, "m_end" : 2},
		{"id" : 2, "result" : "강연/NNG+에/JKB", "target" : "강연에", "word_id" : 2, "m_begin" : 3, "m_end" : 4},
		{"id" : 3, "result" : "감명/NNG+을/JKO", "target" : "감명을", "word_id" : 3, "m_begin" : 5, "m_end" : 6},
		{"id" : 4, "result" : "받/VV+은/ETM", "target" : "받은", "word_id" : 4, "m_begin" : 7, "m_end" : 8},
		{"id" : 5, "result" : "남강/NNG", "target" : "남강", "word_id" : 5, "m_begin" : 9, "m_end" : 9},
		{"id" : 6, "result" : "이승훈/NNG+이/JKS", "target" : "이승훈이", "word_id" : 6, "m_begin" : 10, "m_end" : 11},
		{"id" : 7, "result" : "세운것/NNG+으로/JKB", "target" : "세운것으로", "word_id" : 7, "m_begin" : 12, "m_end" : 13},
		{"id" : 8, "result" : "알려지/VV+ㄴ/ETM", "target" : "알려진", "word_id" : 8, "m_begin" : 14, "m_end" : 15},
		{"id" : 9, "result" : "민족/NNG", "target" : "민족", "word_id" : 9, "m_begin" : 16, "m_end" : 16},
		{"id" : 10, "result" : "사립학교/NNG+는/JX", "target" : "사립학교는", "word_id" : 10, "m_begin" : 17, "m_end" : 19},
		{"id" : 11, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 11, "m_begin" : 20, "m_end" : 23}
	],
	"WSD" : [
		{"id" : 0, "text" : "도산", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 126, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "안창호", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 133, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 142, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "강연", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 146, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 152, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "감명", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 156, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 162, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "받", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 166, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "은", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 169, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "남강", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 173, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "이승훈", "type" : "NNP", "scode" : "99", "weight" : 1, "position" : 180, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 189, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "세운것", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 193, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 202, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "알려지", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 209, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 215, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "민족", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 219, "begin" : 16, "end" : 16},
		{"id" : 17, "text" : "사립학교", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 226, "begin" : 17, "end" : 18},
		{"id" : 18, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 238, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 242, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 248, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 248, "begin" : 22, "end" : 22},
		{"id" : 22, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 254, "begin" : 23, "end" : 23}
	],
	"word" : [
		{"id" : 0, "text" : "도산", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "안창호의", "type" : "", "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "강연에", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 3, "text" : "감명을", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 4, "text" : "받은", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 5, "text" : "남강", "type" : "", "begin" : 9, "end" : 9},
		{"id" : 6, "text" : "이승훈이", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 7, "text" : "세운것으로", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 8, "text" : "알려진", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 9, "text" : "민족", "type" : "", "begin" : 16, "end" : 16},
		{"id" : 10, "text" : "사립학교는", "type" : "", "begin" : 17, "end" : 19},
		{"id" : 11, "text" : "무엇일까?", "type" : "", "begin" : 20, "end" : 23}
	],
	"NE" : [
		{"id" : 0, "text" : "안창호", "type" : "PS_NAME", "begin" : 1, "end" : 1, "weight" : 0.280936, "common_noun" : 0},
		{"id" : 1, "text" : "남강", "type" : "LCG_RIVER", "begin" : 9, "end" : 9, "weight" : 0.166882, "common_noun" : 0},
		{"id" : 2, "text" : "이승훈", "type" : "PS_NAME", "begin" : 10, "end" : 10, "weight" : 0.709176, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "도산", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.673457 },
		{"id" : 1, "text" : "안창호의", "head" : 2, "label" : "NP_MOD", "mod" : [0], "weight" : 0.92361 },
		{"id" : 2, "text" : "강연에", "head" : 4, "label" : "NP_AJT", "mod" : [1], "weight" : 0.702849 },
		{"id" : 3, "text" : "감명을", "head" : 4, "label" : "NP_OBJ", "mod" : [], "weight" : 0.922525 },
		{"id" : 4, "text" : "받은", "head" : 6, "label" : "VP_MOD", "mod" : [2, 3], "weight" : 0.665556 },
		{"id" : 5, "text" : "남강", "head" : 6, "label" : "NP", "mod" : [], "weight" : 0.869608 },
		{"id" : 6, "text" : "이승훈이", "head" : 8, "label" : "NP_SBJ", "mod" : [4, 5], "weight" : 0.975173 },
		{"id" : 7, "text" : "세운것으로", "head" : 8, "label" : "NP_AJT", "mod" : [], "weight" : 0.853161 },
		{"id" : 8, "text" : "알려진", "head" : 10, "label" : "VP_MOD", "mod" : [6, 7], "weight" : 0.988339 },
		{"id" : 9, "text" : "민족", "head" : 10, "label" : "NP", "mod" : [], "weight" : 0.8553 },
		{"id" : 10, "text" : "사립학교는", "head" : 11, "label" : "NP_SBJ", "mod" : [8, 9], "weight" : 0.72568 },
		{"id" : 11, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [10], "weight" : 0.0769253 }
	],
	"SRL" : [
		{"verb" : "받", "sense" : 1, "word_id" : 4, "weight" : 0.266901,
			"argument" : [
				{"type" : "ARG2", "word_id" : 2, "text" : "강연에", "weight" : 0.149776 },
				{"type" : "ARG1", "word_id" : 3, "text" : "감명을", "weight" : 0.25055 },
				{"type" : "ARG0", "word_id" : 6, "text" : "이승훈이", "weight" : 0.400379 }
			] },
		{"verb" : "알려지", "sense" : 1, "word_id" : 8, "weight" : 0.164339,
			"argument" : [
				{"type" : "ARG1", "word_id" : 6, "text" : "이승훈이", "weight" : 0.16085 },
				{"type" : "ARGM-ADV", "word_id" : 7, "text" : "세운것으로", "weight" : 0.0628332 },
				{"type" : "ARG1", "word_id" : 10, "text" : "사립학교는", "weight" : 0.269334 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "PS_NAME", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 10, "sent_id" : 1, "start_eid" : 0, "end_eid" : 10, "ne_id" : -1, "text" : "도산 안창호의 강연에 감명을 받은 남강 이승훈이 세운것으로 알려진 민족 사립학교", "start_eid_short" : 9, "end_eid_short" : 10, "text_short" : "민족 사립학교", "weight" : 0.002 },
		{"id" : 11, "sent_id" : 1, "start_eid" : 0, "end_eid" : 6, "ne_id" : 2, "text" : "도산 안창호의 강연에 감명을 받은 남강 이승훈", "start_eid_short" : 5, "end_eid_short" : 6, "text_short" : "남강 이승훈", "weight" : 0.008 },
		{"id" : 19, "sent_id" : 1, "start_eid" : 11, "end_eid" : 11, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 11, "end_eid_short" : 11, "text_short" : "무엇이ㄹ까?", "weight" : 0.012 }
	] }
 ]
}

