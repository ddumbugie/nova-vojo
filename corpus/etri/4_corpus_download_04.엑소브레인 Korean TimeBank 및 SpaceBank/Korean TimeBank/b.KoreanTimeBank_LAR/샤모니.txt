{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿샤모니몽블랑, 혹은 간단히 샤모니는 몽블랑 산 기슭에 자리한 프랑스 오트사부아 주의 코뮌으로 인구는 9,830 명이다.",
	"morp" : [
		{"id" : 0, "lemma" : "﻿샤모니몽", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "블랑", "type" : "NNG", "position" : 15, "weight" : 0.187994 },
		{"id" : 2, "lemma" : ",", "type" : "SP", "position" : 21, "weight" : 1 },
		{"id" : 3, "lemma" : "혹은", "type" : "MAJ", "position" : 23, "weight" : 0.00755874 },
		{"id" : 4, "lemma" : "간단히", "type" : "MAG", "position" : 30, "weight" : 0.9 },
		{"id" : 5, "lemma" : "샤모니", "type" : "NNP", "position" : 40, "weight" : 0.2 },
		{"id" : 6, "lemma" : "는", "type" : "JX", "position" : 49, "weight" : 0.0332677 },
		{"id" : 7, "lemma" : "몽블랑", "type" : "NNP", "position" : 53, "weight" : 0.35 },
		{"id" : 8, "lemma" : "산", "type" : "NNG", "position" : 63, "weight" : 0.264279 },
		{"id" : 9, "lemma" : "기슭", "type" : "NNG", "position" : 67, "weight" : 0.9 },
		{"id" : 10, "lemma" : "에", "type" : "JKB", "position" : 73, "weight" : 0.153364 },
		{"id" : 11, "lemma" : "자리", "type" : "NNG", "position" : 77, "weight" : 0.207703 },
		{"id" : 12, "lemma" : "하", "type" : "XSV", "position" : 83, "weight" : 0.0001 },
		{"id" : 13, "lemma" : "ㄴ", "type" : "ETM", "position" : 83, "weight" : 0.392321 },
		{"id" : 14, "lemma" : "프랑스", "type" : "NNP", "position" : 87, "weight" : 0.0280546 },
		{"id" : 15, "lemma" : "오트사부아", "type" : "NNP", "position" : 97, "weight" : 0.6 },
		{"id" : 16, "lemma" : "주의", "type" : "NNG", "position" : 113, "weight" : 0.9 },
		{"id" : 17, "lemma" : "코뮌", "type" : "NNG", "position" : 120, "weight" : 0.166356 },
		{"id" : 18, "lemma" : "으로", "type" : "JKB", "position" : 126, "weight" : 0.153406 },
		{"id" : 19, "lemma" : "인구", "type" : "NNG", "position" : 133, "weight" : 0.207185 },
		{"id" : 20, "lemma" : "는", "type" : "JX", "position" : 139, "weight" : 0.0287565 },
		{"id" : 21, "lemma" : "9,830", "type" : "SN", "position" : 143, "weight" : 1 },
		{"id" : 22, "lemma" : "명", "type" : "NNB", "position" : 149, "weight" : 0.399746 },
		{"id" : 23, "lemma" : "이", "type" : "VCP", "position" : 152, "weight" : 0.0607843 },
		{"id" : 24, "lemma" : "다", "type" : "EF", "position" : 155, "weight" : 0.353579 },
		{"id" : 25, "lemma" : ".", "type" : "SF", "position" : 158, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿샤모니몽블랑/NNG+,/SP", "target" : "﻿샤모니몽블랑,", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "혹은/MAJ", "target" : "혹은", "word_id" : 1, "m_begin" : 3, "m_end" : 3},
		{"id" : 2, "result" : "간단히/MAG", "target" : "간단히", "word_id" : 2, "m_begin" : 4, "m_end" : 4},
		{"id" : 3, "result" : "샤모니/NNG+는/JX", "target" : "샤모니는", "word_id" : 3, "m_begin" : 5, "m_end" : 6},
		{"id" : 4, "result" : "몽블랑/NNG", "target" : "몽블랑", "word_id" : 4, "m_begin" : 7, "m_end" : 7},
		{"id" : 5, "result" : "산/NNG", "target" : "산", "word_id" : 5, "m_begin" : 8, "m_end" : 8},
		{"id" : 6, "result" : "기슭/NNG+에/JKB", "target" : "기슭에", "word_id" : 6, "m_begin" : 9, "m_end" : 10},
		{"id" : 7, "result" : "자리하/VV+ㄴ/ETM", "target" : "자리한", "word_id" : 7, "m_begin" : 11, "m_end" : 13},
		{"id" : 8, "result" : "프랑스/NNG", "target" : "프랑스", "word_id" : 8, "m_begin" : 14, "m_end" : 14},
		{"id" : 9, "result" : "오트사부아/NNG", "target" : "오트사부아", "word_id" : 9, "m_begin" : 15, "m_end" : 15},
		{"id" : 10, "result" : "주의/NNG", "target" : "주의", "word_id" : 10, "m_begin" : 16, "m_end" : 16},
		{"id" : 11, "result" : "코뮌/NNG+으로/JKB", "target" : "코뮌으로", "word_id" : 11, "m_begin" : 17, "m_end" : 18},
		{"id" : 12, "result" : "인구/NNG+는/JX", "target" : "인구는", "word_id" : 12, "m_begin" : 19, "m_end" : 20},
		{"id" : 13, "result" : "9,830/SN", "target" : "9,830", "word_id" : 13, "m_begin" : 21, "m_end" : 21},
		{"id" : 14, "result" : "명/NNB+이/VCP+다/EF+./SF", "target" : "명이다.", "word_id" : 14, "m_begin" : 22, "m_end" : 25}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿샤모니몽", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "블랑", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 15, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 21, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "혹은", "type" : "MAJ", "scode" : "00", "weight" : 1, "position" : 23, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "간단히", "type" : "MAG", "scode" : "00", "weight" : 1, "position" : 30, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "샤모니", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 49, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "몽블랑", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 53, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "산", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 63, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "기슭", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 67, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 73, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "자리하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 77, "begin" : 11, "end" : 12},
		{"id" : 12, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 83, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "프랑스", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 87, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "오트사부아", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 97, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "주의", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 113, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "코뮌", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 120, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 126, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "인구", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 133, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 139, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "9,830", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 143, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : "명", "type" : "NNB", "scode" : "03", "weight" : 1, "position" : 149, "begin" : 22, "end" : 22},
		{"id" : 22, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 152, "begin" : 23, "end" : 23},
		{"id" : 23, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 155, "begin" : 24, "end" : 24},
		{"id" : 24, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 158, "begin" : 25, "end" : 25}
	],
	"word" : [
		{"id" : 0, "text" : "﻿샤모니몽블랑,", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "혹은", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 2, "text" : "간단히", "type" : "", "begin" : 4, "end" : 4},
		{"id" : 3, "text" : "샤모니는", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 4, "text" : "몽블랑", "type" : "", "begin" : 7, "end" : 7},
		{"id" : 5, "text" : "산", "type" : "", "begin" : 8, "end" : 8},
		{"id" : 6, "text" : "기슭에", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 7, "text" : "자리한", "type" : "", "begin" : 11, "end" : 13},
		{"id" : 8, "text" : "프랑스", "type" : "", "begin" : 14, "end" : 14},
		{"id" : 9, "text" : "오트사부아", "type" : "", "begin" : 15, "end" : 15},
		{"id" : 10, "text" : "주의", "type" : "", "begin" : 16, "end" : 16},
		{"id" : 11, "text" : "코뮌으로", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 12, "text" : "인구는", "type" : "", "begin" : 19, "end" : 20},
		{"id" : 13, "text" : "9,830", "type" : "", "begin" : 21, "end" : 21},
		{"id" : 14, "text" : "명이다.", "type" : "", "begin" : 22, "end" : 25}
	],
	"NE" : [
		{"id" : 0, "text" : "﻿샤모니몽블랑", "type" : "LCG_MOUNTAIN", "begin" : 0, "end" : 1, "weight" : 0.0656901, "common_noun" : 0},
		{"id" : 1, "text" : "샤모니", "type" : "LCG_MOUNTAIN", "begin" : 5, "end" : 5, "weight" : 0.135547, "common_noun" : 0},
		{"id" : 2, "text" : "몽블랑 산", "type" : "LCG_MOUNTAIN", "begin" : 7, "end" : 8, "weight" : 0.617701, "common_noun" : 0},
		{"id" : 3, "text" : "프랑스", "type" : "LCP_COUNTRY", "begin" : 14, "end" : 14, "weight" : 0.514735, "common_noun" : 0},
		{"id" : 4, "text" : "오트사부아 주의", "type" : "LCP_PROVINCE", "begin" : 15, "end" : 16, "weight" : 0.138203, "common_noun" : 0},
		{"id" : 5, "text" : "코뮌", "type" : "CV_POLICY", "begin" : 17, "end" : 17, "weight" : 0.157398, "common_noun" : 0},
		{"id" : 6, "text" : "9,830 명", "type" : "QT_MAN_COUNT", "begin" : 21, "end" : 22, "weight" : 0.422974, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿샤모니몽블랑,", "head" : 11, "label" : "NP_CNJ", "mod" : [], "weight" : 0.78845 },
		{"id" : 1, "text" : "혹은", "head" : 2, "label" : "AP", "mod" : [], "weight" : 0.501199 },
		{"id" : 2, "text" : "간단히", "head" : 11, "label" : "AP", "mod" : [1], "weight" : 0.672311 },
		{"id" : 3, "text" : "샤모니는", "head" : 11, "label" : "NP_SBJ", "mod" : [], "weight" : 0.626503 },
		{"id" : 4, "text" : "몽블랑", "head" : 5, "label" : "NP", "mod" : [], "weight" : 0.668905 },
		{"id" : 5, "text" : "산", "head" : 6, "label" : "NP", "mod" : [4], "weight" : 0.897957 },
		{"id" : 6, "text" : "기슭에", "head" : 7, "label" : "NP_AJT", "mod" : [5], "weight" : 0.956156 },
		{"id" : 7, "text" : "자리한", "head" : 11, "label" : "VP_MOD", "mod" : [6], "weight" : 0.99549 },
		{"id" : 8, "text" : "프랑스", "head" : 9, "label" : "NP", "mod" : [], "weight" : 0.512518 },
		{"id" : 9, "text" : "오트사부아", "head" : 10, "label" : "NP", "mod" : [8], "weight" : 0.714535 },
		{"id" : 10, "text" : "주의", "head" : 11, "label" : "NP", "mod" : [9], "weight" : 0.676454 },
		{"id" : 11, "text" : "코뮌으로", "head" : 14, "label" : "NP_AJT", "mod" : [0, 2, 3, 7, 10], "weight" : 0.680412 },
		{"id" : 12, "text" : "인구는", "head" : 14, "label" : "NP_SBJ", "mod" : [], "weight" : 0.50418 },
		{"id" : 13, "text" : "9,830", "head" : 14, "label" : "NP", "mod" : [], "weight" : 0.397906 },
		{"id" : 14, "text" : "명이다.", "head" : -1, "label" : "VNP", "mod" : [11, 12, 13], "weight" : 0.00188377 }
	],
	"SRL" : [
		{"verb" : "자리", "sense" : 1, "word_id" : 7, "weight" : 0.1349,
			"argument" : [
				{"type" : "ARG2", "word_id" : 6, "text" : "기슭에", "weight" : 0.0877568 },
				{"type" : "ARG0", "word_id" : 11, "text" : "코뮌으로", "weight" : 0.182044 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "프랑스의 겨울 스포츠 리조트로 알려졌으며, 특히 스키장이 유명하다.",
	"morp" : [
		{"id" : 0, "lemma" : "프랑스", "type" : "NNP", "position" : 159, "weight" : 0.0892825 },
		{"id" : 1, "lemma" : "의", "type" : "JKG", "position" : 168, "weight" : 0.0987295 },
		{"id" : 2, "lemma" : "겨울", "type" : "NNG", "position" : 172, "weight" : 0.9 },
		{"id" : 3, "lemma" : "스포츠", "type" : "NNG", "position" : 179, "weight" : 0.9 },
		{"id" : 4, "lemma" : "리조트", "type" : "NNG", "position" : 189, "weight" : 0.9 },
		{"id" : 5, "lemma" : "로", "type" : "JKB", "position" : 198, "weight" : 0.153229 },
		{"id" : 6, "lemma" : "알려지", "type" : "VV", "position" : 202, "weight" : 0.9 },
		{"id" : 7, "lemma" : "었", "type" : "EP", "position" : 208, "weight" : 0.9 },
		{"id" : 8, "lemma" : "으며", "type" : "EC", "position" : 211, "weight" : 0.197482 },
		{"id" : 9, "lemma" : ",", "type" : "SP", "position" : 217, "weight" : 1 },
		{"id" : 10, "lemma" : "특히", "type" : "MAG", "position" : 219, "weight" : 0.9 },
		{"id" : 11, "lemma" : "스키", "type" : "NNG", "position" : 226, "weight" : 0.260124 },
		{"id" : 12, "lemma" : "장", "type" : "XSN", "position" : 232, "weight" : 0.0110513 },
		{"id" : 13, "lemma" : "이", "type" : "JKS", "position" : 235, "weight" : 0.0731805 },
		{"id" : 14, "lemma" : "유명", "type" : "NNG", "position" : 239, "weight" : 0.9 },
		{"id" : 15, "lemma" : "하", "type" : "XSA", "position" : 245, "weight" : 0.0001 },
		{"id" : 16, "lemma" : "다", "type" : "EF", "position" : 248, "weight" : 0.0497538 },
		{"id" : 17, "lemma" : ".", "type" : "SF", "position" : 251, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "프랑스/NNG+의/JKG", "target" : "프랑스의", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "겨울/NNG", "target" : "겨울", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "스포츠/NNG", "target" : "스포츠", "word_id" : 2, "m_begin" : 3, "m_end" : 3},
		{"id" : 3, "result" : "리조트/NNG+로/JKB", "target" : "리조트로", "word_id" : 3, "m_begin" : 4, "m_end" : 5},
		{"id" : 4, "result" : "알려지/VV+었/EP+으며/EC+,/SP", "target" : "알려졌으며,", "word_id" : 4, "m_begin" : 6, "m_end" : 9},
		{"id" : 5, "result" : "특히/MAG", "target" : "특히", "word_id" : 5, "m_begin" : 10, "m_end" : 10},
		{"id" : 6, "result" : "스키장/NNG+이/JKS", "target" : "스키장이", "word_id" : 6, "m_begin" : 11, "m_end" : 13},
		{"id" : 7, "result" : "유명하/VA+다/EF+./SF", "target" : "유명하다.", "word_id" : 7, "m_begin" : 14, "m_end" : 17}
	],
	"WSD" : [
		{"id" : 0, "text" : "프랑스", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 159, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 168, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "겨울", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 172, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "스포츠", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 179, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "리조트", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 189, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 198, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "알려지", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 202, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 208, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "으며", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 211, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 217, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "특히", "type" : "MAG", "scode" : "00", "weight" : 1, "position" : 219, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "스키장", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 226, "begin" : 11, "end" : 12},
		{"id" : 12, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 235, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "유명하", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 239, "begin" : 14, "end" : 15},
		{"id" : 14, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 248, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 251, "begin" : 17, "end" : 17}
	],
	"word" : [
		{"id" : 0, "text" : "프랑스의", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "겨울", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "스포츠", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "리조트로", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 4, "text" : "알려졌으며,", "type" : "", "begin" : 6, "end" : 9},
		{"id" : 5, "text" : "특히", "type" : "", "begin" : 10, "end" : 10},
		{"id" : 6, "text" : "스키장이", "type" : "", "begin" : 11, "end" : 13},
		{"id" : 7, "text" : "유명하다.", "type" : "", "begin" : 14, "end" : 17}
	],
	"NE" : [
		{"id" : 0, "text" : "프랑스", "type" : "LCP_COUNTRY", "begin" : 0, "end" : 0, "weight" : 0.386626, "common_noun" : 0},
		{"id" : 1, "text" : "겨울", "type" : "DT_SEASON", "begin" : 2, "end" : 2, "weight" : 0.214275, "common_noun" : 0},
		{"id" : 2, "text" : "스키", "type" : "CV_SPORTS", "begin" : 11, "end" : 11, "weight" : 0.364381, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "프랑스의", "head" : 3, "label" : "NP_MOD", "mod" : [], "weight" : 0.864534 },
		{"id" : 1, "text" : "겨울", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.691022 },
		{"id" : 2, "text" : "스포츠", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.860678 },
		{"id" : 3, "text" : "리조트로", "head" : 4, "label" : "NP_AJT", "mod" : [0, 2], "weight" : 0.591392 },
		{"id" : 4, "text" : "알려졌으며,", "head" : 7, "label" : "VP", "mod" : [3], "weight" : 0.799173 },
		{"id" : 5, "text" : "특히", "head" : 7, "label" : "AP", "mod" : [], "weight" : 0.614715 },
		{"id" : 6, "text" : "스키장이", "head" : 7, "label" : "NP_SBJ", "mod" : [], "weight" : 0.509701 },
		{"id" : 7, "text" : "유명하다.", "head" : -1, "label" : "VP", "mod" : [4, 5, 6], "weight" : 0.0410832 }
	],
	"SRL" : [
		{"verb" : "알려지", "sense" : 1, "word_id" : 4, "weight" : 0.102538,
			"argument" : [
				{"type" : "ARG0", "word_id" : 3, "text" : "리조트로", "weight" : 0.102538 }
			] },
		{"verb" : "유명", "sense" : 1, "word_id" : 7, "weight" : 0.140865,
			"argument" : [
				{"type" : "ARGM-ADV", "word_id" : 5, "text" : "특히", "weight" : 0.110842 },
				{"type" : "ARG1", "word_id" : 6, "text" : "스키장이", "weight" : 0.170888 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 4, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.491733 }
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "1924년 동계 올림픽과 1960년 동계 유니버시아드가 이 곳에서 열렸다.",
	"morp" : [
		{"id" : 0, "lemma" : "1924", "type" : "SN", "position" : 252, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 256, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "동계", "type" : "NNG", "position" : 260, "weight" : 0.107886 },
		{"id" : 3, "lemma" : "올림픽", "type" : "NNG", "position" : 267, "weight" : 0.9 },
		{"id" : 4, "lemma" : "과", "type" : "JC", "position" : 276, "weight" : 0.017569 },
		{"id" : 5, "lemma" : "1960", "type" : "SN", "position" : 280, "weight" : 1 },
		{"id" : 6, "lemma" : "년", "type" : "NNB", "position" : 284, "weight" : 0.414343 },
		{"id" : 7, "lemma" : "동계", "type" : "NNG", "position" : 288, "weight" : 0.107886 },
		{"id" : 8, "lemma" : "유니", "type" : "NNP", "position" : 295, "weight" : 0.6 },
		{"id" : 9, "lemma" : "버시아드", "type" : "NNP", "position" : 301, "weight" : 0.6 },
		{"id" : 10, "lemma" : "가", "type" : "JKS", "position" : 313, "weight" : 0.0431193 },
		{"id" : 11, "lemma" : "이", "type" : "MM", "position" : 317, "weight" : 0.00105067 },
		{"id" : 12, "lemma" : "곳", "type" : "NNG", "position" : 321, "weight" : 0.737411 },
		{"id" : 13, "lemma" : "에서", "type" : "JKB", "position" : 324, "weight" : 0.153407 },
		{"id" : 14, "lemma" : "열리", "type" : "VV", "position" : 331, "weight" : 0.9 },
		{"id" : 15, "lemma" : "었", "type" : "EP", "position" : 334, "weight" : 0.9 },
		{"id" : 16, "lemma" : "다", "type" : "EF", "position" : 337, "weight" : 0.640954 },
		{"id" : 17, "lemma" : ".", "type" : "SF", "position" : 340, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "1924/SN+년/NNB", "target" : "1924년", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "동계/NNG", "target" : "동계", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "올림픽/NNG+과/JC", "target" : "올림픽과", "word_id" : 2, "m_begin" : 3, "m_end" : 4},
		{"id" : 3, "result" : "1960/SN+년/NNB", "target" : "1960년", "word_id" : 3, "m_begin" : 5, "m_end" : 6},
		{"id" : 4, "result" : "동계/NNG", "target" : "동계", "word_id" : 4, "m_begin" : 7, "m_end" : 7},
		{"id" : 5, "result" : "유니버시아드/NNG+가/JKS", "target" : "유니버시아드가", "word_id" : 5, "m_begin" : 8, "m_end" : 10},
		{"id" : 6, "result" : "이/MM", "target" : "이", "word_id" : 6, "m_begin" : 11, "m_end" : 11},
		{"id" : 7, "result" : "곳/NNG+에서/JKB", "target" : "곳에서", "word_id" : 7, "m_begin" : 12, "m_end" : 13},
		{"id" : 8, "result" : "열리/VV+었/EP+다/EF+./SF", "target" : "열렸다.", "word_id" : 8, "m_begin" : 14, "m_end" : 17}
	],
	"WSD" : [
		{"id" : 0, "text" : "1924", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 252, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 256, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "동계", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 260, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "올림픽", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 267, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "과", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 276, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "1960", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 280, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 284, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "동계", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 288, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "유니버시아드", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 295, "begin" : 8, "end" : 9},
		{"id" : 9, "text" : "가", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 313, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 317, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "곳", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 321, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 324, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "열리", "type" : "VV", "scode" : "02", "weight" : 1, "position" : 331, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 334, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 337, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 340, "begin" : 17, "end" : 17}
	],
	"word" : [
		{"id" : 0, "text" : "1924년", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "동계", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "올림픽과", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 3, "text" : "1960년", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 4, "text" : "동계", "type" : "", "begin" : 7, "end" : 7},
		{"id" : 5, "text" : "유니버시아드가", "type" : "", "begin" : 8, "end" : 10},
		{"id" : 6, "text" : "이", "type" : "", "begin" : 11, "end" : 11},
		{"id" : 7, "text" : "곳에서", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 8, "text" : "열렸다.", "type" : "", "begin" : 14, "end" : 17}
	],
	"NE" : [
		{"id" : 0, "text" : "1924년", "type" : "DT_YEAR", "begin" : 0, "end" : 1, "weight" : 0.787925, "common_noun" : 0},
		{"id" : 1, "text" : "동계 올림픽", "type" : "EV_SPORTS", "begin" : 2, "end" : 3, "weight" : 0.284506, "common_noun" : 0},
		{"id" : 2, "text" : "1960년", "type" : "DT_YEAR", "begin" : 5, "end" : 6, "weight" : 0.658433, "common_noun" : 0},
		{"id" : 3, "text" : "유니버시아드", "type" : "EV_SPORTS", "begin" : 8, "end" : 9, "weight" : 0.0993993, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "1924년", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.640933 },
		{"id" : 1, "text" : "동계", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.755578 },
		{"id" : 2, "text" : "올림픽과", "head" : 5, "label" : "NP_CNJ", "mod" : [0, 1], "weight" : 0.904431 },
		{"id" : 3, "text" : "1960년", "head" : 4, "label" : "NP", "mod" : [], "weight" : 0.540707 },
		{"id" : 4, "text" : "동계", "head" : 5, "label" : "NP", "mod" : [3], "weight" : 0.805904 },
		{"id" : 5, "text" : "유니버시아드가", "head" : 8, "label" : "NP_SBJ", "mod" : [2, 4], "weight" : 0.50103 },
		{"id" : 6, "text" : "이", "head" : 7, "label" : "DP", "mod" : [], "weight" : 0.829977 },
		{"id" : 7, "text" : "곳에서", "head" : 8, "label" : "NP_AJT", "mod" : [6], "weight" : 0.591655 },
		{"id" : 8, "text" : "열렸다.", "head" : -1, "label" : "VP", "mod" : [5, 7], "weight" : 0.0349588 }
	],
	"SRL" : [
		{"verb" : "열리", "sense" : 1, "word_id" : 8, "weight" : 0.197888,
			"argument" : [
				{"type" : "ARG1", "word_id" : 5, "text" : "유니버시아드가", "weight" : 0.137569 },
				{"type" : "ARGM-LOC", "word_id" : 7, "text" : "곳에서", "weight" : 0.258206 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "LCG_MOUNTAIN", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 1, "sent_id" : 0, "start_eid" : 0, "end_eid" : 0, "ne_id" : 0, "text" : "﻿샤모니몽블랑", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "﻿샤모니몽블랑,", "weight" : 0.002 },
		{"id" : 2, "sent_id" : 0, "start_eid" : 3, "end_eid" : 3, "ne_id" : 1, "text" : "샤모니", "start_eid_short" : 3, "end_eid_short" : 3, "text_short" : "샤모니", "weight" : 0.003 }
	] },
	{"id" : 1, "type" : "LCP_COUNTRY", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "프랑스", 
	 "mention" : [
		{"id" : 6, "sent_id" : 0, "start_eid" : 8, "end_eid" : 8, "ne_id" : 3, "text" : "프랑스", "start_eid_short" : 8, "end_eid_short" : 8, "text_short" : "프랑스", "weight" : 0.01 },
		{"id" : 10, "sent_id" : 1, "start_eid" : 0, "end_eid" : 0, "ne_id" : 0, "text" : "프랑스", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "프랑스", "weight" : 0.016 }
	] },
	{"id" : 2, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "프랑스", 
	 "mention" : [
		{"id" : 9, "sent_id" : 1, "start_eid" : 0, "end_eid" : 3, "ne_id" : -1, "text" : "프랑스의 겨울 스포츠 리조트", "start_eid_short" : 0, "end_eid_short" : 3, "text_short" : "프랑스의 겨울 스포츠 리조트", "weight" : 0.002 },
		{"id" : 20, "sent_id" : 2, "start_eid" : 6, "end_eid" : 7, "ne_id" : -1, "text" : "이 곳", "start_eid_short" : 6, "end_eid_short" : 7, "text_short" : "이 곳", "weight" : 0.006 }
	] }
 ]
}

