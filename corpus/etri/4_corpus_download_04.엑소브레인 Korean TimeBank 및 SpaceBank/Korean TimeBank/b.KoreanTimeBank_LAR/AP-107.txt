{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿진흥왕 순수비(眞興王巡狩碑)는 신라 진흥왕이 재위 기간 중 영토 확장 사업을 하면서, 그 확장된 곳을 돌아보면서(순수(巡狩)) 세운 비석을 가리킨다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿진흥왕", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "순수", "type" : "NNG", "position" : 13, "weight" : 0.280448 },
		{"id" : 2, "lemma" : "비", "type" : "NNG", "position" : 19, "weight" : 0.08306 },
		{"id" : 3, "lemma" : "(", "type" : "SS", "position" : 22, "weight" : 1 },
		{"id" : 4, "lemma" : "眞興王巡狩碑", "type" : "SH", "position" : 23, "weight" : 1 },
		{"id" : 5, "lemma" : ")", "type" : "SS", "position" : 41, "weight" : 1 },
		{"id" : 6, "lemma" : "는", "type" : "JX", "position" : 42, "weight" : 0.00823314 },
		{"id" : 7, "lemma" : "신라", "type" : "NNP", "position" : 46, "weight" : 0.9 },
		{"id" : 8, "lemma" : "진흥왕", "type" : "NNP", "position" : 53, "weight" : 0.080116 },
		{"id" : 9, "lemma" : "이", "type" : "JKS", "position" : 62, "weight" : 0.0234517 },
		{"id" : 10, "lemma" : "재위", "type" : "NNG", "position" : 66, "weight" : 0.9 },
		{"id" : 11, "lemma" : "기간", "type" : "NNG", "position" : 73, "weight" : 0.9 },
		{"id" : 12, "lemma" : "중", "type" : "NNB", "position" : 80, "weight" : 0.013531 },
		{"id" : 13, "lemma" : "영토", "type" : "NNG", "position" : 84, "weight" : 0.9 },
		{"id" : 14, "lemma" : "확장", "type" : "NNG", "position" : 91, "weight" : 0.9 },
		{"id" : 15, "lemma" : "사업", "type" : "NNG", "position" : 98, "weight" : 0.9 },
		{"id" : 16, "lemma" : "을", "type" : "JKO", "position" : 104, "weight" : 0.129611 },
		{"id" : 17, "lemma" : "하", "type" : "VV", "position" : 108, "weight" : 0.48169 },
		{"id" : 18, "lemma" : "면서", "type" : "EC", "position" : 111, "weight" : 0.428918 },
		{"id" : 19, "lemma" : ",", "type" : "SP", "position" : 117, "weight" : 1 },
		{"id" : 20, "lemma" : "그", "type" : "MM", "position" : 119, "weight" : 0.0224513 },
		{"id" : 21, "lemma" : "확장", "type" : "NNG", "position" : 123, "weight" : 0.9 },
		{"id" : 22, "lemma" : "되", "type" : "XSV", "position" : 129, "weight" : 0.000224177 },
		{"id" : 23, "lemma" : "ㄴ", "type" : "ETM", "position" : 129, "weight" : 0.392321 },
		{"id" : 24, "lemma" : "곳", "type" : "NNG", "position" : 133, "weight" : 0.658803 },
		{"id" : 25, "lemma" : "을", "type" : "JKO", "position" : 136, "weight" : 0.129611 },
		{"id" : 26, "lemma" : "돌아보", "type" : "VV", "position" : 140, "weight" : 0.9 },
		{"id" : 27, "lemma" : "면서", "type" : "EC", "position" : 149, "weight" : 0.428918 },
		{"id" : 28, "lemma" : "(", "type" : "SS", "position" : 155, "weight" : 1 },
		{"id" : 29, "lemma" : "순수", "type" : "NNG", "position" : 156, "weight" : 0.198906 },
		{"id" : 30, "lemma" : "(", "type" : "SS", "position" : 162, "weight" : 1 },
		{"id" : 31, "lemma" : "巡狩", "type" : "SH", "position" : 163, "weight" : 1 },
		{"id" : 32, "lemma" : ")", "type" : "SS", "position" : 169, "weight" : 1 },
		{"id" : 33, "lemma" : ")", "type" : "SS", "position" : 170, "weight" : 1 },
		{"id" : 34, "lemma" : "세우", "type" : "VV", "position" : 172, "weight" : 0.0281251 },
		{"id" : 35, "lemma" : "ㄴ", "type" : "ETM", "position" : 175, "weight" : 0.304215 },
		{"id" : 36, "lemma" : "비석", "type" : "NNG", "position" : 179, "weight" : 0.9 },
		{"id" : 37, "lemma" : "을", "type" : "JKO", "position" : 185, "weight" : 0.129611 },
		{"id" : 38, "lemma" : "가리키", "type" : "VV", "position" : 189, "weight" : 0.9 },
		{"id" : 39, "lemma" : "ㄴ다", "type" : "EF", "position" : 195, "weight" : 0.0793196 },
		{"id" : 40, "lemma" : ".", "type" : "SF", "position" : 201, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿진흥왕/NNG", "target" : "﻿진흥왕", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "순수비/NNG+(/SS+眞興王巡狩碑/SH+)/SS+는/JX", "target" : "순수비(眞興王巡狩碑)는", "word_id" : 1, "m_begin" : 1, "m_end" : 6},
		{"id" : 2, "result" : "신라/NNG", "target" : "신라", "word_id" : 2, "m_begin" : 7, "m_end" : 7},
		{"id" : 3, "result" : "진흥왕/NNG+이/JKS", "target" : "진흥왕이", "word_id" : 3, "m_begin" : 8, "m_end" : 9},
		{"id" : 4, "result" : "재위/NNG", "target" : "재위", "word_id" : 4, "m_begin" : 10, "m_end" : 10},
		{"id" : 5, "result" : "기간/NNG", "target" : "기간", "word_id" : 5, "m_begin" : 11, "m_end" : 11},
		{"id" : 6, "result" : "중/NNB", "target" : "중", "word_id" : 6, "m_begin" : 12, "m_end" : 12},
		{"id" : 7, "result" : "영토/NNG", "target" : "영토", "word_id" : 7, "m_begin" : 13, "m_end" : 13},
		{"id" : 8, "result" : "확장/NNG", "target" : "확장", "word_id" : 8, "m_begin" : 14, "m_end" : 14},
		{"id" : 9, "result" : "사업/NNG+을/JKO", "target" : "사업을", "word_id" : 9, "m_begin" : 15, "m_end" : 16},
		{"id" : 10, "result" : "하/VV+면서/EC+,/SP", "target" : "하면서,", "word_id" : 10, "m_begin" : 17, "m_end" : 19},
		{"id" : 11, "result" : "그/MM", "target" : "그", "word_id" : 11, "m_begin" : 20, "m_end" : 20},
		{"id" : 12, "result" : "확장되/VV+ㄴ/ETM", "target" : "확장된", "word_id" : 12, "m_begin" : 21, "m_end" : 23},
		{"id" : 13, "result" : "곳/NNG+을/JKO", "target" : "곳을", "word_id" : 13, "m_begin" : 24, "m_end" : 25},
		{"id" : 14, "result" : "돌아보/VV+면서/EC+(/SS+순수/NNG+(/SS+巡狩/SH+)/SS+)/SS", "target" : "돌아보면서(순수(巡狩))", "word_id" : 14, "m_begin" : 26, "m_end" : 33},
		{"id" : 15, "result" : "세우/VV+ㄴ/ETM", "target" : "세운", "word_id" : 15, "m_begin" : 34, "m_end" : 35},
		{"id" : 16, "result" : "비석/NNG+을/JKO", "target" : "비석을", "word_id" : 16, "m_begin" : 36, "m_end" : 37},
		{"id" : 17, "result" : "가리키/VV+ㄴ다/EF+./SF", "target" : "가리킨다.", "word_id" : 17, "m_begin" : 38, "m_end" : 40}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿진흥왕", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "순수비", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 13, "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 22, "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "眞興王巡狩碑", "type" : "SH", "scode" : "00", "weight" : 1, "position" : 23, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 41, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 42, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "신라", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 46, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "진흥왕", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 53, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 62, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "재위", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 66, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "기간", "type" : "NNG", "scode" : "07", "weight" : 1, "position" : 73, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "중", "type" : "NNB", "scode" : "04", "weight" : 1, "position" : 80, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "영토", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 84, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "확장", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 91, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "사업", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 98, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 104, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 108, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "면서", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 111, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 117, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "그", "type" : "MM", "scode" : "01", "weight" : 1, "position" : 119, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "확장되", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 123, "begin" : 21, "end" : 22},
		{"id" : 21, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 129, "begin" : 23, "end" : 23},
		{"id" : 22, "text" : "곳", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 133, "begin" : 24, "end" : 24},
		{"id" : 23, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 136, "begin" : 25, "end" : 25},
		{"id" : 24, "text" : "돌아보", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 140, "begin" : 26, "end" : 26},
		{"id" : 25, "text" : "면서", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 149, "begin" : 27, "end" : 27},
		{"id" : 26, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 155, "begin" : 28, "end" : 28},
		{"id" : 27, "text" : "순수", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 156, "begin" : 29, "end" : 29},
		{"id" : 28, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 162, "begin" : 30, "end" : 30},
		{"id" : 29, "text" : "巡狩", "type" : "SH", "scode" : "00", "weight" : 1, "position" : 163, "begin" : 31, "end" : 31},
		{"id" : 30, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 169, "begin" : 32, "end" : 32},
		{"id" : 31, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 170, "begin" : 33, "end" : 33},
		{"id" : 32, "text" : "세우", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 172, "begin" : 34, "end" : 34},
		{"id" : 33, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 175, "begin" : 35, "end" : 35},
		{"id" : 34, "text" : "비석", "type" : "NNG", "scode" : "07", "weight" : 1, "position" : 179, "begin" : 36, "end" : 36},
		{"id" : 35, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 185, "begin" : 37, "end" : 37},
		{"id" : 36, "text" : "가리키", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 189, "begin" : 38, "end" : 38},
		{"id" : 37, "text" : "ㄴ다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 195, "begin" : 39, "end" : 39},
		{"id" : 38, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 201, "begin" : 40, "end" : 40}
	],
	"word" : [
		{"id" : 0, "text" : "﻿진흥왕", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "순수비(眞興王巡狩碑)는", "type" : "", "begin" : 1, "end" : 6},
		{"id" : 2, "text" : "신라", "type" : "", "begin" : 7, "end" : 7},
		{"id" : 3, "text" : "진흥왕이", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 4, "text" : "재위", "type" : "", "begin" : 10, "end" : 10},
		{"id" : 5, "text" : "기간", "type" : "", "begin" : 11, "end" : 11},
		{"id" : 6, "text" : "중", "type" : "", "begin" : 12, "end" : 12},
		{"id" : 7, "text" : "영토", "type" : "", "begin" : 13, "end" : 13},
		{"id" : 8, "text" : "확장", "type" : "", "begin" : 14, "end" : 14},
		{"id" : 9, "text" : "사업을", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 10, "text" : "하면서,", "type" : "", "begin" : 17, "end" : 19},
		{"id" : 11, "text" : "그", "type" : "", "begin" : 20, "end" : 20},
		{"id" : 12, "text" : "확장된", "type" : "", "begin" : 21, "end" : 23},
		{"id" : 13, "text" : "곳을", "type" : "", "begin" : 24, "end" : 25},
		{"id" : 14, "text" : "돌아보면서(순수(巡狩))", "type" : "", "begin" : 26, "end" : 33},
		{"id" : 15, "text" : "세운", "type" : "", "begin" : 34, "end" : 35},
		{"id" : 16, "text" : "비석을", "type" : "", "begin" : 36, "end" : 37},
		{"id" : 17, "text" : "가리킨다.", "type" : "", "begin" : 38, "end" : 40}
	],
	"NE" : [
		{"id" : 0, "text" : "﻿진흥왕 순수비", "type" : "AF_CULTURAL_ASSET", "begin" : 0, "end" : 2, "weight" : 0.153309, "common_noun" : 0},
		{"id" : 1, "text" : "신라", "type" : "LCP_COUNTRY", "begin" : 7, "end" : 7, "weight" : 0.355261, "common_noun" : 0},
		{"id" : 2, "text" : "진흥왕", "type" : "PS_NAME", "begin" : 8, "end" : 8, "weight" : 0.235453, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿진흥왕", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.932651 },
		{"id" : 1, "text" : "순수비(眞興王巡狩碑)는", "head" : 17, "label" : "NP_SBJ", "mod" : [0], "weight" : 0.94971 },
		{"id" : 2, "text" : "신라", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.914012 },
		{"id" : 3, "text" : "진흥왕이", "head" : 10, "label" : "NP_SBJ", "mod" : [2], "weight" : 0.99761 },
		{"id" : 4, "text" : "재위", "head" : 5, "label" : "NP", "mod" : [], "weight" : 0.924416 },
		{"id" : 5, "text" : "기간", "head" : 6, "label" : "NP", "mod" : [4], "weight" : 0.774591 },
		{"id" : 6, "text" : "중", "head" : 10, "label" : "NP_AJT", "mod" : [5], "weight" : 0.858983 },
		{"id" : 7, "text" : "영토", "head" : 8, "label" : "NP", "mod" : [], "weight" : 0.712536 },
		{"id" : 8, "text" : "확장", "head" : 9, "label" : "NP", "mod" : [7], "weight" : 0.702359 },
		{"id" : 9, "text" : "사업을", "head" : 10, "label" : "NP_OBJ", "mod" : [8], "weight" : 0.821674 },
		{"id" : 10, "text" : "하면서,", "head" : 14, "label" : "VP", "mod" : [3, 6, 9], "weight" : 0.682614 },
		{"id" : 11, "text" : "그", "head" : 13, "label" : "DP", "mod" : [], "weight" : 0.783109 },
		{"id" : 12, "text" : "확장된", "head" : 13, "label" : "VP_MOD", "mod" : [], "weight" : 0.828486 },
		{"id" : 13, "text" : "곳을", "head" : 14, "label" : "NP_OBJ", "mod" : [11, 12], "weight" : 0.975876 },
		{"id" : 14, "text" : "돌아보면서(순수(巡狩))", "head" : 15, "label" : "VP", "mod" : [10, 13], "weight" : 0.807517 },
		{"id" : 15, "text" : "세운", "head" : 16, "label" : "VP_MOD", "mod" : [14], "weight" : 0.842052 },
		{"id" : 16, "text" : "비석을", "head" : 17, "label" : "NP_OBJ", "mod" : [15], "weight" : 0.611099 },
		{"id" : 17, "text" : "가리킨다.", "head" : -1, "label" : "VP", "mod" : [1, 16], "weight" : 0.0284384 }
	],
	"SRL" : [
		{"verb" : "하", "sense" : 1, "word_id" : 10, "weight" : 0.421011,
			"argument" : [
				{"type" : "ARG0", "word_id" : 3, "text" : "진흥왕이", "weight" : 0.57148 },
				{"type" : "ARG1", "word_id" : 9, "text" : "사업을", "weight" : 0.270542 }
			] },
		{"verb" : "확장", "sense" : 1, "word_id" : 12, "weight" : 0.177051,
			"argument" : [
				{"type" : "ARG1", "word_id" : 13, "text" : "곳을", "weight" : 0.177051 }
			] },
		{"verb" : "돌아보", "sense" : 1, "word_id" : 14, "weight" : 0.314781,
			"argument" : [
				{"type" : "ARG0", "word_id" : 3, "text" : "진흥왕이", "weight" : 0.423704 },
				{"type" : "ARG1", "word_id" : 13, "text" : "곳을", "weight" : 0.205859 }
			] },
		{"verb" : "세우", "sense" : 2, "word_id" : 15, "weight" : 0.248345,
			"argument" : [
				{"type" : "ARG0", "word_id" : 3, "text" : "진흥왕이", "weight" : 0.409602 },
				{"type" : "ARGM-MNR", "word_id" : 14, "text" : "돌아보면서(순수(巡狩))", "weight" : 0.150705 },
				{"type" : "ARG1", "word_id" : 16, "text" : "비석을", "weight" : 0.184729 }
			] },
		{"verb" : "가리키", "sense" : 1, "word_id" : 17, "weight" : 0.278728,
			"argument" : [
				{"type" : "ARG1", "word_id" : 1, "text" : "순수비(眞興王巡狩碑)는", "weight" : 0.196412 },
				{"type" : "ARG1", "word_id" : 16, "text" : "비석을", "weight" : 0.361044 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 14, "ant_sid" : 0, "ant_wid" : 1, "type" : "s", "istitle" : 0, "weight" : 0.310316 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "현재 북한산비, 황초령비, 마운령비, 창녕비가 남아 있다. ",
	"morp" : [
		{"id" : 0, "lemma" : "현재", "type" : "MAG", "position" : 203, "weight" : 0.0393399 },
		{"id" : 1, "lemma" : "북한산", "type" : "NNP", "position" : 210, "weight" : 0.9 },
		{"id" : 2, "lemma" : "비", "type" : "NNG", "position" : 219, "weight" : 0.126716 },
		{"id" : 3, "lemma" : ",", "type" : "SP", "position" : 222, "weight" : 1 },
		{"id" : 4, "lemma" : "황초령", "type" : "NNP", "position" : 224, "weight" : 0.55 },
		{"id" : 5, "lemma" : "비", "type" : "NNG", "position" : 233, "weight" : 0.126716 },
		{"id" : 6, "lemma" : ",", "type" : "SP", "position" : 236, "weight" : 1 },
		{"id" : 7, "lemma" : "마운령", "type" : "NNP", "position" : 238, "weight" : 0.2 },
		{"id" : 8, "lemma" : "비", "type" : "NNG", "position" : 247, "weight" : 0.126716 },
		{"id" : 9, "lemma" : ",", "type" : "SP", "position" : 250, "weight" : 1 },
		{"id" : 10, "lemma" : "창녕", "type" : "NNP", "position" : 252, "weight" : 0.9 },
		{"id" : 11, "lemma" : "비", "type" : "XSN", "position" : 258, "weight" : 0.00121376 },
		{"id" : 12, "lemma" : "가", "type" : "JKS", "position" : 261, "weight" : 0.134553 },
		{"id" : 13, "lemma" : "남", "type" : "VV", "position" : 265, "weight" : 0.133588 },
		{"id" : 14, "lemma" : "아", "type" : "EC", "position" : 268, "weight" : 0.398809 },
		{"id" : 15, "lemma" : "있", "type" : "VX", "position" : 272, "weight" : 0.125953 },
		{"id" : 16, "lemma" : "다", "type" : "EF", "position" : 275, "weight" : 0.180366 },
		{"id" : 17, "lemma" : ".", "type" : "SF", "position" : 278, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "현재/MAG", "target" : "현재", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "북한산비/NNG+,/SP", "target" : "북한산비,", "word_id" : 1, "m_begin" : 1, "m_end" : 3},
		{"id" : 2, "result" : "황초령비/NNG+,/SP", "target" : "황초령비,", "word_id" : 2, "m_begin" : 4, "m_end" : 6},
		{"id" : 3, "result" : "마운령비/NNG+,/SP", "target" : "마운령비,", "word_id" : 3, "m_begin" : 7, "m_end" : 9},
		{"id" : 4, "result" : "창녕비/NNG+가/JKS", "target" : "창녕비가", "word_id" : 4, "m_begin" : 10, "m_end" : 12},
		{"id" : 5, "result" : "남/VV+어/EC", "target" : "남아", "word_id" : 5, "m_begin" : 13, "m_end" : 14},
		{"id" : 6, "result" : "있/VX+다/EF+./SF", "target" : "있다.", "word_id" : 6, "m_begin" : 15, "m_end" : 17}
	],
	"WSD" : [
		{"id" : 0, "text" : "현재", "type" : "MAG", "scode" : "02", "weight" : 1, "position" : 203, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "북한산", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 210, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "비", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 219, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 222, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "황초령비", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 224, "begin" : 4, "end" : 5},
		{"id" : 5, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 236, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "마운령비", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 238, "begin" : 7, "end" : 8},
		{"id" : 7, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 250, "begin" : 9, "end" : 9},
		{"id" : 8, "text" : "창녕", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 252, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "비", "type" : "XSN", "scode" : "33", "weight" : 1, "position" : 258, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "가", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 261, "begin" : 12, "end" : 12},
		{"id" : 11, "text" : "남", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 265, "begin" : 13, "end" : 13},
		{"id" : 12, "text" : "아", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 268, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "있", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 272, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 275, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 278, "begin" : 17, "end" : 17}
	],
	"word" : [
		{"id" : 0, "text" : "현재", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "북한산비,", "type" : "", "begin" : 1, "end" : 3},
		{"id" : 2, "text" : "황초령비,", "type" : "", "begin" : 4, "end" : 6},
		{"id" : 3, "text" : "마운령비,", "type" : "", "begin" : 7, "end" : 9},
		{"id" : 4, "text" : "창녕비가", "type" : "", "begin" : 10, "end" : 12},
		{"id" : 5, "text" : "남아", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 6, "text" : "있다.", "type" : "", "begin" : 15, "end" : 17}
	],
	"NE" : [
		{"id" : 0, "text" : "북한산비", "type" : "AF_CULTURAL_ASSET", "begin" : 1, "end" : 2, "weight" : 0.179994, "common_noun" : 0},
		{"id" : 1, "text" : "황초령비", "type" : "LCG_MOUNTAIN", "begin" : 4, "end" : 5, "weight" : 0.10647, "common_noun" : 0},
		{"id" : 2, "text" : "마운령비", "type" : "LCG_MOUNTAIN", "begin" : 7, "end" : 8, "weight" : 0.115018, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "현재", "head" : 5, "label" : "AP", "mod" : [], "weight" : 0.700272 },
		{"id" : 1, "text" : "북한산비,", "head" : 4, "label" : "NP_CNJ", "mod" : [], "weight" : 0.808973 },
		{"id" : 2, "text" : "황초령비,", "head" : 4, "label" : "NP_CNJ", "mod" : [], "weight" : 0.91883 },
		{"id" : 3, "text" : "마운령비,", "head" : 4, "label" : "NP_CNJ", "mod" : [], "weight" : 0.907489 },
		{"id" : 4, "text" : "창녕비가", "head" : 5, "label" : "NP_SBJ", "mod" : [1, 2, 3], "weight" : 0.663549 },
		{"id" : 5, "text" : "남아", "head" : 6, "label" : "VP", "mod" : [0, 4], "weight" : 0.46499 },
		{"id" : 6, "text" : "있다.", "head" : -1, "label" : "VP", "mod" : [5], "weight" : 0.105135 }
	],
	"SRL" : [
		{"verb" : "남", "sense" : 1, "word_id" : 5, "weight" : 0.349587,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 0, "text" : "현재", "weight" : 0.273242 },
				{"type" : "ARG1", "word_id" : 4, "text" : "창녕비가", "weight" : 0.230648 },
				{"type" : "AUX", "word_id" : 6, "text" : "있다.", "weight" : 0.54487 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
 ]
}

