{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "한승원(1958년 6월 19일 ~ )은 대한민국의 만화가다.",
	"morp" : [
		{"id" : 0, "lemma" : "한승원", "type" : "NNP", "position" : 0, "weight" : 0.7 },
		{"id" : 1, "lemma" : "(", "type" : "SS", "position" : 9, "weight" : 1 },
		{"id" : 2, "lemma" : "1958", "type" : "SN", "position" : 10, "weight" : 1 },
		{"id" : 3, "lemma" : "년", "type" : "NNB", "position" : 14, "weight" : 0.414343 },
		{"id" : 4, "lemma" : "6", "type" : "SN", "position" : 18, "weight" : 1 },
		{"id" : 5, "lemma" : "월", "type" : "NNB", "position" : 19, "weight" : 0.408539 },
		{"id" : 6, "lemma" : "19", "type" : "SN", "position" : 23, "weight" : 1 },
		{"id" : 7, "lemma" : "일", "type" : "NNB", "position" : 25, "weight" : 0.126777 },
		{"id" : 8, "lemma" : "~", "type" : "SO", "position" : 29, "weight" : 1 },
		{"id" : 9, "lemma" : ")", "type" : "SS", "position" : 31, "weight" : 1 },
		{"id" : 10, "lemma" : "은", "type" : "JX", "position" : 32, "weight" : 0.0128817 },
		{"id" : 11, "lemma" : "대한민국", "type" : "NNP", "position" : 36, "weight" : 0.0447775 },
		{"id" : 12, "lemma" : "의", "type" : "JKG", "position" : 48, "weight" : 0.0987295 },
		{"id" : 13, "lemma" : "만화", "type" : "NNG", "position" : 52, "weight" : 0.83848 },
		{"id" : 14, "lemma" : "가", "type" : "XSN", "position" : 58, "weight" : 0.000115417 },
		{"id" : 15, "lemma" : "이", "type" : "VCP", "position" : 58, "weight" : 0.0165001 },
		{"id" : 16, "lemma" : "다", "type" : "EF", "position" : 61, "weight" : 0.353579 },
		{"id" : 17, "lemma" : ".", "type" : "SF", "position" : 64, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "한승원/NNG+(/SS+1958/SN+년/NNB", "target" : "한승원(1958년", "word_id" : 0, "m_begin" : 0, "m_end" : 3},
		{"id" : 1, "result" : "6/SN+월/NNB", "target" : "6월", "word_id" : 1, "m_begin" : 4, "m_end" : 5},
		{"id" : 2, "result" : "19/SN+일/NNB", "target" : "19일", "word_id" : 2, "m_begin" : 6, "m_end" : 7},
		{"id" : 3, "result" : "~/SO", "target" : "~", "word_id" : 3, "m_begin" : 8, "m_end" : 8},
		{"id" : 4, "result" : ")/SS+은/JX", "target" : ")은", "word_id" : 4, "m_begin" : 9, "m_end" : 10},
		{"id" : 5, "result" : "대한민국/NNG+의/JKG", "target" : "대한민국의", "word_id" : 5, "m_begin" : 11, "m_end" : 12},
		{"id" : 6, "result" : "만화가/NNG+이/VCP+다/EF+./SF", "target" : "만화가다.", "word_id" : 6, "m_begin" : 13, "m_end" : 17}
	],
	"WSD" : [
		{"id" : 0, "text" : "한승원", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "1958", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 14, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "6", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 18, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "월", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 19, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "19", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 23, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "일", "type" : "NNB", "scode" : "07", "weight" : 1, "position" : 25, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "~", "type" : "SO", "scode" : "00", "weight" : 1, "position" : 29, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 31, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 32, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "대한민국", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 36, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 48, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "만화가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 52, "begin" : 13, "end" : 14},
		{"id" : 14, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 58, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 61, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 64, "begin" : 17, "end" : 17}
	],
	"word" : [
		{"id" : 0, "text" : "한승원(1958년", "type" : "", "begin" : 0, "end" : 3},
		{"id" : 1, "text" : "6월", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 2, "text" : "19일", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 3, "text" : "~", "type" : "", "begin" : 8, "end" : 8},
		{"id" : 4, "text" : ")은", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 5, "text" : "대한민국의", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 6, "text" : "만화가다.", "type" : "", "begin" : 13, "end" : 17}
	],
	"NE" : [
		{"id" : 0, "text" : "한승원", "type" : "PS_NAME", "begin" : 0, "end" : 0, "weight" : 0.329078, "common_noun" : 0},
		{"id" : 1, "text" : "1958년 6월 19일 ~", "type" : "DT_OTHERS", "begin" : 2, "end" : 8, "weight" : 0.565897, "common_noun" : 0},
		{"id" : 2, "text" : "대한민국", "type" : "LCP_COUNTRY", "begin" : 11, "end" : 11, "weight" : 0.18324, "common_noun" : 0},
		{"id" : 3, "text" : "만화가", "type" : "CV_OCCUPATION", "begin" : 13, "end" : 15, "weight" : 0.22625, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "한승원(1958년", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.582345 },
		{"id" : 1, "text" : "6월", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.729814 },
		{"id" : 2, "text" : "19일", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.291096 },
		{"id" : 3, "text" : "~", "head" : 4, "label" : "X", "mod" : [2], "weight" : 0.749806 },
		{"id" : 4, "text" : ")은", "head" : 6, "label" : "NP_SBJ", "mod" : [3], "weight" : 0.448183 },
		{"id" : 5, "text" : "대한민국의", "head" : 6, "label" : "NP_MOD", "mod" : [], "weight" : 0.431135 },
		{"id" : 6, "text" : "만화가다.", "head" : -1, "label" : "VNP", "mod" : [4, 5], "weight" : 0.0104056 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "서울특별시 출신이다.",
	"morp" : [
		{"id" : 0, "lemma" : "서울특별시", "type" : "NNP", "position" : 65, "weight" : 0.9 },
		{"id" : 1, "lemma" : "출신", "type" : "NNG", "position" : 81, "weight" : 0.9 },
		{"id" : 2, "lemma" : "이", "type" : "VCP", "position" : 87, "weight" : 0.0177525 },
		{"id" : 3, "lemma" : "다", "type" : "EF", "position" : 90, "weight" : 0.353579 },
		{"id" : 4, "lemma" : ".", "type" : "SF", "position" : 93, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "서울특별시/NNG", "target" : "서울특별시", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "출신/NNG+이/VCP+다/EF+./SF", "target" : "출신이다.", "word_id" : 1, "m_begin" : 1, "m_end" : 4}
	],
	"WSD" : [
		{"id" : 0, "text" : "서울특별시", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 65, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "출신", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 81, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 87, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 90, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 93, "begin" : 4, "end" : 4}
	],
	"word" : [
		{"id" : 0, "text" : "서울특별시", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "출신이다.", "type" : "", "begin" : 1, "end" : 4}
	],
	"NE" : [
		{"id" : 0, "text" : "서울특별시", "type" : "LCP_CAPITALCITY", "begin" : 0, "end" : 0, "weight" : 0.516187, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "서울특별시", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.410284 },
		{"id" : 1, "text" : "출신이다.", "head" : -1, "label" : "VNP", "mod" : [0], "weight" : 0.252331 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 1, "ant_sid" : 0, "ant_wid" : 4, "type" : "s", "istitle" : 0, "weight" : 0.603613 }
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "남편은 만화가 김동화이다.",
	"morp" : [
		{"id" : 0, "lemma" : "남편", "type" : "NNG", "position" : 94, "weight" : 0.9 },
		{"id" : 1, "lemma" : "은", "type" : "JX", "position" : 100, "weight" : 0.0449928 },
		{"id" : 2, "lemma" : "만화", "type" : "NNG", "position" : 104, "weight" : 0.312632 },
		{"id" : 3, "lemma" : "가", "type" : "JKS", "position" : 110, "weight" : 0.066324 },
		{"id" : 4, "lemma" : "김동화", "type" : "NNP", "position" : 114, "weight" : 0.05 },
		{"id" : 5, "lemma" : "이", "type" : "VCP", "position" : 123, "weight" : 0.00359898 },
		{"id" : 6, "lemma" : "다", "type" : "EF", "position" : 126, "weight" : 0.353579 },
		{"id" : 7, "lemma" : ".", "type" : "SF", "position" : 129, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "남편/NNG+은/JX", "target" : "남편은", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "만화/NNG+가/JKS", "target" : "만화가", "word_id" : 1, "m_begin" : 2, "m_end" : 3},
		{"id" : 2, "result" : "김동화/NNG+이/VCP+다/EF+./SF", "target" : "김동화이다.", "word_id" : 2, "m_begin" : 4, "m_end" : 7}
	],
	"WSD" : [
		{"id" : 0, "text" : "남편", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 94, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 100, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "만화", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 104, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "가", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 110, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "김동화", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 114, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 123, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 126, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 129, "begin" : 7, "end" : 7}
	],
	"word" : [
		{"id" : 0, "text" : "남편은", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "만화가", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 2, "text" : "김동화이다.", "type" : "", "begin" : 4, "end" : 7}
	],
	"NE" : [
		{"id" : 0, "text" : "남편", "type" : "CV_RELATION", "begin" : 0, "end" : 0, "weight" : 0.267524, "common_noun" : 0},
		{"id" : 1, "text" : "만화", "type" : "FD_ART", "begin" : 2, "end" : 2, "weight" : 0.12356, "common_noun" : 0},
		{"id" : 2, "text" : "김동화", "type" : "PS_NAME", "begin" : 4, "end" : 4, "weight" : 0.330061, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "남편은", "head" : 2, "label" : "NP_SBJ", "mod" : [], "weight" : 0.598878 },
		{"id" : 1, "text" : "만화가", "head" : 2, "label" : "NP_SBJ", "mod" : [], "weight" : 0.49503 },
		{"id" : 2, "text" : "김동화이다.", "head" : -1, "label" : "VNP", "mod" : [0, 1], "weight" : 0.1675 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 3,
	"reserve_str" : "",
	"text" : "김동화와 함께 작업하다가 1981년 〈다섯 번째 계절〉로 데뷔하였다.",
	"morp" : [
		{"id" : 0, "lemma" : "김동화", "type" : "NNP", "position" : 130, "weight" : 0.05 },
		{"id" : 1, "lemma" : "와", "type" : "JKB", "position" : 139, "weight" : 0.0304966 },
		{"id" : 2, "lemma" : "함께", "type" : "MAG", "position" : 143, "weight" : 0.9 },
		{"id" : 3, "lemma" : "작업", "type" : "NNG", "position" : 150, "weight" : 0.9 },
		{"id" : 4, "lemma" : "하", "type" : "XSV", "position" : 156, "weight" : 0.0001 },
		{"id" : 5, "lemma" : "다가", "type" : "EC", "position" : 159, "weight" : 0.35574 },
		{"id" : 6, "lemma" : "1981", "type" : "SN", "position" : 166, "weight" : 1 },
		{"id" : 7, "lemma" : "년", "type" : "NNB", "position" : 170, "weight" : 0.414343 },
		{"id" : 8, "lemma" : "〈", "type" : "SS", "position" : 174, "weight" : 1 },
		{"id" : 9, "lemma" : "다섯", "type" : "NR", "position" : 177, "weight" : 0.9 },
		{"id" : 10, "lemma" : "번째", "type" : "NNB", "position" : 184, "weight" : 0.383074 },
		{"id" : 11, "lemma" : "계절", "type" : "NNG", "position" : 191, "weight" : 0.9 },
		{"id" : 12, "lemma" : "〉", "type" : "SS", "position" : 197, "weight" : 1 },
		{"id" : 13, "lemma" : "로", "type" : "JKB", "position" : 200, "weight" : 0.0485504 },
		{"id" : 14, "lemma" : "데뷔", "type" : "NNG", "position" : 204, "weight" : 0.9 },
		{"id" : 15, "lemma" : "하", "type" : "XSV", "position" : 210, "weight" : 0.0001 },
		{"id" : 16, "lemma" : "었", "type" : "EP", "position" : 213, "weight" : 0.9 },
		{"id" : 17, "lemma" : "다", "type" : "EF", "position" : 216, "weight" : 0.640954 },
		{"id" : 18, "lemma" : ".", "type" : "SF", "position" : 219, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "김동화/NNG+와/JKB", "target" : "김동화와", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "함께/MAG", "target" : "함께", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "작업하/VV+다가/EC", "target" : "작업하다가", "word_id" : 2, "m_begin" : 3, "m_end" : 5},
		{"id" : 3, "result" : "1981/SN+년/NNB", "target" : "1981년", "word_id" : 3, "m_begin" : 6, "m_end" : 7},
		{"id" : 4, "result" : "〈/SS+다섯/NR", "target" : "〈다섯", "word_id" : 4, "m_begin" : 8, "m_end" : 9},
		{"id" : 5, "result" : "번째/NNB", "target" : "번째", "word_id" : 5, "m_begin" : 10, "m_end" : 10},
		{"id" : 6, "result" : "계절/NNG+〉/SS+로/JKB", "target" : "계절〉로", "word_id" : 6, "m_begin" : 11, "m_end" : 13},
		{"id" : 7, "result" : "데뷔하/VV+었/EP+다/EF+./SF", "target" : "데뷔하였다.", "word_id" : 7, "m_begin" : 14, "m_end" : 18}
	],
	"WSD" : [
		{"id" : 0, "text" : "김동화", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 130, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "와", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 139, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "함께", "type" : "MAG", "scode" : "00", "weight" : 1, "position" : 143, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "작업하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 150, "begin" : 3, "end" : 4},
		{"id" : 4, "text" : "다가", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 159, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "1981", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 166, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 170, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "〈", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 174, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "다섯", "type" : "NR", "scode" : "00", "weight" : 1, "position" : 177, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "번째", "type" : "NNB", "scode" : "00", "weight" : 1, "position" : 184, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "계절", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 191, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "〉", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 197, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 200, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "데뷔하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 204, "begin" : 14, "end" : 15},
		{"id" : 14, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 213, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 216, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 219, "begin" : 18, "end" : 18}
	],
	"word" : [
		{"id" : 0, "text" : "김동화와", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "함께", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "작업하다가", "type" : "", "begin" : 3, "end" : 5},
		{"id" : 3, "text" : "1981년", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 4, "text" : "〈다섯", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 5, "text" : "번째", "type" : "", "begin" : 10, "end" : 10},
		{"id" : 6, "text" : "계절〉로", "type" : "", "begin" : 11, "end" : 13},
		{"id" : 7, "text" : "데뷔하였다.", "type" : "", "begin" : 14, "end" : 18}
	],
	"NE" : [
		{"id" : 0, "text" : "김동화", "type" : "PS_NAME", "begin" : 0, "end" : 0, "weight" : 0.388302, "common_noun" : 0},
		{"id" : 1, "text" : "1981년", "type" : "DT_YEAR", "begin" : 6, "end" : 7, "weight" : 0.753474, "common_noun" : 0},
		{"id" : 2, "text" : "다섯 번째 계절", "type" : "AFW_DOCUMENT", "begin" : 9, "end" : 11, "weight" : 0.269679, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "김동화와", "head" : 1, "label" : "NP_AJT", "mod" : [], "weight" : 0.660424 },
		{"id" : 1, "text" : "함께", "head" : 2, "label" : "AP", "mod" : [0], "weight" : 0.652749 },
		{"id" : 2, "text" : "작업하다가", "head" : 7, "label" : "VP", "mod" : [1], "weight" : 0.632014 },
		{"id" : 3, "text" : "1981년", "head" : 6, "label" : "NP", "mod" : [], "weight" : 0.538953 },
		{"id" : 4, "text" : "〈다섯", "head" : 5, "label" : "NP", "mod" : [], "weight" : 0.902732 },
		{"id" : 5, "text" : "번째", "head" : 6, "label" : "NP", "mod" : [4], "weight" : 0.978383 },
		{"id" : 6, "text" : "계절〉로", "head" : 7, "label" : "NP_AJT", "mod" : [3, 5], "weight" : 0.501864 },
		{"id" : 7, "text" : "데뷔하였다.", "head" : -1, "label" : "VP", "mod" : [2, 6], "weight" : 0.0376669 }
	],
	"SRL" : [
		{"verb" : "작업", "sense" : 1, "word_id" : 2, "weight" : 0.171584,
			"argument" : [
				{"type" : "ARGM-MNR", "word_id" : 1, "text" : "함께", "weight" : 0.171584 }
			] },
		{"verb" : "데뷔", "sense" : 1, "word_id" : 7, "weight" : 0.0563416,
			"argument" : [
				{"type" : "ARG2", "word_id" : 6, "text" : "계절〉로", "weight" : 0.0563416 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 2, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.506438 },
		{"id" : 1, "verb_wid" : 7, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.701772 }
	]
	},
	{
	"id" : 4,
	"reserve_str" : "",
	"text" : "만화가 최경아, 원수연, 이빈 작가 등과 지인이다.",
	"morp" : [
		{"id" : 0, "lemma" : "만화", "type" : "NNG", "position" : 220, "weight" : 0.421383 },
		{"id" : 1, "lemma" : "가", "type" : "JKS", "position" : 226, "weight" : 0.066324 },
		{"id" : 2, "lemma" : "최경아", "type" : "NNP", "position" : 230, "weight" : 0.6 },
		{"id" : 3, "lemma" : ",", "type" : "SP", "position" : 239, "weight" : 1 },
		{"id" : 4, "lemma" : "원수연", "type" : "NNP", "position" : 241, "weight" : 0.1 },
		{"id" : 5, "lemma" : ",", "type" : "SP", "position" : 250, "weight" : 1 },
		{"id" : 6, "lemma" : "이빈", "type" : "NNP", "position" : 252, "weight" : 0.111422 },
		{"id" : 7, "lemma" : "작가", "type" : "NNG", "position" : 259, "weight" : 0.281928 },
		{"id" : 8, "lemma" : "등", "type" : "NNB", "position" : 266, "weight" : 0.0146757 },
		{"id" : 9, "lemma" : "과", "type" : "JKB", "position" : 269, "weight" : 0.0444681 },
		{"id" : 10, "lemma" : "지인", "type" : "NNG", "position" : 273, "weight" : 0.203423 },
		{"id" : 11, "lemma" : "이", "type" : "VCP", "position" : 279, "weight" : 0.0177525 },
		{"id" : 12, "lemma" : "다", "type" : "EF", "position" : 282, "weight" : 0.353579 },
		{"id" : 13, "lemma" : ".", "type" : "SF", "position" : 285, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "만화/NNG+가/JKS", "target" : "만화가", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "최경아/NNG+,/SP", "target" : "최경아,", "word_id" : 1, "m_begin" : 2, "m_end" : 3},
		{"id" : 2, "result" : "원수연/NNG+,/SP", "target" : "원수연,", "word_id" : 2, "m_begin" : 4, "m_end" : 5},
		{"id" : 3, "result" : "이빈/NNG", "target" : "이빈", "word_id" : 3, "m_begin" : 6, "m_end" : 6},
		{"id" : 4, "result" : "작가/NNG", "target" : "작가", "word_id" : 4, "m_begin" : 7, "m_end" : 7},
		{"id" : 5, "result" : "등/NNB+과/JKB", "target" : "등과", "word_id" : 5, "m_begin" : 8, "m_end" : 9},
		{"id" : 6, "result" : "지인/NNG+이/VCP+다/EF+./SF", "target" : "지인이다.", "word_id" : 6, "m_begin" : 10, "m_end" : 13}
	],
	"WSD" : [
		{"id" : 0, "text" : "만화", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 220, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "가", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 226, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "최경아", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 230, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 239, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "원수연", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 241, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 250, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "이빈", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 252, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "작가", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 259, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "등", "type" : "NNB", "scode" : "05", "weight" : 1, "position" : 266, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "과", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 269, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "지인", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 273, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 279, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 282, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 285, "begin" : 13, "end" : 13}
	],
	"word" : [
		{"id" : 0, "text" : "만화가", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "최경아,", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 2, "text" : "원수연,", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 3, "text" : "이빈", "type" : "", "begin" : 6, "end" : 6},
		{"id" : 4, "text" : "작가", "type" : "", "begin" : 7, "end" : 7},
		{"id" : 5, "text" : "등과", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 6, "text" : "지인이다.", "type" : "", "begin" : 10, "end" : 13}
	],
	"NE" : [
		{"id" : 0, "text" : "만화", "type" : "FD_ART", "begin" : 0, "end" : 0, "weight" : 0.109898, "common_noun" : 0},
		{"id" : 1, "text" : "최경아", "type" : "PS_NAME", "begin" : 2, "end" : 2, "weight" : 0.472667, "common_noun" : 0},
		{"id" : 2, "text" : "원수연", "type" : "PS_NAME", "begin" : 4, "end" : 4, "weight" : 0.248884, "common_noun" : 0},
		{"id" : 3, "text" : "이빈", "type" : "PS_NAME", "begin" : 6, "end" : 6, "weight" : 0.256937, "common_noun" : 0},
		{"id" : 4, "text" : "작가", "type" : "CV_OCCUPATION", "begin" : 7, "end" : 7, "weight" : 0.410574, "common_noun" : 0},
		{"id" : 5, "text" : "지인", "type" : "CV_RELATION", "begin" : 10, "end" : 10, "weight" : 0.225721, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "만화가", "head" : 6, "label" : "NP_SBJ", "mod" : [], "weight" : 0.573969 },
		{"id" : 1, "text" : "최경아,", "head" : 4, "label" : "NP_CNJ", "mod" : [], "weight" : 0.952592 },
		{"id" : 2, "text" : "원수연,", "head" : 4, "label" : "NP_CNJ", "mod" : [], "weight" : 0.93028 },
		{"id" : 3, "text" : "이빈", "head" : 4, "label" : "NP", "mod" : [], "weight" : 0.729756 },
		{"id" : 4, "text" : "작가", "head" : 5, "label" : "NP", "mod" : [1, 2, 3], "weight" : 0.859644 },
		{"id" : 5, "text" : "등과", "head" : 6, "label" : "NP_AJT", "mod" : [4], "weight" : 0.471259 },
		{"id" : 6, "text" : "지인이다.", "head" : -1, "label" : "VNP", "mod" : [0, 5], "weight" : 0.0822342 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "CV_OCCUPATION", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 0, "ne_id" : 0, "text" : "한승원(1958년", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "한승원(1958년", "weight" : 0.004 },
		{"id" : 2, "sent_id" : 0, "start_eid" : 6, "end_eid" : 6, "ne_id" : 3, "text" : "만화가이다.", "start_eid_short" : 6, "end_eid_short" : 6, "text_short" : "만화가이다.", "weight" : 0.009 }
	] },
	{"id" : 1, "type" : "FD_ART", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 6, "sent_id" : 2, "start_eid" : 1, "end_eid" : 1, "ne_id" : 1, "text" : "만화", "start_eid_short" : 1, "end_eid_short" : 1, "text_short" : "만화", "weight" : 0.01 },
		{"id" : 11, "sent_id" : 4, "start_eid" : 0, "end_eid" : 0, "ne_id" : 0, "text" : "만화", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "만화", "weight" : 0.016 }
	] },
	{"id" : 2, "type" : "CV_RELATION", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 7, "sent_id" : 2, "start_eid" : 2, "end_eid" : 2, "ne_id" : 2, "text" : "김동화이다.", "start_eid_short" : 2, "end_eid_short" : 2, "text_short" : "김동화이다.", "weight" : 0.004 },
		{"id" : 8, "sent_id" : 3, "start_eid" : 0, "end_eid" : 0, "ne_id" : 0, "text" : "김동화", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "김동화", "weight" : 0.003 },
		{"id" : 5, "sent_id" : 2, "start_eid" : 0, "end_eid" : 0, "ne_id" : 0, "text" : "남편", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "남편", "weight" : 0.009 }
	] }
 ]
}

