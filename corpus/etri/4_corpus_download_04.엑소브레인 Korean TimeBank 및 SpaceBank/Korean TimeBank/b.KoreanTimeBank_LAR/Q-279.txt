{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿이것은 숲의 식물들이 만들어내는 살균성을 가진 모든 물질을 통칭하며 1937년 러시아의 생화학자 토킨이 제안한 용어이다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "이것", "type" : "NP", "position" : 3, "weight" : 0.0020857 },
		{"id" : 2, "lemma" : "은", "type" : "JX", "position" : 9, "weight" : 0.191811 },
		{"id" : 3, "lemma" : "숲", "type" : "NNG", "position" : 13, "weight" : 0.9 },
		{"id" : 4, "lemma" : "의", "type" : "JKG", "position" : 16, "weight" : 0.0694213 },
		{"id" : 5, "lemma" : "식물", "type" : "NNG", "position" : 20, "weight" : 0.9 },
		{"id" : 6, "lemma" : "들", "type" : "XSN", "position" : 26, "weight" : 0.0299841 },
		{"id" : 7, "lemma" : "이", "type" : "JKS", "position" : 29, "weight" : 0.0731805 },
		{"id" : 8, "lemma" : "만들", "type" : "VV", "position" : 33, "weight" : 0.9 },
		{"id" : 9, "lemma" : "어", "type" : "EC", "position" : 39, "weight" : 0.41831 },
		{"id" : 10, "lemma" : "내", "type" : "VX", "position" : 42, "weight" : 0.0587845 },
		{"id" : 11, "lemma" : "는", "type" : "ETM", "position" : 45, "weight" : 0.183966 },
		{"id" : 12, "lemma" : "살균", "type" : "NNG", "position" : 49, "weight" : 0.9 },
		{"id" : 13, "lemma" : "성", "type" : "XSN", "position" : 55, "weight" : 0.0161708 },
		{"id" : 14, "lemma" : "을", "type" : "JKO", "position" : 58, "weight" : 0.0819193 },
		{"id" : 15, "lemma" : "가지", "type" : "VV", "position" : 62, "weight" : 0.401357 },
		{"id" : 16, "lemma" : "ㄴ", "type" : "ETM", "position" : 65, "weight" : 0.304215 },
		{"id" : 17, "lemma" : "모든", "type" : "MM", "position" : 69, "weight" : 0.9 },
		{"id" : 18, "lemma" : "물질", "type" : "NNG", "position" : 76, "weight" : 0.9 },
		{"id" : 19, "lemma" : "을", "type" : "JKO", "position" : 82, "weight" : 0.129611 },
		{"id" : 20, "lemma" : "통칭", "type" : "NNG", "position" : 86, "weight" : 0.9 },
		{"id" : 21, "lemma" : "하", "type" : "XSV", "position" : 92, "weight" : 0.0001 },
		{"id" : 22, "lemma" : "며", "type" : "EC", "position" : 95, "weight" : 0.371165 },
		{"id" : 23, "lemma" : "1937", "type" : "SN", "position" : 99, "weight" : 1 },
		{"id" : 24, "lemma" : "년", "type" : "NNB", "position" : 103, "weight" : 0.414343 },
		{"id" : 25, "lemma" : "러시아", "type" : "NNP", "position" : 107, "weight" : 0.9 },
		{"id" : 26, "lemma" : "의", "type" : "JKG", "position" : 116, "weight" : 0.0987295 },
		{"id" : 27, "lemma" : "생", "type" : "NNG", "position" : 120, "weight" : 0.28653 },
		{"id" : 28, "lemma" : "화학", "type" : "NNG", "position" : 123, "weight" : 0.9 },
		{"id" : 29, "lemma" : "자", "type" : "XSN", "position" : 129, "weight" : 0.00398741 },
		{"id" : 30, "lemma" : "토킨", "type" : "NNP", "position" : 133, "weight" : 0.6 },
		{"id" : 31, "lemma" : "이", "type" : "JKS", "position" : 139, "weight" : 0.0234517 },
		{"id" : 32, "lemma" : "제안", "type" : "NNG", "position" : 143, "weight" : 0.208212 },
		{"id" : 33, "lemma" : "하", "type" : "XSV", "position" : 149, "weight" : 0.0001 },
		{"id" : 34, "lemma" : "ㄴ", "type" : "ETM", "position" : 149, "weight" : 0.392321 },
		{"id" : 35, "lemma" : "용어", "type" : "NNG", "position" : 153, "weight" : 0.9 },
		{"id" : 36, "lemma" : "이", "type" : "VCP", "position" : 159, "weight" : 0.0177525 },
		{"id" : 37, "lemma" : "다", "type" : "EF", "position" : 162, "weight" : 0.353579 },
		{"id" : 38, "lemma" : ".", "type" : "SF", "position" : 165, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿/NNG+이것/NP+은/JX", "target" : "﻿이것은", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "숲/NNG+의/JKG", "target" : "숲의", "word_id" : 1, "m_begin" : 3, "m_end" : 4},
		{"id" : 2, "result" : "식물들/NNG+이/JKS", "target" : "식물들이", "word_id" : 2, "m_begin" : 5, "m_end" : 7},
		{"id" : 3, "result" : "만들/VV+어/EC+내/VX+는/ETM", "target" : "만들어내는", "word_id" : 3, "m_begin" : 8, "m_end" : 11},
		{"id" : 4, "result" : "살균성/NNG+을/JKO", "target" : "살균성을", "word_id" : 4, "m_begin" : 12, "m_end" : 14},
		{"id" : 5, "result" : "가지/VV+ㄴ/ETM", "target" : "가진", "word_id" : 5, "m_begin" : 15, "m_end" : 16},
		{"id" : 6, "result" : "모든/MM", "target" : "모든", "word_id" : 6, "m_begin" : 17, "m_end" : 17},
		{"id" : 7, "result" : "물질/NNG+을/JKO", "target" : "물질을", "word_id" : 7, "m_begin" : 18, "m_end" : 19},
		{"id" : 8, "result" : "통칭하/VV+며/EC", "target" : "통칭하며", "word_id" : 8, "m_begin" : 20, "m_end" : 22},
		{"id" : 9, "result" : "1937/SN+년/NNB", "target" : "1937년", "word_id" : 9, "m_begin" : 23, "m_end" : 24},
		{"id" : 10, "result" : "러시아/NNG+의/JKG", "target" : "러시아의", "word_id" : 10, "m_begin" : 25, "m_end" : 26},
		{"id" : 11, "result" : "생화학자/NNG", "target" : "생화학자", "word_id" : 11, "m_begin" : 27, "m_end" : 29},
		{"id" : 12, "result" : "토킨/NNG+이/JKS", "target" : "토킨이", "word_id" : 12, "m_begin" : 30, "m_end" : 31},
		{"id" : 13, "result" : "제안하/VV+ㄴ/ETM", "target" : "제안한", "word_id" : 13, "m_begin" : 32, "m_end" : 34},
		{"id" : 14, "result" : "용어/NNG+이/VCP+다/EF+./SF", "target" : "용어이다.", "word_id" : 14, "m_begin" : 35, "m_end" : 38}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "숲", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 13, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 16, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "식물", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 20, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "들", "type" : "XSN", "scode" : "09", "weight" : 1, "position" : 26, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 29, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "만들", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 33, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 39, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "내", "type" : "VX", "scode" : "02", "weight" : 1, "position" : 42, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 45, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "살균성", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 49, "begin" : 12, "end" : 13},
		{"id" : 13, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 58, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "가지", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 62, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 65, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "모든", "type" : "MM", "scode" : "00", "weight" : 1, "position" : 69, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "물질", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 76, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 82, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "통칭하", "type" : "VV", "scode" : "02", "weight" : 1, "position" : 86, "begin" : 20, "end" : 21},
		{"id" : 20, "text" : "며", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 95, "begin" : 22, "end" : 22},
		{"id" : 21, "text" : "1937", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 99, "begin" : 23, "end" : 23},
		{"id" : 22, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 103, "begin" : 24, "end" : 24},
		{"id" : 23, "text" : "러시아", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 107, "begin" : 25, "end" : 25},
		{"id" : 24, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 116, "begin" : 26, "end" : 26},
		{"id" : 25, "text" : "생", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 120, "begin" : 27, "end" : 27},
		{"id" : 26, "text" : "화학자", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 123, "begin" : 28, "end" : 29},
		{"id" : 27, "text" : "토킨", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 133, "begin" : 30, "end" : 30},
		{"id" : 28, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 139, "begin" : 31, "end" : 31},
		{"id" : 29, "text" : "제안하", "type" : "VV", "scode" : "02", "weight" : 1, "position" : 143, "begin" : 32, "end" : 33},
		{"id" : 30, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 149, "begin" : 34, "end" : 34},
		{"id" : 31, "text" : "용어", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 153, "begin" : 35, "end" : 35},
		{"id" : 32, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 159, "begin" : 36, "end" : 36},
		{"id" : 33, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 162, "begin" : 37, "end" : 37},
		{"id" : 34, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 165, "begin" : 38, "end" : 38}
	],
	"word" : [
		{"id" : 0, "text" : "﻿이것은", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "숲의", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 2, "text" : "식물들이", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 3, "text" : "만들어내는", "type" : "", "begin" : 8, "end" : 11},
		{"id" : 4, "text" : "살균성을", "type" : "", "begin" : 12, "end" : 14},
		{"id" : 5, "text" : "가진", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 6, "text" : "모든", "type" : "", "begin" : 17, "end" : 17},
		{"id" : 7, "text" : "물질을", "type" : "", "begin" : 18, "end" : 19},
		{"id" : 8, "text" : "통칭하며", "type" : "", "begin" : 20, "end" : 22},
		{"id" : 9, "text" : "1937년", "type" : "", "begin" : 23, "end" : 24},
		{"id" : 10, "text" : "러시아의", "type" : "", "begin" : 25, "end" : 26},
		{"id" : 11, "text" : "생화학자", "type" : "", "begin" : 27, "end" : 29},
		{"id" : 12, "text" : "토킨이", "type" : "", "begin" : 30, "end" : 31},
		{"id" : 13, "text" : "제안한", "type" : "", "begin" : 32, "end" : 34},
		{"id" : 14, "text" : "용어이다.", "type" : "", "begin" : 35, "end" : 38}
	],
	"NE" : [
		{"id" : 0, "text" : "1937년", "type" : "DT_YEAR", "begin" : 23, "end" : 24, "weight" : 0.627917, "common_noun" : 0},
		{"id" : 1, "text" : "러시아", "type" : "LCP_COUNTRY", "begin" : 25, "end" : 25, "weight" : 0.559835, "common_noun" : 0},
		{"id" : 2, "text" : "생화학자", "type" : "CV_OCCUPATION", "begin" : 27, "end" : 29, "weight" : 0.522491, "common_noun" : 0},
		{"id" : 3, "text" : "토킨", "type" : "PS_NAME", "begin" : 30, "end" : 30, "weight" : 0.21531, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿이것은", "head" : 8, "label" : "NP_SBJ", "mod" : [], "weight" : 0.719346 },
		{"id" : 1, "text" : "숲의", "head" : 2, "label" : "NP_MOD", "mod" : [], "weight" : 0.741678 },
		{"id" : 2, "text" : "식물들이", "head" : 3, "label" : "NP_SBJ", "mod" : [1], "weight" : 0.921568 },
		{"id" : 3, "text" : "만들어내는", "head" : 7, "label" : "VP_MOD", "mod" : [2], "weight" : 0.681961 },
		{"id" : 4, "text" : "살균성을", "head" : 5, "label" : "NP_OBJ", "mod" : [], "weight" : 0.775355 },
		{"id" : 5, "text" : "가진", "head" : 7, "label" : "VP_MOD", "mod" : [4], "weight" : 0.731339 },
		{"id" : 6, "text" : "모든", "head" : 7, "label" : "DP", "mod" : [], "weight" : 0.809383 },
		{"id" : 7, "text" : "물질을", "head" : 8, "label" : "NP_OBJ", "mod" : [3, 5, 6], "weight" : 0.914208 },
		{"id" : 8, "text" : "통칭하며", "head" : 13, "label" : "VP", "mod" : [0, 7], "weight" : 0.915012 },
		{"id" : 9, "text" : "1937년", "head" : 13, "label" : "NP_AJT", "mod" : [], "weight" : 0.653703 },
		{"id" : 10, "text" : "러시아의", "head" : 11, "label" : "NP_MOD", "mod" : [], "weight" : 0.699593 },
		{"id" : 11, "text" : "생화학자", "head" : 12, "label" : "NP", "mod" : [10], "weight" : 0.772294 },
		{"id" : 12, "text" : "토킨이", "head" : 13, "label" : "NP_SBJ", "mod" : [11], "weight" : 0.612731 },
		{"id" : 13, "text" : "제안한", "head" : 14, "label" : "VP_MOD", "mod" : [8, 9, 12], "weight" : 0.544837 },
		{"id" : 14, "text" : "용어이다.", "head" : -1, "label" : "VNP", "mod" : [13], "weight" : 0.00914868 }
	],
	"SRL" : [
		{"verb" : "만들", "sense" : 1, "word_id" : 3, "weight" : 0.22691,
			"argument" : [
				{"type" : "ARG0", "word_id" : 2, "text" : "식물들이", "weight" : 0.271116 },
				{"type" : "ARG1", "word_id" : 7, "text" : "물질을", "weight" : 0.182704 }
			] },
		{"verb" : "가지", "sense" : 1, "word_id" : 5, "weight" : 0.266051,
			"argument" : [
				{"type" : "ARG1", "word_id" : 4, "text" : "살균성을", "weight" : 0.234104 },
				{"type" : "ARGM-EXT", "word_id" : 6, "text" : "모든", "weight" : 0.27967 },
				{"type" : "ARG0", "word_id" : 7, "text" : "물질을", "weight" : 0.284379 }
			] },
		{"verb" : "통칭", "sense" : 1, "word_id" : 8, "weight" : 0.404396,
			"argument" : [
				{"type" : "ARG1", "word_id" : 7, "text" : "물질을", "weight" : 0.404396 }
			] },
		{"verb" : "제안", "sense" : 1, "word_id" : 13, "weight" : 0.405495,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 9, "text" : "1937년", "weight" : 0.375134 },
				{"type" : "ARG0", "word_id" : 12, "text" : "토킨이", "weight" : 0.435856 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "이것은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "이것", "type" : "NP", "position" : 167, "weight" : 0.0733657 },
		{"id" : 1, "lemma" : "은", "type" : "JX", "position" : 173, "weight" : 0.191811 },
		{"id" : 2, "lemma" : "무엇", "type" : "NP", "position" : 177, "weight" : 0.9 },
		{"id" : 3, "lemma" : "이", "type" : "VCP", "position" : 183, "weight" : 0.0175768 },
		{"id" : 4, "lemma" : "ㄹ까", "type" : "EF", "position" : 183, "weight" : 0.258243 },
		{"id" : 5, "lemma" : "?", "type" : "SF", "position" : 189, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이것/NP+은/JX", "target" : "이것은", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 1, "m_begin" : 2, "m_end" : 5}
	],
	"WSD" : [
		{"id" : 0, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 167, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 173, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 177, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 183, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 183, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 189, "begin" : 5, "end" : 5}
	],
	"word" : [
		{"id" : 0, "text" : "이것은", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "무엇일까?", "type" : "", "begin" : 2, "end" : 5}
	],
	"NE" : [
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이것은", "head" : 1, "label" : "NP_SBJ", "mod" : [], "weight" : 0.769102 },
		{"id" : 1, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [0], "weight" : 0.556699 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "CV_OCCUPATION", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "러시아", 
	 "mention" : [
		{"id" : 7, "sent_id" : 0, "start_eid" : 10, "end_eid" : 12, "ne_id" : 3, "text" : "러시아의 생화학자 토킨", "start_eid_short" : 10, "end_eid_short" : 12, "text_short" : "러시아의 생화학자 토킨", "weight" : 0.01 },
		{"id" : 8, "sent_id" : 0, "start_eid" : 10, "end_eid" : 11, "ne_id" : 2, "text" : "러시아의 생화학자", "start_eid_short" : 10, "end_eid_short" : 11, "text_short" : "러시아의 생화학자", "weight" : 0.015 }
	] }
 ]
}

