{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "김미영은 대한민국의 만화가이다.",
	"morp" : [
		{"id" : 0, "lemma" : "김미영", "type" : "NNP", "position" : 0, "weight" : 0.1 },
		{"id" : 1, "lemma" : "은", "type" : "JX", "position" : 9, "weight" : 0.0520511 },
		{"id" : 2, "lemma" : "대한민국", "type" : "NNP", "position" : 13, "weight" : 0.0447775 },
		{"id" : 3, "lemma" : "의", "type" : "JKG", "position" : 25, "weight" : 0.0987295 },
		{"id" : 4, "lemma" : "만화", "type" : "NNG", "position" : 29, "weight" : 0.83848 },
		{"id" : 5, "lemma" : "가", "type" : "XSN", "position" : 35, "weight" : 0.000115417 },
		{"id" : 6, "lemma" : "이", "type" : "VCP", "position" : 38, "weight" : 0.0165001 },
		{"id" : 7, "lemma" : "다", "type" : "EF", "position" : 41, "weight" : 0.353579 },
		{"id" : 8, "lemma" : ".", "type" : "SF", "position" : 44, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "김미영/NNG+은/JX", "target" : "김미영은", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "대한민국/NNG+의/JKG", "target" : "대한민국의", "word_id" : 1, "m_begin" : 2, "m_end" : 3},
		{"id" : 2, "result" : "만화가/NNG+이/VCP+다/EF+./SF", "target" : "만화가이다.", "word_id" : 2, "m_begin" : 4, "m_end" : 8}
	],
	"WSD" : [
		{"id" : 0, "text" : "김미영", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "대한민국", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 13, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 25, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "만화가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 29, "begin" : 4, "end" : 5},
		{"id" : 5, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 38, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 41, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 44, "begin" : 8, "end" : 8}
	],
	"word" : [
		{"id" : 0, "text" : "김미영은", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "대한민국의", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 2, "text" : "만화가이다.", "type" : "", "begin" : 4, "end" : 8}
	],
	"NE" : [
		{"id" : 0, "text" : "김미영", "type" : "PS_NAME", "begin" : 0, "end" : 0, "weight" : 0.833435, "common_noun" : 0},
		{"id" : 1, "text" : "대한민국", "type" : "LCP_COUNTRY", "begin" : 2, "end" : 2, "weight" : 0.146032, "common_noun" : 0},
		{"id" : 2, "text" : "만화가", "type" : "CV_OCCUPATION", "begin" : 4, "end" : 5, "weight" : 0.286001, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "김미영은", "head" : 2, "label" : "NP_SBJ", "mod" : [], "weight" : 0.730392 },
		{"id" : 1, "text" : "대한민국의", "head" : 2, "label" : "NP_MOD", "mod" : [], "weight" : 0.470915 },
		{"id" : 2, "text" : "만화가이다.", "head" : -1, "label" : "VNP", "mod" : [0, 1], "weight" : 0.196267 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "공주전문대 만화학과를 졸업하였고 만화동호회 '꿈꾸는 고치(COCOON)' 출신이며 1998년 만화잡지 윙크에서 단편 〈무지로소이다〉로 데뷔하였다.",
	"morp" : [
		{"id" : 0, "lemma" : "공주", "type" : "NNP", "position" : 45, "weight" : 0.0260628 },
		{"id" : 1, "lemma" : "전문", "type" : "NNG", "position" : 51, "weight" : 0.281565 },
		{"id" : 2, "lemma" : "대", "type" : "NNG", "position" : 57, "weight" : 0.0277971 },
		{"id" : 3, "lemma" : "만화학", "type" : "NNP", "position" : 61, "weight" : 0.6 },
		{"id" : 4, "lemma" : "과", "type" : "NNG", "position" : 70, "weight" : 0.0056088 },
		{"id" : 5, "lemma" : "를", "type" : "JKO", "position" : 73, "weight" : 0.137686 },
		{"id" : 6, "lemma" : "졸업", "type" : "NNG", "position" : 77, "weight" : 0.101254 },
		{"id" : 7, "lemma" : "하", "type" : "XSV", "position" : 83, "weight" : 0.0001 },
		{"id" : 8, "lemma" : "었", "type" : "EP", "position" : 86, "weight" : 0.9 },
		{"id" : 9, "lemma" : "고", "type" : "EC", "position" : 89, "weight" : 0.190901 },
		{"id" : 10, "lemma" : "만화", "type" : "NNG", "position" : 93, "weight" : 0.211124 },
		{"id" : 11, "lemma" : "동호", "type" : "NNG", "position" : 99, "weight" : 0.0193437 },
		{"id" : 12, "lemma" : "회", "type" : "XSN", "position" : 105, "weight" : 0.010766 },
		{"id" : 13, "lemma" : "'", "type" : "SS", "position" : 109, "weight" : 1 },
		{"id" : 14, "lemma" : "꿈꾸", "type" : "VV", "position" : 110, "weight" : 0.9 },
		{"id" : 15, "lemma" : "는", "type" : "ETM", "position" : 116, "weight" : 0.184941 },
		{"id" : 16, "lemma" : "고치", "type" : "NNG", "position" : 120, "weight" : 0.0454025 },
		{"id" : 17, "lemma" : "(", "type" : "SS", "position" : 126, "weight" : 1 },
		{"id" : 18, "lemma" : "COCOON", "type" : "SL", "position" : 127, "weight" : 1 },
		{"id" : 19, "lemma" : ")", "type" : "SS", "position" : 133, "weight" : 1 },
		{"id" : 20, "lemma" : "'", "type" : "SS", "position" : 134, "weight" : 1 },
		{"id" : 21, "lemma" : "출신", "type" : "NNG", "position" : 136, "weight" : 0.9 },
		{"id" : 22, "lemma" : "이", "type" : "VCP", "position" : 142, "weight" : 0.0177525 },
		{"id" : 23, "lemma" : "며", "type" : "EC", "position" : 145, "weight" : 0.250555 },
		{"id" : 24, "lemma" : "1998", "type" : "SN", "position" : 149, "weight" : 1 },
		{"id" : 25, "lemma" : "년", "type" : "NNB", "position" : 153, "weight" : 0.414343 },
		{"id" : 26, "lemma" : "만화", "type" : "NNG", "position" : 157, "weight" : 0.106433 },
		{"id" : 27, "lemma" : "잡지", "type" : "NNG", "position" : 163, "weight" : 0.9 },
		{"id" : 28, "lemma" : "윙크", "type" : "NNG", "position" : 170, "weight" : 0.9 },
		{"id" : 29, "lemma" : "에서", "type" : "JKB", "position" : 176, "weight" : 0.153407 },
		{"id" : 30, "lemma" : "단편", "type" : "NNG", "position" : 183, "weight" : 0.9 },
		{"id" : 31, "lemma" : "〈", "type" : "SS", "position" : 190, "weight" : 1 },
		{"id" : 32, "lemma" : "무지로소", "type" : "NNP", "position" : 193, "weight" : 0.6 },
		{"id" : 33, "lemma" : "이", "type" : "VCP", "position" : 205, "weight" : 0.00359898 },
		{"id" : 34, "lemma" : "다", "type" : "EF", "position" : 208, "weight" : 0.353579 },
		{"id" : 35, "lemma" : "〉", "type" : "SS", "position" : 211, "weight" : 1 },
		{"id" : 36, "lemma" : "로", "type" : "JKB", "position" : 214, "weight" : 0.0485504 },
		{"id" : 37, "lemma" : "데뷔", "type" : "NNG", "position" : 218, "weight" : 0.9 },
		{"id" : 38, "lemma" : "하", "type" : "XSV", "position" : 224, "weight" : 0.0001 },
		{"id" : 39, "lemma" : "었", "type" : "EP", "position" : 227, "weight" : 0.9 },
		{"id" : 40, "lemma" : "다", "type" : "EF", "position" : 230, "weight" : 0.640954 },
		{"id" : 41, "lemma" : ".", "type" : "SF", "position" : 233, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "공주전문대/NNG", "target" : "공주전문대", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "만화학과/NNG+를/JKO", "target" : "만화학과를", "word_id" : 1, "m_begin" : 3, "m_end" : 5},
		{"id" : 2, "result" : "졸업하/VV+었/EP+고/EC", "target" : "졸업하였고", "word_id" : 2, "m_begin" : 6, "m_end" : 9},
		{"id" : 3, "result" : "만화동호회/NNG", "target" : "만화동호회", "word_id" : 3, "m_begin" : 10, "m_end" : 12},
		{"id" : 4, "result" : "'/SS+꿈꾸/VV+는/ETM", "target" : "'꿈꾸는", "word_id" : 4, "m_begin" : 13, "m_end" : 15},
		{"id" : 5, "result" : "고치/NNG+(/SS+COCOON/SL+)/SS+'/SS", "target" : "고치(COCOON)'", "word_id" : 5, "m_begin" : 16, "m_end" : 20},
		{"id" : 6, "result" : "출신/NNG+이/VCP+며/EC", "target" : "출신이며", "word_id" : 6, "m_begin" : 21, "m_end" : 23},
		{"id" : 7, "result" : "1998/SN+년/NNB", "target" : "1998년", "word_id" : 7, "m_begin" : 24, "m_end" : 25},
		{"id" : 8, "result" : "만화잡지/NNG", "target" : "만화잡지", "word_id" : 8, "m_begin" : 26, "m_end" : 27},
		{"id" : 9, "result" : "윙크/NNG+에서/JKB", "target" : "윙크에서", "word_id" : 9, "m_begin" : 28, "m_end" : 29},
		{"id" : 10, "result" : "단편/NNG", "target" : "단편", "word_id" : 10, "m_begin" : 30, "m_end" : 30},
		{"id" : 11, "result" : "〈/SS+무지로소/NNG+이/VCP+다/EF+〉/SS+로/JKB", "target" : "〈무지로소이다〉로", "word_id" : 11, "m_begin" : 31, "m_end" : 36},
		{"id" : 12, "result" : "데뷔하/VV+었/EP+다/EF+./SF", "target" : "데뷔하였다.", "word_id" : 12, "m_begin" : 37, "m_end" : 41}
	],
	"WSD" : [
		{"id" : 0, "text" : "공주", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 45, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "전문대", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 51, "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "만화학", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 61, "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "과", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 70, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 73, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "졸업하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 77, "begin" : 6, "end" : 7},
		{"id" : 6, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 86, "begin" : 8, "end" : 8},
		{"id" : 7, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 89, "begin" : 9, "end" : 9},
		{"id" : 8, "text" : "만화", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 93, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "동호회", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 99, "begin" : 11, "end" : 12},
		{"id" : 10, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 109, "begin" : 13, "end" : 13},
		{"id" : 11, "text" : "꿈꾸", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 110, "begin" : 14, "end" : 14},
		{"id" : 12, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 116, "begin" : 15, "end" : 15},
		{"id" : 13, "text" : "고치", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 120, "begin" : 16, "end" : 16},
		{"id" : 14, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 126, "begin" : 17, "end" : 17},
		{"id" : 15, "text" : "COCOON", "type" : "SL", "scode" : "00", "weight" : 1, "position" : 127, "begin" : 18, "end" : 18},
		{"id" : 16, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 133, "begin" : 19, "end" : 19},
		{"id" : 17, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 134, "begin" : 20, "end" : 20},
		{"id" : 18, "text" : "출신", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 136, "begin" : 21, "end" : 21},
		{"id" : 19, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 142, "begin" : 22, "end" : 22},
		{"id" : 20, "text" : "며", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 145, "begin" : 23, "end" : 23},
		{"id" : 21, "text" : "1998", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 149, "begin" : 24, "end" : 24},
		{"id" : 22, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 153, "begin" : 25, "end" : 25},
		{"id" : 23, "text" : "만화", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 157, "begin" : 26, "end" : 26},
		{"id" : 24, "text" : "잡지", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 163, "begin" : 27, "end" : 27},
		{"id" : 25, "text" : "윙크", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 170, "begin" : 28, "end" : 28},
		{"id" : 26, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 176, "begin" : 29, "end" : 29},
		{"id" : 27, "text" : "단편", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 183, "begin" : 30, "end" : 30},
		{"id" : 28, "text" : "〈", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 190, "begin" : 31, "end" : 31},
		{"id" : 29, "text" : "무지로소", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 193, "begin" : 32, "end" : 32},
		{"id" : 30, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 205, "begin" : 33, "end" : 33},
		{"id" : 31, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 208, "begin" : 34, "end" : 34},
		{"id" : 32, "text" : "〉", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 211, "begin" : 35, "end" : 35},
		{"id" : 33, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 214, "begin" : 36, "end" : 36},
		{"id" : 34, "text" : "데뷔하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 218, "begin" : 37, "end" : 38},
		{"id" : 35, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 227, "begin" : 39, "end" : 39},
		{"id" : 36, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 230, "begin" : 40, "end" : 40},
		{"id" : 37, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 233, "begin" : 41, "end" : 41}
	],
	"word" : [
		{"id" : 0, "text" : "공주전문대", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "만화학과를", "type" : "", "begin" : 3, "end" : 5},
		{"id" : 2, "text" : "졸업하였고", "type" : "", "begin" : 6, "end" : 9},
		{"id" : 3, "text" : "만화동호회", "type" : "", "begin" : 10, "end" : 12},
		{"id" : 4, "text" : "'꿈꾸는", "type" : "", "begin" : 13, "end" : 15},
		{"id" : 5, "text" : "고치(COCOON)'", "type" : "", "begin" : 16, "end" : 20},
		{"id" : 6, "text" : "출신이며", "type" : "", "begin" : 21, "end" : 23},
		{"id" : 7, "text" : "1998년", "type" : "", "begin" : 24, "end" : 25},
		{"id" : 8, "text" : "만화잡지", "type" : "", "begin" : 26, "end" : 27},
		{"id" : 9, "text" : "윙크에서", "type" : "", "begin" : 28, "end" : 29},
		{"id" : 10, "text" : "단편", "type" : "", "begin" : 30, "end" : 30},
		{"id" : 11, "text" : "〈무지로소이다〉로", "type" : "", "begin" : 31, "end" : 36},
		{"id" : 12, "text" : "데뷔하였다.", "type" : "", "begin" : 37, "end" : 41}
	],
	"NE" : [
		{"id" : 0, "text" : "공주전문대", "type" : "OGG_EDUCATION", "begin" : 0, "end" : 2, "weight" : 0.519958, "common_noun" : 0},
		{"id" : 1, "text" : "만화학과", "type" : "FD_SCIENCE", "begin" : 3, "end" : 4, "weight" : 0.249381, "common_noun" : 0},
		{"id" : 2, "text" : "만화동호회", "type" : "EV_OTHERS", "begin" : 10, "end" : 12, "weight" : 0.113909, "common_noun" : 0},
		{"id" : 3, "text" : "1998년", "type" : "DT_YEAR", "begin" : 24, "end" : 25, "weight" : 0.779882, "common_noun" : 0},
		{"id" : 4, "text" : "만화", "type" : "FD_ART", "begin" : 26, "end" : 26, "weight" : 0.471702, "common_noun" : 0},
		{"id" : 5, "text" : "무지로소이다", "type" : "AFW_DOCUMENT", "begin" : 32, "end" : 34, "weight" : 0.529028, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "공주전문대", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.853355 },
		{"id" : 1, "text" : "만화학과를", "head" : 2, "label" : "NP_OBJ", "mod" : [0], "weight" : 0.544494 },
		{"id" : 2, "text" : "졸업하였고", "head" : 6, "label" : "VP", "mod" : [1], "weight" : 0.773673 },
		{"id" : 3, "text" : "만화동호회", "head" : 4, "label" : "NP_SBJ", "mod" : [], "weight" : 0.575906 },
		{"id" : 4, "text" : "'꿈꾸는", "head" : 5, "label" : "VP_MOD", "mod" : [3], "weight" : 0.848874 },
		{"id" : 5, "text" : "고치(COCOON)'", "head" : 6, "label" : "NP_PRN", "mod" : [4], "weight" : 0.468369 },
		{"id" : 6, "text" : "출신이며", "head" : 12, "label" : "VNP", "mod" : [2, 5], "weight" : 0.485339 },
		{"id" : 7, "text" : "1998년", "head" : 9, "label" : "NP", "mod" : [], "weight" : 0.623458 },
		{"id" : 8, "text" : "만화잡지", "head" : 9, "label" : "NP", "mod" : [], "weight" : 0.790824 },
		{"id" : 9, "text" : "윙크에서", "head" : 12, "label" : "NP_AJT", "mod" : [7, 8], "weight" : 0.621062 },
		{"id" : 10, "text" : "단편", "head" : 11, "label" : "NP", "mod" : [], "weight" : 0.865816 },
		{"id" : 11, "text" : "〈무지로소이다〉로", "head" : 12, "label" : "NP_AJT", "mod" : [10], "weight" : 0.449938 },
		{"id" : 12, "text" : "데뷔하였다.", "head" : -1, "label" : "VP", "mod" : [6, 9, 11], "weight" : 0.00262625 }
	],
	"SRL" : [
		{"verb" : "졸업", "sense" : 1, "word_id" : 2, "weight" : 0.222298,
			"argument" : [
				{"type" : "ARG1", "word_id" : 1, "text" : "만화학과를", "weight" : 0.222298 }
			] },
		{"verb" : "'", "sense" : 1, "word_id" : 4, "weight" : 0.10424,
			"argument" : [
				{"type" : "ARG1", "word_id" : 5, "text" : "고치(COCOON)'", "weight" : 0.10424 }
			] },
		{"verb" : "데뷔", "sense" : 1, "word_id" : 12, "weight" : 0.0733712,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 9, "text" : "윙크에서", "weight" : 0.0754819 },
				{"type" : "ARG3", "word_id" : 11, "text" : "〈무지로소이다〉로", "weight" : 0.0712604 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 2, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.542475 },
		{"id" : 1, "verb_wid" : 6, "ant_sid" : 0, "ant_wid" : 0, "type" : "s", "istitle" : 0, "weight" : 0.512436 },
		{"id" : 2, "verb_wid" : 12, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.541572 }
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "2010년 현재 《기생충》을 인터넷매체에서 재연재하고 있다.",
	"morp" : [
		{"id" : 0, "lemma" : "2010", "type" : "SN", "position" : 234, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 238, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "현재", "type" : "MAG", "position" : 242, "weight" : 0.00662512 },
		{"id" : 3, "lemma" : "《", "type" : "SS", "position" : 249, "weight" : 1 },
		{"id" : 4, "lemma" : "기생", "type" : "NNG", "position" : 252, "weight" : 0.9 },
		{"id" : 5, "lemma" : "충", "type" : "NNG", "position" : 258, "weight" : 0.9 },
		{"id" : 6, "lemma" : "》", "type" : "SS", "position" : 261, "weight" : 1 },
		{"id" : 7, "lemma" : "을", "type" : "JKO", "position" : 264, "weight" : 0.0336085 },
		{"id" : 8, "lemma" : "인터넷", "type" : "NNG", "position" : 268, "weight" : 0.9 },
		{"id" : 9, "lemma" : "매체", "type" : "NNG", "position" : 277, "weight" : 0.9 },
		{"id" : 10, "lemma" : "에서", "type" : "JKB", "position" : 283, "weight" : 0.153407 },
		{"id" : 11, "lemma" : "재연재하", "type" : "VV", "position" : 290, "weight" : 0 },
		{"id" : 12, "lemma" : "고", "type" : "EC", "position" : 302, "weight" : 0.416679 },
		{"id" : 13, "lemma" : "있", "type" : "VX", "position" : 306, "weight" : 0.125953 },
		{"id" : 14, "lemma" : "다", "type" : "EF", "position" : 309, "weight" : 0.180366 },
		{"id" : 15, "lemma" : ".", "type" : "SF", "position" : 312, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "2010/SN+년/NNB", "target" : "2010년", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "현재/MAG", "target" : "현재", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "《/SS+기생충/NNG+》/SS+을/JKO", "target" : "《기생충》을", "word_id" : 2, "m_begin" : 3, "m_end" : 7},
		{"id" : 3, "result" : "인터넷매체/NNG+에서/JKB", "target" : "인터넷매체에서", "word_id" : 3, "m_begin" : 8, "m_end" : 10},
		{"id" : 4, "result" : "재연재하/VV+고/EC", "target" : "재연재하고", "word_id" : 4, "m_begin" : 11, "m_end" : 12},
		{"id" : 5, "result" : "있/VX+다/EF+./SF", "target" : "있다.", "word_id" : 5, "m_begin" : 13, "m_end" : 15}
	],
	"WSD" : [
		{"id" : 0, "text" : "2010", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 234, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 238, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "현재", "type" : "MAG", "scode" : "02", "weight" : 1, "position" : 242, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "《", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 249, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "기생충", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 252, "begin" : 4, "end" : 5},
		{"id" : 5, "text" : "》", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 261, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 264, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "인터넷", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 268, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "매체", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 277, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 283, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "재연재하", "type" : "VV", "scode" : "00", "weight" : 0, "position" : 290, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 302, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "있", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 306, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 309, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 312, "begin" : 15, "end" : 15}
	],
	"word" : [
		{"id" : 0, "text" : "2010년", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "현재", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "《기생충》을", "type" : "", "begin" : 3, "end" : 7},
		{"id" : 3, "text" : "인터넷매체에서", "type" : "", "begin" : 8, "end" : 10},
		{"id" : 4, "text" : "재연재하고", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 5, "text" : "있다.", "type" : "", "begin" : 13, "end" : 15}
	],
	"NE" : [
		{"id" : 0, "text" : "2010년", "type" : "DT_YEAR", "begin" : 0, "end" : 1, "weight" : 0.750546, "common_noun" : 0},
		{"id" : 1, "text" : "기생충", "type" : "AM_TYPE", "begin" : 4, "end" : 5, "weight" : 0.74663, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "2010년", "head" : 1, "label" : "NP_AJT", "mod" : [], "weight" : 0.599995 },
		{"id" : 1, "text" : "현재", "head" : 4, "label" : "AP", "mod" : [0], "weight" : 0.637623 },
		{"id" : 2, "text" : "《기생충》을", "head" : 4, "label" : "NP_OBJ", "mod" : [], "weight" : 0.653548 },
		{"id" : 3, "text" : "인터넷매체에서", "head" : 4, "label" : "NP_AJT", "mod" : [], "weight" : 0.692583 },
		{"id" : 4, "text" : "재연재하고", "head" : 5, "label" : "VP", "mod" : [1, 2, 3], "weight" : 0.527269 },
		{"id" : 5, "text" : "있다.", "head" : -1, "label" : "VP", "mod" : [4], "weight" : 0.0710294 }
	],
	"SRL" : [
		{"verb" : "재연재하", "sense" : 1, "word_id" : 4, "weight" : 0.278451,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 1, "text" : "현재", "weight" : 0.226816 },
				{"type" : "ARG1", "word_id" : 2, "text" : "《기생충》을", "weight" : 0.232263 },
				{"type" : "ARGM-LOC", "word_id" : 3, "text" : "인터넷매체에서", "weight" : 0.143576 },
				{"type" : "AUX", "word_id" : 5, "text" : "있다.", "weight" : 0.511148 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 4, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.445193 }
	]
	}
 ],
 "entity" : [
 ]
}

