{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿김정희는 날카로운 감식안과 열정적인 예술혼으로 수많은 후학을 길러낸 조선후기 최고의 예술인이다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "SW", "position" : 0, "weight" : 1 },
		{"id" : 1, "lemma" : "김정희", "type" : "NNP", "position" : 3, "weight" : 0.9 },
		{"id" : 2, "lemma" : "는", "type" : "JX", "position" : 12, "weight" : 0.0332677 },
		{"id" : 3, "lemma" : "날카롭", "type" : "VA", "position" : 16, "weight" : 0.9 },
		{"id" : 4, "lemma" : "ㄴ", "type" : "ETM", "position" : 22, "weight" : 0.430446 },
		{"id" : 5, "lemma" : "감식", "type" : "NNG", "position" : 29, "weight" : 0.9 },
		{"id" : 6, "lemma" : "안", "type" : "NNG", "position" : 35, "weight" : 0.0903732 },
		{"id" : 7, "lemma" : "과", "type" : "JC", "position" : 38, "weight" : 0.017569 },
		{"id" : 8, "lemma" : "열정", "type" : "NNG", "position" : 42, "weight" : 0.9 },
		{"id" : 9, "lemma" : "적", "type" : "XSN", "position" : 48, "weight" : 0.0168756 },
		{"id" : 10, "lemma" : "이", "type" : "VCP", "position" : 51, "weight" : 0.0165001 },
		{"id" : 11, "lemma" : "ㄴ", "type" : "ETM", "position" : 51, "weight" : 0.220712 },
		{"id" : 12, "lemma" : "예술", "type" : "NNG", "position" : 55, "weight" : 0.9 },
		{"id" : 13, "lemma" : "혼", "type" : "NNG", "position" : 61, "weight" : 0.183478 },
		{"id" : 14, "lemma" : "으로", "type" : "JKB", "position" : 64, "weight" : 0.153406 },
		{"id" : 15, "lemma" : "수많", "type" : "VA", "position" : 71, "weight" : 0.9 },
		{"id" : 16, "lemma" : "은", "type" : "ETM", "position" : 77, "weight" : 0.155123 },
		{"id" : 17, "lemma" : "후학", "type" : "NNG", "position" : 81, "weight" : 0.9 },
		{"id" : 18, "lemma" : "을", "type" : "JKO", "position" : 87, "weight" : 0.129611 },
		{"id" : 19, "lemma" : "기르", "type" : "VV", "position" : 91, "weight" : 0.778275 },
		{"id" : 20, "lemma" : "어", "type" : "EC", "position" : 91, "weight" : 0.41831 },
		{"id" : 21, "lemma" : "내", "type" : "VX", "position" : 91, "weight" : 0.0587845 },
		{"id" : 22, "lemma" : "ㄴ", "type" : "ETM", "position" : 91, "weight" : 0.302612 },
		{"id" : 23, "lemma" : "조선", "type" : "NNP", "position" : 101, "weight" : 0.027614 },
		{"id" : 24, "lemma" : "후기", "type" : "NNG", "position" : 107, "weight" : 0.9 },
		{"id" : 25, "lemma" : "최고", "type" : "NNG", "position" : 114, "weight" : 0.9 },
		{"id" : 26, "lemma" : "의", "type" : "JKG", "position" : 120, "weight" : 0.0694213 },
		{"id" : 27, "lemma" : "예술", "type" : "NNG", "position" : 124, "weight" : 0.9 },
		{"id" : 28, "lemma" : "인", "type" : "XSN", "position" : 130, "weight" : 0.0198339 },
		{"id" : 29, "lemma" : "이", "type" : "VCP", "position" : 133, "weight" : 0.0165001 },
		{"id" : 30, "lemma" : "다", "type" : "EF", "position" : 136, "weight" : 0.353579 },
		{"id" : 31, "lemma" : ".", "type" : "SF", "position" : 139, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿/SW+김정희/NNG+는/JX", "target" : "﻿김정희는", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "날카롭/VA+ㄴ/ETM", "target" : "날카로운", "word_id" : 1, "m_begin" : 3, "m_end" : 4},
		{"id" : 2, "result" : "감식안/NNG+과/JC", "target" : "감식안과", "word_id" : 2, "m_begin" : 5, "m_end" : 7},
		{"id" : 3, "result" : "열정적/NNG+이/VCP+ㄴ/ETM", "target" : "열정적인", "word_id" : 3, "m_begin" : 8, "m_end" : 11},
		{"id" : 4, "result" : "예술혼/NNG+으로/JKB", "target" : "예술혼으로", "word_id" : 4, "m_begin" : 12, "m_end" : 14},
		{"id" : 5, "result" : "수많/VA+은/ETM", "target" : "수많은", "word_id" : 5, "m_begin" : 15, "m_end" : 16},
		{"id" : 6, "result" : "후학/NNG+을/JKO", "target" : "후학을", "word_id" : 6, "m_begin" : 17, "m_end" : 18},
		{"id" : 7, "result" : "기르/VV+어/EC+내/VX+ㄴ/ETM", "target" : "길러낸", "word_id" : 7, "m_begin" : 19, "m_end" : 22},
		{"id" : 8, "result" : "조선후기/NNG", "target" : "조선후기", "word_id" : 8, "m_begin" : 23, "m_end" : 24},
		{"id" : 9, "result" : "최고/NNG+의/JKG", "target" : "최고의", "word_id" : 9, "m_begin" : 25, "m_end" : 26},
		{"id" : 10, "result" : "예술인/NNG+이/VCP+다/EF+./SF", "target" : "예술인이다.", "word_id" : 10, "m_begin" : 27, "m_end" : 31}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "SW", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "김정희", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 12, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "날카롭", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 16, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 22, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "감식안", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 29, "begin" : 5, "end" : 6},
		{"id" : 6, "text" : "과", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 38, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "열정적", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 42, "begin" : 8, "end" : 9},
		{"id" : 8, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 51, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 51, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "예술혼", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 55, "begin" : 12, "end" : 13},
		{"id" : 11, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 64, "begin" : 14, "end" : 14},
		{"id" : 12, "text" : "수많", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 71, "begin" : 15, "end" : 15},
		{"id" : 13, "text" : "은", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 77, "begin" : 16, "end" : 16},
		{"id" : 14, "text" : "후학", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 81, "begin" : 17, "end" : 17},
		{"id" : 15, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 87, "begin" : 18, "end" : 18},
		{"id" : 16, "text" : "기르", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 91, "begin" : 19, "end" : 19},
		{"id" : 17, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 91, "begin" : 20, "end" : 20},
		{"id" : 18, "text" : "내", "type" : "VX", "scode" : "02", "weight" : 1, "position" : 91, "begin" : 21, "end" : 21},
		{"id" : 19, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 91, "begin" : 22, "end" : 22},
		{"id" : 20, "text" : "조선", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 101, "begin" : 23, "end" : 23},
		{"id" : 21, "text" : "후기", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 107, "begin" : 24, "end" : 24},
		{"id" : 22, "text" : "최고", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 114, "begin" : 25, "end" : 25},
		{"id" : 23, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 120, "begin" : 26, "end" : 26},
		{"id" : 24, "text" : "예술인", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 124, "begin" : 27, "end" : 28},
		{"id" : 25, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 133, "begin" : 29, "end" : 29},
		{"id" : 26, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 136, "begin" : 30, "end" : 30},
		{"id" : 27, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 139, "begin" : 31, "end" : 31}
	],
	"word" : [
		{"id" : 0, "text" : "﻿김정희는", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "날카로운", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 2, "text" : "감식안과", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 3, "text" : "열정적인", "type" : "", "begin" : 8, "end" : 11},
		{"id" : 4, "text" : "예술혼으로", "type" : "", "begin" : 12, "end" : 14},
		{"id" : 5, "text" : "수많은", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 6, "text" : "후학을", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 7, "text" : "길러낸", "type" : "", "begin" : 19, "end" : 22},
		{"id" : 8, "text" : "조선후기", "type" : "", "begin" : 23, "end" : 24},
		{"id" : 9, "text" : "최고의", "type" : "", "begin" : 25, "end" : 26},
		{"id" : 10, "text" : "예술인이다.", "type" : "", "begin" : 27, "end" : 31}
	],
	"NE" : [
		{"id" : 0, "text" : "김정희", "type" : "PS_NAME", "begin" : 1, "end" : 1, "weight" : 0.39485, "common_noun" : 0},
		{"id" : 1, "text" : "조선후기", "type" : "DT_DYNASTY", "begin" : 23, "end" : 24, "weight" : 0.832023, "common_noun" : 0},
		{"id" : 2, "text" : "예술인", "type" : "CV_OCCUPATION", "begin" : 27, "end" : 28, "weight" : 0.199289, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿김정희는", "head" : 10, "label" : "NP_SBJ", "mod" : [], "weight" : 0.6381 },
		{"id" : 1, "text" : "날카로운", "head" : 2, "label" : "VP_MOD", "mod" : [], "weight" : 0.661162 },
		{"id" : 2, "text" : "감식안과", "head" : 4, "label" : "NP_CNJ", "mod" : [1], "weight" : 0.711219 },
		{"id" : 3, "text" : "열정적인", "head" : 4, "label" : "VNP_MOD", "mod" : [], "weight" : 0.786448 },
		{"id" : 4, "text" : "예술혼으로", "head" : 7, "label" : "NP_AJT", "mod" : [2, 3], "weight" : 0.803835 },
		{"id" : 5, "text" : "수많은", "head" : 6, "label" : "VP_MOD", "mod" : [], "weight" : 0.896092 },
		{"id" : 6, "text" : "후학을", "head" : 7, "label" : "NP_OBJ", "mod" : [5], "weight" : 0.681465 },
		{"id" : 7, "text" : "길러낸", "head" : 10, "label" : "VP_MOD", "mod" : [4, 6], "weight" : 0.566078 },
		{"id" : 8, "text" : "조선후기", "head" : 9, "label" : "NP", "mod" : [], "weight" : 0.660499 },
		{"id" : 9, "text" : "최고의", "head" : 10, "label" : "NP_MOD", "mod" : [8], "weight" : 0.464588 },
		{"id" : 10, "text" : "예술인이다.", "head" : -1, "label" : "VNP", "mod" : [0, 7, 9], "weight" : 0.0124737 }
	],
	"SRL" : [
		{"verb" : "날카롭", "sense" : 1, "word_id" : 1, "weight" : 0.356239,
			"argument" : [
				{"type" : "ARG1", "word_id" : 2, "text" : "감식안과", "weight" : 0.356239 }
			] },
		{"verb" : "수많", "sense" : 1, "word_id" : 5, "weight" : 0.337563,
			"argument" : [
				{"type" : "ARG1", "word_id" : 6, "text" : "후학을", "weight" : 0.337563 }
			] },
		{"verb" : "기르", "sense" : 1, "word_id" : 7, "weight" : 0.175493,
			"argument" : [
				{"type" : "ARG0", "word_id" : 0, "text" : "﻿김정희는", "weight" : 0.180984 },
				{"type" : "ARGM-INS", "word_id" : 2, "text" : "감식안과", "weight" : 0.166286 },
				{"type" : "ARGM-INS", "word_id" : 4, "text" : "예술혼으로", "weight" : 0.133275 },
				{"type" : "ARG1", "word_id" : 6, "text" : "후학을", "weight" : 0.221429 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "또한 김정희는 이것의 대가이기도 한데 비석에 쓰인 명문을 연구하는 학문인 이것은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "또한", "type" : "MAG", "position" : 141, "weight" : 0.0653106 },
		{"id" : 1, "lemma" : "김정희", "type" : "NNP", "position" : 148, "weight" : 0.9 },
		{"id" : 2, "lemma" : "는", "type" : "JX", "position" : 157, "weight" : 0.0332677 },
		{"id" : 3, "lemma" : "이것", "type" : "NP", "position" : 161, "weight" : 0.0445103 },
		{"id" : 4, "lemma" : "의", "type" : "JKG", "position" : 167, "weight" : 0.134696 },
		{"id" : 5, "lemma" : "대가", "type" : "NNG", "position" : 171, "weight" : 0.868327 },
		{"id" : 6, "lemma" : "이", "type" : "VCP", "position" : 177, "weight" : 0.0177525 },
		{"id" : 7, "lemma" : "기", "type" : "ETN", "position" : 180, "weight" : 0.019088 },
		{"id" : 8, "lemma" : "도", "type" : "JX", "position" : 183, "weight" : 0.147715 },
		{"id" : 9, "lemma" : "한데", "type" : "NNG", "position" : 187, "weight" : 0.258026 },
		{"id" : 10, "lemma" : "비석", "type" : "NNG", "position" : 194, "weight" : 0.9 },
		{"id" : 11, "lemma" : "에", "type" : "JKB", "position" : 200, "weight" : 0.153364 },
		{"id" : 12, "lemma" : "쓰이", "type" : "VV", "position" : 204, "weight" : 0.9 },
		{"id" : 13, "lemma" : "ㄴ", "type" : "ETM", "position" : 207, "weight" : 0.304215 },
		{"id" : 14, "lemma" : "명문", "type" : "NNG", "position" : 211, "weight" : 0.656845 },
		{"id" : 15, "lemma" : "을", "type" : "JKO", "position" : 217, "weight" : 0.129611 },
		{"id" : 16, "lemma" : "연구", "type" : "NNG", "position" : 221, "weight" : 0.9 },
		{"id" : 17, "lemma" : "하", "type" : "XSV", "position" : 227, "weight" : 0.0001 },
		{"id" : 18, "lemma" : "는", "type" : "ETM", "position" : 230, "weight" : 0.238503 },
		{"id" : 19, "lemma" : "학문", "type" : "NNG", "position" : 234, "weight" : 0.9 },
		{"id" : 20, "lemma" : "이", "type" : "VCP", "position" : 240, "weight" : 0.0177525 },
		{"id" : 21, "lemma" : "ㄴ", "type" : "ETM", "position" : 240, "weight" : 0.220712 },
		{"id" : 22, "lemma" : "이것", "type" : "NP", "position" : 244, "weight" : 0.0102992 },
		{"id" : 23, "lemma" : "은", "type" : "JX", "position" : 250, "weight" : 0.191811 },
		{"id" : 24, "lemma" : "무엇", "type" : "NP", "position" : 254, "weight" : 0.9 },
		{"id" : 25, "lemma" : "이", "type" : "VCP", "position" : 260, "weight" : 0.0175768 },
		{"id" : 26, "lemma" : "ㄹ까", "type" : "EF", "position" : 260, "weight" : 0.258243 },
		{"id" : 27, "lemma" : "?", "type" : "SF", "position" : 266, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "또한/MAG", "target" : "또한", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "김정희/NNG+는/JX", "target" : "김정희는", "word_id" : 1, "m_begin" : 1, "m_end" : 2},
		{"id" : 2, "result" : "이것/NP+의/JKG", "target" : "이것의", "word_id" : 2, "m_begin" : 3, "m_end" : 4},
		{"id" : 3, "result" : "대가/NNG+이/VCP+기/ETN+도/JX", "target" : "대가이기도", "word_id" : 3, "m_begin" : 5, "m_end" : 8},
		{"id" : 4, "result" : "한데/NNG", "target" : "한데", "word_id" : 4, "m_begin" : 9, "m_end" : 9},
		{"id" : 5, "result" : "비석/NNG+에/JKB", "target" : "비석에", "word_id" : 5, "m_begin" : 10, "m_end" : 11},
		{"id" : 6, "result" : "쓰이/VV+ㄴ/ETM", "target" : "쓰인", "word_id" : 6, "m_begin" : 12, "m_end" : 13},
		{"id" : 7, "result" : "명문/NNG+을/JKO", "target" : "명문을", "word_id" : 7, "m_begin" : 14, "m_end" : 15},
		{"id" : 8, "result" : "연구하/VV+는/ETM", "target" : "연구하는", "word_id" : 8, "m_begin" : 16, "m_end" : 18},
		{"id" : 9, "result" : "학문/NNG+이/VCP+ㄴ/ETM", "target" : "학문인", "word_id" : 9, "m_begin" : 19, "m_end" : 21},
		{"id" : 10, "result" : "이것/NP+은/JX", "target" : "이것은", "word_id" : 10, "m_begin" : 22, "m_end" : 23},
		{"id" : 11, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 11, "m_begin" : 24, "m_end" : 27}
	],
	"WSD" : [
		{"id" : 0, "text" : "또한", "type" : "MAG", "scode" : "00", "weight" : 1, "position" : 141, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "김정희", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 148, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 157, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 161, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 167, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "대가", "type" : "NNG", "scode" : "06", "weight" : 1, "position" : 171, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 177, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "기", "type" : "ETN", "scode" : "00", "weight" : 1, "position" : 180, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "도", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 183, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "한데", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 187, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "비석", "type" : "NNG", "scode" : "07", "weight" : 1, "position" : 194, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 200, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "쓰이", "type" : "VV", "scode" : "03", "weight" : 1, "position" : 204, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 207, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "명문", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 211, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 217, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "연구하", "type" : "VV", "scode" : "02", "weight" : 1, "position" : 221, "begin" : 16, "end" : 17},
		{"id" : 17, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 230, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "학문", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 234, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 240, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 240, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 244, "begin" : 22, "end" : 22},
		{"id" : 22, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 250, "begin" : 23, "end" : 23},
		{"id" : 23, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 254, "begin" : 24, "end" : 24},
		{"id" : 24, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 260, "begin" : 25, "end" : 25},
		{"id" : 25, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 260, "begin" : 26, "end" : 26},
		{"id" : 26, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 266, "begin" : 27, "end" : 27}
	],
	"word" : [
		{"id" : 0, "text" : "또한", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "김정희는", "type" : "", "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "이것의", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 3, "text" : "대가이기도", "type" : "", "begin" : 5, "end" : 8},
		{"id" : 4, "text" : "한데", "type" : "", "begin" : 9, "end" : 9},
		{"id" : 5, "text" : "비석에", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 6, "text" : "쓰인", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 7, "text" : "명문을", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 8, "text" : "연구하는", "type" : "", "begin" : 16, "end" : 18},
		{"id" : 9, "text" : "학문인", "type" : "", "begin" : 19, "end" : 21},
		{"id" : 10, "text" : "이것은", "type" : "", "begin" : 22, "end" : 23},
		{"id" : 11, "text" : "무엇일까?", "type" : "", "begin" : 24, "end" : 27}
	],
	"NE" : [
		{"id" : 0, "text" : "김정희", "type" : "PS_NAME", "begin" : 1, "end" : 1, "weight" : 0.538784, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "또한", "head" : 3, "label" : "AP", "mod" : [], "weight" : 0.733044 },
		{"id" : 1, "text" : "김정희는", "head" : 3, "label" : "NP_SBJ", "mod" : [], "weight" : 0.772081 },
		{"id" : 2, "text" : "이것의", "head" : 3, "label" : "NP_MOD", "mod" : [], "weight" : 0.635937 },
		{"id" : 3, "text" : "대가이기도", "head" : 4, "label" : "VNP", "mod" : [0, 1, 2], "weight" : 0.702447 },
		{"id" : 4, "text" : "한데", "head" : 9, "label" : "VP", "mod" : [3], "weight" : 0.521153 },
		{"id" : 5, "text" : "비석에", "head" : 6, "label" : "NP_AJT", "mod" : [], "weight" : 0.672676 },
		{"id" : 6, "text" : "쓰인", "head" : 7, "label" : "VP_MOD", "mod" : [5], "weight" : 0.815159 },
		{"id" : 7, "text" : "명문을", "head" : 8, "label" : "NP_OBJ", "mod" : [6], "weight" : 0.972423 },
		{"id" : 8, "text" : "연구하는", "head" : 9, "label" : "VP_MOD", "mod" : [7], "weight" : 0.883498 },
		{"id" : 9, "text" : "학문인", "head" : 10, "label" : "VNP_MOD", "mod" : [4, 8], "weight" : 0.786957 },
		{"id" : 10, "text" : "이것은", "head" : 11, "label" : "NP_SBJ", "mod" : [9], "weight" : 0.710585 },
		{"id" : 11, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [10], "weight" : 0.0242844 }
	],
	"SRL" : [
		{"verb" : "쓰이", "sense" : 2, "word_id" : 6, "weight" : 0.241922,
			"argument" : [
				{"type" : "ARG2", "word_id" : 5, "text" : "비석에", "weight" : 0.162876 },
				{"type" : "ARG1", "word_id" : 7, "text" : "명문을", "weight" : 0.320969 }
			] },
		{"verb" : "연구", "sense" : 1, "word_id" : 8, "weight" : 0.261615,
			"argument" : [
				{"type" : "ARG1", "word_id" : 7, "text" : "명문을", "weight" : 0.261615 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "PS_NAME", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 0, "ne_id" : 0, "text" : "﻿김정희", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "﻿김정희", "weight" : 0.02 },
		{"id" : 10, "sent_id" : 1, "start_eid" : 1, "end_eid" : 1, "ne_id" : 0, "text" : "김정희", "start_eid_short" : 1, "end_eid_short" : 1, "text_short" : "김정희", "weight" : 0.021 },
		{"id" : 11, "sent_id" : 1, "start_eid" : 2, "end_eid" : 2, "ne_id" : -1, "text" : "이것", "start_eid_short" : 2, "end_eid_short" : 2, "text_short" : "이것", "weight" : 0.036 },
		{"id" : 15, "sent_id" : 1, "start_eid" : 10, "end_eid" : 10, "ne_id" : -1, "text" : "이것", "start_eid_short" : 10, "end_eid_short" : 10, "text_short" : "이것", "weight" : 0.024 },
		{"id" : 16, "sent_id" : 1, "start_eid" : 11, "end_eid" : 11, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 11, "end_eid_short" : 11, "text_short" : "무엇이ㄹ까?", "weight" : 0.012 }
	] }
 ]
}

