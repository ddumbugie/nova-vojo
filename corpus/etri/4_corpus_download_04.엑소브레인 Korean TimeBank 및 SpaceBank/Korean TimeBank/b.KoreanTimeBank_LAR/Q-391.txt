{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿2007년에 발행한 만 원 권 지폐의 뒷면에 담긴 그림이 아닌 것은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "XPN", "position" : 0, "weight" : 0 },
		{"id" : 1, "lemma" : "2007", "type" : "SN", "position" : 3, "weight" : 1 },
		{"id" : 2, "lemma" : "년", "type" : "NNB", "position" : 7, "weight" : 0.414343 },
		{"id" : 3, "lemma" : "에", "type" : "JKB", "position" : 10, "weight" : 0.135559 },
		{"id" : 4, "lemma" : "발행", "type" : "NNG", "position" : 14, "weight" : 0.9 },
		{"id" : 5, "lemma" : "하", "type" : "XSV", "position" : 20, "weight" : 0.0001 },
		{"id" : 6, "lemma" : "ㄴ", "type" : "ETM", "position" : 20, "weight" : 0.392321 },
		{"id" : 7, "lemma" : "만", "type" : "NR", "position" : 24, "weight" : 0.000229234 },
		{"id" : 8, "lemma" : "원", "type" : "NNB", "position" : 28, "weight" : 0.318791 },
		{"id" : 9, "lemma" : "권", "type" : "NNB", "position" : 32, "weight" : 0.00511856 },
		{"id" : 10, "lemma" : "지폐", "type" : "NNG", "position" : 36, "weight" : 0.9 },
		{"id" : 11, "lemma" : "의", "type" : "JKG", "position" : 42, "weight" : 0.0694213 },
		{"id" : 12, "lemma" : "뒷면", "type" : "NNG", "position" : 46, "weight" : 0.9 },
		{"id" : 13, "lemma" : "에", "type" : "JKB", "position" : 52, "weight" : 0.153364 },
		{"id" : 14, "lemma" : "담기", "type" : "VV", "position" : 56, "weight" : 0.445042 },
		{"id" : 15, "lemma" : "ㄴ", "type" : "ETM", "position" : 59, "weight" : 0.304215 },
		{"id" : 16, "lemma" : "그림", "type" : "NNG", "position" : 63, "weight" : 0.656598 },
		{"id" : 17, "lemma" : "이", "type" : "JKC", "position" : 69, "weight" : 0.000287945 },
		{"id" : 18, "lemma" : "아니", "type" : "VCN", "position" : 73, "weight" : 0.354175 },
		{"id" : 19, "lemma" : "ㄴ", "type" : "ETM", "position" : 76, "weight" : 0.144018 },
		{"id" : 20, "lemma" : "것", "type" : "NNB", "position" : 80, "weight" : 0.228788 },
		{"id" : 21, "lemma" : "은", "type" : "JX", "position" : 83, "weight" : 0.0688243 },
		{"id" : 22, "lemma" : "무엇", "type" : "NP", "position" : 87, "weight" : 0.9 },
		{"id" : 23, "lemma" : "이", "type" : "VCP", "position" : 93, "weight" : 0.0175768 },
		{"id" : 24, "lemma" : "ㄹ까", "type" : "EF", "position" : 93, "weight" : 0.258243 },
		{"id" : 25, "lemma" : "?", "type" : "SF", "position" : 99, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿/XPN+2007/SN+년/NNB+에/JKB", "target" : "﻿2007년에", "word_id" : 0, "m_begin" : 0, "m_end" : 3},
		{"id" : 1, "result" : "발행하/VV+ㄴ/ETM", "target" : "발행한", "word_id" : 1, "m_begin" : 4, "m_end" : 6},
		{"id" : 2, "result" : "만/NR", "target" : "만", "word_id" : 2, "m_begin" : 7, "m_end" : 7},
		{"id" : 3, "result" : "원/NNB", "target" : "원", "word_id" : 3, "m_begin" : 8, "m_end" : 8},
		{"id" : 4, "result" : "권/NNB", "target" : "권", "word_id" : 4, "m_begin" : 9, "m_end" : 9},
		{"id" : 5, "result" : "지폐/NNG+의/JKG", "target" : "지폐의", "word_id" : 5, "m_begin" : 10, "m_end" : 11},
		{"id" : 6, "result" : "뒷면/NNG+에/JKB", "target" : "뒷면에", "word_id" : 6, "m_begin" : 12, "m_end" : 13},
		{"id" : 7, "result" : "담기/VV+ㄴ/ETM", "target" : "담긴", "word_id" : 7, "m_begin" : 14, "m_end" : 15},
		{"id" : 8, "result" : "그림/NNG+이/JKC", "target" : "그림이", "word_id" : 8, "m_begin" : 16, "m_end" : 17},
		{"id" : 9, "result" : "아니/VCN+ㄴ/ETM", "target" : "아닌", "word_id" : 9, "m_begin" : 18, "m_end" : 19},
		{"id" : 10, "result" : "것/NNB+은/JX", "target" : "것은", "word_id" : 10, "m_begin" : 20, "m_end" : 21},
		{"id" : 11, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 11, "m_begin" : 22, "m_end" : 25}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "XPN", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "2007", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 7, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "발행하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 14, "begin" : 4, "end" : 5},
		{"id" : 5, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 20, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "만", "type" : "NR", "scode" : "06", "weight" : 1, "position" : 24, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "원", "type" : "NNB", "scode" : "01", "weight" : 1, "position" : 28, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "권", "type" : "NNB", "scode" : "01", "weight" : 1, "position" : 32, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "지폐", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 36, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 42, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "뒷면", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 46, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 52, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "담기", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 56, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 59, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "그림", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 63, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "이", "type" : "JKC", "scode" : "00", "weight" : 1, "position" : 69, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "아니", "type" : "VCN", "scode" : "00", "weight" : 1, "position" : 73, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 76, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "것", "type" : "NNB", "scode" : "01", "weight" : 1, "position" : 80, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 83, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 87, "begin" : 22, "end" : 22},
		{"id" : 22, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 93, "begin" : 23, "end" : 23},
		{"id" : 23, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 93, "begin" : 24, "end" : 24},
		{"id" : 24, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 99, "begin" : 25, "end" : 25}
	],
	"word" : [
		{"id" : 0, "text" : "﻿2007년에", "type" : "", "begin" : 0, "end" : 3},
		{"id" : 1, "text" : "발행한", "type" : "", "begin" : 4, "end" : 6},
		{"id" : 2, "text" : "만", "type" : "", "begin" : 7, "end" : 7},
		{"id" : 3, "text" : "원", "type" : "", "begin" : 8, "end" : 8},
		{"id" : 4, "text" : "권", "type" : "", "begin" : 9, "end" : 9},
		{"id" : 5, "text" : "지폐의", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 6, "text" : "뒷면에", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 7, "text" : "담긴", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 8, "text" : "그림이", "type" : "", "begin" : 16, "end" : 17},
		{"id" : 9, "text" : "아닌", "type" : "", "begin" : 18, "end" : 19},
		{"id" : 10, "text" : "것은", "type" : "", "begin" : 20, "end" : 21},
		{"id" : 11, "text" : "무엇일까?", "type" : "", "begin" : 22, "end" : 25}
	],
	"NE" : [
		{"id" : 0, "text" : "﻿2007년", "type" : "DT_YEAR", "begin" : 0, "end" : 2, "weight" : 0.306388, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿2007년에", "head" : 1, "label" : "NP_AJT", "mod" : [], "weight" : 0.66521 },
		{"id" : 1, "text" : "발행한", "head" : 5, "label" : "VP_MOD", "mod" : [0], "weight" : 0.622837 },
		{"id" : 2, "text" : "만", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.964218 },
		{"id" : 3, "text" : "원", "head" : 4, "label" : "NP", "mod" : [2], "weight" : 0.706144 },
		{"id" : 4, "text" : "권", "head" : 5, "label" : "NP", "mod" : [3], "weight" : 0.683469 },
		{"id" : 5, "text" : "지폐의", "head" : 6, "label" : "NP_MOD", "mod" : [1, 4], "weight" : 0.750808 },
		{"id" : 6, "text" : "뒷면에", "head" : 7, "label" : "NP_AJT", "mod" : [5], "weight" : 0.690628 },
		{"id" : 7, "text" : "담긴", "head" : 8, "label" : "VP_MOD", "mod" : [6], "weight" : 0.76403 },
		{"id" : 8, "text" : "그림이", "head" : 9, "label" : "NP_CMP", "mod" : [7], "weight" : 0.968567 },
		{"id" : 9, "text" : "아닌", "head" : 10, "label" : "VP_MOD", "mod" : [8], "weight" : 0.817358 },
		{"id" : 10, "text" : "것은", "head" : 11, "label" : "NP_SBJ", "mod" : [9], "weight" : 0.639813 },
		{"id" : 11, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [10], "weight" : 0.0251206 }
	],
	"SRL" : [
		{"verb" : "발행", "sense" : 1, "word_id" : 1, "weight" : 0.198728,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 0, "text" : "﻿2007년에", "weight" : 0.183496 },
				{"type" : "ARG1", "word_id" : 5, "text" : "지폐의", "weight" : 0.213961 }
			] },
		{"verb" : "담기", "sense" : 1, "word_id" : 7, "weight" : 0.247324,
			"argument" : [
				{"type" : "ARG2", "word_id" : 6, "text" : "뒷면에", "weight" : 0.180275 },
				{"type" : "ARG1", "word_id" : 8, "text" : "그림이", "weight" : 0.314373 }
			] },
		{"verb" : "아니", "sense" : 1, "word_id" : 9, "weight" : 0.283472,
			"argument" : [
				{"type" : "ARG2", "word_id" : 8, "text" : "그림이", "weight" : 0.392101 },
				{"type" : "ARG1", "word_id" : 10, "text" : "것은", "weight" : 0.174844 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
 ]
}

