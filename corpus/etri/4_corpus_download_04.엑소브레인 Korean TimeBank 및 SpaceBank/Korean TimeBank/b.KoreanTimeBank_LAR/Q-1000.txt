{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "초기 철기 시대의 나라인 동예에서 하늘에 지낸 제천 의식은 무엇일까?",
	"morp" : [
		{"id" : 0, "lemma" : "초기", "type" : "NNG", "position" : 0, "weight" : 0.9 },
		{"id" : 1, "lemma" : "철기", "type" : "NNG", "position" : 7, "weight" : 0.181858 },
		{"id" : 2, "lemma" : "시대", "type" : "NNG", "position" : 14, "weight" : 0.9 },
		{"id" : 3, "lemma" : "의", "type" : "JKG", "position" : 20, "weight" : 0.0694213 },
		{"id" : 4, "lemma" : "나라", "type" : "NNG", "position" : 24, "weight" : 0.869517 },
		{"id" : 5, "lemma" : "이", "type" : "VCP", "position" : 30, "weight" : 0.0177525 },
		{"id" : 6, "lemma" : "ㄴ", "type" : "ETM", "position" : 30, "weight" : 0.220712 },
		{"id" : 7, "lemma" : "동예", "type" : "NNG", "position" : 34, "weight" : 0.2 },
		{"id" : 8, "lemma" : "에서", "type" : "JKB", "position" : 40, "weight" : 0.153407 },
		{"id" : 9, "lemma" : "하늘", "type" : "NNG", "position" : 47, "weight" : 0.208012 },
		{"id" : 10, "lemma" : "에", "type" : "JKB", "position" : 53, "weight" : 0.153364 },
		{"id" : 11, "lemma" : "지내", "type" : "VV", "position" : 57, "weight" : 0.444902 },
		{"id" : 12, "lemma" : "ㄴ", "type" : "ETM", "position" : 60, "weight" : 0.304215 },
		{"id" : 13, "lemma" : "제천", "type" : "NNP", "position" : 64, "weight" : 0.0178158 },
		{"id" : 14, "lemma" : "의식", "type" : "NNG", "position" : 71, "weight" : 0.281878 },
		{"id" : 15, "lemma" : "은", "type" : "JX", "position" : 77, "weight" : 0.0449928 },
		{"id" : 16, "lemma" : "무엇", "type" : "NP", "position" : 81, "weight" : 0.9 },
		{"id" : 17, "lemma" : "이", "type" : "VCP", "position" : 87, "weight" : 0.0175768 },
		{"id" : 18, "lemma" : "ㄹ까", "type" : "EF", "position" : 87, "weight" : 0.258243 },
		{"id" : 19, "lemma" : "?", "type" : "SF", "position" : 93, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "초기/NNG", "target" : "초기", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "철기/NNG", "target" : "철기", "word_id" : 1, "m_begin" : 1, "m_end" : 1},
		{"id" : 2, "result" : "시대/NNG+의/JKG", "target" : "시대의", "word_id" : 2, "m_begin" : 2, "m_end" : 3},
		{"id" : 3, "result" : "나라/NNG+이/VCP+ㄴ/ETM", "target" : "나라인", "word_id" : 3, "m_begin" : 4, "m_end" : 6},
		{"id" : 4, "result" : "동예/NNG+에서/JKB", "target" : "동예에서", "word_id" : 4, "m_begin" : 7, "m_end" : 8},
		{"id" : 5, "result" : "하늘/NNG+에/JKB", "target" : "하늘에", "word_id" : 5, "m_begin" : 9, "m_end" : 10},
		{"id" : 6, "result" : "지내/VV+ㄴ/ETM", "target" : "지낸", "word_id" : 6, "m_begin" : 11, "m_end" : 12},
		{"id" : 7, "result" : "제천/NNG", "target" : "제천", "word_id" : 7, "m_begin" : 13, "m_end" : 13},
		{"id" : 8, "result" : "의식/NNG+은/JX", "target" : "의식은", "word_id" : 8, "m_begin" : 14, "m_end" : 15},
		{"id" : 9, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 9, "m_begin" : 16, "m_end" : 19}
	],
	"WSD" : [
		{"id" : 0, "text" : "초기", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "철기", "type" : "NNG", "scode" : "05", "weight" : 1, "position" : 7, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "시대", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 14, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 20, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "나라", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 24, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 30, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 30, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "동예", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 34, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "하늘", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 47, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 53, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "지내", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 57, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 60, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "제천", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 64, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "의식", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 71, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 77, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 81, "begin" : 16, "end" : 16},
		{"id" : 17, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 87, "begin" : 17, "end" : 17},
		{"id" : 18, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 87, "begin" : 18, "end" : 18},
		{"id" : 19, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 93, "begin" : 19, "end" : 19}
	],
	"word" : [
		{"id" : 0, "text" : "초기", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "철기", "type" : "", "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "시대의", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 3, "text" : "나라인", "type" : "", "begin" : 4, "end" : 6},
		{"id" : 4, "text" : "동예에서", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 5, "text" : "하늘에", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 6, "text" : "지낸", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 7, "text" : "제천", "type" : "", "begin" : 13, "end" : 13},
		{"id" : 8, "text" : "의식은", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 9, "text" : "무엇일까?", "type" : "", "begin" : 16, "end" : 19}
	],
	"NE" : [
		{"id" : 0, "text" : "철기 시대", "type" : "DT_DYNASTY", "begin" : 1, "end" : 2, "weight" : 0.753946, "common_noun" : 0},
		{"id" : 1, "text" : "동예", "type" : "LCP_COUNTRY", "begin" : 7, "end" : 7, "weight" : 0.301653, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "초기", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.0689243 },
		{"id" : 1, "text" : "철기", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.783198 },
		{"id" : 2, "text" : "시대의", "head" : 3, "label" : "NP_MOD", "mod" : [1], "weight" : 0.706632 },
		{"id" : 3, "text" : "나라인", "head" : 4, "label" : "VNP_MOD", "mod" : [2], "weight" : 0.596138 },
		{"id" : 4, "text" : "동예에서", "head" : 6, "label" : "NP_AJT", "mod" : [3], "weight" : 0.229278 },
		{"id" : 5, "text" : "하늘에", "head" : 6, "label" : "NP_AJT", "mod" : [], "weight" : 0.71566 },
		{"id" : 6, "text" : "지낸", "head" : 8, "label" : "VP_MOD", "mod" : [4, 5], "weight" : 0.717908 },
		{"id" : 7, "text" : "제천", "head" : 8, "label" : "NP", "mod" : [], "weight" : 0.659859 },
		{"id" : 8, "text" : "의식은", "head" : 9, "label" : "NP_SBJ", "mod" : [6, 7], "weight" : 0.772797 },
		{"id" : 9, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [8], "weight" : 0.00117751 }
	],
	"SRL" : [
		{"verb" : "지내", "sense" : 2, "word_id" : 6, "weight" : 0.179143,
			"argument" : [
				{"type" : "ARG0", "word_id" : 4, "text" : "동예에서", "weight" : 0.106492 },
				{"type" : "ARG2", "word_id" : 5, "text" : "하늘에", "weight" : 0.155979 },
				{"type" : "ARG1", "word_id" : 8, "text" : "의식은", "weight" : 0.274957 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "동예", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 8, "ne_id" : -1, "text" : "초기 철기 시대의 나라인 동예에서 하늘에 지낸 제천 의식", "start_eid_short" : 7, "end_eid_short" : 8, "text_short" : "제천 의식", "weight" : 0.002 },
		{"id" : 6, "sent_id" : 0, "start_eid" : 9, "end_eid" : 9, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 9, "end_eid_short" : 9, "text_short" : "무엇이ㄹ까?", "weight" : 0.006 }
	] }
 ]
}
