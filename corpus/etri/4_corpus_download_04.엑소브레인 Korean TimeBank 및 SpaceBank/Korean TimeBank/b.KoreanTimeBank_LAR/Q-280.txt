{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿이것은 불교의 고승이나 이름난 유학자의 가르침의 내용을 제자들이 기록한 것으로 오늘날에는 그 의미를 확장해 위인들이 한 말을 간추려 모은 기록을 뜻하기도 한다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "이것", "type" : "NP", "position" : 3, "weight" : 0.0020857 },
		{"id" : 2, "lemma" : "은", "type" : "JX", "position" : 9, "weight" : 0.191811 },
		{"id" : 3, "lemma" : "불교", "type" : "NNG", "position" : 13, "weight" : 0.324753 },
		{"id" : 4, "lemma" : "의", "type" : "JKG", "position" : 19, "weight" : 0.0694213 },
		{"id" : 5, "lemma" : "고승", "type" : "NNG", "position" : 23, "weight" : 0.9 },
		{"id" : 6, "lemma" : "이나", "type" : "JC", "position" : 29, "weight" : 0.0238189 },
		{"id" : 7, "lemma" : "이름나", "type" : "VV", "position" : 36, "weight" : 0.9 },
		{"id" : 8, "lemma" : "ㄴ", "type" : "ETM", "position" : 42, "weight" : 0.304215 },
		{"id" : 9, "lemma" : "유학", "type" : "NNG", "position" : 46, "weight" : 0.9 },
		{"id" : 10, "lemma" : "자", "type" : "XSN", "position" : 52, "weight" : 0.00398741 },
		{"id" : 11, "lemma" : "의", "type" : "JKG", "position" : 55, "weight" : 0.120732 },
		{"id" : 12, "lemma" : "가르침", "type" : "NNG", "position" : 59, "weight" : 0.9 },
		{"id" : 13, "lemma" : "의", "type" : "JKG", "position" : 68, "weight" : 0.0694213 },
		{"id" : 14, "lemma" : "내용", "type" : "NNG", "position" : 72, "weight" : 0.87126 },
		{"id" : 15, "lemma" : "을", "type" : "JKO", "position" : 78, "weight" : 0.129611 },
		{"id" : 16, "lemma" : "제자", "type" : "NNG", "position" : 82, "weight" : 0.9 },
		{"id" : 17, "lemma" : "들", "type" : "XSN", "position" : 88, "weight" : 0.0299841 },
		{"id" : 18, "lemma" : "이", "type" : "JKS", "position" : 91, "weight" : 0.0731805 },
		{"id" : 19, "lemma" : "기록", "type" : "NNG", "position" : 95, "weight" : 0.9 },
		{"id" : 20, "lemma" : "하", "type" : "XSV", "position" : 101, "weight" : 0.0001 },
		{"id" : 21, "lemma" : "ㄴ", "type" : "ETM", "position" : 101, "weight" : 0.392321 },
		{"id" : 22, "lemma" : "것", "type" : "NNB", "position" : 105, "weight" : 0.228788 },
		{"id" : 23, "lemma" : "으로", "type" : "JKB", "position" : 108, "weight" : 0.135596 },
		{"id" : 24, "lemma" : "오늘", "type" : "NNG", "position" : 115, "weight" : 0.171408 },
		{"id" : 25, "lemma" : "날", "type" : "NNG", "position" : 121, "weight" : 0.167745 },
		{"id" : 26, "lemma" : "에", "type" : "JKB", "position" : 124, "weight" : 0.153364 },
		{"id" : 27, "lemma" : "는", "type" : "JX", "position" : 127, "weight" : 0.0387928 },
		{"id" : 28, "lemma" : "그", "type" : "MM", "position" : 131, "weight" : 0.0229726 },
		{"id" : 29, "lemma" : "의미", "type" : "NNG", "position" : 135, "weight" : 0.9 },
		{"id" : 30, "lemma" : "를", "type" : "JKO", "position" : 141, "weight" : 0.137686 },
		{"id" : 31, "lemma" : "확장", "type" : "NNG", "position" : 145, "weight" : 0.9 },
		{"id" : 32, "lemma" : "하", "type" : "XSV", "position" : 151, "weight" : 0.0001 },
		{"id" : 33, "lemma" : "어", "type" : "EC", "position" : 151, "weight" : 0.361326 },
		{"id" : 34, "lemma" : "위인", "type" : "NNG", "position" : 155, "weight" : 0.9 },
		{"id" : 35, "lemma" : "들", "type" : "XSN", "position" : 161, "weight" : 0.0299841 },
		{"id" : 36, "lemma" : "이", "type" : "JKS", "position" : 164, "weight" : 0.0731805 },
		{"id" : 37, "lemma" : "하", "type" : "VV", "position" : 168, "weight" : 0.194733 },
		{"id" : 38, "lemma" : "ㄴ", "type" : "ETM", "position" : 168, "weight" : 0.304215 },
		{"id" : 39, "lemma" : "말", "type" : "NNG", "position" : 172, "weight" : 0.528926 },
		{"id" : 40, "lemma" : "을", "type" : "JKO", "position" : 175, "weight" : 0.129611 },
		{"id" : 41, "lemma" : "간추리", "type" : "VV", "position" : 179, "weight" : 0.9 },
		{"id" : 42, "lemma" : "어", "type" : "EC", "position" : 185, "weight" : 0.41831 },
		{"id" : 43, "lemma" : "모으", "type" : "VV", "position" : 189, "weight" : 0.9 },
		{"id" : 44, "lemma" : "ㄴ", "type" : "ETM", "position" : 192, "weight" : 0.304215 },
		{"id" : 45, "lemma" : "기록", "type" : "NNG", "position" : 196, "weight" : 0.9 },
		{"id" : 46, "lemma" : "을", "type" : "JKO", "position" : 202, "weight" : 0.129611 },
		{"id" : 47, "lemma" : "뜻", "type" : "NNG", "position" : 206, "weight" : 0.9 },
		{"id" : 48, "lemma" : "하", "type" : "XSV", "position" : 209, "weight" : 0.0001 },
		{"id" : 49, "lemma" : "기", "type" : "ETN", "position" : 212, "weight" : 0.0556099 },
		{"id" : 50, "lemma" : "도", "type" : "JX", "position" : 215, "weight" : 0.147715 },
		{"id" : 51, "lemma" : "하", "type" : "VX", "position" : 219, "weight" : 0.00978548 },
		{"id" : 52, "lemma" : "ㄴ다", "type" : "EF", "position" : 219, "weight" : 0.184394 },
		{"id" : 53, "lemma" : ".", "type" : "SF", "position" : 225, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿/NNG+이것/NP+은/JX", "target" : "﻿이것은", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "불교/NNG+의/JKG", "target" : "불교의", "word_id" : 1, "m_begin" : 3, "m_end" : 4},
		{"id" : 2, "result" : "고승/NNG+이나/JC", "target" : "고승이나", "word_id" : 2, "m_begin" : 5, "m_end" : 6},
		{"id" : 3, "result" : "이름나/VV+ㄴ/ETM", "target" : "이름난", "word_id" : 3, "m_begin" : 7, "m_end" : 8},
		{"id" : 4, "result" : "유학자/NNG+의/JKG", "target" : "유학자의", "word_id" : 4, "m_begin" : 9, "m_end" : 11},
		{"id" : 5, "result" : "가르침/NNG+의/JKG", "target" : "가르침의", "word_id" : 5, "m_begin" : 12, "m_end" : 13},
		{"id" : 6, "result" : "내용/NNG+을/JKO", "target" : "내용을", "word_id" : 6, "m_begin" : 14, "m_end" : 15},
		{"id" : 7, "result" : "제자들/NNG+이/JKS", "target" : "제자들이", "word_id" : 7, "m_begin" : 16, "m_end" : 18},
		{"id" : 8, "result" : "기록하/VV+ㄴ/ETM", "target" : "기록한", "word_id" : 8, "m_begin" : 19, "m_end" : 21},
		{"id" : 9, "result" : "것/NNB+으로/JKB", "target" : "것으로", "word_id" : 9, "m_begin" : 22, "m_end" : 23},
		{"id" : 10, "result" : "오늘날/NNG+에/JKB+는/JX", "target" : "오늘날에는", "word_id" : 10, "m_begin" : 24, "m_end" : 27},
		{"id" : 11, "result" : "그/MM", "target" : "그", "word_id" : 11, "m_begin" : 28, "m_end" : 28},
		{"id" : 12, "result" : "의미/NNG+를/JKO", "target" : "의미를", "word_id" : 12, "m_begin" : 29, "m_end" : 30},
		{"id" : 13, "result" : "확장하/VV+어/EC", "target" : "확장해", "word_id" : 13, "m_begin" : 31, "m_end" : 33},
		{"id" : 14, "result" : "위인들/NNG+이/JKS", "target" : "위인들이", "word_id" : 14, "m_begin" : 34, "m_end" : 36},
		{"id" : 15, "result" : "하/VV+ㄴ/ETM", "target" : "한", "word_id" : 15, "m_begin" : 37, "m_end" : 38},
		{"id" : 16, "result" : "말/NNG+을/JKO", "target" : "말을", "word_id" : 16, "m_begin" : 39, "m_end" : 40},
		{"id" : 17, "result" : "간추리/VV+어/EC", "target" : "간추려", "word_id" : 17, "m_begin" : 41, "m_end" : 42},
		{"id" : 18, "result" : "모으/VV+ㄴ/ETM", "target" : "모은", "word_id" : 18, "m_begin" : 43, "m_end" : 44},
		{"id" : 19, "result" : "기록/NNG+을/JKO", "target" : "기록을", "word_id" : 19, "m_begin" : 45, "m_end" : 46},
		{"id" : 20, "result" : "뜻하/VV+기/ETN+도/JX", "target" : "뜻하기도", "word_id" : 20, "m_begin" : 47, "m_end" : 50},
		{"id" : 21, "result" : "하/VX+ㄴ다/EF+./SF", "target" : "한다.", "word_id" : 21, "m_begin" : 51, "m_end" : 53}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "불교", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 13, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 19, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "고승", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 23, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "이나", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 29, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "이름나", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 36, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 42, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "유학자", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 46, "begin" : 9, "end" : 10},
		{"id" : 10, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 55, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "가르침", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 59, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 68, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "내용", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 72, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 78, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "제자", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 82, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "들", "type" : "XSN", "scode" : "09", "weight" : 1, "position" : 88, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 91, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "기록하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 95, "begin" : 19, "end" : 20},
		{"id" : 19, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 101, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : "것", "type" : "NNB", "scode" : "01", "weight" : 1, "position" : 105, "begin" : 22, "end" : 22},
		{"id" : 21, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 108, "begin" : 23, "end" : 23},
		{"id" : 22, "text" : "오늘날", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 115, "begin" : 24, "end" : 25},
		{"id" : 23, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 124, "begin" : 26, "end" : 26},
		{"id" : 24, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 127, "begin" : 27, "end" : 27},
		{"id" : 25, "text" : "그", "type" : "MM", "scode" : "01", "weight" : 1, "position" : 131, "begin" : 28, "end" : 28},
		{"id" : 26, "text" : "의미", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 135, "begin" : 29, "end" : 29},
		{"id" : 27, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 141, "begin" : 30, "end" : 30},
		{"id" : 28, "text" : "확장하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 145, "begin" : 31, "end" : 32},
		{"id" : 29, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 151, "begin" : 33, "end" : 33},
		{"id" : 30, "text" : "위인", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 155, "begin" : 34, "end" : 34},
		{"id" : 31, "text" : "들", "type" : "XSN", "scode" : "09", "weight" : 1, "position" : 161, "begin" : 35, "end" : 35},
		{"id" : 32, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 164, "begin" : 36, "end" : 36},
		{"id" : 33, "text" : "하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 168, "begin" : 37, "end" : 37},
		{"id" : 34, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 168, "begin" : 38, "end" : 38},
		{"id" : 35, "text" : "말", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 172, "begin" : 39, "end" : 39},
		{"id" : 36, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 175, "begin" : 40, "end" : 40},
		{"id" : 37, "text" : "간추리", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 179, "begin" : 41, "end" : 41},
		{"id" : 38, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 185, "begin" : 42, "end" : 42},
		{"id" : 39, "text" : "모으", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 189, "begin" : 43, "end" : 43},
		{"id" : 40, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 192, "begin" : 44, "end" : 44},
		{"id" : 41, "text" : "기록", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 196, "begin" : 45, "end" : 45},
		{"id" : 42, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 202, "begin" : 46, "end" : 46},
		{"id" : 43, "text" : "뜻하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 206, "begin" : 47, "end" : 48},
		{"id" : 44, "text" : "기", "type" : "ETN", "scode" : "00", "weight" : 1, "position" : 212, "begin" : 49, "end" : 49},
		{"id" : 45, "text" : "도", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 215, "begin" : 50, "end" : 50},
		{"id" : 46, "text" : "하", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 219, "begin" : 51, "end" : 51},
		{"id" : 47, "text" : "ㄴ다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 219, "begin" : 52, "end" : 52},
		{"id" : 48, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 225, "begin" : 53, "end" : 53}
	],
	"word" : [
		{"id" : 0, "text" : "﻿이것은", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "불교의", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 2, "text" : "고승이나", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 3, "text" : "이름난", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 4, "text" : "유학자의", "type" : "", "begin" : 9, "end" : 11},
		{"id" : 5, "text" : "가르침의", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 6, "text" : "내용을", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 7, "text" : "제자들이", "type" : "", "begin" : 16, "end" : 18},
		{"id" : 8, "text" : "기록한", "type" : "", "begin" : 19, "end" : 21},
		{"id" : 9, "text" : "것으로", "type" : "", "begin" : 22, "end" : 23},
		{"id" : 10, "text" : "오늘날에는", "type" : "", "begin" : 24, "end" : 27},
		{"id" : 11, "text" : "그", "type" : "", "begin" : 28, "end" : 28},
		{"id" : 12, "text" : "의미를", "type" : "", "begin" : 29, "end" : 30},
		{"id" : 13, "text" : "확장해", "type" : "", "begin" : 31, "end" : 33},
		{"id" : 14, "text" : "위인들이", "type" : "", "begin" : 34, "end" : 36},
		{"id" : 15, "text" : "한", "type" : "", "begin" : 37, "end" : 38},
		{"id" : 16, "text" : "말을", "type" : "", "begin" : 39, "end" : 40},
		{"id" : 17, "text" : "간추려", "type" : "", "begin" : 41, "end" : 42},
		{"id" : 18, "text" : "모은", "type" : "", "begin" : 43, "end" : 44},
		{"id" : 19, "text" : "기록을", "type" : "", "begin" : 45, "end" : 46},
		{"id" : 20, "text" : "뜻하기도", "type" : "", "begin" : 47, "end" : 50},
		{"id" : 21, "text" : "한다.", "type" : "", "begin" : 51, "end" : 53}
	],
	"NE" : [
		{"id" : 0, "text" : "불교", "type" : "OGG_RELIGION", "begin" : 3, "end" : 3, "weight" : 0.501231, "common_noun" : 0},
		{"id" : 1, "text" : "고승", "type" : "CV_POSITION", "begin" : 5, "end" : 5, "weight" : 0.454642, "common_noun" : 0},
		{"id" : 2, "text" : "유학자", "type" : "CV_POSITION", "begin" : 9, "end" : 10, "weight" : 0.729989, "common_noun" : 0},
		{"id" : 3, "text" : "제자", "type" : "CV_RELATION", "begin" : 16, "end" : 16, "weight" : 0.288244, "common_noun" : 0},
		{"id" : 4, "text" : "오늘날", "type" : "DT_OTHERS", "begin" : 24, "end" : 25, "weight" : 0.426664, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿이것은", "head" : 13, "label" : "NP_SBJ", "mod" : [], "weight" : 0.897556 },
		{"id" : 1, "text" : "불교의", "head" : 2, "label" : "NP_MOD", "mod" : [], "weight" : 0.558874 },
		{"id" : 2, "text" : "고승이나", "head" : 4, "label" : "NP_CNJ", "mod" : [1], "weight" : 0.500305 },
		{"id" : 3, "text" : "이름난", "head" : 4, "label" : "VP_MOD", "mod" : [], "weight" : 0.69753 },
		{"id" : 4, "text" : "유학자의", "head" : 5, "label" : "NP_MOD", "mod" : [2, 3], "weight" : 0.752501 },
		{"id" : 5, "text" : "가르침의", "head" : 6, "label" : "NP_MOD", "mod" : [4], "weight" : 0.983531 },
		{"id" : 6, "text" : "내용을", "head" : 8, "label" : "NP_OBJ", "mod" : [5], "weight" : 0.670195 },
		{"id" : 7, "text" : "제자들이", "head" : 8, "label" : "NP_SBJ", "mod" : [], "weight" : 0.811315 },
		{"id" : 8, "text" : "기록한", "head" : 9, "label" : "VP_MOD", "mod" : [6, 7], "weight" : 0.733586 },
		{"id" : 9, "text" : "것으로", "head" : 13, "label" : "NP_AJT", "mod" : [8], "weight" : 0.713027 },
		{"id" : 10, "text" : "오늘날에는", "head" : 13, "label" : "NP_AJT", "mod" : [], "weight" : 0.948954 },
		{"id" : 11, "text" : "그", "head" : 12, "label" : "DP", "mod" : [], "weight" : 0.930413 },
		{"id" : 12, "text" : "의미를", "head" : 13, "label" : "NP_OBJ", "mod" : [11], "weight" : 0.757391 },
		{"id" : 13, "text" : "확장해", "head" : 20, "label" : "VP", "mod" : [0, 9, 10, 12], "weight" : 0.962383 },
		{"id" : 14, "text" : "위인들이", "head" : 15, "label" : "NP_SBJ", "mod" : [], "weight" : 0.658313 },
		{"id" : 15, "text" : "한", "head" : 16, "label" : "VP_MOD", "mod" : [14], "weight" : 0.945498 },
		{"id" : 16, "text" : "말을", "head" : 17, "label" : "NP_OBJ", "mod" : [15], "weight" : 0.676593 },
		{"id" : 17, "text" : "간추려", "head" : 18, "label" : "VP", "mod" : [16], "weight" : 0.637979 },
		{"id" : 18, "text" : "모은", "head" : 19, "label" : "VP_MOD", "mod" : [17], "weight" : 0.923938 },
		{"id" : 19, "text" : "기록을", "head" : 20, "label" : "NP_OBJ", "mod" : [18], "weight" : 0.673885 },
		{"id" : 20, "text" : "뜻하기도", "head" : 21, "label" : "VP", "mod" : [13, 19], "weight" : 0.461383 },
		{"id" : 21, "text" : "한다.", "head" : -1, "label" : "VP", "mod" : [20], "weight" : 0.00150201 }
	],
	"SRL" : [
		{"verb" : "이름나", "sense" : 1, "word_id" : 3, "weight" : 0.219369,
			"argument" : [
				{"type" : "ARG1", "word_id" : 4, "text" : "유학자의", "weight" : 0.219369 }
			] },
		{"verb" : "기록", "sense" : 1, "word_id" : 8, "weight" : 0.330792,
			"argument" : [
				{"type" : "ARG1", "word_id" : 6, "text" : "내용을", "weight" : 0.382987 },
				{"type" : "ARG0", "word_id" : 7, "text" : "제자들이", "weight" : 0.278597 }
			] },
		{"verb" : "확장", "sense" : 1, "word_id" : 13, "weight" : 0.192028,
			"argument" : [
				{"type" : "ARGM-PRD", "word_id" : 9, "text" : "것으로", "weight" : 0.11274 },
				{"type" : "ARGM-TMP", "word_id" : 10, "text" : "오늘날에는", "weight" : 0.22331 },
				{"type" : "ARG1", "word_id" : 12, "text" : "의미를", "weight" : 0.240034 }
			] },
		{"verb" : "하", "sense" : 1, "word_id" : 15, "weight" : 0.228354,
			"argument" : [
				{"type" : "ARG0", "word_id" : 14, "text" : "위인들이", "weight" : 0.287687 },
				{"type" : "ARG1", "word_id" : 16, "text" : "말을", "weight" : 0.16902 }
			] },
		{"verb" : "간추리", "sense" : 1, "word_id" : 17, "weight" : 0.258868,
			"argument" : [
				{"type" : "ARG1", "word_id" : 16, "text" : "말을", "weight" : 0.258868 }
			] },
		{"verb" : "모으", "sense" : 1, "word_id" : 18, "weight" : 0.173552,
			"argument" : [
				{"type" : "ARGM-MNR", "word_id" : 17, "text" : "간추려", "weight" : 0.140352 },
				{"type" : "ARG1", "word_id" : 19, "text" : "기록을", "weight" : 0.206752 }
			] },
		{"verb" : "뜻", "sense" : 1, "word_id" : 20, "weight" : 0.325339,
			"argument" : [
				{"type" : "ARGM-MNR", "word_id" : 13, "text" : "확장해", "weight" : 0.144931 },
				{"type" : "ARG2", "word_id" : 19, "text" : "기록을", "weight" : 0.387495 },
				{"type" : "AUX", "word_id" : 21, "text" : "한다.", "weight" : 0.443591 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 17, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.492135 },
		{"id" : 1, "verb_wid" : 20, "ant_sid" : 0, "ant_wid" : 0, "type" : "s", "istitle" : 0, "weight" : 0.41878 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "이것은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "이것", "type" : "NP", "position" : 227, "weight" : 0.0733657 },
		{"id" : 1, "lemma" : "은", "type" : "JX", "position" : 233, "weight" : 0.191811 },
		{"id" : 2, "lemma" : "무엇", "type" : "NP", "position" : 237, "weight" : 0.9 },
		{"id" : 3, "lemma" : "이", "type" : "VCP", "position" : 243, "weight" : 0.0175768 },
		{"id" : 4, "lemma" : "ㄹ까", "type" : "EF", "position" : 243, "weight" : 0.258243 },
		{"id" : 5, "lemma" : "?", "type" : "SF", "position" : 249, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이것/NP+은/JX", "target" : "이것은", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 1, "m_begin" : 2, "m_end" : 5}
	],
	"WSD" : [
		{"id" : 0, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 227, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 233, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 237, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 243, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 243, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 249, "begin" : 5, "end" : 5}
	],
	"word" : [
		{"id" : 0, "text" : "이것은", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "무엇일까?", "type" : "", "begin" : 2, "end" : 5}
	],
	"NE" : [
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이것은", "head" : 1, "label" : "NP_SBJ", "mod" : [], "weight" : 0.769102 },
		{"id" : 1, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [0], "weight" : 0.556699 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "CV_POSITION", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 5, "sent_id" : 0, "start_eid" : 1, "end_eid" : 2, "ne_id" : 1, "text" : "불교의 고승", "start_eid_short" : 1, "end_eid_short" : 2, "text_short" : "불교의 고승", "weight" : 0.005 },
		{"id" : 11, "sent_id" : 0, "start_eid" : 11, "end_eid" : 12, "ne_id" : -1, "text" : "그 의미", "start_eid_short" : 11, "end_eid_short" : 12, "text_short" : "그 의미", "weight" : 0.012 },
		{"id" : 16, "sent_id" : 1, "start_eid" : 0, "end_eid" : 0, "ne_id" : -1, "text" : "이것", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "이것", "weight" : 0.018 }
	] }
 ]
}

