{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿성균관(成均館)은 고려 말부터 이어진 조선의 최고 교육기관이다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿성균관", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "(", "type" : "SS", "position" : 12, "weight" : 1 },
		{"id" : 2, "lemma" : "成均館", "type" : "SH", "position" : 13, "weight" : 1 },
		{"id" : 3, "lemma" : ")", "type" : "SS", "position" : 22, "weight" : 1 },
		{"id" : 4, "lemma" : "은", "type" : "JX", "position" : 23, "weight" : 0.0128817 },
		{"id" : 5, "lemma" : "고려", "type" : "NNP", "position" : 27, "weight" : 0.042748 },
		{"id" : 6, "lemma" : "말", "type" : "NNB", "position" : 34, "weight" : 0.00152423 },
		{"id" : 7, "lemma" : "부터", "type" : "JX", "position" : 37, "weight" : 0.105319 },
		{"id" : 8, "lemma" : "이어지", "type" : "VV", "position" : 44, "weight" : 0.9 },
		{"id" : 9, "lemma" : "ㄴ", "type" : "ETM", "position" : 50, "weight" : 0.304215 },
		{"id" : 10, "lemma" : "조선", "type" : "NNP", "position" : 54, "weight" : 0.027614 },
		{"id" : 11, "lemma" : "의", "type" : "JKG", "position" : 60, "weight" : 0.0987295 },
		{"id" : 12, "lemma" : "최고", "type" : "NNG", "position" : 64, "weight" : 0.9 },
		{"id" : 13, "lemma" : "교육", "type" : "NNG", "position" : 71, "weight" : 0.9 },
		{"id" : 14, "lemma" : "기관", "type" : "NNG", "position" : 77, "weight" : 0.9 },
		{"id" : 15, "lemma" : "이", "type" : "VCP", "position" : 83, "weight" : 0.0177525 },
		{"id" : 16, "lemma" : "다", "type" : "EF", "position" : 86, "weight" : 0.353579 },
		{"id" : 17, "lemma" : ".", "type" : "SF", "position" : 89, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿성균관/NNG+(/SS+成均館/SH+)/SS+은/JX", "target" : "﻿성균관(成均館)은", "word_id" : 0, "m_begin" : 0, "m_end" : 4},
		{"id" : 1, "result" : "고려/NNG", "target" : "고려", "word_id" : 1, "m_begin" : 5, "m_end" : 5},
		{"id" : 2, "result" : "말/NNB+부터/JX", "target" : "말부터", "word_id" : 2, "m_begin" : 6, "m_end" : 7},
		{"id" : 3, "result" : "이어지/VV+ㄴ/ETM", "target" : "이어진", "word_id" : 3, "m_begin" : 8, "m_end" : 9},
		{"id" : 4, "result" : "조선/NNG+의/JKG", "target" : "조선의", "word_id" : 4, "m_begin" : 10, "m_end" : 11},
		{"id" : 5, "result" : "최고/NNG", "target" : "최고", "word_id" : 5, "m_begin" : 12, "m_end" : 12},
		{"id" : 6, "result" : "교육기관/NNG+이/VCP+다/EF+./SF", "target" : "교육기관이다.", "word_id" : 6, "m_begin" : 13, "m_end" : 17}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿성균관", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 12, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "成均館", "type" : "SH", "scode" : "00", "weight" : 1, "position" : 13, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 22, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 23, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "고려", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 27, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "말", "type" : "NNB", "scode" : "11", "weight" : 1, "position" : 34, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "부터", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 37, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "이어지", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 44, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 50, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "조선", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 54, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 60, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "최고", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 64, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "교육기관", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 71, "begin" : 13, "end" : 14},
		{"id" : 14, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 83, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 86, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 89, "begin" : 17, "end" : 17}
	],
	"word" : [
		{"id" : 0, "text" : "﻿성균관(成均館)은", "type" : "", "begin" : 0, "end" : 4},
		{"id" : 1, "text" : "고려", "type" : "", "begin" : 5, "end" : 5},
		{"id" : 2, "text" : "말부터", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 3, "text" : "이어진", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 4, "text" : "조선의", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 5, "text" : "최고", "type" : "", "begin" : 12, "end" : 12},
		{"id" : 6, "text" : "교육기관이다.", "type" : "", "begin" : 13, "end" : 17}
	],
	"NE" : [
		{"id" : 0, "text" : "﻿성균관", "type" : "PS_NAME", "begin" : 0, "end" : 0, "weight" : 0.138221, "common_noun" : 0},
		{"id" : 1, "text" : "成均館", "type" : "PS_NAME", "begin" : 2, "end" : 2, "weight" : 0.121157, "common_noun" : 0},
		{"id" : 2, "text" : "고려 말", "type" : "DT_DYNASTY", "begin" : 5, "end" : 6, "weight" : 0.763413, "common_noun" : 0},
		{"id" : 3, "text" : "조선", "type" : "LCP_COUNTRY", "begin" : 10, "end" : 10, "weight" : 0.158011, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿성균관(成均館)은", "head" : 3, "label" : "NP_SBJ", "mod" : [], "weight" : 0.566995 },
		{"id" : 1, "text" : "고려", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.998882 },
		{"id" : 2, "text" : "말부터", "head" : 3, "label" : "NP_AJT", "mod" : [1], "weight" : 0.707708 },
		{"id" : 3, "text" : "이어진", "head" : 6, "label" : "VP_MOD", "mod" : [0, 2], "weight" : 0.511372 },
		{"id" : 4, "text" : "조선의", "head" : 6, "label" : "NP_MOD", "mod" : [], "weight" : 0.490292 },
		{"id" : 5, "text" : "최고", "head" : 6, "label" : "NP", "mod" : [], "weight" : 0.389673 },
		{"id" : 6, "text" : "교육기관이다.", "head" : -1, "label" : "VNP", "mod" : [3, 4, 5], "weight" : 0.0238777 }
	],
	"SRL" : [
		{"verb" : "이어지", "sense" : 2, "word_id" : 3, "weight" : 0.270982,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 2, "text" : "말부터", "weight" : 0.270982 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
 ]
}

