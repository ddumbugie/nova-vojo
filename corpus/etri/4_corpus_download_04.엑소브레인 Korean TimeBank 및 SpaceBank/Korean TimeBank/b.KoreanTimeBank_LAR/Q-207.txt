{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿현재 우리나라 지폐에는 시각 장애인을 위해 점자가 표시돼 있다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "SS", "position" : 0, "weight" : 1 },
		{"id" : 1, "lemma" : "현재", "type" : "MAG", "position" : 3, "weight" : 0.0117739 },
		{"id" : 2, "lemma" : "우리", "type" : "NNG", "position" : 10, "weight" : 0.000912158 },
		{"id" : 3, "lemma" : "나라", "type" : "NNG", "position" : 16, "weight" : 0.184431 },
		{"id" : 4, "lemma" : "지폐", "type" : "NNG", "position" : 23, "weight" : 0.9 },
		{"id" : 5, "lemma" : "에", "type" : "JKB", "position" : 29, "weight" : 0.153364 },
		{"id" : 6, "lemma" : "는", "type" : "JX", "position" : 32, "weight" : 0.0387928 },
		{"id" : 7, "lemma" : "시각", "type" : "NNG", "position" : 36, "weight" : 0.9 },
		{"id" : 8, "lemma" : "장애", "type" : "NNG", "position" : 43, "weight" : 0.9 },
		{"id" : 9, "lemma" : "인", "type" : "XSN", "position" : 49, "weight" : 0.0198339 },
		{"id" : 10, "lemma" : "을", "type" : "JKO", "position" : 52, "weight" : 0.0819193 },
		{"id" : 11, "lemma" : "위하", "type" : "VV", "position" : 56, "weight" : 0.778555 },
		{"id" : 12, "lemma" : "어", "type" : "EC", "position" : 59, "weight" : 0.41831 },
		{"id" : 13, "lemma" : "점자", "type" : "NNG", "position" : 63, "weight" : 0.9 },
		{"id" : 14, "lemma" : "가", "type" : "JKS", "position" : 69, "weight" : 0.066324 },
		{"id" : 15, "lemma" : "표시", "type" : "NNG", "position" : 73, "weight" : 0.9 },
		{"id" : 16, "lemma" : "되", "type" : "XSV", "position" : 79, "weight" : 0.000224177 },
		{"id" : 17, "lemma" : "어", "type" : "EC", "position" : 79, "weight" : 0.361326 },
		{"id" : 18, "lemma" : "있", "type" : "VX", "position" : 83, "weight" : 0.125953 },
		{"id" : 19, "lemma" : "다", "type" : "EF", "position" : 86, "weight" : 0.180366 },
		{"id" : 20, "lemma" : ".", "type" : "SF", "position" : 89, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿/SS+현재/MAG", "target" : "﻿현재", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "우리나라/NNG", "target" : "우리나라", "word_id" : 1, "m_begin" : 2, "m_end" : 3},
		{"id" : 2, "result" : "지폐/NNG+에/JKB+는/JX", "target" : "지폐에는", "word_id" : 2, "m_begin" : 4, "m_end" : 6},
		{"id" : 3, "result" : "시각/NNG", "target" : "시각", "word_id" : 3, "m_begin" : 7, "m_end" : 7},
		{"id" : 4, "result" : "장애인/NNG+을/JKO", "target" : "장애인을", "word_id" : 4, "m_begin" : 8, "m_end" : 10},
		{"id" : 5, "result" : "위하/VV+어/EC", "target" : "위해", "word_id" : 5, "m_begin" : 11, "m_end" : 12},
		{"id" : 6, "result" : "점자/NNG+가/JKS", "target" : "점자가", "word_id" : 6, "m_begin" : 13, "m_end" : 14},
		{"id" : 7, "result" : "표시되/VV+어/EC", "target" : "표시돼", "word_id" : 7, "m_begin" : 15, "m_end" : 17},
		{"id" : 8, "result" : "있/VX+다/EF+./SF", "target" : "있다.", "word_id" : 8, "m_begin" : 18, "m_end" : 20}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "현재", "type" : "MAG", "scode" : "02", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "우리나라", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 2, "end" : 3},
		{"id" : 3, "text" : "지폐", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 23, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 29, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 32, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "시각", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 36, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "장애인", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 43, "begin" : 8, "end" : 9},
		{"id" : 8, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 52, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "위하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 56, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 59, "begin" : 12, "end" : 12},
		{"id" : 11, "text" : "점자", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 63, "begin" : 13, "end" : 13},
		{"id" : 12, "text" : "가", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 69, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "표시되", "type" : "VV", "scode" : "02", "weight" : 1, "position" : 73, "begin" : 15, "end" : 16},
		{"id" : 14, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 79, "begin" : 17, "end" : 17},
		{"id" : 15, "text" : "있", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 83, "begin" : 18, "end" : 18},
		{"id" : 16, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 86, "begin" : 19, "end" : 19},
		{"id" : 17, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 89, "begin" : 20, "end" : 20}
	],
	"word" : [
		{"id" : 0, "text" : "﻿현재", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "우리나라", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 2, "text" : "지폐에는", "type" : "", "begin" : 4, "end" : 6},
		{"id" : 3, "text" : "시각", "type" : "", "begin" : 7, "end" : 7},
		{"id" : 4, "text" : "장애인을", "type" : "", "begin" : 8, "end" : 10},
		{"id" : 5, "text" : "위해", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 6, "text" : "점자가", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 7, "text" : "표시돼", "type" : "", "begin" : 15, "end" : 17},
		{"id" : 8, "text" : "있다.", "type" : "", "begin" : 18, "end" : 20}
	],
	"NE" : [
		{"id" : 0, "text" : "장애인", "type" : "CV_POSITION", "begin" : 8, "end" : 9, "weight" : 0.389379, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿현재", "head" : 7, "label" : "AP", "mod" : [], "weight" : 0.716356 },
		{"id" : 1, "text" : "우리나라", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.960291 },
		{"id" : 2, "text" : "지폐에는", "head" : 7, "label" : "NP_AJT", "mod" : [1], "weight" : 0.741204 },
		{"id" : 3, "text" : "시각", "head" : 4, "label" : "NP", "mod" : [], "weight" : 0.714194 },
		{"id" : 4, "text" : "장애인을", "head" : 5, "label" : "NP_OBJ", "mod" : [3], "weight" : 0.906046 },
		{"id" : 5, "text" : "위해", "head" : 7, "label" : "VP", "mod" : [4], "weight" : 0.77524 },
		{"id" : 6, "text" : "점자가", "head" : 7, "label" : "NP_SBJ", "mod" : [], "weight" : 0.636997 },
		{"id" : 7, "text" : "표시돼", "head" : 8, "label" : "VP", "mod" : [0, 2, 5, 6], "weight" : 0.507366 },
		{"id" : 8, "text" : "있다.", "head" : -1, "label" : "VP", "mod" : [7], "weight" : 0.0664098 }
	],
	"SRL" : [
		{"verb" : "위하", "sense" : 1, "word_id" : 5, "weight" : 0.463532,
			"argument" : [
				{"type" : "ARG1", "word_id" : 4, "text" : "장애인을", "weight" : 0.463532 }
			] },
		{"verb" : "표시", "sense" : 2, "word_id" : 7, "weight" : 0.311905,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 2, "text" : "지폐에는", "weight" : 0.149771 },
				{"type" : "ARGM-PRP", "word_id" : 5, "text" : "위해", "weight" : 0.189667 },
				{"type" : "ARG1", "word_id" : 6, "text" : "점자가", "weight" : 0.332432 },
				{"type" : "AUX", "word_id" : 8, "text" : "있다.", "weight" : 0.575749 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 5, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.776106 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "만 원권 지폐에는 몇 개의 점자가 있을까? ",
	"morp" : [
		{"id" : 0, "lemma" : "만", "type" : "NR", "position" : 91, "weight" : 0.000783601 },
		{"id" : 1, "lemma" : "원", "type" : "NNB", "position" : 95, "weight" : 0.318791 },
		{"id" : 2, "lemma" : "권", "type" : "XSN", "position" : 98, "weight" : 0.00918236 },
		{"id" : 3, "lemma" : "지폐", "type" : "NNG", "position" : 102, "weight" : 0.9 },
		{"id" : 4, "lemma" : "에", "type" : "JKB", "position" : 108, "weight" : 0.153364 },
		{"id" : 5, "lemma" : "는", "type" : "JX", "position" : 111, "weight" : 0.0387928 },
		{"id" : 6, "lemma" : "몇", "type" : "MM", "position" : 115, "weight" : 0.0399668 },
		{"id" : 7, "lemma" : "개", "type" : "NNB", "position" : 119, "weight" : 0.157893 },
		{"id" : 8, "lemma" : "의", "type" : "JKG", "position" : 122, "weight" : 0.0520933 },
		{"id" : 9, "lemma" : "점자", "type" : "NNG", "position" : 126, "weight" : 0.9 },
		{"id" : 10, "lemma" : "가", "type" : "JKS", "position" : 132, "weight" : 0.066324 },
		{"id" : 11, "lemma" : "있", "type" : "VA", "position" : 136, "weight" : 0.12767 },
		{"id" : 12, "lemma" : "을까", "type" : "EF", "position" : 139, "weight" : 0.10243 },
		{"id" : 13, "lemma" : "?", "type" : "SF", "position" : 145, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "만/NR", "target" : "만", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "원권/NNB", "target" : "원권", "word_id" : 1, "m_begin" : 1, "m_end" : 2},
		{"id" : 2, "result" : "지폐/NNG+에/JKB+는/JX", "target" : "지폐에는", "word_id" : 2, "m_begin" : 3, "m_end" : 5},
		{"id" : 3, "result" : "몇/MM", "target" : "몇", "word_id" : 3, "m_begin" : 6, "m_end" : 6},
		{"id" : 4, "result" : "개/NNB+의/JKG", "target" : "개의", "word_id" : 4, "m_begin" : 7, "m_end" : 8},
		{"id" : 5, "result" : "점자/NNG+가/JKS", "target" : "점자가", "word_id" : 5, "m_begin" : 9, "m_end" : 10},
		{"id" : 6, "result" : "있/VA+을까/EF+?/SF", "target" : "있을까?", "word_id" : 6, "m_begin" : 11, "m_end" : 13}
	],
	"WSD" : [
		{"id" : 0, "text" : "만", "type" : "NR", "scode" : "06", "weight" : 1, "position" : 91, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "원", "type" : "NNB", "scode" : "01", "weight" : 1, "position" : 95, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "권", "type" : "XSN", "scode" : "06", "weight" : 1, "position" : 98, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "지폐", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 102, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 108, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 111, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "몇", "type" : "MM", "scode" : "00", "weight" : 1, "position" : 115, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "개", "type" : "NNB", "scode" : "10", "weight" : 1, "position" : 119, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 122, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "점자", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 126, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "가", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 132, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "있", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 136, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "을까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 139, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 145, "begin" : 13, "end" : 13}
	],
	"word" : [
		{"id" : 0, "text" : "만", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "원권", "type" : "", "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "지폐에는", "type" : "", "begin" : 3, "end" : 5},
		{"id" : 3, "text" : "몇", "type" : "", "begin" : 6, "end" : 6},
		{"id" : 4, "text" : "개의", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 5, "text" : "점자가", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 6, "text" : "있을까?", "type" : "", "begin" : 11, "end" : 13}
	],
	"NE" : [
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "만", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.819029 },
		{"id" : 1, "text" : "원권", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.973383 },
		{"id" : 2, "text" : "지폐에는", "head" : 6, "label" : "NP_AJT", "mod" : [1], "weight" : 0.726215 },
		{"id" : 3, "text" : "몇", "head" : 4, "label" : "DP", "mod" : [], "weight" : 0.792862 },
		{"id" : 4, "text" : "개의", "head" : 5, "label" : "NP_MOD", "mod" : [3], "weight" : 0.733619 },
		{"id" : 5, "text" : "점자가", "head" : 6, "label" : "NP_SBJ", "mod" : [4], "weight" : 0.553383 },
		{"id" : 6, "text" : "있을까?", "head" : -1, "label" : "VP", "mod" : [2, 5], "weight" : 0.143008 }
	],
	"SRL" : [
		{"verb" : "있", "sense" : 1, "word_id" : 6, "weight" : 0.286063,
			"argument" : [
				{"type" : "ARG2", "word_id" : 2, "text" : "지폐에는", "weight" : 0.174503 },
				{"type" : "ARG1", "word_id" : 5, "text" : "점자가", "weight" : 0.397623 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 1, "end_eid" : 2, "ne_id" : -1, "text" : "우리나라 지폐", "start_eid_short" : 1, "end_eid_short" : 2, "text_short" : "우리나라 지폐", "weight" : 0.004 },
		{"id" : 6, "sent_id" : 1, "start_eid" : 0, "end_eid" : 2, "ne_id" : -1, "text" : "만 원권 지폐", "start_eid_short" : 0, "end_eid_short" : 2, "text_short" : "만 원권 지폐", "weight" : 0.004 }
	] },
	{"id" : 1, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 4, "sent_id" : 0, "start_eid" : 6, "end_eid" : 6, "ne_id" : -1, "text" : "점자", "start_eid_short" : 6, "end_eid_short" : 6, "text_short" : "점자", "weight" : 0.004 },
		{"id" : 9, "sent_id" : 1, "start_eid" : 3, "end_eid" : 5, "ne_id" : -1, "text" : "몇 개의 점자", "start_eid_short" : 3, "end_eid_short" : 5, "text_short" : "몇 개의 점자", "weight" : 0.004 }
	] }
 ]
}

