{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "윤미경(1980년 10월 14일 ~)은 대한민국의 만화가이다.",
	"morp" : [
		{"id" : 0, "lemma" : "윤미경", "type" : "NNP", "position" : 0, "weight" : 0.2 },
		{"id" : 1, "lemma" : "(", "type" : "SS", "position" : 9, "weight" : 1 },
		{"id" : 2, "lemma" : "1980", "type" : "SN", "position" : 10, "weight" : 1 },
		{"id" : 3, "lemma" : "년", "type" : "NNB", "position" : 14, "weight" : 0.414343 },
		{"id" : 4, "lemma" : "10", "type" : "SN", "position" : 18, "weight" : 1 },
		{"id" : 5, "lemma" : "월", "type" : "NNB", "position" : 20, "weight" : 0.408539 },
		{"id" : 6, "lemma" : "14", "type" : "SN", "position" : 24, "weight" : 1 },
		{"id" : 7, "lemma" : "일", "type" : "NNB", "position" : 26, "weight" : 0.126777 },
		{"id" : 8, "lemma" : "~", "type" : "SO", "position" : 30, "weight" : 1 },
		{"id" : 9, "lemma" : ")", "type" : "SS", "position" : 31, "weight" : 1 },
		{"id" : 10, "lemma" : "은", "type" : "JX", "position" : 32, "weight" : 0.0128817 },
		{"id" : 11, "lemma" : "대한민국", "type" : "NNP", "position" : 36, "weight" : 0.0447775 },
		{"id" : 12, "lemma" : "의", "type" : "JKG", "position" : 48, "weight" : 0.0987295 },
		{"id" : 13, "lemma" : "만화", "type" : "NNG", "position" : 52, "weight" : 0.83848 },
		{"id" : 14, "lemma" : "가", "type" : "XSN", "position" : 58, "weight" : 0.000115417 },
		{"id" : 15, "lemma" : "이", "type" : "VCP", "position" : 61, "weight" : 0.0165001 },
		{"id" : 16, "lemma" : "다", "type" : "EF", "position" : 64, "weight" : 0.353579 },
		{"id" : 17, "lemma" : ".", "type" : "SF", "position" : 67, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "윤미경/NNG+(/SS+1980/SN+년/NNB", "target" : "윤미경(1980년", "word_id" : 0, "m_begin" : 0, "m_end" : 3},
		{"id" : 1, "result" : "10/SN+월/NNB", "target" : "10월", "word_id" : 1, "m_begin" : 4, "m_end" : 5},
		{"id" : 2, "result" : "14/SN+일/NNB", "target" : "14일", "word_id" : 2, "m_begin" : 6, "m_end" : 7},
		{"id" : 3, "result" : "~/SO+)/SS+은/JX", "target" : "~)은", "word_id" : 3, "m_begin" : 8, "m_end" : 10},
		{"id" : 4, "result" : "대한민국/NNG+의/JKG", "target" : "대한민국의", "word_id" : 4, "m_begin" : 11, "m_end" : 12},
		{"id" : 5, "result" : "만화가/NNG+이/VCP+다/EF+./SF", "target" : "만화가이다.", "word_id" : 5, "m_begin" : 13, "m_end" : 17}
	],
	"WSD" : [
		{"id" : 0, "text" : "윤미경", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "1980", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 14, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "10", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 18, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "월", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 20, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "14", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 24, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "일", "type" : "NNB", "scode" : "07", "weight" : 1, "position" : 26, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "~", "type" : "SO", "scode" : "00", "weight" : 1, "position" : 30, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 31, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 32, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "대한민국", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 36, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 48, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "만화가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 52, "begin" : 13, "end" : 14},
		{"id" : 14, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 61, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 64, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 67, "begin" : 17, "end" : 17}
	],
	"word" : [
		{"id" : 0, "text" : "윤미경(1980년", "type" : "", "begin" : 0, "end" : 3},
		{"id" : 1, "text" : "10월", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 2, "text" : "14일", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 3, "text" : "~)은", "type" : "", "begin" : 8, "end" : 10},
		{"id" : 4, "text" : "대한민국의", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 5, "text" : "만화가이다.", "type" : "", "begin" : 13, "end" : 17}
	],
	"NE" : [
		{"id" : 0, "text" : "윤미경", "type" : "PS_NAME", "begin" : 0, "end" : 0, "weight" : 0.648813, "common_noun" : 0},
		{"id" : 1, "text" : "1980년 10월 14일 ~", "type" : "DT_DURATION", "begin" : 2, "end" : 8, "weight" : 0.772053, "common_noun" : 0},
		{"id" : 2, "text" : "대한민국", "type" : "LCP_COUNTRY", "begin" : 11, "end" : 11, "weight" : 0.190418, "common_noun" : 0},
		{"id" : 3, "text" : "만화가", "type" : "CV_OCCUPATION", "begin" : 13, "end" : 14, "weight" : 0.285798, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "윤미경(1980년", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.615857 },
		{"id" : 1, "text" : "10월", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.783765 },
		{"id" : 2, "text" : "14일", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.640086 },
		{"id" : 3, "text" : "~)은", "head" : 5, "label" : "NP_SBJ", "mod" : [2], "weight" : 0.61224 },
		{"id" : 4, "text" : "대한민국의", "head" : 5, "label" : "NP_MOD", "mod" : [], "weight" : 0.426796 },
		{"id" : 5, "text" : "만화가이다.", "head" : -1, "label" : "VNP", "mod" : [3, 4], "weight" : 0.0470869 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "2003년 하반기 서울문화사 신인공모전에서 단편〈나의 지구 방문기〉로 은상을 수상하였다.",
	"morp" : [
		{"id" : 0, "lemma" : "2003", "type" : "SN", "position" : 68, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 72, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "하", "type" : "NNG", "position" : 76, "weight" : 0.0001 },
		{"id" : 3, "lemma" : "반기", "type" : "NNG", "position" : 79, "weight" : 0.0454214 },
		{"id" : 4, "lemma" : "서울문화사", "type" : "NNP", "position" : 86, "weight" : 0.1 },
		{"id" : 5, "lemma" : "신인", "type" : "NNG", "position" : 102, "weight" : 0.9 },
		{"id" : 6, "lemma" : "공모", "type" : "NNG", "position" : 108, "weight" : 0.9 },
		{"id" : 7, "lemma" : "전", "type" : "XSN", "position" : 114, "weight" : 0.00343717 },
		{"id" : 8, "lemma" : "에서", "type" : "JKB", "position" : 117, "weight" : 0.121718 },
		{"id" : 9, "lemma" : "단편", "type" : "NNG", "position" : 124, "weight" : 0.9 },
		{"id" : 10, "lemma" : "〈", "type" : "SS", "position" : 130, "weight" : 1 },
		{"id" : 11, "lemma" : "나", "type" : "NP", "position" : 133, "weight" : 0.0093447 },
		{"id" : 12, "lemma" : "의", "type" : "JKG", "position" : 136, "weight" : 0.134696 },
		{"id" : 13, "lemma" : "지구", "type" : "NNG", "position" : 140, "weight" : 0.9 },
		{"id" : 14, "lemma" : "방문", "type" : "NNG", "position" : 147, "weight" : 0.9 },
		{"id" : 15, "lemma" : "기", "type" : "XSN", "position" : 153, "weight" : 0.000294499 },
		{"id" : 16, "lemma" : "〉", "type" : "SS", "position" : 156, "weight" : 1 },
		{"id" : 17, "lemma" : "로", "type" : "JKB", "position" : 159, "weight" : 0.0485504 },
		{"id" : 18, "lemma" : "은상", "type" : "NNP", "position" : 163, "weight" : 0.00605787 },
		{"id" : 19, "lemma" : "을", "type" : "JKO", "position" : 169, "weight" : 0.0266447 },
		{"id" : 20, "lemma" : "수상", "type" : "NNG", "position" : 173, "weight" : 0.9 },
		{"id" : 21, "lemma" : "하", "type" : "XSV", "position" : 179, "weight" : 0.0001 },
		{"id" : 22, "lemma" : "었", "type" : "EP", "position" : 182, "weight" : 0.9 },
		{"id" : 23, "lemma" : "다", "type" : "EF", "position" : 185, "weight" : 0.640954 },
		{"id" : 24, "lemma" : ".", "type" : "SF", "position" : 188, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "2003/SN+년/NNB", "target" : "2003년", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "하반기/NNG", "target" : "하반기", "word_id" : 1, "m_begin" : 2, "m_end" : 3},
		{"id" : 2, "result" : "서울문화사/NNG", "target" : "서울문화사", "word_id" : 2, "m_begin" : 4, "m_end" : 4},
		{"id" : 3, "result" : "신인공모전/NNG+에서/JKB", "target" : "신인공모전에서", "word_id" : 3, "m_begin" : 5, "m_end" : 8},
		{"id" : 4, "result" : "단편/NNG+〈/SS+나/NP+의/JKG", "target" : "단편〈나의", "word_id" : 4, "m_begin" : 9, "m_end" : 12},
		{"id" : 5, "result" : "지구/NNG", "target" : "지구", "word_id" : 5, "m_begin" : 13, "m_end" : 13},
		{"id" : 6, "result" : "방문기/NNG+〉/SS+로/JKB", "target" : "방문기〉로", "word_id" : 6, "m_begin" : 14, "m_end" : 17},
		{"id" : 7, "result" : "은상/NNG+을/JKO", "target" : "은상을", "word_id" : 7, "m_begin" : 18, "m_end" : 19},
		{"id" : 8, "result" : "수상하/VV+었/EP+다/EF+./SF", "target" : "수상하였다.", "word_id" : 8, "m_begin" : 20, "m_end" : 24}
	],
	"WSD" : [
		{"id" : 0, "text" : "2003", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 68, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 72, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "하반기", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 76, "begin" : 2, "end" : 3},
		{"id" : 3, "text" : "서울문화사", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 86, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "신인", "type" : "NNG", "scode" : "07", "weight" : 1, "position" : 102, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "공모전", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 108, "begin" : 6, "end" : 7},
		{"id" : 6, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 117, "begin" : 8, "end" : 8},
		{"id" : 7, "text" : "단편", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 124, "begin" : 9, "end" : 9},
		{"id" : 8, "text" : "〈", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 130, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "나", "type" : "NP", "scode" : "03", "weight" : 1, "position" : 133, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 136, "begin" : 12, "end" : 12},
		{"id" : 11, "text" : "지구", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 140, "begin" : 13, "end" : 13},
		{"id" : 12, "text" : "방문기", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 147, "begin" : 14, "end" : 15},
		{"id" : 13, "text" : "〉", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 156, "begin" : 16, "end" : 16},
		{"id" : 14, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 159, "begin" : 17, "end" : 17},
		{"id" : 15, "text" : "은상", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 163, "begin" : 18, "end" : 18},
		{"id" : 16, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 169, "begin" : 19, "end" : 19},
		{"id" : 17, "text" : "수상하", "type" : "VV", "scode" : "03", "weight" : 1, "position" : 173, "begin" : 20, "end" : 21},
		{"id" : 18, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 182, "begin" : 22, "end" : 22},
		{"id" : 19, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 185, "begin" : 23, "end" : 23},
		{"id" : 20, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 188, "begin" : 24, "end" : 24}
	],
	"word" : [
		{"id" : 0, "text" : "2003년", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "하반기", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 2, "text" : "서울문화사", "type" : "", "begin" : 4, "end" : 4},
		{"id" : 3, "text" : "신인공모전에서", "type" : "", "begin" : 5, "end" : 8},
		{"id" : 4, "text" : "단편〈나의", "type" : "", "begin" : 9, "end" : 12},
		{"id" : 5, "text" : "지구", "type" : "", "begin" : 13, "end" : 13},
		{"id" : 6, "text" : "방문기〉로", "type" : "", "begin" : 14, "end" : 17},
		{"id" : 7, "text" : "은상을", "type" : "", "begin" : 18, "end" : 19},
		{"id" : 8, "text" : "수상하였다.", "type" : "", "begin" : 20, "end" : 24}
	],
	"NE" : [
		{"id" : 0, "text" : "2003년 하반기", "type" : "DT_DURATION", "begin" : 0, "end" : 3, "weight" : 0.743204, "common_noun" : 0},
		{"id" : 1, "text" : "서울문화사", "type" : "OGG_ECONOMY", "begin" : 4, "end" : 4, "weight" : 0.227643, "common_noun" : 0},
		{"id" : 2, "text" : "신인", "type" : "CV_POSITION", "begin" : 5, "end" : 5, "weight" : 0.205268, "common_noun" : 0},
		{"id" : 3, "text" : "나의 지구 방문기", "type" : "AFW_DOCUMENT", "begin" : 11, "end" : 15, "weight" : 0.144172, "common_noun" : 0},
		{"id" : 4, "text" : "은상", "type" : "CV_PRIZE", "begin" : 18, "end" : 18, "weight" : 0.365638, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "2003년", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.548342 },
		{"id" : 1, "text" : "하반기", "head" : 3, "label" : "NP", "mod" : [0], "weight" : 0.562069 },
		{"id" : 2, "text" : "서울문화사", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.918609 },
		{"id" : 3, "text" : "신인공모전에서", "head" : 6, "label" : "NP_AJT", "mod" : [1, 2], "weight" : 0.565921 },
		{"id" : 4, "text" : "단편〈나의", "head" : 6, "label" : "NP_MOD", "mod" : [], "weight" : 0.866151 },
		{"id" : 5, "text" : "지구", "head" : 6, "label" : "NP", "mod" : [], "weight" : 0.779885 },
		{"id" : 6, "text" : "방문기〉로", "head" : 8, "label" : "NP_AJT", "mod" : [3, 4, 5], "weight" : 0.646601 },
		{"id" : 7, "text" : "은상을", "head" : 8, "label" : "NP_OBJ", "mod" : [], "weight" : 0.546823 },
		{"id" : 8, "text" : "수상하였다.", "head" : -1, "label" : "VP", "mod" : [6, 7], "weight" : 0.0235611 }
	],
	"SRL" : [
		{"verb" : "수상", "sense" : 1, "word_id" : 8, "weight" : 0.113357,
			"argument" : [
				{"type" : "ARGM-INS", "word_id" : 6, "text" : "방문기〉로", "weight" : 0.0710842 },
				{"type" : "ARG1", "word_id" : 7, "text" : "은상을", "weight" : 0.15563 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 8, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.57401 }
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "《윙크》에서 주로 활동하고 있으며 2012년 현재 《하백의 신부》를 연재중이다.",
	"morp" : [
		{"id" : 0, "lemma" : "《", "type" : "SS", "position" : 189, "weight" : 1 },
		{"id" : 1, "lemma" : "윙크", "type" : "NNG", "position" : 192, "weight" : 0.9 },
		{"id" : 2, "lemma" : "》", "type" : "SS", "position" : 198, "weight" : 1 },
		{"id" : 3, "lemma" : "에서", "type" : "JKB", "position" : 201, "weight" : 0.0486066 },
		{"id" : 4, "lemma" : "주로", "type" : "MAG", "position" : 208, "weight" : 0.0557734 },
		{"id" : 5, "lemma" : "활동", "type" : "NNG", "position" : 215, "weight" : 0.9 },
		{"id" : 6, "lemma" : "하", "type" : "XSV", "position" : 221, "weight" : 0.0001 },
		{"id" : 7, "lemma" : "고", "type" : "EC", "position" : 224, "weight" : 0.359917 },
		{"id" : 8, "lemma" : "있", "type" : "VX", "position" : 228, "weight" : 0.125953 },
		{"id" : 9, "lemma" : "으며", "type" : "EC", "position" : 231, "weight" : 0.265281 },
		{"id" : 10, "lemma" : "2012", "type" : "SN", "position" : 238, "weight" : 1 },
		{"id" : 11, "lemma" : "년", "type" : "NNB", "position" : 242, "weight" : 0.414343 },
		{"id" : 12, "lemma" : "현재", "type" : "MAG", "position" : 246, "weight" : 0.00662512 },
		{"id" : 13, "lemma" : "《", "type" : "SS", "position" : 253, "weight" : 1 },
		{"id" : 14, "lemma" : "하백", "type" : "NNG", "position" : 256, "weight" : 0.0307694 },
		{"id" : 15, "lemma" : "의", "type" : "JKG", "position" : 262, "weight" : 0.0694213 },
		{"id" : 16, "lemma" : "신부", "type" : "NNG", "position" : 266, "weight" : 0.870993 },
		{"id" : 17, "lemma" : "》", "type" : "SS", "position" : 272, "weight" : 1 },
		{"id" : 18, "lemma" : "를", "type" : "JKO", "position" : 275, "weight" : 0.0357023 },
		{"id" : 19, "lemma" : "연재", "type" : "NNG", "position" : 279, "weight" : 0.0975025 },
		{"id" : 20, "lemma" : "중", "type" : "NNB", "position" : 285, "weight" : 0.013531 },
		{"id" : 21, "lemma" : "이", "type" : "VCP", "position" : 288, "weight" : 0.0607843 },
		{"id" : 22, "lemma" : "다", "type" : "EF", "position" : 291, "weight" : 0.353579 },
		{"id" : 23, "lemma" : ".", "type" : "SF", "position" : 294, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "《/SS+윙크/NNG+》/SS+에서/JKB", "target" : "《윙크》에서", "word_id" : 0, "m_begin" : 0, "m_end" : 3},
		{"id" : 1, "result" : "주로/MAG", "target" : "주로", "word_id" : 1, "m_begin" : 4, "m_end" : 4},
		{"id" : 2, "result" : "활동하/VV+고/EC", "target" : "활동하고", "word_id" : 2, "m_begin" : 5, "m_end" : 7},
		{"id" : 3, "result" : "있/VX+으며/EC", "target" : "있으며", "word_id" : 3, "m_begin" : 8, "m_end" : 9},
		{"id" : 4, "result" : "2012/SN+년/NNB", "target" : "2012년", "word_id" : 4, "m_begin" : 10, "m_end" : 11},
		{"id" : 5, "result" : "현재/MAG", "target" : "현재", "word_id" : 5, "m_begin" : 12, "m_end" : 12},
		{"id" : 6, "result" : "《/SS+하백/NNG+의/JKG", "target" : "《하백의", "word_id" : 6, "m_begin" : 13, "m_end" : 15},
		{"id" : 7, "result" : "신부/NNG+》/SS+를/JKO", "target" : "신부》를", "word_id" : 7, "m_begin" : 16, "m_end" : 18},
		{"id" : 8, "result" : "연재/NNG+중/NNB+이/VCP+다/EF+./SF", "target" : "연재중이다.", "word_id" : 8, "m_begin" : 19, "m_end" : 23}
	],
	"WSD" : [
		{"id" : 0, "text" : "《", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 189, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "윙크", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 192, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "》", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 198, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 201, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "주로", "type" : "MAG", "scode" : "01", "weight" : 1, "position" : 208, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "활동하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 215, "begin" : 5, "end" : 6},
		{"id" : 6, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 224, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "있", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 228, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "으며", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 231, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "2012", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 238, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 242, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "현재", "type" : "MAG", "scode" : "02", "weight" : 1, "position" : 246, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "《", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 253, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "하백", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 256, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 262, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "신부", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 266, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "》", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 272, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 275, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "연재", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 279, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "중", "type" : "NNB", "scode" : "04", "weight" : 1, "position" : 285, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 288, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 291, "begin" : 22, "end" : 22},
		{"id" : 22, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 294, "begin" : 23, "end" : 23}
	],
	"word" : [
		{"id" : 0, "text" : "《윙크》에서", "type" : "", "begin" : 0, "end" : 3},
		{"id" : 1, "text" : "주로", "type" : "", "begin" : 4, "end" : 4},
		{"id" : 2, "text" : "활동하고", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 3, "text" : "있으며", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 4, "text" : "2012년", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 5, "text" : "현재", "type" : "", "begin" : 12, "end" : 12},
		{"id" : 6, "text" : "《하백의", "type" : "", "begin" : 13, "end" : 15},
		{"id" : 7, "text" : "신부》를", "type" : "", "begin" : 16, "end" : 18},
		{"id" : 8, "text" : "연재중이다.", "type" : "", "begin" : 19, "end" : 23}
	],
	"NE" : [
		{"id" : 0, "text" : "윙크", "type" : "AF_WORKS", "begin" : 1, "end" : 1, "weight" : 0.579702, "common_noun" : 0},
		{"id" : 1, "text" : "2012년", "type" : "DT_YEAR", "begin" : 10, "end" : 11, "weight" : 0.769418, "common_noun" : 0},
		{"id" : 2, "text" : "하백의 신부", "type" : "AFW_DOCUMENT", "begin" : 14, "end" : 16, "weight" : 0.883092, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "《윙크》에서", "head" : 2, "label" : "NP_AJT", "mod" : [], "weight" : 0.9758 },
		{"id" : 1, "text" : "주로", "head" : 2, "label" : "AP", "mod" : [], "weight" : 0.619816 },
		{"id" : 2, "text" : "활동하고", "head" : 3, "label" : "VP", "mod" : [0, 1], "weight" : 0.848127 },
		{"id" : 3, "text" : "있으며", "head" : 8, "label" : "VP", "mod" : [2], "weight" : 0.634235 },
		{"id" : 4, "text" : "2012년", "head" : 5, "label" : "NP", "mod" : [], "weight" : 0.549736 },
		{"id" : 5, "text" : "현재", "head" : 8, "label" : "AP", "mod" : [4], "weight" : 0.58514 },
		{"id" : 6, "text" : "《하백의", "head" : 7, "label" : "NP_MOD", "mod" : [], "weight" : 0.783671 },
		{"id" : 7, "text" : "신부》를", "head" : 8, "label" : "NP_OBJ", "mod" : [6], "weight" : 0.395549 },
		{"id" : 8, "text" : "연재중이다.", "head" : -1, "label" : "VNP", "mod" : [3, 5, 7], "weight" : 0.0168468 }
	],
	"SRL" : [
		{"verb" : "활동", "sense" : 1, "word_id" : 2, "weight" : 0.328669,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 0, "text" : "《윙크》에서", "weight" : 0.136969 },
				{"type" : "ARGM-ADV", "word_id" : 1, "text" : "주로", "weight" : 0.315097 },
				{"type" : "AUX", "word_id" : 3, "text" : "있으며", "weight" : 0.533942 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 2, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.406957 },
		{"id" : 1, "verb_wid" : 8, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.435262 }
	]
	}
 ],
 "entity" : [
 ]
}

