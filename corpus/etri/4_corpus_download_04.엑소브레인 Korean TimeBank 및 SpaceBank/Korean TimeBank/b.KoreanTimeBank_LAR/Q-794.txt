{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿이 사람의 자는 '중모'로 중국 삼국시대 오나라의 초대황제는 누구일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "이", "type" : "JKS", "position" : 3, "weight" : 0.0234517 },
		{"id" : 2, "lemma" : "사람", "type" : "NNG", "position" : 7, "weight" : 0.209238 },
		{"id" : 3, "lemma" : "의", "type" : "JKG", "position" : 13, "weight" : 0.0694213 },
		{"id" : 4, "lemma" : "자", "type" : "NNG", "position" : 17, "weight" : 0.1046 },
		{"id" : 5, "lemma" : "는", "type" : "JX", "position" : 20, "weight" : 0.0287565 },
		{"id" : 6, "lemma" : "'", "type" : "SS", "position" : 24, "weight" : 1 },
		{"id" : 7, "lemma" : "중모", "type" : "NNG", "position" : 25, "weight" : 0.133334 },
		{"id" : 8, "lemma" : "'", "type" : "SS", "position" : 31, "weight" : 1 },
		{"id" : 9, "lemma" : "로", "type" : "JKB", "position" : 32, "weight" : 0.0485504 },
		{"id" : 10, "lemma" : "중국", "type" : "NNP", "position" : 36, "weight" : 0.0230121 },
		{"id" : 11, "lemma" : "삼국", "type" : "NNG", "position" : 43, "weight" : 0.9 },
		{"id" : 12, "lemma" : "시대", "type" : "NNG", "position" : 49, "weight" : 0.9 },
		{"id" : 13, "lemma" : "오", "type" : "NNP", "position" : 56, "weight" : 0.0001 },
		{"id" : 14, "lemma" : "나라", "type" : "NNG", "position" : 59, "weight" : 0.281369 },
		{"id" : 15, "lemma" : "의", "type" : "JKG", "position" : 65, "weight" : 0.0694213 },
		{"id" : 16, "lemma" : "초대", "type" : "NNG", "position" : 69, "weight" : 0.9 },
		{"id" : 17, "lemma" : "황제", "type" : "NNG", "position" : 75, "weight" : 0.184355 },
		{"id" : 18, "lemma" : "는", "type" : "JX", "position" : 81, "weight" : 0.0287565 },
		{"id" : 19, "lemma" : "누구", "type" : "NP", "position" : 85, "weight" : 0.9 },
		{"id" : 20, "lemma" : "이", "type" : "VCP", "position" : 91, "weight" : 0.0175768 },
		{"id" : 21, "lemma" : "ㄹ까", "type" : "EF", "position" : 91, "weight" : 0.258243 },
		{"id" : 22, "lemma" : "?", "type" : "SF", "position" : 97, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿/NNG+이/JKS", "target" : "﻿이", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "사람/NNG+의/JKG", "target" : "사람의", "word_id" : 1, "m_begin" : 2, "m_end" : 3},
		{"id" : 2, "result" : "자/NNG+는/JX", "target" : "자는", "word_id" : 2, "m_begin" : 4, "m_end" : 5},
		{"id" : 3, "result" : "'/SS+중모/NNG+'/SS+로/JKB", "target" : "'중모'로", "word_id" : 3, "m_begin" : 6, "m_end" : 9},
		{"id" : 4, "result" : "중국/NNG", "target" : "중국", "word_id" : 4, "m_begin" : 10, "m_end" : 10},
		{"id" : 5, "result" : "삼국시대/NNG", "target" : "삼국시대", "word_id" : 5, "m_begin" : 11, "m_end" : 12},
		{"id" : 6, "result" : "오나라/NNG+의/JKG", "target" : "오나라의", "word_id" : 6, "m_begin" : 13, "m_end" : 15},
		{"id" : 7, "result" : "초대황제/NNG+는/JX", "target" : "초대황제는", "word_id" : 7, "m_begin" : 16, "m_end" : 18},
		{"id" : 8, "result" : "누구/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "누구일까?", "word_id" : 8, "m_begin" : 19, "m_end" : 22}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "사람", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 7, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 13, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "자", "type" : "NNG", "scode" : "14", "weight" : 1, "position" : 17, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 20, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 24, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "중모", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 25, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 31, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 32, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "중국", "type" : "NNP", "scode" : "01", "weight" : 1, "position" : 36, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "삼국시대", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 43, "begin" : 11, "end" : 12},
		{"id" : 12, "text" : "오나라", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 56, "begin" : 13, "end" : 14},
		{"id" : 13, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 65, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : "초대", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 69, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : "황제", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 75, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 81, "begin" : 18, "end" : 18},
		{"id" : 17, "text" : "누구", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 85, "begin" : 19, "end" : 19},
		{"id" : 18, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 91, "begin" : 20, "end" : 20},
		{"id" : 19, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 91, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 97, "begin" : 22, "end" : 22}
	],
	"word" : [
		{"id" : 0, "text" : "﻿이", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "사람의", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 2, "text" : "자는", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 3, "text" : "'중모'로", "type" : "", "begin" : 6, "end" : 9},
		{"id" : 4, "text" : "중국", "type" : "", "begin" : 10, "end" : 10},
		{"id" : 5, "text" : "삼국시대", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 6, "text" : "오나라의", "type" : "", "begin" : 13, "end" : 15},
		{"id" : 7, "text" : "초대황제는", "type" : "", "begin" : 16, "end" : 18},
		{"id" : 8, "text" : "누구일까?", "type" : "", "begin" : 19, "end" : 22}
	],
	"NE" : [
		{"id" : 0, "text" : "중국", "type" : "LCP_COUNTRY", "begin" : 10, "end" : 10, "weight" : 0.512824, "common_noun" : 0},
		{"id" : 1, "text" : "삼국시대", "type" : "DT_DYNASTY", "begin" : 11, "end" : 12, "weight" : 0.773659, "common_noun" : 0},
		{"id" : 2, "text" : "오나라", "type" : "LCP_COUNTRY", "begin" : 13, "end" : 14, "weight" : 0.371623, "common_noun" : 0},
		{"id" : 3, "text" : "황제", "type" : "CV_POSITION", "begin" : 17, "end" : 17, "weight" : 0.158966, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿이", "head" : 3, "label" : "NP_SBJ", "mod" : [], "weight" : 0.5166 },
		{"id" : 1, "text" : "사람의", "head" : 2, "label" : "NP_MOD", "mod" : [], "weight" : 0.770117 },
		{"id" : 2, "text" : "자는", "head" : 3, "label" : "NP_SBJ", "mod" : [1], "weight" : 0.754047 },
		{"id" : 3, "text" : "'중모'로", "head" : 7, "label" : "NP_AJT", "mod" : [0, 2], "weight" : 0.708626 },
		{"id" : 4, "text" : "중국", "head" : 5, "label" : "NP", "mod" : [], "weight" : 0.841514 },
		{"id" : 5, "text" : "삼국시대", "head" : 6, "label" : "NP", "mod" : [4], "weight" : 0.881006 },
		{"id" : 6, "text" : "오나라의", "head" : 7, "label" : "NP_MOD", "mod" : [5], "weight" : 0.757907 },
		{"id" : 7, "text" : "초대황제는", "head" : 8, "label" : "NP_SBJ", "mod" : [3, 6], "weight" : 0.671016 },
		{"id" : 8, "text" : "누구일까?", "head" : -1, "label" : "VNP", "mod" : [7], "weight" : 0.0515358 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "CV_POSITION", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "오나라", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 7, "ne_id" : 3, "text" : "﻿이 사람의 자는 '중모'로 중국 삼국시대 오나라의 초대황제", "start_eid_short" : 4, "end_eid_short" : 7, "text_short" : "중국 삼국시대 오나라의 초대황제", "weight" : 0.005 },
		{"id" : 1, "sent_id" : 0, "start_eid" : 0, "end_eid" : 0, "ne_id" : -1, "text" : "﻿", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "﻿", "weight" : 0.008 },
		{"id" : 2, "sent_id" : 0, "start_eid" : 1, "end_eid" : 2, "ne_id" : -1, "text" : "사람의 자", "start_eid_short" : 1, "end_eid_short" : 2, "text_short" : "사람의 자", "weight" : 0.014 },
		{"id" : 7, "sent_id" : 0, "start_eid" : 8, "end_eid" : 8, "ne_id" : -1, "text" : "누구", "start_eid_short" : 8, "end_eid_short" : 8, "text_short" : "누구이ㄹ까?", "weight" : 0.018 }
	] }
 ]
}

