{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "덕수궁에 있는 국보 제229호.",
	"morp" : [
		{"id" : 0, "lemma" : "덕수", "type" : "NNP", "position" : 0, "weight" : 0.0835994 },
		{"id" : 1, "lemma" : "궁", "type" : "NNG", "position" : 6, "weight" : 0.276911 },
		{"id" : 2, "lemma" : "에", "type" : "JKB", "position" : 9, "weight" : 0.153364 },
		{"id" : 3, "lemma" : "있", "type" : "VA", "position" : 13, "weight" : 0.037292 },
		{"id" : 4, "lemma" : "는", "type" : "ETM", "position" : 16, "weight" : 0.26168 },
		{"id" : 5, "lemma" : "국보", "type" : "NNG", "position" : 20, "weight" : 0.657134 },
		{"id" : 6, "lemma" : "제", "type" : "XPN", "position" : 27, "weight" : 0.000343053 },
		{"id" : 7, "lemma" : "229", "type" : "SN", "position" : 30, "weight" : 1 },
		{"id" : 8, "lemma" : "호", "type" : "NNB", "position" : 33, "weight" : 0.215698 },
		{"id" : 9, "lemma" : ".", "type" : "SF", "position" : 36, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "덕수궁/NNG+에/JKB", "target" : "덕수궁에", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "있/VA+는/ETM", "target" : "있는", "word_id" : 1, "m_begin" : 3, "m_end" : 4},
		{"id" : 2, "result" : "국보/NNG", "target" : "국보", "word_id" : 2, "m_begin" : 5, "m_end" : 5},
		{"id" : 3, "result" : "제/XPN+229/SN+호/NNB+./SF", "target" : "제229호.", "word_id" : 3, "m_begin" : 6, "m_end" : 9}
	],
	"WSD" : [
		{"id" : 0, "text" : "덕수궁", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "있", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 13, "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 16, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "국보", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 20, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "제", "type" : "XPN", "scode" : "21", "weight" : 1, "position" : 27, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "229", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 30, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "호", "type" : "NNB", "scode" : "14", "weight" : 1, "position" : 33, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 36, "begin" : 9, "end" : 9}
	],
	"word" : [
		{"id" : 0, "text" : "덕수궁에", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "있는", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 2, "text" : "국보", "type" : "", "begin" : 5, "end" : 5},
		{"id" : 3, "text" : "제229호.", "type" : "", "begin" : 6, "end" : 9}
	],
	"NE" : [
		{"id" : 0, "text" : "덕수궁", "type" : "AF_CULTURAL_ASSET", "begin" : 0, "end" : 1, "weight" : 0.933893, "common_noun" : 0},
		{"id" : 1, "text" : "제229호", "type" : "QT_ORDER", "begin" : 6, "end" : 8, "weight" : 0.822255, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "덕수궁에", "head" : 1, "label" : "NP_AJT", "mod" : [], "weight" : 0.638487 },
		{"id" : 1, "text" : "있는", "head" : 3, "label" : "VP_MOD", "mod" : [0], "weight" : 0.731672 },
		{"id" : 2, "text" : "국보", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.749971 },
		{"id" : 3, "text" : "제229호.", "head" : -1, "label" : "NP", "mod" : [1, 2], "weight" : 0.297366 }
	],
	"SRL" : [
		{"verb" : "있", "sense" : 1, "word_id" : 1, "weight" : 0.48385,
			"argument" : [
				{"type" : "ARG2", "word_id" : 0, "text" : "덕수궁에", "weight" : 0.495274 },
				{"type" : "ARG1", "word_id" : 3, "text" : "제229호.", "weight" : 0.472425 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : " 효종 때부터 조선 후기까지 표준 시계로 사용.",
	"morp" : [
		{"id" : 0, "lemma" : "효종", "type" : "NNP", "position" : 38, "weight" : 0.9 },
		{"id" : 1, "lemma" : "때", "type" : "NNG", "position" : 45, "weight" : 0.280999 },
		{"id" : 2, "lemma" : "부터", "type" : "JX", "position" : 48, "weight" : 0.0688504 },
		{"id" : 3, "lemma" : "조선", "type" : "NNP", "position" : 55, "weight" : 0.0441558 },
		{"id" : 4, "lemma" : "후기", "type" : "NNG", "position" : 62, "weight" : 0.9 },
		{"id" : 5, "lemma" : "까지", "type" : "JX", "position" : 68, "weight" : 0.0687424 },
		{"id" : 6, "lemma" : "표준", "type" : "NNG", "position" : 75, "weight" : 0.9 },
		{"id" : 7, "lemma" : "시계", "type" : "NNG", "position" : 82, "weight" : 0.9 },
		{"id" : 8, "lemma" : "로", "type" : "JKB", "position" : 88, "weight" : 0.153229 },
		{"id" : 9, "lemma" : "사용", "type" : "NNG", "position" : 92, "weight" : 0.9 },
		{"id" : 10, "lemma" : ".", "type" : "SF", "position" : 98, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "효종/NNG", "target" : "효종", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "때/NNG+부터/JX", "target" : "때부터", "word_id" : 1, "m_begin" : 1, "m_end" : 2},
		{"id" : 2, "result" : "조선/NNG", "target" : "조선", "word_id" : 2, "m_begin" : 3, "m_end" : 3},
		{"id" : 3, "result" : "후기/NNG+까지/JX", "target" : "후기까지", "word_id" : 3, "m_begin" : 4, "m_end" : 5},
		{"id" : 4, "result" : "표준/NNG", "target" : "표준", "word_id" : 4, "m_begin" : 6, "m_end" : 6},
		{"id" : 5, "result" : "시계/NNG+로/JKB", "target" : "시계로", "word_id" : 5, "m_begin" : 7, "m_end" : 8},
		{"id" : 6, "result" : "사용/NNG+./SF", "target" : "사용.", "word_id" : 6, "m_begin" : 9, "m_end" : 10}
	],
	"WSD" : [
		{"id" : 0, "text" : "효종", "type" : "NNP", "scode" : "01", "weight" : 1, "position" : 38, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "때", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 45, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "부터", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 48, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "조선", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 55, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "후기", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 62, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "까지", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 68, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "표준", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 75, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "시계", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 82, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 88, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "사용", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 92, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 98, "begin" : 10, "end" : 10}
	],
	"word" : [
		{"id" : 0, "text" : "효종", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "때부터", "type" : "", "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "조선", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "후기까지", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 4, "text" : "표준", "type" : "", "begin" : 6, "end" : 6},
		{"id" : 5, "text" : "시계로", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 6, "text" : "사용.", "type" : "", "begin" : 9, "end" : 10}
	],
	"NE" : [
		{"id" : 0, "text" : "효종 때", "type" : "DT_DYNASTY", "begin" : 0, "end" : 1, "weight" : 0.773002, "common_noun" : 0},
		{"id" : 1, "text" : "조선 후기까지", "type" : "DT_DYNASTY", "begin" : 3, "end" : 5, "weight" : 0.75563, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "효종", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.694277 },
		{"id" : 1, "text" : "때부터", "head" : 6, "label" : "NP_AJT", "mod" : [0], "weight" : 0.879799 },
		{"id" : 2, "text" : "조선", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.74652 },
		{"id" : 3, "text" : "후기까지", "head" : 6, "label" : "NP_AJT", "mod" : [2], "weight" : 0.58781 },
		{"id" : 4, "text" : "표준", "head" : 5, "label" : "NP", "mod" : [], "weight" : 0.667406 },
		{"id" : 5, "text" : "시계로", "head" : 6, "label" : "NP_AJT", "mod" : [4], "weight" : 0.807054 },
		{"id" : 6, "text" : "사용.", "head" : -1, "label" : "NP", "mod" : [1, 3, 5], "weight" : 0.110919 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
 ]
}
