{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "조선 시기 화가인 김득신의 <파적도>와 변상벽의 <묘작도>에 공통으로 등장하는 동물은 무엇일까?",
	"morp" : [
		{"id" : 0, "lemma" : "조선", "type" : "NNP", "position" : 0, "weight" : 0.0878802 },
		{"id" : 1, "lemma" : "시기", "type" : "NNG", "position" : 7, "weight" : 0.281818 },
		{"id" : 2, "lemma" : "화가", "type" : "NNG", "position" : 14, "weight" : 0.9 },
		{"id" : 3, "lemma" : "이", "type" : "VCP", "position" : 20, "weight" : 0.0177525 },
		{"id" : 4, "lemma" : "ㄴ", "type" : "ETM", "position" : 20, "weight" : 0.220712 },
		{"id" : 5, "lemma" : "김득신", "type" : "NNP", "position" : 24, "weight" : 0.1 },
		{"id" : 6, "lemma" : "의", "type" : "JKG", "position" : 33, "weight" : 0.0987295 },
		{"id" : 7, "lemma" : "<", "type" : "SS", "position" : 37, "weight" : 1 },
		{"id" : 8, "lemma" : "파적도", "type" : "NNP", "position" : 38, "weight" : 0.6 },
		{"id" : 9, "lemma" : ">", "type" : "SS", "position" : 47, "weight" : 1 },
		{"id" : 10, "lemma" : "와", "type" : "JC", "position" : 48, "weight" : 0.00426109 },
		{"id" : 11, "lemma" : "변상벽", "type" : "NNP", "position" : 52, "weight" : 0.6 },
		{"id" : 12, "lemma" : "의", "type" : "JKG", "position" : 61, "weight" : 0.0987295 },
		{"id" : 13, "lemma" : "<", "type" : "SS", "position" : 65, "weight" : 1 },
		{"id" : 14, "lemma" : "묘작도", "type" : "NNP", "position" : 66, "weight" : 0.6 },
		{"id" : 15, "lemma" : ">", "type" : "SS", "position" : 75, "weight" : 1 },
		{"id" : 16, "lemma" : "에", "type" : "JKB", "position" : 76, "weight" : 0.048593 },
		{"id" : 17, "lemma" : "공통", "type" : "NNG", "position" : 80, "weight" : 0.9 },
		{"id" : 18, "lemma" : "으로", "type" : "JKB", "position" : 86, "weight" : 0.153406 },
		{"id" : 19, "lemma" : "등장", "type" : "NNG", "position" : 93, "weight" : 0.9 },
		{"id" : 20, "lemma" : "하", "type" : "XSV", "position" : 99, "weight" : 0.0001 },
		{"id" : 21, "lemma" : "는", "type" : "ETM", "position" : 102, "weight" : 0.238503 },
		{"id" : 22, "lemma" : "동물", "type" : "NNG", "position" : 106, "weight" : 0.9 },
		{"id" : 23, "lemma" : "은", "type" : "JX", "position" : 112, "weight" : 0.0449928 },
		{"id" : 24, "lemma" : "무엇", "type" : "NP", "position" : 116, "weight" : 0.9 },
		{"id" : 25, "lemma" : "이", "type" : "VCP", "position" : 122, "weight" : 0.0175768 },
		{"id" : 26, "lemma" : "ㄹ까", "type" : "EF", "position" : 122, "weight" : 0.258243 },
		{"id" : 27, "lemma" : "?", "type" : "SF", "position" : 128, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "조선/NNG", "target" : "조선", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "시기/NNG", "target" : "시기", "word_id" : 1, "m_begin" : 1, "m_end" : 1},
		{"id" : 2, "result" : "화가/NNG+이/VCP+ㄴ/ETM", "target" : "화가인", "word_id" : 2, "m_begin" : 2, "m_end" : 4},
		{"id" : 3, "result" : "김득신/NNG+의/JKG", "target" : "김득신의", "word_id" : 3, "m_begin" : 5, "m_end" : 6},
		{"id" : 4, "result" : "</SS+파적도/NNG+>/SS+와/JC", "target" : "<파적도>와", "word_id" : 4, "m_begin" : 7, "m_end" : 10},
		{"id" : 5, "result" : "변상벽/NNG+의/JKG", "target" : "변상벽의", "word_id" : 5, "m_begin" : 11, "m_end" : 12},
		{"id" : 6, "result" : "</SS+묘작도/NNG+>/SS+에/JKB", "target" : "<묘작도>에", "word_id" : 6, "m_begin" : 13, "m_end" : 16},
		{"id" : 7, "result" : "공통/NNG+으로/JKB", "target" : "공통으로", "word_id" : 7, "m_begin" : 17, "m_end" : 18},
		{"id" : 8, "result" : "등장하/VV+는/ETM", "target" : "등장하는", "word_id" : 8, "m_begin" : 19, "m_end" : 21},
		{"id" : 9, "result" : "동물/NNG+은/JX", "target" : "동물은", "word_id" : 9, "m_begin" : 22, "m_end" : 23},
		{"id" : 10, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 10, "m_begin" : 24, "m_end" : 27}
	],
	"WSD" : [
		{"id" : 0, "text" : "조선", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "시기", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 7, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "화가", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 14, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 20, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 20, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "김득신", "type" : "NNP", "scode" : "01", "weight" : 1, "position" : 24, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 33, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "<", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 37, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "파적도", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 38, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : ">", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 47, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "와", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 48, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "변상벽", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 52, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 61, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "<", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 65, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "묘작도", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 66, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : ">", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 75, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 76, "begin" : 16, "end" : 16},
		{"id" : 17, "text" : "공통", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 80, "begin" : 17, "end" : 17},
		{"id" : 18, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 86, "begin" : 18, "end" : 18},
		{"id" : 19, "text" : "등장하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 93, "begin" : 19, "end" : 20},
		{"id" : 20, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 102, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : "동물", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 106, "begin" : 22, "end" : 22},
		{"id" : 22, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 112, "begin" : 23, "end" : 23},
		{"id" : 23, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 116, "begin" : 24, "end" : 24},
		{"id" : 24, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 122, "begin" : 25, "end" : 25},
		{"id" : 25, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 122, "begin" : 26, "end" : 26},
		{"id" : 26, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 128, "begin" : 27, "end" : 27}
	],
	"word" : [
		{"id" : 0, "text" : "조선", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "시기", "type" : "", "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "화가인", "type" : "", "begin" : 2, "end" : 4},
		{"id" : 3, "text" : "김득신의", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 4, "text" : "<파적도>와", "type" : "", "begin" : 7, "end" : 10},
		{"id" : 5, "text" : "변상벽의", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 6, "text" : "<묘작도>에", "type" : "", "begin" : 13, "end" : 16},
		{"id" : 7, "text" : "공통으로", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 8, "text" : "등장하는", "type" : "", "begin" : 19, "end" : 21},
		{"id" : 9, "text" : "동물은", "type" : "", "begin" : 22, "end" : 23},
		{"id" : 10, "text" : "무엇일까?", "type" : "", "begin" : 24, "end" : 27}
	],
	"NE" : [
		{"id" : 0, "text" : "조선 시기", "type" : "DT_DYNASTY", "begin" : 0, "end" : 1, "weight" : 0.760506, "common_noun" : 0},
		{"id" : 1, "text" : "화가", "type" : "CV_OCCUPATION", "begin" : 2, "end" : 2, "weight" : 0.609946, "common_noun" : 0},
		{"id" : 2, "text" : "김득신", "type" : "PS_NAME", "begin" : 5, "end" : 5, "weight" : 0.618246, "common_noun" : 0},
		{"id" : 3, "text" : "파적도", "type" : "AFW_ART_CRAFT", "begin" : 8, "end" : 8, "weight" : 0.375068, "common_noun" : 0},
		{"id" : 4, "text" : "변상벽", "type" : "PS_NAME", "begin" : 11, "end" : 11, "weight" : 0.565059, "common_noun" : 0},
		{"id" : 5, "text" : "묘작도", "type" : "AFW_ART_CRAFT", "begin" : 14, "end" : 14, "weight" : 0.338642, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "조선", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.529159 },
		{"id" : 1, "text" : "시기", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.64702 },
		{"id" : 2, "text" : "화가인", "head" : 3, "label" : "VNP_MOD", "mod" : [1], "weight" : 0.214878 },
		{"id" : 3, "text" : "김득신의", "head" : 4, "label" : "NP_MOD", "mod" : [2], "weight" : 0.592246 },
		{"id" : 4, "text" : "<파적도>와", "head" : 6, "label" : "NP_CNJ", "mod" : [3], "weight" : 0.631364 },
		{"id" : 5, "text" : "변상벽의", "head" : 6, "label" : "NP_MOD", "mod" : [], "weight" : 0.618802 },
		{"id" : 6, "text" : "<묘작도>에", "head" : 8, "label" : "NP_AJT", "mod" : [4, 5], "weight" : 0.796187 },
		{"id" : 7, "text" : "공통으로", "head" : 8, "label" : "NP_AJT", "mod" : [], "weight" : 0.836715 },
		{"id" : 8, "text" : "등장하는", "head" : 9, "label" : "VP_MOD", "mod" : [6, 7], "weight" : 0.827755 },
		{"id" : 9, "text" : "동물은", "head" : 10, "label" : "NP_SBJ", "mod" : [8], "weight" : 0.856326 },
		{"id" : 10, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [9], "weight" : 0.00703635 }
	],
	"SRL" : [
		{"verb" : "등장", "sense" : 1, "word_id" : 8, "weight" : 0.23369,
			"argument" : [
				{"type" : "ARG2", "word_id" : 6, "text" : "<묘작도>에", "weight" : 0.270383 },
				{"type" : "ARGM-MNR", "word_id" : 7, "text" : "공통으로", "weight" : 0.159929 },
				{"type" : "ARG1", "word_id" : 9, "text" : "동물은", "weight" : 0.270757 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 9, "ne_id" : -1, "text" : "조선 시기 화가인 김득신의 <파적도>와 변상벽의 <묘작도>에 공통으로 등장하는 동물", "start_eid_short" : 9, "end_eid_short" : 9, "text_short" : "동물", "weight" : 0.002 },
		{"id" : 9, "sent_id" : 0, "start_eid" : 9, "end_eid" : 10, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 10, "end_eid_short" : 10, "text_short" : "무엇이ㄹ까?", "weight" : 0.006 }
	] },
	{"id" : 1, "type" : "PS_NAME", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 4, "sent_id" : 0, "start_eid" : 2, "end_eid" : 2, "ne_id" : 1, "text" : "화가이ㄴ", "start_eid_short" : 2, "end_eid_short" : 2, "text_short" : "화가이ㄴ", "weight" : 0.012 },
		{"id" : 2, "sent_id" : 0, "start_eid" : 0, "end_eid" : 3, "ne_id" : 2, "text" : "조선 시기 화가인 김득신", "start_eid_short" : 3, "end_eid_short" : 3, "text_short" : "김득신", "weight" : 0.007 }
	] },
	{"id" : 2, "type" : "AFW_ART_CRAFT", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 7, "sent_id" : 0, "start_eid" : 6, "end_eid" : 6, "ne_id" : 5, "text" : "<묘작도>", "start_eid_short" : 6, "end_eid_short" : 6, "text_short" : "<묘작도>", "weight" : 0.01 },
		{"id" : 5, "sent_id" : 0, "start_eid" : 4, "end_eid" : 4, "ne_id" : 3, "text" : "<파적도>", "start_eid_short" : 4, "end_eid_short" : 4, "text_short" : "<파적도>", "weight" : 0.006 }
	] }
 ]
}
