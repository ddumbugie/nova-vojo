{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "이희재(李喜宰, 1952년11월 16일 ~ )는 대한민국의 만화가이다.",
	"morp" : [
		{"id" : 0, "lemma" : "이희재", "type" : "NNP", "position" : 0, "weight" : 0.15 },
		{"id" : 1, "lemma" : "(", "type" : "SS", "position" : 9, "weight" : 1 },
		{"id" : 2, "lemma" : "李喜宰", "type" : "SH", "position" : 10, "weight" : 1 },
		{"id" : 3, "lemma" : ",", "type" : "SP", "position" : 19, "weight" : 1 },
		{"id" : 4, "lemma" : "1952", "type" : "SN", "position" : 21, "weight" : 1 },
		{"id" : 5, "lemma" : "년", "type" : "NNB", "position" : 25, "weight" : 0.414343 },
		{"id" : 6, "lemma" : "11", "type" : "SN", "position" : 28, "weight" : 1 },
		{"id" : 7, "lemma" : "월", "type" : "NNB", "position" : 30, "weight" : 0.408539 },
		{"id" : 8, "lemma" : "16", "type" : "SN", "position" : 34, "weight" : 1 },
		{"id" : 9, "lemma" : "일", "type" : "NNB", "position" : 36, "weight" : 0.126777 },
		{"id" : 10, "lemma" : "~", "type" : "SO", "position" : 40, "weight" : 1 },
		{"id" : 11, "lemma" : ")", "type" : "SS", "position" : 42, "weight" : 1 },
		{"id" : 12, "lemma" : "는", "type" : "JX", "position" : 43, "weight" : 0.00823314 },
		{"id" : 13, "lemma" : "대한민국", "type" : "NNP", "position" : 47, "weight" : 0.0447775 },
		{"id" : 14, "lemma" : "의", "type" : "JKG", "position" : 59, "weight" : 0.0987295 },
		{"id" : 15, "lemma" : "만화", "type" : "NNG", "position" : 63, "weight" : 0.83848 },
		{"id" : 16, "lemma" : "가", "type" : "XSN", "position" : 69, "weight" : 0.000115417 },
		{"id" : 17, "lemma" : "이", "type" : "VCP", "position" : 72, "weight" : 0.0165001 },
		{"id" : 18, "lemma" : "다", "type" : "EF", "position" : 75, "weight" : 0.353579 },
		{"id" : 19, "lemma" : ".", "type" : "SF", "position" : 78, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이희재/NNG+(/SS+李喜宰/SH+,/SP", "target" : "이희재(李喜宰,", "word_id" : 0, "m_begin" : 0, "m_end" : 3},
		{"id" : 1, "result" : "1952/SN+년/NNB+11/SN+월/NNB", "target" : "1952년11월", "word_id" : 1, "m_begin" : 4, "m_end" : 7},
		{"id" : 2, "result" : "16/SN+일/NNB", "target" : "16일", "word_id" : 2, "m_begin" : 8, "m_end" : 9},
		{"id" : 3, "result" : "~/SO", "target" : "~", "word_id" : 3, "m_begin" : 10, "m_end" : 10},
		{"id" : 4, "result" : ")/SS+는/JX", "target" : ")는", "word_id" : 4, "m_begin" : 11, "m_end" : 12},
		{"id" : 5, "result" : "대한민국/NNG+의/JKG", "target" : "대한민국의", "word_id" : 5, "m_begin" : 13, "m_end" : 14},
		{"id" : 6, "result" : "만화가/NNG+이/VCP+다/EF+./SF", "target" : "만화가이다.", "word_id" : 6, "m_begin" : 15, "m_end" : 19}
	],
	"WSD" : [
		{"id" : 0, "text" : "이희재", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "李喜宰", "type" : "SH", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 19, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "1952", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 21, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 25, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "11", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 28, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "월", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 30, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "16", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 34, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "일", "type" : "NNB", "scode" : "07", "weight" : 1, "position" : 36, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "~", "type" : "SO", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 42, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 43, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "대한민국", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 47, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 59, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "만화가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 63, "begin" : 15, "end" : 16},
		{"id" : 16, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 72, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 75, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 78, "begin" : 19, "end" : 19}
	],
	"word" : [
		{"id" : 0, "text" : "이희재(李喜宰,", "type" : "", "begin" : 0, "end" : 3},
		{"id" : 1, "text" : "1952년11월", "type" : "", "begin" : 4, "end" : 7},
		{"id" : 2, "text" : "16일", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 3, "text" : "~", "type" : "", "begin" : 10, "end" : 10},
		{"id" : 4, "text" : ")는", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 5, "text" : "대한민국의", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 6, "text" : "만화가이다.", "type" : "", "begin" : 15, "end" : 19}
	],
	"NE" : [
		{"id" : 0, "text" : "이희재", "type" : "PS_NAME", "begin" : 0, "end" : 0, "weight" : 0.406866, "common_noun" : 0},
		{"id" : 1, "text" : "李喜宰", "type" : "PS_NAME", "begin" : 2, "end" : 2, "weight" : 0.107081, "common_noun" : 0},
		{"id" : 2, "text" : "1952년11월 16일 ~", "type" : "DT_OTHERS", "begin" : 4, "end" : 10, "weight" : 0.742155, "common_noun" : 0},
		{"id" : 3, "text" : "대한민국", "type" : "LCP_COUNTRY", "begin" : 13, "end" : 13, "weight" : 0.174693, "common_noun" : 0},
		{"id" : 4, "text" : "만화가", "type" : "CV_OCCUPATION", "begin" : 15, "end" : 16, "weight" : 0.284922, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이희재(李喜宰,", "head" : 4, "label" : "NP", "mod" : [], "weight" : 0.383716 },
		{"id" : 1, "text" : "1952년11월", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.504705 },
		{"id" : 2, "text" : "16일", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.421138 },
		{"id" : 3, "text" : "~", "head" : 4, "label" : "X", "mod" : [2], "weight" : 0.764853 },
		{"id" : 4, "text" : ")는", "head" : 6, "label" : "NP_SBJ", "mod" : [0, 3], "weight" : 0.45201 },
		{"id" : 5, "text" : "대한민국의", "head" : 6, "label" : "NP_MOD", "mod" : [], "weight" : 0.425766 },
		{"id" : 6, "text" : "만화가이다.", "head" : -1, "label" : "VNP", "mod" : [4, 5], "weight" : 0.00680083 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
 ]
}

