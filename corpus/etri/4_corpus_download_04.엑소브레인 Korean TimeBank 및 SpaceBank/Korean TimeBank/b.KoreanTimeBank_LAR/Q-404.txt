{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿조선 후기에는 청과의 무역을 통해 은광과 금광 개발이 활발했다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "조선", "type" : "NNP", "position" : 3, "weight" : 0.0796816 },
		{"id" : 2, "lemma" : "후기", "type" : "NNG", "position" : 10, "weight" : 0.9 },
		{"id" : 3, "lemma" : "에", "type" : "JKB", "position" : 16, "weight" : 0.153364 },
		{"id" : 4, "lemma" : "는", "type" : "JX", "position" : 19, "weight" : 0.0387928 },
		{"id" : 5, "lemma" : "청과", "type" : "NNG", "position" : 23, "weight" : 0.25 },
		{"id" : 6, "lemma" : "의", "type" : "JKG", "position" : 29, "weight" : 0.0694213 },
		{"id" : 7, "lemma" : "무역", "type" : "NNG", "position" : 33, "weight" : 0.870646 },
		{"id" : 8, "lemma" : "을", "type" : "JKO", "position" : 39, "weight" : 0.129611 },
		{"id" : 9, "lemma" : "통하", "type" : "VV", "position" : 43, "weight" : 0.9 },
		{"id" : 10, "lemma" : "어", "type" : "EC", "position" : 46, "weight" : 0.41831 },
		{"id" : 11, "lemma" : "은광", "type" : "NNG", "position" : 50, "weight" : 0.109712 },
		{"id" : 12, "lemma" : "과", "type" : "JC", "position" : 56, "weight" : 0.017569 },
		{"id" : 13, "lemma" : "금광", "type" : "NNG", "position" : 60, "weight" : 0.9 },
		{"id" : 14, "lemma" : "개발", "type" : "NNG", "position" : 67, "weight" : 0.9 },
		{"id" : 15, "lemma" : "이", "type" : "JKS", "position" : 73, "weight" : 0.0360723 },
		{"id" : 16, "lemma" : "활발하", "type" : "VA", "position" : 77, "weight" : 0.9 },
		{"id" : 17, "lemma" : "었", "type" : "EP", "position" : 83, "weight" : 0.9 },
		{"id" : 18, "lemma" : "다", "type" : "EF", "position" : 86, "weight" : 0.640954 },
		{"id" : 19, "lemma" : ".", "type" : "SF", "position" : 89, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿조선/NNG", "target" : "﻿조선", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "후기/NNG+에/JKB+는/JX", "target" : "후기에는", "word_id" : 1, "m_begin" : 2, "m_end" : 4},
		{"id" : 2, "result" : "청과/NNG+의/JKG", "target" : "청과의", "word_id" : 2, "m_begin" : 5, "m_end" : 6},
		{"id" : 3, "result" : "무역/NNG+을/JKO", "target" : "무역을", "word_id" : 3, "m_begin" : 7, "m_end" : 8},
		{"id" : 4, "result" : "통하/VV+어/EC", "target" : "통해", "word_id" : 4, "m_begin" : 9, "m_end" : 10},
		{"id" : 5, "result" : "은광/NNG+과/JC", "target" : "은광과", "word_id" : 5, "m_begin" : 11, "m_end" : 12},
		{"id" : 6, "result" : "금광/NNG", "target" : "금광", "word_id" : 6, "m_begin" : 13, "m_end" : 13},
		{"id" : 7, "result" : "개발/NNG+이/JKS", "target" : "개발이", "word_id" : 7, "m_begin" : 14, "m_end" : 15},
		{"id" : 8, "result" : "활발하/VA+었/EP+다/EF+./SF", "target" : "활발했다.", "word_id" : 8, "m_begin" : 16, "m_end" : 19}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "조선", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "후기", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 16, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 19, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "청과", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 23, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 29, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "무역", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 33, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 39, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "통하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 43, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 46, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "은광", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 50, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "과", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 56, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "금광", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 60, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "개발", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 67, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 73, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "활발하", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 77, "begin" : 16, "end" : 16},
		{"id" : 17, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 83, "begin" : 17, "end" : 17},
		{"id" : 18, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 86, "begin" : 18, "end" : 18},
		{"id" : 19, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 89, "begin" : 19, "end" : 19}
	],
	"word" : [
		{"id" : 0, "text" : "﻿조선", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "후기에는", "type" : "", "begin" : 2, "end" : 4},
		{"id" : 2, "text" : "청과의", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 3, "text" : "무역을", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 4, "text" : "통해", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 5, "text" : "은광과", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 6, "text" : "금광", "type" : "", "begin" : 13, "end" : 13},
		{"id" : 7, "text" : "개발이", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 8, "text" : "활발했다.", "type" : "", "begin" : 16, "end" : 19}
	],
	"NE" : [
		{"id" : 0, "text" : "조선 후기", "type" : "DT_DYNASTY", "begin" : 1, "end" : 2, "weight" : 0.759001, "common_noun" : 0},
		{"id" : 1, "text" : "청과", "type" : "LCP_COUNTRY", "begin" : 5, "end" : 5, "weight" : 0.130196, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿조선", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.887173 },
		{"id" : 1, "text" : "후기에는", "head" : 4, "label" : "NP_AJT", "mod" : [0], "weight" : 0.649576 },
		{"id" : 2, "text" : "청과의", "head" : 3, "label" : "NP_MOD", "mod" : [], "weight" : 0.993583 },
		{"id" : 3, "text" : "무역을", "head" : 4, "label" : "NP_OBJ", "mod" : [2], "weight" : 0.774278 },
		{"id" : 4, "text" : "통해", "head" : 8, "label" : "VP", "mod" : [1, 3], "weight" : 0.777718 },
		{"id" : 5, "text" : "은광과", "head" : 7, "label" : "NP_CNJ", "mod" : [], "weight" : 0.913611 },
		{"id" : 6, "text" : "금광", "head" : 7, "label" : "NP", "mod" : [], "weight" : 0.992929 },
		{"id" : 7, "text" : "개발이", "head" : 8, "label" : "NP_SBJ", "mod" : [5, 6], "weight" : 0.554124 },
		{"id" : 8, "text" : "활발했다.", "head" : -1, "label" : "VP", "mod" : [4, 7], "weight" : 0.117134 }
	],
	"SRL" : [
		{"verb" : "통하", "sense" : 1, "word_id" : 4, "weight" : 0.399825,
			"argument" : [
				{"type" : "ARG2", "word_id" : 3, "text" : "무역을", "weight" : 0.399825 }
			] },
		{"verb" : "활발하", "sense" : 1, "word_id" : 8, "weight" : 0.171121,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 1, "text" : "후기에는", "weight" : 0.172669 },
				{"type" : "ARGM-MNR", "word_id" : 4, "text" : "통해", "weight" : 0.112396 },
				{"type" : "ARG1", "word_id" : 7, "text" : "개발이", "weight" : 0.228298 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "당시 대규모 광산 개발을 총괄한 광산 전문 경영인을 부르는 말은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "당시", "type" : "NNG", "position" : 91, "weight" : 0.9 },
		{"id" : 1, "lemma" : "대", "type" : "XPN", "position" : 98, "weight" : 0.0001 },
		{"id" : 2, "lemma" : "규모", "type" : "NNG", "position" : 101, "weight" : 0.9 },
		{"id" : 3, "lemma" : "광산", "type" : "NNG", "position" : 108, "weight" : 0.1803 },
		{"id" : 4, "lemma" : "개발", "type" : "NNG", "position" : 115, "weight" : 0.9 },
		{"id" : 5, "lemma" : "을", "type" : "JKO", "position" : 121, "weight" : 0.129611 },
		{"id" : 6, "lemma" : "총괄", "type" : "NNG", "position" : 125, "weight" : 0.9 },
		{"id" : 7, "lemma" : "하", "type" : "XSV", "position" : 131, "weight" : 0.0001 },
		{"id" : 8, "lemma" : "ㄴ", "type" : "ETM", "position" : 131, "weight" : 0.392321 },
		{"id" : 9, "lemma" : "광산", "type" : "NNG", "position" : 135, "weight" : 0.642659 },
		{"id" : 10, "lemma" : "전문", "type" : "NNG", "position" : 142, "weight" : 0.184561 },
		{"id" : 11, "lemma" : "경영", "type" : "NNG", "position" : 149, "weight" : 0.9 },
		{"id" : 12, "lemma" : "인", "type" : "XSN", "position" : 155, "weight" : 0.0198339 },
		{"id" : 13, "lemma" : "을", "type" : "JKO", "position" : 158, "weight" : 0.0819193 },
		{"id" : 14, "lemma" : "부르", "type" : "VV", "position" : 162, "weight" : 0.763243 },
		{"id" : 15, "lemma" : "는", "type" : "ETM", "position" : 168, "weight" : 0.184941 },
		{"id" : 16, "lemma" : "말", "type" : "NNG", "position" : 172, "weight" : 0.528926 },
		{"id" : 17, "lemma" : "은", "type" : "JX", "position" : 175, "weight" : 0.0449928 },
		{"id" : 18, "lemma" : "무엇", "type" : "NP", "position" : 179, "weight" : 0.9 },
		{"id" : 19, "lemma" : "이", "type" : "VCP", "position" : 185, "weight" : 0.0175768 },
		{"id" : 20, "lemma" : "ㄹ까", "type" : "EF", "position" : 185, "weight" : 0.258243 },
		{"id" : 21, "lemma" : "?", "type" : "SF", "position" : 191, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "당시/NNG", "target" : "당시", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "대규모/NNG", "target" : "대규모", "word_id" : 1, "m_begin" : 1, "m_end" : 2},
		{"id" : 2, "result" : "광산/NNG", "target" : "광산", "word_id" : 2, "m_begin" : 3, "m_end" : 3},
		{"id" : 3, "result" : "개발/NNG+을/JKO", "target" : "개발을", "word_id" : 3, "m_begin" : 4, "m_end" : 5},
		{"id" : 4, "result" : "총괄하/VV+ㄴ/ETM", "target" : "총괄한", "word_id" : 4, "m_begin" : 6, "m_end" : 8},
		{"id" : 5, "result" : "광산/NNG", "target" : "광산", "word_id" : 5, "m_begin" : 9, "m_end" : 9},
		{"id" : 6, "result" : "전문/NNG", "target" : "전문", "word_id" : 6, "m_begin" : 10, "m_end" : 10},
		{"id" : 7, "result" : "경영인/NNG+을/JKO", "target" : "경영인을", "word_id" : 7, "m_begin" : 11, "m_end" : 13},
		{"id" : 8, "result" : "부르/VV+는/ETM", "target" : "부르는", "word_id" : 8, "m_begin" : 14, "m_end" : 15},
		{"id" : 9, "result" : "말/NNG+은/JX", "target" : "말은", "word_id" : 9, "m_begin" : 16, "m_end" : 17},
		{"id" : 10, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 10, "m_begin" : 18, "m_end" : 21}
	],
	"WSD" : [
		{"id" : 0, "text" : "당시", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 91, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "대규모", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 98, "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "광산", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 108, "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "개발", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 115, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 121, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "총괄하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 125, "begin" : 6, "end" : 7},
		{"id" : 6, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 131, "begin" : 8, "end" : 8},
		{"id" : 7, "text" : "광산", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 135, "begin" : 9, "end" : 9},
		{"id" : 8, "text" : "전문", "type" : "NNG", "scode" : "08", "weight" : 1, "position" : 142, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "경영인", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 149, "begin" : 11, "end" : 12},
		{"id" : 10, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 158, "begin" : 13, "end" : 13},
		{"id" : 11, "text" : "부르", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 162, "begin" : 14, "end" : 14},
		{"id" : 12, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 168, "begin" : 15, "end" : 15},
		{"id" : 13, "text" : "말", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 172, "begin" : 16, "end" : 16},
		{"id" : 14, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 175, "begin" : 17, "end" : 17},
		{"id" : 15, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 179, "begin" : 18, "end" : 18},
		{"id" : 16, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 185, "begin" : 19, "end" : 19},
		{"id" : 17, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 185, "begin" : 20, "end" : 20},
		{"id" : 18, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 191, "begin" : 21, "end" : 21}
	],
	"word" : [
		{"id" : 0, "text" : "당시", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "대규모", "type" : "", "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "광산", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "개발을", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 4, "text" : "총괄한", "type" : "", "begin" : 6, "end" : 8},
		{"id" : 5, "text" : "광산", "type" : "", "begin" : 9, "end" : 9},
		{"id" : 6, "text" : "전문", "type" : "", "begin" : 10, "end" : 10},
		{"id" : 7, "text" : "경영인을", "type" : "", "begin" : 11, "end" : 13},
		{"id" : 8, "text" : "부르는", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 9, "text" : "말은", "type" : "", "begin" : 16, "end" : 17},
		{"id" : 10, "text" : "무엇일까?", "type" : "", "begin" : 18, "end" : 21}
	],
	"NE" : [
		{"id" : 0, "text" : "전문 경영인", "type" : "CV_OCCUPATION", "begin" : 10, "end" : 12, "weight" : 0.387882, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "당시", "head" : 4, "label" : "NP_AJT", "mod" : [], "weight" : 0.606761 },
		{"id" : 1, "text" : "대규모", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.669292 },
		{"id" : 2, "text" : "광산", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.837108 },
		{"id" : 3, "text" : "개발을", "head" : 4, "label" : "NP_OBJ", "mod" : [1, 2], "weight" : 0.803556 },
		{"id" : 4, "text" : "총괄한", "head" : 7, "label" : "VP_MOD", "mod" : [0, 3], "weight" : 0.688118 },
		{"id" : 5, "text" : "광산", "head" : 6, "label" : "NP", "mod" : [], "weight" : 0.627379 },
		{"id" : 6, "text" : "전문", "head" : 7, "label" : "NP", "mod" : [5], "weight" : 0.718579 },
		{"id" : 7, "text" : "경영인을", "head" : 8, "label" : "NP_OBJ", "mod" : [4, 6], "weight" : 0.97368 },
		{"id" : 8, "text" : "부르는", "head" : 9, "label" : "VP_MOD", "mod" : [7], "weight" : 0.836037 },
		{"id" : 9, "text" : "말은", "head" : 10, "label" : "NP_SBJ", "mod" : [8], "weight" : 0.745801 },
		{"id" : 10, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [9], "weight" : 0.0375817 }
	],
	"SRL" : [
		{"verb" : "총괄", "sense" : 1, "word_id" : 4, "weight" : 0.325707,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 0, "text" : "당시", "weight" : 0.364354 },
				{"type" : "ARG1", "word_id" : 3, "text" : "개발을", "weight" : 0.287976 },
				{"type" : "ARG0", "word_id" : 7, "text" : "경영인을", "weight" : 0.324791 }
			] },
		{"verb" : "부르", "sense" : 1, "word_id" : 8, "weight" : 0.241693,
			"argument" : [
				{"type" : "ARG1", "word_id" : 7, "text" : "경영인을", "weight" : 0.347192 },
				{"type" : "ARG2", "word_id" : 9, "text" : "말은", "weight" : 0.136194 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "plural", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 3, "sent_id" : 0, "start_eid" : 5, "end_eid" : 7, "ne_id" : -1, "text" : "은광과 금광 개발", "start_eid_short" : 6, "end_eid_short" : 7, "text_short" : "금광 개발", "weight" : 0.006 },
		{"id" : 5, "sent_id" : 0, "start_eid" : 6, "end_eid" : 7, "ne_id" : -1, "text" : "금광 개발", "start_eid_short" : 6, "end_eid_short" : 7, "text_short" : "금광 개발", "weight" : 0.01 }
	] },
	{"id" : 1, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 7, "sent_id" : 1, "start_eid" : 0, "end_eid" : 9, "ne_id" : -1, "text" : "당시 대규모 광산 개발을 총괄한 광산 전문 경영인을 부르는 말", "start_eid_short" : 9, "end_eid_short" : 9, "text_short" : "말", "weight" : 0.002 },
		{"id" : 14, "sent_id" : 1, "start_eid" : 9, "end_eid" : 10, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 10, "end_eid_short" : 10, "text_short" : "무엇이ㄹ까?", "weight" : 0.006 }
	] },
	{"id" : 2, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 13, "sent_id" : 1, "start_eid" : 5, "end_eid" : 5, "ne_id" : -1, "text" : "광산", "start_eid_short" : 5, "end_eid_short" : 5, "text_short" : "광산", "weight" : 0.016 },
		{"id" : 12, "sent_id" : 1, "start_eid" : 2, "end_eid" : 2, "ne_id" : -1, "text" : "광산", "start_eid_short" : 2, "end_eid_short" : 2, "text_short" : "광산", "weight" : 0.01 }
	] }
 ]
}

