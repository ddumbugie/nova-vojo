{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿조선 왕조 시기 인재 양성을 위해 유학 교육 등을 실시한 최고 교육기관 ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "조선", "type" : "NNP", "position" : 3, "weight" : 0.0796816 },
		{"id" : 2, "lemma" : "왕조", "type" : "NNG", "position" : 10, "weight" : 0.9 },
		{"id" : 3, "lemma" : "시기", "type" : "NNG", "position" : 17, "weight" : 0.184726 },
		{"id" : 4, "lemma" : "인재", "type" : "NNG", "position" : 24, "weight" : 0.173637 },
		{"id" : 5, "lemma" : "양성", "type" : "NNG", "position" : 31, "weight" : 0.183692 },
		{"id" : 6, "lemma" : "을", "type" : "JKO", "position" : 37, "weight" : 0.129611 },
		{"id" : 7, "lemma" : "위하", "type" : "VV", "position" : 41, "weight" : 0.778555 },
		{"id" : 8, "lemma" : "어", "type" : "EC", "position" : 44, "weight" : 0.41831 },
		{"id" : 9, "lemma" : "유학", "type" : "NNG", "position" : 48, "weight" : 0.9 },
		{"id" : 10, "lemma" : "교육", "type" : "NNG", "position" : 55, "weight" : 0.9 },
		{"id" : 11, "lemma" : "등", "type" : "NNB", "position" : 62, "weight" : 0.0146757 },
		{"id" : 12, "lemma" : "을", "type" : "JKO", "position" : 65, "weight" : 0.0630104 },
		{"id" : 13, "lemma" : "실시", "type" : "NNG", "position" : 69, "weight" : 0.9 },
		{"id" : 14, "lemma" : "하", "type" : "XSV", "position" : 75, "weight" : 0.0001 },
		{"id" : 15, "lemma" : "ㄴ", "type" : "ETM", "position" : 75, "weight" : 0.392321 },
		{"id" : 16, "lemma" : "최고", "type" : "NNG", "position" : 79, "weight" : 0.9 },
		{"id" : 17, "lemma" : "교육", "type" : "NNG", "position" : 86, "weight" : 0.9 },
		{"id" : 18, "lemma" : "기관", "type" : "NNG", "position" : 92, "weight" : 0.9 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿조선/NNG", "target" : "﻿조선", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "왕조/NNG", "target" : "왕조", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "시기/NNG", "target" : "시기", "word_id" : 2, "m_begin" : 3, "m_end" : 3},
		{"id" : 3, "result" : "인재/NNG", "target" : "인재", "word_id" : 3, "m_begin" : 4, "m_end" : 4},
		{"id" : 4, "result" : "양성/NNG+을/JKO", "target" : "양성을", "word_id" : 4, "m_begin" : 5, "m_end" : 6},
		{"id" : 5, "result" : "위하/VV+어/EC", "target" : "위해", "word_id" : 5, "m_begin" : 7, "m_end" : 8},
		{"id" : 6, "result" : "유학/NNG", "target" : "유학", "word_id" : 6, "m_begin" : 9, "m_end" : 9},
		{"id" : 7, "result" : "교육/NNG", "target" : "교육", "word_id" : 7, "m_begin" : 10, "m_end" : 10},
		{"id" : 8, "result" : "등/NNB+을/JKO", "target" : "등을", "word_id" : 8, "m_begin" : 11, "m_end" : 12},
		{"id" : 9, "result" : "실시하/VV+ㄴ/ETM", "target" : "실시한", "word_id" : 9, "m_begin" : 13, "m_end" : 15},
		{"id" : 10, "result" : "최고/NNG", "target" : "최고", "word_id" : 10, "m_begin" : 16, "m_end" : 16},
		{"id" : 11, "result" : "교육기관/NNG", "target" : "교육기관", "word_id" : 11, "m_begin" : 17, "m_end" : 18}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "조선", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "왕조", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "시기", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 17, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "인재", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 24, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "양성", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 31, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 37, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "위하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 41, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 44, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "유학", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 48, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "교육", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 55, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "등", "type" : "NNB", "scode" : "05", "weight" : 1, "position" : 62, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 65, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "실시하", "type" : "VV", "scode" : "03", "weight" : 1, "position" : 69, "begin" : 13, "end" : 14},
		{"id" : 14, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 75, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "최고", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 79, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "교육기관", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 86, "begin" : 17, "end" : 18}
	],
	"word" : [
		{"id" : 0, "text" : "﻿조선", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "왕조", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "시기", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "인재", "type" : "", "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "양성을", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 5, "text" : "위해", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 6, "text" : "유학", "type" : "", "begin" : 9, "end" : 9},
		{"id" : 7, "text" : "교육", "type" : "", "begin" : 10, "end" : 10},
		{"id" : 8, "text" : "등을", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 9, "text" : "실시한", "type" : "", "begin" : 13, "end" : 15},
		{"id" : 10, "text" : "최고", "type" : "", "begin" : 16, "end" : 16},
		{"id" : 11, "text" : "교육기관", "type" : "", "begin" : 17, "end" : 18}
	],
	"NE" : [
		{"id" : 0, "text" : "조선 왕조", "type" : "DT_DYNASTY", "begin" : 1, "end" : 2, "weight" : 0.554182, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿조선", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.465859 },
		{"id" : 1, "text" : "왕조", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.914466 },
		{"id" : 2, "text" : "시기", "head" : 9, "label" : "NP_AJT", "mod" : [1], "weight" : 0.653611 },
		{"id" : 3, "text" : "인재", "head" : 4, "label" : "NP", "mod" : [], "weight" : 0.750793 },
		{"id" : 4, "text" : "양성을", "head" : 5, "label" : "NP_OBJ", "mod" : [3], "weight" : 0.845573 },
		{"id" : 5, "text" : "위해", "head" : 9, "label" : "VP", "mod" : [4], "weight" : 0.729306 },
		{"id" : 6, "text" : "유학", "head" : 7, "label" : "NP", "mod" : [], "weight" : 0.762731 },
		{"id" : 7, "text" : "교육", "head" : 8, "label" : "NP", "mod" : [6], "weight" : 0.906362 },
		{"id" : 8, "text" : "등을", "head" : 9, "label" : "NP_OBJ", "mod" : [7], "weight" : 0.851526 },
		{"id" : 9, "text" : "실시한", "head" : 11, "label" : "VP_MOD", "mod" : [2, 5, 8], "weight" : 0.651005 },
		{"id" : 10, "text" : "최고", "head" : 11, "label" : "NP", "mod" : [], "weight" : 0.563778 },
		{"id" : 11, "text" : "교육기관", "head" : -1, "label" : "NP", "mod" : [9, 10], "weight" : 0.0140534 }
	],
	"SRL" : [
		{"verb" : "위하", "sense" : 1, "word_id" : 5, "weight" : 0.245284,
			"argument" : [
				{"type" : "ARG1", "word_id" : 4, "text" : "양성을", "weight" : 0.245284 }
			] },
		{"verb" : "실시", "sense" : 1, "word_id" : 9, "weight" : 0.272692,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 2, "text" : "시기", "weight" : 0.336701 },
				{"type" : "ARGM-PRP", "word_id" : 5, "text" : "위해", "weight" : 0.2302 },
				{"type" : "ARG1", "word_id" : 8, "text" : "등을", "weight" : 0.323235 },
				{"type" : "ARG0", "word_id" : 11, "text" : "교육기관", "weight" : 0.20063 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 5, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 1 }
	]
	}
 ],
 "entity" : [
 ]
}

