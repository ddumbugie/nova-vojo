{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "김대중은 (1974년 -) 대한민국의 만화가/삽화가이다.",
	"morp" : [
		{"id" : 0, "lemma" : "김대중", "type" : "NNP", "position" : 0, "weight" : 0.9 },
		{"id" : 1, "lemma" : "은", "type" : "JX", "position" : 9, "weight" : 0.0520511 },
		{"id" : 2, "lemma" : "(", "type" : "SS", "position" : 13, "weight" : 1 },
		{"id" : 3, "lemma" : "1974", "type" : "SN", "position" : 14, "weight" : 1 },
		{"id" : 4, "lemma" : "년", "type" : "NNB", "position" : 18, "weight" : 0.414343 },
		{"id" : 5, "lemma" : "-", "type" : "SS", "position" : 22, "weight" : 1 },
		{"id" : 6, "lemma" : ")", "type" : "SS", "position" : 23, "weight" : 1 },
		{"id" : 7, "lemma" : "대한민국", "type" : "NNP", "position" : 25, "weight" : 0.0673197 },
		{"id" : 8, "lemma" : "의", "type" : "JKG", "position" : 37, "weight" : 0.0987295 },
		{"id" : 9, "lemma" : "만화", "type" : "NNG", "position" : 41, "weight" : 0.83848 },
		{"id" : 10, "lemma" : "가", "type" : "XSN", "position" : 47, "weight" : 0.000115417 },
		{"id" : 11, "lemma" : "/", "type" : "SP", "position" : 50, "weight" : 1 },
		{"id" : 12, "lemma" : "삽화", "type" : "NNG", "position" : 51, "weight" : 0.9 },
		{"id" : 13, "lemma" : "가", "type" : "XSN", "position" : 57, "weight" : 0.000115417 },
		{"id" : 14, "lemma" : "이", "type" : "VCP", "position" : 60, "weight" : 0.0165001 },
		{"id" : 15, "lemma" : "다", "type" : "EF", "position" : 63, "weight" : 0.353579 },
		{"id" : 16, "lemma" : ".", "type" : "SF", "position" : 66, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "김대중/NNG+은/JX", "target" : "김대중은", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "(/SS+1974/SN+년/NNB", "target" : "(1974년", "word_id" : 1, "m_begin" : 2, "m_end" : 4},
		{"id" : 2, "result" : "-/SS+)/SS", "target" : "-)", "word_id" : 2, "m_begin" : 5, "m_end" : 6},
		{"id" : 3, "result" : "대한민국/NNG+의/JKG", "target" : "대한민국의", "word_id" : 3, "m_begin" : 7, "m_end" : 8},
		{"id" : 4, "result" : "만화가/NNG+//SP+삽화가/NNG+이/VCP+다/EF+./SF", "target" : "만화가/삽화가이다.", "word_id" : 4, "m_begin" : 9, "m_end" : 16}
	],
	"WSD" : [
		{"id" : 0, "text" : "김대중", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 13, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "1974", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 14, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 18, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "-", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 22, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 23, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "대한민국", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 25, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 37, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "만화가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 41, "begin" : 9, "end" : 10},
		{"id" : 10, "text" : "/", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 50, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "삽화가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 51, "begin" : 12, "end" : 13},
		{"id" : 12, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 60, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 63, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 66, "begin" : 16, "end" : 16}
	],
	"word" : [
		{"id" : 0, "text" : "김대중은", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "(1974년", "type" : "", "begin" : 2, "end" : 4},
		{"id" : 2, "text" : "-)", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 3, "text" : "대한민국의", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 4, "text" : "만화가/삽화가이다.", "type" : "", "begin" : 9, "end" : 16}
	],
	"NE" : [
		{"id" : 0, "text" : "김대중", "type" : "PS_NAME", "begin" : 0, "end" : 0, "weight" : 0.503455, "common_noun" : 0},
		{"id" : 1, "text" : "1974년", "type" : "DT_YEAR", "begin" : 3, "end" : 4, "weight" : 0.635752, "common_noun" : 0},
		{"id" : 2, "text" : "대한민국의 만화가", "type" : "AFW_DOCUMENT", "begin" : 7, "end" : 10, "weight" : 0.12382, "common_noun" : 0},
		{"id" : 3, "text" : "삽화가", "type" : "CV_OCCUPATION", "begin" : 12, "end" : 13, "weight" : 0.432416, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "김대중은", "head" : 4, "label" : "NP_SBJ", "mod" : [], "weight" : 0.80258 },
		{"id" : 1, "text" : "(1974년", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.753052 },
		{"id" : 2, "text" : "-)", "head" : 4, "label" : "R_PRN", "mod" : [1], "weight" : 0.319054 },
		{"id" : 3, "text" : "대한민국의", "head" : 4, "label" : "NP_MOD", "mod" : [], "weight" : 0.451322 },
		{"id" : 4, "text" : "만화가/삽화가이다.", "head" : -1, "label" : "VNP", "mod" : [0, 2, 3], "weight" : 0.0442661 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "서울대학교 산업디자인과에서 시각디자인을 전공했다.",
	"morp" : [
		{"id" : 0, "lemma" : "서울대학교", "type" : "NNP", "position" : 67, "weight" : 0.9 },
		{"id" : 1, "lemma" : "산업", "type" : "NNG", "position" : 83, "weight" : 0.281859 },
		{"id" : 2, "lemma" : "디자인", "type" : "NNG", "position" : 89, "weight" : 0.9 },
		{"id" : 3, "lemma" : "과", "type" : "XSN", "position" : 98, "weight" : 0.0001 },
		{"id" : 4, "lemma" : "에서", "type" : "JKB", "position" : 101, "weight" : 0.121718 },
		{"id" : 5, "lemma" : "시각", "type" : "NNG", "position" : 108, "weight" : 0.9 },
		{"id" : 6, "lemma" : "디자인", "type" : "NNG", "position" : 114, "weight" : 0.9 },
		{"id" : 7, "lemma" : "을", "type" : "JKO", "position" : 123, "weight" : 0.129611 },
		{"id" : 8, "lemma" : "전공", "type" : "NNG", "position" : 127, "weight" : 0.9 },
		{"id" : 9, "lemma" : "하", "type" : "XSV", "position" : 133, "weight" : 0.0001 },
		{"id" : 10, "lemma" : "었", "type" : "EP", "position" : 133, "weight" : 0.9 },
		{"id" : 11, "lemma" : "다", "type" : "EF", "position" : 136, "weight" : 0.640954 },
		{"id" : 12, "lemma" : ".", "type" : "SF", "position" : 139, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "서울대학교/NNG", "target" : "서울대학교", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "산업디자인과/NNG+에서/JKB", "target" : "산업디자인과에서", "word_id" : 1, "m_begin" : 1, "m_end" : 4},
		{"id" : 2, "result" : "시각디자인/NNG+을/JKO", "target" : "시각디자인을", "word_id" : 2, "m_begin" : 5, "m_end" : 7},
		{"id" : 3, "result" : "전공하/VV+었/EP+다/EF+./SF", "target" : "전공했다.", "word_id" : 3, "m_begin" : 8, "m_end" : 12}
	],
	"WSD" : [
		{"id" : 0, "text" : "서울대학교", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 67, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "산업디자인", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 83, "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "과", "type" : "XSN", "scode" : "14", "weight" : 1, "position" : 98, "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 101, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "시각디자인", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 108, "begin" : 5, "end" : 6},
		{"id" : 5, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 123, "begin" : 7, "end" : 7},
		{"id" : 6, "text" : "전공하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 127, "begin" : 8, "end" : 9},
		{"id" : 7, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 133, "begin" : 10, "end" : 10},
		{"id" : 8, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 136, "begin" : 11, "end" : 11},
		{"id" : 9, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 139, "begin" : 12, "end" : 12}
	],
	"word" : [
		{"id" : 0, "text" : "서울대학교", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "산업디자인과에서", "type" : "", "begin" : 1, "end" : 4},
		{"id" : 2, "text" : "시각디자인을", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 3, "text" : "전공했다.", "type" : "", "begin" : 8, "end" : 12}
	],
	"NE" : [
		{"id" : 0, "text" : "서울대학교", "type" : "OGG_EDUCATION", "begin" : 0, "end" : 0, "weight" : 0.634658, "common_noun" : 0},
		{"id" : 1, "text" : "산업디자인", "type" : "FD_ART", "begin" : 1, "end" : 2, "weight" : 0.331415, "common_noun" : 0},
		{"id" : 2, "text" : "시각디자인", "type" : "FD_ART", "begin" : 5, "end" : 6, "weight" : 0.400657, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "서울대학교", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.80635 },
		{"id" : 1, "text" : "산업디자인과에서", "head" : 3, "label" : "NP_AJT", "mod" : [0], "weight" : 0.726464 },
		{"id" : 2, "text" : "시각디자인을", "head" : 3, "label" : "NP_OBJ", "mod" : [], "weight" : 0.498677 },
		{"id" : 3, "text" : "전공했다.", "head" : -1, "label" : "VP", "mod" : [1, 2], "weight" : 0.164765 }
	],
	"SRL" : [
		{"verb" : "전공", "sense" : 1, "word_id" : 3, "weight" : 0.193358,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 1, "text" : "산업디자인과에서", "weight" : 0.16647 },
				{"type" : "ARG1", "word_id" : 2, "text" : "시각디자인을", "weight" : 0.220246 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 3, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.864167 }
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "<자지 도시의 아름다운 추억>으로 서울애니메이션센터 사전제작지원 공모 장편만화 부문 당선되었다.",
	"morp" : [
		{"id" : 0, "lemma" : "<", "type" : "SS", "position" : 140, "weight" : 1 },
		{"id" : 1, "lemma" : "자지", "type" : "NNG", "position" : 141, "weight" : 0.180001 },
		{"id" : 2, "lemma" : "도시", "type" : "NNG", "position" : 148, "weight" : 0.184157 },
		{"id" : 3, "lemma" : "의", "type" : "JKG", "position" : 154, "weight" : 0.0694213 },
		{"id" : 4, "lemma" : "아름", "type" : "NNG", "position" : 158, "weight" : 0.106273 },
		{"id" : 5, "lemma" : "답", "type" : "XSA", "position" : 164, "weight" : 0.00029128 },
		{"id" : 6, "lemma" : "ㄴ", "type" : "ETM", "position" : 164, "weight" : 0.488779 },
		{"id" : 7, "lemma" : "추억", "type" : "NNG", "position" : 171, "weight" : 0.9 },
		{"id" : 8, "lemma" : ">", "type" : "SS", "position" : 177, "weight" : 1 },
		{"id" : 9, "lemma" : "으로", "type" : "JKB", "position" : 178, "weight" : 0.0486063 },
		{"id" : 10, "lemma" : "서울애니메이션센터", "type" : "NNP", "position" : 185, "weight" : 0.1 },
		{"id" : 11, "lemma" : "사전", "type" : "NNG", "position" : 213, "weight" : 0.281651 },
		{"id" : 12, "lemma" : "제작", "type" : "NNG", "position" : 219, "weight" : 0.9 },
		{"id" : 13, "lemma" : "지원", "type" : "NNG", "position" : 225, "weight" : 0.183669 },
		{"id" : 14, "lemma" : "공모", "type" : "NNG", "position" : 232, "weight" : 0.9 },
		{"id" : 15, "lemma" : "장편", "type" : "NNG", "position" : 239, "weight" : 0.9 },
		{"id" : 16, "lemma" : "만화", "type" : "NNG", "position" : 245, "weight" : 0.177848 },
		{"id" : 17, "lemma" : "부문", "type" : "NNG", "position" : 252, "weight" : 0.9 },
		{"id" : 18, "lemma" : "당선", "type" : "NNG", "position" : 259, "weight" : 0.9 },
		{"id" : 19, "lemma" : "되", "type" : "XSV", "position" : 265, "weight" : 0.000224177 },
		{"id" : 20, "lemma" : "었", "type" : "EP", "position" : 268, "weight" : 0.9 },
		{"id" : 21, "lemma" : "다", "type" : "EF", "position" : 271, "weight" : 0.640954 },
		{"id" : 22, "lemma" : ".", "type" : "SF", "position" : 274, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "</SS+자지/NNG", "target" : "<자지", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "도시/NNG+의/JKG", "target" : "도시의", "word_id" : 1, "m_begin" : 2, "m_end" : 3},
		{"id" : 2, "result" : "아름답/VA+ㄴ/ETM", "target" : "아름다운", "word_id" : 2, "m_begin" : 4, "m_end" : 6},
		{"id" : 3, "result" : "추억/NNG+>/SS+으로/JKB", "target" : "추억>으로", "word_id" : 3, "m_begin" : 7, "m_end" : 9},
		{"id" : 4, "result" : "서울애니메이션센터/NNG", "target" : "서울애니메이션센터", "word_id" : 4, "m_begin" : 10, "m_end" : 10},
		{"id" : 5, "result" : "사전제작지원/NNG", "target" : "사전제작지원", "word_id" : 5, "m_begin" : 11, "m_end" : 13},
		{"id" : 6, "result" : "공모/NNG", "target" : "공모", "word_id" : 6, "m_begin" : 14, "m_end" : 14},
		{"id" : 7, "result" : "장편만화/NNG", "target" : "장편만화", "word_id" : 7, "m_begin" : 15, "m_end" : 16},
		{"id" : 8, "result" : "부문/NNG", "target" : "부문", "word_id" : 8, "m_begin" : 17, "m_end" : 17},
		{"id" : 9, "result" : "당선되/VV+었/EP+다/EF+./SF", "target" : "당선되었다.", "word_id" : 9, "m_begin" : 18, "m_end" : 22}
	],
	"WSD" : [
		{"id" : 0, "text" : "<", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 140, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "자지", "type" : "NNG", "scode" : "88", "weight" : 1, "position" : 141, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "도시", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 148, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 154, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "아름답", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 158, "begin" : 4, "end" : 5},
		{"id" : 5, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 164, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "추억", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 171, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : ">", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 177, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 178, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "서울애니메이션센터", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 185, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "사전", "type" : "NNG", "scode" : "13", "weight" : 1, "position" : 213, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "제작", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 219, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "지원", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 225, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "공모", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 232, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "장편", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 239, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "만화", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 245, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "부문", "type" : "NNG", "scode" : "06", "weight" : 1, "position" : 252, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "당선되", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 259, "begin" : 18, "end" : 19},
		{"id" : 18, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 268, "begin" : 20, "end" : 20},
		{"id" : 19, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 271, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 274, "begin" : 22, "end" : 22}
	],
	"word" : [
		{"id" : 0, "text" : "<자지", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "도시의", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 2, "text" : "아름다운", "type" : "", "begin" : 4, "end" : 6},
		{"id" : 3, "text" : "추억>으로", "type" : "", "begin" : 7, "end" : 9},
		{"id" : 4, "text" : "서울애니메이션센터", "type" : "", "begin" : 10, "end" : 10},
		{"id" : 5, "text" : "사전제작지원", "type" : "", "begin" : 11, "end" : 13},
		{"id" : 6, "text" : "공모", "type" : "", "begin" : 14, "end" : 14},
		{"id" : 7, "text" : "장편만화", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 8, "text" : "부문", "type" : "", "begin" : 17, "end" : 17},
		{"id" : 9, "text" : "당선되었다.", "type" : "", "begin" : 18, "end" : 22}
	],
	"NE" : [
		{"id" : 0, "text" : "서울애니메이션센터", "type" : "OGG_ART", "begin" : 10, "end" : 10, "weight" : 0.302872, "common_noun" : 0},
		{"id" : 1, "text" : "만화", "type" : "FD_ART", "begin" : 16, "end" : 16, "weight" : 0.182562, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "<자지", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.686101 },
		{"id" : 1, "text" : "도시의", "head" : 3, "label" : "NP_MOD", "mod" : [0], "weight" : 0.97325 },
		{"id" : 2, "text" : "아름다운", "head" : 3, "label" : "VP_MOD", "mod" : [], "weight" : 0.692002 },
		{"id" : 3, "text" : "추억>으로", "head" : 9, "label" : "NP_AJT", "mod" : [1, 2], "weight" : 0.718819 },
		{"id" : 4, "text" : "서울애니메이션센터", "head" : 5, "label" : "NP", "mod" : [], "weight" : 0.62961 },
		{"id" : 5, "text" : "사전제작지원", "head" : 6, "label" : "NP", "mod" : [4], "weight" : 0.632648 },
		{"id" : 6, "text" : "공모", "head" : 7, "label" : "NP", "mod" : [5], "weight" : 0.556276 },
		{"id" : 7, "text" : "장편만화", "head" : 8, "label" : "NP", "mod" : [6], "weight" : 0.747501 },
		{"id" : 8, "text" : "부문", "head" : 9, "label" : "NP_AJT", "mod" : [7], "weight" : 0.28131 },
		{"id" : 9, "text" : "당선되었다.", "head" : -1, "label" : "VP", "mod" : [3, 8], "weight" : 0.00852368 }
	],
	"SRL" : [
		{"verb" : "아름", "sense" : 1, "word_id" : 2, "weight" : 0.361391,
			"argument" : [
				{"type" : "ARG1", "word_id" : 3, "text" : "추억>으로", "weight" : 0.361391 }
			] },
		{"verb" : "당선", "sense" : 1, "word_id" : 9, "weight" : 0.0842469,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 3, "text" : "추억>으로", "weight" : 0.0728334 },
				{"type" : "ARG2", "word_id" : 8, "text" : "부문", "weight" : 0.0956603 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 9, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.628168 }
	]
	},
	{
	"id" : 3,
	"reserve_str" : "",
	"text" : "출판사 <새만화책>의 공동발행인이며, 한겨레, 씨네21, 한겨레21 등에서 그의 작품들을 볼 수 있다.",
	"morp" : [
		{"id" : 0, "lemma" : "출판", "type" : "NNG", "position" : 275, "weight" : 0.9 },
		{"id" : 1, "lemma" : "사", "type" : "XSN", "position" : 281, "weight" : 0.00634534 },
		{"id" : 2, "lemma" : "<", "type" : "SS", "position" : 285, "weight" : 1 },
		{"id" : 3, "lemma" : "새", "type" : "NNP", "position" : 286, "weight" : 0.0001 },
		{"id" : 4, "lemma" : "만화", "type" : "NNG", "position" : 289, "weight" : 0.271325 },
		{"id" : 5, "lemma" : "책", "type" : "NNG", "position" : 295, "weight" : 0.18015 },
		{"id" : 6, "lemma" : ">", "type" : "SS", "position" : 298, "weight" : 1 },
		{"id" : 7, "lemma" : "의", "type" : "JKG", "position" : 299, "weight" : 0.0878251 },
		{"id" : 8, "lemma" : "공동", "type" : "NNG", "position" : 303, "weight" : 0.867095 },
		{"id" : 9, "lemma" : "발행", "type" : "NNG", "position" : 309, "weight" : 0.9 },
		{"id" : 10, "lemma" : "인", "type" : "XSN", "position" : 315, "weight" : 0.0198339 },
		{"id" : 11, "lemma" : "이", "type" : "VCP", "position" : 318, "weight" : 0.0165001 },
		{"id" : 12, "lemma" : "며", "type" : "EC", "position" : 321, "weight" : 0.250555 },
		{"id" : 13, "lemma" : ",", "type" : "SP", "position" : 324, "weight" : 1 },
		{"id" : 14, "lemma" : "한겨레", "type" : "NNP", "position" : 326, "weight" : 0.112783 },
		{"id" : 15, "lemma" : ",", "type" : "SP", "position" : 335, "weight" : 1 },
		{"id" : 16, "lemma" : "씨네", "type" : "NNG", "position" : 337, "weight" : 0.479188 },
		{"id" : 17, "lemma" : "21", "type" : "SN", "position" : 343, "weight" : 1 },
		{"id" : 18, "lemma" : ",", "type" : "SP", "position" : 345, "weight" : 1 },
		{"id" : 19, "lemma" : "한겨레", "type" : "NNP", "position" : 347, "weight" : 0.112783 },
		{"id" : 20, "lemma" : "21", "type" : "SN", "position" : 356, "weight" : 1 },
		{"id" : 21, "lemma" : "등", "type" : "NNB", "position" : 359, "weight" : 0.385695 },
		{"id" : 22, "lemma" : "에서", "type" : "JKB", "position" : 362, "weight" : 0.135597 },
		{"id" : 23, "lemma" : "그", "type" : "NP", "position" : 369, "weight" : 0.00637967 },
		{"id" : 24, "lemma" : "의", "type" : "JKG", "position" : 372, "weight" : 0.134696 },
		{"id" : 25, "lemma" : "작품", "type" : "NNG", "position" : 376, "weight" : 0.871348 },
		{"id" : 26, "lemma" : "들", "type" : "XSN", "position" : 382, "weight" : 0.0299841 },
		{"id" : 27, "lemma" : "을", "type" : "JKO", "position" : 385, "weight" : 0.0819193 },
		{"id" : 28, "lemma" : "보", "type" : "VV", "position" : 389, "weight" : 0.475105 },
		{"id" : 29, "lemma" : "ㄹ", "type" : "ETM", "position" : 389, "weight" : 0.300746 },
		{"id" : 30, "lemma" : "수", "type" : "NNB", "position" : 393, "weight" : 0.215617 },
		{"id" : 31, "lemma" : "있", "type" : "VA", "position" : 397, "weight" : 0.0518488 },
		{"id" : 32, "lemma" : "다", "type" : "EF", "position" : 400, "weight" : 0.132573 },
		{"id" : 33, "lemma" : ".", "type" : "SF", "position" : 403, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "출판사/NNG", "target" : "출판사", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "</SS+새만화책/NNG+>/SS+의/JKG", "target" : "<새만화책>의", "word_id" : 1, "m_begin" : 2, "m_end" : 7},
		{"id" : 2, "result" : "공동발행인/NNG+이/VCP+며/EC+,/SP", "target" : "공동발행인이며,", "word_id" : 2, "m_begin" : 8, "m_end" : 13},
		{"id" : 3, "result" : "한겨레/NNG+,/SP", "target" : "한겨레,", "word_id" : 3, "m_begin" : 14, "m_end" : 15},
		{"id" : 4, "result" : "씨네/NNG+21/SN+,/SP", "target" : "씨네21,", "word_id" : 4, "m_begin" : 16, "m_end" : 18},
		{"id" : 5, "result" : "한겨레/NNG+21/SN", "target" : "한겨레21", "word_id" : 5, "m_begin" : 19, "m_end" : 20},
		{"id" : 6, "result" : "등/NNB+에서/JKB", "target" : "등에서", "word_id" : 6, "m_begin" : 21, "m_end" : 22},
		{"id" : 7, "result" : "그/NP+의/JKG", "target" : "그의", "word_id" : 7, "m_begin" : 23, "m_end" : 24},
		{"id" : 8, "result" : "작품들/NNG+을/JKO", "target" : "작품들을", "word_id" : 8, "m_begin" : 25, "m_end" : 27},
		{"id" : 9, "result" : "보/VV+ㄹ/ETM", "target" : "볼", "word_id" : 9, "m_begin" : 28, "m_end" : 29},
		{"id" : 10, "result" : "수/NNB", "target" : "수", "word_id" : 10, "m_begin" : 30, "m_end" : 30},
		{"id" : 11, "result" : "있/VA+다/EF+./SF", "target" : "있다.", "word_id" : 11, "m_begin" : 31, "m_end" : 33}
	],
	"WSD" : [
		{"id" : 0, "text" : "출판사", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 275, "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "<", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 285, "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "새", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 286, "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "만화책", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 289, "begin" : 4, "end" : 5},
		{"id" : 4, "text" : ">", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 298, "begin" : 6, "end" : 6},
		{"id" : 5, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 299, "begin" : 7, "end" : 7},
		{"id" : 6, "text" : "공동", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 303, "begin" : 8, "end" : 8},
		{"id" : 7, "text" : "발행인", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 309, "begin" : 9, "end" : 10},
		{"id" : 8, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 318, "begin" : 11, "end" : 11},
		{"id" : 9, "text" : "며", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 321, "begin" : 12, "end" : 12},
		{"id" : 10, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 324, "begin" : 13, "end" : 13},
		{"id" : 11, "text" : "한겨레", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 326, "begin" : 14, "end" : 14},
		{"id" : 12, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 335, "begin" : 15, "end" : 15},
		{"id" : 13, "text" : "씨네", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 337, "begin" : 16, "end" : 16},
		{"id" : 14, "text" : "21", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 343, "begin" : 17, "end" : 17},
		{"id" : 15, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 345, "begin" : 18, "end" : 18},
		{"id" : 16, "text" : "한겨레", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 347, "begin" : 19, "end" : 19},
		{"id" : 17, "text" : "21", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 356, "begin" : 20, "end" : 20},
		{"id" : 18, "text" : "등", "type" : "NNB", "scode" : "05", "weight" : 1, "position" : 359, "begin" : 21, "end" : 21},
		{"id" : 19, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 362, "begin" : 22, "end" : 22},
		{"id" : 20, "text" : "그", "type" : "NP", "scode" : "01", "weight" : 1, "position" : 369, "begin" : 23, "end" : 23},
		{"id" : 21, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 372, "begin" : 24, "end" : 24},
		{"id" : 22, "text" : "작품", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 376, "begin" : 25, "end" : 25},
		{"id" : 23, "text" : "들", "type" : "XSN", "scode" : "09", "weight" : 1, "position" : 382, "begin" : 26, "end" : 26},
		{"id" : 24, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 385, "begin" : 27, "end" : 27},
		{"id" : 25, "text" : "보", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 389, "begin" : 28, "end" : 28},
		{"id" : 26, "text" : "ㄹ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 389, "begin" : 29, "end" : 29},
		{"id" : 27, "text" : "수", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 393, "begin" : 30, "end" : 30},
		{"id" : 28, "text" : "있", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 397, "begin" : 31, "end" : 31},
		{"id" : 29, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 400, "begin" : 32, "end" : 32},
		{"id" : 30, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 403, "begin" : 33, "end" : 33}
	],
	"word" : [
		{"id" : 0, "text" : "출판사", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "<새만화책>의", "type" : "", "begin" : 2, "end" : 7},
		{"id" : 2, "text" : "공동발행인이며,", "type" : "", "begin" : 8, "end" : 13},
		{"id" : 3, "text" : "한겨레,", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 4, "text" : "씨네21,", "type" : "", "begin" : 16, "end" : 18},
		{"id" : 5, "text" : "한겨레21", "type" : "", "begin" : 19, "end" : 20},
		{"id" : 6, "text" : "등에서", "type" : "", "begin" : 21, "end" : 22},
		{"id" : 7, "text" : "그의", "type" : "", "begin" : 23, "end" : 24},
		{"id" : 8, "text" : "작품들을", "type" : "", "begin" : 25, "end" : 27},
		{"id" : 9, "text" : "볼", "type" : "", "begin" : 28, "end" : 29},
		{"id" : 10, "text" : "수", "type" : "", "begin" : 30, "end" : 30},
		{"id" : 11, "text" : "있다.", "type" : "", "begin" : 31, "end" : 33}
	],
	"NE" : [
		{"id" : 0, "text" : "새만화책", "type" : "AF_WORKS", "begin" : 3, "end" : 5, "weight" : 0.278505, "common_noun" : 0},
		{"id" : 1, "text" : "한겨레", "type" : "OGG_MEDIA", "begin" : 14, "end" : 14, "weight" : 0.248494, "common_noun" : 0},
		{"id" : 2, "text" : "씨네21", "type" : "OGG_MEDIA", "begin" : 16, "end" : 17, "weight" : 0.395759, "common_noun" : 0},
		{"id" : 3, "text" : "한겨레21", "type" : "OGG_MEDIA", "begin" : 19, "end" : 20, "weight" : 0.660905, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "출판사", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.667059 },
		{"id" : 1, "text" : "<새만화책>의", "head" : 2, "label" : "NP_MOD", "mod" : [0], "weight" : 0.695633 },
		{"id" : 2, "text" : "공동발행인이며,", "head" : 9, "label" : "VNP", "mod" : [1], "weight" : 0.84906 },
		{"id" : 3, "text" : "한겨레,", "head" : 5, "label" : "NP_CNJ", "mod" : [], "weight" : 0.744253 },
		{"id" : 4, "text" : "씨네21,", "head" : 5, "label" : "NP_CNJ", "mod" : [], "weight" : 0.837704 },
		{"id" : 5, "text" : "한겨레21", "head" : 6, "label" : "NP", "mod" : [3, 4], "weight" : 0.829832 },
		{"id" : 6, "text" : "등에서", "head" : 9, "label" : "NP_AJT", "mod" : [5], "weight" : 0.997401 },
		{"id" : 7, "text" : "그의", "head" : 8, "label" : "NP_MOD", "mod" : [], "weight" : 0.977335 },
		{"id" : 8, "text" : "작품들을", "head" : 9, "label" : "NP_OBJ", "mod" : [7], "weight" : 0.767101 },
		{"id" : 9, "text" : "볼", "head" : 10, "label" : "VP_MOD", "mod" : [2, 6, 8], "weight" : 0.887204 },
		{"id" : 10, "text" : "수", "head" : 11, "label" : "NP_SBJ", "mod" : [9], "weight" : 0.492626 },
		{"id" : 11, "text" : "있다.", "head" : -1, "label" : "VP", "mod" : [10], "weight" : 0.0534418 }
	],
	"SRL" : [
		{"verb" : "보", "sense" : 1, "word_id" : 9, "weight" : 0.358023,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 6, "text" : "등에서", "weight" : 0.164483 },
				{"type" : "ARG1", "word_id" : 8, "text" : "작품들을", "weight" : 0.431731 },
				{"type" : "AUX", "word_id" : 11, "text" : "있다.", "weight" : 0.477855 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 2, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.616767 },
		{"id" : 1, "verb_wid" : 9, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.751355 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "PS_NAME", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 4, "ne_id" : 3, "text" : "김대중은 (1974년 -) 대한민국의 만화가", "start_eid_short" : 1, "end_eid_short" : 4, "text_short" : "(1974년 -) 대한민국의 만화가/삽화가이다.", "weight" : 0.012 },
		{"id" : 1, "sent_id" : 0, "start_eid" : 0, "end_eid" : 0, "ne_id" : 0, "text" : "김대중", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "김대중", "weight" : 0.007 }
	] },
	{"id" : 1, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 2, "sent_id" : 0, "start_eid" : 1, "end_eid" : 2, "ne_id" : -1, "text" : "(1974년 -)", "start_eid_short" : 1, "end_eid_short" : 2, "text_short" : "(1974년 -)", "weight" : 0.002 },
		{"id" : 6, "sent_id" : 2, "start_eid" : 0, "end_eid" : 0, "ne_id" : -1, "text" : "<자지", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "<자지", "weight" : 0.0035 }
	] },
	{"id" : 2, "type" : "OGG_ART", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 11, "sent_id" : 2, "start_eid" : 4, "end_eid" : 4, "ne_id" : 0, "text" : "서울애니메이션센터", "start_eid_short" : 4, "end_eid_short" : 4, "text_short" : "서울애니메이션센터", "weight" : 0.002 },
		{"id" : 20, "sent_id" : 3, "start_eid" : 7, "end_eid" : 7, "ne_id" : -1, "text" : "그", "start_eid_short" : 7, "end_eid_short" : 7, "text_short" : "그", "weight" : 0.006 }
	] }
 ]
}

