{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿'전봉준의 재판기록' 중에서 \"내 자신 혼자서 박해를 받았다고 해서 들고 일어섰다면 어찌 사내라고 할 수 있겠는가? ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "'", "type" : "SS", "position" : 3, "weight" : 1 },
		{"id" : 2, "lemma" : "전봉준", "type" : "NNP", "position" : 4, "weight" : 0.9 },
		{"id" : 3, "lemma" : "의", "type" : "JKG", "position" : 13, "weight" : 0.0987295 },
		{"id" : 4, "lemma" : "재판", "type" : "NNG", "position" : 17, "weight" : 0.9 },
		{"id" : 5, "lemma" : "기록", "type" : "NNG", "position" : 23, "weight" : 0.9 },
		{"id" : 6, "lemma" : "'", "type" : "SS", "position" : 29, "weight" : 1 },
		{"id" : 7, "lemma" : "중", "type" : "NNB", "position" : 31, "weight" : 0.0112257 },
		{"id" : 8, "lemma" : "에서", "type" : "JKB", "position" : 34, "weight" : 0.135597 },
		{"id" : 9, "lemma" : "\"", "type" : "SS", "position" : 41, "weight" : 1 },
		{"id" : 10, "lemma" : "나", "type" : "NP", "position" : 42, "weight" : 0.0093447 },
		{"id" : 11, "lemma" : "의", "type" : "JKG", "position" : 42, "weight" : 0.134696 },
		{"id" : 12, "lemma" : "자신", "type" : "NNG", "position" : 46, "weight" : 0.871396 },
		{"id" : 13, "lemma" : "혼자", "type" : "NNG", "position" : 53, "weight" : 0.9 },
		{"id" : 14, "lemma" : "서", "type" : "JKS", "position" : 59, "weight" : 0.00294378 },
		{"id" : 15, "lemma" : "박해", "type" : "NNG", "position" : 63, "weight" : 0.9 },
		{"id" : 16, "lemma" : "를", "type" : "JKO", "position" : 69, "weight" : 0.137686 },
		{"id" : 17, "lemma" : "받", "type" : "VV", "position" : 73, "weight" : 0.765648 },
		{"id" : 18, "lemma" : "았", "type" : "EP", "position" : 76, "weight" : 0.9 },
		{"id" : 19, "lemma" : "다고", "type" : "EC", "position" : 79, "weight" : 0.193823 },
		{"id" : 20, "lemma" : "하", "type" : "VV", "position" : 86, "weight" : 0.140183 },
		{"id" : 21, "lemma" : "어서", "type" : "EC", "position" : 86, "weight" : 0.421998 },
		{"id" : 22, "lemma" : "들", "type" : "VV", "position" : 93, "weight" : 0.0265776 },
		{"id" : 23, "lemma" : "고", "type" : "EC", "position" : 96, "weight" : 0.416679 },
		{"id" : 24, "lemma" : "일어서", "type" : "VV", "position" : 100, "weight" : 0.9 },
		{"id" : 25, "lemma" : "었", "type" : "EP", "position" : 106, "weight" : 0.9 },
		{"id" : 26, "lemma" : "다면", "type" : "EC", "position" : 109, "weight" : 0.193062 },
		{"id" : 27, "lemma" : "어찌", "type" : "MAG", "position" : 116, "weight" : 0.0471716 },
		{"id" : 28, "lemma" : "사내", "type" : "NNG", "position" : 123, "weight" : 0.9 },
		{"id" : 29, "lemma" : "이", "type" : "VCP", "position" : 126, "weight" : 0.0177525 },
		{"id" : 30, "lemma" : "라고", "type" : "EC", "position" : 129, "weight" : 0.204147 },
		{"id" : 31, "lemma" : "하", "type" : "VV", "position" : 136, "weight" : 0.140183 },
		{"id" : 32, "lemma" : "ㄹ", "type" : "ETM", "position" : 136, "weight" : 0.300746 },
		{"id" : 33, "lemma" : "수", "type" : "NNB", "position" : 140, "weight" : 0.215617 },
		{"id" : 34, "lemma" : "있", "type" : "VA", "position" : 144, "weight" : 0.0518488 },
		{"id" : 35, "lemma" : "겠", "type" : "EP", "position" : 147, "weight" : 0.9 },
		{"id" : 36, "lemma" : "는가", "type" : "EF", "position" : 150, "weight" : 0.401762 },
		{"id" : 37, "lemma" : "?", "type" : "SF", "position" : 156, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿/NNG+'/SS+전봉준/NNG+의/JKG", "target" : "﻿'전봉준의", "word_id" : 0, "m_begin" : 0, "m_end" : 3},
		{"id" : 1, "result" : "재판기록/NNG+'/SS", "target" : "재판기록'", "word_id" : 1, "m_begin" : 4, "m_end" : 6},
		{"id" : 2, "result" : "중/NNB+에서/JKB", "target" : "중에서", "word_id" : 2, "m_begin" : 7, "m_end" : 8},
		{"id" : 3, "result" : "\"/SS+나/NP+의/JKG", "target" : "\"내", "word_id" : 3, "m_begin" : 9, "m_end" : 11},
		{"id" : 4, "result" : "자신/NNG", "target" : "자신", "word_id" : 4, "m_begin" : 12, "m_end" : 12},
		{"id" : 5, "result" : "혼자/NNG+서/JKS", "target" : "혼자서", "word_id" : 5, "m_begin" : 13, "m_end" : 14},
		{"id" : 6, "result" : "박해/NNG+를/JKO", "target" : "박해를", "word_id" : 6, "m_begin" : 15, "m_end" : 16},
		{"id" : 7, "result" : "받/VV+었/EP+다고/EC", "target" : "받았다고", "word_id" : 7, "m_begin" : 17, "m_end" : 19},
		{"id" : 8, "result" : "하/VV+어서/EC", "target" : "해서", "word_id" : 8, "m_begin" : 20, "m_end" : 21},
		{"id" : 9, "result" : "들/VV+고/EC", "target" : "들고", "word_id" : 9, "m_begin" : 22, "m_end" : 23},
		{"id" : 10, "result" : "일어서/VV+었/EP+다면/EC", "target" : "일어섰다면", "word_id" : 10, "m_begin" : 24, "m_end" : 26},
		{"id" : 11, "result" : "어찌/MAG", "target" : "어찌", "word_id" : 11, "m_begin" : 27, "m_end" : 27},
		{"id" : 12, "result" : "사내/NNG+이/VCP+라고/EC", "target" : "사내라고", "word_id" : 12, "m_begin" : 28, "m_end" : 30},
		{"id" : 13, "result" : "하/VV+ㄹ/ETM", "target" : "할", "word_id" : 13, "m_begin" : 31, "m_end" : 32},
		{"id" : 14, "result" : "수/NNB", "target" : "수", "word_id" : 14, "m_begin" : 33, "m_end" : 33},
		{"id" : 15, "result" : "있/VA+겠/EP+는가/EF+?/SF", "target" : "있겠는가?", "word_id" : 15, "m_begin" : 34, "m_end" : 37}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "전봉준", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 4, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 13, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "재판", "type" : "NNG", "scode" : "06", "weight" : 1, "position" : 17, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "기록", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 23, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 29, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "중", "type" : "NNB", "scode" : "04", "weight" : 1, "position" : 31, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 34, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "\"", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 41, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "나", "type" : "NP", "scode" : "03", "weight" : 1, "position" : 42, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 42, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "자신", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 46, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "혼자", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 53, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "서", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 59, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "박해", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 63, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 69, "begin" : 16, "end" : 16},
		{"id" : 17, "text" : "받", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 73, "begin" : 17, "end" : 17},
		{"id" : 18, "text" : "았", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 76, "begin" : 18, "end" : 18},
		{"id" : 19, "text" : "다고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 79, "begin" : 19, "end" : 19},
		{"id" : 20, "text" : "하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 86, "begin" : 20, "end" : 20},
		{"id" : 21, "text" : "어서", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 86, "begin" : 21, "end" : 21},
		{"id" : 22, "text" : "들", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 93, "begin" : 22, "end" : 22},
		{"id" : 23, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 96, "begin" : 23, "end" : 23},
		{"id" : 24, "text" : "일어서", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 100, "begin" : 24, "end" : 24},
		{"id" : 25, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 106, "begin" : 25, "end" : 25},
		{"id" : 26, "text" : "다면", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 109, "begin" : 26, "end" : 26},
		{"id" : 27, "text" : "어찌", "type" : "MAG", "scode" : "00", "weight" : 1, "position" : 116, "begin" : 27, "end" : 27},
		{"id" : 28, "text" : "사내", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 123, "begin" : 28, "end" : 28},
		{"id" : 29, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 126, "begin" : 29, "end" : 29},
		{"id" : 30, "text" : "라고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 129, "begin" : 30, "end" : 30},
		{"id" : 31, "text" : "하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 136, "begin" : 31, "end" : 31},
		{"id" : 32, "text" : "ㄹ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 136, "begin" : 32, "end" : 32},
		{"id" : 33, "text" : "수", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 140, "begin" : 33, "end" : 33},
		{"id" : 34, "text" : "있", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 144, "begin" : 34, "end" : 34},
		{"id" : 35, "text" : "겠", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 147, "begin" : 35, "end" : 35},
		{"id" : 36, "text" : "는가", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 150, "begin" : 36, "end" : 36},
		{"id" : 37, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 156, "begin" : 37, "end" : 37}
	],
	"word" : [
		{"id" : 0, "text" : "﻿'전봉준의", "type" : "", "begin" : 0, "end" : 3},
		{"id" : 1, "text" : "재판기록'", "type" : "", "begin" : 4, "end" : 6},
		{"id" : 2, "text" : "중에서", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 3, "text" : "\"내", "type" : "", "begin" : 9, "end" : 11},
		{"id" : 4, "text" : "자신", "type" : "", "begin" : 12, "end" : 12},
		{"id" : 5, "text" : "혼자서", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 6, "text" : "박해를", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 7, "text" : "받았다고", "type" : "", "begin" : 17, "end" : 19},
		{"id" : 8, "text" : "해서", "type" : "", "begin" : 20, "end" : 21},
		{"id" : 9, "text" : "들고", "type" : "", "begin" : 22, "end" : 23},
		{"id" : 10, "text" : "일어섰다면", "type" : "", "begin" : 24, "end" : 26},
		{"id" : 11, "text" : "어찌", "type" : "", "begin" : 27, "end" : 27},
		{"id" : 12, "text" : "사내라고", "type" : "", "begin" : 28, "end" : 30},
		{"id" : 13, "text" : "할", "type" : "", "begin" : 31, "end" : 32},
		{"id" : 14, "text" : "수", "type" : "", "begin" : 33, "end" : 33},
		{"id" : 15, "text" : "있겠는가?", "type" : "", "begin" : 34, "end" : 37}
	],
	"NE" : [
		{"id" : 0, "text" : "전봉준", "type" : "PS_NAME", "begin" : 2, "end" : 2, "weight" : 0.247636, "common_noun" : 0},
		{"id" : 1, "text" : "혼자", "type" : "QT_MAN_COUNT", "begin" : 13, "end" : 13, "weight" : 0.167493, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿'전봉준의", "head" : 1, "label" : "NP_MOD", "mod" : [], "weight" : 0.9549 },
		{"id" : 1, "text" : "재판기록'", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.958943 },
		{"id" : 2, "text" : "중에서", "head" : 15, "label" : "NP_AJT", "mod" : [1], "weight" : 0.812404 },
		{"id" : 3, "text" : "\"내", "head" : 4, "label" : "NP_MOD", "mod" : [], "weight" : 0.684136 },
		{"id" : 4, "text" : "자신", "head" : 5, "label" : "NP", "mod" : [3], "weight" : 0.789722 },
		{"id" : 5, "text" : "혼자서", "head" : 7, "label" : "NP_SBJ", "mod" : [4], "weight" : 0.84052 },
		{"id" : 6, "text" : "박해를", "head" : 7, "label" : "NP_OBJ", "mod" : [], "weight" : 0.810331 },
		{"id" : 7, "text" : "받았다고", "head" : 8, "label" : "VP_CMP", "mod" : [5, 6], "weight" : 0.910678 },
		{"id" : 8, "text" : "해서", "head" : 9, "label" : "VP", "mod" : [7], "weight" : 0.913442 },
		{"id" : 9, "text" : "들고", "head" : 10, "label" : "VP", "mod" : [8], "weight" : 0.858073 },
		{"id" : 10, "text" : "일어섰다면", "head" : 12, "label" : "VP", "mod" : [9], "weight" : 0.704362 },
		{"id" : 11, "text" : "어찌", "head" : 12, "label" : "AP", "mod" : [], "weight" : 0.78514 },
		{"id" : 12, "text" : "사내라고", "head" : 13, "label" : "VNP_CMP", "mod" : [10, 11], "weight" : 0.716757 },
		{"id" : 13, "text" : "할", "head" : 14, "label" : "VP_MOD", "mod" : [12], "weight" : 0.936283 },
		{"id" : 14, "text" : "수", "head" : 15, "label" : "NP_SBJ", "mod" : [13], "weight" : 0.515163 },
		{"id" : 15, "text" : "있겠는가?", "head" : -1, "label" : "VP", "mod" : [2, 14], "weight" : 0.0288024 }
	],
	"SRL" : [
		{"verb" : "받", "sense" : 1, "word_id" : 7, "weight" : 0.222293,
			"argument" : [
				{"type" : "ARGM-EXT", "word_id" : 2, "text" : "중에서", "weight" : 0.198413 },
				{"type" : "ARG0", "word_id" : 5, "text" : "혼자서", "weight" : 0.238286 },
				{"type" : "ARG1", "word_id" : 6, "text" : "박해를", "weight" : 0.23018 }
			] },
		{"verb" : "하", "sense" : 2, "word_id" : 8, "weight" : 0.269941,
			"argument" : [
				{"type" : "ARG1", "word_id" : 7, "text" : "받았다고", "weight" : 0.269941 }
			] },
		{"verb" : "일어서", "sense" : 1, "word_id" : 10, "weight" : 0.294117,
			"argument" : [
				{"type" : "ARGM-CAU", "word_id" : 8, "text" : "해서", "weight" : 0.294117 }
			] },
		{"verb" : "하", "sense" : 1, "word_id" : 13, "weight" : 0.289773,
			"argument" : [
				{"type" : "ARG2", "word_id" : 12, "text" : "사내라고", "weight" : 0.17092 },
				{"type" : "AUX", "word_id" : 15, "text" : "있겠는가?", "weight" : 0.408626 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 9, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.62258 },
		{"id" : 1, "verb_wid" : 10, "ant_sid" : 0, "ant_wid" : 5, "type" : "s", "istitle" : 0, "weight" : 0.399351 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "모든 백성의 원한이기 때문에 백성을 위하여 해독을 제거하려는 것이다. ",
	"morp" : [
		{"id" : 0, "lemma" : "모든", "type" : "MM", "position" : 158, "weight" : 0.9 },
		{"id" : 1, "lemma" : "백성", "type" : "NNG", "position" : 165, "weight" : 0.9 },
		{"id" : 2, "lemma" : "의", "type" : "JKG", "position" : 171, "weight" : 0.0694213 },
		{"id" : 3, "lemma" : "원한", "type" : "NNG", "position" : 175, "weight" : 0.9 },
		{"id" : 4, "lemma" : "이", "type" : "VCP", "position" : 181, "weight" : 0.0177525 },
		{"id" : 5, "lemma" : "기", "type" : "ETN", "position" : 184, "weight" : 0.019088 },
		{"id" : 6, "lemma" : "때문", "type" : "NNB", "position" : 188, "weight" : 0.9 },
		{"id" : 7, "lemma" : "에", "type" : "JKB", "position" : 194, "weight" : 0.135559 },
		{"id" : 8, "lemma" : "백성", "type" : "NNG", "position" : 198, "weight" : 0.9 },
		{"id" : 9, "lemma" : "을", "type" : "JKO", "position" : 204, "weight" : 0.129611 },
		{"id" : 10, "lemma" : "위하", "type" : "VV", "position" : 208, "weight" : 0.778555 },
		{"id" : 11, "lemma" : "어", "type" : "EC", "position" : 214, "weight" : 0.41831 },
		{"id" : 12, "lemma" : "해독", "type" : "NNG", "position" : 218, "weight" : 0.9 },
		{"id" : 13, "lemma" : "을", "type" : "JKO", "position" : 224, "weight" : 0.129611 },
		{"id" : 14, "lemma" : "제거", "type" : "NNG", "position" : 228, "weight" : 0.9 },
		{"id" : 15, "lemma" : "하", "type" : "XSV", "position" : 234, "weight" : 0.0001 },
		{"id" : 16, "lemma" : "려는", "type" : "ETM", "position" : 237, "weight" : 0.9 },
		{"id" : 17, "lemma" : "것", "type" : "NNB", "position" : 244, "weight" : 0.228788 },
		{"id" : 18, "lemma" : "이", "type" : "VCP", "position" : 247, "weight" : 0.0607843 },
		{"id" : 19, "lemma" : "다", "type" : "EF", "position" : 250, "weight" : 0.353579 },
		{"id" : 20, "lemma" : ".", "type" : "SF", "position" : 253, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "모든/MM", "target" : "모든", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "백성/NNG+의/JKG", "target" : "백성의", "word_id" : 1, "m_begin" : 1, "m_end" : 2},
		{"id" : 2, "result" : "원한/NNG+이/VCP+기/ETN", "target" : "원한이기", "word_id" : 2, "m_begin" : 3, "m_end" : 5},
		{"id" : 3, "result" : "때문/NNB+에/JKB", "target" : "때문에", "word_id" : 3, "m_begin" : 6, "m_end" : 7},
		{"id" : 4, "result" : "백성/NNG+을/JKO", "target" : "백성을", "word_id" : 4, "m_begin" : 8, "m_end" : 9},
		{"id" : 5, "result" : "위하/VV+어/EC", "target" : "위하여", "word_id" : 5, "m_begin" : 10, "m_end" : 11},
		{"id" : 6, "result" : "해독/NNG+을/JKO", "target" : "해독을", "word_id" : 6, "m_begin" : 12, "m_end" : 13},
		{"id" : 7, "result" : "제거하/VV+려는/ETM", "target" : "제거하려는", "word_id" : 7, "m_begin" : 14, "m_end" : 16},
		{"id" : 8, "result" : "것/NNB+이/VCP+다/EF+./SF", "target" : "것이다.", "word_id" : 8, "m_begin" : 17, "m_end" : 20}
	],
	"WSD" : [
		{"id" : 0, "text" : "모든", "type" : "MM", "scode" : "00", "weight" : 1, "position" : 158, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "백성", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 165, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 171, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "원한", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 175, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 181, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "기", "type" : "ETN", "scode" : "00", "weight" : 1, "position" : 184, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "때문", "type" : "NNB", "scode" : "00", "weight" : 1, "position" : 188, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 194, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "백성", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 198, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 204, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "위하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 208, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 214, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "해독", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 218, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 224, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "제거하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 228, "begin" : 14, "end" : 15},
		{"id" : 15, "text" : "려는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 237, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "것", "type" : "NNB", "scode" : "01", "weight" : 1, "position" : 244, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 247, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 250, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 253, "begin" : 20, "end" : 20}
	],
	"word" : [
		{"id" : 0, "text" : "모든", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "백성의", "type" : "", "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "원한이기", "type" : "", "begin" : 3, "end" : 5},
		{"id" : 3, "text" : "때문에", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 4, "text" : "백성을", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 5, "text" : "위하여", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 6, "text" : "해독을", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 7, "text" : "제거하려는", "type" : "", "begin" : 14, "end" : 16},
		{"id" : 8, "text" : "것이다.", "type" : "", "begin" : 17, "end" : 20}
	],
	"NE" : [
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "모든", "head" : 1, "label" : "DP", "mod" : [], "weight" : 0.683932 },
		{"id" : 1, "text" : "백성의", "head" : 2, "label" : "NP_MOD", "mod" : [0], "weight" : 0.987166 },
		{"id" : 2, "text" : "원한이기", "head" : 3, "label" : "VNP", "mod" : [1], "weight" : 0.898368 },
		{"id" : 3, "text" : "때문에", "head" : 7, "label" : "NP_AJT", "mod" : [2], "weight" : 0.924975 },
		{"id" : 4, "text" : "백성을", "head" : 5, "label" : "NP_OBJ", "mod" : [], "weight" : 0.877301 },
		{"id" : 5, "text" : "위하여", "head" : 7, "label" : "VP", "mod" : [4], "weight" : 0.652166 },
		{"id" : 6, "text" : "해독을", "head" : 7, "label" : "NP_OBJ", "mod" : [], "weight" : 0.545633 },
		{"id" : 7, "text" : "제거하려는", "head" : 8, "label" : "VP_MOD", "mod" : [3, 5, 6], "weight" : 0.541454 },
		{"id" : 8, "text" : "것이다.", "head" : -1, "label" : "VNP", "mod" : [7], "weight" : 0.0578151 }
	],
	"SRL" : [
		{"verb" : "위하", "sense" : 1, "word_id" : 5, "weight" : 0.275511,
			"argument" : [
				{"type" : "ARG1", "word_id" : 4, "text" : "백성을", "weight" : 0.275511 }
			] },
		{"verb" : "제거", "sense" : 1, "word_id" : 7, "weight" : 0.26047,
			"argument" : [
				{"type" : "ARGM-CAU", "word_id" : 3, "text" : "때문에", "weight" : 0.322738 },
				{"type" : "ARGM-PRP", "word_id" : 5, "text" : "위하여", "weight" : 0.192545 },
				{"type" : "ARG1", "word_id" : 6, "text" : "해독을", "weight" : 0.266127 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 2, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.747451 },
		{"id" : 1, "verb_wid" : 5, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.655803 }
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "\" 이처럼 조선 왕조 시기의 형사사건에서 죄인을 심문한 내용을 기록한 것을 뜻하는 말은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "\"", "type" : "SS", "position" : 255, "weight" : 1 },
		{"id" : 1, "lemma" : "이", "type" : "NP", "position" : 257, "weight" : 0.000471436 },
		{"id" : 2, "lemma" : "처럼", "type" : "JKB", "position" : 260, "weight" : 0.9 },
		{"id" : 3, "lemma" : "조선", "type" : "NNP", "position" : 267, "weight" : 0.0226541 },
		{"id" : 4, "lemma" : "왕조", "type" : "NNG", "position" : 274, "weight" : 0.9 },
		{"id" : 5, "lemma" : "시기", "type" : "NNG", "position" : 281, "weight" : 0.184726 },
		{"id" : 6, "lemma" : "의", "type" : "JKG", "position" : 287, "weight" : 0.0694213 },
		{"id" : 7, "lemma" : "형사", "type" : "NNG", "position" : 291, "weight" : 0.9 },
		{"id" : 8, "lemma" : "사건", "type" : "NNG", "position" : 297, "weight" : 0.9 },
		{"id" : 9, "lemma" : "에서", "type" : "JKB", "position" : 303, "weight" : 0.153407 },
		{"id" : 10, "lemma" : "죄인", "type" : "NNG", "position" : 310, "weight" : 0.9 },
		{"id" : 11, "lemma" : "을", "type" : "JKO", "position" : 316, "weight" : 0.129611 },
		{"id" : 12, "lemma" : "심문", "type" : "NNG", "position" : 320, "weight" : 0.9 },
		{"id" : 13, "lemma" : "하", "type" : "XSV", "position" : 326, "weight" : 0.0001 },
		{"id" : 14, "lemma" : "ㄴ", "type" : "ETM", "position" : 326, "weight" : 0.392321 },
		{"id" : 15, "lemma" : "내용", "type" : "NNG", "position" : 330, "weight" : 0.658704 },
		{"id" : 16, "lemma" : "을", "type" : "JKO", "position" : 336, "weight" : 0.129611 },
		{"id" : 17, "lemma" : "기록", "type" : "NNG", "position" : 340, "weight" : 0.9 },
		{"id" : 18, "lemma" : "하", "type" : "XSV", "position" : 346, "weight" : 0.0001 },
		{"id" : 19, "lemma" : "ㄴ", "type" : "ETM", "position" : 346, "weight" : 0.392321 },
		{"id" : 20, "lemma" : "것", "type" : "NNB", "position" : 350, "weight" : 0.228788 },
		{"id" : 21, "lemma" : "을", "type" : "JKO", "position" : 353, "weight" : 0.0630104 },
		{"id" : 22, "lemma" : "뜻", "type" : "NNG", "position" : 357, "weight" : 0.9 },
		{"id" : 23, "lemma" : "하", "type" : "XSV", "position" : 360, "weight" : 0.0001 },
		{"id" : 24, "lemma" : "는", "type" : "ETM", "position" : 363, "weight" : 0.238503 },
		{"id" : 25, "lemma" : "말", "type" : "NNG", "position" : 367, "weight" : 0.528926 },
		{"id" : 26, "lemma" : "은", "type" : "JX", "position" : 370, "weight" : 0.0449928 },
		{"id" : 27, "lemma" : "무엇", "type" : "NP", "position" : 374, "weight" : 0.9 },
		{"id" : 28, "lemma" : "이", "type" : "VCP", "position" : 380, "weight" : 0.0175768 },
		{"id" : 29, "lemma" : "ㄹ까", "type" : "EF", "position" : 380, "weight" : 0.258243 },
		{"id" : 30, "lemma" : "?", "type" : "SF", "position" : 386, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "\"/SS", "target" : "\"", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "이/NP+처럼/JKB", "target" : "이처럼", "word_id" : 1, "m_begin" : 1, "m_end" : 2},
		{"id" : 2, "result" : "조선/NNG", "target" : "조선", "word_id" : 2, "m_begin" : 3, "m_end" : 3},
		{"id" : 3, "result" : "왕조/NNG", "target" : "왕조", "word_id" : 3, "m_begin" : 4, "m_end" : 4},
		{"id" : 4, "result" : "시기/NNG+의/JKG", "target" : "시기의", "word_id" : 4, "m_begin" : 5, "m_end" : 6},
		{"id" : 5, "result" : "형사사건/NNG+에서/JKB", "target" : "형사사건에서", "word_id" : 5, "m_begin" : 7, "m_end" : 9},
		{"id" : 6, "result" : "죄인/NNG+을/JKO", "target" : "죄인을", "word_id" : 6, "m_begin" : 10, "m_end" : 11},
		{"id" : 7, "result" : "심문하/VV+ㄴ/ETM", "target" : "심문한", "word_id" : 7, "m_begin" : 12, "m_end" : 14},
		{"id" : 8, "result" : "내용/NNG+을/JKO", "target" : "내용을", "word_id" : 8, "m_begin" : 15, "m_end" : 16},
		{"id" : 9, "result" : "기록하/VV+ㄴ/ETM", "target" : "기록한", "word_id" : 9, "m_begin" : 17, "m_end" : 19},
		{"id" : 10, "result" : "것/NNB+을/JKO", "target" : "것을", "word_id" : 10, "m_begin" : 20, "m_end" : 21},
		{"id" : 11, "result" : "뜻하/VV+는/ETM", "target" : "뜻하는", "word_id" : 11, "m_begin" : 22, "m_end" : 24},
		{"id" : 12, "result" : "말/NNG+은/JX", "target" : "말은", "word_id" : 12, "m_begin" : 25, "m_end" : 26},
		{"id" : 13, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 13, "m_begin" : 27, "m_end" : 30}
	],
	"WSD" : [
		{"id" : 0, "text" : "\"", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 255, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "이", "type" : "NP", "scode" : "05", "weight" : 1, "position" : 257, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "처럼", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 260, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "조선", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 267, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "왕조", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 274, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "시기", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 281, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 287, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "형사사건", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 291, "begin" : 7, "end" : 8},
		{"id" : 8, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 303, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "죄인", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 310, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 316, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "심문하", "type" : "VV", "scode" : "02", "weight" : 1, "position" : 320, "begin" : 12, "end" : 13},
		{"id" : 12, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 326, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "내용", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 330, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 336, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : "기록하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 340, "begin" : 17, "end" : 18},
		{"id" : 16, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 346, "begin" : 19, "end" : 19},
		{"id" : 17, "text" : "것", "type" : "NNB", "scode" : "01", "weight" : 1, "position" : 350, "begin" : 20, "end" : 20},
		{"id" : 18, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 353, "begin" : 21, "end" : 21},
		{"id" : 19, "text" : "뜻하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 357, "begin" : 22, "end" : 23},
		{"id" : 20, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 363, "begin" : 24, "end" : 24},
		{"id" : 21, "text" : "말", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 367, "begin" : 25, "end" : 25},
		{"id" : 22, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 370, "begin" : 26, "end" : 26},
		{"id" : 23, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 374, "begin" : 27, "end" : 27},
		{"id" : 24, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 380, "begin" : 28, "end" : 28},
		{"id" : 25, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 380, "begin" : 29, "end" : 29},
		{"id" : 26, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 386, "begin" : 30, "end" : 30}
	],
	"word" : [
		{"id" : 0, "text" : "\"", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "이처럼", "type" : "", "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "조선", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "왕조", "type" : "", "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "시기의", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 5, "text" : "형사사건에서", "type" : "", "begin" : 7, "end" : 9},
		{"id" : 6, "text" : "죄인을", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 7, "text" : "심문한", "type" : "", "begin" : 12, "end" : 14},
		{"id" : 8, "text" : "내용을", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 9, "text" : "기록한", "type" : "", "begin" : 17, "end" : 19},
		{"id" : 10, "text" : "것을", "type" : "", "begin" : 20, "end" : 21},
		{"id" : 11, "text" : "뜻하는", "type" : "", "begin" : 22, "end" : 24},
		{"id" : 12, "text" : "말은", "type" : "", "begin" : 25, "end" : 26},
		{"id" : 13, "text" : "무엇일까?", "type" : "", "begin" : 27, "end" : 30}
	],
	"NE" : [
		{"id" : 0, "text" : "조선 왕조", "type" : "DT_DYNASTY", "begin" : 3, "end" : 4, "weight" : 0.622423, "common_noun" : 0},
		{"id" : 1, "text" : "형사", "type" : "CV_OCCUPATION", "begin" : 7, "end" : 7, "weight" : 0.24918, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "\"", "head" : 13, "label" : "L", "mod" : [], "weight" : 0.750963 },
		{"id" : 1, "text" : "이처럼", "head" : 9, "label" : "NP_AJT", "mod" : [], "weight" : 0.719845 },
		{"id" : 2, "text" : "조선", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.871711 },
		{"id" : 3, "text" : "왕조", "head" : 4, "label" : "NP", "mod" : [2], "weight" : 0.957277 },
		{"id" : 4, "text" : "시기의", "head" : 5, "label" : "NP_MOD", "mod" : [3], "weight" : 0.633181 },
		{"id" : 5, "text" : "형사사건에서", "head" : 9, "label" : "NP_AJT", "mod" : [4], "weight" : 0.617099 },
		{"id" : 6, "text" : "죄인을", "head" : 7, "label" : "NP_OBJ", "mod" : [], "weight" : 0.889182 },
		{"id" : 7, "text" : "심문한", "head" : 8, "label" : "VP_MOD", "mod" : [6], "weight" : 0.881348 },
		{"id" : 8, "text" : "내용을", "head" : 9, "label" : "NP_OBJ", "mod" : [7], "weight" : 0.893725 },
		{"id" : 9, "text" : "기록한", "head" : 10, "label" : "VP_MOD", "mod" : [1, 5, 8], "weight" : 0.906468 },
		{"id" : 10, "text" : "것을", "head" : 11, "label" : "NP_OBJ", "mod" : [9], "weight" : 0.830384 },
		{"id" : 11, "text" : "뜻하는", "head" : 12, "label" : "VP_MOD", "mod" : [10], "weight" : 0.837336 },
		{"id" : 12, "text" : "말은", "head" : 13, "label" : "NP_SBJ", "mod" : [11], "weight" : 0.782217 },
		{"id" : 13, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [0, 12], "weight" : 0.045357 }
	],
	"SRL" : [
		{"verb" : "심문", "sense" : 1, "word_id" : 7, "weight" : 0.193796,
			"argument" : [
				{"type" : "ARG1", "word_id" : 6, "text" : "죄인을", "weight" : 0.193796 }
			] },
		{"verb" : "기록", "sense" : 1, "word_id" : 9, "weight" : 0.290455,
			"argument" : [
				{"type" : "ARGM-DIS", "word_id" : 1, "text" : "이처럼", "weight" : 0.198947 },
				{"type" : "ARGM-LOC", "word_id" : 5, "text" : "형사사건에서", "weight" : 0.157163 },
				{"type" : "ARG1", "word_id" : 8, "text" : "내용을", "weight" : 0.515254 }
			] },
		{"verb" : "뜻", "sense" : 1, "word_id" : 11, "weight" : 0.316769,
			"argument" : [
				{"type" : "ARG2", "word_id" : 10, "text" : "것을", "weight" : 0.427972 },
				{"type" : "ARG1", "word_id" : 12, "text" : "말은", "weight" : 0.205566 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "QT_MAN_COUNT", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 2, "sent_id" : 0, "start_eid" : 3, "end_eid" : 5, "ne_id" : 1, "text" : "\"내 자신 혼자", "start_eid_short" : 3, "end_eid_short" : 5, "text_short" : "\"내 자신 혼자", "weight" : 0.002 },
		{"id" : 3, "sent_id" : 0, "start_eid" : 3, "end_eid" : 4, "ne_id" : -1, "text" : "\"내 자신", "start_eid_short" : 3, "end_eid_short" : 4, "text_short" : "\"내 자신", "weight" : 0.006 }
	] },
	{"id" : 1, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 8, "sent_id" : 1, "start_eid" : 0, "end_eid" : 1, "ne_id" : -1, "text" : "모든 백성", "start_eid_short" : 0, "end_eid_short" : 1, "text_short" : "모든 백성", "weight" : 0.006 },
		{"id" : 11, "sent_id" : 1, "start_eid" : 4, "end_eid" : 4, "ne_id" : -1, "text" : "백성", "start_eid_short" : 4, "end_eid_short" : 4, "text_short" : "백성", "weight" : 0.01 }
	] },
	{"id" : 2, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 13, "sent_id" : 2, "start_eid" : 1, "end_eid" : 12, "ne_id" : -1, "text" : "이처럼 조선 왕조 시기의 형사사건에서 죄인을 심문한 내용을 기록한 것을 뜻하는 말", "start_eid_short" : 12, "end_eid_short" : 12, "text_short" : "말", "weight" : 0.002 },
		{"id" : 20, "sent_id" : 2, "start_eid" : 13, "end_eid" : 13, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 13, "end_eid_short" : 13, "text_short" : "무엇이ㄹ까?", "weight" : 0.006 }
	] }
 ]
}

