{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "1944년 이 사람은 조선건국동맹을 조직해 일본의 패망을 예견하고 해방을 준비했다.",
	"morp" : [
		{"id" : 0, "lemma" : "1944", "type" : "SN", "position" : 0, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 4, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "이", "type" : "MM", "position" : 8, "weight" : 0.000222516 },
		{"id" : 3, "lemma" : "사람", "type" : "NNG", "position" : 12, "weight" : 0.737436 },
		{"id" : 4, "lemma" : "은", "type" : "JX", "position" : 18, "weight" : 0.0449928 },
		{"id" : 5, "lemma" : "조선", "type" : "NNG", "position" : 22, "weight" : 0.00516349 },
		{"id" : 6, "lemma" : "건국", "type" : "NNG", "position" : 28, "weight" : 0.184155 },
		{"id" : 7, "lemma" : "동맹", "type" : "NNG", "position" : 34, "weight" : 0.9 },
		{"id" : 8, "lemma" : "을", "type" : "JKO", "position" : 40, "weight" : 0.129611 },
		{"id" : 9, "lemma" : "조직", "type" : "NNG", "position" : 44, "weight" : 0.101381 },
		{"id" : 10, "lemma" : "하", "type" : "XSV", "position" : 50, "weight" : 0.0001 },
		{"id" : 11, "lemma" : "어", "type" : "EC", "position" : 50, "weight" : 0.361326 },
		{"id" : 12, "lemma" : "일본", "type" : "NNP", "position" : 54, "weight" : 0.0219717 },
		{"id" : 13, "lemma" : "의", "type" : "JKG", "position" : 60, "weight" : 0.0987295 },
		{"id" : 14, "lemma" : "패망", "type" : "NNG", "position" : 64, "weight" : 0.9 },
		{"id" : 15, "lemma" : "을", "type" : "JKO", "position" : 70, "weight" : 0.129611 },
		{"id" : 16, "lemma" : "예견", "type" : "NNG", "position" : 74, "weight" : 0.9 },
		{"id" : 17, "lemma" : "하", "type" : "XSV", "position" : 80, "weight" : 0.0001 },
		{"id" : 18, "lemma" : "고", "type" : "EC", "position" : 83, "weight" : 0.359917 },
		{"id" : 19, "lemma" : "해방", "type" : "NNG", "position" : 87, "weight" : 0.9 },
		{"id" : 20, "lemma" : "을", "type" : "JKO", "position" : 93, "weight" : 0.129611 },
		{"id" : 21, "lemma" : "준비", "type" : "NNG", "position" : 97, "weight" : 0.9 },
		{"id" : 22, "lemma" : "하", "type" : "XSV", "position" : 103, "weight" : 0.0001 },
		{"id" : 23, "lemma" : "었", "type" : "EP", "position" : 103, "weight" : 0.9 },
		{"id" : 24, "lemma" : "다", "type" : "EF", "position" : 106, "weight" : 0.640954 },
		{"id" : 25, "lemma" : ".", "type" : "SF", "position" : 109, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "1944/SN+년/NNB", "target" : "1944년", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "이/MM", "target" : "이", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "사람/NNG+은/JX", "target" : "사람은", "word_id" : 2, "m_begin" : 3, "m_end" : 4},
		{"id" : 3, "result" : "조선건국동맹/NNG+을/JKO", "target" : "조선건국동맹을", "word_id" : 3, "m_begin" : 5, "m_end" : 8},
		{"id" : 4, "result" : "조직하/VV+어/EC", "target" : "조직해", "word_id" : 4, "m_begin" : 9, "m_end" : 11},
		{"id" : 5, "result" : "일본/NNG+의/JKG", "target" : "일본의", "word_id" : 5, "m_begin" : 12, "m_end" : 13},
		{"id" : 6, "result" : "패망/NNG+을/JKO", "target" : "패망을", "word_id" : 6, "m_begin" : 14, "m_end" : 15},
		{"id" : 7, "result" : "예견하/VV+고/EC", "target" : "예견하고", "word_id" : 7, "m_begin" : 16, "m_end" : 18},
		{"id" : 8, "result" : "해방/NNG+을/JKO", "target" : "해방을", "word_id" : 8, "m_begin" : 19, "m_end" : 20},
		{"id" : 9, "result" : "준비하/VV+었/EP+다/EF+./SF", "target" : "준비했다.", "word_id" : 9, "m_begin" : 21, "m_end" : 25}
	],
	"WSD" : [
		{"id" : 0, "text" : "1944", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 4, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 8, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "사람", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 12, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 18, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "조선", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 22, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "건국동맹", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 28, "begin" : 6, "end" : 7},
		{"id" : 7, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "조직하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 44, "begin" : 9, "end" : 10},
		{"id" : 9, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 50, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "일본", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 54, "begin" : 12, "end" : 12},
		{"id" : 11, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 60, "begin" : 13, "end" : 13},
		{"id" : 12, "text" : "패망", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 64, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 70, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : "예견하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 74, "begin" : 16, "end" : 17},
		{"id" : 15, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 83, "begin" : 18, "end" : 18},
		{"id" : 16, "text" : "해방", "type" : "NNG", "scode" : "05", "weight" : 1, "position" : 87, "begin" : 19, "end" : 19},
		{"id" : 17, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 93, "begin" : 20, "end" : 20},
		{"id" : 18, "text" : "준비하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 97, "begin" : 21, "end" : 22},
		{"id" : 19, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 103, "begin" : 23, "end" : 23},
		{"id" : 20, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 106, "begin" : 24, "end" : 24},
		{"id" : 21, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 109, "begin" : 25, "end" : 25}
	],
	"word" : [
		{"id" : 0, "text" : "1944년", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "이", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "사람은", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 3, "text" : "조선건국동맹을", "type" : "", "begin" : 5, "end" : 8},
		{"id" : 4, "text" : "조직해", "type" : "", "begin" : 9, "end" : 11},
		{"id" : 5, "text" : "일본의", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 6, "text" : "패망을", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 7, "text" : "예견하고", "type" : "", "begin" : 16, "end" : 18},
		{"id" : 8, "text" : "해방을", "type" : "", "begin" : 19, "end" : 20},
		{"id" : 9, "text" : "준비했다.", "type" : "", "begin" : 21, "end" : 25}
	],
	"NE" : [
		{"id" : 0, "text" : "1944년", "type" : "DT_YEAR", "begin" : 0, "end" : 1, "weight" : 0.467599, "common_noun" : 0},
		{"id" : 1, "text" : "조선건국동맹", "type" : "OGG_POLITICS", "begin" : 5, "end" : 7, "weight" : 0.377499, "common_noun" : 0},
		{"id" : 2, "text" : "일본", "type" : "LCP_COUNTRY", "begin" : 12, "end" : 12, "weight" : 0.54077, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "1944년", "head" : 4, "label" : "NP_AJT", "mod" : [], "weight" : 0.916665 },
		{"id" : 1, "text" : "이", "head" : 2, "label" : "DP", "mod" : [], "weight" : 0.916548 },
		{"id" : 2, "text" : "사람은", "head" : 4, "label" : "NP_SBJ", "mod" : [1], "weight" : 0.711492 },
		{"id" : 3, "text" : "조선건국동맹을", "head" : 4, "label" : "NP_OBJ", "mod" : [], "weight" : 0.787814 },
		{"id" : 4, "text" : "조직해", "head" : 7, "label" : "VP", "mod" : [0, 2, 3], "weight" : 0.856948 },
		{"id" : 5, "text" : "일본의", "head" : 6, "label" : "NP_MOD", "mod" : [], "weight" : 0.94938 },
		{"id" : 6, "text" : "패망을", "head" : 7, "label" : "NP_OBJ", "mod" : [5], "weight" : 0.869593 },
		{"id" : 7, "text" : "예견하고", "head" : 9, "label" : "VP", "mod" : [4, 6], "weight" : 0.676001 },
		{"id" : 8, "text" : "해방을", "head" : 9, "label" : "NP_OBJ", "mod" : [], "weight" : 0.523784 },
		{"id" : 9, "text" : "준비했다.", "head" : -1, "label" : "VP", "mod" : [7, 8], "weight" : 0.0672952 }
	],
	"SRL" : [
		{"verb" : "조직", "sense" : 1, "word_id" : 4, "weight" : 0.24523,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 0, "text" : "1944년", "weight" : 0.281203 },
				{"type" : "ARG0", "word_id" : 2, "text" : "사람은", "weight" : 0.181139 },
				{"type" : "ARG1", "word_id" : 3, "text" : "조선건국동맹을", "weight" : 0.273349 }
			] },
		{"verb" : "예견", "sense" : 1, "word_id" : 7, "weight" : 0.354491,
			"argument" : [
				{"type" : "ARG1", "word_id" : 6, "text" : "패망을", "weight" : 0.354491 }
			] },
		{"verb" : "준비", "sense" : 1, "word_id" : 9, "weight" : 0.199321,
			"argument" : [
				{"type" : "ARGM-MNR", "word_id" : 4, "text" : "조직해", "weight" : 0.155878 },
				{"type" : "ARG1", "word_id" : 8, "text" : "해방을", "weight" : 0.242765 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 7, "ant_sid" : 0, "ant_wid" : 2, "type" : "s", "istitle" : 0, "weight" : 0.773521 },
		{"id" : 1, "verb_wid" : 9, "ant_sid" : 0, "ant_wid" : 2, "type" : "s", "istitle" : 0, "weight" : 0.701908 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "또한 해방 이후 건국준비위원회를 조직했으며 김규식과 함께 좌우합작운동을 이끌었는데 이 사람은 누구일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "또한", "type" : "MAG", "position" : 110, "weight" : 0.0653106 },
		{"id" : 1, "lemma" : "해방", "type" : "NNG", "position" : 117, "weight" : 0.9 },
		{"id" : 2, "lemma" : "이후", "type" : "NNG", "position" : 124, "weight" : 0.184814 },
		{"id" : 3, "lemma" : "건국", "type" : "NNG", "position" : 131, "weight" : 0.184155 },
		{"id" : 4, "lemma" : "준비", "type" : "NNG", "position" : 137, "weight" : 0.9 },
		{"id" : 5, "lemma" : "위원", "type" : "NNG", "position" : 143, "weight" : 0.184216 },
		{"id" : 6, "lemma" : "회", "type" : "XSN", "position" : 149, "weight" : 0.010766 },
		{"id" : 7, "lemma" : "를", "type" : "JKO", "position" : 152, "weight" : 0.0870227 },
		{"id" : 8, "lemma" : "조직", "type" : "NNG", "position" : 156, "weight" : 0.101381 },
		{"id" : 9, "lemma" : "하", "type" : "XSV", "position" : 162, "weight" : 0.0001 },
		{"id" : 10, "lemma" : "었", "type" : "EP", "position" : 162, "weight" : 0.9 },
		{"id" : 11, "lemma" : "으며", "type" : "EC", "position" : 165, "weight" : 0.197482 },
		{"id" : 12, "lemma" : "김규식", "type" : "NNP", "position" : 172, "weight" : 0.7 },
		{"id" : 13, "lemma" : "과", "type" : "JKB", "position" : 181, "weight" : 0.0270179 },
		{"id" : 14, "lemma" : "함께", "type" : "MAG", "position" : 185, "weight" : 0.9 },
		{"id" : 15, "lemma" : "좌우", "type" : "NNG", "position" : 192, "weight" : 0.9 },
		{"id" : 16, "lemma" : "합작", "type" : "NNG", "position" : 198, "weight" : 0.9 },
		{"id" : 17, "lemma" : "운동", "type" : "NNG", "position" : 204, "weight" : 0.9 },
		{"id" : 18, "lemma" : "을", "type" : "JKO", "position" : 210, "weight" : 0.129611 },
		{"id" : 19, "lemma" : "이끌", "type" : "VV", "position" : 214, "weight" : 0.9 },
		{"id" : 20, "lemma" : "었", "type" : "EP", "position" : 220, "weight" : 0.9 },
		{"id" : 21, "lemma" : "는데", "type" : "EC", "position" : 223, "weight" : 0.188154 },
		{"id" : 22, "lemma" : "이", "type" : "MM", "position" : 230, "weight" : 0.000947656 },
		{"id" : 23, "lemma" : "사람", "type" : "NNG", "position" : 234, "weight" : 0.737436 },
		{"id" : 24, "lemma" : "은", "type" : "JX", "position" : 240, "weight" : 0.0449928 },
		{"id" : 25, "lemma" : "누구", "type" : "NP", "position" : 244, "weight" : 0.9 },
		{"id" : 26, "lemma" : "이", "type" : "VCP", "position" : 250, "weight" : 0.0175768 },
		{"id" : 27, "lemma" : "ㄹ까", "type" : "EF", "position" : 250, "weight" : 0.258243 },
		{"id" : 28, "lemma" : "?", "type" : "SF", "position" : 256, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "또한/MAG", "target" : "또한", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "해방/NNG", "target" : "해방", "word_id" : 1, "m_begin" : 1, "m_end" : 1},
		{"id" : 2, "result" : "이후/NNG", "target" : "이후", "word_id" : 2, "m_begin" : 2, "m_end" : 2},
		{"id" : 3, "result" : "건국준비위원회/NNG+를/JKO", "target" : "건국준비위원회를", "word_id" : 3, "m_begin" : 3, "m_end" : 7},
		{"id" : 4, "result" : "조직하/VV+었/EP+으며/EC", "target" : "조직했으며", "word_id" : 4, "m_begin" : 8, "m_end" : 11},
		{"id" : 5, "result" : "김규식/NNG+과/JKB", "target" : "김규식과", "word_id" : 5, "m_begin" : 12, "m_end" : 13},
		{"id" : 6, "result" : "함께/MAG", "target" : "함께", "word_id" : 6, "m_begin" : 14, "m_end" : 14},
		{"id" : 7, "result" : "좌우합작운동/NNG+을/JKO", "target" : "좌우합작운동을", "word_id" : 7, "m_begin" : 15, "m_end" : 18},
		{"id" : 8, "result" : "이끌/VV+었/EP+는데/EC", "target" : "이끌었는데", "word_id" : 8, "m_begin" : 19, "m_end" : 21},
		{"id" : 9, "result" : "이/MM", "target" : "이", "word_id" : 9, "m_begin" : 22, "m_end" : 22},
		{"id" : 10, "result" : "사람/NNG+은/JX", "target" : "사람은", "word_id" : 10, "m_begin" : 23, "m_end" : 24},
		{"id" : 11, "result" : "누구/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "누구일까?", "word_id" : 11, "m_begin" : 25, "m_end" : 28}
	],
	"WSD" : [
		{"id" : 0, "text" : "또한", "type" : "MAG", "scode" : "00", "weight" : 1, "position" : 110, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "해방", "type" : "NNG", "scode" : "05", "weight" : 1, "position" : 117, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "이후", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 124, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "건국준비위원회", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 131, "begin" : 3, "end" : 6},
		{"id" : 4, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 152, "begin" : 7, "end" : 7},
		{"id" : 5, "text" : "조직하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 156, "begin" : 8, "end" : 9},
		{"id" : 6, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 162, "begin" : 10, "end" : 10},
		{"id" : 7, "text" : "으며", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 165, "begin" : 11, "end" : 11},
		{"id" : 8, "text" : "김규식", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 172, "begin" : 12, "end" : 12},
		{"id" : 9, "text" : "과", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 181, "begin" : 13, "end" : 13},
		{"id" : 10, "text" : "함께", "type" : "MAG", "scode" : "00", "weight" : 1, "position" : 185, "begin" : 14, "end" : 14},
		{"id" : 11, "text" : "좌우합작", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 192, "begin" : 15, "end" : 16},
		{"id" : 12, "text" : "운동", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 204, "begin" : 17, "end" : 17},
		{"id" : 13, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 210, "begin" : 18, "end" : 18},
		{"id" : 14, "text" : "이끌", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 214, "begin" : 19, "end" : 19},
		{"id" : 15, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 220, "begin" : 20, "end" : 20},
		{"id" : 16, "text" : "는데", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 223, "begin" : 21, "end" : 21},
		{"id" : 17, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 230, "begin" : 22, "end" : 22},
		{"id" : 18, "text" : "사람", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 234, "begin" : 23, "end" : 23},
		{"id" : 19, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 240, "begin" : 24, "end" : 24},
		{"id" : 20, "text" : "누구", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 244, "begin" : 25, "end" : 25},
		{"id" : 21, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 250, "begin" : 26, "end" : 26},
		{"id" : 22, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 250, "begin" : 27, "end" : 27},
		{"id" : 23, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 256, "begin" : 28, "end" : 28}
	],
	"word" : [
		{"id" : 0, "text" : "또한", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "해방", "type" : "", "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "이후", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "건국준비위원회를", "type" : "", "begin" : 3, "end" : 7},
		{"id" : 4, "text" : "조직했으며", "type" : "", "begin" : 8, "end" : 11},
		{"id" : 5, "text" : "김규식과", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 6, "text" : "함께", "type" : "", "begin" : 14, "end" : 14},
		{"id" : 7, "text" : "좌우합작운동을", "type" : "", "begin" : 15, "end" : 18},
		{"id" : 8, "text" : "이끌었는데", "type" : "", "begin" : 19, "end" : 21},
		{"id" : 9, "text" : "이", "type" : "", "begin" : 22, "end" : 22},
		{"id" : 10, "text" : "사람은", "type" : "", "begin" : 23, "end" : 24},
		{"id" : 11, "text" : "누구일까?", "type" : "", "begin" : 25, "end" : 28}
	],
	"NE" : [
		{"id" : 0, "text" : "건국준비위원회", "type" : "OGG_POLITICS", "begin" : 3, "end" : 6, "weight" : 0.934211, "common_noun" : 0},
		{"id" : 1, "text" : "김규식", "type" : "PS_NAME", "begin" : 12, "end" : 12, "weight" : 0.68434, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "또한", "head" : 4, "label" : "AP", "mod" : [], "weight" : 0.827666 },
		{"id" : 1, "text" : "해방", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.893598 },
		{"id" : 2, "text" : "이후", "head" : 4, "label" : "NP_AJT", "mod" : [1], "weight" : 0.803894 },
		{"id" : 3, "text" : "건국준비위원회를", "head" : 4, "label" : "NP_OBJ", "mod" : [], "weight" : 0.614883 },
		{"id" : 4, "text" : "조직했으며", "head" : 8, "label" : "VP", "mod" : [0, 2, 3], "weight" : 0.941106 },
		{"id" : 5, "text" : "김규식과", "head" : 6, "label" : "NP_AJT", "mod" : [], "weight" : 0.8485 },
		{"id" : 6, "text" : "함께", "head" : 8, "label" : "AP", "mod" : [5], "weight" : 0.692313 },
		{"id" : 7, "text" : "좌우합작운동을", "head" : 8, "label" : "NP_OBJ", "mod" : [], "weight" : 0.667389 },
		{"id" : 8, "text" : "이끌었는데", "head" : 11, "label" : "VP", "mod" : [4, 6, 7], "weight" : 0.885091 },
		{"id" : 9, "text" : "이", "head" : 10, "label" : "DP", "mod" : [], "weight" : 0.860789 },
		{"id" : 10, "text" : "사람은", "head" : 11, "label" : "NP_SBJ", "mod" : [9], "weight" : 0.729832 },
		{"id" : 11, "text" : "누구일까?", "head" : -1, "label" : "VNP", "mod" : [8, 10], "weight" : 0.0503154 }
	],
	"SRL" : [
		{"verb" : "조직", "sense" : 1, "word_id" : 4, "weight" : 0.290023,
			"argument" : [
				{"type" : "ARGM-DIS", "word_id" : 0, "text" : "또한", "weight" : 0.204396 },
				{"type" : "ARGM-TMP", "word_id" : 2, "text" : "이후", "weight" : 0.238435 },
				{"type" : "ARG1", "word_id" : 3, "text" : "건국준비위원회를", "weight" : 0.427237 }
			] },
		{"verb" : "이끌", "sense" : 1, "word_id" : 8, "weight" : 0.219751,
			"argument" : [
				{"type" : "ARGM-MNR", "word_id" : 6, "text" : "함께", "weight" : 0.186325 },
				{"type" : "ARG1", "word_id" : 7, "text" : "좌우합작운동을", "weight" : 0.253176 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 4, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.378677 },
		{"id" : 1, "verb_wid" : 8, "ant_sid" : 0, "ant_wid" : 2, "type" : "s", "istitle" : 0, "weight" : 0.136566 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 1, "sent_id" : 0, "start_eid" : 1, "end_eid" : 2, "ne_id" : -1, "text" : "이 사람", "start_eid_short" : 1, "end_eid_short" : 2, "text_short" : "이 사람", "weight" : 0 },
		{"id" : 11, "sent_id" : 1, "start_eid" : 9, "end_eid" : 10, "ne_id" : -1, "text" : "이 사람", "start_eid_short" : 9, "end_eid_short" : 10, "text_short" : "이 사람", "weight" : 0.006 },
		{"id" : 12, "sent_id" : 1, "start_eid" : 11, "end_eid" : 11, "ne_id" : -1, "text" : "누구", "start_eid_short" : 11, "end_eid_short" : 11, "text_short" : "누구이ㄹ까?", "weight" : 0.006 }
	] },
	{"id" : 1, "type" : "OGG_POLITICS", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 2, "sent_id" : 0, "start_eid" : 3, "end_eid" : 3, "ne_id" : 1, "text" : "조선건국동맹", "start_eid_short" : 3, "end_eid_short" : 3, "text_short" : "조선건국동맹", "weight" : 0.002 },
		{"id" : 8, "sent_id" : 1, "start_eid" : 3, "end_eid" : 3, "ne_id" : 0, "text" : "건국준비위원회", "start_eid_short" : 3, "end_eid_short" : 3, "text_short" : "건국준비위원회", "weight" : 0.003 }
	] },
	{"id" : 2, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 7, "sent_id" : 1, "start_eid" : 1, "end_eid" : 1, "ne_id" : -1, "text" : "해방", "start_eid_short" : 1, "end_eid_short" : 1, "text_short" : "해방", "weight" : 0.016 },
		{"id" : 5, "sent_id" : 0, "start_eid" : 8, "end_eid" : 8, "ne_id" : -1, "text" : "해방", "start_eid_short" : 8, "end_eid_short" : 8, "text_short" : "해방", "weight" : 0.01 }
	] }
 ]
}

