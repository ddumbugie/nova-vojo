{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "12세기 여진족의 한 부족인 완옌부는 세력을 확장하며 고려와 자주 충돌했다.  ",
	"morp" : [
		{"id" : 0, "lemma" : "12", "type" : "SN", "position" : 0, "weight" : 1 },
		{"id" : 1, "lemma" : "세기", "type" : "NNG", "position" : 2, "weight" : 0.100259 },
		{"id" : 2, "lemma" : "여진", "type" : "NNG", "position" : 9, "weight" : 0.169251 },
		{"id" : 3, "lemma" : "족", "type" : "XSN", "position" : 15, "weight" : 0.0238227 },
		{"id" : 4, "lemma" : "의", "type" : "JKG", "position" : 18, "weight" : 0.120732 },
		{"id" : 5, "lemma" : "한", "type" : "MM", "position" : 22, "weight" : 0.0231003 },
		{"id" : 6, "lemma" : "부족", "type" : "NNG", "position" : 26, "weight" : 0.9 },
		{"id" : 7, "lemma" : "이", "type" : "VCP", "position" : 32, "weight" : 0.0177525 },
		{"id" : 8, "lemma" : "ㄴ", "type" : "ETM", "position" : 32, "weight" : 0.220712 },
		{"id" : 9, "lemma" : "완옌부", "type" : "NNP", "position" : 36, "weight" : 0.6 },
		{"id" : 10, "lemma" : "는", "type" : "JX", "position" : 45, "weight" : 0.0332677 },
		{"id" : 11, "lemma" : "세력", "type" : "NNG", "position" : 49, "weight" : 0.9 },
		{"id" : 12, "lemma" : "을", "type" : "JKO", "position" : 55, "weight" : 0.129611 },
		{"id" : 13, "lemma" : "확장", "type" : "NNG", "position" : 59, "weight" : 0.9 },
		{"id" : 14, "lemma" : "하", "type" : "XSV", "position" : 65, "weight" : 0.0001 },
		{"id" : 15, "lemma" : "며", "type" : "EC", "position" : 68, "weight" : 0.371165 },
		{"id" : 16, "lemma" : "고려", "type" : "NNP", "position" : 72, "weight" : 0.0209345 },
		{"id" : 17, "lemma" : "와", "type" : "JC", "position" : 78, "weight" : 0.0104534 },
		{"id" : 18, "lemma" : "자주", "type" : "MAG", "position" : 82, "weight" : 0.0070007 },
		{"id" : 19, "lemma" : "충돌", "type" : "NNG", "position" : 89, "weight" : 0.9 },
		{"id" : 20, "lemma" : "하", "type" : "XSV", "position" : 95, "weight" : 0.0001 },
		{"id" : 21, "lemma" : "었", "type" : "EP", "position" : 95, "weight" : 0.9 },
		{"id" : 22, "lemma" : "다", "type" : "EF", "position" : 98, "weight" : 0.640954 },
		{"id" : 23, "lemma" : ".", "type" : "SF", "position" : 101, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "12/SN+세기/NNG", "target" : "12세기", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "여진족/NNG+의/JKG", "target" : "여진족의", "word_id" : 1, "m_begin" : 2, "m_end" : 4},
		{"id" : 2, "result" : "한/MM", "target" : "한", "word_id" : 2, "m_begin" : 5, "m_end" : 5},
		{"id" : 3, "result" : "부족/NNG+이/VCP+ㄴ/ETM", "target" : "부족인", "word_id" : 3, "m_begin" : 6, "m_end" : 8},
		{"id" : 4, "result" : "완옌부/NNG+는/JX", "target" : "완옌부는", "word_id" : 4, "m_begin" : 9, "m_end" : 10},
		{"id" : 5, "result" : "세력/NNG+을/JKO", "target" : "세력을", "word_id" : 5, "m_begin" : 11, "m_end" : 12},
		{"id" : 6, "result" : "확장하/VV+며/EC", "target" : "확장하며", "word_id" : 6, "m_begin" : 13, "m_end" : 15},
		{"id" : 7, "result" : "고려/NNG+와/JC", "target" : "고려와", "word_id" : 7, "m_begin" : 16, "m_end" : 17},
		{"id" : 8, "result" : "자주/MAG", "target" : "자주", "word_id" : 8, "m_begin" : 18, "m_end" : 18},
		{"id" : 9, "result" : "충돌하/VV+었/EP+다/EF+./SF", "target" : "충돌했다.", "word_id" : 9, "m_begin" : 19, "m_end" : 23}
	],
	"WSD" : [
		{"id" : 0, "text" : "12", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "세기", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 2, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "여진족", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 2, "end" : 3},
		{"id" : 3, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 18, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "한", "type" : "MM", "scode" : "01", "weight" : 1, "position" : 22, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "부족", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 26, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 32, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 32, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "완옌부", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 36, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 45, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "세력", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 49, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 55, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "확장하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 59, "begin" : 13, "end" : 14},
		{"id" : 13, "text" : "며", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 68, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : "고려", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 72, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : "와", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 78, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : "자주", "type" : "MAG", "scode" : "01", "weight" : 1, "position" : 82, "begin" : 18, "end" : 18},
		{"id" : 17, "text" : "충돌하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 89, "begin" : 19, "end" : 20},
		{"id" : 18, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 95, "begin" : 21, "end" : 21},
		{"id" : 19, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 98, "begin" : 22, "end" : 22},
		{"id" : 20, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 101, "begin" : 23, "end" : 23}
	],
	"word" : [
		{"id" : 0, "text" : "12세기", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "여진족의", "type" : "", "begin" : 2, "end" : 4},
		{"id" : 2, "text" : "한", "type" : "", "begin" : 5, "end" : 5},
		{"id" : 3, "text" : "부족인", "type" : "", "begin" : 6, "end" : 8},
		{"id" : 4, "text" : "완옌부는", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 5, "text" : "세력을", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 6, "text" : "확장하며", "type" : "", "begin" : 13, "end" : 15},
		{"id" : 7, "text" : "고려와", "type" : "", "begin" : 16, "end" : 17},
		{"id" : 8, "text" : "자주", "type" : "", "begin" : 18, "end" : 18},
		{"id" : 9, "text" : "충돌했다.", "type" : "", "begin" : 19, "end" : 23}
	],
	"NE" : [
		{"id" : 0, "text" : "12세기", "type" : "DT_OTHERS", "begin" : 0, "end" : 1, "weight" : 0.360721, "common_noun" : 0},
		{"id" : 1, "text" : "여진족", "type" : "CV_TRIBE", "begin" : 2, "end" : 3, "weight" : 0.499713, "common_noun" : 0},
		{"id" : 2, "text" : "한 부족", "type" : "QT_COUNT", "begin" : 5, "end" : 6, "weight" : 0.219113, "common_noun" : 0},
		{"id" : 3, "text" : "고려", "type" : "LCP_COUNTRY", "begin" : 16, "end" : 16, "weight" : 0.215292, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "12세기", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.781702 },
		{"id" : 1, "text" : "여진족의", "head" : 3, "label" : "NP_MOD", "mod" : [0], "weight" : 0.752813 },
		{"id" : 2, "text" : "한", "head" : 3, "label" : "DP", "mod" : [], "weight" : 0.723585 },
		{"id" : 3, "text" : "부족인", "head" : 4, "label" : "VNP_MOD", "mod" : [1, 2], "weight" : 0.771467 },
		{"id" : 4, "text" : "완옌부는", "head" : 6, "label" : "NP_SBJ", "mod" : [3], "weight" : 0.769605 },
		{"id" : 5, "text" : "세력을", "head" : 6, "label" : "NP_OBJ", "mod" : [], "weight" : 0.795402 },
		{"id" : 6, "text" : "확장하며", "head" : 9, "label" : "VP", "mod" : [4, 5], "weight" : 0.756015 },
		{"id" : 7, "text" : "고려와", "head" : 9, "label" : "NP_AJT", "mod" : [], "weight" : 0.266143 },
		{"id" : 8, "text" : "자주", "head" : 9, "label" : "AP", "mod" : [], "weight" : 0.452855 },
		{"id" : 9, "text" : "충돌했다.", "head" : -1, "label" : "VP", "mod" : [6, 7, 8], "weight" : 0.0101256 }
	],
	"SRL" : [
		{"verb" : "확장", "sense" : 1, "word_id" : 6, "weight" : 0.267982,
			"argument" : [
				{"type" : "ARG0", "word_id" : 4, "text" : "완옌부는", "weight" : 0.258491 },
				{"type" : "ARG1", "word_id" : 5, "text" : "세력을", "weight" : 0.277472 }
			] },
		{"verb" : "충돌", "sense" : 1, "word_id" : 9, "weight" : 0.139245,
			"argument" : [
				{"type" : "ARG1", "word_id" : 7, "text" : "고려와", "weight" : 0.135346 },
				{"type" : "ARGM-ADV", "word_id" : 8, "text" : "자주", "weight" : 0.143145 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 9, "ant_sid" : 0, "ant_wid" : 4, "type" : "s", "istitle" : 0, "weight" : 0.845541 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "보다 효과적으로 여진족과 전투를 벌이기 위해 숙종은 윤관의 건의를 받아들여 새로운 군사조직을 편성했는데 이것은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "보다", "type" : "MAG", "position" : 104, "weight" : 0.00418937 },
		{"id" : 1, "lemma" : "효과", "type" : "NNG", "position" : 111, "weight" : 0.9 },
		{"id" : 2, "lemma" : "적", "type" : "XSN", "position" : 117, "weight" : 0.0168756 },
		{"id" : 3, "lemma" : "으로", "type" : "JKB", "position" : 120, "weight" : 0.121717 },
		{"id" : 4, "lemma" : "여진", "type" : "NNG", "position" : 127, "weight" : 0.1905 },
		{"id" : 5, "lemma" : "족", "type" : "XSN", "position" : 133, "weight" : 0.0238227 },
		{"id" : 6, "lemma" : "과", "type" : "JKB", "position" : 136, "weight" : 0.0399165 },
		{"id" : 7, "lemma" : "전투", "type" : "NNG", "position" : 140, "weight" : 0.9 },
		{"id" : 8, "lemma" : "를", "type" : "JKO", "position" : 146, "weight" : 0.137686 },
		{"id" : 9, "lemma" : "벌이", "type" : "VV", "position" : 150, "weight" : 0.761655 },
		{"id" : 10, "lemma" : "기", "type" : "ETN", "position" : 156, "weight" : 0.0380514 },
		{"id" : 11, "lemma" : "위하", "type" : "VV", "position" : 160, "weight" : 0.176484 },
		{"id" : 12, "lemma" : "어", "type" : "EC", "position" : 163, "weight" : 0.41831 },
		{"id" : 13, "lemma" : "숙종", "type" : "NNP", "position" : 167, "weight" : 0.9 },
		{"id" : 14, "lemma" : "은", "type" : "JX", "position" : 173, "weight" : 0.0520511 },
		{"id" : 15, "lemma" : "윤관", "type" : "NNP", "position" : 177, "weight" : 0.0437034 },
		{"id" : 16, "lemma" : "의", "type" : "JKG", "position" : 183, "weight" : 0.0987295 },
		{"id" : 17, "lemma" : "건의", "type" : "NNG", "position" : 187, "weight" : 0.9 },
		{"id" : 18, "lemma" : "를", "type" : "JKO", "position" : 193, "weight" : 0.137686 },
		{"id" : 19, "lemma" : "받아들이", "type" : "VV", "position" : 197, "weight" : 0.9 },
		{"id" : 20, "lemma" : "어", "type" : "EC", "position" : 206, "weight" : 0.41831 },
		{"id" : 21, "lemma" : "새롭", "type" : "VA", "position" : 210, "weight" : 0.9 },
		{"id" : 22, "lemma" : "ㄴ", "type" : "ETM", "position" : 213, "weight" : 0.430446 },
		{"id" : 23, "lemma" : "군사", "type" : "NNG", "position" : 220, "weight" : 0.9 },
		{"id" : 24, "lemma" : "조직", "type" : "NNG", "position" : 226, "weight" : 0.184801 },
		{"id" : 25, "lemma" : "을", "type" : "JKO", "position" : 232, "weight" : 0.129611 },
		{"id" : 26, "lemma" : "편성", "type" : "NNG", "position" : 236, "weight" : 0.9 },
		{"id" : 27, "lemma" : "하", "type" : "XSV", "position" : 242, "weight" : 0.0001 },
		{"id" : 28, "lemma" : "었", "type" : "EP", "position" : 242, "weight" : 0.9 },
		{"id" : 29, "lemma" : "는데", "type" : "EC", "position" : 245, "weight" : 0.188154 },
		{"id" : 30, "lemma" : "이것", "type" : "NP", "position" : 252, "weight" : 0.0259829 },
		{"id" : 31, "lemma" : "은", "type" : "JX", "position" : 258, "weight" : 0.191811 },
		{"id" : 32, "lemma" : "무엇", "type" : "NP", "position" : 262, "weight" : 0.9 },
		{"id" : 33, "lemma" : "이", "type" : "VCP", "position" : 268, "weight" : 0.0175768 },
		{"id" : 34, "lemma" : "ㄹ까", "type" : "EF", "position" : 268, "weight" : 0.258243 },
		{"id" : 35, "lemma" : "?", "type" : "SF", "position" : 274, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "보다/MAG", "target" : "보다", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "효과적/NNG+으로/JKB", "target" : "효과적으로", "word_id" : 1, "m_begin" : 1, "m_end" : 3},
		{"id" : 2, "result" : "여진족/NNG+과/JKB", "target" : "여진족과", "word_id" : 2, "m_begin" : 4, "m_end" : 6},
		{"id" : 3, "result" : "전투/NNG+를/JKO", "target" : "전투를", "word_id" : 3, "m_begin" : 7, "m_end" : 8},
		{"id" : 4, "result" : "벌이/VV+기/ETN", "target" : "벌이기", "word_id" : 4, "m_begin" : 9, "m_end" : 10},
		{"id" : 5, "result" : "위하/VV+어/EC", "target" : "위해", "word_id" : 5, "m_begin" : 11, "m_end" : 12},
		{"id" : 6, "result" : "숙종/NNG+은/JX", "target" : "숙종은", "word_id" : 6, "m_begin" : 13, "m_end" : 14},
		{"id" : 7, "result" : "윤관/NNG+의/JKG", "target" : "윤관의", "word_id" : 7, "m_begin" : 15, "m_end" : 16},
		{"id" : 8, "result" : "건의/NNG+를/JKO", "target" : "건의를", "word_id" : 8, "m_begin" : 17, "m_end" : 18},
		{"id" : 9, "result" : "받아들이/VV+어/EC", "target" : "받아들여", "word_id" : 9, "m_begin" : 19, "m_end" : 20},
		{"id" : 10, "result" : "새롭/VA+ㄴ/ETM", "target" : "새로운", "word_id" : 10, "m_begin" : 21, "m_end" : 22},
		{"id" : 11, "result" : "군사조직/NNG+을/JKO", "target" : "군사조직을", "word_id" : 11, "m_begin" : 23, "m_end" : 25},
		{"id" : 12, "result" : "편성하/VV+었/EP+는데/EC", "target" : "편성했는데", "word_id" : 12, "m_begin" : 26, "m_end" : 29},
		{"id" : 13, "result" : "이것/NP+은/JX", "target" : "이것은", "word_id" : 13, "m_begin" : 30, "m_end" : 31},
		{"id" : 14, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 14, "m_begin" : 32, "m_end" : 35}
	],
	"WSD" : [
		{"id" : 0, "text" : "보다", "type" : "MAG", "scode" : "02", "weight" : 1, "position" : 104, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "효과적", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 111, "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 120, "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "여진족", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 127, "begin" : 4, "end" : 5},
		{"id" : 4, "text" : "과", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 136, "begin" : 6, "end" : 6},
		{"id" : 5, "text" : "전투", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 140, "begin" : 7, "end" : 7},
		{"id" : 6, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 146, "begin" : 8, "end" : 8},
		{"id" : 7, "text" : "벌이", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 150, "begin" : 9, "end" : 9},
		{"id" : 8, "text" : "기", "type" : "ETN", "scode" : "00", "weight" : 1, "position" : 156, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "위하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 160, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 163, "begin" : 12, "end" : 12},
		{"id" : 11, "text" : "숙종", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 167, "begin" : 13, "end" : 13},
		{"id" : 12, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 173, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "윤관", "type" : "NNP", "scode" : "99", "weight" : 1, "position" : 177, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 183, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : "건의", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 187, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 193, "begin" : 18, "end" : 18},
		{"id" : 17, "text" : "받아들이", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 197, "begin" : 19, "end" : 19},
		{"id" : 18, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 206, "begin" : 20, "end" : 20},
		{"id" : 19, "text" : "새롭", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 210, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 213, "begin" : 22, "end" : 22},
		{"id" : 21, "text" : "군사", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 220, "begin" : 23, "end" : 23},
		{"id" : 22, "text" : "조직", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 226, "begin" : 24, "end" : 24},
		{"id" : 23, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 232, "begin" : 25, "end" : 25},
		{"id" : 24, "text" : "편성하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 236, "begin" : 26, "end" : 27},
		{"id" : 25, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 242, "begin" : 28, "end" : 28},
		{"id" : 26, "text" : "는데", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 245, "begin" : 29, "end" : 29},
		{"id" : 27, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 252, "begin" : 30, "end" : 30},
		{"id" : 28, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 258, "begin" : 31, "end" : 31},
		{"id" : 29, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 262, "begin" : 32, "end" : 32},
		{"id" : 30, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 268, "begin" : 33, "end" : 33},
		{"id" : 31, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 268, "begin" : 34, "end" : 34},
		{"id" : 32, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 274, "begin" : 35, "end" : 35}
	],
	"word" : [
		{"id" : 0, "text" : "보다", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "효과적으로", "type" : "", "begin" : 1, "end" : 3},
		{"id" : 2, "text" : "여진족과", "type" : "", "begin" : 4, "end" : 6},
		{"id" : 3, "text" : "전투를", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 4, "text" : "벌이기", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 5, "text" : "위해", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 6, "text" : "숙종은", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 7, "text" : "윤관의", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 8, "text" : "건의를", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 9, "text" : "받아들여", "type" : "", "begin" : 19, "end" : 20},
		{"id" : 10, "text" : "새로운", "type" : "", "begin" : 21, "end" : 22},
		{"id" : 11, "text" : "군사조직을", "type" : "", "begin" : 23, "end" : 25},
		{"id" : 12, "text" : "편성했는데", "type" : "", "begin" : 26, "end" : 29},
		{"id" : 13, "text" : "이것은", "type" : "", "begin" : 30, "end" : 31},
		{"id" : 14, "text" : "무엇일까?", "type" : "", "begin" : 32, "end" : 35}
	],
	"NE" : [
		{"id" : 0, "text" : "여진족", "type" : "CV_TRIBE", "begin" : 4, "end" : 5, "weight" : 0.510514, "common_noun" : 0},
		{"id" : 1, "text" : "숙종", "type" : "PS_NAME", "begin" : 13, "end" : 13, "weight" : 0.318626, "common_noun" : 0},
		{"id" : 2, "text" : "윤관", "type" : "PS_NAME", "begin" : 15, "end" : 15, "weight" : 0.464365, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "보다", "head" : 1, "label" : "AP", "mod" : [], "weight" : 0.588068 },
		{"id" : 1, "text" : "효과적으로", "head" : 4, "label" : "NP_AJT", "mod" : [0], "weight" : 0.751709 },
		{"id" : 2, "text" : "여진족과", "head" : 4, "label" : "NP_AJT", "mod" : [], "weight" : 0.669873 },
		{"id" : 3, "text" : "전투를", "head" : 4, "label" : "NP_OBJ", "mod" : [], "weight" : 0.911465 },
		{"id" : 4, "text" : "벌이기", "head" : 5, "label" : "VP_OBJ", "mod" : [1, 2, 3], "weight" : 0.685532 },
		{"id" : 5, "text" : "위해", "head" : 9, "label" : "VP", "mod" : [4], "weight" : 0.818732 },
		{"id" : 6, "text" : "숙종은", "head" : 9, "label" : "NP_SBJ", "mod" : [], "weight" : 0.924904 },
		{"id" : 7, "text" : "윤관의", "head" : 8, "label" : "NP_MOD", "mod" : [], "weight" : 0.86502 },
		{"id" : 8, "text" : "건의를", "head" : 9, "label" : "NP_OBJ", "mod" : [7], "weight" : 0.673833 },
		{"id" : 9, "text" : "받아들여", "head" : 12, "label" : "VP", "mod" : [5, 6, 8], "weight" : 0.782543 },
		{"id" : 10, "text" : "새로운", "head" : 11, "label" : "VP_MOD", "mod" : [], "weight" : 0.774588 },
		{"id" : 11, "text" : "군사조직을", "head" : 12, "label" : "NP_OBJ", "mod" : [10], "weight" : 0.576886 },
		{"id" : 12, "text" : "편성했는데", "head" : 14, "label" : "VP", "mod" : [9, 11], "weight" : 0.666802 },
		{"id" : 13, "text" : "이것은", "head" : 14, "label" : "NP_SBJ", "mod" : [], "weight" : 0.619538 },
		{"id" : 14, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [12, 13], "weight" : 0.00811339 }
	],
	"SRL" : [
		{"verb" : "벌이", "sense" : 1, "word_id" : 4, "weight" : 0.227345,
			"argument" : [
				{"type" : "ARG0", "word_id" : 2, "text" : "여진족과", "weight" : 0.110127 },
				{"type" : "ARG1", "word_id" : 3, "text" : "전투를", "weight" : 0.344564 }
			] },
		{"verb" : "위하", "sense" : 1, "word_id" : 5, "weight" : 0.312376,
			"argument" : [
				{"type" : "ARG1", "word_id" : 4, "text" : "벌이기", "weight" : 0.312376 }
			] },
		{"verb" : "받아들이", "sense" : 1, "word_id" : 9, "weight" : 0.302301,
			"argument" : [
				{"type" : "ARGM-PRP", "word_id" : 5, "text" : "위해", "weight" : 0.268904 },
				{"type" : "ARG0", "word_id" : 6, "text" : "숙종은", "weight" : 0.305011 },
				{"type" : "ARG1", "word_id" : 8, "text" : "건의를", "weight" : 0.332987 }
			] },
		{"verb" : "새롭", "sense" : 1, "word_id" : 10, "weight" : 0.443679,
			"argument" : [
				{"type" : "ARG1", "word_id" : 11, "text" : "군사조직을", "weight" : 0.443679 }
			] },
		{"verb" : "편성", "sense" : 1, "word_id" : 12, "weight" : 0.23045,
			"argument" : [
				{"type" : "ARG0", "word_id" : 6, "text" : "숙종은", "weight" : 0.211514 },
				{"type" : "ARGM-CAU", "word_id" : 9, "text" : "받아들여", "weight" : 0.142571 },
				{"type" : "ARG1", "word_id" : 11, "text" : "군사조직을", "weight" : 0.337265 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 4, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.498555 },
		{"id" : 1, "verb_wid" : 12, "ant_sid" : 1, "ant_wid" : 6, "type" : "s", "istitle" : 0, "weight" : 0.154072 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "CV_TRIBE", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 1, "sent_id" : 0, "start_eid" : 0, "end_eid" : 1, "ne_id" : 1, "text" : "12세기 여진족", "start_eid_short" : 0, "end_eid_short" : 1, "text_short" : "12세기 여진족", "weight" : 0.006 },
		{"id" : 7, "sent_id" : 1, "start_eid" : 2, "end_eid" : 2, "ne_id" : 0, "text" : "여진족", "start_eid_short" : 2, "end_eid_short" : 2, "text_short" : "여진족", "weight" : 0.01 }
	] },
	{"id" : 1, "type" : "PS_NAME", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 9, "sent_id" : 1, "start_eid" : 6, "end_eid" : 6, "ne_id" : 1, "text" : "숙종", "start_eid_short" : 6, "end_eid_short" : 6, "text_short" : "숙종", "weight" : 0.002 },
		{"id" : 13, "sent_id" : 1, "start_eid" : 13, "end_eid" : 13, "ne_id" : -1, "text" : "이것", "start_eid_short" : 13, "end_eid_short" : 13, "text_short" : "이것", "weight" : 0.012 },
		{"id" : 14, "sent_id" : 1, "start_eid" : 14, "end_eid" : 14, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 14, "end_eid_short" : 14, "text_short" : "무엇이ㄹ까?", "weight" : 0.012 }
	] }
 ]
}

