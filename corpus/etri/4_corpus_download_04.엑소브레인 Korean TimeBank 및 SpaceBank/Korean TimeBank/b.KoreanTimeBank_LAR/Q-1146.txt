{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "1990년 로마 월드컵 공연을 비롯해 2005년까지 왕성한 활동을 했던 세계 3대 테너가 아닌 사람은 누구일까?",
	"morp" : [
		{"id" : 0, "lemma" : "1990", "type" : "SN", "position" : 0, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 4, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "로마", "type" : "NNG", "position" : 8, "weight" : 0.0615543 },
		{"id" : 3, "lemma" : "월드컵", "type" : "NNG", "position" : 15, "weight" : 0.9 },
		{"id" : 4, "lemma" : "공연", "type" : "NNG", "position" : 25, "weight" : 0.9 },
		{"id" : 5, "lemma" : "을", "type" : "JKO", "position" : 31, "weight" : 0.129611 },
		{"id" : 6, "lemma" : "비롯하", "type" : "VV", "position" : 35, "weight" : 0.9 },
		{"id" : 7, "lemma" : "어", "type" : "EC", "position" : 41, "weight" : 0.41831 },
		{"id" : 8, "lemma" : "2005", "type" : "SN", "position" : 45, "weight" : 1 },
		{"id" : 9, "lemma" : "년", "type" : "NNB", "position" : 49, "weight" : 0.414343 },
		{"id" : 10, "lemma" : "까지", "type" : "JX", "position" : 52, "weight" : 0.105153 },
		{"id" : 11, "lemma" : "왕성", "type" : "NNG", "position" : 59, "weight" : 0.299928 },
		{"id" : 12, "lemma" : "하", "type" : "XSA", "position" : 65, "weight" : 0.0001 },
		{"id" : 13, "lemma" : "ㄴ", "type" : "ETM", "position" : 65, "weight" : 0.488779 },
		{"id" : 14, "lemma" : "활동", "type" : "NNG", "position" : 69, "weight" : 0.9 },
		{"id" : 15, "lemma" : "을", "type" : "JKO", "position" : 75, "weight" : 0.129611 },
		{"id" : 16, "lemma" : "하", "type" : "VV", "position" : 79, "weight" : 0.48169 },
		{"id" : 17, "lemma" : "었", "type" : "EP", "position" : 79, "weight" : 0.9 },
		{"id" : 18, "lemma" : "던", "type" : "ETM", "position" : 82, "weight" : 0.107547 },
		{"id" : 19, "lemma" : "세계", "type" : "NNG", "position" : 86, "weight" : 0.9 },
		{"id" : 20, "lemma" : "3", "type" : "SN", "position" : 93, "weight" : 1 },
		{"id" : 21, "lemma" : "대", "type" : "NNG", "position" : 94, "weight" : 0.0150854 },
		{"id" : 22, "lemma" : "테너", "type" : "NNG", "position" : 98, "weight" : 0.9 },
		{"id" : 23, "lemma" : "가", "type" : "JKC", "position" : 104, "weight" : 0.000442184 },
		{"id" : 24, "lemma" : "아니", "type" : "VCN", "position" : 108, "weight" : 0.354175 },
		{"id" : 25, "lemma" : "ㄴ", "type" : "ETM", "position" : 111, "weight" : 0.144018 },
		{"id" : 26, "lemma" : "사람", "type" : "NNG", "position" : 115, "weight" : 0.658826 },
		{"id" : 27, "lemma" : "은", "type" : "JX", "position" : 121, "weight" : 0.0449928 },
		{"id" : 28, "lemma" : "누구", "type" : "NP", "position" : 125, "weight" : 0.9 },
		{"id" : 29, "lemma" : "이", "type" : "VCP", "position" : 131, "weight" : 0.0175768 },
		{"id" : 30, "lemma" : "ㄹ까", "type" : "EF", "position" : 131, "weight" : 0.258243 },
		{"id" : 31, "lemma" : "?", "type" : "SF", "position" : 137, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "1990/SN+년/NNB", "target" : "1990년", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "로마/NNG", "target" : "로마", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "월드컵/NNG", "target" : "월드컵", "word_id" : 2, "m_begin" : 3, "m_end" : 3},
		{"id" : 3, "result" : "공연/NNG+을/JKO", "target" : "공연을", "word_id" : 3, "m_begin" : 4, "m_end" : 5},
		{"id" : 4, "result" : "비롯하/VV+어/EC", "target" : "비롯해", "word_id" : 4, "m_begin" : 6, "m_end" : 7},
		{"id" : 5, "result" : "2005/SN+년/NNB+까지/JX", "target" : "2005년까지", "word_id" : 5, "m_begin" : 8, "m_end" : 10},
		{"id" : 6, "result" : "왕성하/VA+ㄴ/ETM", "target" : "왕성한", "word_id" : 6, "m_begin" : 11, "m_end" : 13},
		{"id" : 7, "result" : "활동/NNG+을/JKO", "target" : "활동을", "word_id" : 7, "m_begin" : 14, "m_end" : 15},
		{"id" : 8, "result" : "하/VV+었/EP+던/ETM", "target" : "했던", "word_id" : 8, "m_begin" : 16, "m_end" : 18},
		{"id" : 9, "result" : "세계/NNG", "target" : "세계", "word_id" : 9, "m_begin" : 19, "m_end" : 19},
		{"id" : 10, "result" : "3/SN+대/NNG", "target" : "3대", "word_id" : 10, "m_begin" : 20, "m_end" : 21},
		{"id" : 11, "result" : "테너/NNG+가/JKC", "target" : "테너가", "word_id" : 11, "m_begin" : 22, "m_end" : 23},
		{"id" : 12, "result" : "아니/VCN+ㄴ/ETM", "target" : "아닌", "word_id" : 12, "m_begin" : 24, "m_end" : 25},
		{"id" : 13, "result" : "사람/NNG+은/JX", "target" : "사람은", "word_id" : 13, "m_begin" : 26, "m_end" : 27},
		{"id" : 14, "result" : "누구/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "누구일까?", "word_id" : 14, "m_begin" : 28, "m_end" : 31}
	],
	"WSD" : [
		{"id" : 0, "text" : "1990", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 4, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "로마", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 8, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "월드컵", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 15, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "공연", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 25, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 31, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "비롯하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 35, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 41, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "2005", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 45, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 49, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "까지", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 52, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "왕성하", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 59, "begin" : 11, "end" : 12},
		{"id" : 12, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 65, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "활동", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 69, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 75, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 79, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 79, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "던", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 82, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "세계", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 86, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "3", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 93, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "대", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 94, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : "테너", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 98, "begin" : 22, "end" : 22},
		{"id" : 22, "text" : "가", "type" : "JKC", "scode" : "00", "weight" : 1, "position" : 104, "begin" : 23, "end" : 23},
		{"id" : 23, "text" : "아니", "type" : "VCN", "scode" : "00", "weight" : 1, "position" : 108, "begin" : 24, "end" : 24},
		{"id" : 24, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 111, "begin" : 25, "end" : 25},
		{"id" : 25, "text" : "사람", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 115, "begin" : 26, "end" : 26},
		{"id" : 26, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 121, "begin" : 27, "end" : 27},
		{"id" : 27, "text" : "누구", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 125, "begin" : 28, "end" : 28},
		{"id" : 28, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 131, "begin" : 29, "end" : 29},
		{"id" : 29, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 131, "begin" : 30, "end" : 30},
		{"id" : 30, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 137, "begin" : 31, "end" : 31}
	],
	"word" : [
		{"id" : 0, "text" : "1990년", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "로마", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "월드컵", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "공연을", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 4, "text" : "비롯해", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 5, "text" : "2005년까지", "type" : "", "begin" : 8, "end" : 10},
		{"id" : 6, "text" : "왕성한", "type" : "", "begin" : 11, "end" : 13},
		{"id" : 7, "text" : "활동을", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 8, "text" : "했던", "type" : "", "begin" : 16, "end" : 18},
		{"id" : 9, "text" : "세계", "type" : "", "begin" : 19, "end" : 19},
		{"id" : 10, "text" : "3대", "type" : "", "begin" : 20, "end" : 21},
		{"id" : 11, "text" : "테너가", "type" : "", "begin" : 22, "end" : 23},
		{"id" : 12, "text" : "아닌", "type" : "", "begin" : 24, "end" : 25},
		{"id" : 13, "text" : "사람은", "type" : "", "begin" : 26, "end" : 27},
		{"id" : 14, "text" : "누구일까?", "type" : "", "begin" : 28, "end" : 31}
	],
	"NE" : [
		{"id" : 0, "text" : "1990년", "type" : "DT_YEAR", "begin" : 0, "end" : 1, "weight" : 0.758213, "common_noun" : 0},
		{"id" : 1, "text" : "로마 월드컵", "type" : "EV_SPORTS", "begin" : 2, "end" : 3, "weight" : 0.603493, "common_noun" : 0},
		{"id" : 2, "text" : "2005년까지", "type" : "DT_OTHERS", "begin" : 8, "end" : 10, "weight" : 0.762041, "common_noun" : 0},
		{"id" : 3, "text" : "3대", "type" : "QT_COUNT", "begin" : 20, "end" : 21, "weight" : 0.729341, "common_noun" : 0},
		{"id" : 4, "text" : "테너", "type" : "CV_POSITION", "begin" : 22, "end" : 22, "weight" : 0.275597, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "1990년", "head" : 4, "label" : "NP_AJT", "mod" : [], "weight" : 0.556098 },
		{"id" : 1, "text" : "로마", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.0171216 },
		{"id" : 2, "text" : "월드컵", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.570696 },
		{"id" : 3, "text" : "공연을", "head" : 4, "label" : "NP_OBJ", "mod" : [2], "weight" : 0.843662 },
		{"id" : 4, "text" : "비롯해", "head" : 8, "label" : "VP", "mod" : [0, 3], "weight" : 0.385899 },
		{"id" : 5, "text" : "2005년까지", "head" : 8, "label" : "NP_AJT", "mod" : [], "weight" : 0.520323 },
		{"id" : 6, "text" : "왕성한", "head" : 7, "label" : "VP_MOD", "mod" : [], "weight" : 0.711581 },
		{"id" : 7, "text" : "활동을", "head" : 8, "label" : "NP_OBJ", "mod" : [6], "weight" : 0.785612 },
		{"id" : 8, "text" : "했던", "head" : 11, "label" : "VP_MOD", "mod" : [4, 5, 7], "weight" : 0.353268 },
		{"id" : 9, "text" : "세계", "head" : 11, "label" : "NP", "mod" : [], "weight" : 0.333092 },
		{"id" : 10, "text" : "3대", "head" : 11, "label" : "NP", "mod" : [], "weight" : 0.703903 },
		{"id" : 11, "text" : "테너가", "head" : 12, "label" : "NP_CMP", "mod" : [8, 9, 10], "weight" : 0.691516 },
		{"id" : 12, "text" : "아닌", "head" : 13, "label" : "VP_MOD", "mod" : [11], "weight" : 0.679768 },
		{"id" : 13, "text" : "사람은", "head" : 14, "label" : "NP_SBJ", "mod" : [12], "weight" : 0.891544 },
		{"id" : 14, "text" : "누구일까?", "head" : -1, "label" : "VNP", "mod" : [13], "weight" : 1.50908e-05 }
	],
	"SRL" : [
		{"verb" : "비롯하", "sense" : 1, "word_id" : 4, "weight" : 0.707698,
			"argument" : [
				{"type" : "ARG2", "word_id" : 3, "text" : "공연을", "weight" : 0.707698 }
			] },
		{"verb" : "왕성", "sense" : 1, "word_id" : 6, "weight" : 0.531068,
			"argument" : [
				{"type" : "ARG1", "word_id" : 7, "text" : "활동을", "weight" : 0.531068 }
			] },
		{"verb" : "하", "sense" : 1, "word_id" : 8, "weight" : 0.387502,
			"argument" : [
				{"type" : "ARG1", "word_id" : 7, "text" : "활동을", "weight" : 0.47191 },
				{"type" : "ARG0", "word_id" : 11, "text" : "테너가", "weight" : 0.303093 }
			] },
		{"verb" : "아니", "sense" : 1, "word_id" : 12, "weight" : 0.375658,
			"argument" : [
				{"type" : "ARG2", "word_id" : 11, "text" : "테너가", "weight" : 0.551697 },
				{"type" : "ARG1", "word_id" : 13, "text" : "사람은", "weight" : 0.199619 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 13, "ne_id" : -1, "text" : "1990년 로마 월드컵 공연을 비롯해 2005년까지 왕성한 활동을 했던 세계 3대 테너가 아닌 사람", "start_eid_short" : 13, "end_eid_short" : 13, "text_short" : "사람", "weight" : 0.002 },
		{"id" : 9, "sent_id" : 0, "start_eid" : 13, "end_eid" : 14, "ne_id" : -1, "text" : "누구", "start_eid_short" : 14, "end_eid_short" : 14, "text_short" : "누구이ㄹ까?", "weight" : 0.006 }
	] }
 ]
}
