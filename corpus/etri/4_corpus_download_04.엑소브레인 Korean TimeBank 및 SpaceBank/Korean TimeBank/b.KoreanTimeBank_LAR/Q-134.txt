{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿우리나라 예법은 크게 관례, 혼례, 상례, 제례로 나뉜다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿우리나라", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "예법", "type" : "NNG", "position" : 16, "weight" : 0.9 },
		{"id" : 2, "lemma" : "은", "type" : "JX", "position" : 22, "weight" : 0.0449928 },
		{"id" : 3, "lemma" : "크", "type" : "VA", "position" : 26, "weight" : 0.107542 },
		{"id" : 4, "lemma" : "게", "type" : "EC", "position" : 29, "weight" : 0.31879 },
		{"id" : 5, "lemma" : "관례", "type" : "NNG", "position" : 33, "weight" : 0.9 },
		{"id" : 6, "lemma" : ",", "type" : "SP", "position" : 39, "weight" : 1 },
		{"id" : 7, "lemma" : "혼례", "type" : "NNG", "position" : 41, "weight" : 0.9 },
		{"id" : 8, "lemma" : ",", "type" : "SP", "position" : 47, "weight" : 1 },
		{"id" : 9, "lemma" : "상례", "type" : "NNG", "position" : 49, "weight" : 0.9 },
		{"id" : 10, "lemma" : ",", "type" : "SP", "position" : 55, "weight" : 1 },
		{"id" : 11, "lemma" : "제례", "type" : "NNG", "position" : 57, "weight" : 0.9 },
		{"id" : 12, "lemma" : "로", "type" : "JKB", "position" : 63, "weight" : 0.153229 },
		{"id" : 13, "lemma" : "나뉘", "type" : "VV", "position" : 67, "weight" : 0.9 },
		{"id" : 14, "lemma" : "ㄴ다", "type" : "EF", "position" : 70, "weight" : 0.0793196 },
		{"id" : 15, "lemma" : ".", "type" : "SF", "position" : 76, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿우리나라/NNG", "target" : "﻿우리나라", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "예법/NNG+은/JX", "target" : "예법은", "word_id" : 1, "m_begin" : 1, "m_end" : 2},
		{"id" : 2, "result" : "크/VA+게/EC", "target" : "크게", "word_id" : 2, "m_begin" : 3, "m_end" : 4},
		{"id" : 3, "result" : "관례/NNG+,/SP", "target" : "관례,", "word_id" : 3, "m_begin" : 5, "m_end" : 6},
		{"id" : 4, "result" : "혼례/NNG+,/SP", "target" : "혼례,", "word_id" : 4, "m_begin" : 7, "m_end" : 8},
		{"id" : 5, "result" : "상례/NNG+,/SP", "target" : "상례,", "word_id" : 5, "m_begin" : 9, "m_end" : 10},
		{"id" : 6, "result" : "제례/NNG+로/JKB", "target" : "제례로", "word_id" : 6, "m_begin" : 11, "m_end" : 12},
		{"id" : 7, "result" : "나뉘/VV+ㄴ다/EF+./SF", "target" : "나뉜다.", "word_id" : 7, "m_begin" : 13, "m_end" : 15}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿우리나라", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "예법", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 16, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 22, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "크", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 26, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "게", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 29, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "관례", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 33, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 39, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "혼례", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 41, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 47, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "상례", "type" : "NNG", "scode" : "07", "weight" : 1, "position" : 49, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 55, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "제례", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 57, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 63, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "나뉘", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 67, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "ㄴ다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 70, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 76, "begin" : 15, "end" : 15}
	],
	"word" : [
		{"id" : 0, "text" : "﻿우리나라", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "예법은", "type" : "", "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "크게", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 3, "text" : "관례,", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 4, "text" : "혼례,", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 5, "text" : "상례,", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 6, "text" : "제례로", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 7, "text" : "나뉜다.", "type" : "", "begin" : 13, "end" : 15}
	],
	"NE" : [
		{"id" : 0, "text" : "﻿우리나라", "type" : "OGG_ECONOMY", "begin" : 0, "end" : 0, "weight" : 0.0950165, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿우리나라", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.807874 },
		{"id" : 1, "text" : "예법은", "head" : 7, "label" : "NP_SBJ", "mod" : [0], "weight" : 0.660347 },
		{"id" : 2, "text" : "크게", "head" : 7, "label" : "VP_AJT", "mod" : [], "weight" : 0.550714 },
		{"id" : 3, "text" : "관례,", "head" : 6, "label" : "NP_CNJ", "mod" : [], "weight" : 0.80779 },
		{"id" : 4, "text" : "혼례,", "head" : 6, "label" : "NP_CNJ", "mod" : [], "weight" : 0.876099 },
		{"id" : 5, "text" : "상례,", "head" : 6, "label" : "NP_CNJ", "mod" : [], "weight" : 0.800479 },
		{"id" : 6, "text" : "제례로", "head" : 7, "label" : "NP_AJT", "mod" : [3, 4, 5], "weight" : 0.466573 },
		{"id" : 7, "text" : "나뉜다.", "head" : -1, "label" : "VP", "mod" : [1, 2, 6], "weight" : 0.0523886 }
	],
	"SRL" : [
		{"verb" : "크", "sense" : 1, "word_id" : 2, "weight" : 0,
			"argument" : [
			] },
		{"verb" : "나뉘", "sense" : 1, "word_id" : 7, "weight" : 0.198552,
			"argument" : [
				{"type" : "ARG1", "word_id" : 1, "text" : "예법은", "weight" : 0.222232 },
				{"type" : "ARGM-EXT", "word_id" : 2, "text" : "크게", "weight" : 0.259015 },
				{"type" : "ARG2", "word_id" : 6, "text" : "제례로", "weight" : 0.114407 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "이 중 '관례'란 오늘날 어떤 의식과 관련이 있을까? ",
	"morp" : [
		{"id" : 0, "lemma" : "이", "type" : "MM", "position" : 78, "weight" : 0.0020222 },
		{"id" : 1, "lemma" : "중", "type" : "NNB", "position" : 82, "weight" : 0.154562 },
		{"id" : 2, "lemma" : "'", "type" : "SS", "position" : 86, "weight" : 1 },
		{"id" : 3, "lemma" : "관례", "type" : "NNG", "position" : 87, "weight" : 0.9 },
		{"id" : 4, "lemma" : "'", "type" : "SS", "position" : 93, "weight" : 1 },
		{"id" : 5, "lemma" : "이", "type" : "VCP", "position" : 94, "weight" : 0.00747344 },
		{"id" : 6, "lemma" : "란", "type" : "ETM", "position" : 94, "weight" : 0.165137 },
		{"id" : 7, "lemma" : "오늘", "type" : "NNG", "position" : 98, "weight" : 0.542817 },
		{"id" : 8, "lemma" : "날", "type" : "NNG", "position" : 104, "weight" : 0.167745 },
		{"id" : 9, "lemma" : "어떤", "type" : "MM", "position" : 108, "weight" : 0.00334865 },
		{"id" : 10, "lemma" : "의식", "type" : "NNG", "position" : 115, "weight" : 0.737157 },
		{"id" : 11, "lemma" : "과", "type" : "JKB", "position" : 121, "weight" : 0.0503087 },
		{"id" : 12, "lemma" : "관련", "type" : "NNG", "position" : 125, "weight" : 0.9 },
		{"id" : 13, "lemma" : "이", "type" : "JKS", "position" : 131, "weight" : 0.0360723 },
		{"id" : 14, "lemma" : "있", "type" : "VA", "position" : 135, "weight" : 0.12767 },
		{"id" : 15, "lemma" : "을까", "type" : "EF", "position" : 138, "weight" : 0.10243 },
		{"id" : 16, "lemma" : "?", "type" : "SF", "position" : 144, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이/MM", "target" : "이", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "중/NNB", "target" : "중", "word_id" : 1, "m_begin" : 1, "m_end" : 1},
		{"id" : 2, "result" : "'/SS+관례/NNG+'/SS+이/VCP+란/ETM", "target" : "'관례'란", "word_id" : 2, "m_begin" : 2, "m_end" : 6},
		{"id" : 3, "result" : "오늘날/NNG", "target" : "오늘날", "word_id" : 3, "m_begin" : 7, "m_end" : 8},
		{"id" : 4, "result" : "어떤/MM", "target" : "어떤", "word_id" : 4, "m_begin" : 9, "m_end" : 9},
		{"id" : 5, "result" : "의식/NNG+과/JKB", "target" : "의식과", "word_id" : 5, "m_begin" : 10, "m_end" : 11},
		{"id" : 6, "result" : "관련/NNG+이/JKS", "target" : "관련이", "word_id" : 6, "m_begin" : 12, "m_end" : 13},
		{"id" : 7, "result" : "있/VA+을까/EF+?/SF", "target" : "있을까?", "word_id" : 7, "m_begin" : 14, "m_end" : 16}
	],
	"WSD" : [
		{"id" : 0, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 78, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "중", "type" : "NNB", "scode" : "04", "weight" : 1, "position" : 82, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 86, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "관례", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 87, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 93, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 94, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "란", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 94, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "오늘날", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 98, "begin" : 7, "end" : 8},
		{"id" : 8, "text" : "어떤", "type" : "MM", "scode" : "00", "weight" : 1, "position" : 108, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "의식", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 115, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "과", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 121, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "관련", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 125, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 131, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "있", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 135, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "을까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 138, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 144, "begin" : 16, "end" : 16}
	],
	"word" : [
		{"id" : 0, "text" : "이", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "중", "type" : "", "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "'관례'란", "type" : "", "begin" : 2, "end" : 6},
		{"id" : 3, "text" : "오늘날", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 4, "text" : "어떤", "type" : "", "begin" : 9, "end" : 9},
		{"id" : 5, "text" : "의식과", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 6, "text" : "관련이", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 7, "text" : "있을까?", "type" : "", "begin" : 14, "end" : 16}
	],
	"NE" : [
		{"id" : 0, "text" : "오늘날", "type" : "DT_OTHERS", "begin" : 7, "end" : 8, "weight" : 0.508167, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이", "head" : 1, "label" : "DP", "mod" : [], "weight" : 0.950284 },
		{"id" : 1, "text" : "중", "head" : 7, "label" : "NP_AJT", "mod" : [0], "weight" : 0.599064 },
		{"id" : 2, "text" : "'관례'란", "head" : 7, "label" : "NP_SBJ", "mod" : [], "weight" : 0.542186 },
		{"id" : 3, "text" : "오늘날", "head" : 7, "label" : "NP_AJT", "mod" : [], "weight" : 0.647007 },
		{"id" : 4, "text" : "어떤", "head" : 5, "label" : "DP", "mod" : [], "weight" : 0.742165 },
		{"id" : 5, "text" : "의식과", "head" : 7, "label" : "NP_AJT", "mod" : [4], "weight" : 0.573184 },
		{"id" : 6, "text" : "관련이", "head" : 7, "label" : "NP_SBJ", "mod" : [], "weight" : 0.52871 },
		{"id" : 7, "text" : "있을까?", "head" : -1, "label" : "VP", "mod" : [1, 2, 3, 5, 6], "weight" : 0.0326849 }
	],
	"SRL" : [
		{"verb" : "있", "sense" : 1, "word_id" : 7, "weight" : 0.173915,
			"argument" : [
				{"type" : "ARG1", "word_id" : 2, "text" : "'관례'란", "weight" : 0.140656 },
				{"type" : "ARG2", "word_id" : 5, "text" : "의식과", "weight" : 0.125172 },
				{"type" : "ARG1", "word_id" : 6, "text" : "관련이", "weight" : 0.255918 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
 ]
}

