{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "지금 들으신 곡은 판소리 단가 '사철가'입니다.",
	"morp" : [
		{"id" : 0, "lemma" : "지금", "type" : "MAG", "position" : 0, "weight" : 0.0282617 },
		{"id" : 1, "lemma" : "듣", "type" : "VV", "position" : 7, "weight" : 0.9 },
		{"id" : 2, "lemma" : "으시", "type" : "EP", "position" : 10, "weight" : 0.9 },
		{"id" : 3, "lemma" : "ㄴ", "type" : "ETM", "position" : 13, "weight" : 0.103064 },
		{"id" : 4, "lemma" : "곡", "type" : "NNG", "position" : 17, "weight" : 0.585242 },
		{"id" : 5, "lemma" : "은", "type" : "JX", "position" : 20, "weight" : 0.0449928 },
		{"id" : 6, "lemma" : "판", "type" : "NNG", "position" : 24, "weight" : 0.224946 },
		{"id" : 7, "lemma" : "소리", "type" : "NNG", "position" : 27, "weight" : 0.9 },
		{"id" : 8, "lemma" : "단가", "type" : "NNG", "position" : 34, "weight" : 0.179371 },
		{"id" : 9, "lemma" : "'", "type" : "SS", "position" : 41, "weight" : 1 },
		{"id" : 10, "lemma" : "사철가", "type" : "NNP", "position" : 42, "weight" : 0.6 },
		{"id" : 11, "lemma" : "'", "type" : "SS", "position" : 51, "weight" : 1 },
		{"id" : 12, "lemma" : "이", "type" : "VCP", "position" : 52, "weight" : 0.00747344 },
		{"id" : 13, "lemma" : "ㅂ니다", "type" : "EF", "position" : 52, "weight" : 0.36174 },
		{"id" : 14, "lemma" : ".", "type" : "SF", "position" : 61, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "지금/MAG", "target" : "지금", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "듣/VV+으시/EP+ㄴ/ETM", "target" : "들으신", "word_id" : 1, "m_begin" : 1, "m_end" : 3},
		{"id" : 2, "result" : "곡/NNG+은/JX", "target" : "곡은", "word_id" : 2, "m_begin" : 4, "m_end" : 5},
		{"id" : 3, "result" : "판소리/NNG", "target" : "판소리", "word_id" : 3, "m_begin" : 6, "m_end" : 7},
		{"id" : 4, "result" : "단가/NNG", "target" : "단가", "word_id" : 4, "m_begin" : 8, "m_end" : 8},
		{"id" : 5, "result" : "'/SS+사철가/NNG+'/SS+이/VCP+ㅂ니다/EF+./SF", "target" : "'사철가'입니다.", "word_id" : 5, "m_begin" : 9, "m_end" : 14}
	],
	"WSD" : [
		{"id" : 0, "text" : "지금", "type" : "MAG", "scode" : "03", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "듣", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 7, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "으시", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 13, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "곡", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 17, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 20, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "판소리", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 24, "begin" : 6, "end" : 7},
		{"id" : 7, "text" : "단가", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 34, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 41, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "사철가", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 42, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 51, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 52, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "ㅂ니다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 52, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 61, "begin" : 14, "end" : 14}
	],
	"word" : [
		{"id" : 0, "text" : "지금", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "들으신", "type" : "", "begin" : 1, "end" : 3},
		{"id" : 2, "text" : "곡은", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 3, "text" : "판소리", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 4, "text" : "단가", "type" : "", "begin" : 8, "end" : 8},
		{"id" : 5, "text" : "'사철가'입니다.", "type" : "", "begin" : 9, "end" : 14}
	],
	"NE" : [
		{"id" : 0, "text" : "판소리", "type" : "FD_ART", "begin" : 6, "end" : 7, "weight" : 0.459305, "common_noun" : 0},
		{"id" : 1, "text" : "단가", "type" : "FD_ART", "begin" : 8, "end" : 8, "weight" : 0.203729, "common_noun" : 0},
		{"id" : 2, "text" : "사철가", "type" : "AFW_MUSIC", "begin" : 10, "end" : 10, "weight" : 0.200526, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "지금", "head" : 1, "label" : "AP", "mod" : [], "weight" : 0.808573 },
		{"id" : 1, "text" : "들으신", "head" : 2, "label" : "VP_MOD", "mod" : [0], "weight" : 0.93655 },
		{"id" : 2, "text" : "곡은", "head" : 5, "label" : "NP_SBJ", "mod" : [1], "weight" : 0.733228 },
		{"id" : 3, "text" : "판소리", "head" : 4, "label" : "NP", "mod" : [], "weight" : 0.694532 },
		{"id" : 4, "text" : "단가", "head" : 5, "label" : "NP", "mod" : [3], "weight" : 0.423813 },
		{"id" : 5, "text" : "'사철가'입니다.", "head" : -1, "label" : "VNP", "mod" : [2, 4], "weight" : 0.0890878 }
	],
	"SRL" : [
		{"verb" : "듣", "sense" : 1, "word_id" : 1, "weight" : 0.314432,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 0, "text" : "지금", "weight" : 0.264882 },
				{"type" : "ARG1", "word_id" : 2, "text" : "곡은", "weight" : 0.363982 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "조선 순조 때 '송만재'가 지은 이 책에서는 판소리 단가를 '영산'이라고 표현했습니다.",
	"morp" : [
		{"id" : 0, "lemma" : "조선", "type" : "NNP", "position" : 62, "weight" : 0.0878802 },
		{"id" : 1, "lemma" : "순조", "type" : "NNP", "position" : 69, "weight" : 0.0791528 },
		{"id" : 2, "lemma" : "때", "type" : "NNG", "position" : 76, "weight" : 0.280999 },
		{"id" : 3, "lemma" : "'", "type" : "SS", "position" : 80, "weight" : 1 },
		{"id" : 4, "lemma" : "송만재", "type" : "NNP", "position" : 81, "weight" : 0.15 },
		{"id" : 5, "lemma" : "'", "type" : "SS", "position" : 90, "weight" : 1 },
		{"id" : 6, "lemma" : "가", "type" : "JKS", "position" : 91, "weight" : 0.0144542 },
		{"id" : 7, "lemma" : "짓", "type" : "VV", "position" : 95, "weight" : 0.256547 },
		{"id" : 8, "lemma" : "은", "type" : "ETM", "position" : 98, "weight" : 0.109632 },
		{"id" : 9, "lemma" : "이", "type" : "MM", "position" : 102, "weight" : 0.00060084 },
		{"id" : 10, "lemma" : "책", "type" : "NNG", "position" : 106, "weight" : 0.718743 },
		{"id" : 11, "lemma" : "에서", "type" : "JKB", "position" : 109, "weight" : 0.153407 },
		{"id" : 12, "lemma" : "는", "type" : "JX", "position" : 115, "weight" : 0.0387928 },
		{"id" : 13, "lemma" : "판", "type" : "NNG", "position" : 119, "weight" : 0.224946 },
		{"id" : 14, "lemma" : "소리", "type" : "NNG", "position" : 122, "weight" : 0.9 },
		{"id" : 15, "lemma" : "단가", "type" : "NNG", "position" : 129, "weight" : 0.179371 },
		{"id" : 16, "lemma" : "를", "type" : "JKO", "position" : 135, "weight" : 0.137686 },
		{"id" : 17, "lemma" : "'", "type" : "SS", "position" : 139, "weight" : 1 },
		{"id" : 18, "lemma" : "영산", "type" : "NNG", "position" : 140, "weight" : 0.125582 },
		{"id" : 19, "lemma" : "'", "type" : "SS", "position" : 146, "weight" : 1 },
		{"id" : 20, "lemma" : "이라고", "type" : "JKQ", "position" : 147, "weight" : 0.0202133 },
		{"id" : 21, "lemma" : "표현", "type" : "NNG", "position" : 157, "weight" : 0.9 },
		{"id" : 22, "lemma" : "하", "type" : "XSV", "position" : 163, "weight" : 0.0001 },
		{"id" : 23, "lemma" : "었", "type" : "EP", "position" : 163, "weight" : 0.9 },
		{"id" : 24, "lemma" : "습니다", "type" : "EF", "position" : 166, "weight" : 0.660908 },
		{"id" : 25, "lemma" : ".", "type" : "SF", "position" : 175, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "조선/NNG", "target" : "조선", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "순조/NNG", "target" : "순조", "word_id" : 1, "m_begin" : 1, "m_end" : 1},
		{"id" : 2, "result" : "때/NNG", "target" : "때", "word_id" : 2, "m_begin" : 2, "m_end" : 2},
		{"id" : 3, "result" : "'/SS+송만재/NNG+'/SS+가/JKS", "target" : "'송만재'가", "word_id" : 3, "m_begin" : 3, "m_end" : 6},
		{"id" : 4, "result" : "짓/VV+은/ETM", "target" : "지은", "word_id" : 4, "m_begin" : 7, "m_end" : 8},
		{"id" : 5, "result" : "이/MM", "target" : "이", "word_id" : 5, "m_begin" : 9, "m_end" : 9},
		{"id" : 6, "result" : "책/NNG+에서/JKB+는/JX", "target" : "책에서는", "word_id" : 6, "m_begin" : 10, "m_end" : 12},
		{"id" : 7, "result" : "판소리/NNG", "target" : "판소리", "word_id" : 7, "m_begin" : 13, "m_end" : 14},
		{"id" : 8, "result" : "단가/NNG+를/JKO", "target" : "단가를", "word_id" : 8, "m_begin" : 15, "m_end" : 16},
		{"id" : 9, "result" : "'/SS+영산/NNG+'/SS+이라고/JKQ", "target" : "'영산'이라고", "word_id" : 9, "m_begin" : 17, "m_end" : 20},
		{"id" : 10, "result" : "표현하/VV+었/EP+습니다/EF+./SF", "target" : "표현했습니다.", "word_id" : 10, "m_begin" : 21, "m_end" : 25}
	],
	"WSD" : [
		{"id" : 0, "text" : "조선", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 62, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "순조", "type" : "NNP", "scode" : "01", "weight" : 1, "position" : 69, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "때", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 76, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 80, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "송만재", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 81, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 90, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "가", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 91, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "짓", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 95, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "은", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 98, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 102, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "책", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 106, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 109, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 115, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "판소리", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 119, "begin" : 13, "end" : 14},
		{"id" : 14, "text" : "단가", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 129, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 135, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 139, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "영산", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 140, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 146, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "이라고", "type" : "JKQ", "scode" : "00", "weight" : 1, "position" : 147, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "표현하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 157, "begin" : 21, "end" : 22},
		{"id" : 21, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 163, "begin" : 23, "end" : 23},
		{"id" : 22, "text" : "습니다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 166, "begin" : 24, "end" : 24},
		{"id" : 23, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 175, "begin" : 25, "end" : 25}
	],
	"word" : [
		{"id" : 0, "text" : "조선", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "순조", "type" : "", "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "때", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "'송만재'가", "type" : "", "begin" : 3, "end" : 6},
		{"id" : 4, "text" : "지은", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 5, "text" : "이", "type" : "", "begin" : 9, "end" : 9},
		{"id" : 6, "text" : "책에서는", "type" : "", "begin" : 10, "end" : 12},
		{"id" : 7, "text" : "판소리", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 8, "text" : "단가를", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 9, "text" : "'영산'이라고", "type" : "", "begin" : 17, "end" : 20},
		{"id" : 10, "text" : "표현했습니다.", "type" : "", "begin" : 21, "end" : 25}
	],
	"NE" : [
		{"id" : 0, "text" : "조선 순조 때", "type" : "DT_DYNASTY", "begin" : 0, "end" : 2, "weight" : 0.533652, "common_noun" : 0},
		{"id" : 1, "text" : "송만재", "type" : "PS_NAME", "begin" : 4, "end" : 4, "weight" : 0.358674, "common_noun" : 0},
		{"id" : 2, "text" : "판소리", "type" : "FD_ART", "begin" : 13, "end" : 14, "weight" : 0.411637, "common_noun" : 0},
		{"id" : 3, "text" : "영산", "type" : "LCG_MOUNTAIN", "begin" : 18, "end" : 18, "weight" : 0.143954, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "조선", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.864496 },
		{"id" : 1, "text" : "순조", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.939224 },
		{"id" : 2, "text" : "때", "head" : 4, "label" : "NP_AJT", "mod" : [1], "weight" : 0.857979 },
		{"id" : 3, "text" : "'송만재'가", "head" : 4, "label" : "NP_SBJ", "mod" : [], "weight" : 0.998732 },
		{"id" : 4, "text" : "지은", "head" : 6, "label" : "VP_MOD", "mod" : [2, 3], "weight" : 0.783071 },
		{"id" : 5, "text" : "이", "head" : 6, "label" : "DP", "mod" : [], "weight" : 0.867503 },
		{"id" : 6, "text" : "책에서는", "head" : 10, "label" : "NP_AJT", "mod" : [4, 5], "weight" : 0.85311 },
		{"id" : 7, "text" : "판소리", "head" : 8, "label" : "NP", "mod" : [], "weight" : 0.793335 },
		{"id" : 8, "text" : "단가를", "head" : 9, "label" : "NP_OBJ", "mod" : [7], "weight" : 0.749267 },
		{"id" : 9, "text" : "'영산'이라고", "head" : 10, "label" : "NP_CMP", "mod" : [8], "weight" : 0.365051 },
		{"id" : 10, "text" : "표현했습니다.", "head" : -1, "label" : "VP", "mod" : [6, 9], "weight" : 0.0545964 }
	],
	"SRL" : [
		{"verb" : "짓", "sense" : 1, "word_id" : 4, "weight" : 0.418903,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 2, "text" : "때", "weight" : 0.520214 },
				{"type" : "ARG0", "word_id" : 3, "text" : "'송만재'가", "weight" : 0.448085 },
				{"type" : "ARG1", "word_id" : 6, "text" : "책에서는", "weight" : 0.288411 }
			] },
		{"verb" : "표현", "sense" : 1, "word_id" : 10, "weight" : 0.153702,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 6, "text" : "책에서는", "weight" : 0.160359 },
				{"type" : "ARG1", "word_id" : 9, "text" : "'영산'이라고", "weight" : 0.147045 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 10, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.779263 }
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "이책은 판소리 12마당에 관한 문헌으로 판소리를 연구하는데 빼놓을 수 없는 중요한 책인데 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "이", "type" : "MM", "position" : 176, "weight" : 0.0020222 },
		{"id" : 1, "lemma" : "책", "type" : "NNG", "position" : 179, "weight" : 0.718743 },
		{"id" : 2, "lemma" : "은", "type" : "JX", "position" : 182, "weight" : 0.0449928 },
		{"id" : 3, "lemma" : "판", "type" : "NNG", "position" : 186, "weight" : 0.224946 },
		{"id" : 4, "lemma" : "소리", "type" : "NNG", "position" : 189, "weight" : 0.9 },
		{"id" : 5, "lemma" : "12", "type" : "SN", "position" : 196, "weight" : 1 },
		{"id" : 6, "lemma" : "마당", "type" : "NNG", "position" : 198, "weight" : 0.0778063 },
		{"id" : 7, "lemma" : "에", "type" : "JKB", "position" : 204, "weight" : 0.153364 },
		{"id" : 8, "lemma" : "관", "type" : "NNG", "position" : 208, "weight" : 0.130437 },
		{"id" : 9, "lemma" : "하", "type" : "XSV", "position" : 211, "weight" : 0.0001 },
		{"id" : 10, "lemma" : "ㄴ", "type" : "ETM", "position" : 211, "weight" : 0.392321 },
		{"id" : 11, "lemma" : "문헌", "type" : "NNG", "position" : 215, "weight" : 0.657364 },
		{"id" : 12, "lemma" : "으로", "type" : "JKB", "position" : 221, "weight" : 0.153406 },
		{"id" : 13, "lemma" : "판", "type" : "NNG", "position" : 228, "weight" : 0.144032 },
		{"id" : 14, "lemma" : "소리", "type" : "NNG", "position" : 231, "weight" : 0.9 },
		{"id" : 15, "lemma" : "를", "type" : "JKO", "position" : 237, "weight" : 0.137686 },
		{"id" : 16, "lemma" : "연구", "type" : "NNG", "position" : 241, "weight" : 0.9 },
		{"id" : 17, "lemma" : "하", "type" : "XSV", "position" : 247, "weight" : 0.0001 },
		{"id" : 18, "lemma" : "는데", "type" : "EC", "position" : 250, "weight" : 0.35474 },
		{"id" : 19, "lemma" : "빼놓", "type" : "VV", "position" : 257, "weight" : 0.9 },
		{"id" : 20, "lemma" : "을", "type" : "ETM", "position" : 263, "weight" : 0.0216774 },
		{"id" : 21, "lemma" : "수", "type" : "NNB", "position" : 267, "weight" : 0.215617 },
		{"id" : 22, "lemma" : "없", "type" : "VA", "position" : 271, "weight" : 0.101269 },
		{"id" : 23, "lemma" : "는", "type" : "ETM", "position" : 274, "weight" : 0.26168 },
		{"id" : 24, "lemma" : "중요", "type" : "NNG", "position" : 278, "weight" : 0.9 },
		{"id" : 25, "lemma" : "하", "type" : "XSA", "position" : 284, "weight" : 0.0001 },
		{"id" : 26, "lemma" : "ㄴ", "type" : "ETM", "position" : 284, "weight" : 0.488779 },
		{"id" : 27, "lemma" : "책", "type" : "NNG", "position" : 288, "weight" : 0.642125 },
		{"id" : 28, "lemma" : "이", "type" : "VCP", "position" : 291, "weight" : 0.0177525 },
		{"id" : 29, "lemma" : "ㄴ데", "type" : "EC", "position" : 291, "weight" : 0.209075 },
		{"id" : 30, "lemma" : "무엇", "type" : "NP", "position" : 298, "weight" : 0.9 },
		{"id" : 31, "lemma" : "이", "type" : "VCP", "position" : 304, "weight" : 0.0175768 },
		{"id" : 32, "lemma" : "ㄹ까", "type" : "EF", "position" : 304, "weight" : 0.258243 },
		{"id" : 33, "lemma" : "?", "type" : "SF", "position" : 310, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이/MM+책/NNG+은/JX", "target" : "이책은", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "판소리/NNG", "target" : "판소리", "word_id" : 1, "m_begin" : 3, "m_end" : 4},
		{"id" : 2, "result" : "12/SN+마당/NNG+에/JKB", "target" : "12마당에", "word_id" : 2, "m_begin" : 5, "m_end" : 7},
		{"id" : 3, "result" : "관하/VV+ㄴ/ETM", "target" : "관한", "word_id" : 3, "m_begin" : 8, "m_end" : 10},
		{"id" : 4, "result" : "문헌/NNG+으로/JKB", "target" : "문헌으로", "word_id" : 4, "m_begin" : 11, "m_end" : 12},
		{"id" : 5, "result" : "판소리/NNG+를/JKO", "target" : "판소리를", "word_id" : 5, "m_begin" : 13, "m_end" : 15},
		{"id" : 6, "result" : "연구하/VV+는데/EC", "target" : "연구하는데", "word_id" : 6, "m_begin" : 16, "m_end" : 18},
		{"id" : 7, "result" : "빼놓/VV+을/ETM", "target" : "빼놓을", "word_id" : 7, "m_begin" : 19, "m_end" : 20},
		{"id" : 8, "result" : "수/NNB", "target" : "수", "word_id" : 8, "m_begin" : 21, "m_end" : 21},
		{"id" : 9, "result" : "없/VA+는/ETM", "target" : "없는", "word_id" : 9, "m_begin" : 22, "m_end" : 23},
		{"id" : 10, "result" : "중요하/VA+ㄴ/ETM", "target" : "중요한", "word_id" : 10, "m_begin" : 24, "m_end" : 26},
		{"id" : 11, "result" : "책/NNG+이/VCP+ㄴ데/EC", "target" : "책인데", "word_id" : 11, "m_begin" : 27, "m_end" : 29},
		{"id" : 12, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 12, "m_begin" : 30, "m_end" : 33}
	],
	"WSD" : [
		{"id" : 0, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 176, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "책", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 179, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 182, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "판소리", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 186, "begin" : 3, "end" : 4},
		{"id" : 4, "text" : "12", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 196, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "마당", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 198, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 204, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "관하", "type" : "VV", "scode" : "02", "weight" : 1, "position" : 208, "begin" : 8, "end" : 9},
		{"id" : 8, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 211, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "문헌", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 215, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 221, "begin" : 12, "end" : 12},
		{"id" : 11, "text" : "판소리", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 228, "begin" : 13, "end" : 14},
		{"id" : 12, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 237, "begin" : 15, "end" : 15},
		{"id" : 13, "text" : "연구하", "type" : "VV", "scode" : "02", "weight" : 1, "position" : 241, "begin" : 16, "end" : 17},
		{"id" : 14, "text" : "는데", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 250, "begin" : 18, "end" : 18},
		{"id" : 15, "text" : "빼놓", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 257, "begin" : 19, "end" : 19},
		{"id" : 16, "text" : "을", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 263, "begin" : 20, "end" : 20},
		{"id" : 17, "text" : "수", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 267, "begin" : 21, "end" : 21},
		{"id" : 18, "text" : "없", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 271, "begin" : 22, "end" : 22},
		{"id" : 19, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 274, "begin" : 23, "end" : 23},
		{"id" : 20, "text" : "중요하", "type" : "VA", "scode" : "02", "weight" : 1, "position" : 278, "begin" : 24, "end" : 25},
		{"id" : 21, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 284, "begin" : 26, "end" : 26},
		{"id" : 22, "text" : "책", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 288, "begin" : 27, "end" : 27},
		{"id" : 23, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 291, "begin" : 28, "end" : 28},
		{"id" : 24, "text" : "ㄴ데", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 291, "begin" : 29, "end" : 29},
		{"id" : 25, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 298, "begin" : 30, "end" : 30},
		{"id" : 26, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 304, "begin" : 31, "end" : 31},
		{"id" : 27, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 304, "begin" : 32, "end" : 32},
		{"id" : 28, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 310, "begin" : 33, "end" : 33}
	],
	"word" : [
		{"id" : 0, "text" : "이책은", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "판소리", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 2, "text" : "12마당에", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 3, "text" : "관한", "type" : "", "begin" : 8, "end" : 10},
		{"id" : 4, "text" : "문헌으로", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 5, "text" : "판소리를", "type" : "", "begin" : 13, "end" : 15},
		{"id" : 6, "text" : "연구하는데", "type" : "", "begin" : 16, "end" : 18},
		{"id" : 7, "text" : "빼놓을", "type" : "", "begin" : 19, "end" : 20},
		{"id" : 8, "text" : "수", "type" : "", "begin" : 21, "end" : 21},
		{"id" : 9, "text" : "없는", "type" : "", "begin" : 22, "end" : 23},
		{"id" : 10, "text" : "중요한", "type" : "", "begin" : 24, "end" : 26},
		{"id" : 11, "text" : "책인데", "type" : "", "begin" : 27, "end" : 29},
		{"id" : 12, "text" : "무엇일까?", "type" : "", "begin" : 30, "end" : 33}
	],
	"NE" : [
		{"id" : 0, "text" : "판소리", "type" : "FD_ART", "begin" : 3, "end" : 4, "weight" : 0.38129, "common_noun" : 0},
		{"id" : 1, "text" : "12마당", "type" : "QT_COUNT", "begin" : 5, "end" : 6, "weight" : 0.197083, "common_noun" : 0},
		{"id" : 2, "text" : "판소리", "type" : "FD_ART", "begin" : 13, "end" : 14, "weight" : 0.475984, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이책은", "head" : 11, "label" : "NP_SBJ", "mod" : [], "weight" : 0.994828 },
		{"id" : 1, "text" : "판소리", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.823047 },
		{"id" : 2, "text" : "12마당에", "head" : 3, "label" : "NP_AJT", "mod" : [1], "weight" : 0.673886 },
		{"id" : 3, "text" : "관한", "head" : 4, "label" : "VP_MOD", "mod" : [2], "weight" : 0.967451 },
		{"id" : 4, "text" : "문헌으로", "head" : 10, "label" : "NP_AJT", "mod" : [3], "weight" : 0.705228 },
		{"id" : 5, "text" : "판소리를", "head" : 6, "label" : "NP_OBJ", "mod" : [], "weight" : 0.76683 },
		{"id" : 6, "text" : "연구하는데", "head" : 7, "label" : "VP", "mod" : [5], "weight" : 0.656908 },
		{"id" : 7, "text" : "빼놓을", "head" : 8, "label" : "VP_MOD", "mod" : [6], "weight" : 0.859946 },
		{"id" : 8, "text" : "수", "head" : 9, "label" : "NP_SBJ", "mod" : [7], "weight" : 0.874228 },
		{"id" : 9, "text" : "없는", "head" : 10, "label" : "VP_MOD", "mod" : [8], "weight" : 0.81974 },
		{"id" : 10, "text" : "중요한", "head" : 11, "label" : "VP_MOD", "mod" : [4, 9], "weight" : 0.684681 },
		{"id" : 11, "text" : "책인데", "head" : 12, "label" : "VNP", "mod" : [0, 10], "weight" : 0.771066 },
		{"id" : 12, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [11], "weight" : 0.039079 }
	],
	"SRL" : [
		{"verb" : "관", "sense" : 1, "word_id" : 3, "weight" : 0.293669,
			"argument" : [
				{"type" : "ARG2", "word_id" : 2, "text" : "12마당에", "weight" : 0.201654 },
				{"type" : "ARG1", "word_id" : 4, "text" : "문헌으로", "weight" : 0.385685 }
			] },
		{"verb" : "연구", "sense" : 1, "word_id" : 6, "weight" : 0.244777,
			"argument" : [
				{"type" : "ARGM-INS", "word_id" : 4, "text" : "문헌으로", "weight" : 0.174258 },
				{"type" : "ARG1", "word_id" : 5, "text" : "판소리를", "weight" : 0.315297 }
			] },
		{"verb" : "빼놓", "sense" : 1, "word_id" : 7, "weight" : 0.207334,
			"argument" : [
				{"type" : "ARG2", "word_id" : 6, "text" : "연구하는데", "weight" : 0.130037 },
				{"type" : "ARGM-NEG", "word_id" : 9, "text" : "없는", "weight" : 0.284632 }
			] },
		{"verb" : "중요", "sense" : 1, "word_id" : 10, "weight" : 0.190985,
			"argument" : [
				{"type" : "ARG1", "word_id" : 11, "text" : "책인데", "weight" : 0.190985 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 6, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.491651 },
		{"id" : 1, "verb_wid" : 12, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.339194 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 2, "ne_id" : -1, "text" : "지금 들으신 곡", "start_eid_short" : 2, "end_eid_short" : 2, "text_short" : "곡", "weight" : 0.002 },
		{"id" : 6, "sent_id" : 1, "start_eid" : 5, "end_eid" : 6, "ne_id" : -1, "text" : "이 책", "start_eid_short" : 5, "end_eid_short" : 6, "text_short" : "이 책", "weight" : 0.006 },
		{"id" : 11, "sent_id" : 2, "start_eid" : 0, "end_eid" : 2, "ne_id" : -1, "text" : "이책은 판소리 12마당", "start_eid_short" : 1, "end_eid_short" : 2, "text_short" : "판소리 12마당", "weight" : 0.006 }
	] },
	{"id" : 1, "type" : "FD_ART", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 1, "sent_id" : 0, "start_eid" : 3, "end_eid" : 4, "ne_id" : 1, "text" : "판소리 단가", "start_eid_short" : 3, "end_eid_short" : 4, "text_short" : "판소리 단가", "weight" : 0.01 },
		{"id" : 7, "sent_id" : 1, "start_eid" : 5, "end_eid" : 5, "ne_id" : -1, "text" : "이", "start_eid_short" : 5, "end_eid_short" : 5, "text_short" : "이", "weight" : 0.006 },
		{"id" : 8, "sent_id" : 1, "start_eid" : 7, "end_eid" : 8, "ne_id" : -1, "text" : "판소리 단가", "start_eid_short" : 7, "end_eid_short" : 8, "text_short" : "판소리 단가", "weight" : 0.016 },
		{"id" : 9, "sent_id" : 1, "start_eid" : 7, "end_eid" : 7, "ne_id" : 2, "text" : "판소리", "start_eid_short" : 7, "end_eid_short" : 7, "text_short" : "판소리", "weight" : 0.051 },
		{"id" : 2, "sent_id" : 0, "start_eid" : 3, "end_eid" : 3, "ne_id" : 0, "text" : "판소리", "start_eid_short" : 3, "end_eid_short" : 3, "text_short" : "판소리", "weight" : 0.045 },
		{"id" : 12, "sent_id" : 2, "start_eid" : 0, "end_eid" : 0, "ne_id" : -1, "text" : "이책", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "이책", "weight" : 0.008 },
		{"id" : 15, "sent_id" : 2, "start_eid" : 1, "end_eid" : 1, "ne_id" : 0, "text" : "판소리", "start_eid_short" : 1, "end_eid_short" : 1, "text_short" : "판소리", "weight" : 0.051 },
		{"id" : 16, "sent_id" : 2, "start_eid" : 5, "end_eid" : 5, "ne_id" : 2, "text" : "판소리", "start_eid_short" : 5, "end_eid_short" : 5, "text_short" : "판소리", "weight" : 0.051 },
		{"id" : 17, "sent_id" : 2, "start_eid" : 11, "end_eid" : 12, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 11, "end_eid_short" : 12, "text_short" : "책인데 무엇이ㄹ까?", "weight" : 0.012 }
	] },
	{"id" : 2, "type" : "AFW_MUSIC", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 5, "sent_id" : 1, "start_eid" : 3, "end_eid" : 3, "ne_id" : 1, "text" : "'송만재'", "start_eid_short" : 3, "end_eid_short" : 3, "text_short" : "'송만재'", "weight" : 0.01 },
		{"id" : 3, "sent_id" : 0, "start_eid" : 5, "end_eid" : 5, "ne_id" : 2, "text" : "'사철가'이ㅂ니다.", "start_eid_short" : 5, "end_eid_short" : 5, "text_short" : "'사철가'이ㅂ니다.", "weight" : 0.006 }
	] }
 ]
}

