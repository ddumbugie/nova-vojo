{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿조선 중기의 연시조인 고산 윤선도의 <어부사시사>에는 다양한 여음구가 등장한다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "조선", "type" : "NNP", "position" : 3, "weight" : 0.0796816 },
		{"id" : 2, "lemma" : "중기", "type" : "NNG", "position" : 10, "weight" : 0.279669 },
		{"id" : 3, "lemma" : "의", "type" : "JKG", "position" : 16, "weight" : 0.0694213 },
		{"id" : 4, "lemma" : "연", "type" : "NNG", "position" : 20, "weight" : 0.761352 },
		{"id" : 5, "lemma" : "시조", "type" : "NNG", "position" : 23, "weight" : 0.9 },
		{"id" : 6, "lemma" : "이", "type" : "VCP", "position" : 29, "weight" : 0.0177525 },
		{"id" : 7, "lemma" : "ㄴ", "type" : "ETM", "position" : 29, "weight" : 0.220712 },
		{"id" : 8, "lemma" : "고산", "type" : "NNG", "position" : 33, "weight" : 0.537396 },
		{"id" : 9, "lemma" : "윤선도", "type" : "NNP", "position" : 40, "weight" : 0.9 },
		{"id" : 10, "lemma" : "의", "type" : "JKG", "position" : 49, "weight" : 0.0987295 },
		{"id" : 11, "lemma" : "<", "type" : "SS", "position" : 53, "weight" : 1 },
		{"id" : 12, "lemma" : "어부", "type" : "NNG", "position" : 54, "weight" : 0.19912 },
		{"id" : 13, "lemma" : "사시", "type" : "NNG", "position" : 60, "weight" : 0.9 },
		{"id" : 14, "lemma" : "사", "type" : "XSN", "position" : 66, "weight" : 0.00634534 },
		{"id" : 15, "lemma" : ">", "type" : "SS", "position" : 69, "weight" : 1 },
		{"id" : 16, "lemma" : "에", "type" : "JKB", "position" : 70, "weight" : 0.048593 },
		{"id" : 17, "lemma" : "는", "type" : "JX", "position" : 73, "weight" : 0.0387928 },
		{"id" : 18, "lemma" : "다양", "type" : "NNG", "position" : 77, "weight" : 0.9 },
		{"id" : 19, "lemma" : "하", "type" : "XSA", "position" : 83, "weight" : 0.0001 },
		{"id" : 20, "lemma" : "ㄴ", "type" : "ETM", "position" : 83, "weight" : 0.488779 },
		{"id" : 21, "lemma" : "여음구", "type" : "NNP", "position" : 87, "weight" : 0.6 },
		{"id" : 22, "lemma" : "가", "type" : "JKS", "position" : 96, "weight" : 0.0431193 },
		{"id" : 23, "lemma" : "등장", "type" : "NNG", "position" : 100, "weight" : 0.9 },
		{"id" : 24, "lemma" : "하", "type" : "XSV", "position" : 106, "weight" : 0.0001 },
		{"id" : 25, "lemma" : "ㄴ다", "type" : "EF", "position" : 106, "weight" : 0.0449045 },
		{"id" : 26, "lemma" : ".", "type" : "SF", "position" : 112, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿조선/NNG", "target" : "﻿조선", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "중기/NNG+의/JKG", "target" : "중기의", "word_id" : 1, "m_begin" : 2, "m_end" : 3},
		{"id" : 2, "result" : "연시조/NNG+이/VCP+ㄴ/ETM", "target" : "연시조인", "word_id" : 2, "m_begin" : 4, "m_end" : 7},
		{"id" : 3, "result" : "고산/NNG", "target" : "고산", "word_id" : 3, "m_begin" : 8, "m_end" : 8},
		{"id" : 4, "result" : "윤선도/NNG+의/JKG", "target" : "윤선도의", "word_id" : 4, "m_begin" : 9, "m_end" : 10},
		{"id" : 5, "result" : "</SS+어부사시사/NNG+>/SS+에/JKB+는/JX", "target" : "<어부사시사>에는", "word_id" : 5, "m_begin" : 11, "m_end" : 17},
		{"id" : 6, "result" : "다양하/VA+ㄴ/ETM", "target" : "다양한", "word_id" : 6, "m_begin" : 18, "m_end" : 20},
		{"id" : 7, "result" : "여음구/NNG+가/JKS", "target" : "여음구가", "word_id" : 7, "m_begin" : 21, "m_end" : 22},
		{"id" : 8, "result" : "등장하/VV+ㄴ다/EF+./SF", "target" : "등장한다.", "word_id" : 8, "m_begin" : 23, "m_end" : 26}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "조선", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "중기", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 16, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "연시조", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 20, "begin" : 4, "end" : 5},
		{"id" : 5, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 29, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 29, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "고산", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 33, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "윤선도", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 49, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "<", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 53, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "어부사시사", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 54, "begin" : 12, "end" : 14},
		{"id" : 12, "text" : ">", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 69, "begin" : 15, "end" : 15},
		{"id" : 13, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 70, "begin" : 16, "end" : 16},
		{"id" : 14, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 73, "begin" : 17, "end" : 17},
		{"id" : 15, "text" : "다양하", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 77, "begin" : 18, "end" : 19},
		{"id" : 16, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 83, "begin" : 20, "end" : 20},
		{"id" : 17, "text" : "여음구", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 87, "begin" : 21, "end" : 21},
		{"id" : 18, "text" : "가", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 96, "begin" : 22, "end" : 22},
		{"id" : 19, "text" : "등장하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 100, "begin" : 23, "end" : 24},
		{"id" : 20, "text" : "ㄴ다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 106, "begin" : 25, "end" : 25},
		{"id" : 21, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 112, "begin" : 26, "end" : 26}
	],
	"word" : [
		{"id" : 0, "text" : "﻿조선", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "중기의", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 2, "text" : "연시조인", "type" : "", "begin" : 4, "end" : 7},
		{"id" : 3, "text" : "고산", "type" : "", "begin" : 8, "end" : 8},
		{"id" : 4, "text" : "윤선도의", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 5, "text" : "<어부사시사>에는", "type" : "", "begin" : 11, "end" : 17},
		{"id" : 6, "text" : "다양한", "type" : "", "begin" : 18, "end" : 20},
		{"id" : 7, "text" : "여음구가", "type" : "", "begin" : 21, "end" : 22},
		{"id" : 8, "text" : "등장한다.", "type" : "", "begin" : 23, "end" : 26}
	],
	"NE" : [
		{"id" : 0, "text" : "조선 중기", "type" : "DT_DYNASTY", "begin" : 1, "end" : 2, "weight" : 0.674453, "common_noun" : 0},
		{"id" : 1, "text" : "연시조", "type" : "FD_ART", "begin" : 4, "end" : 5, "weight" : 0.242872, "common_noun" : 0},
		{"id" : 2, "text" : "고산", "type" : "PS_NAME", "begin" : 8, "end" : 8, "weight" : 0.290364, "common_noun" : 0},
		{"id" : 3, "text" : "윤선도", "type" : "PS_NAME", "begin" : 9, "end" : 9, "weight" : 0.298185, "common_noun" : 0},
		{"id" : 4, "text" : "어부사시사", "type" : "AF_WORKS", "begin" : 12, "end" : 14, "weight" : 0.583594, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿조선", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.684793 },
		{"id" : 1, "text" : "중기의", "head" : 2, "label" : "NP_MOD", "mod" : [0], "weight" : 0.591453 },
		{"id" : 2, "text" : "연시조인", "head" : 5, "label" : "VNP_MOD", "mod" : [1], "weight" : 0.98269 },
		{"id" : 3, "text" : "고산", "head" : 4, "label" : "NP", "mod" : [], "weight" : 0.742551 },
		{"id" : 4, "text" : "윤선도의", "head" : 5, "label" : "NP_MOD", "mod" : [3], "weight" : 0.967932 },
		{"id" : 5, "text" : "<어부사시사>에는", "head" : 8, "label" : "NP_AJT", "mod" : [2, 4], "weight" : 0.646076 },
		{"id" : 6, "text" : "다양한", "head" : 7, "label" : "VP_MOD", "mod" : [], "weight" : 0.844512 },
		{"id" : 7, "text" : "여음구가", "head" : 8, "label" : "NP_SBJ", "mod" : [6], "weight" : 0.505976 },
		{"id" : 8, "text" : "등장한다.", "head" : -1, "label" : "VP", "mod" : [5, 7], "weight" : 0.0473953 }
	],
	"SRL" : [
		{"verb" : "다양", "sense" : 1, "word_id" : 6, "weight" : 0.339363,
			"argument" : [
				{"type" : "ARG1", "word_id" : 7, "text" : "여음구가", "weight" : 0.339363 }
			] },
		{"verb" : "등장", "sense" : 1, "word_id" : 8, "weight" : 0.26929,
			"argument" : [
				{"type" : "ARG2", "word_id" : 5, "text" : "<어부사시사>에는", "weight" : 0.246572 },
				{"type" : "ARG1", "word_id" : 7, "text" : "여음구가", "weight" : 0.292008 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "이 작품에서 등장하는 여음구가 아닌 것은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "이", "type" : "MM", "position" : 114, "weight" : 0.0020222 },
		{"id" : 1, "lemma" : "작품", "type" : "NNG", "position" : 118, "weight" : 0.737373 },
		{"id" : 2, "lemma" : "에서", "type" : "JKB", "position" : 124, "weight" : 0.153407 },
		{"id" : 3, "lemma" : "등장", "type" : "NNG", "position" : 131, "weight" : 0.9 },
		{"id" : 4, "lemma" : "하", "type" : "XSV", "position" : 137, "weight" : 0.0001 },
		{"id" : 5, "lemma" : "는", "type" : "ETM", "position" : 140, "weight" : 0.238503 },
		{"id" : 6, "lemma" : "여음구", "type" : "NNP", "position" : 144, "weight" : 0.6 },
		{"id" : 7, "lemma" : "가", "type" : "JKC", "position" : 153, "weight" : 0.0001 },
		{"id" : 8, "lemma" : "아니", "type" : "VCN", "position" : 157, "weight" : 0.354175 },
		{"id" : 9, "lemma" : "ㄴ", "type" : "ETM", "position" : 160, "weight" : 0.144018 },
		{"id" : 10, "lemma" : "것", "type" : "NNB", "position" : 164, "weight" : 0.228788 },
		{"id" : 11, "lemma" : "은", "type" : "JX", "position" : 167, "weight" : 0.0688243 },
		{"id" : 12, "lemma" : "무엇", "type" : "NP", "position" : 171, "weight" : 0.9 },
		{"id" : 13, "lemma" : "이", "type" : "VCP", "position" : 177, "weight" : 0.0175768 },
		{"id" : 14, "lemma" : "ㄹ까", "type" : "EF", "position" : 177, "weight" : 0.258243 },
		{"id" : 15, "lemma" : "?", "type" : "SF", "position" : 183, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이/MM", "target" : "이", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "작품/NNG+에서/JKB", "target" : "작품에서", "word_id" : 1, "m_begin" : 1, "m_end" : 2},
		{"id" : 2, "result" : "등장하/VV+는/ETM", "target" : "등장하는", "word_id" : 2, "m_begin" : 3, "m_end" : 5},
		{"id" : 3, "result" : "여음구/NNG+가/JKC", "target" : "여음구가", "word_id" : 3, "m_begin" : 6, "m_end" : 7},
		{"id" : 4, "result" : "아니/VCN+ㄴ/ETM", "target" : "아닌", "word_id" : 4, "m_begin" : 8, "m_end" : 9},
		{"id" : 5, "result" : "것/NNB+은/JX", "target" : "것은", "word_id" : 5, "m_begin" : 10, "m_end" : 11},
		{"id" : 6, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 6, "m_begin" : 12, "m_end" : 15}
	],
	"WSD" : [
		{"id" : 0, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 114, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "작품", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 118, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 124, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "등장하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 131, "begin" : 3, "end" : 4},
		{"id" : 4, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 140, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "여음구", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 144, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "가", "type" : "JKC", "scode" : "00", "weight" : 1, "position" : 153, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "아니", "type" : "VCN", "scode" : "00", "weight" : 1, "position" : 157, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 160, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "것", "type" : "NNB", "scode" : "01", "weight" : 1, "position" : 164, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 167, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 171, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 177, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 177, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 183, "begin" : 15, "end" : 15}
	],
	"word" : [
		{"id" : 0, "text" : "이", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "작품에서", "type" : "", "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "등장하는", "type" : "", "begin" : 3, "end" : 5},
		{"id" : 3, "text" : "여음구가", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 4, "text" : "아닌", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 5, "text" : "것은", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 6, "text" : "무엇일까?", "type" : "", "begin" : 12, "end" : 15}
	],
	"NE" : [
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이", "head" : 1, "label" : "DP", "mod" : [], "weight" : 0.853743 },
		{"id" : 1, "text" : "작품에서", "head" : 2, "label" : "NP_AJT", "mod" : [0], "weight" : 0.816124 },
		{"id" : 2, "text" : "등장하는", "head" : 3, "label" : "VP_MOD", "mod" : [1], "weight" : 0.648494 },
		{"id" : 3, "text" : "여음구가", "head" : 4, "label" : "NP_CMP", "mod" : [2], "weight" : 0.992362 },
		{"id" : 4, "text" : "아닌", "head" : 5, "label" : "VP_MOD", "mod" : [3], "weight" : 0.746849 },
		{"id" : 5, "text" : "것은", "head" : 6, "label" : "NP_SBJ", "mod" : [4], "weight" : 0.63839 },
		{"id" : 6, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [5], "weight" : 0.138807 }
	],
	"SRL" : [
		{"verb" : "등장", "sense" : 1, "word_id" : 2, "weight" : 0.2337,
			"argument" : [
				{"type" : "ARG2", "word_id" : 1, "text" : "작품에서", "weight" : 0.176013 },
				{"type" : "ARG1", "word_id" : 3, "text" : "여음구가", "weight" : 0.291387 }
			] },
		{"verb" : "아니", "sense" : 1, "word_id" : 4, "weight" : 0.218045,
			"argument" : [
				{"type" : "ARG2", "word_id" : 3, "text" : "여음구가", "weight" : 0.300211 },
				{"type" : "ARG1", "word_id" : 5, "text" : "것은", "weight" : 0.135879 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "FD_ART", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 1, "sent_id" : 0, "start_eid" : 2, "end_eid" : 2, "ne_id" : 1, "text" : "연시조이ㄴ", "start_eid_short" : 2, "end_eid_short" : 2, "text_short" : "연시조이ㄴ", "weight" : 0.002 },
		{"id" : 8, "sent_id" : 1, "start_eid" : 5, "end_eid" : 6, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 6, "end_eid_short" : 6, "text_short" : "무엇이ㄹ까?", "weight" : 0.006 }
	] },
	{"id" : 1, "type" : "PS_NAME", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 3, "sent_id" : 0, "start_eid" : 3, "end_eid" : 3, "ne_id" : 2, "text" : "고산", "start_eid_short" : 3, "end_eid_short" : 3, "text_short" : "고산", "weight" : 0.003 },
		{"id" : 2, "sent_id" : 0, "start_eid" : 3, "end_eid" : 4, "ne_id" : 3, "text" : "고산 윤선도", "start_eid_short" : 3, "end_eid_short" : 4, "text_short" : "고산 윤선도", "weight" : 0.002 }
	] },
	{"id" : 2, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 6, "sent_id" : 1, "start_eid" : 0, "end_eid" : 3, "ne_id" : -1, "text" : "이 작품에서 등장하는 여음구", "start_eid_short" : 3, "end_eid_short" : 3, "text_short" : "여음구", "weight" : 0.0035 },
		{"id" : 5, "sent_id" : 0, "start_eid" : 7, "end_eid" : 7, "ne_id" : -1, "text" : "여음구", "start_eid_short" : 7, "end_eid_short" : 7, "text_short" : "여음구", "weight" : 0.002 }
	] }
 ]
}

