{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "박수동(朴水東, 1941년11월 12일 ~ )은 대한민국의 만화가이다.",
	"morp" : [
		{"id" : 0, "lemma" : "박수동", "type" : "NNP", "position" : 0, "weight" : 0.15 },
		{"id" : 1, "lemma" : "(", "type" : "SS", "position" : 9, "weight" : 1 },
		{"id" : 2, "lemma" : "朴水東", "type" : "SH", "position" : 10, "weight" : 1 },
		{"id" : 3, "lemma" : ",", "type" : "SP", "position" : 19, "weight" : 1 },
		{"id" : 4, "lemma" : "1941", "type" : "SN", "position" : 21, "weight" : 1 },
		{"id" : 5, "lemma" : "년", "type" : "NNB", "position" : 25, "weight" : 0.414343 },
		{"id" : 6, "lemma" : "11", "type" : "SN", "position" : 28, "weight" : 1 },
		{"id" : 7, "lemma" : "월", "type" : "NNB", "position" : 30, "weight" : 0.408539 },
		{"id" : 8, "lemma" : "12", "type" : "SN", "position" : 34, "weight" : 1 },
		{"id" : 9, "lemma" : "일", "type" : "NNB", "position" : 36, "weight" : 0.126777 },
		{"id" : 10, "lemma" : "~", "type" : "SO", "position" : 40, "weight" : 1 },
		{"id" : 11, "lemma" : ")", "type" : "SS", "position" : 42, "weight" : 1 },
		{"id" : 12, "lemma" : "은", "type" : "JX", "position" : 43, "weight" : 0.0128817 },
		{"id" : 13, "lemma" : "대한민국", "type" : "NNP", "position" : 47, "weight" : 0.0447775 },
		{"id" : 14, "lemma" : "의", "type" : "JKG", "position" : 59, "weight" : 0.0987295 },
		{"id" : 15, "lemma" : "만화", "type" : "NNG", "position" : 63, "weight" : 0.83848 },
		{"id" : 16, "lemma" : "가", "type" : "XSN", "position" : 69, "weight" : 0.000115417 },
		{"id" : 17, "lemma" : "이", "type" : "VCP", "position" : 72, "weight" : 0.0165001 },
		{"id" : 18, "lemma" : "다", "type" : "EF", "position" : 75, "weight" : 0.353579 },
		{"id" : 19, "lemma" : ".", "type" : "SF", "position" : 78, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "박수동/NNG+(/SS+朴水東/SH+,/SP", "target" : "박수동(朴水東,", "word_id" : 0, "m_begin" : 0, "m_end" : 3},
		{"id" : 1, "result" : "1941/SN+년/NNB+11/SN+월/NNB", "target" : "1941년11월", "word_id" : 1, "m_begin" : 4, "m_end" : 7},
		{"id" : 2, "result" : "12/SN+일/NNB", "target" : "12일", "word_id" : 2, "m_begin" : 8, "m_end" : 9},
		{"id" : 3, "result" : "~/SO", "target" : "~", "word_id" : 3, "m_begin" : 10, "m_end" : 10},
		{"id" : 4, "result" : ")/SS+은/JX", "target" : ")은", "word_id" : 4, "m_begin" : 11, "m_end" : 12},
		{"id" : 5, "result" : "대한민국/NNG+의/JKG", "target" : "대한민국의", "word_id" : 5, "m_begin" : 13, "m_end" : 14},
		{"id" : 6, "result" : "만화가/NNG+이/VCP+다/EF+./SF", "target" : "만화가이다.", "word_id" : 6, "m_begin" : 15, "m_end" : 19}
	],
	"WSD" : [
		{"id" : 0, "text" : "박수동", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "朴水東", "type" : "SH", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 19, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "1941", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 21, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 25, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "11", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 28, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "월", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 30, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "12", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 34, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "일", "type" : "NNB", "scode" : "07", "weight" : 1, "position" : 36, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "~", "type" : "SO", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 42, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 43, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "대한민국", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 47, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 59, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "만화가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 63, "begin" : 15, "end" : 16},
		{"id" : 16, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 72, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 75, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 78, "begin" : 19, "end" : 19}
	],
	"word" : [
		{"id" : 0, "text" : "박수동(朴水東,", "type" : "", "begin" : 0, "end" : 3},
		{"id" : 1, "text" : "1941년11월", "type" : "", "begin" : 4, "end" : 7},
		{"id" : 2, "text" : "12일", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 3, "text" : "~", "type" : "", "begin" : 10, "end" : 10},
		{"id" : 4, "text" : ")은", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 5, "text" : "대한민국의", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 6, "text" : "만화가이다.", "type" : "", "begin" : 15, "end" : 19}
	],
	"NE" : [
		{"id" : 0, "text" : "박수동", "type" : "PS_NAME", "begin" : 0, "end" : 0, "weight" : 0.472663, "common_noun" : 0},
		{"id" : 1, "text" : "朴水東", "type" : "PS_NAME", "begin" : 2, "end" : 2, "weight" : 0.102269, "common_noun" : 0},
		{"id" : 2, "text" : "1941년11월 12일 ~", "type" : "DT_OTHERS", "begin" : 4, "end" : 10, "weight" : 0.667372, "common_noun" : 0},
		{"id" : 3, "text" : "대한민국", "type" : "LCP_COUNTRY", "begin" : 13, "end" : 13, "weight" : 0.184214, "common_noun" : 0},
		{"id" : 4, "text" : "만화가", "type" : "CV_OCCUPATION", "begin" : 15, "end" : 16, "weight" : 0.285656, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "박수동(朴水東,", "head" : 4, "label" : "NP", "mod" : [], "weight" : 0.366619 },
		{"id" : 1, "text" : "1941년11월", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.526154 },
		{"id" : 2, "text" : "12일", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.398079 },
		{"id" : 3, "text" : "~", "head" : 4, "label" : "X", "mod" : [2], "weight" : 0.805798 },
		{"id" : 4, "text" : ")은", "head" : 6, "label" : "NP_SBJ", "mod" : [0, 3], "weight" : 0.448183 },
		{"id" : 5, "text" : "대한민국의", "head" : 6, "label" : "NP_MOD", "mod" : [], "weight" : 0.431135 },
		{"id" : 6, "text" : "만화가이다.", "head" : -1, "label" : "VNP", "mod" : [4, 5], "weight" : 0.00694087 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "1941년 일본가나가와 현에서 태어났다가 1945년 8·15 광복과 함께 고국으로 귀국하였다.",
	"morp" : [
		{"id" : 0, "lemma" : "1941", "type" : "SN", "position" : 79, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 83, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "일본", "type" : "NNP", "position" : 87, "weight" : 0.0152662 },
		{"id" : 3, "lemma" : "가나가", "type" : "NNP", "position" : 93, "weight" : 0.6 },
		{"id" : 4, "lemma" : "와", "type" : "JC", "position" : 102, "weight" : 0.0104534 },
		{"id" : 5, "lemma" : "현", "type" : "NNG", "position" : 106, "weight" : 0.211463 },
		{"id" : 6, "lemma" : "에서", "type" : "JKB", "position" : 109, "weight" : 0.153407 },
		{"id" : 7, "lemma" : "태어나", "type" : "VV", "position" : 116, "weight" : 0.9 },
		{"id" : 8, "lemma" : "았", "type" : "EP", "position" : 122, "weight" : 0.9 },
		{"id" : 9, "lemma" : "다가", "type" : "EC", "position" : 125, "weight" : 0.188685 },
		{"id" : 10, "lemma" : "1945", "type" : "SN", "position" : 132, "weight" : 1 },
		{"id" : 11, "lemma" : "년", "type" : "NNB", "position" : 136, "weight" : 0.414343 },
		{"id" : 12, "lemma" : "8", "type" : "SN", "position" : 140, "weight" : 1 },
		{"id" : 13, "lemma" : "·", "type" : "SP", "position" : 141, "weight" : 1 },
		{"id" : 14, "lemma" : "15", "type" : "SN", "position" : 143, "weight" : 1 },
		{"id" : 15, "lemma" : "광복", "type" : "NNG", "position" : 146, "weight" : 0.9 },
		{"id" : 16, "lemma" : "과", "type" : "JKB", "position" : 152, "weight" : 0.0503087 },
		{"id" : 17, "lemma" : "함께", "type" : "MAG", "position" : 156, "weight" : 0.9 },
		{"id" : 18, "lemma" : "고국", "type" : "NNG", "position" : 163, "weight" : 0.9 },
		{"id" : 19, "lemma" : "으로", "type" : "JKB", "position" : 169, "weight" : 0.153406 },
		{"id" : 20, "lemma" : "귀국", "type" : "NNG", "position" : 176, "weight" : 0.9 },
		{"id" : 21, "lemma" : "하", "type" : "XSV", "position" : 182, "weight" : 0.0001 },
		{"id" : 22, "lemma" : "었", "type" : "EP", "position" : 185, "weight" : 0.9 },
		{"id" : 23, "lemma" : "다", "type" : "EF", "position" : 188, "weight" : 0.640954 },
		{"id" : 24, "lemma" : ".", "type" : "SF", "position" : 191, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "1941/SN+년/NNB", "target" : "1941년", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "일본가나가/NNG+와/JC", "target" : "일본가나가와", "word_id" : 1, "m_begin" : 2, "m_end" : 4},
		{"id" : 2, "result" : "현/NNG+에서/JKB", "target" : "현에서", "word_id" : 2, "m_begin" : 5, "m_end" : 6},
		{"id" : 3, "result" : "태어나/VV+었/EP+다가/EC", "target" : "태어났다가", "word_id" : 3, "m_begin" : 7, "m_end" : 9},
		{"id" : 4, "result" : "1945/SN+년/NNB", "target" : "1945년", "word_id" : 4, "m_begin" : 10, "m_end" : 11},
		{"id" : 5, "result" : "8/SN+·/SP+15/SN", "target" : "8·15", "word_id" : 5, "m_begin" : 12, "m_end" : 14},
		{"id" : 6, "result" : "광복/NNG+과/JKB", "target" : "광복과", "word_id" : 6, "m_begin" : 15, "m_end" : 16},
		{"id" : 7, "result" : "함께/MAG", "target" : "함께", "word_id" : 7, "m_begin" : 17, "m_end" : 17},
		{"id" : 8, "result" : "고국/NNG+으로/JKB", "target" : "고국으로", "word_id" : 8, "m_begin" : 18, "m_end" : 19},
		{"id" : 9, "result" : "귀국하/VV+었/EP+다/EF+./SF", "target" : "귀국하였다.", "word_id" : 9, "m_begin" : 20, "m_end" : 24}
	],
	"WSD" : [
		{"id" : 0, "text" : "1941", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 79, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 83, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "일본", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 87, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "가나가", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 93, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "와", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 102, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "현", "type" : "NNG", "scode" : "08", "weight" : 1, "position" : 106, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 109, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "태어나", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 116, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "았", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 122, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "다가", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 125, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "1945", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 132, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 136, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "8", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 140, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "·", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 141, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "15", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 143, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "광복", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 146, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "과", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 152, "begin" : 16, "end" : 16},
		{"id" : 17, "text" : "함께", "type" : "MAG", "scode" : "00", "weight" : 1, "position" : 156, "begin" : 17, "end" : 17},
		{"id" : 18, "text" : "고국", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 163, "begin" : 18, "end" : 18},
		{"id" : 19, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 169, "begin" : 19, "end" : 19},
		{"id" : 20, "text" : "귀국하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 176, "begin" : 20, "end" : 21},
		{"id" : 21, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 185, "begin" : 22, "end" : 22},
		{"id" : 22, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 188, "begin" : 23, "end" : 23},
		{"id" : 23, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 191, "begin" : 24, "end" : 24}
	],
	"word" : [
		{"id" : 0, "text" : "1941년", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "일본가나가와", "type" : "", "begin" : 2, "end" : 4},
		{"id" : 2, "text" : "현에서", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 3, "text" : "태어났다가", "type" : "", "begin" : 7, "end" : 9},
		{"id" : 4, "text" : "1945년", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 5, "text" : "8·15", "type" : "", "begin" : 12, "end" : 14},
		{"id" : 6, "text" : "광복과", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 7, "text" : "함께", "type" : "", "begin" : 17, "end" : 17},
		{"id" : 8, "text" : "고국으로", "type" : "", "begin" : 18, "end" : 19},
		{"id" : 9, "text" : "귀국하였다.", "type" : "", "begin" : 20, "end" : 24}
	],
	"NE" : [
		{"id" : 0, "text" : "1941년", "type" : "DT_YEAR", "begin" : 0, "end" : 1, "weight" : 0.608085, "common_noun" : 0},
		{"id" : 1, "text" : "일본", "type" : "LCP_COUNTRY", "begin" : 2, "end" : 2, "weight" : 0.497991, "common_noun" : 0},
		{"id" : 2, "text" : "가나가와 현", "type" : "LCP_PROVINCE", "begin" : 3, "end" : 5, "weight" : 0.389938, "common_noun" : 0},
		{"id" : 3, "text" : "1945년", "type" : "DT_YEAR", "begin" : 10, "end" : 11, "weight" : 0.662434, "common_noun" : 0},
		{"id" : 4, "text" : "8·15 광복", "type" : "EV_OTHERS", "begin" : 12, "end" : 15, "weight" : 0.284678, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "1941년", "head" : 3, "label" : "NP_AJT", "mod" : [], "weight" : 0.802166 },
		{"id" : 1, "text" : "일본가나가와", "head" : 2, "label" : "NP_CNJ", "mod" : [], "weight" : 0.450619 },
		{"id" : 2, "text" : "현에서", "head" : 3, "label" : "NP_AJT", "mod" : [1], "weight" : 0.67492 },
		{"id" : 3, "text" : "태어났다가", "head" : 9, "label" : "VP", "mod" : [0, 2], "weight" : 0.588616 },
		{"id" : 4, "text" : "1945년", "head" : 9, "label" : "NP_AJT", "mod" : [], "weight" : 0.391145 },
		{"id" : 5, "text" : "8·15", "head" : 6, "label" : "NP", "mod" : [], "weight" : 0.955953 },
		{"id" : 6, "text" : "광복과", "head" : 7, "label" : "NP_AJT", "mod" : [5], "weight" : 0.83323 },
		{"id" : 7, "text" : "함께", "head" : 9, "label" : "AP", "mod" : [6], "weight" : 0.538027 },
		{"id" : 8, "text" : "고국으로", "head" : 9, "label" : "NP_AJT", "mod" : [], "weight" : 0.498251 },
		{"id" : 9, "text" : "귀국하였다.", "head" : -1, "label" : "VP", "mod" : [3, 4, 7, 8], "weight" : 0.00680764 }
	],
	"SRL" : [
		{"verb" : "태어나", "sense" : 1, "word_id" : 3, "weight" : 0.158189,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 0, "text" : "1941년", "weight" : 0.186762 },
				{"type" : "ARGM-LOC", "word_id" : 2, "text" : "현에서", "weight" : 0.129616 }
			] },
		{"verb" : "귀국", "sense" : 1, "word_id" : 9, "weight" : 0.094702,
			"argument" : [
				{"type" : "ARGM-MNR", "word_id" : 7, "text" : "함께", "weight" : 0.11993 },
				{"type" : "ARG2", "word_id" : 8, "text" : "고국으로", "weight" : 0.0694743 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 3, "ant_sid" : 0, "ant_wid" : 4, "type" : "s", "istitle" : 0, "weight" : 0.493804 },
		{"id" : 1, "verb_wid" : 9, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.431255 }
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "1965년 만화 《천연기념물》로 데뷔하였으며 선데이 서울에 만화 《고인돌》을 연재하여 고인돌 작가로도 잘 알려져 있다.",
	"morp" : [
		{"id" : 0, "lemma" : "1965", "type" : "SN", "position" : 192, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 196, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "만화", "type" : "NNG", "position" : 200, "weight" : 0.106433 },
		{"id" : 3, "lemma" : "《", "type" : "SS", "position" : 207, "weight" : 1 },
		{"id" : 4, "lemma" : "천연", "type" : "NNG", "position" : 210, "weight" : 0.199064 },
		{"id" : 5, "lemma" : "기념", "type" : "NNG", "position" : 216, "weight" : 0.9 },
		{"id" : 6, "lemma" : "물", "type" : "XSN", "position" : 222, "weight" : 0.00142289 },
		{"id" : 7, "lemma" : "》", "type" : "SS", "position" : 225, "weight" : 1 },
		{"id" : 8, "lemma" : "로", "type" : "JKB", "position" : 228, "weight" : 0.0485504 },
		{"id" : 9, "lemma" : "데뷔", "type" : "NNG", "position" : 232, "weight" : 0.9 },
		{"id" : 10, "lemma" : "하", "type" : "XSV", "position" : 238, "weight" : 0.0001 },
		{"id" : 11, "lemma" : "었", "type" : "EP", "position" : 241, "weight" : 0.9 },
		{"id" : 12, "lemma" : "으며", "type" : "EC", "position" : 244, "weight" : 0.197482 },
		{"id" : 13, "lemma" : "선데이", "type" : "NNG", "position" : 251, "weight" : 0.65 },
		{"id" : 14, "lemma" : "서울", "type" : "NNP", "position" : 261, "weight" : 0.00704221 },
		{"id" : 15, "lemma" : "에", "type" : "JKB", "position" : 267, "weight" : 0.0823628 },
		{"id" : 16, "lemma" : "만화", "type" : "NNG", "position" : 271, "weight" : 0.200177 },
		{"id" : 17, "lemma" : "《", "type" : "SS", "position" : 278, "weight" : 1 },
		{"id" : 18, "lemma" : "고인돌", "type" : "NNG", "position" : 281, "weight" : 0.9 },
		{"id" : 19, "lemma" : "》", "type" : "SS", "position" : 290, "weight" : 1 },
		{"id" : 20, "lemma" : "을", "type" : "JKO", "position" : 293, "weight" : 0.0336085 },
		{"id" : 21, "lemma" : "연재", "type" : "NNG", "position" : 297, "weight" : 0.0975025 },
		{"id" : 22, "lemma" : "하", "type" : "XSV", "position" : 303, "weight" : 0.0001 },
		{"id" : 23, "lemma" : "어", "type" : "EC", "position" : 306, "weight" : 0.361326 },
		{"id" : 24, "lemma" : "고인", "type" : "NNG", "position" : 310, "weight" : 0.9 },
		{"id" : 25, "lemma" : "돌", "type" : "NNG", "position" : 316, "weight" : 0.0851771 },
		{"id" : 26, "lemma" : "작가", "type" : "NNG", "position" : 320, "weight" : 0.184798 },
		{"id" : 27, "lemma" : "로", "type" : "JKB", "position" : 326, "weight" : 0.153229 },
		{"id" : 28, "lemma" : "도", "type" : "JX", "position" : 329, "weight" : 0.0912297 },
		{"id" : 29, "lemma" : "잘", "type" : "MAG", "position" : 333, "weight" : 0.121113 },
		{"id" : 30, "lemma" : "알려지", "type" : "VV", "position" : 337, "weight" : 0.9 },
		{"id" : 31, "lemma" : "어", "type" : "EC", "position" : 343, "weight" : 0.41831 },
		{"id" : 32, "lemma" : "있", "type" : "VX", "position" : 347, "weight" : 0.125953 },
		{"id" : 33, "lemma" : "다", "type" : "EF", "position" : 350, "weight" : 0.180366 },
		{"id" : 34, "lemma" : ".", "type" : "SF", "position" : 353, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "1965/SN+년/NNB", "target" : "1965년", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "만화/NNG", "target" : "만화", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "《/SS+천연기념물/NNG+》/SS+로/JKB", "target" : "《천연기념물》로", "word_id" : 2, "m_begin" : 3, "m_end" : 8},
		{"id" : 3, "result" : "데뷔하/VV+었/EP+으며/EC", "target" : "데뷔하였으며", "word_id" : 3, "m_begin" : 9, "m_end" : 12},
		{"id" : 4, "result" : "선데이/NNG", "target" : "선데이", "word_id" : 4, "m_begin" : 13, "m_end" : 13},
		{"id" : 5, "result" : "서울/NNG+에/JKB", "target" : "서울에", "word_id" : 5, "m_begin" : 14, "m_end" : 15},
		{"id" : 6, "result" : "만화/NNG", "target" : "만화", "word_id" : 6, "m_begin" : 16, "m_end" : 16},
		{"id" : 7, "result" : "《/SS+고인돌/NNG+》/SS+을/JKO", "target" : "《고인돌》을", "word_id" : 7, "m_begin" : 17, "m_end" : 20},
		{"id" : 8, "result" : "연재하/VV+어/EC", "target" : "연재하여", "word_id" : 8, "m_begin" : 21, "m_end" : 23},
		{"id" : 9, "result" : "고인돌/NNG", "target" : "고인돌", "word_id" : 9, "m_begin" : 24, "m_end" : 25},
		{"id" : 10, "result" : "작가/NNG+로/JKB+도/JX", "target" : "작가로도", "word_id" : 10, "m_begin" : 26, "m_end" : 28},
		{"id" : 11, "result" : "잘/MAG", "target" : "잘", "word_id" : 11, "m_begin" : 29, "m_end" : 29},
		{"id" : 12, "result" : "알려지/VV+어/EC", "target" : "알려져", "word_id" : 12, "m_begin" : 30, "m_end" : 31},
		{"id" : 13, "result" : "있/VX+다/EF+./SF", "target" : "있다.", "word_id" : 13, "m_begin" : 32, "m_end" : 34}
	],
	"WSD" : [
		{"id" : 0, "text" : "1965", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 192, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 196, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "만화", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 200, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "《", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 207, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "천연기념물", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 210, "begin" : 4, "end" : 6},
		{"id" : 5, "text" : "》", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 225, "begin" : 7, "end" : 7},
		{"id" : 6, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 228, "begin" : 8, "end" : 8},
		{"id" : 7, "text" : "데뷔하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 232, "begin" : 9, "end" : 10},
		{"id" : 8, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 241, "begin" : 11, "end" : 11},
		{"id" : 9, "text" : "으며", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 244, "begin" : 12, "end" : 12},
		{"id" : 10, "text" : "선데이", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 251, "begin" : 13, "end" : 13},
		{"id" : 11, "text" : "서울", "type" : "NNP", "scode" : "01", "weight" : 1, "position" : 261, "begin" : 14, "end" : 14},
		{"id" : 12, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 267, "begin" : 15, "end" : 15},
		{"id" : 13, "text" : "만화", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 271, "begin" : 16, "end" : 16},
		{"id" : 14, "text" : "《", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 278, "begin" : 17, "end" : 17},
		{"id" : 15, "text" : "고인돌", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 281, "begin" : 18, "end" : 18},
		{"id" : 16, "text" : "》", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 290, "begin" : 19, "end" : 19},
		{"id" : 17, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 293, "begin" : 20, "end" : 20},
		{"id" : 18, "text" : "연재하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 297, "begin" : 21, "end" : 22},
		{"id" : 19, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 306, "begin" : 23, "end" : 23},
		{"id" : 20, "text" : "고인돌", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 310, "begin" : 24, "end" : 25},
		{"id" : 21, "text" : "작가", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 320, "begin" : 26, "end" : 26},
		{"id" : 22, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 326, "begin" : 27, "end" : 27},
		{"id" : 23, "text" : "도", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 329, "begin" : 28, "end" : 28},
		{"id" : 24, "text" : "잘", "type" : "MAG", "scode" : "02", "weight" : 1, "position" : 333, "begin" : 29, "end" : 29},
		{"id" : 25, "text" : "알려지", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 337, "begin" : 30, "end" : 30},
		{"id" : 26, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 343, "begin" : 31, "end" : 31},
		{"id" : 27, "text" : "있", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 347, "begin" : 32, "end" : 32},
		{"id" : 28, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 350, "begin" : 33, "end" : 33},
		{"id" : 29, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 353, "begin" : 34, "end" : 34}
	],
	"word" : [
		{"id" : 0, "text" : "1965년", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "만화", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "《천연기념물》로", "type" : "", "begin" : 3, "end" : 8},
		{"id" : 3, "text" : "데뷔하였으며", "type" : "", "begin" : 9, "end" : 12},
		{"id" : 4, "text" : "선데이", "type" : "", "begin" : 13, "end" : 13},
		{"id" : 5, "text" : "서울에", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 6, "text" : "만화", "type" : "", "begin" : 16, "end" : 16},
		{"id" : 7, "text" : "《고인돌》을", "type" : "", "begin" : 17, "end" : 20},
		{"id" : 8, "text" : "연재하여", "type" : "", "begin" : 21, "end" : 23},
		{"id" : 9, "text" : "고인돌", "type" : "", "begin" : 24, "end" : 25},
		{"id" : 10, "text" : "작가로도", "type" : "", "begin" : 26, "end" : 28},
		{"id" : 11, "text" : "잘", "type" : "", "begin" : 29, "end" : 29},
		{"id" : 12, "text" : "알려져", "type" : "", "begin" : 30, "end" : 31},
		{"id" : 13, "text" : "있다.", "type" : "", "begin" : 32, "end" : 34}
	],
	"NE" : [
		{"id" : 0, "text" : "1965년", "type" : "DT_YEAR", "begin" : 0, "end" : 1, "weight" : 0.753603, "common_noun" : 0},
		{"id" : 1, "text" : "만화", "type" : "FD_ART", "begin" : 2, "end" : 2, "weight" : 0.547082, "common_noun" : 0},
		{"id" : 2, "text" : "천연기념물", "type" : "AF_WORKS", "begin" : 4, "end" : 6, "weight" : 0.93314, "common_noun" : 0},
		{"id" : 3, "text" : "선데이", "type" : "AF_WORKS", "begin" : 13, "end" : 13, "weight" : 0.133325, "common_noun" : 0},
		{"id" : 4, "text" : "서울", "type" : "LCP_CAPITALCITY", "begin" : 14, "end" : 14, "weight" : 0.257097, "common_noun" : 0},
		{"id" : 5, "text" : "만화", "type" : "FD_ART", "begin" : 16, "end" : 16, "weight" : 0.366654, "common_noun" : 0},
		{"id" : 6, "text" : "고인돌", "type" : "AF_WORKS", "begin" : 18, "end" : 18, "weight" : 0.816405, "common_noun" : 0},
		{"id" : 7, "text" : "작가", "type" : "CV_OCCUPATION", "begin" : 26, "end" : 26, "weight" : 0.433341, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "1965년", "head" : 3, "label" : "NP_AJT", "mod" : [], "weight" : 0.737369 },
		{"id" : 1, "text" : "만화", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.719289 },
		{"id" : 2, "text" : "《천연기념물》로", "head" : 3, "label" : "NP_AJT", "mod" : [1], "weight" : 0.735165 },
		{"id" : 3, "text" : "데뷔하였으며", "head" : 8, "label" : "VP", "mod" : [0, 2], "weight" : 0.974824 },
		{"id" : 4, "text" : "선데이", "head" : 5, "label" : "NP", "mod" : [], "weight" : 0.69572 },
		{"id" : 5, "text" : "서울에", "head" : 8, "label" : "NP_AJT", "mod" : [4], "weight" : 0.792315 },
		{"id" : 6, "text" : "만화", "head" : 7, "label" : "NP", "mod" : [], "weight" : 0.681823 },
		{"id" : 7, "text" : "《고인돌》을", "head" : 8, "label" : "NP_OBJ", "mod" : [6], "weight" : 0.707776 },
		{"id" : 8, "text" : "연재하여", "head" : 12, "label" : "VP", "mod" : [3, 5, 7], "weight" : 0.824496 },
		{"id" : 9, "text" : "고인돌", "head" : 10, "label" : "NP", "mod" : [], "weight" : 0.752752 },
		{"id" : 10, "text" : "작가로도", "head" : 12, "label" : "NP_AJT", "mod" : [9], "weight" : 0.695188 },
		{"id" : 11, "text" : "잘", "head" : 12, "label" : "AP", "mod" : [], "weight" : 0.57391 },
		{"id" : 12, "text" : "알려져", "head" : 13, "label" : "VP", "mod" : [8, 10, 11], "weight" : 0.470306 },
		{"id" : 13, "text" : "있다.", "head" : -1, "label" : "VP", "mod" : [12], "weight" : 0.00900223 }
	],
	"SRL" : [
		{"verb" : "데뷔", "sense" : 1, "word_id" : 3, "weight" : 0.162375,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 0, "text" : "1965년", "weight" : 0.23919 },
				{"type" : "ARG2", "word_id" : 2, "text" : "《천연기념물》로", "weight" : 0.0855597 }
			] },
		{"verb" : "연재", "sense" : 1, "word_id" : 8, "weight" : 0.240402,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 5, "text" : "서울에", "weight" : 0.233901 },
				{"type" : "ARG1", "word_id" : 7, "text" : "《고인돌》을", "weight" : 0.246902 }
			] },
		{"verb" : "알려지", "sense" : 1, "word_id" : 12, "weight" : 0.228607,
			"argument" : [
				{"type" : "ARG2", "word_id" : 10, "text" : "작가로도", "weight" : 0.0800129 },
				{"type" : "ARGM-ADV", "word_id" : 11, "text" : "잘", "weight" : 0.112301 },
				{"type" : "AUX", "word_id" : 13, "text" : "있다.", "weight" : 0.493508 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 3, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.635104 },
		{"id" : 1, "verb_wid" : 8, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.692652 },
		{"id" : 2, "verb_wid" : 12, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.343013 }
	]
	},
	{
	"id" : 3,
	"reserve_str" : "",
	"text" : "정감있고 익살스런 그림체가 인상적이다.",
	"morp" : [
		{"id" : 0, "lemma" : "정감", "type" : "NNG", "position" : 354, "weight" : 0.433569 },
		{"id" : 1, "lemma" : "있", "type" : "VA", "position" : 360, "weight" : 0.00300704 },
		{"id" : 2, "lemma" : "고", "type" : "EC", "position" : 363, "weight" : 0.310376 },
		{"id" : 3, "lemma" : "익살스렇", "type" : "VA", "position" : 367, "weight" : 0 },
		{"id" : 4, "lemma" : "ㄴ", "type" : "ETM", "position" : 376, "weight" : 0.430446 },
		{"id" : 5, "lemma" : "그림", "type" : "NNG", "position" : 380, "weight" : 0.656598 },
		{"id" : 6, "lemma" : "체", "type" : "XSN", "position" : 386, "weight" : 0.0209224 },
		{"id" : 7, "lemma" : "가", "type" : "JKS", "position" : 389, "weight" : 0.134553 },
		{"id" : 8, "lemma" : "인상", "type" : "NNG", "position" : 393, "weight" : 0.209147 },
		{"id" : 9, "lemma" : "적", "type" : "XSN", "position" : 399, "weight" : 0.0168756 },
		{"id" : 10, "lemma" : "이", "type" : "VCP", "position" : 402, "weight" : 0.0165001 },
		{"id" : 11, "lemma" : "다", "type" : "EF", "position" : 405, "weight" : 0.353579 },
		{"id" : 12, "lemma" : ".", "type" : "SF", "position" : 408, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "정감/NNG+있/VA+고/EC", "target" : "정감있고", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "익살스렇/VA+ㄴ/ETM", "target" : "익살스런", "word_id" : 1, "m_begin" : 3, "m_end" : 4},
		{"id" : 2, "result" : "그림체/NNG+가/JKS", "target" : "그림체가", "word_id" : 2, "m_begin" : 5, "m_end" : 7},
		{"id" : 3, "result" : "인상적/NNG+이/VCP+다/EF+./SF", "target" : "인상적이다.", "word_id" : 3, "m_begin" : 8, "m_end" : 12}
	],
	"WSD" : [
		{"id" : 0, "text" : "정감", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 354, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "있", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 360, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 363, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "익살스렇", "type" : "VA", "scode" : "00", "weight" : 0, "position" : 367, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 376, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "그림", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 380, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "체", "type" : "XSN", "scode" : "10", "weight" : 1, "position" : 386, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "가", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 389, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "인상적", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 393, "begin" : 8, "end" : 9},
		{"id" : 9, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 402, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 405, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 408, "begin" : 12, "end" : 12}
	],
	"word" : [
		{"id" : 0, "text" : "정감있고", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "익살스런", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 2, "text" : "그림체가", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 3, "text" : "인상적이다.", "type" : "", "begin" : 8, "end" : 12}
	],
	"NE" : [
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "정감있고", "head" : 1, "label" : "VP", "mod" : [], "weight" : 0.719219 },
		{"id" : 1, "text" : "익살스런", "head" : 2, "label" : "VP_MOD", "mod" : [0], "weight" : 0.927833 },
		{"id" : 2, "text" : "그림체가", "head" : 3, "label" : "NP_SBJ", "mod" : [1], "weight" : 0.540344 },
		{"id" : 3, "text" : "인상적이다.", "head" : -1, "label" : "VNP", "mod" : [2], "weight" : 0.190306 }
	],
	"SRL" : [
		{"verb" : "정감", "sense" : 1, "word_id" : 0, "weight" : 0.256669,
			"argument" : [
				{"type" : "ARG1", "word_id" : 2, "text" : "그림체가", "weight" : 0.256669 }
			] },
		{"verb" : "익살스렇", "sense" : 1, "word_id" : 1, "weight" : 0.327144,
			"argument" : [
				{"type" : "ARG1", "word_id" : 2, "text" : "그림체가", "weight" : 0.327144 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 0, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.5677 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "FD_ART", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 10, "sent_id" : 2, "start_eid" : 1, "end_eid" : 1, "ne_id" : 1, "text" : "만화", "start_eid_short" : 1, "end_eid_short" : 1, "text_short" : "만화", "weight" : 0.01 },
		{"id" : 14, "sent_id" : 2, "start_eid" : 6, "end_eid" : 6, "ne_id" : 5, "text" : "만화", "start_eid_short" : 6, "end_eid_short" : 6, "text_short" : "만화", "weight" : 0.016 }
	] },
	{"id" : 1, "type" : "AF_WORKS", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 11, "sent_id" : 2, "start_eid" : 2, "end_eid" : 2, "ne_id" : 2, "text" : "《천연기념물》", "start_eid_short" : 2, "end_eid_short" : 2, "text_short" : "《천연기념물》", "weight" : 0.002 },
		{"id" : 15, "sent_id" : 2, "start_eid" : 7, "end_eid" : 7, "ne_id" : 6, "text" : "《고인돌》", "start_eid_short" : 7, "end_eid_short" : 7, "text_short" : "《고인돌》", "weight" : 0.0035 }
	] }
 ]
}

