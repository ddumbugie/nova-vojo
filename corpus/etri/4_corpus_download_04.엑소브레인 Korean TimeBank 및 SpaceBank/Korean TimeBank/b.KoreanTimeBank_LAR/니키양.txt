{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "니키 양(Niki Yang)은 미국에 거주하는 한국인애니메이터이다.",
	"morp" : [
		{"id" : 0, "lemma" : "니키", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "양", "type" : "NNG", "position" : 7, "weight" : 0.156798 },
		{"id" : 2, "lemma" : "(", "type" : "SS", "position" : 10, "weight" : 1 },
		{"id" : 3, "lemma" : "Niki", "type" : "SL", "position" : 11, "weight" : 1 },
		{"id" : 4, "lemma" : "Yang", "type" : "SL", "position" : 16, "weight" : 1 },
		{"id" : 5, "lemma" : ")", "type" : "SS", "position" : 20, "weight" : 1 },
		{"id" : 6, "lemma" : "은", "type" : "JX", "position" : 21, "weight" : 0.0128817 },
		{"id" : 7, "lemma" : "미국", "type" : "NNP", "position" : 25, "weight" : 0.0448659 },
		{"id" : 8, "lemma" : "에", "type" : "JKB", "position" : 31, "weight" : 0.0823628 },
		{"id" : 9, "lemma" : "거주", "type" : "NNG", "position" : 35, "weight" : 0.206488 },
		{"id" : 10, "lemma" : "하", "type" : "XSV", "position" : 41, "weight" : 0.0001 },
		{"id" : 11, "lemma" : "는", "type" : "ETM", "position" : 44, "weight" : 0.238503 },
		{"id" : 12, "lemma" : "한국", "type" : "NNP", "position" : 48, "weight" : 0.0280282 },
		{"id" : 13, "lemma" : "인", "type" : "XSN", "position" : 54, "weight" : 0.00915919 },
		{"id" : 14, "lemma" : "애니메이터", "type" : "NNG", "position" : 57, "weight" : 0.4 },
		{"id" : 15, "lemma" : "이", "type" : "VCP", "position" : 72, "weight" : 0.0177525 },
		{"id" : 16, "lemma" : "다", "type" : "EF", "position" : 75, "weight" : 0.353579 },
		{"id" : 17, "lemma" : ".", "type" : "SF", "position" : 78, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "니키/NNG", "target" : "니키", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "양/NNG+(/SS+Niki/SL", "target" : "양(Niki", "word_id" : 1, "m_begin" : 1, "m_end" : 3},
		{"id" : 2, "result" : "Yang/SL+)/SS+은/JX", "target" : "Yang)은", "word_id" : 2, "m_begin" : 4, "m_end" : 6},
		{"id" : 3, "result" : "미국/NNG+에/JKB", "target" : "미국에", "word_id" : 3, "m_begin" : 7, "m_end" : 8},
		{"id" : 4, "result" : "거주하/VV+는/ETM", "target" : "거주하는", "word_id" : 4, "m_begin" : 9, "m_end" : 11},
		{"id" : 5, "result" : "한국인애니메이터/NNG+이/VCP+다/EF+./SF", "target" : "한국인애니메이터이다.", "word_id" : 5, "m_begin" : 12, "m_end" : 17}
	],
	"WSD" : [
		{"id" : 0, "text" : "니키", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "양", "type" : "NNG", "scode" : "20", "weight" : 1, "position" : 7, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "Niki", "type" : "SL", "scode" : "00", "weight" : 1, "position" : 11, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "Yang", "type" : "SL", "scode" : "00", "weight" : 1, "position" : 16, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 20, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 21, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "미국", "type" : "NNP", "scode" : "03", "weight" : 1, "position" : 25, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 31, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "거주하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 35, "begin" : 9, "end" : 10},
		{"id" : 10, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 44, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "한국인", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 48, "begin" : 12, "end" : 13},
		{"id" : 12, "text" : "애니메이터", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 57, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 72, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 75, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 78, "begin" : 17, "end" : 17}
	],
	"word" : [
		{"id" : 0, "text" : "니키", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "양(Niki", "type" : "", "begin" : 1, "end" : 3},
		{"id" : 2, "text" : "Yang)은", "type" : "", "begin" : 4, "end" : 6},
		{"id" : 3, "text" : "미국에", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 4, "text" : "거주하는", "type" : "", "begin" : 9, "end" : 11},
		{"id" : 5, "text" : "한국인애니메이터이다.", "type" : "", "begin" : 12, "end" : 17}
	],
	"NE" : [
		{"id" : 0, "text" : "니키 양", "type" : "PS_NAME", "begin" : 0, "end" : 1, "weight" : 0.237973, "common_noun" : 0},
		{"id" : 1, "text" : "미국", "type" : "LCP_COUNTRY", "begin" : 7, "end" : 7, "weight" : 0.608512, "common_noun" : 0},
		{"id" : 2, "text" : "한국인애니메이터", "type" : "CV_TRIBE", "begin" : 12, "end" : 14, "weight" : 0.476705, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "니키", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.823101 },
		{"id" : 1, "text" : "양(Niki", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.886661 },
		{"id" : 2, "text" : "Yang)은", "head" : 4, "label" : "NP_SBJ", "mod" : [1], "weight" : 0.712799 },
		{"id" : 3, "text" : "미국에", "head" : 4, "label" : "NP_AJT", "mod" : [], "weight" : 0.620768 },
		{"id" : 4, "text" : "거주하는", "head" : 5, "label" : "VP_MOD", "mod" : [2, 3], "weight" : 0.536496 },
		{"id" : 5, "text" : "한국인애니메이터이다.", "head" : -1, "label" : "VNP", "mod" : [4], "weight" : 0.0972496 }
	],
	"SRL" : [
		{"verb" : "거주", "sense" : 1, "word_id" : 4, "weight" : 0.169992,
			"argument" : [
				{"type" : "ARG1", "word_id" : 2, "text" : "Yang)은", "weight" : 0.209423 },
				{"type" : "ARG1", "word_id" : 3, "text" : "미국에", "weight" : 0.130562 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "본명은 양현정이다.",
	"morp" : [
		{"id" : 0, "lemma" : "본명", "type" : "NNG", "position" : 79, "weight" : 0.9 },
		{"id" : 1, "lemma" : "은", "type" : "JX", "position" : 85, "weight" : 0.0449928 },
		{"id" : 2, "lemma" : "양현정", "type" : "NNP", "position" : 89, "weight" : 0.2 },
		{"id" : 3, "lemma" : "이", "type" : "VCP", "position" : 98, "weight" : 0.00359898 },
		{"id" : 4, "lemma" : "다", "type" : "EF", "position" : 101, "weight" : 0.353579 },
		{"id" : 5, "lemma" : ".", "type" : "SF", "position" : 104, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "본명/NNG+은/JX", "target" : "본명은", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "양현정/NNG+이/VCP+다/EF+./SF", "target" : "양현정이다.", "word_id" : 1, "m_begin" : 2, "m_end" : 5}
	],
	"WSD" : [
		{"id" : 0, "text" : "본명", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 79, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 85, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "양현정", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 89, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 98, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 101, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 104, "begin" : 5, "end" : 5}
	],
	"word" : [
		{"id" : 0, "text" : "본명은", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "양현정이다.", "type" : "", "begin" : 2, "end" : 5}
	],
	"NE" : [
		{"id" : 0, "text" : "양현정", "type" : "PS_NAME", "begin" : 2, "end" : 2, "weight" : 0.489095, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "본명은", "head" : 1, "label" : "NP_SBJ", "mod" : [], "weight" : 0.500793 },
		{"id" : 1, "text" : "양현정이다.", "head" : -1, "label" : "VNP", "mod" : [0], "weight" : 0.293294 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "홍익대학교와 캘리포니아 예술 학교(CalArts)을 졸업한 후프레더레이터 스튜디오에서 《패밀리 가이》의 스토리보드 작가를 맡았다.",
	"morp" : [
		{"id" : 0, "lemma" : "홍익", "type" : "NNP", "position" : 105, "weight" : 0.0669745 },
		{"id" : 1, "lemma" : "대", "type" : "XPN", "position" : 111, "weight" : 0.000103476 },
		{"id" : 2, "lemma" : "학교", "type" : "NNG", "position" : 114, "weight" : 0.473729 },
		{"id" : 3, "lemma" : "와", "type" : "JC", "position" : 120, "weight" : 0.0169714 },
		{"id" : 4, "lemma" : "캘리포니아", "type" : "NNP", "position" : 124, "weight" : 0.9 },
		{"id" : 5, "lemma" : "예술", "type" : "NNG", "position" : 140, "weight" : 0.9 },
		{"id" : 6, "lemma" : "학교", "type" : "NNG", "position" : 147, "weight" : 0.184769 },
		{"id" : 7, "lemma" : "(", "type" : "SS", "position" : 153, "weight" : 1 },
		{"id" : 8, "lemma" : "CalArts", "type" : "SL", "position" : 154, "weight" : 1 },
		{"id" : 9, "lemma" : ")", "type" : "SS", "position" : 161, "weight" : 1 },
		{"id" : 10, "lemma" : "을", "type" : "JKO", "position" : 162, "weight" : 0.0336085 },
		{"id" : 11, "lemma" : "졸업", "type" : "NNG", "position" : 166, "weight" : 0.101254 },
		{"id" : 12, "lemma" : "하", "type" : "XSV", "position" : 172, "weight" : 0.0001 },
		{"id" : 13, "lemma" : "ㄴ", "type" : "ETM", "position" : 172, "weight" : 0.392321 },
		{"id" : 14, "lemma" : "후프레더레이터", "type" : "NNP", "position" : 176, "weight" : 0.6 },
		{"id" : 15, "lemma" : "스튜디오", "type" : "NNG", "position" : 198, "weight" : 0.281036 },
		{"id" : 16, "lemma" : "에서", "type" : "JKB", "position" : 210, "weight" : 0.153407 },
		{"id" : 17, "lemma" : "《", "type" : "SS", "position" : 217, "weight" : 1 },
		{"id" : 18, "lemma" : "패밀리", "type" : "NNG", "position" : 220, "weight" : 0.9 },
		{"id" : 19, "lemma" : "가이", "type" : "NNG", "position" : 230, "weight" : 0.160194 },
		{"id" : 20, "lemma" : "》", "type" : "SS", "position" : 236, "weight" : 1 },
		{"id" : 21, "lemma" : "의", "type" : "JKG", "position" : 239, "weight" : 0.0878251 },
		{"id" : 22, "lemma" : "스토리", "type" : "NNG", "position" : 243, "weight" : 0.9 },
		{"id" : 23, "lemma" : "보드", "type" : "NNG", "position" : 252, "weight" : 0.9 },
		{"id" : 24, "lemma" : "작가", "type" : "NNG", "position" : 259, "weight" : 0.184798 },
		{"id" : 25, "lemma" : "를", "type" : "JKO", "position" : 265, "weight" : 0.137686 },
		{"id" : 26, "lemma" : "맡", "type" : "VV", "position" : 269, "weight" : 0.77645 },
		{"id" : 27, "lemma" : "았", "type" : "EP", "position" : 272, "weight" : 0.9 },
		{"id" : 28, "lemma" : "다", "type" : "EF", "position" : 275, "weight" : 0.640954 },
		{"id" : 29, "lemma" : ".", "type" : "SF", "position" : 278, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "홍익대학교/NNG+와/JC", "target" : "홍익대학교와", "word_id" : 0, "m_begin" : 0, "m_end" : 3},
		{"id" : 1, "result" : "캘리포니아/NNG", "target" : "캘리포니아", "word_id" : 1, "m_begin" : 4, "m_end" : 4},
		{"id" : 2, "result" : "예술/NNG", "target" : "예술", "word_id" : 2, "m_begin" : 5, "m_end" : 5},
		{"id" : 3, "result" : "학교/NNG+(/SS+CalArts/SL+)/SS+을/JKO", "target" : "학교(CalArts)을", "word_id" : 3, "m_begin" : 6, "m_end" : 10},
		{"id" : 4, "result" : "졸업하/VV+ㄴ/ETM", "target" : "졸업한", "word_id" : 4, "m_begin" : 11, "m_end" : 13},
		{"id" : 5, "result" : "후프레더레이터/NNG", "target" : "후프레더레이터", "word_id" : 5, "m_begin" : 14, "m_end" : 14},
		{"id" : 6, "result" : "스튜디오/NNG+에서/JKB", "target" : "스튜디오에서", "word_id" : 6, "m_begin" : 15, "m_end" : 16},
		{"id" : 7, "result" : "《/SS+패밀리/NNG", "target" : "《패밀리", "word_id" : 7, "m_begin" : 17, "m_end" : 18},
		{"id" : 8, "result" : "가이/NNG+》/SS+의/JKG", "target" : "가이》의", "word_id" : 8, "m_begin" : 19, "m_end" : 21},
		{"id" : 9, "result" : "스토리보드/NNG", "target" : "스토리보드", "word_id" : 9, "m_begin" : 22, "m_end" : 23},
		{"id" : 10, "result" : "작가/NNG+를/JKO", "target" : "작가를", "word_id" : 10, "m_begin" : 24, "m_end" : 25},
		{"id" : 11, "result" : "맡/VV+었/EP+다/EF+./SF", "target" : "맡았다.", "word_id" : 11, "m_begin" : 26, "m_end" : 29}
	],
	"WSD" : [
		{"id" : 0, "text" : "홍익", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 105, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "대학교", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 111, "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "와", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 120, "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "캘리포니아", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 124, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "예술", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 140, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "학교", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 147, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 153, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "CalArts", "type" : "SL", "scode" : "00", "weight" : 1, "position" : 154, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 161, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 162, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "졸업하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 166, "begin" : 11, "end" : 12},
		{"id" : 11, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 172, "begin" : 13, "end" : 13},
		{"id" : 12, "text" : "후프레더레이터", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 176, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "스튜디오", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 198, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 210, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : "《", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 217, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : "패밀리", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 220, "begin" : 18, "end" : 18},
		{"id" : 17, "text" : "가이", "type" : "NNG", "scode" : "88", "weight" : 1, "position" : 230, "begin" : 19, "end" : 19},
		{"id" : 18, "text" : "》", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 236, "begin" : 20, "end" : 20},
		{"id" : 19, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 239, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : "스토리", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 243, "begin" : 22, "end" : 22},
		{"id" : 21, "text" : "보드", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 252, "begin" : 23, "end" : 23},
		{"id" : 22, "text" : "작가", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 259, "begin" : 24, "end" : 24},
		{"id" : 23, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 265, "begin" : 25, "end" : 25},
		{"id" : 24, "text" : "맡", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 269, "begin" : 26, "end" : 26},
		{"id" : 25, "text" : "았", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 272, "begin" : 27, "end" : 27},
		{"id" : 26, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 275, "begin" : 28, "end" : 28},
		{"id" : 27, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 278, "begin" : 29, "end" : 29}
	],
	"word" : [
		{"id" : 0, "text" : "홍익대학교와", "type" : "", "begin" : 0, "end" : 3},
		{"id" : 1, "text" : "캘리포니아", "type" : "", "begin" : 4, "end" : 4},
		{"id" : 2, "text" : "예술", "type" : "", "begin" : 5, "end" : 5},
		{"id" : 3, "text" : "학교(CalArts)을", "type" : "", "begin" : 6, "end" : 10},
		{"id" : 4, "text" : "졸업한", "type" : "", "begin" : 11, "end" : 13},
		{"id" : 5, "text" : "후프레더레이터", "type" : "", "begin" : 14, "end" : 14},
		{"id" : 6, "text" : "스튜디오에서", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 7, "text" : "《패밀리", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 8, "text" : "가이》의", "type" : "", "begin" : 19, "end" : 21},
		{"id" : 9, "text" : "스토리보드", "type" : "", "begin" : 22, "end" : 23},
		{"id" : 10, "text" : "작가를", "type" : "", "begin" : 24, "end" : 25},
		{"id" : 11, "text" : "맡았다.", "type" : "", "begin" : 26, "end" : 29}
	],
	"NE" : [
		{"id" : 0, "text" : "홍익대학교", "type" : "OGG_EDUCATION", "begin" : 0, "end" : 2, "weight" : 0.649005, "common_noun" : 0},
		{"id" : 1, "text" : "캘리포니아 예술 학교", "type" : "OGG_EDUCATION", "begin" : 4, "end" : 6, "weight" : 0.74082, "common_noun" : 0},
		{"id" : 2, "text" : "CalArts", "type" : "FD_ART", "begin" : 8, "end" : 8, "weight" : 0.126011, "common_noun" : 0},
		{"id" : 3, "text" : "후프레더레이터", "type" : "CV_OCCUPATION", "begin" : 14, "end" : 14, "weight" : 0.129551, "common_noun" : 0},
		{"id" : 4, "text" : "패밀리 가이", "type" : "AFW_VIDEO", "begin" : 18, "end" : 19, "weight" : 0.900342, "common_noun" : 0},
		{"id" : 5, "text" : "작가", "type" : "CV_OCCUPATION", "begin" : 24, "end" : 24, "weight" : 0.375049, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "홍익대학교와", "head" : 3, "label" : "NP_CNJ", "mod" : [], "weight" : 0.624313 },
		{"id" : 1, "text" : "캘리포니아", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.572416 },
		{"id" : 2, "text" : "예술", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.771823 },
		{"id" : 3, "text" : "학교(CalArts)을", "head" : 4, "label" : "NP_OBJ", "mod" : [0, 2], "weight" : 0.842483 },
		{"id" : 4, "text" : "졸업한", "head" : 6, "label" : "VP_MOD", "mod" : [3], "weight" : 0.8893 },
		{"id" : 5, "text" : "후프레더레이터", "head" : 6, "label" : "NP", "mod" : [], "weight" : 0.974207 },
		{"id" : 6, "text" : "스튜디오에서", "head" : 11, "label" : "NP_AJT", "mod" : [4, 5], "weight" : 0.881809 },
		{"id" : 7, "text" : "《패밀리", "head" : 8, "label" : "NP", "mod" : [], "weight" : 0.781955 },
		{"id" : 8, "text" : "가이》의", "head" : 10, "label" : "NP_MOD", "mod" : [7], "weight" : 0.99371 },
		{"id" : 9, "text" : "스토리보드", "head" : 10, "label" : "NP", "mod" : [], "weight" : 0.986197 },
		{"id" : 10, "text" : "작가를", "head" : 11, "label" : "NP_OBJ", "mod" : [8, 9], "weight" : 0.595093 },
		{"id" : 11, "text" : "맡았다.", "head" : -1, "label" : "VP", "mod" : [6, 10], "weight" : 0.062521 }
	],
	"SRL" : [
		{"verb" : "졸업", "sense" : 1, "word_id" : 4, "weight" : 0.19897,
			"argument" : [
				{"type" : "ARG1", "word_id" : 3, "text" : "학교(CalArts)을", "weight" : 0.149768 },
				{"type" : "ARG0", "word_id" : 6, "text" : "스튜디오에서", "weight" : 0.248172 }
			] },
		{"verb" : "맡", "sense" : 1, "word_id" : 11, "weight" : 0.195363,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 6, "text" : "스튜디오에서", "weight" : 0.0971775 },
				{"type" : "ARG1", "word_id" : 10, "text" : "작가를", "weight" : 0.293549 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 11, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.590103 }
	]
	},
	{
	"id" : 3,
	"reserve_str" : "",
	"text" : "현재 《어드벤처 타임》에서 BMO와 레이디 레이니콘(Lady Rainicorn)의 목소리를 담당하고 있다.",
	"morp" : [
		{"id" : 0, "lemma" : "현재", "type" : "MAG", "position" : 279, "weight" : 0.0393399 },
		{"id" : 1, "lemma" : "《", "type" : "SS", "position" : 286, "weight" : 1 },
		{"id" : 2, "lemma" : "어드", "type" : "NNP", "position" : 289, "weight" : 0.6 },
		{"id" : 3, "lemma" : "벤처", "type" : "NNG", "position" : 295, "weight" : 0.9 },
		{"id" : 4, "lemma" : "타임", "type" : "NNG", "position" : 302, "weight" : 0.136452 },
		{"id" : 5, "lemma" : "》", "type" : "SS", "position" : 308, "weight" : 1 },
		{"id" : 6, "lemma" : "에서", "type" : "JKB", "position" : 311, "weight" : 0.0486066 },
		{"id" : 7, "lemma" : "BMO", "type" : "SL", "position" : 318, "weight" : 1 },
		{"id" : 8, "lemma" : "와", "type" : "JC", "position" : 321, "weight" : 0.00429202 },
		{"id" : 9, "lemma" : "레이디", "type" : "NNG", "position" : 325, "weight" : 0.9 },
		{"id" : 10, "lemma" : "레이니콘", "type" : "NNP", "position" : 335, "weight" : 0.6 },
		{"id" : 11, "lemma" : "(", "type" : "SS", "position" : 347, "weight" : 1 },
		{"id" : 12, "lemma" : "Lady", "type" : "SL", "position" : 348, "weight" : 1 },
		{"id" : 13, "lemma" : "Rainicorn", "type" : "SL", "position" : 353, "weight" : 1 },
		{"id" : 14, "lemma" : ")", "type" : "SS", "position" : 362, "weight" : 1 },
		{"id" : 15, "lemma" : "의", "type" : "JKG", "position" : 363, "weight" : 0.0878251 },
		{"id" : 16, "lemma" : "목소", "type" : "NNP", "position" : 367, "weight" : 0.6 },
		{"id" : 17, "lemma" : "리", "type" : "NNG", "position" : 373, "weight" : 0.0211918 },
		{"id" : 18, "lemma" : "를", "type" : "JKO", "position" : 376, "weight" : 0.137686 },
		{"id" : 19, "lemma" : "담당", "type" : "NNG", "position" : 380, "weight" : 0.101301 },
		{"id" : 20, "lemma" : "하", "type" : "XSV", "position" : 386, "weight" : 0.0001 },
		{"id" : 21, "lemma" : "고", "type" : "EC", "position" : 389, "weight" : 0.359917 },
		{"id" : 22, "lemma" : "있", "type" : "VX", "position" : 393, "weight" : 0.125953 },
		{"id" : 23, "lemma" : "다", "type" : "EF", "position" : 396, "weight" : 0.180366 },
		{"id" : 24, "lemma" : ".", "type" : "SF", "position" : 399, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "현재/MAG", "target" : "현재", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "《/SS+어드벤처/NNG", "target" : "《어드벤처", "word_id" : 1, "m_begin" : 1, "m_end" : 3},
		{"id" : 2, "result" : "타임/NNG+》/SS+에서/JKB", "target" : "타임》에서", "word_id" : 2, "m_begin" : 4, "m_end" : 6},
		{"id" : 3, "result" : "BMO/SL+와/JC", "target" : "BMO와", "word_id" : 3, "m_begin" : 7, "m_end" : 8},
		{"id" : 4, "result" : "레이디/NNG", "target" : "레이디", "word_id" : 4, "m_begin" : 9, "m_end" : 9},
		{"id" : 5, "result" : "레이니콘/NNG+(/SS+Lady/SL", "target" : "레이니콘(Lady", "word_id" : 5, "m_begin" : 10, "m_end" : 12},
		{"id" : 6, "result" : "Rainicorn/SL+)/SS+의/JKG", "target" : "Rainicorn)의", "word_id" : 6, "m_begin" : 13, "m_end" : 15},
		{"id" : 7, "result" : "목소리/NNG+를/JKO", "target" : "목소리를", "word_id" : 7, "m_begin" : 16, "m_end" : 18},
		{"id" : 8, "result" : "담당하/VV+고/EC", "target" : "담당하고", "word_id" : 8, "m_begin" : 19, "m_end" : 21},
		{"id" : 9, "result" : "있/VX+다/EF+./SF", "target" : "있다.", "word_id" : 9, "m_begin" : 22, "m_end" : 24}
	],
	"WSD" : [
		{"id" : 0, "text" : "현재", "type" : "MAG", "scode" : "02", "weight" : 1, "position" : 279, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "《", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 286, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "어드", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 289, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "벤처", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 295, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "타임", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 302, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "》", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 308, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 311, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "BMO", "type" : "SL", "scode" : "00", "weight" : 1, "position" : 318, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "와", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 321, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "레이디", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 325, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "레이니콘", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 335, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 347, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "Lady", "type" : "SL", "scode" : "00", "weight" : 1, "position" : 348, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "Rainicorn", "type" : "SL", "scode" : "00", "weight" : 1, "position" : 353, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 362, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 363, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "목소리", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 367, "begin" : 16, "end" : 17},
		{"id" : 17, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 376, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "담당하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 380, "begin" : 19, "end" : 20},
		{"id" : 19, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 389, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : "있", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 393, "begin" : 22, "end" : 22},
		{"id" : 21, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 396, "begin" : 23, "end" : 23},
		{"id" : 22, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 399, "begin" : 24, "end" : 24}
	],
	"word" : [
		{"id" : 0, "text" : "현재", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "《어드벤처", "type" : "", "begin" : 1, "end" : 3},
		{"id" : 2, "text" : "타임》에서", "type" : "", "begin" : 4, "end" : 6},
		{"id" : 3, "text" : "BMO와", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 4, "text" : "레이디", "type" : "", "begin" : 9, "end" : 9},
		{"id" : 5, "text" : "레이니콘(Lady", "type" : "", "begin" : 10, "end" : 12},
		{"id" : 6, "text" : "Rainicorn)의", "type" : "", "begin" : 13, "end" : 15},
		{"id" : 7, "text" : "목소리를", "type" : "", "begin" : 16, "end" : 18},
		{"id" : 8, "text" : "담당하고", "type" : "", "begin" : 19, "end" : 21},
		{"id" : 9, "text" : "있다.", "type" : "", "begin" : 22, "end" : 24}
	],
	"NE" : [
		{"id" : 0, "text" : "어드벤처 타임", "type" : "AFW_VIDEO", "begin" : 2, "end" : 4, "weight" : 0.971386, "common_noun" : 0},
		{"id" : 1, "text" : "BMO", "type" : "OGG_ECONOMY", "begin" : 7, "end" : 7, "weight" : 0.192148, "common_noun" : 0},
		{"id" : 2, "text" : "레이디 레이니콘", "type" : "OGG_ECONOMY", "begin" : 9, "end" : 10, "weight" : 0.0892514, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "현재", "head" : 8, "label" : "AP", "mod" : [], "weight" : 0.683975 },
		{"id" : 1, "text" : "《어드벤처", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.710305 },
		{"id" : 2, "text" : "타임》에서", "head" : 8, "label" : "NP_AJT", "mod" : [1], "weight" : 0.702374 },
		{"id" : 3, "text" : "BMO와", "head" : 6, "label" : "NP_CNJ", "mod" : [], "weight" : 0.607529 },
		{"id" : 4, "text" : "레이디", "head" : 5, "label" : "NP", "mod" : [], "weight" : 0.580178 },
		{"id" : 5, "text" : "레이니콘(Lady", "head" : 6, "label" : "NP", "mod" : [4], "weight" : 0.934442 },
		{"id" : 6, "text" : "Rainicorn)의", "head" : 7, "label" : "NP_MOD", "mod" : [3, 5], "weight" : 0.926472 },
		{"id" : 7, "text" : "목소리를", "head" : 8, "label" : "NP_OBJ", "mod" : [6], "weight" : 0.500319 },
		{"id" : 8, "text" : "담당하고", "head" : 9, "label" : "VP", "mod" : [0, 2, 7], "weight" : 0.483275 },
		{"id" : 9, "text" : "있다.", "head" : -1, "label" : "VP", "mod" : [8], "weight" : 0.0196768 }
	],
	"SRL" : [
		{"verb" : "담당", "sense" : 1, "word_id" : 8, "weight" : 0.239357,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 0, "text" : "현재", "weight" : 0.151018 },
				{"type" : "ARGM-LOC", "word_id" : 2, "text" : "타임》에서", "weight" : 0.14764 },
				{"type" : "ARG1", "word_id" : 7, "text" : "목소리를", "weight" : 0.285986 },
				{"type" : "AUX", "word_id" : 9, "text" : "있다.", "weight" : 0.372784 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 8, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.601898 }
	]
	}
 ],
 "entity" : [
 ]
}

