{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "사진은 조선 후기의 풍속 화가인 신윤복의 '미인도'이다.",
	"morp" : [
		{"id" : 0, "lemma" : "사진", "type" : "NNG", "position" : 0, "weight" : 0.437722 },
		{"id" : 1, "lemma" : "은", "type" : "JX", "position" : 6, "weight" : 0.0449928 },
		{"id" : 2, "lemma" : "조선", "type" : "NNP", "position" : 10, "weight" : 0.0441558 },
		{"id" : 3, "lemma" : "후기", "type" : "NNG", "position" : 17, "weight" : 0.9 },
		{"id" : 4, "lemma" : "의", "type" : "JKG", "position" : 23, "weight" : 0.0694213 },
		{"id" : 5, "lemma" : "풍속", "type" : "NNG", "position" : 27, "weight" : 0.9 },
		{"id" : 6, "lemma" : "화가", "type" : "NNG", "position" : 34, "weight" : 0.9 },
		{"id" : 7, "lemma" : "이", "type" : "VCP", "position" : 40, "weight" : 0.0177525 },
		{"id" : 8, "lemma" : "ㄴ", "type" : "ETM", "position" : 40, "weight" : 0.220712 },
		{"id" : 9, "lemma" : "신윤복", "type" : "NNP", "position" : 44, "weight" : 0.55 },
		{"id" : 10, "lemma" : "의", "type" : "JKG", "position" : 53, "weight" : 0.0987295 },
		{"id" : 11, "lemma" : "'", "type" : "SS", "position" : 57, "weight" : 1 },
		{"id" : 12, "lemma" : "미인", "type" : "NNG", "position" : 58, "weight" : 0.199527 },
		{"id" : 13, "lemma" : "도", "type" : "XSN", "position" : 64, "weight" : 0.0001 },
		{"id" : 14, "lemma" : "'", "type" : "SS", "position" : 67, "weight" : 1 },
		{"id" : 15, "lemma" : "이", "type" : "VCP", "position" : 68, "weight" : 0.00747344 },
		{"id" : 16, "lemma" : "다", "type" : "EF", "position" : 71, "weight" : 0.353579 },
		{"id" : 17, "lemma" : ".", "type" : "SF", "position" : 74, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "사진/NNG+은/JX", "target" : "사진은", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "조선/NNG", "target" : "조선", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "후기/NNG+의/JKG", "target" : "후기의", "word_id" : 2, "m_begin" : 3, "m_end" : 4},
		{"id" : 3, "result" : "풍속/NNG", "target" : "풍속", "word_id" : 3, "m_begin" : 5, "m_end" : 5},
		{"id" : 4, "result" : "화가/NNG+이/VCP+ㄴ/ETM", "target" : "화가인", "word_id" : 4, "m_begin" : 6, "m_end" : 8},
		{"id" : 5, "result" : "신윤복/NNG+의/JKG", "target" : "신윤복의", "word_id" : 5, "m_begin" : 9, "m_end" : 10},
		{"id" : 6, "result" : "'/SS+미인도/NNG+'/SS+이/VCP+다/EF+./SF", "target" : "'미인도'이다.", "word_id" : 6, "m_begin" : 11, "m_end" : 17}
	],
	"WSD" : [
		{"id" : 0, "text" : "사진", "type" : "NNG", "scode" : "07", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 6, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "조선", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "후기", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 17, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 23, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "풍속", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 27, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "화가", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 34, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "신윤복", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 44, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 53, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 57, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "미인도", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 58, "begin" : 12, "end" : 13},
		{"id" : 13, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 67, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 68, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 71, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 74, "begin" : 17, "end" : 17}
	],
	"word" : [
		{"id" : 0, "text" : "사진은", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "조선", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "후기의", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 3, "text" : "풍속", "type" : "", "begin" : 5, "end" : 5},
		{"id" : 4, "text" : "화가인", "type" : "", "begin" : 6, "end" : 8},
		{"id" : 5, "text" : "신윤복의", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 6, "text" : "'미인도'이다.", "type" : "", "begin" : 11, "end" : 17}
	],
	"NE" : [
		{"id" : 0, "text" : "조선 후기", "type" : "DT_DYNASTY", "begin" : 2, "end" : 3, "weight" : 0.943387, "common_noun" : 0},
		{"id" : 1, "text" : "풍속 화가", "type" : "CV_OCCUPATION", "begin" : 5, "end" : 6, "weight" : 0.302481, "common_noun" : 0},
		{"id" : 2, "text" : "신윤복", "type" : "PS_NAME", "begin" : 9, "end" : 9, "weight" : 0.498201, "common_noun" : 0},
		{"id" : 3, "text" : "미인도", "type" : "AFW_ART_CRAFT", "begin" : 12, "end" : 13, "weight" : 0.634216, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "사진은", "head" : 6, "label" : "NP_SBJ", "mod" : [], "weight" : 0.781272 },
		{"id" : 1, "text" : "조선", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.814141 },
		{"id" : 2, "text" : "후기의", "head" : 4, "label" : "NP_MOD", "mod" : [1], "weight" : 0.834942 },
		{"id" : 3, "text" : "풍속", "head" : 4, "label" : "NP", "mod" : [], "weight" : 0.698287 },
		{"id" : 4, "text" : "화가인", "head" : 5, "label" : "VNP_MOD", "mod" : [2, 3], "weight" : 0.542193 },
		{"id" : 5, "text" : "신윤복의", "head" : 6, "label" : "NP_MOD", "mod" : [4], "weight" : 0.845852 },
		{"id" : 6, "text" : "'미인도'이다.", "head" : -1, "label" : "VNP", "mod" : [0, 5], "weight" : 0.130539 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "이 그림은 우리나라의 전통적인 미인상을 보여주고 있는데 이 미인의 머리를 장식하고 있는 가발의 명칭은 무엇일까?",
	"morp" : [
		{"id" : 0, "lemma" : "이", "type" : "MM", "position" : 75, "weight" : 0.0020222 },
		{"id" : 1, "lemma" : "그림", "type" : "NNG", "position" : 79, "weight" : 0.734943 },
		{"id" : 2, "lemma" : "은", "type" : "JX", "position" : 85, "weight" : 0.0449928 },
		{"id" : 3, "lemma" : "우리", "type" : "NNG", "position" : 89, "weight" : 0.0011334 },
		{"id" : 4, "lemma" : "나라", "type" : "NNG", "position" : 95, "weight" : 0.184431 },
		{"id" : 5, "lemma" : "의", "type" : "JKG", "position" : 101, "weight" : 0.0694213 },
		{"id" : 6, "lemma" : "전통", "type" : "NNG", "position" : 105, "weight" : 0.9 },
		{"id" : 7, "lemma" : "적", "type" : "NNG", "position" : 111, "weight" : 0.0169769 },
		{"id" : 8, "lemma" : "이", "type" : "VCP", "position" : 114, "weight" : 0.0177525 },
		{"id" : 9, "lemma" : "ㄴ", "type" : "ETM", "position" : 114, "weight" : 0.220712 },
		{"id" : 10, "lemma" : "미인", "type" : "NNG", "position" : 118, "weight" : 0.65728 },
		{"id" : 11, "lemma" : "상", "type" : "NNG", "position" : 124, "weight" : 0.0834075 },
		{"id" : 12, "lemma" : "을", "type" : "JKO", "position" : 127, "weight" : 0.129611 },
		{"id" : 13, "lemma" : "보이", "type" : "VV", "position" : 131, "weight" : 0.776105 },
		{"id" : 14, "lemma" : "어", "type" : "EC", "position" : 134, "weight" : 0.41831 },
		{"id" : 15, "lemma" : "주", "type" : "VX", "position" : 137, "weight" : 0.161283 },
		{"id" : 16, "lemma" : "고", "type" : "EC", "position" : 140, "weight" : 0.25644 },
		{"id" : 17, "lemma" : "있", "type" : "VX", "position" : 144, "weight" : 0.125953 },
		{"id" : 18, "lemma" : "는데", "type" : "EC", "position" : 147, "weight" : 0.252751 },
		{"id" : 19, "lemma" : "이", "type" : "MM", "position" : 154, "weight" : 0.000947656 },
		{"id" : 20, "lemma" : "미인", "type" : "NNG", "position" : 158, "weight" : 0.735705 },
		{"id" : 21, "lemma" : "의", "type" : "JKG", "position" : 164, "weight" : 0.0694213 },
		{"id" : 22, "lemma" : "머리", "type" : "NNG", "position" : 168, "weight" : 0.871093 },
		{"id" : 23, "lemma" : "를", "type" : "JKO", "position" : 174, "weight" : 0.137686 },
		{"id" : 24, "lemma" : "장식", "type" : "NNG", "position" : 178, "weight" : 0.101189 },
		{"id" : 25, "lemma" : "하", "type" : "XSV", "position" : 184, "weight" : 0.0001 },
		{"id" : 26, "lemma" : "고", "type" : "EC", "position" : 187, "weight" : 0.359917 },
		{"id" : 27, "lemma" : "있", "type" : "VX", "position" : 191, "weight" : 0.125953 },
		{"id" : 28, "lemma" : "는", "type" : "ETM", "position" : 194, "weight" : 0.183966 },
		{"id" : 29, "lemma" : "가발", "type" : "NNG", "position" : 198, "weight" : 0.9 },
		{"id" : 30, "lemma" : "의", "type" : "JKG", "position" : 204, "weight" : 0.0694213 },
		{"id" : 31, "lemma" : "명칭", "type" : "NNG", "position" : 208, "weight" : 0.9 },
		{"id" : 32, "lemma" : "은", "type" : "JX", "position" : 214, "weight" : 0.0449928 },
		{"id" : 33, "lemma" : "무엇", "type" : "NP", "position" : 218, "weight" : 0.9 },
		{"id" : 34, "lemma" : "이", "type" : "VCP", "position" : 224, "weight" : 0.0175768 },
		{"id" : 35, "lemma" : "ㄹ까", "type" : "EF", "position" : 224, "weight" : 0.258243 },
		{"id" : 36, "lemma" : "?", "type" : "SF", "position" : 230, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이/MM", "target" : "이", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "그림/NNG+은/JX", "target" : "그림은", "word_id" : 1, "m_begin" : 1, "m_end" : 2},
		{"id" : 2, "result" : "우리나라/NNG+의/JKG", "target" : "우리나라의", "word_id" : 2, "m_begin" : 3, "m_end" : 5},
		{"id" : 3, "result" : "전통적/NNG+이/VCP+ㄴ/ETM", "target" : "전통적인", "word_id" : 3, "m_begin" : 6, "m_end" : 9},
		{"id" : 4, "result" : "미인상/NNG+을/JKO", "target" : "미인상을", "word_id" : 4, "m_begin" : 10, "m_end" : 12},
		{"id" : 5, "result" : "보이/VV+어/EC+주/VX+고/EC", "target" : "보여주고", "word_id" : 5, "m_begin" : 13, "m_end" : 16},
		{"id" : 6, "result" : "있/VX+는데/EC", "target" : "있는데", "word_id" : 6, "m_begin" : 17, "m_end" : 18},
		{"id" : 7, "result" : "이/MM", "target" : "이", "word_id" : 7, "m_begin" : 19, "m_end" : 19},
		{"id" : 8, "result" : "미인/NNG+의/JKG", "target" : "미인의", "word_id" : 8, "m_begin" : 20, "m_end" : 21},
		{"id" : 9, "result" : "머리/NNG+를/JKO", "target" : "머리를", "word_id" : 9, "m_begin" : 22, "m_end" : 23},
		{"id" : 10, "result" : "장식하/VV+고/EC", "target" : "장식하고", "word_id" : 10, "m_begin" : 24, "m_end" : 26},
		{"id" : 11, "result" : "있/VX+는/ETM", "target" : "있는", "word_id" : 11, "m_begin" : 27, "m_end" : 28},
		{"id" : 12, "result" : "가발/NNG+의/JKG", "target" : "가발의", "word_id" : 12, "m_begin" : 29, "m_end" : 30},
		{"id" : 13, "result" : "명칭/NNG+은/JX", "target" : "명칭은", "word_id" : 13, "m_begin" : 31, "m_end" : 32},
		{"id" : 14, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 14, "m_begin" : 33, "m_end" : 36}
	],
	"WSD" : [
		{"id" : 0, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 75, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "그림", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 79, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 85, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "우리나라", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 89, "begin" : 3, "end" : 4},
		{"id" : 4, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 101, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "전통적", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 105, "begin" : 6, "end" : 7},
		{"id" : 6, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 114, "begin" : 8, "end" : 8},
		{"id" : 7, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 114, "begin" : 9, "end" : 9},
		{"id" : 8, "text" : "미인", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 118, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "상", "type" : "NNG", "scode" : "25", "weight" : 1, "position" : 124, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 127, "begin" : 12, "end" : 12},
		{"id" : 11, "text" : "보이", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 131, "begin" : 13, "end" : 13},
		{"id" : 12, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 134, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "주", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 137, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 140, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : "있", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 144, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : "는데", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 147, "begin" : 18, "end" : 18},
		{"id" : 17, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 154, "begin" : 19, "end" : 19},
		{"id" : 18, "text" : "미인", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 158, "begin" : 20, "end" : 20},
		{"id" : 19, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 164, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : "머리", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 168, "begin" : 22, "end" : 22},
		{"id" : 21, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 174, "begin" : 23, "end" : 23},
		{"id" : 22, "text" : "장식하", "type" : "VV", "scode" : "03", "weight" : 1, "position" : 178, "begin" : 24, "end" : 25},
		{"id" : 23, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 187, "begin" : 26, "end" : 26},
		{"id" : 24, "text" : "있", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 191, "begin" : 27, "end" : 27},
		{"id" : 25, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 194, "begin" : 28, "end" : 28},
		{"id" : 26, "text" : "가발", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 198, "begin" : 29, "end" : 29},
		{"id" : 27, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 204, "begin" : 30, "end" : 30},
		{"id" : 28, "text" : "명칭", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 208, "begin" : 31, "end" : 31},
		{"id" : 29, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 214, "begin" : 32, "end" : 32},
		{"id" : 30, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 218, "begin" : 33, "end" : 33},
		{"id" : 31, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 224, "begin" : 34, "end" : 34},
		{"id" : 32, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 224, "begin" : 35, "end" : 35},
		{"id" : 33, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 230, "begin" : 36, "end" : 36}
	],
	"word" : [
		{"id" : 0, "text" : "이", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "그림은", "type" : "", "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "우리나라의", "type" : "", "begin" : 3, "end" : 5},
		{"id" : 3, "text" : "전통적인", "type" : "", "begin" : 6, "end" : 9},
		{"id" : 4, "text" : "미인상을", "type" : "", "begin" : 10, "end" : 12},
		{"id" : 5, "text" : "보여주고", "type" : "", "begin" : 13, "end" : 16},
		{"id" : 6, "text" : "있는데", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 7, "text" : "이", "type" : "", "begin" : 19, "end" : 19},
		{"id" : 8, "text" : "미인의", "type" : "", "begin" : 20, "end" : 21},
		{"id" : 9, "text" : "머리를", "type" : "", "begin" : 22, "end" : 23},
		{"id" : 10, "text" : "장식하고", "type" : "", "begin" : 24, "end" : 26},
		{"id" : 11, "text" : "있는", "type" : "", "begin" : 27, "end" : 28},
		{"id" : 12, "text" : "가발의", "type" : "", "begin" : 29, "end" : 30},
		{"id" : 13, "text" : "명칭은", "type" : "", "begin" : 31, "end" : 32},
		{"id" : 14, "text" : "무엇일까?", "type" : "", "begin" : 33, "end" : 36}
	],
	"NE" : [
		{"id" : 0, "text" : "머리", "type" : "AM_PART", "begin" : 22, "end" : 22, "weight" : 0.551065, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이", "head" : 1, "label" : "DP", "mod" : [], "weight" : 0.684757 },
		{"id" : 1, "text" : "그림은", "head" : 5, "label" : "NP_SBJ", "mod" : [0], "weight" : 0.479772 },
		{"id" : 2, "text" : "우리나라의", "head" : 3, "label" : "NP_MOD", "mod" : [], "weight" : 0.24365 },
		{"id" : 3, "text" : "전통적인", "head" : 4, "label" : "VNP_MOD", "mod" : [2], "weight" : 0.675626 },
		{"id" : 4, "text" : "미인상을", "head" : 5, "label" : "NP_OBJ", "mod" : [3], "weight" : 0.713583 },
		{"id" : 5, "text" : "보여주고", "head" : 6, "label" : "VP", "mod" : [1, 4], "weight" : 0.812287 },
		{"id" : 6, "text" : "있는데", "head" : 14, "label" : "VP", "mod" : [5], "weight" : 0.832068 },
		{"id" : 7, "text" : "이", "head" : 8, "label" : "DP", "mod" : [], "weight" : 0.442177 },
		{"id" : 8, "text" : "미인의", "head" : 9, "label" : "NP_MOD", "mod" : [7], "weight" : 0.693746 },
		{"id" : 9, "text" : "머리를", "head" : 10, "label" : "NP_OBJ", "mod" : [8], "weight" : 0.654255 },
		{"id" : 10, "text" : "장식하고", "head" : 11, "label" : "VP", "mod" : [9], "weight" : 0.771382 },
		{"id" : 11, "text" : "있는", "head" : 12, "label" : "VP_MOD", "mod" : [10], "weight" : 0.552757 },
		{"id" : 12, "text" : "가발의", "head" : 13, "label" : "NP_MOD", "mod" : [11], "weight" : 0.744603 },
		{"id" : 13, "text" : "명칭은", "head" : 14, "label" : "NP_SBJ", "mod" : [12], "weight" : 0.843625 },
		{"id" : 14, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [6, 13], "weight" : 0.00120858 }
	],
	"SRL" : [
		{"verb" : "보이", "sense" : 1, "word_id" : 5, "weight" : 0.511622,
			"argument" : [
				{"type" : "ARG1", "word_id" : 1, "text" : "그림은", "weight" : 0.207346 },
				{"type" : "ARG1", "word_id" : 4, "text" : "미인상을", "weight" : 0.509094 },
				{"type" : "AUX", "word_id" : 6, "text" : "있는데", "weight" : 0.818425 }
			] },
		{"verb" : "장식", "sense" : 1, "word_id" : 10, "weight" : 0.674851,
			"argument" : [
				{"type" : "ARG1", "word_id" : 9, "text" : "머리를", "weight" : 0.55069 },
				{"type" : "AUX", "word_id" : 11, "text" : "있는", "weight" : 0.799013 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 1, "sent_id" : 0, "start_eid" : 1, "end_eid" : 6, "ne_id" : -1, "text" : "조선 후기의 풍속 화가인 신윤복의 '미인도'이다.", "start_eid_short" : 5, "end_eid_short" : 6, "text_short" : "신윤복의 '미인도'이다.", "weight" : 0 },
		{"id" : 10, "sent_id" : 1, "start_eid" : 7, "end_eid" : 8, "ne_id" : -1, "text" : "이 미인", "start_eid_short" : 7, "end_eid_short" : 8, "text_short" : "이 미인", "weight" : 0.006 }
	] },
	{"id" : 1, "type" : "PS_NAME", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 4, "sent_id" : 0, "start_eid" : 3, "end_eid" : 4, "ne_id" : 1, "text" : "풍속 화가이ㄴ", "start_eid_short" : 3, "end_eid_short" : 4, "text_short" : "풍속 화가이ㄴ", "weight" : 0.015 },
		{"id" : 2, "sent_id" : 0, "start_eid" : 1, "end_eid" : 5, "ne_id" : 2, "text" : "조선 후기의 풍속 화가인 신윤복", "start_eid_short" : 5, "end_eid_short" : 5, "text_short" : "신윤복", "weight" : 0.01 }
	] },
	{"id" : 2, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 5, "sent_id" : 1, "start_eid" : 0, "end_eid" : 1, "ne_id" : -1, "text" : "이 그림", "start_eid_short" : 0, "end_eid_short" : 1, "text_short" : "이 그림", "weight" : 0 },
		{"id" : 11, "sent_id" : 1, "start_eid" : 12, "end_eid" : 13, "ne_id" : -1, "text" : "가발의 명칭", "start_eid_short" : 12, "end_eid_short" : 13, "text_short" : "가발의 명칭", "weight" : 0.008 },
		{"id" : 12, "sent_id" : 1, "start_eid" : 14, "end_eid" : 14, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 14, "end_eid_short" : 14, "text_short" : "무엇이ㄹ까?", "weight" : 0.012 }
	] }
 ]
}
