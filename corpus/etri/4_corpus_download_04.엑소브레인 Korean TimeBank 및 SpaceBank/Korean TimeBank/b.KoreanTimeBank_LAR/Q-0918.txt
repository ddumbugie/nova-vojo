{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "우리나라 최초의 근대 신문인 '한성순보'는 며칠에 한 번씩 발행되었던 신문일까?",
	"morp" : [
		{"id" : 0, "lemma" : "우리", "type" : "NNG", "position" : 0, "weight" : 0.00152766 },
		{"id" : 1, "lemma" : "나라", "type" : "NNG", "position" : 6, "weight" : 0.184431 },
		{"id" : 2, "lemma" : "최초", "type" : "NNG", "position" : 13, "weight" : 0.9 },
		{"id" : 3, "lemma" : "의", "type" : "JKG", "position" : 19, "weight" : 0.0694213 },
		{"id" : 4, "lemma" : "근대", "type" : "NNG", "position" : 23, "weight" : 0.870753 },
		{"id" : 5, "lemma" : "신문", "type" : "NNG", "position" : 30, "weight" : 0.9 },
		{"id" : 6, "lemma" : "이", "type" : "VCP", "position" : 36, "weight" : 0.0177525 },
		{"id" : 7, "lemma" : "ㄴ", "type" : "ETM", "position" : 36, "weight" : 0.220712 },
		{"id" : 8, "lemma" : "'", "type" : "SS", "position" : 40, "weight" : 1 },
		{"id" : 9, "lemma" : "한성순보", "type" : "NNG", "position" : 41, "weight" : 0.25 },
		{"id" : 10, "lemma" : "'", "type" : "SS", "position" : 53, "weight" : 1 },
		{"id" : 11, "lemma" : "는", "type" : "JX", "position" : 54, "weight" : 0.00823314 },
		{"id" : 12, "lemma" : "며칠", "type" : "NNG", "position" : 58, "weight" : 0.9 },
		{"id" : 13, "lemma" : "에", "type" : "JKB", "position" : 64, "weight" : 0.153364 },
		{"id" : 14, "lemma" : "한", "type" : "MM", "position" : 68, "weight" : 0.0170035 },
		{"id" : 15, "lemma" : "번", "type" : "NNB", "position" : 72, "weight" : 0.179863 },
		{"id" : 16, "lemma" : "씩", "type" : "XSN", "position" : 75, "weight" : 0.0223618 },
		{"id" : 17, "lemma" : "발행", "type" : "NNG", "position" : 79, "weight" : 0.9 },
		{"id" : 18, "lemma" : "되", "type" : "XSV", "position" : 85, "weight" : 0.000224177 },
		{"id" : 19, "lemma" : "었", "type" : "EP", "position" : 88, "weight" : 0.9 },
		{"id" : 20, "lemma" : "던", "type" : "ETM", "position" : 91, "weight" : 0.107547 },
		{"id" : 21, "lemma" : "신문", "type" : "NNG", "position" : 95, "weight" : 0.9 },
		{"id" : 22, "lemma" : "이", "type" : "VCP", "position" : 101, "weight" : 0.0177525 },
		{"id" : 23, "lemma" : "ㄹ까", "type" : "EF", "position" : 101, "weight" : 0.258243 },
		{"id" : 24, "lemma" : "?", "type" : "SF", "position" : 107, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "우리나라/NNG", "target" : "우리나라", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "최초/NNG+의/JKG", "target" : "최초의", "word_id" : 1, "m_begin" : 2, "m_end" : 3},
		{"id" : 2, "result" : "근대/NNG", "target" : "근대", "word_id" : 2, "m_begin" : 4, "m_end" : 4},
		{"id" : 3, "result" : "신문/NNG+이/VCP+ㄴ/ETM", "target" : "신문인", "word_id" : 3, "m_begin" : 5, "m_end" : 7},
		{"id" : 4, "result" : "'/SS+한성순보/NNG+'/SS+는/JX", "target" : "'한성순보'는", "word_id" : 4, "m_begin" : 8, "m_end" : 11},
		{"id" : 5, "result" : "며칠/NNG+에/JKB", "target" : "며칠에", "word_id" : 5, "m_begin" : 12, "m_end" : 13},
		{"id" : 6, "result" : "한/MM", "target" : "한", "word_id" : 6, "m_begin" : 14, "m_end" : 14},
		{"id" : 7, "result" : "번씩/NNB", "target" : "번씩", "word_id" : 7, "m_begin" : 15, "m_end" : 16},
		{"id" : 8, "result" : "발행되/VV+었/EP+던/ETM", "target" : "발행되었던", "word_id" : 8, "m_begin" : 17, "m_end" : 20},
		{"id" : 9, "result" : "신문/NNG+이/VCP+ㄹ까/EF+?/SF", "target" : "신문일까?", "word_id" : 9, "m_begin" : 21, "m_end" : 24}
	],
	"WSD" : [
		{"id" : 0, "text" : "우리나라", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "최초", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 13, "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 19, "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "근대", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 23, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "신문", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 30, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 36, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 36, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "한성순보", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 41, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 53, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 54, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "며칠", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 58, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 64, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "한", "type" : "MM", "scode" : "01", "weight" : 1, "position" : 68, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "번", "type" : "NNB", "scode" : "04", "weight" : 1, "position" : 72, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "씩", "type" : "XSN", "scode" : "03", "weight" : 1, "position" : 75, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "발행되", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 79, "begin" : 17, "end" : 18},
		{"id" : 17, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 88, "begin" : 19, "end" : 19},
		{"id" : 18, "text" : "던", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 91, "begin" : 20, "end" : 20},
		{"id" : 19, "text" : "신문", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 95, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 101, "begin" : 22, "end" : 22},
		{"id" : 21, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 101, "begin" : 23, "end" : 23},
		{"id" : 22, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 107, "begin" : 24, "end" : 24}
	],
	"word" : [
		{"id" : 0, "text" : "우리나라", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "최초의", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 2, "text" : "근대", "type" : "", "begin" : 4, "end" : 4},
		{"id" : 3, "text" : "신문인", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 4, "text" : "'한성순보'는", "type" : "", "begin" : 8, "end" : 11},
		{"id" : 5, "text" : "며칠에", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 6, "text" : "한", "type" : "", "begin" : 14, "end" : 14},
		{"id" : 7, "text" : "번씩", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 8, "text" : "발행되었던", "type" : "", "begin" : 17, "end" : 20},
		{"id" : 9, "text" : "신문일까?", "type" : "", "begin" : 21, "end" : 24}
	],
	"NE" : [
		{"id" : 0, "text" : "근대", "type" : "DT_OTHERS", "begin" : 4, "end" : 4, "weight" : 0.520918, "common_noun" : 0},
		{"id" : 1, "text" : "한성순보", "type" : "OGG_MEDIA", "begin" : 9, "end" : 9, "weight" : 0.18226, "common_noun" : 0},
		{"id" : 2, "text" : "며칠", "type" : "DT_DURATION", "begin" : 12, "end" : 12, "weight" : 0.16226, "common_noun" : 0},
		{"id" : 3, "text" : "한 번", "type" : "QT_COUNT", "begin" : 14, "end" : 15, "weight" : 0.842027, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "우리나라", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.856078 },
		{"id" : 1, "text" : "최초의", "head" : 3, "label" : "NP_MOD", "mod" : [0], "weight" : 0.489823 },
		{"id" : 2, "text" : "근대", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.772047 },
		{"id" : 3, "text" : "신문인", "head" : 4, "label" : "VNP_MOD", "mod" : [1, 2], "weight" : 0.592579 },
		{"id" : 4, "text" : "'한성순보'는", "head" : 8, "label" : "NP_SBJ", "mod" : [3], "weight" : 0.764645 },
		{"id" : 5, "text" : "며칠에", "head" : 8, "label" : "NP_AJT", "mod" : [], "weight" : 0.743606 },
		{"id" : 6, "text" : "한", "head" : 7, "label" : "DP", "mod" : [], "weight" : 0.800228 },
		{"id" : 7, "text" : "번씩", "head" : 8, "label" : "NP_AJT", "mod" : [6], "weight" : 0.568959 },
		{"id" : 8, "text" : "발행되었던", "head" : 9, "label" : "VP_MOD", "mod" : [4, 5, 7], "weight" : 0.632726 },
		{"id" : 9, "text" : "신문일까?", "head" : -1, "label" : "VNP", "mod" : [8], "weight" : 0.0255356 }
	],
	"SRL" : [
		{"verb" : "발행", "sense" : 1, "word_id" : 8, "weight" : 0.364288,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 5, "text" : "며칠에", "weight" : 0.348274 },
				{"type" : "ARGM-EXT", "word_id" : 7, "text" : "번씩", "weight" : 0.380301 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
 ]
}
