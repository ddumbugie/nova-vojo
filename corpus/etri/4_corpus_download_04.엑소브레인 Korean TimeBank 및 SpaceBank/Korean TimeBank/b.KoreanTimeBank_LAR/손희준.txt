{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "손희준(1976년9월 15일 ~ )은 대한민국의 만화가이다.",
	"morp" : [
		{"id" : 0, "lemma" : "손희준", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "(", "type" : "SS", "position" : 9, "weight" : 1 },
		{"id" : 2, "lemma" : "1976", "type" : "SN", "position" : 10, "weight" : 1 },
		{"id" : 3, "lemma" : "년", "type" : "NNB", "position" : 14, "weight" : 0.414343 },
		{"id" : 4, "lemma" : "9", "type" : "SN", "position" : 17, "weight" : 1 },
		{"id" : 5, "lemma" : "월", "type" : "NNB", "position" : 18, "weight" : 0.408539 },
		{"id" : 6, "lemma" : "15", "type" : "SN", "position" : 22, "weight" : 1 },
		{"id" : 7, "lemma" : "일", "type" : "NNB", "position" : 24, "weight" : 0.126777 },
		{"id" : 8, "lemma" : "~", "type" : "SO", "position" : 28, "weight" : 1 },
		{"id" : 9, "lemma" : ")", "type" : "SS", "position" : 30, "weight" : 1 },
		{"id" : 10, "lemma" : "은", "type" : "JX", "position" : 31, "weight" : 0.0128817 },
		{"id" : 11, "lemma" : "대한민국", "type" : "NNP", "position" : 35, "weight" : 0.0447775 },
		{"id" : 12, "lemma" : "의", "type" : "JKG", "position" : 47, "weight" : 0.0987295 },
		{"id" : 13, "lemma" : "만화", "type" : "NNG", "position" : 51, "weight" : 0.83848 },
		{"id" : 14, "lemma" : "가", "type" : "XSN", "position" : 57, "weight" : 0.000115417 },
		{"id" : 15, "lemma" : "이", "type" : "VCP", "position" : 60, "weight" : 0.0165001 },
		{"id" : 16, "lemma" : "다", "type" : "EF", "position" : 63, "weight" : 0.353579 },
		{"id" : 17, "lemma" : ".", "type" : "SF", "position" : 66, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "손희준/NNG+(/SS+1976/SN+년/NNB+9/SN+월/NNB", "target" : "손희준(1976년9월", "word_id" : 0, "m_begin" : 0, "m_end" : 5},
		{"id" : 1, "result" : "15/SN+일/NNB", "target" : "15일", "word_id" : 1, "m_begin" : 6, "m_end" : 7},
		{"id" : 2, "result" : "~/SO", "target" : "~", "word_id" : 2, "m_begin" : 8, "m_end" : 8},
		{"id" : 3, "result" : ")/SS+은/JX", "target" : ")은", "word_id" : 3, "m_begin" : 9, "m_end" : 10},
		{"id" : 4, "result" : "대한민국/NNG+의/JKG", "target" : "대한민국의", "word_id" : 4, "m_begin" : 11, "m_end" : 12},
		{"id" : 5, "result" : "만화가/NNG+이/VCP+다/EF+./SF", "target" : "만화가이다.", "word_id" : 5, "m_begin" : 13, "m_end" : 17}
	],
	"WSD" : [
		{"id" : 0, "text" : "손희준", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "1976", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 14, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "9", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 17, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "월", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 18, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "15", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 22, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "일", "type" : "NNB", "scode" : "07", "weight" : 1, "position" : 24, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "~", "type" : "SO", "scode" : "00", "weight" : 1, "position" : 28, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 30, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 31, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "대한민국", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 35, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 47, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "만화가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 51, "begin" : 13, "end" : 14},
		{"id" : 14, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 60, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 63, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 66, "begin" : 17, "end" : 17}
	],
	"word" : [
		{"id" : 0, "text" : "손희준(1976년9월", "type" : "", "begin" : 0, "end" : 5},
		{"id" : 1, "text" : "15일", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 2, "text" : "~", "type" : "", "begin" : 8, "end" : 8},
		{"id" : 3, "text" : ")은", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 4, "text" : "대한민국의", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 5, "text" : "만화가이다.", "type" : "", "begin" : 13, "end" : 17}
	],
	"NE" : [
		{"id" : 0, "text" : "손희준", "type" : "PS_NAME", "begin" : 0, "end" : 0, "weight" : 0.454402, "common_noun" : 0},
		{"id" : 1, "text" : "1976년9월 15일 ~", "type" : "DT_OTHERS", "begin" : 2, "end" : 8, "weight" : 0.660747, "common_noun" : 0},
		{"id" : 2, "text" : "대한민국", "type" : "LCP_COUNTRY", "begin" : 11, "end" : 11, "weight" : 0.184235, "common_noun" : 0},
		{"id" : 3, "text" : "만화가", "type" : "CV_OCCUPATION", "begin" : 13, "end" : 14, "weight" : 0.285658, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "손희준(1976년9월", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.338325 },
		{"id" : 1, "text" : "15일", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.447907 },
		{"id" : 2, "text" : "~", "head" : 3, "label" : "X", "mod" : [0, 1], "weight" : 0.805369 },
		{"id" : 3, "text" : ")은", "head" : 5, "label" : "NP_SBJ", "mod" : [2], "weight" : 0.448183 },
		{"id" : 4, "text" : "대한민국의", "head" : 5, "label" : "NP_MOD", "mod" : [], "weight" : 0.431135 },
		{"id" : 5, "text" : "만화가이다.", "head" : -1, "label" : "VNP", "mod" : [3, 4], "weight" : 0.0136901 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "1992년 소년챔프 신인만화 공모전 《꼴찌본색》 으로 16세의 나이로 데뷔하였으며, 만화서클 해오름에서 회지를 내기도 하였다.",
	"morp" : [
		{"id" : 0, "lemma" : "1992", "type" : "SN", "position" : 67, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 71, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "소년", "type" : "NNG", "position" : 75, "weight" : 0.110314 },
		{"id" : 3, "lemma" : "챔프", "type" : "NNG", "position" : 81, "weight" : 0.45 },
		{"id" : 4, "lemma" : "신인", "type" : "NNG", "position" : 88, "weight" : 0.9 },
		{"id" : 5, "lemma" : "만화", "type" : "NNG", "position" : 94, "weight" : 0.177848 },
		{"id" : 6, "lemma" : "공모", "type" : "NNG", "position" : 101, "weight" : 0.9 },
		{"id" : 7, "lemma" : "전", "type" : "XSN", "position" : 107, "weight" : 0.00343717 },
		{"id" : 8, "lemma" : "《", "type" : "SS", "position" : 111, "weight" : 1 },
		{"id" : 9, "lemma" : "꼴찌", "type" : "NNG", "position" : 114, "weight" : 0.9 },
		{"id" : 10, "lemma" : "본색", "type" : "NNG", "position" : 120, "weight" : 0.9 },
		{"id" : 11, "lemma" : "》", "type" : "SS", "position" : 126, "weight" : 1 },
		{"id" : 12, "lemma" : "으로", "type" : "JKB", "position" : 130, "weight" : 0.0486063 },
		{"id" : 13, "lemma" : "16", "type" : "SN", "position" : 137, "weight" : 1 },
		{"id" : 14, "lemma" : "세", "type" : "NNB", "position" : 139, "weight" : 0.0850602 },
		{"id" : 15, "lemma" : "의", "type" : "JKG", "position" : 142, "weight" : 0.0520933 },
		{"id" : 16, "lemma" : "나이", "type" : "NNG", "position" : 146, "weight" : 0.9 },
		{"id" : 17, "lemma" : "로", "type" : "JKB", "position" : 152, "weight" : 0.153229 },
		{"id" : 18, "lemma" : "데뷔", "type" : "NNG", "position" : 156, "weight" : 0.9 },
		{"id" : 19, "lemma" : "하", "type" : "XSV", "position" : 162, "weight" : 0.0001 },
		{"id" : 20, "lemma" : "었", "type" : "EP", "position" : 165, "weight" : 0.9 },
		{"id" : 21, "lemma" : "으며", "type" : "EC", "position" : 168, "weight" : 0.197482 },
		{"id" : 22, "lemma" : ",", "type" : "SP", "position" : 174, "weight" : 1 },
		{"id" : 23, "lemma" : "만화", "type" : "NNG", "position" : 176, "weight" : 0.511492 },
		{"id" : 24, "lemma" : "서클", "type" : "NNG", "position" : 182, "weight" : 0.9 },
		{"id" : 25, "lemma" : "해오름", "type" : "NNP", "position" : 189, "weight" : 0.6 },
		{"id" : 26, "lemma" : "에서", "type" : "JKB", "position" : 198, "weight" : 0.0823859 },
		{"id" : 27, "lemma" : "회지", "type" : "NNG", "position" : 205, "weight" : 0.55 },
		{"id" : 28, "lemma" : "를", "type" : "JKO", "position" : 211, "weight" : 0.137686 },
		{"id" : 29, "lemma" : "내", "type" : "VV", "position" : 215, "weight" : 0.254295 },
		{"id" : 30, "lemma" : "기", "type" : "ETN", "position" : 218, "weight" : 0.0380514 },
		{"id" : 31, "lemma" : "도", "type" : "JX", "position" : 221, "weight" : 0.147715 },
		{"id" : 32, "lemma" : "하", "type" : "VX", "position" : 225, "weight" : 0.00978548 },
		{"id" : 33, "lemma" : "었", "type" : "EP", "position" : 228, "weight" : 0.9 },
		{"id" : 34, "lemma" : "다", "type" : "EF", "position" : 231, "weight" : 0.640954 },
		{"id" : 35, "lemma" : ".", "type" : "SF", "position" : 234, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "1992/SN+년/NNB", "target" : "1992년", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "소년챔프/NNG", "target" : "소년챔프", "word_id" : 1, "m_begin" : 2, "m_end" : 3},
		{"id" : 2, "result" : "신인만화/NNG", "target" : "신인만화", "word_id" : 2, "m_begin" : 4, "m_end" : 5},
		{"id" : 3, "result" : "공모전/NNG", "target" : "공모전", "word_id" : 3, "m_begin" : 6, "m_end" : 7},
		{"id" : 4, "result" : "《/SS+꼴찌본색/NNG+》/SS", "target" : "《꼴찌본색》", "word_id" : 4, "m_begin" : 8, "m_end" : 11},
		{"id" : 5, "result" : "으로/JKB", "target" : "으로", "word_id" : 5, "m_begin" : 12, "m_end" : 12},
		{"id" : 6, "result" : "16/SN+세/NNB+의/JKG", "target" : "16세의", "word_id" : 6, "m_begin" : 13, "m_end" : 15},
		{"id" : 7, "result" : "나이/NNG+로/JKB", "target" : "나이로", "word_id" : 7, "m_begin" : 16, "m_end" : 17},
		{"id" : 8, "result" : "데뷔하/VV+었/EP+으며/EC+,/SP", "target" : "데뷔하였으며,", "word_id" : 8, "m_begin" : 18, "m_end" : 22},
		{"id" : 9, "result" : "만화서클/NNG", "target" : "만화서클", "word_id" : 9, "m_begin" : 23, "m_end" : 24},
		{"id" : 10, "result" : "해오름/NNG+에서/JKB", "target" : "해오름에서", "word_id" : 10, "m_begin" : 25, "m_end" : 26},
		{"id" : 11, "result" : "회지/NNG+를/JKO", "target" : "회지를", "word_id" : 11, "m_begin" : 27, "m_end" : 28},
		{"id" : 12, "result" : "내/VV+기/ETN+도/JX", "target" : "내기도", "word_id" : 12, "m_begin" : 29, "m_end" : 31},
		{"id" : 13, "result" : "하/VX+었/EP+다/EF+./SF", "target" : "하였다.", "word_id" : 13, "m_begin" : 32, "m_end" : 35}
	],
	"WSD" : [
		{"id" : 0, "text" : "1992", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 67, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 71, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "소년", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 75, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "챔프", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 81, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "신인", "type" : "NNG", "scode" : "07", "weight" : 1, "position" : 88, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "만화", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 94, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "공모전", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 101, "begin" : 6, "end" : 7},
		{"id" : 7, "text" : "《", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 111, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "꼴찌", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 114, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "본색", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 120, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "》", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 126, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 130, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "16", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 137, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "세", "type" : "NNB", "scode" : "13", "weight" : 1, "position" : 139, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 142, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "나이", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 146, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 152, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "데뷔하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 156, "begin" : 18, "end" : 19},
		{"id" : 18, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 165, "begin" : 20, "end" : 20},
		{"id" : 19, "text" : "으며", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 168, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 174, "begin" : 22, "end" : 22},
		{"id" : 21, "text" : "만화", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 176, "begin" : 23, "end" : 23},
		{"id" : 22, "text" : "서클", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 182, "begin" : 24, "end" : 24},
		{"id" : 23, "text" : "해오름", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 189, "begin" : 25, "end" : 25},
		{"id" : 24, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 198, "begin" : 26, "end" : 26},
		{"id" : 25, "text" : "회지", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 205, "begin" : 27, "end" : 27},
		{"id" : 26, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 211, "begin" : 28, "end" : 28},
		{"id" : 27, "text" : "내", "type" : "VV", "scode" : "02", "weight" : 1, "position" : 215, "begin" : 29, "end" : 29},
		{"id" : 28, "text" : "기", "type" : "ETN", "scode" : "00", "weight" : 1, "position" : 218, "begin" : 30, "end" : 30},
		{"id" : 29, "text" : "도", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 221, "begin" : 31, "end" : 31},
		{"id" : 30, "text" : "하", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 225, "begin" : 32, "end" : 32},
		{"id" : 31, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 228, "begin" : 33, "end" : 33},
		{"id" : 32, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 231, "begin" : 34, "end" : 34},
		{"id" : 33, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 234, "begin" : 35, "end" : 35}
	],
	"word" : [
		{"id" : 0, "text" : "1992년", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "소년챔프", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 2, "text" : "신인만화", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 3, "text" : "공모전", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 4, "text" : "《꼴찌본색》", "type" : "", "begin" : 8, "end" : 11},
		{"id" : 5, "text" : "으로", "type" : "", "begin" : 12, "end" : 12},
		{"id" : 6, "text" : "16세의", "type" : "", "begin" : 13, "end" : 15},
		{"id" : 7, "text" : "나이로", "type" : "", "begin" : 16, "end" : 17},
		{"id" : 8, "text" : "데뷔하였으며,", "type" : "", "begin" : 18, "end" : 22},
		{"id" : 9, "text" : "만화서클", "type" : "", "begin" : 23, "end" : 24},
		{"id" : 10, "text" : "해오름에서", "type" : "", "begin" : 25, "end" : 26},
		{"id" : 11, "text" : "회지를", "type" : "", "begin" : 27, "end" : 28},
		{"id" : 12, "text" : "내기도", "type" : "", "begin" : 29, "end" : 31},
		{"id" : 13, "text" : "하였다.", "type" : "", "begin" : 32, "end" : 35}
	],
	"NE" : [
		{"id" : 0, "text" : "1992년", "type" : "DT_YEAR", "begin" : 0, "end" : 1, "weight" : 0.68017, "common_noun" : 0},
		{"id" : 1, "text" : "소년챔프 신인만화 공모전", "type" : "EV_OTHERS", "begin" : 2, "end" : 7, "weight" : 0.301034, "common_noun" : 0},
		{"id" : 2, "text" : "꼴찌본색", "type" : "AF_WORKS", "begin" : 9, "end" : 10, "weight" : 0.845141, "common_noun" : 0},
		{"id" : 3, "text" : "16세", "type" : "QT_AGE", "begin" : 13, "end" : 14, "weight" : 0.412151, "common_noun" : 0},
		{"id" : 4, "text" : "만화", "type" : "FD_ART", "begin" : 23, "end" : 23, "weight" : 0.318217, "common_noun" : 0},
		{"id" : 5, "text" : "해오름", "type" : "AF_BUILDING", "begin" : 25, "end" : 25, "weight" : 0.217964, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "1992년", "head" : 8, "label" : "NP_AJT", "mod" : [], "weight" : 0.79453 },
		{"id" : 1, "text" : "소년챔프", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.517964 },
		{"id" : 2, "text" : "신인만화", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.662322 },
		{"id" : 3, "text" : "공모전", "head" : 4, "label" : "NP", "mod" : [2], "weight" : 0.62156 },
		{"id" : 4, "text" : "《꼴찌본색》", "head" : 5, "label" : "NP", "mod" : [3], "weight" : 0.593697 },
		{"id" : 5, "text" : "으로", "head" : 8, "label" : "NP_AJT", "mod" : [4], "weight" : 0.606293 },
		{"id" : 6, "text" : "16세의", "head" : 7, "label" : "NP_MOD", "mod" : [], "weight" : 0.886399 },
		{"id" : 7, "text" : "나이로", "head" : 8, "label" : "NP_AJT", "mod" : [6], "weight" : 0.612606 },
		{"id" : 8, "text" : "데뷔하였으며,", "head" : 12, "label" : "VP", "mod" : [0, 5, 7], "weight" : 0.920223 },
		{"id" : 9, "text" : "만화서클", "head" : 10, "label" : "NP", "mod" : [], "weight" : 0.922572 },
		{"id" : 10, "text" : "해오름에서", "head" : 12, "label" : "NP_AJT", "mod" : [9], "weight" : 0.663267 },
		{"id" : 11, "text" : "회지를", "head" : 12, "label" : "NP_OBJ", "mod" : [], "weight" : 0.516023 },
		{"id" : 12, "text" : "내기도", "head" : 13, "label" : "VP", "mod" : [8, 10, 11], "weight" : 0.497041 },
		{"id" : 13, "text" : "하였다.", "head" : -1, "label" : "VP", "mod" : [12], "weight" : 0.00355419 }
	],
	"SRL" : [
		{"verb" : "데뷔", "sense" : 1, "word_id" : 8, "weight" : 0.0620603,
			"argument" : [
				{"type" : "ARGM-CAU", "word_id" : 7, "text" : "나이로", "weight" : 0.0620603 }
			] },
		{"verb" : "내", "sense" : 1, "word_id" : 12, "weight" : 0.288604,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 10, "text" : "해오름에서", "weight" : 0.21988 },
				{"type" : "ARG1", "word_id" : 11, "text" : "회지를", "weight" : 0.205833 },
				{"type" : "AUX", "word_id" : 13, "text" : "하였다.", "weight" : 0.440101 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 8, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.75105 },
		{"id" : 1, "verb_wid" : 12, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.471553 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "AF_WORKS", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 6, "sent_id" : 1, "start_eid" : 1, "end_eid" : 5, "ne_id" : -1, "text" : "소년챔프 신인만화 공모전 《꼴찌본색》", "start_eid_short" : 1, "end_eid_short" : 5, "text_short" : "소년챔프 신인만화 공모전 《꼴찌본색》 ", "weight" : 0.006 },
		{"id" : 8, "sent_id" : 1, "start_eid" : 4, "end_eid" : 4, "ne_id" : 2, "text" : "《꼴찌본색》", "start_eid_short" : 4, "end_eid_short" : 4, "text_short" : "《꼴찌본색》", "weight" : 0.01 }
	] }
 ]
}

