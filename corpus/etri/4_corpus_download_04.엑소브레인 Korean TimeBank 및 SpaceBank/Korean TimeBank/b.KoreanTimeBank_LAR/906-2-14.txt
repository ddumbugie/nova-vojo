{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "아리스토텔레스의 <시학>에서 제시한 '삼일치의 법칙'은 17~18세기 프랑스 고전주의 연극의 기본 법칙이다.",
	"morp" : [
		{"id" : 0, "lemma" : "아리스토텔레스", "type" : "NNP", "position" : 0, "weight" : 0.9 },
		{"id" : 1, "lemma" : "의", "type" : "JKG", "position" : 21, "weight" : 0.0987295 },
		{"id" : 2, "lemma" : "<", "type" : "SS", "position" : 25, "weight" : 1 },
		{"id" : 3, "lemma" : "시학", "type" : "NNG", "position" : 26, "weight" : 0.150589 },
		{"id" : 4, "lemma" : ">", "type" : "SS", "position" : 32, "weight" : 1 },
		{"id" : 5, "lemma" : "에서", "type" : "JKB", "position" : 33, "weight" : 0.0486066 },
		{"id" : 6, "lemma" : "제시", "type" : "NNG", "position" : 40, "weight" : 0.134207 },
		{"id" : 7, "lemma" : "하", "type" : "XSV", "position" : 46, "weight" : 0.0001 },
		{"id" : 8, "lemma" : "ㄴ", "type" : "ETM", "position" : 46, "weight" : 0.392321 },
		{"id" : 9, "lemma" : "'", "type" : "SS", "position" : 50, "weight" : 1 },
		{"id" : 10, "lemma" : "삼일", "type" : "NNG", "position" : 51, "weight" : 0.150001 },
		{"id" : 11, "lemma" : "치", "type" : "XSN", "position" : 57, "weight" : 0.0010799 },
		{"id" : 12, "lemma" : "의", "type" : "JKG", "position" : 60, "weight" : 0.120732 },
		{"id" : 13, "lemma" : "법칙", "type" : "NNG", "position" : 64, "weight" : 0.9 },
		{"id" : 14, "lemma" : "'", "type" : "SS", "position" : 70, "weight" : 1 },
		{"id" : 15, "lemma" : "은", "type" : "JX", "position" : 71, "weight" : 0.0128817 },
		{"id" : 16, "lemma" : "17", "type" : "SN", "position" : 75, "weight" : 1 },
		{"id" : 17, "lemma" : "~", "type" : "SO", "position" : 77, "weight" : 1 },
		{"id" : 18, "lemma" : "18", "type" : "SN", "position" : 78, "weight" : 1 },
		{"id" : 19, "lemma" : "세기", "type" : "NNG", "position" : 80, "weight" : 0.100259 },
		{"id" : 20, "lemma" : "프랑스", "type" : "NNP", "position" : 87, "weight" : 0.00710219 },
		{"id" : 21, "lemma" : "고전", "type" : "NNG", "position" : 97, "weight" : 0.281597 },
		{"id" : 22, "lemma" : "주의", "type" : "NNG", "position" : 103, "weight" : 0.9 },
		{"id" : 23, "lemma" : "연극", "type" : "NNG", "position" : 110, "weight" : 0.18477 },
		{"id" : 24, "lemma" : "의", "type" : "JKG", "position" : 116, "weight" : 0.0694213 },
		{"id" : 25, "lemma" : "기본", "type" : "NNG", "position" : 120, "weight" : 0.9 },
		{"id" : 26, "lemma" : "법칙", "type" : "NNG", "position" : 127, "weight" : 0.9 },
		{"id" : 27, "lemma" : "이", "type" : "VCP", "position" : 133, "weight" : 0.0177525 },
		{"id" : 28, "lemma" : "다", "type" : "EF", "position" : 136, "weight" : 0.353579 },
		{"id" : 29, "lemma" : ".", "type" : "SF", "position" : 139, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "아리스토텔레스/NNG+의/JKG", "target" : "아리스토텔레스의", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "</SS+시학/NNG+>/SS+에서/JKB", "target" : "<시학>에서", "word_id" : 1, "m_begin" : 2, "m_end" : 5},
		{"id" : 2, "result" : "제시하/VV+ㄴ/ETM", "target" : "제시한", "word_id" : 2, "m_begin" : 6, "m_end" : 8},
		{"id" : 3, "result" : "'/SS+삼일치/NNG+의/JKG", "target" : "'삼일치의", "word_id" : 3, "m_begin" : 9, "m_end" : 12},
		{"id" : 4, "result" : "법칙/NNG+'/SS+은/JX", "target" : "법칙'은", "word_id" : 4, "m_begin" : 13, "m_end" : 15},
		{"id" : 5, "result" : "17/SN+~/SO+18/SN+세기/NNG", "target" : "17~18세기", "word_id" : 5, "m_begin" : 16, "m_end" : 19},
		{"id" : 6, "result" : "프랑스/NNG", "target" : "프랑스", "word_id" : 6, "m_begin" : 20, "m_end" : 20},
		{"id" : 7, "result" : "고전주의/NNG", "target" : "고전주의", "word_id" : 7, "m_begin" : 21, "m_end" : 22},
		{"id" : 8, "result" : "연극/NNG+의/JKG", "target" : "연극의", "word_id" : 8, "m_begin" : 23, "m_end" : 24},
		{"id" : 9, "result" : "기본/NNG", "target" : "기본", "word_id" : 9, "m_begin" : 25, "m_end" : 25},
		{"id" : 10, "result" : "법칙/NNG+이/VCP+다/EF+./SF", "target" : "법칙이다.", "word_id" : 10, "m_begin" : 26, "m_end" : 29}
	],
	"WSD" : [
		{"id" : 0, "text" : "아리스토텔레스", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 21, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "<", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 25, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "시학", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 26, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : ">", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 32, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 33, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "제시하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 40, "begin" : 6, "end" : 7},
		{"id" : 7, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 46, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 50, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "삼일", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 51, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "치", "type" : "XSN", "scode" : "18", "weight" : 1, "position" : 57, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 60, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "법칙", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 64, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 70, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 71, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "17", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 75, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "~", "type" : "SO", "scode" : "00", "weight" : 1, "position" : 77, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "18", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 78, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "세기", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 80, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "프랑스", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 87, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "고전주의", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 97, "begin" : 21, "end" : 22},
		{"id" : 21, "text" : "연극", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 110, "begin" : 23, "end" : 23},
		{"id" : 22, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 116, "begin" : 24, "end" : 24},
		{"id" : 23, "text" : "기본", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 120, "begin" : 25, "end" : 25},
		{"id" : 24, "text" : "법칙", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 127, "begin" : 26, "end" : 26},
		{"id" : 25, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 133, "begin" : 27, "end" : 27},
		{"id" : 26, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 136, "begin" : 28, "end" : 28},
		{"id" : 27, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 139, "begin" : 29, "end" : 29}
	],
	"word" : [
		{"id" : 0, "text" : "아리스토텔레스의", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "<시학>에서", "type" : "", "begin" : 2, "end" : 5},
		{"id" : 2, "text" : "제시한", "type" : "", "begin" : 6, "end" : 8},
		{"id" : 3, "text" : "'삼일치의", "type" : "", "begin" : 9, "end" : 12},
		{"id" : 4, "text" : "법칙'은", "type" : "", "begin" : 13, "end" : 15},
		{"id" : 5, "text" : "17~18세기", "type" : "", "begin" : 16, "end" : 19},
		{"id" : 6, "text" : "프랑스", "type" : "", "begin" : 20, "end" : 20},
		{"id" : 7, "text" : "고전주의", "type" : "", "begin" : 21, "end" : 22},
		{"id" : 8, "text" : "연극의", "type" : "", "begin" : 23, "end" : 24},
		{"id" : 9, "text" : "기본", "type" : "", "begin" : 25, "end" : 25},
		{"id" : 10, "text" : "법칙이다.", "type" : "", "begin" : 26, "end" : 29}
	],
	"NE" : [
		{"id" : 0, "text" : "아리스토텔레스", "type" : "PS_NAME", "begin" : 0, "end" : 0, "weight" : 0.30907, "common_noun" : 0},
		{"id" : 1, "text" : "시학", "type" : "FD_ART", "begin" : 3, "end" : 3, "weight" : 0.217476, "common_noun" : 0},
		{"id" : 2, "text" : "삼일치의 법칙", "type" : "AF_WORKS", "begin" : 10, "end" : 13, "weight" : 0.307036, "common_noun" : 0},
		{"id" : 3, "text" : "17~18세기", "type" : "DT_DURATION", "begin" : 16, "end" : 19, "weight" : 0.839063, "common_noun" : 0},
		{"id" : 4, "text" : "프랑스", "type" : "LCP_COUNTRY", "begin" : 20, "end" : 20, "weight" : 0.239843, "common_noun" : 0},
		{"id" : 5, "text" : "고전주의", "type" : "TR_ART", "begin" : 21, "end" : 22, "weight" : 0.389491, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "아리스토텔레스의", "head" : 1, "label" : "NP_MOD", "mod" : [], "weight" : 0.883033 },
		{"id" : 1, "text" : "<시학>에서", "head" : 2, "label" : "NP_AJT", "mod" : [0], "weight" : 0.808029 },
		{"id" : 2, "text" : "제시한", "head" : 4, "label" : "VP_MOD", "mod" : [1], "weight" : 0.908052 },
		{"id" : 3, "text" : "'삼일치의", "head" : 4, "label" : "NP_MOD", "mod" : [], "weight" : 0.809367 },
		{"id" : 4, "text" : "법칙'은", "head" : 10, "label" : "NP_SBJ", "mod" : [2, 3], "weight" : 0.656641 },
		{"id" : 5, "text" : "17~18세기", "head" : 8, "label" : "NP", "mod" : [], "weight" : 0.745744 },
		{"id" : 6, "text" : "프랑스", "head" : 7, "label" : "NP", "mod" : [], "weight" : 0.745745 },
		{"id" : 7, "text" : "고전주의", "head" : 8, "label" : "NP", "mod" : [6], "weight" : 0.835735 },
		{"id" : 8, "text" : "연극의", "head" : 10, "label" : "NP_MOD", "mod" : [5, 7], "weight" : 0.510489 },
		{"id" : 9, "text" : "기본", "head" : 10, "label" : "NP", "mod" : [], "weight" : 0.404121 },
		{"id" : 10, "text" : "법칙이다.", "head" : -1, "label" : "VNP", "mod" : [4, 8, 9], "weight" : 0.0202223 }
	],
	"SRL" : [
		{"verb" : "제시", "sense" : 1, "word_id" : 2, "weight" : 0.24509,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 1, "text" : "<시학>에서", "weight" : 0.225206 },
				{"type" : "ARG1", "word_id" : 4, "text" : "법칙'은", "weight" : 0.264974 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "'삼일치의 법칙'에 해당하지 않는 것은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "'", "type" : "SS", "position" : 140, "weight" : 1 },
		{"id" : 1, "lemma" : "삼일", "type" : "NNG", "position" : 141, "weight" : 0.150001 },
		{"id" : 2, "lemma" : "치", "type" : "XSN", "position" : 147, "weight" : 0.0010799 },
		{"id" : 3, "lemma" : "의", "type" : "JKG", "position" : 150, "weight" : 0.120732 },
		{"id" : 4, "lemma" : "법칙", "type" : "NNG", "position" : 154, "weight" : 0.9 },
		{"id" : 5, "lemma" : "'", "type" : "SS", "position" : 160, "weight" : 1 },
		{"id" : 6, "lemma" : "에", "type" : "JKB", "position" : 161, "weight" : 0.048593 },
		{"id" : 7, "lemma" : "해당", "type" : "NNG", "position" : 165, "weight" : 0.9 },
		{"id" : 8, "lemma" : "하", "type" : "XSV", "position" : 171, "weight" : 0.0001 },
		{"id" : 9, "lemma" : "지", "type" : "EC", "position" : 174, "weight" : 0.260723 },
		{"id" : 10, "lemma" : "않", "type" : "VX", "position" : 178, "weight" : 0.256301 },
		{"id" : 11, "lemma" : "는", "type" : "ETM", "position" : 181, "weight" : 0.183966 },
		{"id" : 12, "lemma" : "것", "type" : "NNB", "position" : 185, "weight" : 0.228788 },
		{"id" : 13, "lemma" : "은", "type" : "JX", "position" : 188, "weight" : 0.0688243 },
		{"id" : 14, "lemma" : "무엇", "type" : "NP", "position" : 192, "weight" : 0.9 },
		{"id" : 15, "lemma" : "이", "type" : "VCP", "position" : 198, "weight" : 0.0175768 },
		{"id" : 16, "lemma" : "ㄹ까", "type" : "EF", "position" : 198, "weight" : 0.258243 },
		{"id" : 17, "lemma" : "?", "type" : "SF", "position" : 204, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "'/SS+삼일치/NNG+의/JKG", "target" : "'삼일치의", "word_id" : 0, "m_begin" : 0, "m_end" : 3},
		{"id" : 1, "result" : "법칙/NNG+'/SS+에/JKB", "target" : "법칙'에", "word_id" : 1, "m_begin" : 4, "m_end" : 6},
		{"id" : 2, "result" : "해당하/VV+지/EC", "target" : "해당하지", "word_id" : 2, "m_begin" : 7, "m_end" : 9},
		{"id" : 3, "result" : "않/VX+는/ETM", "target" : "않는", "word_id" : 3, "m_begin" : 10, "m_end" : 11},
		{"id" : 4, "result" : "것/NNB+은/JX", "target" : "것은", "word_id" : 4, "m_begin" : 12, "m_end" : 13},
		{"id" : 5, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 5, "m_begin" : 14, "m_end" : 17}
	],
	"WSD" : [
		{"id" : 0, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 140, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "삼일", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 141, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "치", "type" : "XSN", "scode" : "18", "weight" : 1, "position" : 147, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 150, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "법칙", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 154, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 160, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 161, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "해당하", "type" : "VV", "scode" : "04", "weight" : 1, "position" : 165, "begin" : 7, "end" : 8},
		{"id" : 8, "text" : "지", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 174, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "않", "type" : "VX", "scode" : "00", "weight" : 1, "position" : 178, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 181, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "것", "type" : "NNB", "scode" : "01", "weight" : 1, "position" : 185, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 188, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 192, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 198, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 198, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 204, "begin" : 17, "end" : 17}
	],
	"word" : [
		{"id" : 0, "text" : "'삼일치의", "type" : "", "begin" : 0, "end" : 3},
		{"id" : 1, "text" : "법칙'에", "type" : "", "begin" : 4, "end" : 6},
		{"id" : 2, "text" : "해당하지", "type" : "", "begin" : 7, "end" : 9},
		{"id" : 3, "text" : "않는", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 4, "text" : "것은", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 5, "text" : "무엇일까?", "type" : "", "begin" : 14, "end" : 17}
	],
	"NE" : [
		{"id" : 0, "text" : "삼일치의 법칙", "type" : "AF_WORKS", "begin" : 1, "end" : 4, "weight" : 0.288374, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "'삼일치의", "head" : 1, "label" : "NP_MOD", "mod" : [], "weight" : 0.896859 },
		{"id" : 1, "text" : "법칙'에", "head" : 2, "label" : "NP_AJT", "mod" : [0], "weight" : 0.852459 },
		{"id" : 2, "text" : "해당하지", "head" : 3, "label" : "VP", "mod" : [1], "weight" : 0.946367 },
		{"id" : 3, "text" : "않는", "head" : 4, "label" : "VP_MOD", "mod" : [2], "weight" : 0.795087 },
		{"id" : 4, "text" : "것은", "head" : 5, "label" : "NP_SBJ", "mod" : [3], "weight" : 0.659516 },
		{"id" : 5, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [4], "weight" : 0.260714 }
	],
	"SRL" : [
		{"verb" : "해당", "sense" : 1, "word_id" : 2, "weight" : 0.305817,
			"argument" : [
				{"type" : "ARG2", "word_id" : 1, "text" : "법칙'에", "weight" : 0.261774 },
				{"type" : "ARGM-NEG", "word_id" : 3, "text" : "않는", "weight" : 0.349859 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "FD_ART", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 1, "sent_id" : 0, "start_eid" : 1, "end_eid" : 1, "ne_id" : 1, "text" : "<시학>", "start_eid_short" : 1, "end_eid_short" : 1, "text_short" : "<시학>", "weight" : 0.002 },
		{"id" : 10, "sent_id" : 1, "start_eid" : 4, "end_eid" : 5, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 5, "end_eid_short" : 5, "text_short" : "무엇이ㄹ까?", "weight" : 0.006 }
	] },
	{"id" : 1, "type" : "AF_WORKS", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 9, "sent_id" : 1, "start_eid" : 0, "end_eid" : 1, "ne_id" : 0, "text" : "'삼일치의 법칙'", "start_eid_short" : 0, "end_eid_short" : 1, "text_short" : "'삼일치의 법칙'", "weight" : 0.016 },
		{"id" : 2, "sent_id" : 0, "start_eid" : 3, "end_eid" : 4, "ne_id" : 2, "text" : "'삼일치의 법칙'", "start_eid_short" : 3, "end_eid_short" : 4, "text_short" : "'삼일치의 법칙'", "weight" : 0.01 }
	] }
 ]
}

