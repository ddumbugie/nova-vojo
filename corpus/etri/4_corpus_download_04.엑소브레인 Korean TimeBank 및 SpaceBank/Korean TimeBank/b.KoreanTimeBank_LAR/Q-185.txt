{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿이것은 조선 왕조 시기에 죽은 관료의 가족들이 생계를 유지할 수 있도록 20세 미만의 자녀에게 과전 가운데 일부를 다시 지급해 세습이 가능하도록 한 토지 제도다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "SS", "position" : 0, "weight" : 1 },
		{"id" : 1, "lemma" : "이것", "type" : "NP", "position" : 3, "weight" : 0.0186277 },
		{"id" : 2, "lemma" : "은", "type" : "JX", "position" : 9, "weight" : 0.191811 },
		{"id" : 3, "lemma" : "조선", "type" : "NNP", "position" : 13, "weight" : 0.0441558 },
		{"id" : 4, "lemma" : "왕조", "type" : "NNG", "position" : 20, "weight" : 0.9 },
		{"id" : 5, "lemma" : "시기", "type" : "NNG", "position" : 27, "weight" : 0.184726 },
		{"id" : 6, "lemma" : "에", "type" : "JKB", "position" : 33, "weight" : 0.153364 },
		{"id" : 7, "lemma" : "죽", "type" : "VV", "position" : 37, "weight" : 0.408668 },
		{"id" : 8, "lemma" : "은", "type" : "ETM", "position" : 40, "weight" : 0.109632 },
		{"id" : 9, "lemma" : "관료", "type" : "NNG", "position" : 44, "weight" : 0.9 },
		{"id" : 10, "lemma" : "의", "type" : "JKG", "position" : 50, "weight" : 0.0694213 },
		{"id" : 11, "lemma" : "가족", "type" : "NNG", "position" : 54, "weight" : 0.9 },
		{"id" : 12, "lemma" : "들", "type" : "XSN", "position" : 60, "weight" : 0.0299841 },
		{"id" : 13, "lemma" : "이", "type" : "JKS", "position" : 63, "weight" : 0.0731805 },
		{"id" : 14, "lemma" : "생계", "type" : "NNG", "position" : 67, "weight" : 0.9 },
		{"id" : 15, "lemma" : "를", "type" : "JKO", "position" : 73, "weight" : 0.137686 },
		{"id" : 16, "lemma" : "유지", "type" : "NNG", "position" : 77, "weight" : 0.0866432 },
		{"id" : 17, "lemma" : "하", "type" : "XSV", "position" : 83, "weight" : 0.0001 },
		{"id" : 18, "lemma" : "ㄹ", "type" : "ETM", "position" : 83, "weight" : 0.387847 },
		{"id" : 19, "lemma" : "수", "type" : "NNB", "position" : 87, "weight" : 0.215617 },
		{"id" : 20, "lemma" : "있", "type" : "VA", "position" : 91, "weight" : 0.0518488 },
		{"id" : 21, "lemma" : "도록", "type" : "EC", "position" : 94, "weight" : 0.319723 },
		{"id" : 22, "lemma" : "20", "type" : "SN", "position" : 101, "weight" : 1 },
		{"id" : 23, "lemma" : "세", "type" : "NNB", "position" : 103, "weight" : 0.0850602 },
		{"id" : 24, "lemma" : "미만", "type" : "NNG", "position" : 107, "weight" : 0.9 },
		{"id" : 25, "lemma" : "의", "type" : "JKG", "position" : 113, "weight" : 0.0694213 },
		{"id" : 26, "lemma" : "자녀", "type" : "NNG", "position" : 117, "weight" : 0.870084 },
		{"id" : 27, "lemma" : "에게", "type" : "JKB", "position" : 123, "weight" : 0.153356 },
		{"id" : 28, "lemma" : "과전", "type" : "NNG", "position" : 130, "weight" : 0.9 },
		{"id" : 29, "lemma" : "가운데", "type" : "NNG", "position" : 137, "weight" : 0.9 },
		{"id" : 30, "lemma" : "일부", "type" : "NNG", "position" : 147, "weight" : 0.184806 },
		{"id" : 31, "lemma" : "를", "type" : "JKO", "position" : 153, "weight" : 0.137686 },
		{"id" : 32, "lemma" : "다시", "type" : "MAG", "position" : 157, "weight" : 0.0673101 },
		{"id" : 33, "lemma" : "지급", "type" : "NNG", "position" : 164, "weight" : 0.9 },
		{"id" : 34, "lemma" : "하", "type" : "XSV", "position" : 170, "weight" : 0.0001 },
		{"id" : 35, "lemma" : "어", "type" : "EC", "position" : 170, "weight" : 0.361326 },
		{"id" : 36, "lemma" : "세습", "type" : "NNG", "position" : 174, "weight" : 0.9 },
		{"id" : 37, "lemma" : "이", "type" : "JKS", "position" : 180, "weight" : 0.0360723 },
		{"id" : 38, "lemma" : "가능", "type" : "NNG", "position" : 184, "weight" : 0.208321 },
		{"id" : 39, "lemma" : "하", "type" : "XSA", "position" : 190, "weight" : 0.0001 },
		{"id" : 40, "lemma" : "도록", "type" : "EC", "position" : 193, "weight" : 0.363315 },
		{"id" : 41, "lemma" : "한", "type" : "MM", "position" : 200, "weight" : 0.0206582 },
		{"id" : 42, "lemma" : "토지", "type" : "NNG", "position" : 204, "weight" : 0.730936 },
		{"id" : 43, "lemma" : "제도", "type" : "NNG", "position" : 211, "weight" : 0.9 },
		{"id" : 44, "lemma" : "이", "type" : "VCP", "position" : 214, "weight" : 0.0177525 },
		{"id" : 45, "lemma" : "다", "type" : "EF", "position" : 217, "weight" : 0.353579 },
		{"id" : 46, "lemma" : ".", "type" : "SF", "position" : 220, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿/SS+이것/NP+은/JX", "target" : "﻿이것은", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "조선/NNG", "target" : "조선", "word_id" : 1, "m_begin" : 3, "m_end" : 3},
		{"id" : 2, "result" : "왕조/NNG", "target" : "왕조", "word_id" : 2, "m_begin" : 4, "m_end" : 4},
		{"id" : 3, "result" : "시기/NNG+에/JKB", "target" : "시기에", "word_id" : 3, "m_begin" : 5, "m_end" : 6},
		{"id" : 4, "result" : "죽/VV+은/ETM", "target" : "죽은", "word_id" : 4, "m_begin" : 7, "m_end" : 8},
		{"id" : 5, "result" : "관료/NNG+의/JKG", "target" : "관료의", "word_id" : 5, "m_begin" : 9, "m_end" : 10},
		{"id" : 6, "result" : "가족들/NNG+이/JKS", "target" : "가족들이", "word_id" : 6, "m_begin" : 11, "m_end" : 13},
		{"id" : 7, "result" : "생계/NNG+를/JKO", "target" : "생계를", "word_id" : 7, "m_begin" : 14, "m_end" : 15},
		{"id" : 8, "result" : "유지하/VV+ㄹ/ETM", "target" : "유지할", "word_id" : 8, "m_begin" : 16, "m_end" : 18},
		{"id" : 9, "result" : "수/NNB", "target" : "수", "word_id" : 9, "m_begin" : 19, "m_end" : 19},
		{"id" : 10, "result" : "있/VA+도록/EC", "target" : "있도록", "word_id" : 10, "m_begin" : 20, "m_end" : 21},
		{"id" : 11, "result" : "20/SN+세/NNB", "target" : "20세", "word_id" : 11, "m_begin" : 22, "m_end" : 23},
		{"id" : 12, "result" : "미만/NNG+의/JKG", "target" : "미만의", "word_id" : 12, "m_begin" : 24, "m_end" : 25},
		{"id" : 13, "result" : "자녀/NNG+에게/JKB", "target" : "자녀에게", "word_id" : 13, "m_begin" : 26, "m_end" : 27},
		{"id" : 14, "result" : "과전/NNG", "target" : "과전", "word_id" : 14, "m_begin" : 28, "m_end" : 28},
		{"id" : 15, "result" : "가운데/NNG", "target" : "가운데", "word_id" : 15, "m_begin" : 29, "m_end" : 29},
		{"id" : 16, "result" : "일부/NNG+를/JKO", "target" : "일부를", "word_id" : 16, "m_begin" : 30, "m_end" : 31},
		{"id" : 17, "result" : "다시/MAG", "target" : "다시", "word_id" : 17, "m_begin" : 32, "m_end" : 32},
		{"id" : 18, "result" : "지급하/VV+어/EC", "target" : "지급해", "word_id" : 18, "m_begin" : 33, "m_end" : 35},
		{"id" : 19, "result" : "세습/NNG+이/JKS", "target" : "세습이", "word_id" : 19, "m_begin" : 36, "m_end" : 37},
		{"id" : 20, "result" : "가능하/VA+도록/EC", "target" : "가능하도록", "word_id" : 20, "m_begin" : 38, "m_end" : 40},
		{"id" : 21, "result" : "한/MM", "target" : "한", "word_id" : 21, "m_begin" : 41, "m_end" : 41},
		{"id" : 22, "result" : "토지/NNG", "target" : "토지", "word_id" : 22, "m_begin" : 42, "m_end" : 42},
		{"id" : 23, "result" : "제도/NNG+이/VCP+다/EF+./SF", "target" : "제도다.", "word_id" : 23, "m_begin" : 43, "m_end" : 46}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "조선", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 13, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "왕조", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 20, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "시기", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 27, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 33, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "죽", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 37, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "은", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "관료", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 44, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 50, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "가족", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 54, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "들", "type" : "XSN", "scode" : "09", "weight" : 1, "position" : 60, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 63, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "생계", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 67, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 73, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "유지하", "type" : "VV", "scode" : "02", "weight" : 1, "position" : 77, "begin" : 16, "end" : 17},
		{"id" : 17, "text" : "ㄹ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 83, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "수", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 87, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "있", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 91, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "도록", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 94, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : "20", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 101, "begin" : 22, "end" : 22},
		{"id" : 22, "text" : "세", "type" : "NNB", "scode" : "13", "weight" : 1, "position" : 103, "begin" : 23, "end" : 23},
		{"id" : 23, "text" : "미만", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 107, "begin" : 24, "end" : 24},
		{"id" : 24, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 113, "begin" : 25, "end" : 25},
		{"id" : 25, "text" : "자녀", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 117, "begin" : 26, "end" : 26},
		{"id" : 26, "text" : "에게", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 123, "begin" : 27, "end" : 27},
		{"id" : 27, "text" : "과전", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 130, "begin" : 28, "end" : 28},
		{"id" : 28, "text" : "가운데", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 137, "begin" : 29, "end" : 29},
		{"id" : 29, "text" : "일부", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 147, "begin" : 30, "end" : 30},
		{"id" : 30, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 153, "begin" : 31, "end" : 31},
		{"id" : 31, "text" : "다시", "type" : "MAG", "scode" : "01", "weight" : 1, "position" : 157, "begin" : 32, "end" : 32},
		{"id" : 32, "text" : "지급하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 164, "begin" : 33, "end" : 34},
		{"id" : 33, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 170, "begin" : 35, "end" : 35},
		{"id" : 34, "text" : "세습", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 174, "begin" : 36, "end" : 36},
		{"id" : 35, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 180, "begin" : 37, "end" : 37},
		{"id" : 36, "text" : "가능하", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 184, "begin" : 38, "end" : 39},
		{"id" : 37, "text" : "도록", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 193, "begin" : 40, "end" : 40},
		{"id" : 38, "text" : "한", "type" : "MM", "scode" : "01", "weight" : 1, "position" : 200, "begin" : 41, "end" : 41},
		{"id" : 39, "text" : "토지", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 204, "begin" : 42, "end" : 42},
		{"id" : 40, "text" : "제도", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 211, "begin" : 43, "end" : 43},
		{"id" : 41, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 214, "begin" : 44, "end" : 44},
		{"id" : 42, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 217, "begin" : 45, "end" : 45},
		{"id" : 43, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 220, "begin" : 46, "end" : 46}
	],
	"word" : [
		{"id" : 0, "text" : "﻿이것은", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "조선", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 2, "text" : "왕조", "type" : "", "begin" : 4, "end" : 4},
		{"id" : 3, "text" : "시기에", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 4, "text" : "죽은", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 5, "text" : "관료의", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 6, "text" : "가족들이", "type" : "", "begin" : 11, "end" : 13},
		{"id" : 7, "text" : "생계를", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 8, "text" : "유지할", "type" : "", "begin" : 16, "end" : 18},
		{"id" : 9, "text" : "수", "type" : "", "begin" : 19, "end" : 19},
		{"id" : 10, "text" : "있도록", "type" : "", "begin" : 20, "end" : 21},
		{"id" : 11, "text" : "20세", "type" : "", "begin" : 22, "end" : 23},
		{"id" : 12, "text" : "미만의", "type" : "", "begin" : 24, "end" : 25},
		{"id" : 13, "text" : "자녀에게", "type" : "", "begin" : 26, "end" : 27},
		{"id" : 14, "text" : "과전", "type" : "", "begin" : 28, "end" : 28},
		{"id" : 15, "text" : "가운데", "type" : "", "begin" : 29, "end" : 29},
		{"id" : 16, "text" : "일부를", "type" : "", "begin" : 30, "end" : 31},
		{"id" : 17, "text" : "다시", "type" : "", "begin" : 32, "end" : 32},
		{"id" : 18, "text" : "지급해", "type" : "", "begin" : 33, "end" : 35},
		{"id" : 19, "text" : "세습이", "type" : "", "begin" : 36, "end" : 37},
		{"id" : 20, "text" : "가능하도록", "type" : "", "begin" : 38, "end" : 40},
		{"id" : 21, "text" : "한", "type" : "", "begin" : 41, "end" : 41},
		{"id" : 22, "text" : "토지", "type" : "", "begin" : 42, "end" : 42},
		{"id" : 23, "text" : "제도다.", "type" : "", "begin" : 43, "end" : 46}
	],
	"NE" : [
		{"id" : 0, "text" : "조선 왕조", "type" : "DT_DYNASTY", "begin" : 3, "end" : 4, "weight" : 0.731913, "common_noun" : 0},
		{"id" : 1, "text" : "관료", "type" : "CV_POSITION", "begin" : 9, "end" : 9, "weight" : 0.227974, "common_noun" : 0},
		{"id" : 2, "text" : "가족", "type" : "CV_RELATION", "begin" : 11, "end" : 11, "weight" : 0.498923, "common_noun" : 0},
		{"id" : 3, "text" : "20세 미만", "type" : "QT_AGE", "begin" : 22, "end" : 24, "weight" : 0.288555, "common_noun" : 0},
		{"id" : 4, "text" : "자녀", "type" : "CV_RELATION", "begin" : 26, "end" : 26, "weight" : 0.520041, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿이것은", "head" : 21, "label" : "NP_SBJ", "mod" : [], "weight" : 0.58223 },
		{"id" : 1, "text" : "조선", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.800583 },
		{"id" : 2, "text" : "왕조", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.886945 },
		{"id" : 3, "text" : "시기에", "head" : 4, "label" : "NP_AJT", "mod" : [2], "weight" : 0.55911 },
		{"id" : 4, "text" : "죽은", "head" : 6, "label" : "VP_MOD", "mod" : [3], "weight" : 0.920279 },
		{"id" : 5, "text" : "관료의", "head" : 6, "label" : "NP_MOD", "mod" : [], "weight" : 0.873111 },
		{"id" : 6, "text" : "가족들이", "head" : 8, "label" : "NP_SBJ", "mod" : [4, 5], "weight" : 0.743441 },
		{"id" : 7, "text" : "생계를", "head" : 8, "label" : "NP_OBJ", "mod" : [], "weight" : 0.948754 },
		{"id" : 8, "text" : "유지할", "head" : 9, "label" : "VP_MOD", "mod" : [6, 7], "weight" : 0.878128 },
		{"id" : 9, "text" : "수", "head" : 10, "label" : "NP_SBJ", "mod" : [8], "weight" : 0.861871 },
		{"id" : 10, "text" : "있도록", "head" : 18, "label" : "VP", "mod" : [9], "weight" : 0.820298 },
		{"id" : 11, "text" : "20세", "head" : 12, "label" : "NP", "mod" : [], "weight" : 0.700686 },
		{"id" : 12, "text" : "미만의", "head" : 13, "label" : "NP_MOD", "mod" : [11], "weight" : 0.87188 },
		{"id" : 13, "text" : "자녀에게", "head" : 18, "label" : "NP_AJT", "mod" : [12], "weight" : 0.728501 },
		{"id" : 14, "text" : "과전", "head" : 15, "label" : "NP", "mod" : [], "weight" : 0.98191 },
		{"id" : 15, "text" : "가운데", "head" : 18, "label" : "NP_AJT", "mod" : [14], "weight" : 0.664041 },
		{"id" : 16, "text" : "일부를", "head" : 18, "label" : "NP_OBJ", "mod" : [], "weight" : 0.969699 },
		{"id" : 17, "text" : "다시", "head" : 18, "label" : "AP", "mod" : [], "weight" : 0.948851 },
		{"id" : 18, "text" : "지급해", "head" : 21, "label" : "VP", "mod" : [10, 13, 15, 16, 17], "weight" : 0.874515 },
		{"id" : 19, "text" : "세습이", "head" : 21, "label" : "NP_SBJ", "mod" : [], "weight" : 0.827643 },
		{"id" : 20, "text" : "가능하도록", "head" : 21, "label" : "VP_AJT", "mod" : [], "weight" : 0.591219 },
		{"id" : 21, "text" : "한", "head" : 23, "label" : "VP_MOD", "mod" : [0, 18, 19, 20], "weight" : 0.350037 },
		{"id" : 22, "text" : "토지", "head" : 23, "label" : "NP", "mod" : [], "weight" : 0.357469 },
		{"id" : 23, "text" : "제도다.", "head" : -1, "label" : "VNP", "mod" : [21, 22], "weight" : 0.000680603 }
	],
	"SRL" : [
		{"verb" : "죽", "sense" : 1, "word_id" : 4, "weight" : 0.248021,
			"argument" : [
				{"type" : "ARG1", "word_id" : 5, "text" : "관료의", "weight" : 0.248021 }
			] },
		{"verb" : "유지", "sense" : 1, "word_id" : 8, "weight" : 0.287222,
			"argument" : [
				{"type" : "ARG0", "word_id" : 6, "text" : "가족들이", "weight" : 0.229002 },
				{"type" : "ARG1", "word_id" : 7, "text" : "생계를", "weight" : 0.23313 },
				{"type" : "AUX", "word_id" : 10, "text" : "있도록", "weight" : 0.399535 }
			] },
		{"verb" : "지급", "sense" : 1, "word_id" : 18, "weight" : 0.213543,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 3, "text" : "시기에", "weight" : 0.370052 },
				{"type" : "ARGM-PRP", "word_id" : 10, "text" : "있도록", "weight" : 0.179506 },
				{"type" : "ARG2", "word_id" : 13, "text" : "자녀에게", "weight" : 0.137615 },
				{"type" : "ARG1", "word_id" : 16, "text" : "일부를", "weight" : 0.164284 },
				{"type" : "ARGM-TMP", "word_id" : 17, "text" : "다시", "weight" : 0.21626 }
			] },
		{"verb" : "가능", "sense" : 1, "word_id" : 20, "weight" : 0.200185,
			"argument" : [
				{"type" : "ARG1", "word_id" : 19, "text" : "세습이", "weight" : 0.200185 }
			] },
		{"verb" : "한", "sense" : 4, "word_id" : 21, "weight" : 0.185037,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 3, "text" : "시기에", "weight" : 0.317271 },
				{"type" : "ARGM-CAU", "word_id" : 18, "text" : "지급해", "weight" : 0.129017 },
				{"type" : "ARG1", "word_id" : 20, "text" : "가능하도록", "weight" : 0.108824 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 18, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.711495 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "이것은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "이것", "type" : "NP", "position" : 222, "weight" : 0.0733657 },
		{"id" : 1, "lemma" : "은", "type" : "JX", "position" : 228, "weight" : 0.191811 },
		{"id" : 2, "lemma" : "무엇", "type" : "NP", "position" : 232, "weight" : 0.9 },
		{"id" : 3, "lemma" : "이", "type" : "VCP", "position" : 238, "weight" : 0.0175768 },
		{"id" : 4, "lemma" : "ㄹ까", "type" : "EF", "position" : 238, "weight" : 0.258243 },
		{"id" : 5, "lemma" : "?", "type" : "SF", "position" : 244, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이것/NP+은/JX", "target" : "이것은", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 1, "m_begin" : 2, "m_end" : 5}
	],
	"WSD" : [
		{"id" : 0, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 222, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 228, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 232, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 238, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 238, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 244, "begin" : 5, "end" : 5}
	],
	"word" : [
		{"id" : 0, "text" : "이것은", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "무엇일까?", "type" : "", "begin" : 2, "end" : 5}
	],
	"NE" : [
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이것은", "head" : 1, "label" : "NP_SBJ", "mod" : [], "weight" : 0.769102 },
		{"id" : 1, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [0], "weight" : 0.556699 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "CV_RELATION", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 7, "sent_id" : 0, "start_eid" : 11, "end_eid" : 13, "ne_id" : 4, "text" : "20세 미만의 자녀", "start_eid_short" : 11, "end_eid_short" : 13, "text_short" : "20세 미만의 자녀", "weight" : 0.002 },
		{"id" : 15, "sent_id" : 1, "start_eid" : 0, "end_eid" : 0, "ne_id" : -1, "text" : "이것", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "이것", "weight" : 0.012 }
	] }
 ]
}

