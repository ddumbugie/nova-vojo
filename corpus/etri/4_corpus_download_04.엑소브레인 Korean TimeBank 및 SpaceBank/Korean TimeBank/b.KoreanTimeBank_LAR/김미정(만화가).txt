{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "김미정(11월 16일 ~ )은 대한민국의 만화가다.",
	"morp" : [
		{"id" : 0, "lemma" : "김미정", "type" : "NNP", "position" : 0, "weight" : 0.35 },
		{"id" : 1, "lemma" : "(", "type" : "SS", "position" : 9, "weight" : 1 },
		{"id" : 2, "lemma" : "11", "type" : "SN", "position" : 10, "weight" : 1 },
		{"id" : 3, "lemma" : "월", "type" : "NNB", "position" : 12, "weight" : 0.408539 },
		{"id" : 4, "lemma" : "16", "type" : "SN", "position" : 16, "weight" : 1 },
		{"id" : 5, "lemma" : "일", "type" : "NNB", "position" : 18, "weight" : 0.126777 },
		{"id" : 6, "lemma" : "~", "type" : "SO", "position" : 22, "weight" : 1 },
		{"id" : 7, "lemma" : ")", "type" : "SS", "position" : 24, "weight" : 1 },
		{"id" : 8, "lemma" : "은", "type" : "JX", "position" : 25, "weight" : 0.0128817 },
		{"id" : 9, "lemma" : "대한민국", "type" : "NNP", "position" : 29, "weight" : 0.0447775 },
		{"id" : 10, "lemma" : "의", "type" : "JKG", "position" : 41, "weight" : 0.0987295 },
		{"id" : 11, "lemma" : "만화", "type" : "NNG", "position" : 45, "weight" : 0.83848 },
		{"id" : 12, "lemma" : "가", "type" : "XSN", "position" : 51, "weight" : 0.000115417 },
		{"id" : 13, "lemma" : "이", "type" : "VCP", "position" : 51, "weight" : 0.0165001 },
		{"id" : 14, "lemma" : "다", "type" : "EF", "position" : 54, "weight" : 0.353579 },
		{"id" : 15, "lemma" : ".", "type" : "SF", "position" : 57, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "김미정/NNG+(/SS+11/SN+월/NNB", "target" : "김미정(11월", "word_id" : 0, "m_begin" : 0, "m_end" : 3},
		{"id" : 1, "result" : "16/SN+일/NNB", "target" : "16일", "word_id" : 1, "m_begin" : 4, "m_end" : 5},
		{"id" : 2, "result" : "~/SO", "target" : "~", "word_id" : 2, "m_begin" : 6, "m_end" : 6},
		{"id" : 3, "result" : ")/SS+은/JX", "target" : ")은", "word_id" : 3, "m_begin" : 7, "m_end" : 8},
		{"id" : 4, "result" : "대한민국/NNG+의/JKG", "target" : "대한민국의", "word_id" : 4, "m_begin" : 9, "m_end" : 10},
		{"id" : 5, "result" : "만화가/NNG+이/VCP+다/EF+./SF", "target" : "만화가다.", "word_id" : 5, "m_begin" : 11, "m_end" : 15}
	],
	"WSD" : [
		{"id" : 0, "text" : "김미정", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "11", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "월", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 12, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "16", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 16, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "일", "type" : "NNB", "scode" : "07", "weight" : 1, "position" : 18, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "~", "type" : "SO", "scode" : "00", "weight" : 1, "position" : 22, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 24, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 25, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "대한민국", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 29, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 41, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "만화가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 45, "begin" : 11, "end" : 12},
		{"id" : 12, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 51, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 54, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 57, "begin" : 15, "end" : 15}
	],
	"word" : [
		{"id" : 0, "text" : "김미정(11월", "type" : "", "begin" : 0, "end" : 3},
		{"id" : 1, "text" : "16일", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 2, "text" : "~", "type" : "", "begin" : 6, "end" : 6},
		{"id" : 3, "text" : ")은", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 4, "text" : "대한민국의", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 5, "text" : "만화가다.", "type" : "", "begin" : 11, "end" : 15}
	],
	"NE" : [
		{"id" : 0, "text" : "김미정", "type" : "PS_NAME", "begin" : 0, "end" : 0, "weight" : 0.775551, "common_noun" : 0},
		{"id" : 1, "text" : "11월 16일 ~", "type" : "DT_OTHERS", "begin" : 2, "end" : 6, "weight" : 0.673417, "common_noun" : 0},
		{"id" : 2, "text" : "대한민국", "type" : "LCP_COUNTRY", "begin" : 9, "end" : 9, "weight" : 0.183246, "common_noun" : 0},
		{"id" : 3, "text" : "만화가", "type" : "CV_OCCUPATION", "begin" : 11, "end" : 13, "weight" : 0.226251, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "김미정(11월", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.402569 },
		{"id" : 1, "text" : "16일", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.445364 },
		{"id" : 2, "text" : "~", "head" : 3, "label" : "X", "mod" : [0, 1], "weight" : 0.829103 },
		{"id" : 3, "text" : ")은", "head" : 5, "label" : "NP_SBJ", "mod" : [2], "weight" : 0.448183 },
		{"id" : 4, "text" : "대한민국의", "head" : 5, "label" : "NP_MOD", "mod" : [], "weight" : 0.431135 },
		{"id" : 5, "text" : "만화가다.", "head" : -1, "label" : "VNP", "mod" : [3, 4], "weight" : 0.0166745 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "만화동호회 '오징어회'에서 활동하였고 2003년 만화잡지 《윙크》에서 단편 〈Alley〉(앨리)로 데뷔하였다.",
	"morp" : [
		{"id" : 0, "lemma" : "만화", "type" : "NNG", "position" : 58, "weight" : 0.421383 },
		{"id" : 1, "lemma" : "동호", "type" : "NNG", "position" : 64, "weight" : 0.0193437 },
		{"id" : 2, "lemma" : "회", "type" : "XSN", "position" : 70, "weight" : 0.010766 },
		{"id" : 3, "lemma" : "'", "type" : "SS", "position" : 74, "weight" : 1 },
		{"id" : 4, "lemma" : "오징어", "type" : "NNG", "position" : 75, "weight" : 0.9 },
		{"id" : 5, "lemma" : "회", "type" : "NNG", "position" : 84, "weight" : 0.0122234 },
		{"id" : 6, "lemma" : "'", "type" : "SS", "position" : 87, "weight" : 1 },
		{"id" : 7, "lemma" : "에서", "type" : "JKB", "position" : 88, "weight" : 0.0486066 },
		{"id" : 8, "lemma" : "활동", "type" : "NNG", "position" : 95, "weight" : 0.9 },
		{"id" : 9, "lemma" : "하", "type" : "XSV", "position" : 101, "weight" : 0.0001 },
		{"id" : 10, "lemma" : "었", "type" : "EP", "position" : 104, "weight" : 0.9 },
		{"id" : 11, "lemma" : "고", "type" : "EC", "position" : 107, "weight" : 0.190901 },
		{"id" : 12, "lemma" : "2003", "type" : "SN", "position" : 111, "weight" : 1 },
		{"id" : 13, "lemma" : "년", "type" : "NNB", "position" : 115, "weight" : 0.414343 },
		{"id" : 14, "lemma" : "만화", "type" : "NNG", "position" : 119, "weight" : 0.106433 },
		{"id" : 15, "lemma" : "잡지", "type" : "NNG", "position" : 125, "weight" : 0.9 },
		{"id" : 16, "lemma" : "《", "type" : "SS", "position" : 132, "weight" : 1 },
		{"id" : 17, "lemma" : "윙크", "type" : "NNG", "position" : 135, "weight" : 0.9 },
		{"id" : 18, "lemma" : "》", "type" : "SS", "position" : 141, "weight" : 1 },
		{"id" : 19, "lemma" : "에서", "type" : "JKB", "position" : 144, "weight" : 0.0486066 },
		{"id" : 20, "lemma" : "단편", "type" : "NNG", "position" : 151, "weight" : 0.9 },
		{"id" : 21, "lemma" : "〈", "type" : "SS", "position" : 158, "weight" : 1 },
		{"id" : 22, "lemma" : "Alley", "type" : "SL", "position" : 161, "weight" : 1 },
		{"id" : 23, "lemma" : "〉", "type" : "SS", "position" : 166, "weight" : 1 },
		{"id" : 24, "lemma" : "(", "type" : "SS", "position" : 169, "weight" : 1 },
		{"id" : 25, "lemma" : "앨리", "type" : "NNG", "position" : 170, "weight" : 0.0500002 },
		{"id" : 26, "lemma" : ")", "type" : "SS", "position" : 176, "weight" : 1 },
		{"id" : 27, "lemma" : "로", "type" : "JKB", "position" : 177, "weight" : 0.0485504 },
		{"id" : 28, "lemma" : "데뷔", "type" : "NNG", "position" : 181, "weight" : 0.9 },
		{"id" : 29, "lemma" : "하", "type" : "XSV", "position" : 187, "weight" : 0.0001 },
		{"id" : 30, "lemma" : "었", "type" : "EP", "position" : 190, "weight" : 0.9 },
		{"id" : 31, "lemma" : "다", "type" : "EF", "position" : 193, "weight" : 0.640954 },
		{"id" : 32, "lemma" : ".", "type" : "SF", "position" : 196, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "만화동호회/NNG", "target" : "만화동호회", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "'/SS+오징어회/NNG+'/SS+에서/JKB", "target" : "'오징어회'에서", "word_id" : 1, "m_begin" : 3, "m_end" : 7},
		{"id" : 2, "result" : "활동하/VV+었/EP+고/EC", "target" : "활동하였고", "word_id" : 2, "m_begin" : 8, "m_end" : 11},
		{"id" : 3, "result" : "2003/SN+년/NNB", "target" : "2003년", "word_id" : 3, "m_begin" : 12, "m_end" : 13},
		{"id" : 4, "result" : "만화잡지/NNG", "target" : "만화잡지", "word_id" : 4, "m_begin" : 14, "m_end" : 15},
		{"id" : 5, "result" : "《/SS+윙크/NNG+》/SS+에서/JKB", "target" : "《윙크》에서", "word_id" : 5, "m_begin" : 16, "m_end" : 19},
		{"id" : 6, "result" : "단편/NNG", "target" : "단편", "word_id" : 6, "m_begin" : 20, "m_end" : 20},
		{"id" : 7, "result" : "〈/SS+Alley/SL+〉/SS+(/SS+앨리/NNG+)/SS+로/JKB", "target" : "〈Alley〉(앨리)로", "word_id" : 7, "m_begin" : 21, "m_end" : 27},
		{"id" : 8, "result" : "데뷔하/VV+었/EP+다/EF+./SF", "target" : "데뷔하였다.", "word_id" : 8, "m_begin" : 28, "m_end" : 32}
	],
	"WSD" : [
		{"id" : 0, "text" : "만화", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 58, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "동호회", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 64, "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 74, "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "오징어", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 75, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "회", "type" : "NNG", "scode" : "13", "weight" : 1, "position" : 84, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 87, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 88, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "활동하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 95, "begin" : 8, "end" : 9},
		{"id" : 8, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 104, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 107, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "2003", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 111, "begin" : 12, "end" : 12},
		{"id" : 11, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 115, "begin" : 13, "end" : 13},
		{"id" : 12, "text" : "만화", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 119, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "잡지", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 125, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : "《", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 132, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : "윙크", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 135, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : "》", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 141, "begin" : 18, "end" : 18},
		{"id" : 17, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 144, "begin" : 19, "end" : 19},
		{"id" : 18, "text" : "단편", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 151, "begin" : 20, "end" : 20},
		{"id" : 19, "text" : "〈", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 158, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : "Alley", "type" : "SL", "scode" : "00", "weight" : 1, "position" : 161, "begin" : 22, "end" : 22},
		{"id" : 21, "text" : "〉", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 166, "begin" : 23, "end" : 23},
		{"id" : 22, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 169, "begin" : 24, "end" : 24},
		{"id" : 23, "text" : "앨리", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 170, "begin" : 25, "end" : 25},
		{"id" : 24, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 176, "begin" : 26, "end" : 26},
		{"id" : 25, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 177, "begin" : 27, "end" : 27},
		{"id" : 26, "text" : "데뷔하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 181, "begin" : 28, "end" : 29},
		{"id" : 27, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 190, "begin" : 30, "end" : 30},
		{"id" : 28, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 193, "begin" : 31, "end" : 31},
		{"id" : 29, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 196, "begin" : 32, "end" : 32}
	],
	"word" : [
		{"id" : 0, "text" : "만화동호회", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "'오징어회'에서", "type" : "", "begin" : 3, "end" : 7},
		{"id" : 2, "text" : "활동하였고", "type" : "", "begin" : 8, "end" : 11},
		{"id" : 3, "text" : "2003년", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 4, "text" : "만화잡지", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 5, "text" : "《윙크》에서", "type" : "", "begin" : 16, "end" : 19},
		{"id" : 6, "text" : "단편", "type" : "", "begin" : 20, "end" : 20},
		{"id" : 7, "text" : "〈Alley〉(앨리)로", "type" : "", "begin" : 21, "end" : 27},
		{"id" : 8, "text" : "데뷔하였다.", "type" : "", "begin" : 28, "end" : 32}
	],
	"NE" : [
		{"id" : 0, "text" : "만화동호회", "type" : "EV_OTHERS", "begin" : 0, "end" : 2, "weight" : 0.141077, "common_noun" : 0},
		{"id" : 1, "text" : "오징어회", "type" : "EV_OTHERS", "begin" : 4, "end" : 5, "weight" : 0.0748748, "common_noun" : 0},
		{"id" : 2, "text" : "2003년", "type" : "DT_YEAR", "begin" : 12, "end" : 13, "weight" : 0.742166, "common_noun" : 0},
		{"id" : 3, "text" : "만화", "type" : "FD_ART", "begin" : 14, "end" : 14, "weight" : 0.353534, "common_noun" : 0},
		{"id" : 4, "text" : "윙크", "type" : "AF_WORKS", "begin" : 17, "end" : 17, "weight" : 0.593698, "common_noun" : 0},
		{"id" : 5, "text" : "Alley", "type" : "AFW_DOCUMENT", "begin" : 22, "end" : 22, "weight" : 0.358635, "common_noun" : 0},
		{"id" : 6, "text" : "앨리", "type" : "PS_NAME", "begin" : 25, "end" : 25, "weight" : 0.103551, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "만화동호회", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.807524 },
		{"id" : 1, "text" : "'오징어회'에서", "head" : 2, "label" : "NP_AJT", "mod" : [0], "weight" : 0.722184 },
		{"id" : 2, "text" : "활동하였고", "head" : 8, "label" : "VP", "mod" : [1], "weight" : 0.593461 },
		{"id" : 3, "text" : "2003년", "head" : 5, "label" : "NP", "mod" : [], "weight" : 0.631683 },
		{"id" : 4, "text" : "만화잡지", "head" : 5, "label" : "NP", "mod" : [], "weight" : 0.740495 },
		{"id" : 5, "text" : "《윙크》에서", "head" : 8, "label" : "NP_AJT", "mod" : [3, 4], "weight" : 0.585154 },
		{"id" : 6, "text" : "단편", "head" : 7, "label" : "NP", "mod" : [], "weight" : 0.752517 },
		{"id" : 7, "text" : "〈Alley〉(앨리)로", "head" : 8, "label" : "NP_AJT", "mod" : [6], "weight" : 0.400245 },
		{"id" : 8, "text" : "데뷔하였다.", "head" : -1, "label" : "VP", "mod" : [2, 5, 7], "weight" : 0.0155391 }
	],
	"SRL" : [
		{"verb" : "활동", "sense" : 1, "word_id" : 2, "weight" : 0.0858389,
			"argument" : [
				{"type" : "ARG1", "word_id" : 1, "text" : "'오징어회'에서", "weight" : 0.0858389 }
			] },
		{"verb" : "데뷔", "sense" : 1, "word_id" : 8, "weight" : 0.0933625,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 5, "text" : "《윙크》에서", "weight" : 0.106624 },
				{"type" : "ARG3", "word_id" : 7, "text" : "〈Alley〉(앨리)로", "weight" : 0.0801009 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 2, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.500348 },
		{"id" : 1, "verb_wid" : 8, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.591771 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "CV_OCCUPATION", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 0, "ne_id" : 0, "text" : "김미정(11월", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "김미정(11월", "weight" : 0.004 },
		{"id" : 2, "sent_id" : 0, "start_eid" : 5, "end_eid" : 5, "ne_id" : 3, "text" : "만화가이다.", "start_eid_short" : 5, "end_eid_short" : 5, "text_short" : "만화가이다.", "weight" : 0.009 }
	] },
	{"id" : 1, "type" : "EV_OTHERS", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 3, "sent_id" : 1, "start_eid" : 0, "end_eid" : 0, "ne_id" : 0, "text" : "만화동호회", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "만화동호회", "weight" : 0.002 },
		{"id" : 4, "sent_id" : 1, "start_eid" : 1, "end_eid" : 1, "ne_id" : 1, "text" : "'오징어회'", "start_eid_short" : 1, "end_eid_short" : 1, "text_short" : "'오징어회'", "weight" : 0.003 }
	] }
 ]
}

