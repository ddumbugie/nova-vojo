{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿이것은 고려 왕조 시기에 화약 및 화기의 제조를 맡아보던 관청으로 '우왕'때 최무선의 건의로 설치했다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "이것", "type" : "NP", "position" : 3, "weight" : 0.0020857 },
		{"id" : 2, "lemma" : "은", "type" : "JX", "position" : 9, "weight" : 0.191811 },
		{"id" : 3, "lemma" : "고려", "type" : "NNP", "position" : 13, "weight" : 0.042748 },
		{"id" : 4, "lemma" : "왕조", "type" : "NNG", "position" : 20, "weight" : 0.9 },
		{"id" : 5, "lemma" : "시기", "type" : "NNG", "position" : 27, "weight" : 0.184726 },
		{"id" : 6, "lemma" : "에", "type" : "JKB", "position" : 33, "weight" : 0.153364 },
		{"id" : 7, "lemma" : "화약", "type" : "NNG", "position" : 37, "weight" : 0.9 },
		{"id" : 8, "lemma" : "및", "type" : "MAJ", "position" : 44, "weight" : 0.00215414 },
		{"id" : 9, "lemma" : "화기", "type" : "NNG", "position" : 48, "weight" : 0.9 },
		{"id" : 10, "lemma" : "의", "type" : "JKG", "position" : 54, "weight" : 0.0694213 },
		{"id" : 11, "lemma" : "제조", "type" : "NNG", "position" : 58, "weight" : 0.9 },
		{"id" : 12, "lemma" : "를", "type" : "JKO", "position" : 64, "weight" : 0.137686 },
		{"id" : 13, "lemma" : "맡아보", "type" : "VV", "position" : 68, "weight" : 0.9 },
		{"id" : 14, "lemma" : "던", "type" : "ETM", "position" : 77, "weight" : 0.317449 },
		{"id" : 15, "lemma" : "관청", "type" : "NNG", "position" : 81, "weight" : 0.9 },
		{"id" : 16, "lemma" : "으로", "type" : "JKB", "position" : 87, "weight" : 0.153406 },
		{"id" : 17, "lemma" : "'", "type" : "SS", "position" : 94, "weight" : 1 },
		{"id" : 18, "lemma" : "우왕", "type" : "NNP", "position" : 95, "weight" : 0.9 },
		{"id" : 19, "lemma" : "'", "type" : "SS", "position" : 101, "weight" : 1 },
		{"id" : 20, "lemma" : "때", "type" : "NNG", "position" : 102, "weight" : 0.199297 },
		{"id" : 21, "lemma" : "최무선", "type" : "NNP", "position" : 106, "weight" : 0.15 },
		{"id" : 22, "lemma" : "의", "type" : "JKG", "position" : 115, "weight" : 0.0987295 },
		{"id" : 23, "lemma" : "건의", "type" : "NNG", "position" : 119, "weight" : 0.9 },
		{"id" : 24, "lemma" : "로", "type" : "JKB", "position" : 125, "weight" : 0.153229 },
		{"id" : 25, "lemma" : "설치", "type" : "NNG", "position" : 129, "weight" : 0.153832 },
		{"id" : 26, "lemma" : "하", "type" : "XSV", "position" : 135, "weight" : 0.0001 },
		{"id" : 27, "lemma" : "었", "type" : "EP", "position" : 135, "weight" : 0.9 },
		{"id" : 28, "lemma" : "다", "type" : "EF", "position" : 138, "weight" : 0.640954 },
		{"id" : 29, "lemma" : ".", "type" : "SF", "position" : 141, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿/NNG+이것/NP+은/JX", "target" : "﻿이것은", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "고려/NNG", "target" : "고려", "word_id" : 1, "m_begin" : 3, "m_end" : 3},
		{"id" : 2, "result" : "왕조/NNG", "target" : "왕조", "word_id" : 2, "m_begin" : 4, "m_end" : 4},
		{"id" : 3, "result" : "시기/NNG+에/JKB", "target" : "시기에", "word_id" : 3, "m_begin" : 5, "m_end" : 6},
		{"id" : 4, "result" : "화약/NNG", "target" : "화약", "word_id" : 4, "m_begin" : 7, "m_end" : 7},
		{"id" : 5, "result" : "및/MAJ", "target" : "및", "word_id" : 5, "m_begin" : 8, "m_end" : 8},
		{"id" : 6, "result" : "화기/NNG+의/JKG", "target" : "화기의", "word_id" : 6, "m_begin" : 9, "m_end" : 10},
		{"id" : 7, "result" : "제조/NNG+를/JKO", "target" : "제조를", "word_id" : 7, "m_begin" : 11, "m_end" : 12},
		{"id" : 8, "result" : "맡아보/VV+던/ETM", "target" : "맡아보던", "word_id" : 8, "m_begin" : 13, "m_end" : 14},
		{"id" : 9, "result" : "관청/NNG+으로/JKB", "target" : "관청으로", "word_id" : 9, "m_begin" : 15, "m_end" : 16},
		{"id" : 10, "result" : "'/SS+우왕/NNG+'/SS+때/NNG", "target" : "'우왕'때", "word_id" : 10, "m_begin" : 17, "m_end" : 20},
		{"id" : 11, "result" : "최무선/NNG+의/JKG", "target" : "최무선의", "word_id" : 11, "m_begin" : 21, "m_end" : 22},
		{"id" : 12, "result" : "건의/NNG+로/JKB", "target" : "건의로", "word_id" : 12, "m_begin" : 23, "m_end" : 24},
		{"id" : 13, "result" : "설치하/VV+었/EP+다/EF+./SF", "target" : "설치했다.", "word_id" : 13, "m_begin" : 25, "m_end" : 29}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "고려", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 13, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "왕조", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 20, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "시기", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 27, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 33, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "화약", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 37, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "및", "type" : "MAJ", "scode" : "00", "weight" : 1, "position" : 44, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "화기", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 48, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 54, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "제조", "type" : "NNG", "scode" : "07", "weight" : 1, "position" : 58, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 64, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "맡아보", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 68, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "던", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 77, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "관청", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 81, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 87, "begin" : 16, "end" : 16},
		{"id" : 17, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 94, "begin" : 17, "end" : 17},
		{"id" : 18, "text" : "우왕", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 95, "begin" : 18, "end" : 18},
		{"id" : 19, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 101, "begin" : 19, "end" : 19},
		{"id" : 20, "text" : "때", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 102, "begin" : 20, "end" : 20},
		{"id" : 21, "text" : "최무선", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 106, "begin" : 21, "end" : 21},
		{"id" : 22, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 115, "begin" : 22, "end" : 22},
		{"id" : 23, "text" : "건의", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 119, "begin" : 23, "end" : 23},
		{"id" : 24, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 125, "begin" : 24, "end" : 24},
		{"id" : 25, "text" : "설치하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 129, "begin" : 25, "end" : 26},
		{"id" : 26, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 135, "begin" : 27, "end" : 27},
		{"id" : 27, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 138, "begin" : 28, "end" : 28},
		{"id" : 28, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 141, "begin" : 29, "end" : 29}
	],
	"word" : [
		{"id" : 0, "text" : "﻿이것은", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "고려", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 2, "text" : "왕조", "type" : "", "begin" : 4, "end" : 4},
		{"id" : 3, "text" : "시기에", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 4, "text" : "화약", "type" : "", "begin" : 7, "end" : 7},
		{"id" : 5, "text" : "및", "type" : "", "begin" : 8, "end" : 8},
		{"id" : 6, "text" : "화기의", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 7, "text" : "제조를", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 8, "text" : "맡아보던", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 9, "text" : "관청으로", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 10, "text" : "'우왕'때", "type" : "", "begin" : 17, "end" : 20},
		{"id" : 11, "text" : "최무선의", "type" : "", "begin" : 21, "end" : 22},
		{"id" : 12, "text" : "건의로", "type" : "", "begin" : 23, "end" : 24},
		{"id" : 13, "text" : "설치했다.", "type" : "", "begin" : 25, "end" : 29}
	],
	"NE" : [
		{"id" : 0, "text" : "고려 왕조", "type" : "DT_DYNASTY", "begin" : 3, "end" : 4, "weight" : 0.747962, "common_noun" : 0},
		{"id" : 1, "text" : "우왕", "type" : "PS_NAME", "begin" : 18, "end" : 18, "weight" : 0.226633, "common_noun" : 0},
		{"id" : 2, "text" : "최무선", "type" : "PS_NAME", "begin" : 21, "end" : 21, "weight" : 0.389977, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿이것은", "head" : 9, "label" : "NP_SBJ", "mod" : [], "weight" : 0.544043 },
		{"id" : 1, "text" : "고려", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.740371 },
		{"id" : 2, "text" : "왕조", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.958325 },
		{"id" : 3, "text" : "시기에", "head" : 8, "label" : "NP_AJT", "mod" : [2], "weight" : 0.642284 },
		{"id" : 4, "text" : "화약", "head" : 6, "label" : "NP_CNJ", "mod" : [], "weight" : 0.967028 },
		{"id" : 5, "text" : "및", "head" : 6, "label" : "AP", "mod" : [], "weight" : 0.664508 },
		{"id" : 6, "text" : "화기의", "head" : 7, "label" : "NP_MOD", "mod" : [4, 5], "weight" : 0.927963 },
		{"id" : 7, "text" : "제조를", "head" : 8, "label" : "NP_OBJ", "mod" : [6], "weight" : 0.933421 },
		{"id" : 8, "text" : "맡아보던", "head" : 9, "label" : "VP_MOD", "mod" : [3, 7], "weight" : 0.680803 },
		{"id" : 9, "text" : "관청으로", "head" : 13, "label" : "NP_AJT", "mod" : [0, 8], "weight" : 0.680376 },
		{"id" : 10, "text" : "'우왕'때", "head" : 13, "label" : "NP_AJT", "mod" : [], "weight" : 0.431866 },
		{"id" : 11, "text" : "최무선의", "head" : 12, "label" : "NP_MOD", "mod" : [], "weight" : 0.830531 },
		{"id" : 12, "text" : "건의로", "head" : 13, "label" : "NP_AJT", "mod" : [11], "weight" : 0.486919 },
		{"id" : 13, "text" : "설치했다.", "head" : -1, "label" : "VP", "mod" : [9, 10, 12], "weight" : 0.00661586 }
	],
	"SRL" : [
		{"verb" : "맡아보", "sense" : 1, "word_id" : 8, "weight" : 0.258635,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 3, "text" : "시기에", "weight" : 0.288365 },
				{"type" : "ARG1", "word_id" : 7, "text" : "제조를", "weight" : 0.258059 },
				{"type" : "ARG0", "word_id" : 9, "text" : "관청으로", "weight" : 0.22948 }
			] },
		{"verb" : "설치", "sense" : 1, "word_id" : 13, "weight" : 0.13425,
			"argument" : [
				{"type" : "ARG1", "word_id" : 0, "text" : "﻿이것은", "weight" : 0.163299 },
				{"type" : "ARG1", "word_id" : 9, "text" : "관청으로", "weight" : 0.0747943 },
				{"type" : "ARGM-TMP", "word_id" : 10, "text" : "'우왕'때", "weight" : 0.136751 },
				{"type" : "ARGM-CAU", "word_id" : 12, "text" : "건의로", "weight" : 0.162155 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 13, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.524837 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "고려 말 왜구를 격퇴하는데 필요한 화기를 생산했던 이 관청은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "고려", "type" : "NNP", "position" : 143, "weight" : 0.0850783 },
		{"id" : 1, "lemma" : "말", "type" : "NNG", "position" : 150, "weight" : 0.226386 },
		{"id" : 2, "lemma" : "왜구", "type" : "NNG", "position" : 154, "weight" : 0.9 },
		{"id" : 3, "lemma" : "를", "type" : "JKO", "position" : 160, "weight" : 0.137686 },
		{"id" : 4, "lemma" : "격퇴", "type" : "NNG", "position" : 164, "weight" : 0.65 },
		{"id" : 5, "lemma" : "하", "type" : "XSV", "position" : 170, "weight" : 0.0001 },
		{"id" : 6, "lemma" : "는데", "type" : "EC", "position" : 173, "weight" : 0.35474 },
		{"id" : 7, "lemma" : "필요", "type" : "NNG", "position" : 180, "weight" : 0.9 },
		{"id" : 8, "lemma" : "하", "type" : "XSA", "position" : 186, "weight" : 0.0001 },
		{"id" : 9, "lemma" : "ㄴ", "type" : "ETM", "position" : 186, "weight" : 0.488779 },
		{"id" : 10, "lemma" : "화기", "type" : "NNG", "position" : 190, "weight" : 0.9 },
		{"id" : 11, "lemma" : "를", "type" : "JKO", "position" : 196, "weight" : 0.137686 },
		{"id" : 12, "lemma" : "생산", "type" : "NNG", "position" : 200, "weight" : 0.9 },
		{"id" : 13, "lemma" : "하", "type" : "XSV", "position" : 206, "weight" : 0.0001 },
		{"id" : 14, "lemma" : "었", "type" : "EP", "position" : 206, "weight" : 0.9 },
		{"id" : 15, "lemma" : "던", "type" : "ETM", "position" : 209, "weight" : 0.107547 },
		{"id" : 16, "lemma" : "이", "type" : "MM", "position" : 213, "weight" : 0.00060084 },
		{"id" : 17, "lemma" : "관청", "type" : "NNG", "position" : 217, "weight" : 0.9 },
		{"id" : 18, "lemma" : "은", "type" : "JX", "position" : 223, "weight" : 0.0449928 },
		{"id" : 19, "lemma" : "무엇", "type" : "NP", "position" : 227, "weight" : 0.9 },
		{"id" : 20, "lemma" : "이", "type" : "VCP", "position" : 233, "weight" : 0.0175768 },
		{"id" : 21, "lemma" : "ㄹ까", "type" : "EF", "position" : 233, "weight" : 0.258243 },
		{"id" : 22, "lemma" : "?", "type" : "SF", "position" : 239, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "고려/NNG", "target" : "고려", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "말/NNG", "target" : "말", "word_id" : 1, "m_begin" : 1, "m_end" : 1},
		{"id" : 2, "result" : "왜구/NNG+를/JKO", "target" : "왜구를", "word_id" : 2, "m_begin" : 2, "m_end" : 3},
		{"id" : 3, "result" : "격퇴하/VV+는데/EC", "target" : "격퇴하는데", "word_id" : 3, "m_begin" : 4, "m_end" : 6},
		{"id" : 4, "result" : "필요하/VA+ㄴ/ETM", "target" : "필요한", "word_id" : 4, "m_begin" : 7, "m_end" : 9},
		{"id" : 5, "result" : "화기/NNG+를/JKO", "target" : "화기를", "word_id" : 5, "m_begin" : 10, "m_end" : 11},
		{"id" : 6, "result" : "생산하/VV+었/EP+던/ETM", "target" : "생산했던", "word_id" : 6, "m_begin" : 12, "m_end" : 15},
		{"id" : 7, "result" : "이/MM", "target" : "이", "word_id" : 7, "m_begin" : 16, "m_end" : 16},
		{"id" : 8, "result" : "관청/NNG+은/JX", "target" : "관청은", "word_id" : 8, "m_begin" : 17, "m_end" : 18},
		{"id" : 9, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 9, "m_begin" : 19, "m_end" : 22}
	],
	"WSD" : [
		{"id" : 0, "text" : "고려", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 143, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "말", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 150, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "왜구", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 154, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 160, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "격퇴하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 164, "begin" : 4, "end" : 5},
		{"id" : 5, "text" : "는데", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 173, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "필요하", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 180, "begin" : 7, "end" : 8},
		{"id" : 7, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 186, "begin" : 9, "end" : 9},
		{"id" : 8, "text" : "화기", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 190, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 196, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "생산하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 200, "begin" : 12, "end" : 13},
		{"id" : 11, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 206, "begin" : 14, "end" : 14},
		{"id" : 12, "text" : "던", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 209, "begin" : 15, "end" : 15},
		{"id" : 13, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 213, "begin" : 16, "end" : 16},
		{"id" : 14, "text" : "관청", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 217, "begin" : 17, "end" : 17},
		{"id" : 15, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 223, "begin" : 18, "end" : 18},
		{"id" : 16, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 227, "begin" : 19, "end" : 19},
		{"id" : 17, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 233, "begin" : 20, "end" : 20},
		{"id" : 18, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 233, "begin" : 21, "end" : 21},
		{"id" : 19, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 239, "begin" : 22, "end" : 22}
	],
	"word" : [
		{"id" : 0, "text" : "고려", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "말", "type" : "", "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "왜구를", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 3, "text" : "격퇴하는데", "type" : "", "begin" : 4, "end" : 6},
		{"id" : 4, "text" : "필요한", "type" : "", "begin" : 7, "end" : 9},
		{"id" : 5, "text" : "화기를", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 6, "text" : "생산했던", "type" : "", "begin" : 12, "end" : 15},
		{"id" : 7, "text" : "이", "type" : "", "begin" : 16, "end" : 16},
		{"id" : 8, "text" : "관청은", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 9, "text" : "무엇일까?", "type" : "", "begin" : 19, "end" : 22}
	],
	"NE" : [
		{"id" : 0, "text" : "고려 말", "type" : "DT_DYNASTY", "begin" : 0, "end" : 1, "weight" : 0.613406, "common_noun" : 0},
		{"id" : 1, "text" : "왜구", "type" : "OGG_MILITARY", "begin" : 2, "end" : 2, "weight" : 0.260757, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "고려", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.847687 },
		{"id" : 1, "text" : "말", "head" : 6, "label" : "NP", "mod" : [0], "weight" : 0.597453 },
		{"id" : 2, "text" : "왜구를", "head" : 3, "label" : "NP_OBJ", "mod" : [], "weight" : 0.8069 },
		{"id" : 3, "text" : "격퇴하는데", "head" : 4, "label" : "VP", "mod" : [2], "weight" : 0.571349 },
		{"id" : 4, "text" : "필요한", "head" : 5, "label" : "VP_MOD", "mod" : [3], "weight" : 0.975259 },
		{"id" : 5, "text" : "화기를", "head" : 6, "label" : "NP_OBJ", "mod" : [4], "weight" : 0.878053 },
		{"id" : 6, "text" : "생산했던", "head" : 8, "label" : "VP_MOD", "mod" : [1, 5], "weight" : 0.861056 },
		{"id" : 7, "text" : "이", "head" : 8, "label" : "DP", "mod" : [], "weight" : 0.918607 },
		{"id" : 8, "text" : "관청은", "head" : 9, "label" : "NP_SBJ", "mod" : [6, 7], "weight" : 0.785433 },
		{"id" : 9, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [8], "weight" : 0.08849 }
	],
	"SRL" : [
		{"verb" : "격퇴", "sense" : 1, "word_id" : 3, "weight" : 0.282156,
			"argument" : [
				{"type" : "ARG1", "word_id" : 2, "text" : "왜구를", "weight" : 0.282156 }
			] },
		{"verb" : "필요", "sense" : 1, "word_id" : 4, "weight" : 0.153084,
			"argument" : [
				{"type" : "ARG2", "word_id" : 3, "text" : "격퇴하는데", "weight" : 0.164994 },
				{"type" : "ARG1", "word_id" : 5, "text" : "화기를", "weight" : 0.141174 }
			] },
		{"verb" : "생산", "sense" : 1, "word_id" : 6, "weight" : 0.255199,
			"argument" : [
				{"type" : "ARG1", "word_id" : 5, "text" : "화기를", "weight" : 0.325595 },
				{"type" : "ARG0", "word_id" : 8, "text" : "관청은", "weight" : 0.184802 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 3, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.644831 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 9, "ne_id" : -1, "text" : "﻿이것은 고려 왕조 시기에 화약 및 화기의 제조를 맡아보던 관청", "start_eid_short" : 9, "end_eid_short" : 9, "text_short" : "관청", "weight" : 0.002 },
		{"id" : 13, "sent_id" : 1, "start_eid" : 7, "end_eid" : 8, "ne_id" : -1, "text" : "이 관청", "start_eid_short" : 7, "end_eid_short" : 8, "text_short" : "이 관청", "weight" : 0.006 },
		{"id" : 15, "sent_id" : 1, "start_eid" : 9, "end_eid" : 9, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 9, "end_eid_short" : 9, "text_short" : "무엇이ㄹ까?", "weight" : 0.006 }
	] }
 ]
}

