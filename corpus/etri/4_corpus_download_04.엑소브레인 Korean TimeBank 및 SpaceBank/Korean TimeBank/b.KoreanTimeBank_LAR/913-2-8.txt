{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "이 단체는 구한말 충청도 지방에서 봉기한 농민 집단으로 주로 부자들을 약탈해 빼앗은 재산을 가난한 사람들에게 나눠주는 활동을 벌였다.",
	"morp" : [
		{"id" : 0, "lemma" : "이", "type" : "MM", "position" : 0, "weight" : 0.0020222 },
		{"id" : 1, "lemma" : "단체", "type" : "NNG", "position" : 4, "weight" : 0.737327 },
		{"id" : 2, "lemma" : "는", "type" : "JX", "position" : 10, "weight" : 0.0287565 },
		{"id" : 3, "lemma" : "구", "type" : "XPN", "position" : 14, "weight" : 0.000125725 },
		{"id" : 4, "lemma" : "한말", "type" : "NNG", "position" : 17, "weight" : 0.9 },
		{"id" : 5, "lemma" : "충청도", "type" : "NNP", "position" : 24, "weight" : 0.00549445 },
		{"id" : 6, "lemma" : "지방", "type" : "NNG", "position" : 34, "weight" : 0.9 },
		{"id" : 7, "lemma" : "에서", "type" : "JKB", "position" : 40, "weight" : 0.153407 },
		{"id" : 8, "lemma" : "봉기", "type" : "NNG", "position" : 47, "weight" : 0.193698 },
		{"id" : 9, "lemma" : "하", "type" : "XSV", "position" : 53, "weight" : 0.0001 },
		{"id" : 10, "lemma" : "ㄴ", "type" : "ETM", "position" : 53, "weight" : 0.392321 },
		{"id" : 11, "lemma" : "농민", "type" : "NNG", "position" : 57, "weight" : 0.9 },
		{"id" : 12, "lemma" : "집단", "type" : "NNG", "position" : 64, "weight" : 0.9 },
		{"id" : 13, "lemma" : "으로", "type" : "JKB", "position" : 70, "weight" : 0.153406 },
		{"id" : 14, "lemma" : "주로", "type" : "MAG", "position" : 77, "weight" : 0.0557734 },
		{"id" : 15, "lemma" : "부자", "type" : "NNG", "position" : 84, "weight" : 0.9 },
		{"id" : 16, "lemma" : "들", "type" : "XSN", "position" : 90, "weight" : 0.0299841 },
		{"id" : 17, "lemma" : "을", "type" : "JKO", "position" : 93, "weight" : 0.0819193 },
		{"id" : 18, "lemma" : "약탈", "type" : "NNG", "position" : 97, "weight" : 0.9 },
		{"id" : 19, "lemma" : "하", "type" : "XSV", "position" : 103, "weight" : 0.0001 },
		{"id" : 20, "lemma" : "어", "type" : "EC", "position" : 103, "weight" : 0.361326 },
		{"id" : 21, "lemma" : "빼앗", "type" : "VV", "position" : 107, "weight" : 0.9 },
		{"id" : 22, "lemma" : "은", "type" : "ETM", "position" : 113, "weight" : 0.109632 },
		{"id" : 23, "lemma" : "재산", "type" : "NNG", "position" : 117, "weight" : 0.9 },
		{"id" : 24, "lemma" : "을", "type" : "JKO", "position" : 123, "weight" : 0.129611 },
		{"id" : 25, "lemma" : "가난", "type" : "NNG", "position" : 127, "weight" : 0.9 },
		{"id" : 26, "lemma" : "하", "type" : "XSA", "position" : 133, "weight" : 0.0001 },
		{"id" : 27, "lemma" : "ㄴ", "type" : "ETM", "position" : 133, "weight" : 0.488779 },
		{"id" : 28, "lemma" : "사람", "type" : "NNG", "position" : 137, "weight" : 0.658826 },
		{"id" : 29, "lemma" : "들", "type" : "XSN", "position" : 143, "weight" : 0.0299841 },
		{"id" : 30, "lemma" : "에게", "type" : "JKB", "position" : 146, "weight" : 0.121677 },
		{"id" : 31, "lemma" : "나누", "type" : "VV", "position" : 153, "weight" : 0.9 },
		{"id" : 32, "lemma" : "어", "type" : "EC", "position" : 156, "weight" : 0.41831 },
		{"id" : 33, "lemma" : "주", "type" : "VV", "position" : 159, "weight" : 0.0652823 },
		{"id" : 34, "lemma" : "는", "type" : "ETM", "position" : 162, "weight" : 0.184941 },
		{"id" : 35, "lemma" : "활동", "type" : "NNG", "position" : 166, "weight" : 0.9 },
		{"id" : 36, "lemma" : "을", "type" : "JKO", "position" : 172, "weight" : 0.129611 },
		{"id" : 37, "lemma" : "벌이", "type" : "VV", "position" : 176, "weight" : 0.761655 },
		{"id" : 38, "lemma" : "었", "type" : "EP", "position" : 179, "weight" : 0.9 },
		{"id" : 39, "lemma" : "다", "type" : "EF", "position" : 182, "weight" : 0.640954 },
		{"id" : 40, "lemma" : ".", "type" : "SF", "position" : 185, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이/MM", "target" : "이", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "단체/NNG+는/JX", "target" : "단체는", "word_id" : 1, "m_begin" : 1, "m_end" : 2},
		{"id" : 2, "result" : "구한말/NNG", "target" : "구한말", "word_id" : 2, "m_begin" : 3, "m_end" : 4},
		{"id" : 3, "result" : "충청도/NNG", "target" : "충청도", "word_id" : 3, "m_begin" : 5, "m_end" : 5},
		{"id" : 4, "result" : "지방/NNG+에서/JKB", "target" : "지방에서", "word_id" : 4, "m_begin" : 6, "m_end" : 7},
		{"id" : 5, "result" : "봉기하/VV+ㄴ/ETM", "target" : "봉기한", "word_id" : 5, "m_begin" : 8, "m_end" : 10},
		{"id" : 6, "result" : "농민/NNG", "target" : "농민", "word_id" : 6, "m_begin" : 11, "m_end" : 11},
		{"id" : 7, "result" : "집단/NNG+으로/JKB", "target" : "집단으로", "word_id" : 7, "m_begin" : 12, "m_end" : 13},
		{"id" : 8, "result" : "주로/MAG", "target" : "주로", "word_id" : 8, "m_begin" : 14, "m_end" : 14},
		{"id" : 9, "result" : "부자들/NNG+을/JKO", "target" : "부자들을", "word_id" : 9, "m_begin" : 15, "m_end" : 17},
		{"id" : 10, "result" : "약탈하/VV+어/EC", "target" : "약탈해", "word_id" : 10, "m_begin" : 18, "m_end" : 20},
		{"id" : 11, "result" : "빼앗/VV+은/ETM", "target" : "빼앗은", "word_id" : 11, "m_begin" : 21, "m_end" : 22},
		{"id" : 12, "result" : "재산/NNG+을/JKO", "target" : "재산을", "word_id" : 12, "m_begin" : 23, "m_end" : 24},
		{"id" : 13, "result" : "가난하/VA+ㄴ/ETM", "target" : "가난한", "word_id" : 13, "m_begin" : 25, "m_end" : 27},
		{"id" : 14, "result" : "사람들/NNG+에게/JKB", "target" : "사람들에게", "word_id" : 14, "m_begin" : 28, "m_end" : 30},
		{"id" : 15, "result" : "나누/VV+어/EC+주/VV+는/ETM", "target" : "나눠주는", "word_id" : 15, "m_begin" : 31, "m_end" : 34},
		{"id" : 16, "result" : "활동/NNG+을/JKO", "target" : "활동을", "word_id" : 16, "m_begin" : 35, "m_end" : 36},
		{"id" : 17, "result" : "벌이/VV+었/EP+다/EF+./SF", "target" : "벌였다.", "word_id" : 17, "m_begin" : 37, "m_end" : 40}
	],
	"WSD" : [
		{"id" : 0, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "단체", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 4, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "구한말", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 14, "begin" : 3, "end" : 4},
		{"id" : 4, "text" : "충청도", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 24, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "지방", "type" : "NNG", "scode" : "05", "weight" : 1, "position" : 34, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "봉기하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 47, "begin" : 8, "end" : 9},
		{"id" : 8, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 53, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "농민", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 57, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "집단", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 64, "begin" : 12, "end" : 12},
		{"id" : 11, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 70, "begin" : 13, "end" : 13},
		{"id" : 12, "text" : "주로", "type" : "MAG", "scode" : "01", "weight" : 1, "position" : 77, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "부자", "type" : "NNG", "scode" : "08", "weight" : 1, "position" : 84, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : "들", "type" : "XSN", "scode" : "09", "weight" : 1, "position" : 90, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 93, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : "약탈하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 97, "begin" : 18, "end" : 19},
		{"id" : 17, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 103, "begin" : 20, "end" : 20},
		{"id" : 18, "text" : "빼앗", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 107, "begin" : 21, "end" : 21},
		{"id" : 19, "text" : "은", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 113, "begin" : 22, "end" : 22},
		{"id" : 20, "text" : "재산", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 117, "begin" : 23, "end" : 23},
		{"id" : 21, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 123, "begin" : 24, "end" : 24},
		{"id" : 22, "text" : "가난하", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 127, "begin" : 25, "end" : 26},
		{"id" : 23, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 133, "begin" : 27, "end" : 27},
		{"id" : 24, "text" : "사람", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 137, "begin" : 28, "end" : 28},
		{"id" : 25, "text" : "들", "type" : "XSN", "scode" : "09", "weight" : 1, "position" : 143, "begin" : 29, "end" : 29},
		{"id" : 26, "text" : "에게", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 146, "begin" : 30, "end" : 30},
		{"id" : 27, "text" : "나누", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 153, "begin" : 31, "end" : 31},
		{"id" : 28, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 156, "begin" : 32, "end" : 32},
		{"id" : 29, "text" : "주", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 159, "begin" : 33, "end" : 33},
		{"id" : 30, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 162, "begin" : 34, "end" : 34},
		{"id" : 31, "text" : "활동", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 166, "begin" : 35, "end" : 35},
		{"id" : 32, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 172, "begin" : 36, "end" : 36},
		{"id" : 33, "text" : "벌이", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 176, "begin" : 37, "end" : 37},
		{"id" : 34, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 179, "begin" : 38, "end" : 38},
		{"id" : 35, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 182, "begin" : 39, "end" : 39},
		{"id" : 36, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 185, "begin" : 40, "end" : 40}
	],
	"word" : [
		{"id" : 0, "text" : "이", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "단체는", "type" : "", "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "구한말", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 3, "text" : "충청도", "type" : "", "begin" : 5, "end" : 5},
		{"id" : 4, "text" : "지방에서", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 5, "text" : "봉기한", "type" : "", "begin" : 8, "end" : 10},
		{"id" : 6, "text" : "농민", "type" : "", "begin" : 11, "end" : 11},
		{"id" : 7, "text" : "집단으로", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 8, "text" : "주로", "type" : "", "begin" : 14, "end" : 14},
		{"id" : 9, "text" : "부자들을", "type" : "", "begin" : 15, "end" : 17},
		{"id" : 10, "text" : "약탈해", "type" : "", "begin" : 18, "end" : 20},
		{"id" : 11, "text" : "빼앗은", "type" : "", "begin" : 21, "end" : 22},
		{"id" : 12, "text" : "재산을", "type" : "", "begin" : 23, "end" : 24},
		{"id" : 13, "text" : "가난한", "type" : "", "begin" : 25, "end" : 27},
		{"id" : 14, "text" : "사람들에게", "type" : "", "begin" : 28, "end" : 30},
		{"id" : 15, "text" : "나눠주는", "type" : "", "begin" : 31, "end" : 34},
		{"id" : 16, "text" : "활동을", "type" : "", "begin" : 35, "end" : 36},
		{"id" : 17, "text" : "벌였다.", "type" : "", "begin" : 37, "end" : 40}
	],
	"NE" : [
		{"id" : 0, "text" : "구한말", "type" : "DT_DYNASTY", "begin" : 3, "end" : 4, "weight" : 0.461897, "common_noun" : 0},
		{"id" : 1, "text" : "충청도", "type" : "LCP_PROVINCE", "begin" : 5, "end" : 5, "weight" : 0.574466, "common_noun" : 0},
		{"id" : 2, "text" : "농민", "type" : "CV_OCCUPATION", "begin" : 11, "end" : 11, "weight" : 0.447428, "common_noun" : 0},
		{"id" : 3, "text" : "부자", "type" : "CV_RELATION", "begin" : 15, "end" : 15, "weight" : 0.490614, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이", "head" : 1, "label" : "DP", "mod" : [], "weight" : 0.986191 },
		{"id" : 1, "text" : "단체는", "head" : 17, "label" : "NP_SBJ", "mod" : [0], "weight" : 0.776362 },
		{"id" : 2, "text" : "구한말", "head" : 15, "label" : "NP_AJT", "mod" : [], "weight" : 0.546523 },
		{"id" : 3, "text" : "충청도", "head" : 4, "label" : "NP", "mod" : [], "weight" : 0.910373 },
		{"id" : 4, "text" : "지방에서", "head" : 5, "label" : "NP_AJT", "mod" : [3], "weight" : 0.645359 },
		{"id" : 5, "text" : "봉기한", "head" : 7, "label" : "VP_MOD", "mod" : [4], "weight" : 0.777965 },
		{"id" : 6, "text" : "농민", "head" : 7, "label" : "NP", "mod" : [], "weight" : 0.837641 },
		{"id" : 7, "text" : "집단으로", "head" : 10, "label" : "NP_AJT", "mod" : [5, 6], "weight" : 0.550612 },
		{"id" : 8, "text" : "주로", "head" : 10, "label" : "AP", "mod" : [], "weight" : 0.507315 },
		{"id" : 9, "text" : "부자들을", "head" : 10, "label" : "NP_OBJ", "mod" : [], "weight" : 0.657117 },
		{"id" : 10, "text" : "약탈해", "head" : 11, "label" : "VP", "mod" : [7, 8, 9], "weight" : 0.659932 },
		{"id" : 11, "text" : "빼앗은", "head" : 12, "label" : "VP_MOD", "mod" : [10], "weight" : 0.805409 },
		{"id" : 12, "text" : "재산을", "head" : 15, "label" : "NP_OBJ", "mod" : [11], "weight" : 0.792774 },
		{"id" : 13, "text" : "가난한", "head" : 14, "label" : "VP_MOD", "mod" : [], "weight" : 0.877223 },
		{"id" : 14, "text" : "사람들에게", "head" : 15, "label" : "NP_AJT", "mod" : [13], "weight" : 0.815604 },
		{"id" : 15, "text" : "나눠주는", "head" : 16, "label" : "VP_MOD", "mod" : [2, 12, 14], "weight" : 0.835987 },
		{"id" : 16, "text" : "활동을", "head" : 17, "label" : "NP_OBJ", "mod" : [15], "weight" : 0.608533 },
		{"id" : 17, "text" : "벌였다.", "head" : -1, "label" : "VP", "mod" : [1, 16], "weight" : 0.00325916 }
	],
	"SRL" : [
		{"verb" : "봉기", "sense" : 1, "word_id" : 5, "weight" : 0.223784,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 2, "text" : "구한말", "weight" : 0.301403 },
				{"type" : "ARGM-LOC", "word_id" : 4, "text" : "지방에서", "weight" : 0.15878 },
				{"type" : "ARG1", "word_id" : 7, "text" : "집단으로", "weight" : 0.211168 }
			] },
		{"verb" : "약탈", "sense" : 1, "word_id" : 10, "weight" : 0.214099,
			"argument" : [
				{"type" : "ARGM-ADV", "word_id" : 8, "text" : "주로", "weight" : 0.206814 },
				{"type" : "ARG1", "word_id" : 9, "text" : "부자들을", "weight" : 0.221384 }
			] },
		{"verb" : "가난", "sense" : 1, "word_id" : 13, "weight" : 0.235152,
			"argument" : [
				{"type" : "ARG1", "word_id" : 14, "text" : "사람들에게", "weight" : 0.235152 }
			] },
		{"verb" : "나누", "sense" : 1, "word_id" : 15, "weight" : 0.292436,
			"argument" : [
				{"type" : "ARG1", "word_id" : 12, "text" : "재산을", "weight" : 0.303251 },
				{"type" : "ARG2", "word_id" : 14, "text" : "사람들에게", "weight" : 0.281621 }
			] },
		{"verb" : "벌이", "sense" : 1, "word_id" : 17, "weight" : 0.242646,
			"argument" : [
				{"type" : "ARG0", "word_id" : 1, "text" : "단체는", "weight" : 0.258161 },
				{"type" : "ARG1", "word_id" : 16, "text" : "활동을", "weight" : 0.227131 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 10, "ant_sid" : 0, "ant_wid" : 1, "type" : "s", "istitle" : 0, "weight" : 0.488569 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "정부에 의해 세력이 약화된 후 일부가 의병 활동에 가담해 반일 무장투쟁에 참여하기도 한 이 단체는 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "정부", "type" : "NNG", "position" : 186, "weight" : 0.437887 },
		{"id" : 1, "lemma" : "에", "type" : "JKB", "position" : 192, "weight" : 0.153364 },
		{"id" : 2, "lemma" : "의하", "type" : "VV", "position" : 196, "weight" : 0.9 },
		{"id" : 3, "lemma" : "어", "type" : "EC", "position" : 199, "weight" : 0.41831 },
		{"id" : 4, "lemma" : "세력", "type" : "NNG", "position" : 203, "weight" : 0.9 },
		{"id" : 5, "lemma" : "이", "type" : "JKS", "position" : 209, "weight" : 0.0360723 },
		{"id" : 6, "lemma" : "약화", "type" : "NNG", "position" : 213, "weight" : 0.9 },
		{"id" : 7, "lemma" : "되", "type" : "XSV", "position" : 219, "weight" : 0.000224177 },
		{"id" : 8, "lemma" : "ㄴ", "type" : "ETM", "position" : 219, "weight" : 0.392321 },
		{"id" : 9, "lemma" : "후", "type" : "NNG", "position" : 223, "weight" : 0.655812 },
		{"id" : 10, "lemma" : "일부", "type" : "NNG", "position" : 227, "weight" : 0.184806 },
		{"id" : 11, "lemma" : "가", "type" : "JKS", "position" : 233, "weight" : 0.066324 },
		{"id" : 12, "lemma" : "의병", "type" : "NNG", "position" : 237, "weight" : 0.9 },
		{"id" : 13, "lemma" : "활동", "type" : "NNG", "position" : 244, "weight" : 0.9 },
		{"id" : 14, "lemma" : "에", "type" : "JKB", "position" : 250, "weight" : 0.153364 },
		{"id" : 15, "lemma" : "가담", "type" : "NNG", "position" : 254, "weight" : 0.9 },
		{"id" : 16, "lemma" : "하", "type" : "XSV", "position" : 260, "weight" : 0.0001 },
		{"id" : 17, "lemma" : "어", "type" : "EC", "position" : 260, "weight" : 0.361326 },
		{"id" : 18, "lemma" : "반일", "type" : "NNG", "position" : 264, "weight" : 0.9 },
		{"id" : 19, "lemma" : "무장", "type" : "NNG", "position" : 271, "weight" : 0.183398 },
		{"id" : 20, "lemma" : "투쟁", "type" : "NNG", "position" : 277, "weight" : 0.9 },
		{"id" : 21, "lemma" : "에", "type" : "JKB", "position" : 283, "weight" : 0.153364 },
		{"id" : 22, "lemma" : "참여", "type" : "NNG", "position" : 287, "weight" : 0.9 },
		{"id" : 23, "lemma" : "하", "type" : "XSV", "position" : 293, "weight" : 0.0001 },
		{"id" : 24, "lemma" : "기", "type" : "ETN", "position" : 296, "weight" : 0.0556099 },
		{"id" : 25, "lemma" : "도", "type" : "JX", "position" : 299, "weight" : 0.147715 },
		{"id" : 26, "lemma" : "한", "type" : "MM", "position" : 303, "weight" : 0.0384747 },
		{"id" : 27, "lemma" : "이", "type" : "MM", "position" : 307, "weight" : 0.000595254 },
		{"id" : 28, "lemma" : "단체", "type" : "NNG", "position" : 311, "weight" : 0.737327 },
		{"id" : 29, "lemma" : "는", "type" : "JX", "position" : 317, "weight" : 0.0287565 },
		{"id" : 30, "lemma" : "무엇", "type" : "NP", "position" : 321, "weight" : 0.9 },
		{"id" : 31, "lemma" : "이", "type" : "VCP", "position" : 327, "weight" : 0.0175768 },
		{"id" : 32, "lemma" : "ㄹ까", "type" : "EF", "position" : 327, "weight" : 0.258243 },
		{"id" : 33, "lemma" : "?", "type" : "SF", "position" : 333, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "정부/NNG+에/JKB", "target" : "정부에", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "의하/VV+어/EC", "target" : "의해", "word_id" : 1, "m_begin" : 2, "m_end" : 3},
		{"id" : 2, "result" : "세력/NNG+이/JKS", "target" : "세력이", "word_id" : 2, "m_begin" : 4, "m_end" : 5},
		{"id" : 3, "result" : "약화되/VV+ㄴ/ETM", "target" : "약화된", "word_id" : 3, "m_begin" : 6, "m_end" : 8},
		{"id" : 4, "result" : "후/NNG", "target" : "후", "word_id" : 4, "m_begin" : 9, "m_end" : 9},
		{"id" : 5, "result" : "일부/NNG+가/JKS", "target" : "일부가", "word_id" : 5, "m_begin" : 10, "m_end" : 11},
		{"id" : 6, "result" : "의병/NNG", "target" : "의병", "word_id" : 6, "m_begin" : 12, "m_end" : 12},
		{"id" : 7, "result" : "활동/NNG+에/JKB", "target" : "활동에", "word_id" : 7, "m_begin" : 13, "m_end" : 14},
		{"id" : 8, "result" : "가담하/VV+어/EC", "target" : "가담해", "word_id" : 8, "m_begin" : 15, "m_end" : 17},
		{"id" : 9, "result" : "반일/NNG", "target" : "반일", "word_id" : 9, "m_begin" : 18, "m_end" : 18},
		{"id" : 10, "result" : "무장투쟁/NNG+에/JKB", "target" : "무장투쟁에", "word_id" : 10, "m_begin" : 19, "m_end" : 21},
		{"id" : 11, "result" : "참여하/VV+기/ETN+도/JX", "target" : "참여하기도", "word_id" : 11, "m_begin" : 22, "m_end" : 25},
		{"id" : 12, "result" : "한/MM", "target" : "한", "word_id" : 12, "m_begin" : 26, "m_end" : 26},
		{"id" : 13, "result" : "이/MM", "target" : "이", "word_id" : 13, "m_begin" : 27, "m_end" : 27},
		{"id" : 14, "result" : "단체/NNG+는/JX", "target" : "단체는", "word_id" : 14, "m_begin" : 28, "m_end" : 29},
		{"id" : 15, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 15, "m_begin" : 30, "m_end" : 33}
	],
	"WSD" : [
		{"id" : 0, "text" : "정부", "type" : "NNG", "scode" : "08", "weight" : 1, "position" : 186, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 192, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "의하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 196, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 199, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "세력", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 203, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 209, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "약화되", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 213, "begin" : 6, "end" : 7},
		{"id" : 7, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 219, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "후", "type" : "NNG", "scode" : "08", "weight" : 1, "position" : 223, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "일부", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 227, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "가", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 233, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "의병", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 237, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "활동", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 244, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 250, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "가담하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 254, "begin" : 15, "end" : 16},
		{"id" : 15, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 260, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : "반일", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 264, "begin" : 18, "end" : 18},
		{"id" : 17, "text" : "무장투쟁", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 271, "begin" : 19, "end" : 20},
		{"id" : 18, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 283, "begin" : 21, "end" : 21},
		{"id" : 19, "text" : "참여하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 287, "begin" : 22, "end" : 23},
		{"id" : 20, "text" : "기", "type" : "ETN", "scode" : "00", "weight" : 1, "position" : 296, "begin" : 24, "end" : 24},
		{"id" : 21, "text" : "도", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 299, "begin" : 25, "end" : 25},
		{"id" : 22, "text" : "한", "type" : "MM", "scode" : "01", "weight" : 1, "position" : 303, "begin" : 26, "end" : 26},
		{"id" : 23, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 307, "begin" : 27, "end" : 27},
		{"id" : 24, "text" : "단체", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 311, "begin" : 28, "end" : 28},
		{"id" : 25, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 317, "begin" : 29, "end" : 29},
		{"id" : 26, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 321, "begin" : 30, "end" : 30},
		{"id" : 27, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 327, "begin" : 31, "end" : 31},
		{"id" : 28, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 327, "begin" : 32, "end" : 32},
		{"id" : 29, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 333, "begin" : 33, "end" : 33}
	],
	"word" : [
		{"id" : 0, "text" : "정부에", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "의해", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 2, "text" : "세력이", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 3, "text" : "약화된", "type" : "", "begin" : 6, "end" : 8},
		{"id" : 4, "text" : "후", "type" : "", "begin" : 9, "end" : 9},
		{"id" : 5, "text" : "일부가", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 6, "text" : "의병", "type" : "", "begin" : 12, "end" : 12},
		{"id" : 7, "text" : "활동에", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 8, "text" : "가담해", "type" : "", "begin" : 15, "end" : 17},
		{"id" : 9, "text" : "반일", "type" : "", "begin" : 18, "end" : 18},
		{"id" : 10, "text" : "무장투쟁에", "type" : "", "begin" : 19, "end" : 21},
		{"id" : 11, "text" : "참여하기도", "type" : "", "begin" : 22, "end" : 25},
		{"id" : 12, "text" : "한", "type" : "", "begin" : 26, "end" : 26},
		{"id" : 13, "text" : "이", "type" : "", "begin" : 27, "end" : 27},
		{"id" : 14, "text" : "단체는", "type" : "", "begin" : 28, "end" : 29},
		{"id" : 15, "text" : "무엇일까?", "type" : "", "begin" : 30, "end" : 33}
	],
	"NE" : [
		{"id" : 0, "text" : "정부", "type" : "OGG_POLITICS", "begin" : 0, "end" : 0, "weight" : 0.245708, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "정부에", "head" : 1, "label" : "NP_AJT", "mod" : [], "weight" : 0.998321 },
		{"id" : 1, "text" : "의해", "head" : 3, "label" : "VP", "mod" : [0], "weight" : 0.678233 },
		{"id" : 2, "text" : "세력이", "head" : 3, "label" : "NP_SBJ", "mod" : [], "weight" : 0.989705 },
		{"id" : 3, "text" : "약화된", "head" : 4, "label" : "VP_MOD", "mod" : [1, 2], "weight" : 0.900921 },
		{"id" : 4, "text" : "후", "head" : 8, "label" : "NP_AJT", "mod" : [3], "weight" : 0.708552 },
		{"id" : 5, "text" : "일부가", "head" : 8, "label" : "NP_SBJ", "mod" : [], "weight" : 0.714308 },
		{"id" : 6, "text" : "의병", "head" : 7, "label" : "NP", "mod" : [], "weight" : 0.99962 },
		{"id" : 7, "text" : "활동에", "head" : 8, "label" : "NP_AJT", "mod" : [6], "weight" : 0.741039 },
		{"id" : 8, "text" : "가담해", "head" : 11, "label" : "VP", "mod" : [4, 5, 7], "weight" : 0.800692 },
		{"id" : 9, "text" : "반일", "head" : 10, "label" : "NP", "mod" : [], "weight" : 0.723579 },
		{"id" : 10, "text" : "무장투쟁에", "head" : 11, "label" : "NP_AJT", "mod" : [9], "weight" : 0.828858 },
		{"id" : 11, "text" : "참여하기도", "head" : 12, "label" : "VP", "mod" : [8, 10], "weight" : 0.924377 },
		{"id" : 12, "text" : "한", "head" : 14, "label" : "VP_MOD", "mod" : [11], "weight" : 0.973202 },
		{"id" : 13, "text" : "이", "head" : 14, "label" : "DP", "mod" : [], "weight" : 0.757608 },
		{"id" : 14, "text" : "단체는", "head" : 15, "label" : "NP_SBJ", "mod" : [12, 13], "weight" : 0.712768 },
		{"id" : 15, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [14], "weight" : 0.0377994 }
	],
	"SRL" : [
		{"verb" : "의하", "sense" : 1, "word_id" : 1, "weight" : 0.282727,
			"argument" : [
				{"type" : "ARG2", "word_id" : 0, "text" : "정부에", "weight" : 0.282727 }
			] },
		{"verb" : "약화", "sense" : 1, "word_id" : 3, "weight" : 0.181334,
			"argument" : [
				{"type" : "ARGM-CAU", "word_id" : 1, "text" : "의해", "weight" : 0.179476 },
				{"type" : "ARG1", "word_id" : 2, "text" : "세력이", "weight" : 0.183192 }
			] },
		{"verb" : "가담", "sense" : 1, "word_id" : 8, "weight" : 0.265436,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 4, "text" : "후", "weight" : 0.337343 },
				{"type" : "ARG0", "word_id" : 5, "text" : "일부가", "weight" : 0.271307 },
				{"type" : "ARG1", "word_id" : 7, "text" : "활동에", "weight" : 0.187656 }
			] },
		{"verb" : "참여", "sense" : 1, "word_id" : 11, "weight" : 0.199505,
			"argument" : [
				{"type" : "ARGM-PRD", "word_id" : 8, "text" : "가담해", "weight" : 0.152068 },
				{"type" : "ARG1", "word_id" : 10, "text" : "무장투쟁에", "weight" : 0.190484 },
				{"type" : "ARG0", "word_id" : 14, "text" : "단체는", "weight" : 0.255964 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 11, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.308367 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 1, "ne_id" : -1, "text" : "이 단체", "start_eid_short" : 0, "end_eid_short" : 1, "text_short" : "이 단체", "weight" : 0.012 },
		{"id" : 12, "sent_id" : 1, "start_eid" : 2, "end_eid" : 2, "ne_id" : -1, "text" : "세력", "start_eid_short" : 2, "end_eid_short" : 2, "text_short" : "세력", "weight" : 0.002 },
		{"id" : 15, "sent_id" : 1, "start_eid" : 5, "end_eid" : 5, "ne_id" : -1, "text" : "일부", "start_eid_short" : 5, "end_eid_short" : 5, "text_short" : "일부", "weight" : 0.008 },
		{"id" : 20, "sent_id" : 1, "start_eid" : 13, "end_eid" : 14, "ne_id" : -1, "text" : "이 단체", "start_eid_short" : 13, "end_eid_short" : 14, "text_short" : "이 단체", "weight" : 0.018 },
		{"id" : 22, "sent_id" : 1, "start_eid" : 15, "end_eid" : 15, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 15, "end_eid_short" : 15, "text_short" : "무엇이ㄹ까?", "weight" : 0.018 }
	] }
 ]
}

