{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿아리스토텔레스의 <시학>에서 제시한 '삼일치의 법칙'은 17~18세기 프랑스 고전주의 연극의 기본 법칙이다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "SS", "position" : 0, "weight" : 1 },
		{"id" : 1, "lemma" : "아리스토텔레스", "type" : "NNP", "position" : 3, "weight" : 0.9 },
		{"id" : 2, "lemma" : "의", "type" : "JKG", "position" : 24, "weight" : 0.0987295 },
		{"id" : 3, "lemma" : "<", "type" : "SS", "position" : 28, "weight" : 1 },
		{"id" : 4, "lemma" : "시학", "type" : "NNG", "position" : 29, "weight" : 0.150589 },
		{"id" : 5, "lemma" : ">", "type" : "SS", "position" : 35, "weight" : 1 },
		{"id" : 6, "lemma" : "에서", "type" : "JKB", "position" : 36, "weight" : 0.0486066 },
		{"id" : 7, "lemma" : "제시", "type" : "NNG", "position" : 43, "weight" : 0.134207 },
		{"id" : 8, "lemma" : "하", "type" : "XSV", "position" : 49, "weight" : 0.0001 },
		{"id" : 9, "lemma" : "ㄴ", "type" : "ETM", "position" : 49, "weight" : 0.392321 },
		{"id" : 10, "lemma" : "'", "type" : "SS", "position" : 53, "weight" : 1 },
		{"id" : 11, "lemma" : "삼일", "type" : "NNG", "position" : 54, "weight" : 0.150001 },
		{"id" : 12, "lemma" : "치", "type" : "XSN", "position" : 60, "weight" : 0.0010799 },
		{"id" : 13, "lemma" : "의", "type" : "JKG", "position" : 63, "weight" : 0.120732 },
		{"id" : 14, "lemma" : "법칙", "type" : "NNG", "position" : 67, "weight" : 0.9 },
		{"id" : 15, "lemma" : "'", "type" : "SS", "position" : 73, "weight" : 1 },
		{"id" : 16, "lemma" : "은", "type" : "JX", "position" : 74, "weight" : 0.0128817 },
		{"id" : 17, "lemma" : "17", "type" : "SN", "position" : 78, "weight" : 1 },
		{"id" : 18, "lemma" : "~", "type" : "SO", "position" : 80, "weight" : 1 },
		{"id" : 19, "lemma" : "18", "type" : "SN", "position" : 81, "weight" : 1 },
		{"id" : 20, "lemma" : "세기", "type" : "NNG", "position" : 83, "weight" : 0.100259 },
		{"id" : 21, "lemma" : "프랑스", "type" : "NNP", "position" : 90, "weight" : 0.00710219 },
		{"id" : 22, "lemma" : "고전", "type" : "NNG", "position" : 100, "weight" : 0.281597 },
		{"id" : 23, "lemma" : "주의", "type" : "NNG", "position" : 106, "weight" : 0.9 },
		{"id" : 24, "lemma" : "연극", "type" : "NNG", "position" : 113, "weight" : 0.18477 },
		{"id" : 25, "lemma" : "의", "type" : "JKG", "position" : 119, "weight" : 0.0694213 },
		{"id" : 26, "lemma" : "기본", "type" : "NNG", "position" : 123, "weight" : 0.9 },
		{"id" : 27, "lemma" : "법칙", "type" : "NNG", "position" : 130, "weight" : 0.9 },
		{"id" : 28, "lemma" : "이", "type" : "VCP", "position" : 136, "weight" : 0.0177525 },
		{"id" : 29, "lemma" : "다", "type" : "EF", "position" : 139, "weight" : 0.353579 },
		{"id" : 30, "lemma" : ".", "type" : "SF", "position" : 142, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿/SS+아리스토텔레스/NNG+의/JKG", "target" : "﻿아리스토텔레스의", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "</SS+시학/NNG+>/SS+에서/JKB", "target" : "<시학>에서", "word_id" : 1, "m_begin" : 3, "m_end" : 6},
		{"id" : 2, "result" : "제시하/VV+ㄴ/ETM", "target" : "제시한", "word_id" : 2, "m_begin" : 7, "m_end" : 9},
		{"id" : 3, "result" : "'/SS+삼일치/NNG+의/JKG", "target" : "'삼일치의", "word_id" : 3, "m_begin" : 10, "m_end" : 13},
		{"id" : 4, "result" : "법칙/NNG+'/SS+은/JX", "target" : "법칙'은", "word_id" : 4, "m_begin" : 14, "m_end" : 16},
		{"id" : 5, "result" : "17/SN+~/SO+18/SN+세기/NNG", "target" : "17~18세기", "word_id" : 5, "m_begin" : 17, "m_end" : 20},
		{"id" : 6, "result" : "프랑스/NNG", "target" : "프랑스", "word_id" : 6, "m_begin" : 21, "m_end" : 21},
		{"id" : 7, "result" : "고전주의/NNG", "target" : "고전주의", "word_id" : 7, "m_begin" : 22, "m_end" : 23},
		{"id" : 8, "result" : "연극/NNG+의/JKG", "target" : "연극의", "word_id" : 8, "m_begin" : 24, "m_end" : 25},
		{"id" : 9, "result" : "기본/NNG", "target" : "기본", "word_id" : 9, "m_begin" : 26, "m_end" : 26},
		{"id" : 10, "result" : "법칙/NNG+이/VCP+다/EF+./SF", "target" : "법칙이다.", "word_id" : 10, "m_begin" : 27, "m_end" : 30}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "아리스토텔레스", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 24, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "<", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 28, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "시학", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 29, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : ">", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 35, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 36, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "제시하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 43, "begin" : 7, "end" : 8},
		{"id" : 8, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 49, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 53, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "삼일", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 54, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "치", "type" : "XSN", "scode" : "18", "weight" : 1, "position" : 60, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 63, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "법칙", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 67, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 73, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 74, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "17", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 78, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "~", "type" : "SO", "scode" : "00", "weight" : 1, "position" : 80, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "18", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 81, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "세기", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 83, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "프랑스", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 90, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : "고전주의", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 100, "begin" : 22, "end" : 23},
		{"id" : 22, "text" : "연극", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 113, "begin" : 24, "end" : 24},
		{"id" : 23, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 119, "begin" : 25, "end" : 25},
		{"id" : 24, "text" : "기본", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 123, "begin" : 26, "end" : 26},
		{"id" : 25, "text" : "법칙", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 130, "begin" : 27, "end" : 27},
		{"id" : 26, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 136, "begin" : 28, "end" : 28},
		{"id" : 27, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 139, "begin" : 29, "end" : 29},
		{"id" : 28, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 142, "begin" : 30, "end" : 30}
	],
	"word" : [
		{"id" : 0, "text" : "﻿아리스토텔레스의", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "<시학>에서", "type" : "", "begin" : 3, "end" : 6},
		{"id" : 2, "text" : "제시한", "type" : "", "begin" : 7, "end" : 9},
		{"id" : 3, "text" : "'삼일치의", "type" : "", "begin" : 10, "end" : 13},
		{"id" : 4, "text" : "법칙'은", "type" : "", "begin" : 14, "end" : 16},
		{"id" : 5, "text" : "17~18세기", "type" : "", "begin" : 17, "end" : 20},
		{"id" : 6, "text" : "프랑스", "type" : "", "begin" : 21, "end" : 21},
		{"id" : 7, "text" : "고전주의", "type" : "", "begin" : 22, "end" : 23},
		{"id" : 8, "text" : "연극의", "type" : "", "begin" : 24, "end" : 25},
		{"id" : 9, "text" : "기본", "type" : "", "begin" : 26, "end" : 26},
		{"id" : 10, "text" : "법칙이다.", "type" : "", "begin" : 27, "end" : 30}
	],
	"NE" : [
		{"id" : 0, "text" : "아리스토텔레스", "type" : "PS_NAME", "begin" : 1, "end" : 1, "weight" : 0.24609, "common_noun" : 0},
		{"id" : 1, "text" : "시학", "type" : "FD_ART", "begin" : 4, "end" : 4, "weight" : 0.216816, "common_noun" : 0},
		{"id" : 2, "text" : "삼일치의 법칙", "type" : "AF_WORKS", "begin" : 11, "end" : 14, "weight" : 0.307036, "common_noun" : 0},
		{"id" : 3, "text" : "17~18세기", "type" : "DT_DURATION", "begin" : 17, "end" : 20, "weight" : 0.839063, "common_noun" : 0},
		{"id" : 4, "text" : "프랑스", "type" : "LCP_COUNTRY", "begin" : 21, "end" : 21, "weight" : 0.239843, "common_noun" : 0},
		{"id" : 5, "text" : "고전주의", "type" : "TR_ART", "begin" : 22, "end" : 23, "weight" : 0.389492, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿아리스토텔레스의", "head" : 1, "label" : "NP_MOD", "mod" : [], "weight" : 0.921426 },
		{"id" : 1, "text" : "<시학>에서", "head" : 2, "label" : "NP_AJT", "mod" : [0], "weight" : 0.805798 },
		{"id" : 2, "text" : "제시한", "head" : 4, "label" : "VP_MOD", "mod" : [1], "weight" : 0.911746 },
		{"id" : 3, "text" : "'삼일치의", "head" : 4, "label" : "NP_MOD", "mod" : [], "weight" : 0.809367 },
		{"id" : 4, "text" : "법칙'은", "head" : 10, "label" : "NP_SBJ", "mod" : [2, 3], "weight" : 0.656641 },
		{"id" : 5, "text" : "17~18세기", "head" : 8, "label" : "NP", "mod" : [], "weight" : 0.745744 },
		{"id" : 6, "text" : "프랑스", "head" : 7, "label" : "NP", "mod" : [], "weight" : 0.745745 },
		{"id" : 7, "text" : "고전주의", "head" : 8, "label" : "NP", "mod" : [6], "weight" : 0.835735 },
		{"id" : 8, "text" : "연극의", "head" : 10, "label" : "NP_MOD", "mod" : [5, 7], "weight" : 0.510489 },
		{"id" : 9, "text" : "기본", "head" : 10, "label" : "NP", "mod" : [], "weight" : 0.404121 },
		{"id" : 10, "text" : "법칙이다.", "head" : -1, "label" : "VNP", "mod" : [4, 8, 9], "weight" : 0.0211289 }
	],
	"SRL" : [
		{"verb" : "제시", "sense" : 1, "word_id" : 2, "weight" : 0.227031,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 1, "text" : "<시학>에서", "weight" : 0.189089 },
				{"type" : "ARG1", "word_id" : 4, "text" : "법칙'은", "weight" : 0.264974 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "'삼일치의 법칙'에 해당하지 않는 것은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "'", "type" : "SS", "position" : 144, "weight" : 1 },
		{"id" : 1, "lemma" : "삼일", "type" : "NNG", "position" : 145, "weight" : 0.150001 },
		{"id" : 2, "lemma" : "치", "type" : "XSN", "position" : 151, "weight" : 0.0010799 },
		{"id" : 3, "lemma" : "의", "type" : "JKG", "position" : 154, "weight" : 0.120732 },
		{"id" : 4, "lemma" : "법칙", "type" : "NNG", "position" : 158, "weight" : 0.9 },
		{"id" : 5, "lemma" : "'", "type" : "SS", "position" : 164, "weight" : 1 },
		{"id" : 6, "lemma" : "에", "type" : "JKB", "position" : 165, "weight" : 0.048593 },
		{"id" : 7, "lemma" : "해당", "type" : "NNG", "position" : 169, "weight" : 0.9 },
		{"id" : 8, "lemma" : "하", "type" : "XSV", "position" : 175, "weight" : 0.0001 },
		{"id" : 9, "lemma" : "지", "type" : "EC", "position" : 178, "weight" : 0.260723 },
		{"id" : 10, "lemma" : "않", "type" : "VX", "position" : 182, "weight" : 0.256301 },
		{"id" : 11, "lemma" : "는", "type" : "ETM", "position" : 185, "weight" : 0.183966 },
		{"id" : 12, "lemma" : "것", "type" : "NNB", "position" : 189, "weight" : 0.228788 },
		{"id" : 13, "lemma" : "은", "type" : "JX", "position" : 192, "weight" : 0.0688243 },
		{"id" : 14, "lemma" : "무엇", "type" : "NP", "position" : 196, "weight" : 0.9 },
		{"id" : 15, "lemma" : "이", "type" : "VCP", "position" : 202, "weight" : 0.0175768 },
		{"id" : 16, "lemma" : "ㄹ까", "type" : "EF", "position" : 202, "weight" : 0.258243 },
		{"id" : 17, "lemma" : "?", "type" : "SF", "position" : 208, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "'/SS+삼일치/NNG+의/JKG", "target" : "'삼일치의", "word_id" : 0, "m_begin" : 0, "m_end" : 3},
		{"id" : 1, "result" : "법칙/NNG+'/SS+에/JKB", "target" : "법칙'에", "word_id" : 1, "m_begin" : 4, "m_end" : 6},
		{"id" : 2, "result" : "해당하/VV+지/EC", "target" : "해당하지", "word_id" : 2, "m_begin" : 7, "m_end" : 9},
		{"id" : 3, "result" : "않/VX+는/ETM", "target" : "않는", "word_id" : 3, "m_begin" : 10, "m_end" : 11},
		{"id" : 4, "result" : "것/NNB+은/JX", "target" : "것은", "word_id" : 4, "m_begin" : 12, "m_end" : 13},
		{"id" : 5, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 5, "m_begin" : 14, "m_end" : 17}
	],
	"WSD" : [
		{"id" : 0, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 144, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "삼일", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 145, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "치", "type" : "XSN", "scode" : "18", "weight" : 1, "position" : 151, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 154, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "법칙", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 158, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 164, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 165, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "해당하", "type" : "VV", "scode" : "04", "weight" : 1, "position" : 169, "begin" : 7, "end" : 8},
		{"id" : 8, "text" : "지", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 178, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "않", "type" : "VX", "scode" : "00", "weight" : 1, "position" : 182, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 185, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "것", "type" : "NNB", "scode" : "01", "weight" : 1, "position" : 189, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 192, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 196, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 202, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 202, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 208, "begin" : 17, "end" : 17}
	],
	"word" : [
		{"id" : 0, "text" : "'삼일치의", "type" : "", "begin" : 0, "end" : 3},
		{"id" : 1, "text" : "법칙'에", "type" : "", "begin" : 4, "end" : 6},
		{"id" : 2, "text" : "해당하지", "type" : "", "begin" : 7, "end" : 9},
		{"id" : 3, "text" : "않는", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 4, "text" : "것은", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 5, "text" : "무엇일까?", "type" : "", "begin" : 14, "end" : 17}
	],
	"NE" : [
		{"id" : 0, "text" : "삼일치의 법칙", "type" : "AF_WORKS", "begin" : 1, "end" : 4, "weight" : 0.288374, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "'삼일치의", "head" : 1, "label" : "NP_MOD", "mod" : [], "weight" : 0.896859 },
		{"id" : 1, "text" : "법칙'에", "head" : 2, "label" : "NP_AJT", "mod" : [0], "weight" : 0.852459 },
		{"id" : 2, "text" : "해당하지", "head" : 3, "label" : "VP", "mod" : [1], "weight" : 0.946367 },
		{"id" : 3, "text" : "않는", "head" : 4, "label" : "VP_MOD", "mod" : [2], "weight" : 0.795087 },
		{"id" : 4, "text" : "것은", "head" : 5, "label" : "NP_SBJ", "mod" : [3], "weight" : 0.659516 },
		{"id" : 5, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [4], "weight" : 0.260714 }
	],
	"SRL" : [
		{"verb" : "해당", "sense" : 1, "word_id" : 2, "weight" : 0.305817,
			"argument" : [
				{"type" : "ARG2", "word_id" : 1, "text" : "법칙'에", "weight" : 0.261774 },
				{"type" : "ARGM-NEG", "word_id" : 3, "text" : "않는", "weight" : 0.349859 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "FD_ART", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 1, "sent_id" : 0, "start_eid" : 1, "end_eid" : 1, "ne_id" : 1, "text" : "<시학>", "start_eid_short" : 1, "end_eid_short" : 1, "text_short" : "<시학>", "weight" : 0.002 },
		{"id" : 10, "sent_id" : 1, "start_eid" : 4, "end_eid" : 5, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 5, "end_eid_short" : 5, "text_short" : "무엇이ㄹ까?", "weight" : 0.006 }
	] },
	{"id" : 1, "type" : "AF_WORKS", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 9, "sent_id" : 1, "start_eid" : 0, "end_eid" : 1, "ne_id" : 0, "text" : "'삼일치의 법칙'", "start_eid_short" : 0, "end_eid_short" : 1, "text_short" : "'삼일치의 법칙'", "weight" : 0.016 },
		{"id" : 2, "sent_id" : 0, "start_eid" : 3, "end_eid" : 4, "ne_id" : 2, "text" : "'삼일치의 법칙'", "start_eid_short" : 3, "end_eid_short" : 4, "text_short" : "'삼일치의 법칙'", "weight" : 0.01 }
	] }
 ]
}

