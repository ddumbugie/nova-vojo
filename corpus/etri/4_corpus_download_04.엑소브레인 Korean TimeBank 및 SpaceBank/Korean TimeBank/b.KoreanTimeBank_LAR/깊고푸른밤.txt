{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "《깊고 푸른 밤》은 최인호의 소설이다.",
	"morp" : [
		{"id" : 0, "lemma" : "《", "type" : "SS", "position" : 0, "weight" : 1 },
		{"id" : 1, "lemma" : "깊", "type" : "VA", "position" : 3, "weight" : 0.9 },
		{"id" : 2, "lemma" : "고", "type" : "EC", "position" : 6, "weight" : 0.310376 },
		{"id" : 3, "lemma" : "푸르", "type" : "VA", "position" : 10, "weight" : 0.9 },
		{"id" : 4, "lemma" : "ㄴ", "type" : "ETM", "position" : 13, "weight" : 0.430446 },
		{"id" : 5, "lemma" : "밤", "type" : "NNG", "position" : 17, "weight" : 0.658489 },
		{"id" : 6, "lemma" : "》", "type" : "SS", "position" : 20, "weight" : 1 },
		{"id" : 7, "lemma" : "은", "type" : "JX", "position" : 23, "weight" : 0.0128817 },
		{"id" : 8, "lemma" : "최인호", "type" : "NNP", "position" : 27, "weight" : 0.9 },
		{"id" : 9, "lemma" : "의", "type" : "JKG", "position" : 36, "weight" : 0.0987295 },
		{"id" : 10, "lemma" : "소설", "type" : "NNG", "position" : 40, "weight" : 0.9 },
		{"id" : 11, "lemma" : "이", "type" : "VCP", "position" : 46, "weight" : 0.0177525 },
		{"id" : 12, "lemma" : "다", "type" : "EF", "position" : 49, "weight" : 0.353579 },
		{"id" : 13, "lemma" : ".", "type" : "SF", "position" : 52, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "《/SS+깊/VA+고/EC", "target" : "《깊고", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "푸르/VA+ㄴ/ETM", "target" : "푸른", "word_id" : 1, "m_begin" : 3, "m_end" : 4},
		{"id" : 2, "result" : "밤/NNG+》/SS+은/JX", "target" : "밤》은", "word_id" : 2, "m_begin" : 5, "m_end" : 7},
		{"id" : 3, "result" : "최인호/NNG+의/JKG", "target" : "최인호의", "word_id" : 3, "m_begin" : 8, "m_end" : 9},
		{"id" : 4, "result" : "소설/NNG+이/VCP+다/EF+./SF", "target" : "소설이다.", "word_id" : 4, "m_begin" : 10, "m_end" : 13}
	],
	"WSD" : [
		{"id" : 0, "text" : "《", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "깊", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 6, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "푸르", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 13, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "밤", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 17, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "》", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 20, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 23, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "최인호", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 27, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 36, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "소설", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 40, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 46, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 49, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 52, "begin" : 13, "end" : 13}
	],
	"word" : [
		{"id" : 0, "text" : "《깊고", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "푸른", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 2, "text" : "밤》은", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 3, "text" : "최인호의", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 4, "text" : "소설이다.", "type" : "", "begin" : 10, "end" : 13}
	],
	"NE" : [
		{"id" : 0, "text" : "깊고 푸른 밤", "type" : "AFW_DOCUMENT", "begin" : 1, "end" : 5, "weight" : 0.983945, "common_noun" : 0},
		{"id" : 1, "text" : "최인호", "type" : "PS_NAME", "begin" : 8, "end" : 8, "weight" : 0.574707, "common_noun" : 0},
		{"id" : 2, "text" : "소설", "type" : "FD_ART", "begin" : 10, "end" : 10, "weight" : 0.256173, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "《깊고", "head" : 1, "label" : "VP", "mod" : [], "weight" : 0.953322 },
		{"id" : 1, "text" : "푸른", "head" : 2, "label" : "VP_MOD", "mod" : [0], "weight" : 0.764659 },
		{"id" : 2, "text" : "밤》은", "head" : 4, "label" : "NP_SBJ", "mod" : [1], "weight" : 0.685586 },
		{"id" : 3, "text" : "최인호의", "head" : 4, "label" : "NP_MOD", "mod" : [], "weight" : 0.488757 },
		{"id" : 4, "text" : "소설이다.", "head" : -1, "label" : "VNP", "mod" : [2, 3], "weight" : 0.138833 }
	],
	"SRL" : [
		{"verb" : "푸르", "sense" : 1, "word_id" : 1, "weight" : 0.205,
			"argument" : [
				{"type" : "ARG1", "word_id" : 2, "text" : "밤》은", "weight" : 0.205 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 0, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 1 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "1984년 영화로도 만들어졌다.",
	"morp" : [
		{"id" : 0, "lemma" : "1984", "type" : "SN", "position" : 53, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 57, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "영화", "type" : "NNG", "position" : 61, "weight" : 0.109734 },
		{"id" : 3, "lemma" : "로", "type" : "JKB", "position" : 67, "weight" : 0.153229 },
		{"id" : 4, "lemma" : "도", "type" : "JX", "position" : 70, "weight" : 0.0912297 },
		{"id" : 5, "lemma" : "만들", "type" : "VV", "position" : 74, "weight" : 0.9 },
		{"id" : 6, "lemma" : "어", "type" : "EC", "position" : 80, "weight" : 0.41831 },
		{"id" : 7, "lemma" : "지", "type" : "VX", "position" : 83, "weight" : 0.0539764 },
		{"id" : 8, "lemma" : "었", "type" : "EP", "position" : 83, "weight" : 0.9 },
		{"id" : 9, "lemma" : "다", "type" : "EF", "position" : 86, "weight" : 0.640954 },
		{"id" : 10, "lemma" : ".", "type" : "SF", "position" : 89, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "1984/SN+년/NNB", "target" : "1984년", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "영화/NNG+로/JKB+도/JX", "target" : "영화로도", "word_id" : 1, "m_begin" : 2, "m_end" : 4},
		{"id" : 2, "result" : "만들/VV+어/EC+지/VX+었/EP+다/EF+./SF", "target" : "만들어졌다.", "word_id" : 2, "m_begin" : 5, "m_end" : 10}
	],
	"WSD" : [
		{"id" : 0, "text" : "1984", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 53, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 57, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "영화", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 61, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 67, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "도", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 70, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "만들", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 74, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 80, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "지", "type" : "VX", "scode" : "04", "weight" : 1, "position" : 83, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 83, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 86, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 89, "begin" : 10, "end" : 10}
	],
	"word" : [
		{"id" : 0, "text" : "1984년", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "영화로도", "type" : "", "begin" : 2, "end" : 4},
		{"id" : 2, "text" : "만들어졌다.", "type" : "", "begin" : 5, "end" : 10}
	],
	"NE" : [
		{"id" : 0, "text" : "1984년", "type" : "DT_YEAR", "begin" : 0, "end" : 1, "weight" : 0.429692, "common_noun" : 0},
		{"id" : 1, "text" : "영화", "type" : "FD_ART", "begin" : 2, "end" : 2, "weight" : 0.218813, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "1984년", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.565012 },
		{"id" : 1, "text" : "영화로도", "head" : 2, "label" : "NP_AJT", "mod" : [0], "weight" : 0.545126 },
		{"id" : 2, "text" : "만들어졌다.", "head" : -1, "label" : "VP", "mod" : [1], "weight" : 0.204829 }
	],
	"SRL" : [
		{"verb" : "만들", "sense" : 1, "word_id" : 2, "weight" : 0.106703,
			"argument" : [
				{"type" : "ARG1", "word_id" : 1, "text" : "영화로도", "weight" : 0.106703 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 2, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.733626 }
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "대마초를 피운 혐의로 한국 무대에서 쫓겨나 미국으로 가게된 가수 준호의 이야기이다.",
	"morp" : [
		{"id" : 0, "lemma" : "대마", "type" : "NNG", "position" : 90, "weight" : 0.421525 },
		{"id" : 1, "lemma" : "초", "type" : "NNG", "position" : 96, "weight" : 0.0244202 },
		{"id" : 2, "lemma" : "를", "type" : "JKO", "position" : 99, "weight" : 0.137686 },
		{"id" : 3, "lemma" : "피우", "type" : "VV", "position" : 103, "weight" : 0.775582 },
		{"id" : 4, "lemma" : "ㄴ", "type" : "ETM", "position" : 106, "weight" : 0.304215 },
		{"id" : 5, "lemma" : "혐의", "type" : "NNG", "position" : 110, "weight" : 0.9 },
		{"id" : 6, "lemma" : "로", "type" : "JKB", "position" : 116, "weight" : 0.153229 },
		{"id" : 7, "lemma" : "한국", "type" : "NNP", "position" : 120, "weight" : 0.0229938 },
		{"id" : 8, "lemma" : "무대", "type" : "NNG", "position" : 127, "weight" : 0.281882 },
		{"id" : 9, "lemma" : "에서", "type" : "JKB", "position" : 133, "weight" : 0.153407 },
		{"id" : 10, "lemma" : "쫓겨나", "type" : "VV", "position" : 140, "weight" : 0.9 },
		{"id" : 11, "lemma" : "아", "type" : "EC", "position" : 146, "weight" : 0.398809 },
		{"id" : 12, "lemma" : "미국", "type" : "NNP", "position" : 150, "weight" : 0.0219717 },
		{"id" : 13, "lemma" : "으로", "type" : "JKB", "position" : 156, "weight" : 0.0823853 },
		{"id" : 14, "lemma" : "가", "type" : "VV", "position" : 163, "weight" : 0.036078 },
		{"id" : 15, "lemma" : "게", "type" : "EC", "position" : 166, "weight" : 0.427975 },
		{"id" : 16, "lemma" : "되", "type" : "VV", "position" : 169, "weight" : 0.209958 },
		{"id" : 17, "lemma" : "ㄴ", "type" : "ETM", "position" : 169, "weight" : 0.304215 },
		{"id" : 18, "lemma" : "가수", "type" : "NNG", "position" : 173, "weight" : 0.9 },
		{"id" : 19, "lemma" : "준호", "type" : "NNP", "position" : 180, "weight" : 0.00678065 },
		{"id" : 20, "lemma" : "의", "type" : "JKG", "position" : 186, "weight" : 0.0987295 },
		{"id" : 21, "lemma" : "이야기", "type" : "NNG", "position" : 190, "weight" : 0.9 },
		{"id" : 22, "lemma" : "이", "type" : "VCP", "position" : 199, "weight" : 0.0177525 },
		{"id" : 23, "lemma" : "다", "type" : "EF", "position" : 202, "weight" : 0.353579 },
		{"id" : 24, "lemma" : ".", "type" : "SF", "position" : 205, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "대마초/NNG+를/JKO", "target" : "대마초를", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "피우/VV+ㄴ/ETM", "target" : "피운", "word_id" : 1, "m_begin" : 3, "m_end" : 4},
		{"id" : 2, "result" : "혐의/NNG+로/JKB", "target" : "혐의로", "word_id" : 2, "m_begin" : 5, "m_end" : 6},
		{"id" : 3, "result" : "한국/NNG", "target" : "한국", "word_id" : 3, "m_begin" : 7, "m_end" : 7},
		{"id" : 4, "result" : "무대/NNG+에서/JKB", "target" : "무대에서", "word_id" : 4, "m_begin" : 8, "m_end" : 9},
		{"id" : 5, "result" : "쫓겨나/VV+어/EC", "target" : "쫓겨나", "word_id" : 5, "m_begin" : 10, "m_end" : 11},
		{"id" : 6, "result" : "미국/NNG+으로/JKB", "target" : "미국으로", "word_id" : 6, "m_begin" : 12, "m_end" : 13},
		{"id" : 7, "result" : "가/VV+게/EC+되/VV+ㄴ/ETM", "target" : "가게된", "word_id" : 7, "m_begin" : 14, "m_end" : 17},
		{"id" : 8, "result" : "가수/NNG", "target" : "가수", "word_id" : 8, "m_begin" : 18, "m_end" : 18},
		{"id" : 9, "result" : "준호/NNG+의/JKG", "target" : "준호의", "word_id" : 9, "m_begin" : 19, "m_end" : 20},
		{"id" : 10, "result" : "이야기/NNG+이/VCP+다/EF+./SF", "target" : "이야기이다.", "word_id" : 10, "m_begin" : 21, "m_end" : 24}
	],
	"WSD" : [
		{"id" : 0, "text" : "대마초", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 90, "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 99, "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "피우", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 103, "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 106, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "혐의", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 110, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 116, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "한국", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 120, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "무대", "type" : "NNG", "scode" : "06", "weight" : 1, "position" : 127, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 133, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "쫓겨나", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 140, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "아", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 146, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "미국", "type" : "NNP", "scode" : "03", "weight" : 1, "position" : 150, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 156, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "가", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 163, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "게", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 166, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "되", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 169, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 169, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "가수", "type" : "NNG", "scode" : "11", "weight" : 1, "position" : 173, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "준호", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 180, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 186, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "이야기", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 190, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 199, "begin" : 22, "end" : 22},
		{"id" : 22, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 202, "begin" : 23, "end" : 23},
		{"id" : 23, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 205, "begin" : 24, "end" : 24}
	],
	"word" : [
		{"id" : 0, "text" : "대마초를", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "피운", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 2, "text" : "혐의로", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 3, "text" : "한국", "type" : "", "begin" : 7, "end" : 7},
		{"id" : 4, "text" : "무대에서", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 5, "text" : "쫓겨나", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 6, "text" : "미국으로", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 7, "text" : "가게된", "type" : "", "begin" : 14, "end" : 17},
		{"id" : 8, "text" : "가수", "type" : "", "begin" : 18, "end" : 18},
		{"id" : 9, "text" : "준호의", "type" : "", "begin" : 19, "end" : 20},
		{"id" : 10, "text" : "이야기이다.", "type" : "", "begin" : 21, "end" : 24}
	],
	"NE" : [
		{"id" : 0, "text" : "대마초", "type" : "PT_GRASS", "begin" : 0, "end" : 1, "weight" : 0.355393, "common_noun" : 0},
		{"id" : 1, "text" : "한국", "type" : "LCP_COUNTRY", "begin" : 7, "end" : 7, "weight" : 0.438036, "common_noun" : 0},
		{"id" : 2, "text" : "미국", "type" : "LCP_COUNTRY", "begin" : 12, "end" : 12, "weight" : 0.570568, "common_noun" : 0},
		{"id" : 3, "text" : "가수", "type" : "CV_OCCUPATION", "begin" : 18, "end" : 18, "weight" : 0.308282, "common_noun" : 0},
		{"id" : 4, "text" : "준호", "type" : "PS_NAME", "begin" : 19, "end" : 19, "weight" : 0.509988, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "대마초를", "head" : 1, "label" : "NP_OBJ", "mod" : [], "weight" : 0.876072 },
		{"id" : 1, "text" : "피운", "head" : 2, "label" : "VP_MOD", "mod" : [0], "weight" : 0.823373 },
		{"id" : 2, "text" : "혐의로", "head" : 5, "label" : "NP_AJT", "mod" : [1], "weight" : 0.553904 },
		{"id" : 3, "text" : "한국", "head" : 4, "label" : "NP", "mod" : [], "weight" : 0.830234 },
		{"id" : 4, "text" : "무대에서", "head" : 5, "label" : "NP_AJT", "mod" : [3], "weight" : 0.757248 },
		{"id" : 5, "text" : "쫓겨나", "head" : 7, "label" : "VP", "mod" : [2, 4], "weight" : 0.710998 },
		{"id" : 6, "text" : "미국으로", "head" : 7, "label" : "NP_AJT", "mod" : [], "weight" : 0.718771 },
		{"id" : 7, "text" : "가게된", "head" : 8, "label" : "VP_MOD", "mod" : [5, 6], "weight" : 0.541715 },
		{"id" : 8, "text" : "가수", "head" : 9, "label" : "NP", "mod" : [7], "weight" : 0.7021 },
		{"id" : 9, "text" : "준호의", "head" : 10, "label" : "NP_MOD", "mod" : [8], "weight" : 0.513802 },
		{"id" : 10, "text" : "이야기이다.", "head" : -1, "label" : "VNP", "mod" : [9], "weight" : 0.0145433 }
	],
	"SRL" : [
		{"verb" : "피우", "sense" : 1, "word_id" : 1, "weight" : 0.293157,
			"argument" : [
				{"type" : "ARG1", "word_id" : 0, "text" : "대마초를", "weight" : 0.293157 }
			] },
		{"verb" : "쫓겨나", "sense" : 1, "word_id" : 5, "weight" : 0.136671,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 4, "text" : "무대에서", "weight" : 0.136671 }
			] },
		{"verb" : "가", "sense" : 1, "word_id" : 7, "weight" : 0.146728,
			"argument" : [
				{"type" : "ARGM-MNR", "word_id" : 5, "text" : "쫓겨나", "weight" : 0.0783667 },
				{"type" : "ARG3", "word_id" : 6, "text" : "미국으로", "weight" : 0.257542 },
				{"type" : "ARG1", "word_id" : 8, "text" : "가수", "weight" : 0.104274 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 5, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.422096 },
		{"id" : 1, "verb_wid" : 10, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.478599 }
	]
	},
	{
	"id" : 3,
	"reserve_str" : "",
	"text" : "1980년대 한국의 사회상을 비추는 내용을 담고있다.",
	"morp" : [
		{"id" : 0, "lemma" : "1980", "type" : "SN", "position" : 206, "weight" : 1 },
		{"id" : 1, "lemma" : "년대", "type" : "NNB", "position" : 210, "weight" : 0.9 },
		{"id" : 2, "lemma" : "한국", "type" : "NNP", "position" : 217, "weight" : 0.0152499 },
		{"id" : 3, "lemma" : "의", "type" : "JKG", "position" : 223, "weight" : 0.0987295 },
		{"id" : 4, "lemma" : "사회", "type" : "NNG", "position" : 227, "weight" : 0.871282 },
		{"id" : 5, "lemma" : "상", "type" : "XSN", "position" : 233, "weight" : 0.0188915 },
		{"id" : 6, "lemma" : "을", "type" : "JKO", "position" : 236, "weight" : 0.0819193 },
		{"id" : 7, "lemma" : "비추", "type" : "VV", "position" : 240, "weight" : 0.777964 },
		{"id" : 8, "lemma" : "는", "type" : "ETM", "position" : 246, "weight" : 0.184941 },
		{"id" : 9, "lemma" : "내용", "type" : "NNG", "position" : 250, "weight" : 0.658704 },
		{"id" : 10, "lemma" : "을", "type" : "JKO", "position" : 256, "weight" : 0.129611 },
		{"id" : 11, "lemma" : "담", "type" : "VV", "position" : 260, "weight" : 0.603567 },
		{"id" : 12, "lemma" : "고", "type" : "EC", "position" : 263, "weight" : 0.416679 },
		{"id" : 13, "lemma" : "있", "type" : "VX", "position" : 266, "weight" : 0.125953 },
		{"id" : 14, "lemma" : "다", "type" : "EF", "position" : 269, "weight" : 0.180366 },
		{"id" : 15, "lemma" : ".", "type" : "SF", "position" : 272, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "1980/SN+년대/NNB", "target" : "1980년대", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "한국/NNG+의/JKG", "target" : "한국의", "word_id" : 1, "m_begin" : 2, "m_end" : 3},
		{"id" : 2, "result" : "사회상/NNG+을/JKO", "target" : "사회상을", "word_id" : 2, "m_begin" : 4, "m_end" : 6},
		{"id" : 3, "result" : "비추/VV+는/ETM", "target" : "비추는", "word_id" : 3, "m_begin" : 7, "m_end" : 8},
		{"id" : 4, "result" : "내용/NNG+을/JKO", "target" : "내용을", "word_id" : 4, "m_begin" : 9, "m_end" : 10},
		{"id" : 5, "result" : "담/VV+고/EC+있/VX+다/EF+./SF", "target" : "담고있다.", "word_id" : 5, "m_begin" : 11, "m_end" : 15}
	],
	"WSD" : [
		{"id" : 0, "text" : "1980", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 206, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년대", "type" : "NNB", "scode" : "00", "weight" : 1, "position" : 210, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "한국", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 217, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 223, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "사회상", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 227, "begin" : 4, "end" : 5},
		{"id" : 5, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 236, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "비추", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 240, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 246, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "내용", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 250, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 256, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "담", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 260, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 263, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "있", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 266, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 269, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 272, "begin" : 15, "end" : 15}
	],
	"word" : [
		{"id" : 0, "text" : "1980년대", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "한국의", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 2, "text" : "사회상을", "type" : "", "begin" : 4, "end" : 6},
		{"id" : 3, "text" : "비추는", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 4, "text" : "내용을", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 5, "text" : "담고있다.", "type" : "", "begin" : 11, "end" : 15}
	],
	"NE" : [
		{"id" : 0, "text" : "1980년대", "type" : "DT_YEAR", "begin" : 0, "end" : 1, "weight" : 0.607925, "common_noun" : 0},
		{"id" : 1, "text" : "한국", "type" : "LCP_COUNTRY", "begin" : 2, "end" : 2, "weight" : 0.552515, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "1980년대", "head" : 5, "label" : "NP_AJT", "mod" : [], "weight" : 0.514252 },
		{"id" : 1, "text" : "한국의", "head" : 2, "label" : "NP_MOD", "mod" : [], "weight" : 0.622531 },
		{"id" : 2, "text" : "사회상을", "head" : 3, "label" : "NP_OBJ", "mod" : [1], "weight" : 0.950581 },
		{"id" : 3, "text" : "비추는", "head" : 4, "label" : "VP_MOD", "mod" : [2], "weight" : 0.829134 },
		{"id" : 4, "text" : "내용을", "head" : 5, "label" : "NP_OBJ", "mod" : [3], "weight" : 0.578687 },
		{"id" : 5, "text" : "담고있다.", "head" : -1, "label" : "VP", "mod" : [0, 4], "weight" : 0.104522 }
	],
	"SRL" : [
		{"verb" : "비추", "sense" : 1, "word_id" : 3, "weight" : 0.211345,
			"argument" : [
				{"type" : "ARG1", "word_id" : 2, "text" : "사회상을", "weight" : 0.211345 }
			] },
		{"verb" : "담", "sense" : 1, "word_id" : 5, "weight" : 0.25654,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 0, "text" : "1980년대", "weight" : 0.175644 },
				{"type" : "ARG1", "word_id" : 4, "text" : "내용을", "weight" : 0.337436 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 5, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.451249 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "LCP_COUNTRY", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "한국", 
	 "mention" : [
		{"id" : 10, "sent_id" : 2, "start_eid" : 3, "end_eid" : 3, "ne_id" : 1, "text" : "한국", "start_eid_short" : 3, "end_eid_short" : 3, "text_short" : "한국", "weight" : 0.01 },
		{"id" : 16, "sent_id" : 3, "start_eid" : 1, "end_eid" : 1, "ne_id" : 1, "text" : "한국", "start_eid_short" : 1, "end_eid_short" : 1, "text_short" : "한국", "weight" : 0.016 }
	] }
 ]
}

