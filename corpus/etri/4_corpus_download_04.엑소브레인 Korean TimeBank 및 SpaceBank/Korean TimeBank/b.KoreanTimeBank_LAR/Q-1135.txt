{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "이것은 중세 유럽에서 토양의 비옥도를 유지하기 위해 실시한 농경 방식이다.",
	"morp" : [
		{"id" : 0, "lemma" : "이것", "type" : "NP", "position" : 0, "weight" : 0.0733657 },
		{"id" : 1, "lemma" : "은", "type" : "JX", "position" : 6, "weight" : 0.191811 },
		{"id" : 2, "lemma" : "중세", "type" : "NNG", "position" : 10, "weight" : 0.9 },
		{"id" : 3, "lemma" : "유럽", "type" : "NNP", "position" : 17, "weight" : 0.9 },
		{"id" : 4, "lemma" : "에서", "type" : "JKB", "position" : 23, "weight" : 0.0823859 },
		{"id" : 5, "lemma" : "토양", "type" : "NNG", "position" : 30, "weight" : 0.9 },
		{"id" : 6, "lemma" : "의", "type" : "JKG", "position" : 36, "weight" : 0.0694213 },
		{"id" : 7, "lemma" : "비옥", "type" : "NNG", "position" : 40, "weight" : 0.4 },
		{"id" : 8, "lemma" : "도", "type" : "NNG", "position" : 46, "weight" : 0.00190905 },
		{"id" : 9, "lemma" : "를", "type" : "JKO", "position" : 49, "weight" : 0.137686 },
		{"id" : 10, "lemma" : "유지", "type" : "NNG", "position" : 53, "weight" : 0.0866432 },
		{"id" : 11, "lemma" : "하", "type" : "XSV", "position" : 59, "weight" : 0.0001 },
		{"id" : 12, "lemma" : "기", "type" : "ETN", "position" : 62, "weight" : 0.0556099 },
		{"id" : 13, "lemma" : "위하", "type" : "VV", "position" : 66, "weight" : 0.176484 },
		{"id" : 14, "lemma" : "어", "type" : "EC", "position" : 69, "weight" : 0.41831 },
		{"id" : 15, "lemma" : "실시", "type" : "NNG", "position" : 73, "weight" : 0.9 },
		{"id" : 16, "lemma" : "하", "type" : "XSV", "position" : 79, "weight" : 0.0001 },
		{"id" : 17, "lemma" : "ㄴ", "type" : "ETM", "position" : 79, "weight" : 0.392321 },
		{"id" : 18, "lemma" : "농경", "type" : "NNG", "position" : 83, "weight" : 0.9 },
		{"id" : 19, "lemma" : "방식", "type" : "NNG", "position" : 90, "weight" : 0.9 },
		{"id" : 20, "lemma" : "이", "type" : "VCP", "position" : 96, "weight" : 0.0177525 },
		{"id" : 21, "lemma" : "다", "type" : "EF", "position" : 99, "weight" : 0.353579 },
		{"id" : 22, "lemma" : ".", "type" : "SF", "position" : 102, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이것/NP+은/JX", "target" : "이것은", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "중세/NNG", "target" : "중세", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "유럽/NNG+에서/JKB", "target" : "유럽에서", "word_id" : 2, "m_begin" : 3, "m_end" : 4},
		{"id" : 3, "result" : "토양/NNG+의/JKG", "target" : "토양의", "word_id" : 3, "m_begin" : 5, "m_end" : 6},
		{"id" : 4, "result" : "비옥도/NNG+를/JKO", "target" : "비옥도를", "word_id" : 4, "m_begin" : 7, "m_end" : 9},
		{"id" : 5, "result" : "유지하/VV+기/ETN", "target" : "유지하기", "word_id" : 5, "m_begin" : 10, "m_end" : 12},
		{"id" : 6, "result" : "위하/VV+어/EC", "target" : "위해", "word_id" : 6, "m_begin" : 13, "m_end" : 14},
		{"id" : 7, "result" : "실시하/VV+ㄴ/ETM", "target" : "실시한", "word_id" : 7, "m_begin" : 15, "m_end" : 17},
		{"id" : 8, "result" : "농경/NNG", "target" : "농경", "word_id" : 8, "m_begin" : 18, "m_end" : 18},
		{"id" : 9, "result" : "방식/NNG+이/VCP+다/EF+./SF", "target" : "방식이다.", "word_id" : 9, "m_begin" : 19, "m_end" : 22}
	],
	"WSD" : [
		{"id" : 0, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 6, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "중세", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "유럽", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 17, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 23, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "토양", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 30, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 36, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "비옥도", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 7, "end" : 8},
		{"id" : 8, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 49, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "유지하", "type" : "VV", "scode" : "02", "weight" : 1, "position" : 53, "begin" : 10, "end" : 11},
		{"id" : 10, "text" : "기", "type" : "ETN", "scode" : "00", "weight" : 1, "position" : 62, "begin" : 12, "end" : 12},
		{"id" : 11, "text" : "위하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 66, "begin" : 13, "end" : 13},
		{"id" : 12, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 69, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "실시하", "type" : "VV", "scode" : "03", "weight" : 1, "position" : 73, "begin" : 15, "end" : 16},
		{"id" : 14, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 79, "begin" : 17, "end" : 17},
		{"id" : 15, "text" : "농경", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 83, "begin" : 18, "end" : 18},
		{"id" : 16, "text" : "방식", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 90, "begin" : 19, "end" : 19},
		{"id" : 17, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 96, "begin" : 20, "end" : 20},
		{"id" : 18, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 99, "begin" : 21, "end" : 21},
		{"id" : 19, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 102, "begin" : 22, "end" : 22}
	],
	"word" : [
		{"id" : 0, "text" : "이것은", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "중세", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "유럽에서", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 3, "text" : "토양의", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 4, "text" : "비옥도를", "type" : "", "begin" : 7, "end" : 9},
		{"id" : 5, "text" : "유지하기", "type" : "", "begin" : 10, "end" : 12},
		{"id" : 6, "text" : "위해", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 7, "text" : "실시한", "type" : "", "begin" : 15, "end" : 17},
		{"id" : 8, "text" : "농경", "type" : "", "begin" : 18, "end" : 18},
		{"id" : 9, "text" : "방식이다.", "type" : "", "begin" : 19, "end" : 22}
	],
	"NE" : [
		{"id" : 0, "text" : "중세", "type" : "DT_DYNASTY", "begin" : 2, "end" : 2, "weight" : 0.564298, "common_noun" : 0},
		{"id" : 1, "text" : "유럽", "type" : "LCG_CONTINENT", "begin" : 3, "end" : 3, "weight" : 0.619491, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이것은", "head" : 9, "label" : "NP_SBJ", "mod" : [], "weight" : 0.849252 },
		{"id" : 1, "text" : "중세", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.816089 },
		{"id" : 2, "text" : "유럽에서", "head" : 5, "label" : "NP_AJT", "mod" : [1], "weight" : 0.663673 },
		{"id" : 3, "text" : "토양의", "head" : 4, "label" : "NP_MOD", "mod" : [], "weight" : 0.562586 },
		{"id" : 4, "text" : "비옥도를", "head" : 5, "label" : "NP_OBJ", "mod" : [3], "weight" : 0.765765 },
		{"id" : 5, "text" : "유지하기", "head" : 6, "label" : "VP_OBJ", "mod" : [2, 4], "weight" : 0.631938 },
		{"id" : 6, "text" : "위해", "head" : 7, "label" : "VP", "mod" : [5], "weight" : 0.805568 },
		{"id" : 7, "text" : "실시한", "head" : 9, "label" : "VP_MOD", "mod" : [6], "weight" : 0.746421 },
		{"id" : 8, "text" : "농경", "head" : 9, "label" : "NP", "mod" : [], "weight" : 0.84597 },
		{"id" : 9, "text" : "방식이다.", "head" : -1, "label" : "VNP", "mod" : [0, 7, 8], "weight" : 0.0475519 }
	],
	"SRL" : [
		{"verb" : "유지", "sense" : 1, "word_id" : 5, "weight" : 0.573247,
			"argument" : [
				{"type" : "ARG1", "word_id" : 4, "text" : "비옥도를", "weight" : 0.573247 }
			] },
		{"verb" : "위하", "sense" : 1, "word_id" : 6, "weight" : 0.260505,
			"argument" : [
				{"type" : "ARG1", "word_id" : 5, "text" : "유지하기", "weight" : 0.260505 }
			] },
		{"verb" : "실시", "sense" : 1, "word_id" : 7, "weight" : 0.320533,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 2, "text" : "유럽에서", "weight" : 0.419328 },
				{"type" : "ARGM-PRP", "word_id" : 6, "text" : "위해", "weight" : 0.389643 },
				{"type" : "ARG1", "word_id" : 9, "text" : "방식이다.", "weight" : 0.152629 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 5, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.794342 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "경작지를 춘경지, 추경지, 휴경지로 나누어 계절에 따라 번갈아 경작하는 농경 방식은 무엇일까?",
	"morp" : [
		{"id" : 0, "lemma" : "경작", "type" : "NNG", "position" : 103, "weight" : 0.9 },
		{"id" : 1, "lemma" : "지", "type" : "XSN", "position" : 109, "weight" : 0.000168337 },
		{"id" : 2, "lemma" : "를", "type" : "JKO", "position" : 112, "weight" : 0.0870227 },
		{"id" : 3, "lemma" : "춘경지", "type" : "NNP", "position" : 116, "weight" : 0.6 },
		{"id" : 4, "lemma" : ",", "type" : "SP", "position" : 125, "weight" : 1 },
		{"id" : 5, "lemma" : "추경", "type" : "NNG", "position" : 127, "weight" : 0.508978 },
		{"id" : 6, "lemma" : "지", "type" : "XSN", "position" : 133, "weight" : 0.000168337 },
		{"id" : 7, "lemma" : ",", "type" : "SP", "position" : 136, "weight" : 1 },
		{"id" : 8, "lemma" : "휴경", "type" : "NNG", "position" : 138, "weight" : 0.7 },
		{"id" : 9, "lemma" : "지", "type" : "XSN", "position" : 144, "weight" : 0.000168337 },
		{"id" : 10, "lemma" : "로", "type" : "JKB", "position" : 147, "weight" : 0.121577 },
		{"id" : 11, "lemma" : "나누", "type" : "VV", "position" : 151, "weight" : 0.9 },
		{"id" : 12, "lemma" : "어", "type" : "EC", "position" : 157, "weight" : 0.41831 },
		{"id" : 13, "lemma" : "계절", "type" : "NNG", "position" : 161, "weight" : 0.9 },
		{"id" : 14, "lemma" : "에", "type" : "JKB", "position" : 167, "weight" : 0.153364 },
		{"id" : 15, "lemma" : "따르", "type" : "VV", "position" : 171, "weight" : 0.9 },
		{"id" : 16, "lemma" : "아", "type" : "EC", "position" : 174, "weight" : 0.398809 },
		{"id" : 17, "lemma" : "번갈", "type" : "VV", "position" : 178, "weight" : 0.224368 },
		{"id" : 18, "lemma" : "아", "type" : "EC", "position" : 184, "weight" : 0.398809 },
		{"id" : 19, "lemma" : "경작", "type" : "NNG", "position" : 188, "weight" : 0.9 },
		{"id" : 20, "lemma" : "하", "type" : "XSV", "position" : 194, "weight" : 0.0001 },
		{"id" : 21, "lemma" : "는", "type" : "ETM", "position" : 197, "weight" : 0.238503 },
		{"id" : 22, "lemma" : "농경", "type" : "NNG", "position" : 201, "weight" : 0.9 },
		{"id" : 23, "lemma" : "방식", "type" : "NNG", "position" : 208, "weight" : 0.9 },
		{"id" : 24, "lemma" : "은", "type" : "JX", "position" : 214, "weight" : 0.0449928 },
		{"id" : 25, "lemma" : "무엇", "type" : "NP", "position" : 218, "weight" : 0.9 },
		{"id" : 26, "lemma" : "이", "type" : "VCP", "position" : 224, "weight" : 0.0175768 },
		{"id" : 27, "lemma" : "ㄹ까", "type" : "EF", "position" : 224, "weight" : 0.258243 },
		{"id" : 28, "lemma" : "?", "type" : "SF", "position" : 230, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "경작지/NNG+를/JKO", "target" : "경작지를", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "춘경지/NNG+,/SP", "target" : "춘경지,", "word_id" : 1, "m_begin" : 3, "m_end" : 4},
		{"id" : 2, "result" : "추경지/NNG+,/SP", "target" : "추경지,", "word_id" : 2, "m_begin" : 5, "m_end" : 7},
		{"id" : 3, "result" : "휴경지/NNG+로/JKB", "target" : "휴경지로", "word_id" : 3, "m_begin" : 8, "m_end" : 10},
		{"id" : 4, "result" : "나누/VV+어/EC", "target" : "나누어", "word_id" : 4, "m_begin" : 11, "m_end" : 12},
		{"id" : 5, "result" : "계절/NNG+에/JKB", "target" : "계절에", "word_id" : 5, "m_begin" : 13, "m_end" : 14},
		{"id" : 6, "result" : "따르/VV+어/EC", "target" : "따라", "word_id" : 6, "m_begin" : 15, "m_end" : 16},
		{"id" : 7, "result" : "번갈/VV+어/EC", "target" : "번갈아", "word_id" : 7, "m_begin" : 17, "m_end" : 18},
		{"id" : 8, "result" : "경작하/VV+는/ETM", "target" : "경작하는", "word_id" : 8, "m_begin" : 19, "m_end" : 21},
		{"id" : 9, "result" : "농경/NNG", "target" : "농경", "word_id" : 9, "m_begin" : 22, "m_end" : 22},
		{"id" : 10, "result" : "방식/NNG+은/JX", "target" : "방식은", "word_id" : 10, "m_begin" : 23, "m_end" : 24},
		{"id" : 11, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 11, "m_begin" : 25, "m_end" : 28}
	],
	"WSD" : [
		{"id" : 0, "text" : "경작지", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 103, "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 112, "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "춘경지", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 116, "begin" : 3, "end" : 3},
		{"id" : 3, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 125, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "추경", "type" : "NNG", "scode" : "88", "weight" : 1, "position" : 127, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "지", "type" : "XSN", "scode" : "26", "weight" : 1, "position" : 133, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 136, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "휴경지", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 138, "begin" : 8, "end" : 9},
		{"id" : 8, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 147, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "나누", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 151, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 157, "begin" : 12, "end" : 12},
		{"id" : 11, "text" : "계절", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 161, "begin" : 13, "end" : 13},
		{"id" : 12, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 167, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "따르", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 171, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : "아", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 174, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : "번갈", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 178, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : "아", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 184, "begin" : 18, "end" : 18},
		{"id" : 17, "text" : "경작하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 188, "begin" : 19, "end" : 20},
		{"id" : 18, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 197, "begin" : 21, "end" : 21},
		{"id" : 19, "text" : "농경", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 201, "begin" : 22, "end" : 22},
		{"id" : 20, "text" : "방식", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 208, "begin" : 23, "end" : 23},
		{"id" : 21, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 214, "begin" : 24, "end" : 24},
		{"id" : 22, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 218, "begin" : 25, "end" : 25},
		{"id" : 23, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 224, "begin" : 26, "end" : 26},
		{"id" : 24, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 224, "begin" : 27, "end" : 27},
		{"id" : 25, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 230, "begin" : 28, "end" : 28}
	],
	"word" : [
		{"id" : 0, "text" : "경작지를", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "춘경지,", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 2, "text" : "추경지,", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 3, "text" : "휴경지로", "type" : "", "begin" : 8, "end" : 10},
		{"id" : 4, "text" : "나누어", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 5, "text" : "계절에", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 6, "text" : "따라", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 7, "text" : "번갈아", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 8, "text" : "경작하는", "type" : "", "begin" : 19, "end" : 21},
		{"id" : 9, "text" : "농경", "type" : "", "begin" : 22, "end" : 22},
		{"id" : 10, "text" : "방식은", "type" : "", "begin" : 23, "end" : 24},
		{"id" : 11, "text" : "무엇일까?", "type" : "", "begin" : 25, "end" : 28}
	],
	"NE" : [
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "경작지를", "head" : 4, "label" : "NP_OBJ", "mod" : [], "weight" : 0.799319 },
		{"id" : 1, "text" : "춘경지,", "head" : 3, "label" : "NP_CNJ", "mod" : [], "weight" : 0.299146 },
		{"id" : 2, "text" : "추경지,", "head" : 3, "label" : "NP_CNJ", "mod" : [], "weight" : 0.465598 },
		{"id" : 3, "text" : "휴경지로", "head" : 4, "label" : "NP_AJT", "mod" : [1, 2], "weight" : 0.836914 },
		{"id" : 4, "text" : "나누어", "head" : 6, "label" : "VP", "mod" : [0, 3], "weight" : 0.592907 },
		{"id" : 5, "text" : "계절에", "head" : 6, "label" : "NP_AJT", "mod" : [], "weight" : 0.628659 },
		{"id" : 6, "text" : "따라", "head" : 7, "label" : "VP", "mod" : [4, 5], "weight" : 0.599024 },
		{"id" : 7, "text" : "번갈아", "head" : 8, "label" : "VP", "mod" : [6], "weight" : 0.803181 },
		{"id" : 8, "text" : "경작하는", "head" : 10, "label" : "VP_MOD", "mod" : [7], "weight" : 0.766157 },
		{"id" : 9, "text" : "농경", "head" : 10, "label" : "NP", "mod" : [], "weight" : 0.661971 },
		{"id" : 10, "text" : "방식은", "head" : 11, "label" : "NP_SBJ", "mod" : [8, 9], "weight" : 0.767582 },
		{"id" : 11, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [10], "weight" : 0.00576236 }
	],
	"SRL" : [
		{"verb" : "나누", "sense" : 1, "word_id" : 4, "weight" : 0.407753,
			"argument" : [
				{"type" : "ARG1", "word_id" : 0, "text" : "경작지를", "weight" : 0.436309 },
				{"type" : "ARG2", "word_id" : 3, "text" : "휴경지로", "weight" : 0.379196 }
			] },
		{"verb" : "따르", "sense" : 2, "word_id" : 6, "weight" : 0.503121,
			"argument" : [
				{"type" : "ARG2", "word_id" : 5, "text" : "계절에", "weight" : 0.503121 }
			] },
		{"verb" : "번갈", "sense" : 1, "word_id" : 7, "weight" : 0.320197,
			"argument" : [
				{"type" : "ARG1", "word_id" : 8, "text" : "경작하는", "weight" : 0.320197 }
			] },
		{"verb" : "경작", "sense" : 1, "word_id" : 8, "weight" : 0,
			"argument" : [
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 4, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.927167 },
		{"id" : 1, "verb_wid" : 7, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.825271 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 6, "sent_id" : 0, "start_eid" : 8, "end_eid" : 8, "ne_id" : -1, "text" : "농경", "start_eid_short" : 8, "end_eid_short" : 8, "text_short" : "농경", "weight" : 0.01 },
		{"id" : 13, "sent_id" : 1, "start_eid" : 9, "end_eid" : 9, "ne_id" : -1, "text" : "농경", "start_eid_short" : 9, "end_eid_short" : 9, "text_short" : "농경", "weight" : 0.016 }
	] },
	{"id" : 1, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 7, "sent_id" : 1, "start_eid" : 0, "end_eid" : 10, "ne_id" : -1, "text" : "경작지를 춘경지, 추경지, 휴경지로 나누어 계절에 따라 번갈아 경작하는 농경 방식", "start_eid_short" : 9, "end_eid_short" : 10, "text_short" : "농경 방식", "weight" : 0.002 },
		{"id" : 14, "sent_id" : 1, "start_eid" : 11, "end_eid" : 11, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 11, "end_eid_short" : 11, "text_short" : "무엇이ㄹ까?", "weight" : 0.006 }
	] }
 ]
}
