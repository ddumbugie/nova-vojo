{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "한혜연(1969년 3월 3일 ~)은 대한민국의 만화가이다.",
	"morp" : [
		{"id" : 0, "lemma" : "한혜연", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "(", "type" : "SS", "position" : 9, "weight" : 1 },
		{"id" : 2, "lemma" : "1969", "type" : "SN", "position" : 10, "weight" : 1 },
		{"id" : 3, "lemma" : "년", "type" : "NNB", "position" : 14, "weight" : 0.414343 },
		{"id" : 4, "lemma" : "3", "type" : "SN", "position" : 18, "weight" : 1 },
		{"id" : 5, "lemma" : "월", "type" : "NNB", "position" : 19, "weight" : 0.408539 },
		{"id" : 6, "lemma" : "3", "type" : "SN", "position" : 23, "weight" : 1 },
		{"id" : 7, "lemma" : "일", "type" : "NNB", "position" : 24, "weight" : 0.126777 },
		{"id" : 8, "lemma" : "~", "type" : "SO", "position" : 28, "weight" : 1 },
		{"id" : 9, "lemma" : ")", "type" : "SS", "position" : 29, "weight" : 1 },
		{"id" : 10, "lemma" : "은", "type" : "JX", "position" : 30, "weight" : 0.0128817 },
		{"id" : 11, "lemma" : "대한민국", "type" : "NNP", "position" : 34, "weight" : 0.0447775 },
		{"id" : 12, "lemma" : "의", "type" : "JKG", "position" : 46, "weight" : 0.0987295 },
		{"id" : 13, "lemma" : "만화", "type" : "NNG", "position" : 50, "weight" : 0.83848 },
		{"id" : 14, "lemma" : "가", "type" : "XSN", "position" : 56, "weight" : 0.000115417 },
		{"id" : 15, "lemma" : "이", "type" : "VCP", "position" : 59, "weight" : 0.0165001 },
		{"id" : 16, "lemma" : "다", "type" : "EF", "position" : 62, "weight" : 0.353579 },
		{"id" : 17, "lemma" : ".", "type" : "SF", "position" : 65, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "한혜연/NNG+(/SS+1969/SN+년/NNB", "target" : "한혜연(1969년", "word_id" : 0, "m_begin" : 0, "m_end" : 3},
		{"id" : 1, "result" : "3/SN+월/NNB", "target" : "3월", "word_id" : 1, "m_begin" : 4, "m_end" : 5},
		{"id" : 2, "result" : "3/SN+일/NNB", "target" : "3일", "word_id" : 2, "m_begin" : 6, "m_end" : 7},
		{"id" : 3, "result" : "~/SO+)/SS+은/JX", "target" : "~)은", "word_id" : 3, "m_begin" : 8, "m_end" : 10},
		{"id" : 4, "result" : "대한민국/NNG+의/JKG", "target" : "대한민국의", "word_id" : 4, "m_begin" : 11, "m_end" : 12},
		{"id" : 5, "result" : "만화가/NNG+이/VCP+다/EF+./SF", "target" : "만화가이다.", "word_id" : 5, "m_begin" : 13, "m_end" : 17}
	],
	"WSD" : [
		{"id" : 0, "text" : "한혜연", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "1969", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 14, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "3", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 18, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "월", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 19, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "3", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 23, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "일", "type" : "NNB", "scode" : "07", "weight" : 1, "position" : 24, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "~", "type" : "SO", "scode" : "00", "weight" : 1, "position" : 28, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 29, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 30, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "대한민국", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 34, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 46, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "만화가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 50, "begin" : 13, "end" : 14},
		{"id" : 14, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 59, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 62, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 65, "begin" : 17, "end" : 17}
	],
	"word" : [
		{"id" : 0, "text" : "한혜연(1969년", "type" : "", "begin" : 0, "end" : 3},
		{"id" : 1, "text" : "3월", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 2, "text" : "3일", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 3, "text" : "~)은", "type" : "", "begin" : 8, "end" : 10},
		{"id" : 4, "text" : "대한민국의", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 5, "text" : "만화가이다.", "type" : "", "begin" : 13, "end" : 17}
	],
	"NE" : [
		{"id" : 0, "text" : "한혜연", "type" : "PS_NAME", "begin" : 0, "end" : 0, "weight" : 0.370587, "common_noun" : 0},
		{"id" : 1, "text" : "1969년 3월 3일 ~", "type" : "DT_OTHERS", "begin" : 2, "end" : 8, "weight" : 0.606743, "common_noun" : 0},
		{"id" : 2, "text" : "대한민국", "type" : "LCP_COUNTRY", "begin" : 11, "end" : 11, "weight" : 0.190445, "common_noun" : 0},
		{"id" : 3, "text" : "만화가", "type" : "CV_OCCUPATION", "begin" : 13, "end" : 14, "weight" : 0.285799, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "한혜연(1969년", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.583097 },
		{"id" : 1, "text" : "3월", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.796497 },
		{"id" : 2, "text" : "3일", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.639419 },
		{"id" : 3, "text" : "~)은", "head" : 5, "label" : "NP_SBJ", "mod" : [2], "weight" : 0.61224 },
		{"id" : 4, "text" : "대한민국의", "head" : 5, "label" : "NP_MOD", "mod" : [], "weight" : 0.426796 },
		{"id" : 5, "text" : "만화가이다.", "head" : -1, "label" : "VNP", "mod" : [3, 4], "weight" : 0.045259 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "1993년 단편 〈마네킹〉으로 데뷔하였다.",
	"morp" : [
		{"id" : 0, "lemma" : "1993", "type" : "SN", "position" : 66, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 70, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "단편", "type" : "NNG", "position" : 74, "weight" : 0.9 },
		{"id" : 3, "lemma" : "〈", "type" : "SS", "position" : 81, "weight" : 1 },
		{"id" : 4, "lemma" : "마네킹", "type" : "NNG", "position" : 84, "weight" : 0.9 },
		{"id" : 5, "lemma" : "〉", "type" : "SS", "position" : 93, "weight" : 1 },
		{"id" : 6, "lemma" : "으로", "type" : "JKB", "position" : 96, "weight" : 0.0486063 },
		{"id" : 7, "lemma" : "데뷔", "type" : "NNG", "position" : 103, "weight" : 0.9 },
		{"id" : 8, "lemma" : "하", "type" : "XSV", "position" : 109, "weight" : 0.0001 },
		{"id" : 9, "lemma" : "었", "type" : "EP", "position" : 112, "weight" : 0.9 },
		{"id" : 10, "lemma" : "다", "type" : "EF", "position" : 115, "weight" : 0.640954 },
		{"id" : 11, "lemma" : ".", "type" : "SF", "position" : 118, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "1993/SN+년/NNB", "target" : "1993년", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "단편/NNG", "target" : "단편", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "〈/SS+마네킹/NNG+〉/SS+으로/JKB", "target" : "〈마네킹〉으로", "word_id" : 2, "m_begin" : 3, "m_end" : 6},
		{"id" : 3, "result" : "데뷔하/VV+었/EP+다/EF+./SF", "target" : "데뷔하였다.", "word_id" : 3, "m_begin" : 7, "m_end" : 11}
	],
	"WSD" : [
		{"id" : 0, "text" : "1993", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 66, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 70, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "단편", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 74, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "〈", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 81, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "마네킹", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 84, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "〉", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 93, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 96, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "데뷔하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 103, "begin" : 7, "end" : 8},
		{"id" : 8, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 112, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 115, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 118, "begin" : 11, "end" : 11}
	],
	"word" : [
		{"id" : 0, "text" : "1993년", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "단편", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "〈마네킹〉으로", "type" : "", "begin" : 3, "end" : 6},
		{"id" : 3, "text" : "데뷔하였다.", "type" : "", "begin" : 7, "end" : 11}
	],
	"NE" : [
		{"id" : 0, "text" : "1993년", "type" : "DT_YEAR", "begin" : 0, "end" : 1, "weight" : 0.649391, "common_noun" : 0},
		{"id" : 1, "text" : "마네킹", "type" : "AFW_DOCUMENT", "begin" : 4, "end" : 4, "weight" : 0.321536, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "1993년", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.560432 },
		{"id" : 1, "text" : "단편", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.649054 },
		{"id" : 2, "text" : "〈마네킹〉으로", "head" : 3, "label" : "NP_AJT", "mod" : [0, 1], "weight" : 0.537833 },
		{"id" : 3, "text" : "데뷔하였다.", "head" : -1, "label" : "VP", "mod" : [2], "weight" : 0.111349 }
	],
	"SRL" : [
		{"verb" : "데뷔", "sense" : 1, "word_id" : 3, "weight" : 0.0759483,
			"argument" : [
				{"type" : "ARG3", "word_id" : 2, "text" : "〈마네킹〉으로", "weight" : 0.0759483 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 3, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.71393 }
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "이화여자대학교에서 생물학을, 대학원에서 디자인을 전공하였다.",
	"morp" : [
		{"id" : 0, "lemma" : "이화", "type" : "NNP", "position" : 119, "weight" : 0.0318926 },
		{"id" : 1, "lemma" : "여자", "type" : "NNG", "position" : 125, "weight" : 0.9 },
		{"id" : 2, "lemma" : "대", "type" : "XPN", "position" : 131, "weight" : 0.0001 },
		{"id" : 3, "lemma" : "학교", "type" : "NNG", "position" : 134, "weight" : 0.473729 },
		{"id" : 4, "lemma" : "에서", "type" : "JKB", "position" : 140, "weight" : 0.153407 },
		{"id" : 5, "lemma" : "생물", "type" : "NNG", "position" : 147, "weight" : 0.9 },
		{"id" : 6, "lemma" : "학", "type" : "XSN", "position" : 153, "weight" : 0.0233263 },
		{"id" : 7, "lemma" : "을", "type" : "JKO", "position" : 156, "weight" : 0.0819193 },
		{"id" : 8, "lemma" : ",", "type" : "SP", "position" : 159, "weight" : 1 },
		{"id" : 9, "lemma" : "대학", "type" : "NNG", "position" : 161, "weight" : 0.530171 },
		{"id" : 10, "lemma" : "원", "type" : "XSN", "position" : 167, "weight" : 0.00350502 },
		{"id" : 11, "lemma" : "에서", "type" : "JKB", "position" : 170, "weight" : 0.121718 },
		{"id" : 12, "lemma" : "디자인", "type" : "NNG", "position" : 177, "weight" : 0.9 },
		{"id" : 13, "lemma" : "을", "type" : "JKO", "position" : 186, "weight" : 0.129611 },
		{"id" : 14, "lemma" : "전공", "type" : "NNG", "position" : 190, "weight" : 0.9 },
		{"id" : 15, "lemma" : "하", "type" : "XSV", "position" : 196, "weight" : 0.0001 },
		{"id" : 16, "lemma" : "었", "type" : "EP", "position" : 199, "weight" : 0.9 },
		{"id" : 17, "lemma" : "다", "type" : "EF", "position" : 202, "weight" : 0.640954 },
		{"id" : 18, "lemma" : ".", "type" : "SF", "position" : 205, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이화여자대학교/NNG+에서/JKB", "target" : "이화여자대학교에서", "word_id" : 0, "m_begin" : 0, "m_end" : 4},
		{"id" : 1, "result" : "생물학/NNG+을/JKO+,/SP", "target" : "생물학을,", "word_id" : 1, "m_begin" : 5, "m_end" : 8},
		{"id" : 2, "result" : "대학원/NNG+에서/JKB", "target" : "대학원에서", "word_id" : 2, "m_begin" : 9, "m_end" : 11},
		{"id" : 3, "result" : "디자인/NNG+을/JKO", "target" : "디자인을", "word_id" : 3, "m_begin" : 12, "m_end" : 13},
		{"id" : 4, "result" : "전공하/VV+었/EP+다/EF+./SF", "target" : "전공하였다.", "word_id" : 4, "m_begin" : 14, "m_end" : 18}
	],
	"WSD" : [
		{"id" : 0, "text" : "이화", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 119, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "여자", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 125, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "대학교", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 131, "begin" : 2, "end" : 3},
		{"id" : 3, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 140, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "생물학", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 147, "begin" : 5, "end" : 6},
		{"id" : 5, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 156, "begin" : 7, "end" : 7},
		{"id" : 6, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 159, "begin" : 8, "end" : 8},
		{"id" : 7, "text" : "대학원", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 161, "begin" : 9, "end" : 10},
		{"id" : 8, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 170, "begin" : 11, "end" : 11},
		{"id" : 9, "text" : "디자인", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 177, "begin" : 12, "end" : 12},
		{"id" : 10, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 186, "begin" : 13, "end" : 13},
		{"id" : 11, "text" : "전공하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 190, "begin" : 14, "end" : 15},
		{"id" : 12, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 199, "begin" : 16, "end" : 16},
		{"id" : 13, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 202, "begin" : 17, "end" : 17},
		{"id" : 14, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 205, "begin" : 18, "end" : 18}
	],
	"word" : [
		{"id" : 0, "text" : "이화여자대학교에서", "type" : "", "begin" : 0, "end" : 4},
		{"id" : 1, "text" : "생물학을,", "type" : "", "begin" : 5, "end" : 8},
		{"id" : 2, "text" : "대학원에서", "type" : "", "begin" : 9, "end" : 11},
		{"id" : 3, "text" : "디자인을", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 4, "text" : "전공하였다.", "type" : "", "begin" : 14, "end" : 18}
	],
	"NE" : [
		{"id" : 0, "text" : "이화여자대학교", "type" : "OGG_EDUCATION", "begin" : 0, "end" : 3, "weight" : 0.794102, "common_noun" : 0},
		{"id" : 1, "text" : "생물학", "type" : "FD_SCIENCE", "begin" : 5, "end" : 6, "weight" : 0.556347, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이화여자대학교에서", "head" : 4, "label" : "NP_AJT", "mod" : [], "weight" : 0.659886 },
		{"id" : 1, "text" : "생물학을,", "head" : 4, "label" : "NP_OBJ", "mod" : [], "weight" : 0.419022 },
		{"id" : 2, "text" : "대학원에서", "head" : 4, "label" : "NP_AJT", "mod" : [], "weight" : 0.581812 },
		{"id" : 3, "text" : "디자인을", "head" : 4, "label" : "NP_OBJ", "mod" : [], "weight" : 0.48342 },
		{"id" : 4, "text" : "전공하였다.", "head" : -1, "label" : "VP", "mod" : [0, 1, 2, 3], "weight" : 0.0464759 }
	],
	"SRL" : [
		{"verb" : "전공", "sense" : 1, "word_id" : 4, "weight" : 0.142434,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 0, "text" : "이화여자대학교에서", "weight" : 0.144671 },
				{"type" : "ARGM-LOC", "word_id" : 2, "text" : "대학원에서", "weight" : 0.12039 },
				{"type" : "ARG1", "word_id" : 3, "text" : "디자인을", "weight" : 0.162241 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 4, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.706464 }
	]
	}
 ],
 "entity" : [
 ]
}

