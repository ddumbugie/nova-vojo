{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "이미라(1962년10월 5일 ~ )는 대한민국의 만화가이다.",
	"morp" : [
		{"id" : 0, "lemma" : "이미라", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "(", "type" : "SS", "position" : 9, "weight" : 1 },
		{"id" : 2, "lemma" : "1962", "type" : "SN", "position" : 10, "weight" : 1 },
		{"id" : 3, "lemma" : "년", "type" : "NNB", "position" : 14, "weight" : 0.414343 },
		{"id" : 4, "lemma" : "10", "type" : "SN", "position" : 17, "weight" : 1 },
		{"id" : 5, "lemma" : "월", "type" : "NNB", "position" : 19, "weight" : 0.408539 },
		{"id" : 6, "lemma" : "5", "type" : "SN", "position" : 23, "weight" : 1 },
		{"id" : 7, "lemma" : "일", "type" : "NNB", "position" : 24, "weight" : 0.126777 },
		{"id" : 8, "lemma" : "~", "type" : "SO", "position" : 28, "weight" : 1 },
		{"id" : 9, "lemma" : ")", "type" : "SS", "position" : 30, "weight" : 1 },
		{"id" : 10, "lemma" : "는", "type" : "JX", "position" : 31, "weight" : 0.00823314 },
		{"id" : 11, "lemma" : "대한민국", "type" : "NNP", "position" : 35, "weight" : 0.0447775 },
		{"id" : 12, "lemma" : "의", "type" : "JKG", "position" : 47, "weight" : 0.0987295 },
		{"id" : 13, "lemma" : "만화", "type" : "NNG", "position" : 51, "weight" : 0.83848 },
		{"id" : 14, "lemma" : "가", "type" : "XSN", "position" : 57, "weight" : 0.000115417 },
		{"id" : 15, "lemma" : "이", "type" : "VCP", "position" : 60, "weight" : 0.0165001 },
		{"id" : 16, "lemma" : "다", "type" : "EF", "position" : 63, "weight" : 0.353579 },
		{"id" : 17, "lemma" : ".", "type" : "SF", "position" : 66, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이미라/NNG+(/SS+1962/SN+년/NNB+10/SN+월/NNB", "target" : "이미라(1962년10월", "word_id" : 0, "m_begin" : 0, "m_end" : 5},
		{"id" : 1, "result" : "5/SN+일/NNB", "target" : "5일", "word_id" : 1, "m_begin" : 6, "m_end" : 7},
		{"id" : 2, "result" : "~/SO", "target" : "~", "word_id" : 2, "m_begin" : 8, "m_end" : 8},
		{"id" : 3, "result" : ")/SS+는/JX", "target" : ")는", "word_id" : 3, "m_begin" : 9, "m_end" : 10},
		{"id" : 4, "result" : "대한민국/NNG+의/JKG", "target" : "대한민국의", "word_id" : 4, "m_begin" : 11, "m_end" : 12},
		{"id" : 5, "result" : "만화가/NNG+이/VCP+다/EF+./SF", "target" : "만화가이다.", "word_id" : 5, "m_begin" : 13, "m_end" : 17}
	],
	"WSD" : [
		{"id" : 0, "text" : "이미라", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "1962", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 14, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "10", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 17, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "월", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 19, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "5", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 23, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "일", "type" : "NNB", "scode" : "07", "weight" : 1, "position" : 24, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "~", "type" : "SO", "scode" : "00", "weight" : 1, "position" : 28, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 30, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 31, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "대한민국", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 35, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 47, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "만화가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 51, "begin" : 13, "end" : 14},
		{"id" : 14, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 60, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 63, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 66, "begin" : 17, "end" : 17}
	],
	"word" : [
		{"id" : 0, "text" : "이미라(1962년10월", "type" : "", "begin" : 0, "end" : 5},
		{"id" : 1, "text" : "5일", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 2, "text" : "~", "type" : "", "begin" : 8, "end" : 8},
		{"id" : 3, "text" : ")는", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 4, "text" : "대한민국의", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 5, "text" : "만화가이다.", "type" : "", "begin" : 13, "end" : 17}
	],
	"NE" : [
		{"id" : 0, "text" : "이미라", "type" : "PS_NAME", "begin" : 0, "end" : 0, "weight" : 0.504428, "common_noun" : 0},
		{"id" : 1, "text" : "1962년10월 5일 ~", "type" : "DT_OTHERS", "begin" : 2, "end" : 8, "weight" : 0.729469, "common_noun" : 0},
		{"id" : 2, "text" : "대한민국", "type" : "LCP_COUNTRY", "begin" : 11, "end" : 11, "weight" : 0.174696, "common_noun" : 0},
		{"id" : 3, "text" : "만화가", "type" : "CV_OCCUPATION", "begin" : 13, "end" : 14, "weight" : 0.284922, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이미라(1962년10월", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.34211 },
		{"id" : 1, "text" : "5일", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.472065 },
		{"id" : 2, "text" : "~", "head" : 3, "label" : "X", "mod" : [0, 1], "weight" : 0.76808 },
		{"id" : 3, "text" : ")는", "head" : 5, "label" : "NP_SBJ", "mod" : [2], "weight" : 0.45201 },
		{"id" : 4, "text" : "대한민국의", "head" : 5, "label" : "NP_MOD", "mod" : [], "weight" : 0.425766 },
		{"id" : 5, "text" : "만화가이다.", "head" : -1, "label" : "VNP", "mod" : [3, 4], "weight" : 0.0135234 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "1986년 《바람의 방향》으로 데뷔했다.",
	"morp" : [
		{"id" : 0, "lemma" : "1986", "type" : "SN", "position" : 67, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 71, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "《", "type" : "SS", "position" : 75, "weight" : 1 },
		{"id" : 3, "lemma" : "바람", "type" : "NNG", "position" : 78, "weight" : 0.157171 },
		{"id" : 4, "lemma" : "의", "type" : "JKG", "position" : 84, "weight" : 0.0694213 },
		{"id" : 5, "lemma" : "방향", "type" : "NNG", "position" : 88, "weight" : 0.9 },
		{"id" : 6, "lemma" : "》", "type" : "SS", "position" : 94, "weight" : 1 },
		{"id" : 7, "lemma" : "으로", "type" : "JKB", "position" : 97, "weight" : 0.0486063 },
		{"id" : 8, "lemma" : "데뷔", "type" : "NNG", "position" : 104, "weight" : 0.9 },
		{"id" : 9, "lemma" : "하", "type" : "XSV", "position" : 110, "weight" : 0.0001 },
		{"id" : 10, "lemma" : "었", "type" : "EP", "position" : 110, "weight" : 0.9 },
		{"id" : 11, "lemma" : "다", "type" : "EF", "position" : 113, "weight" : 0.640954 },
		{"id" : 12, "lemma" : ".", "type" : "SF", "position" : 116, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "1986/SN+년/NNB", "target" : "1986년", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "《/SS+바람/NNG+의/JKG", "target" : "《바람의", "word_id" : 1, "m_begin" : 2, "m_end" : 4},
		{"id" : 2, "result" : "방향/NNG+》/SS+으로/JKB", "target" : "방향》으로", "word_id" : 2, "m_begin" : 5, "m_end" : 7},
		{"id" : 3, "result" : "데뷔하/VV+었/EP+다/EF+./SF", "target" : "데뷔했다.", "word_id" : 3, "m_begin" : 8, "m_end" : 12}
	],
	"WSD" : [
		{"id" : 0, "text" : "1986", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 67, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 71, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "《", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 75, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "바람", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 78, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 84, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "방향", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 88, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "》", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 94, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 97, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "데뷔하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 104, "begin" : 8, "end" : 9},
		{"id" : 9, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 110, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 113, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 116, "begin" : 12, "end" : 12}
	],
	"word" : [
		{"id" : 0, "text" : "1986년", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "《바람의", "type" : "", "begin" : 2, "end" : 4},
		{"id" : 2, "text" : "방향》으로", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 3, "text" : "데뷔했다.", "type" : "", "begin" : 8, "end" : 12}
	],
	"NE" : [
		{"id" : 0, "text" : "1986년", "type" : "DT_YEAR", "begin" : 0, "end" : 1, "weight" : 0.662256, "common_noun" : 0},
		{"id" : 1, "text" : "바람의 방향", "type" : "AFW_DOCUMENT", "begin" : 3, "end" : 5, "weight" : 0.927706, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "1986년", "head" : 3, "label" : "NP_AJT", "mod" : [], "weight" : 0.510995 },
		{"id" : 1, "text" : "《바람의", "head" : 2, "label" : "NP_MOD", "mod" : [], "weight" : 0.888438 },
		{"id" : 2, "text" : "방향》으로", "head" : 3, "label" : "NP_AJT", "mod" : [1], "weight" : 0.565054 },
		{"id" : 3, "text" : "데뷔했다.", "head" : -1, "label" : "VP", "mod" : [0, 2], "weight" : 0.142206 }
	],
	"SRL" : [
		{"verb" : "데뷔", "sense" : 1, "word_id" : 3, "weight" : 0.0789814,
			"argument" : [
				{"type" : "ARG2", "word_id" : 2, "text" : "방향》으로", "weight" : 0.0789814 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 3, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.721779 }
	]
	}
 ],
 "entity" : [
 ]
}

