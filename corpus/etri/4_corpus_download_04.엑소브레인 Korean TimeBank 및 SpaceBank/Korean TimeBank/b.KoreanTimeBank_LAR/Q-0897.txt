{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "우리나라는 신생대 3기에 지반이 융기되었다.",
	"morp" : [
		{"id" : 0, "lemma" : "우리", "type" : "NNG", "position" : 0, "weight" : 0.00152766 },
		{"id" : 1, "lemma" : "나라", "type" : "NNG", "position" : 6, "weight" : 0.184431 },
		{"id" : 2, "lemma" : "는", "type" : "JX", "position" : 12, "weight" : 0.0287565 },
		{"id" : 3, "lemma" : "신생", "type" : "NNG", "position" : 16, "weight" : 0.9 },
		{"id" : 4, "lemma" : "대", "type" : "NNG", "position" : 22, "weight" : 0.0277971 },
		{"id" : 5, "lemma" : "3", "type" : "SN", "position" : 26, "weight" : 1 },
		{"id" : 6, "lemma" : "기", "type" : "NNG", "position" : 27, "weight" : 0.00183918 },
		{"id" : 7, "lemma" : "에", "type" : "JKB", "position" : 30, "weight" : 0.153364 },
		{"id" : 8, "lemma" : "지반", "type" : "NNG", "position" : 34, "weight" : 0.9 },
		{"id" : 9, "lemma" : "이", "type" : "JKS", "position" : 40, "weight" : 0.0360723 },
		{"id" : 10, "lemma" : "융기", "type" : "NNG", "position" : 44, "weight" : 0.205572 },
		{"id" : 11, "lemma" : "되", "type" : "XSV", "position" : 50, "weight" : 0.000224177 },
		{"id" : 12, "lemma" : "었", "type" : "EP", "position" : 53, "weight" : 0.9 },
		{"id" : 13, "lemma" : "다", "type" : "EF", "position" : 56, "weight" : 0.640954 },
		{"id" : 14, "lemma" : ".", "type" : "SF", "position" : 59, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "우리나라/NNG+는/JX", "target" : "우리나라는", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "신생대/NNG", "target" : "신생대", "word_id" : 1, "m_begin" : 3, "m_end" : 4},
		{"id" : 2, "result" : "3/SN+기/NNG+에/JKB", "target" : "3기에", "word_id" : 2, "m_begin" : 5, "m_end" : 7},
		{"id" : 3, "result" : "지반/NNG+이/JKS", "target" : "지반이", "word_id" : 3, "m_begin" : 8, "m_end" : 9},
		{"id" : 4, "result" : "융기되/VV+었/EP+다/EF+./SF", "target" : "융기되었다.", "word_id" : 4, "m_begin" : 10, "m_end" : 14}
	],
	"WSD" : [
		{"id" : 0, "text" : "우리나라", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 12, "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "신생대", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 16, "begin" : 3, "end" : 4},
		{"id" : 3, "text" : "3", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 26, "begin" : 5, "end" : 5},
		{"id" : 4, "text" : "기", "type" : "NNG", "scode" : "13", "weight" : 1, "position" : 27, "begin" : 6, "end" : 6},
		{"id" : 5, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 30, "begin" : 7, "end" : 7},
		{"id" : 6, "text" : "지반", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 34, "begin" : 8, "end" : 8},
		{"id" : 7, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 9, "end" : 9},
		{"id" : 8, "text" : "융기", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 44, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "되", "type" : "XSV", "scode" : "00", "weight" : 1, "position" : 50, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 53, "begin" : 12, "end" : 12},
		{"id" : 11, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 56, "begin" : 13, "end" : 13},
		{"id" : 12, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 59, "begin" : 14, "end" : 14}
	],
	"word" : [
		{"id" : 0, "text" : "우리나라는", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "신생대", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 2, "text" : "3기에", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 3, "text" : "지반이", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 4, "text" : "융기되었다.", "type" : "", "begin" : 10, "end" : 14}
	],
	"NE" : [
		{"id" : 0, "text" : "신생대 3기", "type" : "DT_GEOAGE", "begin" : 3, "end" : 6, "weight" : 0.362989, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "우리나라는", "head" : 4, "label" : "NP_SBJ", "mod" : [], "weight" : 0.680922 },
		{"id" : 1, "text" : "신생대", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.561304 },
		{"id" : 2, "text" : "3기에", "head" : 4, "label" : "NP_AJT", "mod" : [1], "weight" : 0.76222 },
		{"id" : 3, "text" : "지반이", "head" : 4, "label" : "NP_SBJ", "mod" : [], "weight" : 0.825974 },
		{"id" : 4, "text" : "융기되었다.", "head" : -1, "label" : "VP", "mod" : [0, 2, 3], "weight" : 0.199425 }
	],
	"SRL" : [
		{"verb" : "융기", "sense" : 1, "word_id" : 4, "weight" : 0.205495,
			"argument" : [
				{"type" : "ARG2", "word_id" : 2, "text" : "3기에", "weight" : 0.155313 },
				{"type" : "ARG1", "word_id" : 3, "text" : "지반이", "weight" : 0.255676 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "지반이 융기되면서 다양한 지형들이 만들어졌는데, 융기와 관련한 지형이 아닌 것은 무엇일까?",
	"morp" : [
		{"id" : 0, "lemma" : "지반", "type" : "NNG", "position" : 60, "weight" : 0.9 },
		{"id" : 1, "lemma" : "이", "type" : "JKS", "position" : 66, "weight" : 0.0360723 },
		{"id" : 2, "lemma" : "융기", "type" : "NNG", "position" : 70, "weight" : 0.205572 },
		{"id" : 3, "lemma" : "되", "type" : "XSV", "position" : 76, "weight" : 0.000224177 },
		{"id" : 4, "lemma" : "면서", "type" : "EC", "position" : 79, "weight" : 0.370489 },
		{"id" : 5, "lemma" : "다양", "type" : "NNG", "position" : 86, "weight" : 0.9 },
		{"id" : 6, "lemma" : "하", "type" : "XSA", "position" : 92, "weight" : 0.0001 },
		{"id" : 7, "lemma" : "ㄴ", "type" : "ETM", "position" : 92, "weight" : 0.488779 },
		{"id" : 8, "lemma" : "지형", "type" : "NNG", "position" : 96, "weight" : 0.657634 },
		{"id" : 9, "lemma" : "들", "type" : "XSN", "position" : 102, "weight" : 0.0299841 },
		{"id" : 10, "lemma" : "이", "type" : "JKS", "position" : 105, "weight" : 0.0731805 },
		{"id" : 11, "lemma" : "만들", "type" : "VV", "position" : 109, "weight" : 0.9 },
		{"id" : 12, "lemma" : "어", "type" : "EC", "position" : 115, "weight" : 0.41831 },
		{"id" : 13, "lemma" : "지", "type" : "VX", "position" : 118, "weight" : 0.0539764 },
		{"id" : 14, "lemma" : "었", "type" : "EP", "position" : 118, "weight" : 0.9 },
		{"id" : 15, "lemma" : "는데", "type" : "EC", "position" : 121, "weight" : 0.188154 },
		{"id" : 16, "lemma" : ",", "type" : "SP", "position" : 127, "weight" : 1 },
		{"id" : 17, "lemma" : "융기", "type" : "NNG", "position" : 129, "weight" : 0.522273 },
		{"id" : 18, "lemma" : "와", "type" : "JC", "position" : 135, "weight" : 0.0169714 },
		{"id" : 19, "lemma" : "관련", "type" : "NNG", "position" : 139, "weight" : 0.9 },
		{"id" : 20, "lemma" : "하", "type" : "XSV", "position" : 145, "weight" : 0.0001 },
		{"id" : 21, "lemma" : "ㄴ", "type" : "ETM", "position" : 145, "weight" : 0.392321 },
		{"id" : 22, "lemma" : "지형", "type" : "NNG", "position" : 149, "weight" : 0.657634 },
		{"id" : 23, "lemma" : "이", "type" : "JKC", "position" : 155, "weight" : 0.000287945 },
		{"id" : 24, "lemma" : "아니", "type" : "VCN", "position" : 159, "weight" : 0.354175 },
		{"id" : 25, "lemma" : "ㄴ", "type" : "ETM", "position" : 162, "weight" : 0.144018 },
		{"id" : 26, "lemma" : "것", "type" : "NNB", "position" : 166, "weight" : 0.228788 },
		{"id" : 27, "lemma" : "은", "type" : "JX", "position" : 169, "weight" : 0.0688243 },
		{"id" : 28, "lemma" : "무엇", "type" : "NP", "position" : 173, "weight" : 0.9 },
		{"id" : 29, "lemma" : "이", "type" : "VCP", "position" : 179, "weight" : 0.0175768 },
		{"id" : 30, "lemma" : "ㄹ까", "type" : "EF", "position" : 179, "weight" : 0.258243 },
		{"id" : 31, "lemma" : "?", "type" : "SF", "position" : 185, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "지반/NNG+이/JKS", "target" : "지반이", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "융기되/VV+면서/EC", "target" : "융기되면서", "word_id" : 1, "m_begin" : 2, "m_end" : 4},
		{"id" : 2, "result" : "다양하/VA+ㄴ/ETM", "target" : "다양한", "word_id" : 2, "m_begin" : 5, "m_end" : 7},
		{"id" : 3, "result" : "지형들/NNG+이/JKS", "target" : "지형들이", "word_id" : 3, "m_begin" : 8, "m_end" : 10},
		{"id" : 4, "result" : "만들/VV+어/EC+지/VX+었/EP+는데/EC+,/SP", "target" : "만들어졌는데,", "word_id" : 4, "m_begin" : 11, "m_end" : 16},
		{"id" : 5, "result" : "융기/NNG+와/JC", "target" : "융기와", "word_id" : 5, "m_begin" : 17, "m_end" : 18},
		{"id" : 6, "result" : "관련하/VV+ㄴ/ETM", "target" : "관련한", "word_id" : 6, "m_begin" : 19, "m_end" : 21},
		{"id" : 7, "result" : "지형/NNG+이/JKC", "target" : "지형이", "word_id" : 7, "m_begin" : 22, "m_end" : 23},
		{"id" : 8, "result" : "아니/VCN+ㄴ/ETM", "target" : "아닌", "word_id" : 8, "m_begin" : 24, "m_end" : 25},
		{"id" : 9, "result" : "것/NNB+은/JX", "target" : "것은", "word_id" : 9, "m_begin" : 26, "m_end" : 27},
		{"id" : 10, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 10, "m_begin" : 28, "m_end" : 31}
	],
	"WSD" : [
		{"id" : 0, "text" : "지반", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 60, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 66, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "융기", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 70, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "되", "type" : "XSV", "scode" : "00", "weight" : 1, "position" : 76, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "면서", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 79, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "다양하", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 86, "begin" : 5, "end" : 6},
		{"id" : 6, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 92, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "지형", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 96, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "들", "type" : "XSN", "scode" : "09", "weight" : 1, "position" : 102, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 105, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "만들", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 109, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 115, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "지", "type" : "VX", "scode" : "04", "weight" : 1, "position" : 118, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 118, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "는데", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 121, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 127, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "융기", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 129, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "와", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 135, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "관련하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 139, "begin" : 19, "end" : 20},
		{"id" : 19, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 145, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : "지형", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 149, "begin" : 22, "end" : 22},
		{"id" : 21, "text" : "이", "type" : "JKC", "scode" : "00", "weight" : 1, "position" : 155, "begin" : 23, "end" : 23},
		{"id" : 22, "text" : "아니", "type" : "VCN", "scode" : "00", "weight" : 1, "position" : 159, "begin" : 24, "end" : 24},
		{"id" : 23, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 162, "begin" : 25, "end" : 25},
		{"id" : 24, "text" : "것", "type" : "NNB", "scode" : "01", "weight" : 1, "position" : 166, "begin" : 26, "end" : 26},
		{"id" : 25, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 169, "begin" : 27, "end" : 27},
		{"id" : 26, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 173, "begin" : 28, "end" : 28},
		{"id" : 27, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 179, "begin" : 29, "end" : 29},
		{"id" : 28, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 179, "begin" : 30, "end" : 30},
		{"id" : 29, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 185, "begin" : 31, "end" : 31}
	],
	"word" : [
		{"id" : 0, "text" : "지반이", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "융기되면서", "type" : "", "begin" : 2, "end" : 4},
		{"id" : 2, "text" : "다양한", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 3, "text" : "지형들이", "type" : "", "begin" : 8, "end" : 10},
		{"id" : 4, "text" : "만들어졌는데,", "type" : "", "begin" : 11, "end" : 16},
		{"id" : 5, "text" : "융기와", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 6, "text" : "관련한", "type" : "", "begin" : 19, "end" : 21},
		{"id" : 7, "text" : "지형이", "type" : "", "begin" : 22, "end" : 23},
		{"id" : 8, "text" : "아닌", "type" : "", "begin" : 24, "end" : 25},
		{"id" : 9, "text" : "것은", "type" : "", "begin" : 26, "end" : 27},
		{"id" : 10, "text" : "무엇일까?", "type" : "", "begin" : 28, "end" : 31}
	],
	"NE" : [
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "지반이", "head" : 1, "label" : "NP_SBJ", "mod" : [], "weight" : 0.739387 },
		{"id" : 1, "text" : "융기되면서", "head" : 4, "label" : "VP", "mod" : [0], "weight" : 0.797205 },
		{"id" : 2, "text" : "다양한", "head" : 3, "label" : "VP_MOD", "mod" : [], "weight" : 0.676762 },
		{"id" : 3, "text" : "지형들이", "head" : 4, "label" : "NP_SBJ", "mod" : [2], "weight" : 0.757321 },
		{"id" : 4, "text" : "만들어졌는데,", "head" : 10, "label" : "VP", "mod" : [1, 3], "weight" : 0.620242 },
		{"id" : 5, "text" : "융기와", "head" : 6, "label" : "NP_AJT", "mod" : [], "weight" : 0.667487 },
		{"id" : 6, "text" : "관련한", "head" : 7, "label" : "VP_MOD", "mod" : [5], "weight" : 0.66752 },
		{"id" : 7, "text" : "지형이", "head" : 8, "label" : "NP_CMP", "mod" : [6], "weight" : 0.774262 },
		{"id" : 8, "text" : "아닌", "head" : 9, "label" : "VP_MOD", "mod" : [7], "weight" : 0.684449 },
		{"id" : 9, "text" : "것은", "head" : 10, "label" : "NP_SBJ", "mod" : [8], "weight" : 0.774908 },
		{"id" : 10, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [4, 9], "weight" : 0.0291062 }
	],
	"SRL" : [
		{"verb" : "융기", "sense" : 1, "word_id" : 1, "weight" : 0.262597,
			"argument" : [
				{"type" : "ARG1", "word_id" : 0, "text" : "지반이", "weight" : 0.262597 }
			] },
		{"verb" : "다양", "sense" : 1, "word_id" : 2, "weight" : 0.723358,
			"argument" : [
				{"type" : "ARG1", "word_id" : 3, "text" : "지형들이", "weight" : 0.723358 }
			] },
		{"verb" : "만들", "sense" : 1, "word_id" : 4, "weight" : 0.396725,
			"argument" : [
				{"type" : "ARG1", "word_id" : 3, "text" : "지형들이", "weight" : 0.396725 }
			] },
		{"verb" : "관련", "sense" : 1, "word_id" : 6, "weight" : 0.40701,
			"argument" : [
				{"type" : "ARG2", "word_id" : 5, "text" : "융기와", "weight" : 0.44592 },
				{"type" : "ARG1", "word_id" : 7, "text" : "지형이", "weight" : 0.3681 }
			] },
		{"verb" : "아니", "sense" : 1, "word_id" : 8, "weight" : 0.607896,
			"argument" : [
				{"type" : "ARG2", "word_id" : 7, "text" : "지형이", "weight" : 0.607896 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 2, "sent_id" : 0, "start_eid" : 3, "end_eid" : 3, "ne_id" : -1, "text" : "지반", "start_eid_short" : 3, "end_eid_short" : 3, "text_short" : "지반", "weight" : 0.01 },
		{"id" : 3, "sent_id" : 1, "start_eid" : 0, "end_eid" : 0, "ne_id" : -1, "text" : "지반", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "지반", "weight" : 0.016 },
		{"id" : 10, "sent_id" : 1, "start_eid" : 10, "end_eid" : 10, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 10, "end_eid_short" : 10, "text_short" : "무엇이ㄹ까?", "weight" : 0.006 }
	] }
 ]
}
