{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "《늑대의 유혹》은 귀여니가 2003년에 발간한 두 번째 연애 소설이다.",
	"morp" : [
		{"id" : 0, "lemma" : "《", "type" : "SS", "position" : 0, "weight" : 1 },
		{"id" : 1, "lemma" : "늑대", "type" : "NNG", "position" : 3, "weight" : 0.9 },
		{"id" : 2, "lemma" : "의", "type" : "JKG", "position" : 9, "weight" : 0.0694213 },
		{"id" : 3, "lemma" : "유혹", "type" : "NNG", "position" : 13, "weight" : 0.869213 },
		{"id" : 4, "lemma" : "》", "type" : "SS", "position" : 19, "weight" : 1 },
		{"id" : 5, "lemma" : "은", "type" : "JX", "position" : 22, "weight" : 0.0128817 },
		{"id" : 6, "lemma" : "귀여니", "type" : "NNP", "position" : 26, "weight" : 0.6 },
		{"id" : 7, "lemma" : "가", "type" : "JKS", "position" : 35, "weight" : 0.0431193 },
		{"id" : 8, "lemma" : "2003", "type" : "SN", "position" : 39, "weight" : 1 },
		{"id" : 9, "lemma" : "년", "type" : "NNB", "position" : 43, "weight" : 0.414343 },
		{"id" : 10, "lemma" : "에", "type" : "JKB", "position" : 46, "weight" : 0.135559 },
		{"id" : 11, "lemma" : "발간", "type" : "NNG", "position" : 50, "weight" : 0.9 },
		{"id" : 12, "lemma" : "하", "type" : "XSV", "position" : 56, "weight" : 0.0001 },
		{"id" : 13, "lemma" : "ㄴ", "type" : "ETM", "position" : 56, "weight" : 0.392321 },
		{"id" : 14, "lemma" : "두", "type" : "MM", "position" : 60, "weight" : 0.00826235 },
		{"id" : 15, "lemma" : "번째", "type" : "NNB", "position" : 64, "weight" : 0.18051 },
		{"id" : 16, "lemma" : "연애", "type" : "NNG", "position" : 71, "weight" : 0.9 },
		{"id" : 17, "lemma" : "소설", "type" : "NNG", "position" : 78, "weight" : 0.9 },
		{"id" : 18, "lemma" : "이", "type" : "VCP", "position" : 84, "weight" : 0.0177525 },
		{"id" : 19, "lemma" : "다", "type" : "EF", "position" : 87, "weight" : 0.353579 },
		{"id" : 20, "lemma" : ".", "type" : "SF", "position" : 90, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "《/SS+늑대/NNG+의/JKG", "target" : "《늑대의", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "유혹/NNG+》/SS+은/JX", "target" : "유혹》은", "word_id" : 1, "m_begin" : 3, "m_end" : 5},
		{"id" : 2, "result" : "귀여니/NNG+가/JKS", "target" : "귀여니가", "word_id" : 2, "m_begin" : 6, "m_end" : 7},
		{"id" : 3, "result" : "2003/SN+년/NNB+에/JKB", "target" : "2003년에", "word_id" : 3, "m_begin" : 8, "m_end" : 10},
		{"id" : 4, "result" : "발간하/VV+ㄴ/ETM", "target" : "발간한", "word_id" : 4, "m_begin" : 11, "m_end" : 13},
		{"id" : 5, "result" : "두/MM", "target" : "두", "word_id" : 5, "m_begin" : 14, "m_end" : 14},
		{"id" : 6, "result" : "번째/NNB", "target" : "번째", "word_id" : 6, "m_begin" : 15, "m_end" : 15},
		{"id" : 7, "result" : "연애/NNG", "target" : "연애", "word_id" : 7, "m_begin" : 16, "m_end" : 16},
		{"id" : 8, "result" : "소설/NNG+이/VCP+다/EF+./SF", "target" : "소설이다.", "word_id" : 8, "m_begin" : 17, "m_end" : 20}
	],
	"WSD" : [
		{"id" : 0, "text" : "《", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "늑대", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "유혹", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 13, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "》", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 19, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 22, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "귀여니", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 26, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "가", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 35, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "2003", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 39, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 43, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 46, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "발간하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 50, "begin" : 11, "end" : 12},
		{"id" : 12, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 56, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "두", "type" : "MM", "scode" : "01", "weight" : 1, "position" : 60, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "번째", "type" : "NNB", "scode" : "00", "weight" : 1, "position" : 64, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "연애", "type" : "NNG", "scode" : "05", "weight" : 1, "position" : 71, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "소설", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 78, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 84, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 87, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 90, "begin" : 20, "end" : 20}
	],
	"word" : [
		{"id" : 0, "text" : "《늑대의", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "유혹》은", "type" : "", "begin" : 3, "end" : 5},
		{"id" : 2, "text" : "귀여니가", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 3, "text" : "2003년에", "type" : "", "begin" : 8, "end" : 10},
		{"id" : 4, "text" : "발간한", "type" : "", "begin" : 11, "end" : 13},
		{"id" : 5, "text" : "두", "type" : "", "begin" : 14, "end" : 14},
		{"id" : 6, "text" : "번째", "type" : "", "begin" : 15, "end" : 15},
		{"id" : 7, "text" : "연애", "type" : "", "begin" : 16, "end" : 16},
		{"id" : 8, "text" : "소설이다.", "type" : "", "begin" : 17, "end" : 20}
	],
	"NE" : [
		{"id" : 0, "text" : "늑대의 유혹", "type" : "AFW_VIDEO", "begin" : 1, "end" : 3, "weight" : 0.982021, "common_noun" : 0},
		{"id" : 1, "text" : "귀여니", "type" : "PS_NAME", "begin" : 6, "end" : 6, "weight" : 0.313002, "common_noun" : 0},
		{"id" : 2, "text" : "2003년", "type" : "DT_YEAR", "begin" : 8, "end" : 9, "weight" : 0.694455, "common_noun" : 0},
		{"id" : 3, "text" : "두 번째", "type" : "QT_OTHERS", "begin" : 14, "end" : 15, "weight" : 0.781052, "common_noun" : 0},
		{"id" : 4, "text" : "연애 소설", "type" : "FD_ART", "begin" : 16, "end" : 17, "weight" : 0.490362, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "《늑대의", "head" : 1, "label" : "NP_MOD", "mod" : [], "weight" : 0.752602 },
		{"id" : 1, "text" : "유혹》은", "head" : 8, "label" : "NP_SBJ", "mod" : [0], "weight" : 0.863333 },
		{"id" : 2, "text" : "귀여니가", "head" : 4, "label" : "NP_SBJ", "mod" : [], "weight" : 0.60682 },
		{"id" : 3, "text" : "2003년에", "head" : 4, "label" : "NP_AJT", "mod" : [], "weight" : 0.961679 },
		{"id" : 4, "text" : "발간한", "head" : 6, "label" : "VP_MOD", "mod" : [2, 3], "weight" : 0.636903 },
		{"id" : 5, "text" : "두", "head" : 6, "label" : "DP", "mod" : [], "weight" : 0.835987 },
		{"id" : 6, "text" : "번째", "head" : 8, "label" : "NP_AJT", "mod" : [4, 5], "weight" : 0.331079 },
		{"id" : 7, "text" : "연애", "head" : 8, "label" : "NP", "mod" : [], "weight" : 0.379197 },
		{"id" : 8, "text" : "소설이다.", "head" : -1, "label" : "VNP", "mod" : [1, 6, 7], "weight" : 0.0146271 }
	],
	"SRL" : [
		{"verb" : "발간", "sense" : 1, "word_id" : 4, "weight" : 0.290609,
			"argument" : [
				{"type" : "ARG0", "word_id" : 2, "text" : "귀여니가", "weight" : 0.291121 },
				{"type" : "ARGM-TMP", "word_id" : 3, "text" : "2003년에", "weight" : 0.290097 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "2004년김태균 감독에 의해 강동원, 조한선, 이청아 주연으로 영화화되었다.",
	"morp" : [
		{"id" : 0, "lemma" : "2004", "type" : "SN", "position" : 91, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 95, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "김태균", "type" : "NNP", "position" : 98, "weight" : 0.1 },
		{"id" : 3, "lemma" : "감독", "type" : "NNG", "position" : 108, "weight" : 0.9 },
		{"id" : 4, "lemma" : "에", "type" : "JKB", "position" : 114, "weight" : 0.153364 },
		{"id" : 5, "lemma" : "의하", "type" : "VV", "position" : 118, "weight" : 0.9 },
		{"id" : 6, "lemma" : "어", "type" : "EC", "position" : 121, "weight" : 0.41831 },
		{"id" : 7, "lemma" : "강동원", "type" : "NNP", "position" : 125, "weight" : 0.1 },
		{"id" : 8, "lemma" : ",", "type" : "SP", "position" : 134, "weight" : 1 },
		{"id" : 9, "lemma" : "조한선", "type" : "NNP", "position" : 136, "weight" : 0.15 },
		{"id" : 10, "lemma" : ",", "type" : "SP", "position" : 145, "weight" : 1 },
		{"id" : 11, "lemma" : "이청아", "type" : "NNP", "position" : 147, "weight" : 0.6 },
		{"id" : 12, "lemma" : "주연", "type" : "NNG", "position" : 157, "weight" : 0.27545 },
		{"id" : 13, "lemma" : "으로", "type" : "JKB", "position" : 163, "weight" : 0.153406 },
		{"id" : 14, "lemma" : "영화", "type" : "NNG", "position" : 170, "weight" : 0.206384 },
		{"id" : 15, "lemma" : "화", "type" : "XSN", "position" : 176, "weight" : 0.0240371 },
		{"id" : 16, "lemma" : "되", "type" : "XSV", "position" : 179, "weight" : 0.000260989 },
		{"id" : 17, "lemma" : "었", "type" : "EP", "position" : 182, "weight" : 0.9 },
		{"id" : 18, "lemma" : "다", "type" : "EF", "position" : 185, "weight" : 0.640954 },
		{"id" : 19, "lemma" : ".", "type" : "SF", "position" : 188, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "2004/SN+년/NNB+김태균/NNG", "target" : "2004년김태균", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "감독/NNG+에/JKB", "target" : "감독에", "word_id" : 1, "m_begin" : 3, "m_end" : 4},
		{"id" : 2, "result" : "의하/VV+어/EC", "target" : "의해", "word_id" : 2, "m_begin" : 5, "m_end" : 6},
		{"id" : 3, "result" : "강동원/NNG+,/SP", "target" : "강동원,", "word_id" : 3, "m_begin" : 7, "m_end" : 8},
		{"id" : 4, "result" : "조한선/NNG+,/SP", "target" : "조한선,", "word_id" : 4, "m_begin" : 9, "m_end" : 10},
		{"id" : 5, "result" : "이청아/NNG", "target" : "이청아", "word_id" : 5, "m_begin" : 11, "m_end" : 11},
		{"id" : 6, "result" : "주연/NNG+으로/JKB", "target" : "주연으로", "word_id" : 6, "m_begin" : 12, "m_end" : 13},
		{"id" : 7, "result" : "영화화되/VV+었/EP+다/EF+./SF", "target" : "영화화되었다.", "word_id" : 7, "m_begin" : 14, "m_end" : 19}
	],
	"WSD" : [
		{"id" : 0, "text" : "2004", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 91, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 95, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "김태균", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 98, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "감독", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 108, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 114, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "의하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 118, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 121, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "강동원", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 125, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 134, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "조한선", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 136, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 145, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "이청아", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 147, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "주연", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 157, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 163, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "영화화되", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 170, "begin" : 14, "end" : 16},
		{"id" : 15, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 182, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 185, "begin" : 18, "end" : 18},
		{"id" : 17, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 188, "begin" : 19, "end" : 19}
	],
	"word" : [
		{"id" : 0, "text" : "2004년김태균", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "감독에", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 2, "text" : "의해", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 3, "text" : "강동원,", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 4, "text" : "조한선,", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 5, "text" : "이청아", "type" : "", "begin" : 11, "end" : 11},
		{"id" : 6, "text" : "주연으로", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 7, "text" : "영화화되었다.", "type" : "", "begin" : 14, "end" : 19}
	],
	"NE" : [
		{"id" : 0, "text" : "2004년", "type" : "DT_YEAR", "begin" : 0, "end" : 1, "weight" : 0.409795, "common_noun" : 0},
		{"id" : 1, "text" : "김태균", "type" : "PS_NAME", "begin" : 2, "end" : 2, "weight" : 0.804451, "common_noun" : 0},
		{"id" : 2, "text" : "감독", "type" : "CV_OCCUPATION", "begin" : 3, "end" : 3, "weight" : 0.511816, "common_noun" : 0},
		{"id" : 3, "text" : "강동원", "type" : "PS_NAME", "begin" : 7, "end" : 7, "weight" : 0.467015, "common_noun" : 0},
		{"id" : 4, "text" : "조한선", "type" : "PS_NAME", "begin" : 9, "end" : 9, "weight" : 0.547966, "common_noun" : 0},
		{"id" : 5, "text" : "이청아", "type" : "PS_NAME", "begin" : 11, "end" : 11, "weight" : 0.57942, "common_noun" : 0},
		{"id" : 6, "text" : "주연", "type" : "CV_POSITION", "begin" : 12, "end" : 12, "weight" : 0.318304, "common_noun" : 0},
		{"id" : 7, "text" : "영화", "type" : "FD_ART", "begin" : 14, "end" : 14, "weight" : 0.32333, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "2004년김태균", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.581639 },
		{"id" : 1, "text" : "감독에", "head" : 2, "label" : "NP_AJT", "mod" : [0], "weight" : 0.641134 },
		{"id" : 2, "text" : "의해", "head" : 7, "label" : "VP", "mod" : [1], "weight" : 0.588869 },
		{"id" : 3, "text" : "강동원,", "head" : 5, "label" : "NP_CNJ", "mod" : [], "weight" : 0.676703 },
		{"id" : 4, "text" : "조한선,", "head" : 5, "label" : "NP_CNJ", "mod" : [], "weight" : 0.601479 },
		{"id" : 5, "text" : "이청아", "head" : 6, "label" : "NP", "mod" : [3, 4], "weight" : 0.943703 },
		{"id" : 6, "text" : "주연으로", "head" : 7, "label" : "NP_AJT", "mod" : [5], "weight" : 0.551716 },
		{"id" : 7, "text" : "영화화되었다.", "head" : -1, "label" : "VP", "mod" : [2, 6], "weight" : 0.0240835 }
	],
	"SRL" : [
		{"verb" : "의하", "sense" : 1, "word_id" : 2, "weight" : 0.299995,
			"argument" : [
				{"type" : "ARG2", "word_id" : 1, "text" : "감독에", "weight" : 0.299995 }
			] },
		{"verb" : "영화", "sense" : 1, "word_id" : 7, "weight" : 0.0893896,
			"argument" : [
				{"type" : "ARGM-PRD", "word_id" : 2, "text" : "의해", "weight" : 0.0927705 },
				{"type" : "ARG2", "word_id" : 6, "text" : "주연으로", "weight" : 0.0860087 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 7, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.544733 }
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "2011년 뮤지컬화되었다.",
	"morp" : [
		{"id" : 0, "lemma" : "2011", "type" : "SN", "position" : 189, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 193, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "뮤지", "type" : "NNP", "position" : 197, "weight" : 0.6 },
		{"id" : 3, "lemma" : "컬화되", "type" : "VV", "position" : 203, "weight" : 0 },
		{"id" : 4, "lemma" : "었", "type" : "EP", "position" : 212, "weight" : 0.9 },
		{"id" : 5, "lemma" : "다", "type" : "EF", "position" : 215, "weight" : 0.640954 },
		{"id" : 6, "lemma" : ".", "type" : "SF", "position" : 218, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "2011/SN+년/NNB", "target" : "2011년", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "뮤지/NNG+컬화되/VV+었/EP+다/EF+./SF", "target" : "뮤지컬화되었다.", "word_id" : 1, "m_begin" : 2, "m_end" : 6}
	],
	"WSD" : [
		{"id" : 0, "text" : "2011", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 189, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 193, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "뮤지", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 197, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "컬화되", "type" : "VV", "scode" : "00", "weight" : 0, "position" : 203, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 212, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 215, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 218, "begin" : 6, "end" : 6}
	],
	"word" : [
		{"id" : 0, "text" : "2011년", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "뮤지컬화되었다.", "type" : "", "begin" : 2, "end" : 6}
	],
	"NE" : [
		{"id" : 0, "text" : "2011년", "type" : "DT_YEAR", "begin" : 0, "end" : 1, "weight" : 0.633522, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "2011년", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.412496 },
		{"id" : 1, "text" : "뮤지컬화되었다.", "head" : -1, "label" : "VNP", "mod" : [0], "weight" : 0.231968 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "PS_NAME", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 5, "sent_id" : 1, "start_eid" : 0, "end_eid" : 1, "ne_id" : 2, "text" : "2004년김태균 감독", "start_eid_short" : 0, "end_eid_short" : 1, "text_short" : "2004년김태균 감독", "weight" : 0.012 },
		{"id" : 6, "sent_id" : 1, "start_eid" : 0, "end_eid" : 0, "ne_id" : 1, "text" : "2004년김태균", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "2004년김태균", "weight" : 0.007 }
	] },
	{"id" : 1, "type" : "PS_NAME", "number" : "plural", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 7, "sent_id" : 1, "start_eid" : 3, "end_eid" : 6, "ne_id" : 6, "text" : "강동원, 조한선, 이청아 주연", "start_eid_short" : 3, "end_eid_short" : 6, "text_short" : "강동원, 조한선, 이청아 주연", "weight" : 0.015 },
		{"id" : 8, "sent_id" : 1, "start_eid" : 3, "end_eid" : 5, "ne_id" : 5, "text" : "강동원, 조한선, 이청아", "start_eid_short" : 3, "end_eid_short" : 5, "text_short" : "강동원, 조한선, 이청아", "weight" : 0.01 }
	] }
 ]
}

