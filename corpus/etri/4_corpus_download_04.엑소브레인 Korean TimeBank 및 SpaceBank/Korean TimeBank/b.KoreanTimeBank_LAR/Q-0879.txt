{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "이것은 조선 전기를 대표하는 그림으로 당시 양반들의 정신세계를 엿볼 수 있다.",
	"morp" : [
		{"id" : 0, "lemma" : "이것", "type" : "NP", "position" : 0, "weight" : 0.0733657 },
		{"id" : 1, "lemma" : "은", "type" : "JX", "position" : 6, "weight" : 0.191811 },
		{"id" : 2, "lemma" : "조선", "type" : "NNP", "position" : 10, "weight" : 0.0441558 },
		{"id" : 3, "lemma" : "전기", "type" : "NNG", "position" : 17, "weight" : 0.280641 },
		{"id" : 4, "lemma" : "를", "type" : "JKO", "position" : 23, "weight" : 0.137686 },
		{"id" : 5, "lemma" : "대표", "type" : "NNG", "position" : 27, "weight" : 0.9 },
		{"id" : 6, "lemma" : "하", "type" : "XSV", "position" : 33, "weight" : 0.0001 },
		{"id" : 7, "lemma" : "는", "type" : "ETM", "position" : 36, "weight" : 0.238503 },
		{"id" : 8, "lemma" : "그림", "type" : "NNG", "position" : 40, "weight" : 0.656598 },
		{"id" : 9, "lemma" : "으로", "type" : "JKB", "position" : 46, "weight" : 0.153406 },
		{"id" : 10, "lemma" : "당시", "type" : "NNG", "position" : 53, "weight" : 0.9 },
		{"id" : 11, "lemma" : "양반", "type" : "NNG", "position" : 60, "weight" : 0.9 },
		{"id" : 12, "lemma" : "들", "type" : "XSN", "position" : 66, "weight" : 0.0299841 },
		{"id" : 13, "lemma" : "의", "type" : "JKG", "position" : 69, "weight" : 0.120732 },
		{"id" : 14, "lemma" : "정신", "type" : "NNG", "position" : 73, "weight" : 0.871311 },
		{"id" : 15, "lemma" : "세계", "type" : "NNG", "position" : 79, "weight" : 0.9 },
		{"id" : 16, "lemma" : "를", "type" : "JKO", "position" : 85, "weight" : 0.137686 },
		{"id" : 17, "lemma" : "엿보", "type" : "VV", "position" : 89, "weight" : 0.9 },
		{"id" : 18, "lemma" : "ㄹ", "type" : "ETM", "position" : 92, "weight" : 0.300746 },
		{"id" : 19, "lemma" : "수", "type" : "NNB", "position" : 96, "weight" : 0.215617 },
		{"id" : 20, "lemma" : "있", "type" : "VA", "position" : 100, "weight" : 0.0518488 },
		{"id" : 21, "lemma" : "다", "type" : "EF", "position" : 103, "weight" : 0.132573 },
		{"id" : 22, "lemma" : ".", "type" : "SF", "position" : 106, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이것/NP+은/JX", "target" : "이것은", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "조선/NNG", "target" : "조선", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "전기/NNG+를/JKO", "target" : "전기를", "word_id" : 2, "m_begin" : 3, "m_end" : 4},
		{"id" : 3, "result" : "대표하/VV+는/ETM", "target" : "대표하는", "word_id" : 3, "m_begin" : 5, "m_end" : 7},
		{"id" : 4, "result" : "그림/NNG+으로/JKB", "target" : "그림으로", "word_id" : 4, "m_begin" : 8, "m_end" : 9},
		{"id" : 5, "result" : "당시/NNG", "target" : "당시", "word_id" : 5, "m_begin" : 10, "m_end" : 10},
		{"id" : 6, "result" : "양반들/NNG+의/JKG", "target" : "양반들의", "word_id" : 6, "m_begin" : 11, "m_end" : 13},
		{"id" : 7, "result" : "정신세계/NNG+를/JKO", "target" : "정신세계를", "word_id" : 7, "m_begin" : 14, "m_end" : 16},
		{"id" : 8, "result" : "엿보/VV+ㄹ/ETM", "target" : "엿볼", "word_id" : 8, "m_begin" : 17, "m_end" : 18},
		{"id" : 9, "result" : "수/NNB", "target" : "수", "word_id" : 9, "m_begin" : 19, "m_end" : 19},
		{"id" : 10, "result" : "있/VA+다/EF+./SF", "target" : "있다.", "word_id" : 10, "m_begin" : 20, "m_end" : 22}
	],
	"WSD" : [
		{"id" : 0, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 6, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "조선", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "전기", "type" : "NNG", "scode" : "15", "weight" : 1, "position" : 17, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 23, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "대표하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 27, "begin" : 5, "end" : 6},
		{"id" : 6, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 36, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "그림", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 40, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 46, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "당시", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 53, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "양반", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 60, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "들", "type" : "XSN", "scode" : "09", "weight" : 1, "position" : 66, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 69, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "정신세계", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 73, "begin" : 14, "end" : 15},
		{"id" : 14, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 85, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : "엿보", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 89, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : "ㄹ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 92, "begin" : 18, "end" : 18},
		{"id" : 17, "text" : "수", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 96, "begin" : 19, "end" : 19},
		{"id" : 18, "text" : "있", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 100, "begin" : 20, "end" : 20},
		{"id" : 19, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 103, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 106, "begin" : 22, "end" : 22}
	],
	"word" : [
		{"id" : 0, "text" : "이것은", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "조선", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "전기를", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 3, "text" : "대표하는", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 4, "text" : "그림으로", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 5, "text" : "당시", "type" : "", "begin" : 10, "end" : 10},
		{"id" : 6, "text" : "양반들의", "type" : "", "begin" : 11, "end" : 13},
		{"id" : 7, "text" : "정신세계를", "type" : "", "begin" : 14, "end" : 16},
		{"id" : 8, "text" : "엿볼", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 9, "text" : "수", "type" : "", "begin" : 19, "end" : 19},
		{"id" : 10, "text" : "있다.", "type" : "", "begin" : 20, "end" : 22}
	],
	"NE" : [
		{"id" : 0, "text" : "조선 전기", "type" : "DT_DYNASTY", "begin" : 2, "end" : 3, "weight" : 0.65849, "common_noun" : 0},
		{"id" : 1, "text" : "양반", "type" : "CV_POSITION", "begin" : 11, "end" : 11, "weight" : 0.586453, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이것은", "head" : 4, "label" : "NP_SBJ", "mod" : [], "weight" : 0.206284 },
		{"id" : 1, "text" : "조선", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.77588 },
		{"id" : 2, "text" : "전기를", "head" : 3, "label" : "NP_OBJ", "mod" : [1], "weight" : 0.646191 },
		{"id" : 3, "text" : "대표하는", "head" : 4, "label" : "VP_MOD", "mod" : [2], "weight" : 0.784351 },
		{"id" : 4, "text" : "그림으로", "head" : 8, "label" : "NP_AJT", "mod" : [3, 0], "weight" : 0.687131 },
		{"id" : 5, "text" : "당시", "head" : 6, "label" : "NP", "mod" : [], "weight" : 0.381883 },
		{"id" : 6, "text" : "양반들의", "head" : 7, "label" : "NP_MOD", "mod" : [5], "weight" : 0.761124 },
		{"id" : 7, "text" : "정신세계를", "head" : 8, "label" : "NP_OBJ", "mod" : [6], "weight" : 0.733422 },
		{"id" : 8, "text" : "엿볼", "head" : 9, "label" : "VP_MOD", "mod" : [4, 7], "weight" : 0.741674 },
		{"id" : 9, "text" : "수", "head" : 10, "label" : "NP_SBJ", "mod" : [8], "weight" : 0.743991 },
		{"id" : 10, "text" : "있다.", "head" : -1, "label" : "VP", "mod" : [9], "weight" : 0.00435779 }
	],
	"SRL" : [
		{"verb" : "대표", "sense" : 1, "word_id" : 3, "weight" : 0.229726,
			"argument" : [
				{"type" : "ARG1", "word_id" : 2, "text" : "전기를", "weight" : 0.24727 },
				{"type" : "ARG0", "word_id" : 4, "text" : "그림으로", "weight" : 0.212182 }
			] },
		{"verb" : "엿보", "sense" : 1, "word_id" : 8, "weight" : 0.63427,
			"argument" : [
				{"type" : "ARG1", "word_id" : 7, "text" : "정신세계를", "weight" : 0.522145 },
				{"type" : "AUX", "word_id" : 10, "text" : "있다.", "weight" : 0.746395 }
			] },
		{"verb" : "있", "sense" : 1, "word_id" : 10, "weight" : 0.240684,
			"argument" : [
				{"type" : "ARG1", "word_id" : 4, "text" : "그림으로", "weight" : 0.210774 },
				{"type" : "ARGM-MNR", "word_id" : 8, "text" : "엿볼", "weight" : 0.270594 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 8, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.518543 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "비단에 수묵 담채로 표현한 산수화로 왼편의 현실 세계와 오른편의 이상세계가 대조를 이루고 있는데 이 그림은 무엇일까?",
	"morp" : [
		{"id" : 0, "lemma" : "비단", "type" : "NNG", "position" : 107, "weight" : 0.325051 },
		{"id" : 1, "lemma" : "에", "type" : "JKB", "position" : 113, "weight" : 0.153364 },
		{"id" : 2, "lemma" : "수묵", "type" : "NNG", "position" : 117, "weight" : 0.95 },
		{"id" : 3, "lemma" : "담채", "type" : "NNG", "position" : 124, "weight" : 0.2 },
		{"id" : 4, "lemma" : "로", "type" : "JKB", "position" : 130, "weight" : 0.153229 },
		{"id" : 5, "lemma" : "표현", "type" : "NNG", "position" : 134, "weight" : 0.9 },
		{"id" : 6, "lemma" : "하", "type" : "XSV", "position" : 140, "weight" : 0.0001 },
		{"id" : 7, "lemma" : "ㄴ", "type" : "ETM", "position" : 140, "weight" : 0.392321 },
		{"id" : 8, "lemma" : "산수", "type" : "NNG", "position" : 144, "weight" : 0.9 },
		{"id" : 9, "lemma" : "화", "type" : "NNG", "position" : 150, "weight" : 0.056252 },
		{"id" : 10, "lemma" : "로", "type" : "JKB", "position" : 153, "weight" : 0.153229 },
		{"id" : 11, "lemma" : "왼편", "type" : "NNG", "position" : 157, "weight" : 0.9 },
		{"id" : 12, "lemma" : "의", "type" : "JKG", "position" : 163, "weight" : 0.0694213 },
		{"id" : 13, "lemma" : "현실", "type" : "NNG", "position" : 167, "weight" : 0.9 },
		{"id" : 14, "lemma" : "세계", "type" : "NNG", "position" : 174, "weight" : 0.9 },
		{"id" : 15, "lemma" : "와", "type" : "JC", "position" : 180, "weight" : 0.0169714 },
		{"id" : 16, "lemma" : "오른편", "type" : "NNG", "position" : 184, "weight" : 0.9 },
		{"id" : 17, "lemma" : "의", "type" : "JKG", "position" : 193, "weight" : 0.0694213 },
		{"id" : 18, "lemma" : "이상", "type" : "NNG", "position" : 197, "weight" : 0.866213 },
		{"id" : 19, "lemma" : "세계", "type" : "NNG", "position" : 203, "weight" : 0.9 },
		{"id" : 20, "lemma" : "가", "type" : "JKS", "position" : 209, "weight" : 0.066324 },
		{"id" : 21, "lemma" : "대조", "type" : "NNG", "position" : 213, "weight" : 0.9 },
		{"id" : 22, "lemma" : "를", "type" : "JKO", "position" : 219, "weight" : 0.137686 },
		{"id" : 23, "lemma" : "이루", "type" : "VV", "position" : 223, "weight" : 0.760047 },
		{"id" : 24, "lemma" : "고", "type" : "EC", "position" : 229, "weight" : 0.416679 },
		{"id" : 25, "lemma" : "있", "type" : "VX", "position" : 233, "weight" : 0.125953 },
		{"id" : 26, "lemma" : "는데", "type" : "EC", "position" : 236, "weight" : 0.252751 },
		{"id" : 27, "lemma" : "이", "type" : "MM", "position" : 243, "weight" : 0.000947656 },
		{"id" : 28, "lemma" : "그림", "type" : "NNG", "position" : 247, "weight" : 0.734943 },
		{"id" : 29, "lemma" : "은", "type" : "JX", "position" : 253, "weight" : 0.0449928 },
		{"id" : 30, "lemma" : "무엇", "type" : "NP", "position" : 257, "weight" : 0.9 },
		{"id" : 31, "lemma" : "이", "type" : "VCP", "position" : 263, "weight" : 0.0175768 },
		{"id" : 32, "lemma" : "ㄹ까", "type" : "EF", "position" : 263, "weight" : 0.258243 },
		{"id" : 33, "lemma" : "?", "type" : "SF", "position" : 269, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "비단/NNG+에/JKB", "target" : "비단에", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "수묵/NNG", "target" : "수묵", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "담채/NNG+로/JKB", "target" : "담채로", "word_id" : 2, "m_begin" : 3, "m_end" : 4},
		{"id" : 3, "result" : "표현하/VV+ㄴ/ETM", "target" : "표현한", "word_id" : 3, "m_begin" : 5, "m_end" : 7},
		{"id" : 4, "result" : "산수화/NNG+로/JKB", "target" : "산수화로", "word_id" : 4, "m_begin" : 8, "m_end" : 10},
		{"id" : 5, "result" : "왼편/NNG+의/JKG", "target" : "왼편의", "word_id" : 5, "m_begin" : 11, "m_end" : 12},
		{"id" : 6, "result" : "현실/NNG", "target" : "현실", "word_id" : 6, "m_begin" : 13, "m_end" : 13},
		{"id" : 7, "result" : "세계/NNG+와/JC", "target" : "세계와", "word_id" : 7, "m_begin" : 14, "m_end" : 15},
		{"id" : 8, "result" : "오른편/NNG+의/JKG", "target" : "오른편의", "word_id" : 8, "m_begin" : 16, "m_end" : 17},
		{"id" : 9, "result" : "이상세계/NNG+가/JKS", "target" : "이상세계가", "word_id" : 9, "m_begin" : 18, "m_end" : 20},
		{"id" : 10, "result" : "대조/NNG+를/JKO", "target" : "대조를", "word_id" : 10, "m_begin" : 21, "m_end" : 22},
		{"id" : 11, "result" : "이루/VV+고/EC", "target" : "이루고", "word_id" : 11, "m_begin" : 23, "m_end" : 24},
		{"id" : 12, "result" : "있/VX+는데/EC", "target" : "있는데", "word_id" : 12, "m_begin" : 25, "m_end" : 26},
		{"id" : 13, "result" : "이/MM", "target" : "이", "word_id" : 13, "m_begin" : 27, "m_end" : 27},
		{"id" : 14, "result" : "그림/NNG+은/JX", "target" : "그림은", "word_id" : 14, "m_begin" : 28, "m_end" : 29},
		{"id" : 15, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 15, "m_begin" : 30, "m_end" : 33}
	],
	"WSD" : [
		{"id" : 0, "text" : "비단", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 107, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 113, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "수묵", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 117, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "담채", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 124, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 130, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "표현하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 134, "begin" : 5, "end" : 6},
		{"id" : 6, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 140, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "산수화", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 144, "begin" : 8, "end" : 9},
		{"id" : 8, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 153, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "왼편", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 157, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 163, "begin" : 12, "end" : 12},
		{"id" : 11, "text" : "현실", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 167, "begin" : 13, "end" : 13},
		{"id" : 12, "text" : "세계", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 174, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "와", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 180, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : "오른편", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 184, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 193, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : "이상", "type" : "NNG", "scode" : "05", "weight" : 1, "position" : 197, "begin" : 18, "end" : 18},
		{"id" : 17, "text" : "세계", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 203, "begin" : 19, "end" : 19},
		{"id" : 18, "text" : "가", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 209, "begin" : 20, "end" : 20},
		{"id" : 19, "text" : "대조", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 213, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 219, "begin" : 22, "end" : 22},
		{"id" : 21, "text" : "이루", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 223, "begin" : 23, "end" : 23},
		{"id" : 22, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 229, "begin" : 24, "end" : 24},
		{"id" : 23, "text" : "있", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 233, "begin" : 25, "end" : 25},
		{"id" : 24, "text" : "는데", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 236, "begin" : 26, "end" : 26},
		{"id" : 25, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 243, "begin" : 27, "end" : 27},
		{"id" : 26, "text" : "그림", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 247, "begin" : 28, "end" : 28},
		{"id" : 27, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 253, "begin" : 29, "end" : 29},
		{"id" : 28, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 257, "begin" : 30, "end" : 30},
		{"id" : 29, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 263, "begin" : 31, "end" : 31},
		{"id" : 30, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 263, "begin" : 32, "end" : 32},
		{"id" : 31, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 269, "begin" : 33, "end" : 33}
	],
	"word" : [
		{"id" : 0, "text" : "비단에", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "수묵", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "담채로", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 3, "text" : "표현한", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 4, "text" : "산수화로", "type" : "", "begin" : 8, "end" : 10},
		{"id" : 5, "text" : "왼편의", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 6, "text" : "현실", "type" : "", "begin" : 13, "end" : 13},
		{"id" : 7, "text" : "세계와", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 8, "text" : "오른편의", "type" : "", "begin" : 16, "end" : 17},
		{"id" : 9, "text" : "이상세계가", "type" : "", "begin" : 18, "end" : 20},
		{"id" : 10, "text" : "대조를", "type" : "", "begin" : 21, "end" : 22},
		{"id" : 11, "text" : "이루고", "type" : "", "begin" : 23, "end" : 24},
		{"id" : 12, "text" : "있는데", "type" : "", "begin" : 25, "end" : 26},
		{"id" : 13, "text" : "이", "type" : "", "begin" : 27, "end" : 27},
		{"id" : 14, "text" : "그림은", "type" : "", "begin" : 28, "end" : 29},
		{"id" : 15, "text" : "무엇일까?", "type" : "", "begin" : 30, "end" : 33}
	],
	"NE" : [
		{"id" : 0, "text" : "비단", "type" : "CV_CLOTHING", "begin" : 0, "end" : 0, "weight" : 0.252838, "common_noun" : 0},
		{"id" : 1, "text" : "수묵 담채", "type" : "TR_ART", "begin" : 2, "end" : 3, "weight" : 0.211176, "common_noun" : 0},
		{"id" : 2, "text" : "산수화", "type" : "FD_ART", "begin" : 8, "end" : 9, "weight" : 0.815998, "common_noun" : 0},
		{"id" : 3, "text" : "왼편", "type" : "TM_DIRECTION", "begin" : 11, "end" : 11, "weight" : 0.204733, "common_noun" : 0},
		{"id" : 4, "text" : "오른편", "type" : "TM_DIRECTION", "begin" : 16, "end" : 16, "weight" : 0.311693, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "비단에", "head" : 3, "label" : "NP_AJT", "mod" : [], "weight" : 0.39709 },
		{"id" : 1, "text" : "수묵", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.700066 },
		{"id" : 2, "text" : "담채로", "head" : 3, "label" : "NP_AJT", "mod" : [1], "weight" : 0.837047 },
		{"id" : 3, "text" : "표현한", "head" : 4, "label" : "VP_MOD", "mod" : [0, 2], "weight" : 0.81749 },
		{"id" : 4, "text" : "산수화로", "head" : 11, "label" : "NP_AJT", "mod" : [3], "weight" : 0.73989 },
		{"id" : 5, "text" : "왼편의", "head" : 7, "label" : "NP_MOD", "mod" : [], "weight" : 0.373455 },
		{"id" : 6, "text" : "현실", "head" : 7, "label" : "NP", "mod" : [], "weight" : 0.739595 },
		{"id" : 7, "text" : "세계와", "head" : 9, "label" : "NP_CNJ", "mod" : [5, 6], "weight" : 0.603764 },
		{"id" : 8, "text" : "오른편의", "head" : 9, "label" : "NP_MOD", "mod" : [], "weight" : 0.717359 },
		{"id" : 9, "text" : "이상세계가", "head" : 11, "label" : "NP_SBJ", "mod" : [7, 8], "weight" : 0.703185 },
		{"id" : 10, "text" : "대조를", "head" : 11, "label" : "NP_OBJ", "mod" : [], "weight" : 0.698102 },
		{"id" : 11, "text" : "이루고", "head" : 12, "label" : "VP", "mod" : [4, 9, 10], "weight" : 0.802904 },
		{"id" : 12, "text" : "있는데", "head" : 15, "label" : "VP", "mod" : [11], "weight" : 0.725049 },
		{"id" : 13, "text" : "이", "head" : 14, "label" : "DP", "mod" : [], "weight" : 0.653209 },
		{"id" : 14, "text" : "그림은", "head" : 15, "label" : "NP_SBJ", "mod" : [13], "weight" : 0.741096 },
		{"id" : 15, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [12, 14], "weight" : 0.00208793 }
	],
	"SRL" : [
		{"verb" : "표현", "sense" : 1, "word_id" : 3, "weight" : 0.203818,
			"argument" : [
				{"type" : "ARG1", "word_id" : 0, "text" : "비단에", "weight" : 0.102244 },
				{"type" : "ARGM-MNR", "word_id" : 2, "text" : "담채로", "weight" : 0.162983 },
				{"type" : "ARG1", "word_id" : 4, "text" : "산수화로", "weight" : 0.346227 }
			] },
		{"verb" : "이루", "sense" : 1, "word_id" : 11, "weight" : 0.365649,
			"argument" : [
				{"type" : "ARGM-PRD", "word_id" : 4, "text" : "산수화로", "weight" : 0.112921 },
				{"type" : "ARG0", "word_id" : 9, "text" : "이상세계가", "weight" : 0.250432 },
				{"type" : "ARG1", "word_id" : 10, "text" : "대조를", "weight" : 0.483358 },
				{"type" : "AUX", "word_id" : 12, "text" : "있는데", "weight" : 0.615884 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 4, "ne_id" : -1, "text" : "이것은 조선 전기를 대표하는 그림", "start_eid_short" : 4, "end_eid_short" : 4, "text_short" : "그림", "weight" : 0.002 },
		{"id" : 1, "sent_id" : 0, "start_eid" : 0, "end_eid" : 0, "ne_id" : -1, "text" : "이것", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "이것", "weight" : 0.018 },
		{"id" : 15, "sent_id" : 1, "start_eid" : 13, "end_eid" : 14, "ne_id" : -1, "text" : "이 그림", "start_eid_short" : 13, "end_eid_short" : 14, "text_short" : "이 그림", "weight" : 0.006 },
		{"id" : 16, "sent_id" : 1, "start_eid" : 15, "end_eid" : 15, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 15, "end_eid_short" : 15, "text_short" : "무엇이ㄹ까?", "weight" : 0.006 }
	] }
 ]
}
