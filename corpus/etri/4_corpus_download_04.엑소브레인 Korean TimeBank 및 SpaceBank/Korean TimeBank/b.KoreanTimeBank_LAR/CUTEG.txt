{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "CUTEG(1984년1월 6일 ~ )는 대한민국의 만화가·일러스트레이터이다.",
	"morp" : [
		{"id" : 0, "lemma" : "CUTEG", "type" : "SL", "position" : 0, "weight" : 1 },
		{"id" : 1, "lemma" : "(", "type" : "SS", "position" : 5, "weight" : 1 },
		{"id" : 2, "lemma" : "1984", "type" : "SN", "position" : 6, "weight" : 1 },
		{"id" : 3, "lemma" : "년", "type" : "NNB", "position" : 10, "weight" : 0.414343 },
		{"id" : 4, "lemma" : "1", "type" : "SN", "position" : 13, "weight" : 1 },
		{"id" : 5, "lemma" : "월", "type" : "NNB", "position" : 14, "weight" : 0.408539 },
		{"id" : 6, "lemma" : "6", "type" : "SN", "position" : 18, "weight" : 1 },
		{"id" : 7, "lemma" : "일", "type" : "NNB", "position" : 19, "weight" : 0.126777 },
		{"id" : 8, "lemma" : "~", "type" : "SO", "position" : 23, "weight" : 1 },
		{"id" : 9, "lemma" : ")", "type" : "SS", "position" : 25, "weight" : 1 },
		{"id" : 10, "lemma" : "는", "type" : "JX", "position" : 26, "weight" : 0.00823314 },
		{"id" : 11, "lemma" : "대한민국", "type" : "NNP", "position" : 30, "weight" : 0.0447775 },
		{"id" : 12, "lemma" : "의", "type" : "JKG", "position" : 42, "weight" : 0.0987295 },
		{"id" : 13, "lemma" : "만화", "type" : "NNG", "position" : 46, "weight" : 0.83848 },
		{"id" : 14, "lemma" : "가", "type" : "XSN", "position" : 52, "weight" : 0.000115417 },
		{"id" : 15, "lemma" : "·", "type" : "SW", "position" : 55, "weight" : 1 },
		{"id" : 16, "lemma" : "일러스트레이터", "type" : "NNG", "position" : 57, "weight" : 0.15 },
		{"id" : 17, "lemma" : "이", "type" : "VCP", "position" : 78, "weight" : 0.0177525 },
		{"id" : 18, "lemma" : "다", "type" : "EF", "position" : 81, "weight" : 0.353579 },
		{"id" : 19, "lemma" : ".", "type" : "SF", "position" : 84, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "CUTEG/SL+(/SS+1984/SN+년/NNB+1/SN+월/NNB", "target" : "CUTEG(1984년1월", "word_id" : 0, "m_begin" : 0, "m_end" : 5},
		{"id" : 1, "result" : "6/SN+일/NNB", "target" : "6일", "word_id" : 1, "m_begin" : 6, "m_end" : 7},
		{"id" : 2, "result" : "~/SO", "target" : "~", "word_id" : 2, "m_begin" : 8, "m_end" : 8},
		{"id" : 3, "result" : ")/SS+는/JX", "target" : ")는", "word_id" : 3, "m_begin" : 9, "m_end" : 10},
		{"id" : 4, "result" : "대한민국/NNG+의/JKG", "target" : "대한민국의", "word_id" : 4, "m_begin" : 11, "m_end" : 12},
		{"id" : 5, "result" : "만화가/NNG+·/SW+일러스트레이터/NNG+이/VCP+다/EF+./SF", "target" : "만화가·일러스트레이터이다.", "word_id" : 5, "m_begin" : 13, "m_end" : 19}
	],
	"WSD" : [
		{"id" : 0, "text" : "CUTEG", "type" : "SL", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 5, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "1984", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 6, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 10, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "1", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 13, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "월", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 14, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "6", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 18, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "일", "type" : "NNB", "scode" : "07", "weight" : 1, "position" : 19, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "~", "type" : "SO", "scode" : "00", "weight" : 1, "position" : 23, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 25, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 26, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "대한민국", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 30, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 42, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "만화가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 46, "begin" : 13, "end" : 14},
		{"id" : 14, "text" : "·", "type" : "SW", "scode" : "00", "weight" : 1, "position" : 55, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "일러스트레이터", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 57, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 78, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 81, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 84, "begin" : 19, "end" : 19}
	],
	"word" : [
		{"id" : 0, "text" : "CUTEG(1984년1월", "type" : "", "begin" : 0, "end" : 5},
		{"id" : 1, "text" : "6일", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 2, "text" : "~", "type" : "", "begin" : 8, "end" : 8},
		{"id" : 3, "text" : ")는", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 4, "text" : "대한민국의", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 5, "text" : "만화가·일러스트레이터이다.", "type" : "", "begin" : 13, "end" : 19}
	],
	"NE" : [
		{"id" : 0, "text" : "CUTEG", "type" : "OGG_SCIENCE", "begin" : 0, "end" : 0, "weight" : 0.140258, "common_noun" : 0},
		{"id" : 1, "text" : "1984년1월 6일 ~", "type" : "DT_OTHERS", "begin" : 2, "end" : 8, "weight" : 0.691515, "common_noun" : 0},
		{"id" : 2, "text" : "대한민국", "type" : "LCP_COUNTRY", "begin" : 11, "end" : 11, "weight" : 0.174015, "common_noun" : 0},
		{"id" : 3, "text" : "만화가", "type" : "CV_OCCUPATION", "begin" : 13, "end" : 14, "weight" : 0.214394, "common_noun" : 0},
		{"id" : 4, "text" : "일러스트레이터", "type" : "CV_OCCUPATION", "begin" : 16, "end" : 16, "weight" : 0.194824, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "CUTEG(1984년1월", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.584925 },
		{"id" : 1, "text" : "6일", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.436086 },
		{"id" : 2, "text" : "~", "head" : 3, "label" : "X", "mod" : [1], "weight" : 0.745449 },
		{"id" : 3, "text" : ")는", "head" : 5, "label" : "NP_SBJ", "mod" : [2], "weight" : 0.456842 },
		{"id" : 4, "text" : "대한민국의", "head" : 5, "label" : "NP_MOD", "mod" : [], "weight" : 0.43961 },
		{"id" : 5, "text" : "만화가·일러스트레이터이다.", "head" : -1, "label" : "VNP", "mod" : [3, 4], "weight" : 0.0189528 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "2009년의 코믹 마켓 77에서 'StrawberryPink' 명의로, 2010년의 선샤인 크리에이션에서 'ONIGIRIズ' 명의로 직접 참가했다.",
	"morp" : [
		{"id" : 0, "lemma" : "2009", "type" : "SN", "position" : 85, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 89, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "의", "type" : "JKG", "position" : 92, "weight" : 0.0520933 },
		{"id" : 3, "lemma" : "코믹", "type" : "NNG", "position" : 96, "weight" : 0.9 },
		{"id" : 4, "lemma" : "마켓", "type" : "NNG", "position" : 103, "weight" : 0.9 },
		{"id" : 5, "lemma" : "77", "type" : "SN", "position" : 110, "weight" : 1 },
		{"id" : 6, "lemma" : "에서", "type" : "JKB", "position" : 112, "weight" : 0.00395809 },
		{"id" : 7, "lemma" : "'", "type" : "SS", "position" : 119, "weight" : 1 },
		{"id" : 8, "lemma" : "StrawberryPink", "type" : "SL", "position" : 120, "weight" : 1 },
		{"id" : 9, "lemma" : "'", "type" : "SS", "position" : 134, "weight" : 1 },
		{"id" : 10, "lemma" : "명의", "type" : "NNG", "position" : 136, "weight" : 0.9 },
		{"id" : 11, "lemma" : "로", "type" : "JKB", "position" : 142, "weight" : 0.153229 },
		{"id" : 12, "lemma" : ",", "type" : "SP", "position" : 145, "weight" : 1 },
		{"id" : 13, "lemma" : "2010", "type" : "SN", "position" : 147, "weight" : 1 },
		{"id" : 14, "lemma" : "년", "type" : "NNB", "position" : 151, "weight" : 0.414343 },
		{"id" : 15, "lemma" : "의", "type" : "JKG", "position" : 154, "weight" : 0.0520933 },
		{"id" : 16, "lemma" : "선샤", "type" : "NNP", "position" : 158, "weight" : 0.6 },
		{"id" : 17, "lemma" : "이", "type" : "VCP", "position" : 164, "weight" : 0.00359898 },
		{"id" : 18, "lemma" : "ㄴ", "type" : "ETM", "position" : 164, "weight" : 0.220712 },
		{"id" : 19, "lemma" : "크리에이션", "type" : "NNP", "position" : 168, "weight" : 0.05 },
		{"id" : 20, "lemma" : "에서", "type" : "JKB", "position" : 183, "weight" : 0.0823859 },
		{"id" : 21, "lemma" : "'", "type" : "SS", "position" : 190, "weight" : 1 },
		{"id" : 22, "lemma" : "ONIGIRIズ", "type" : "SL", "position" : 191, "weight" : 1 },
		{"id" : 23, "lemma" : "'", "type" : "SS", "position" : 201, "weight" : 1 },
		{"id" : 24, "lemma" : "명의", "type" : "NNG", "position" : 203, "weight" : 0.9 },
		{"id" : 25, "lemma" : "로", "type" : "JKB", "position" : 209, "weight" : 0.153229 },
		{"id" : 26, "lemma" : "직접", "type" : "MAG", "position" : 213, "weight" : 0.0492981 },
		{"id" : 27, "lemma" : "참가", "type" : "NNG", "position" : 220, "weight" : 0.9 },
		{"id" : 28, "lemma" : "하", "type" : "XSV", "position" : 226, "weight" : 0.0001 },
		{"id" : 29, "lemma" : "었", "type" : "EP", "position" : 226, "weight" : 0.9 },
		{"id" : 30, "lemma" : "다", "type" : "EF", "position" : 229, "weight" : 0.640954 },
		{"id" : 31, "lemma" : ".", "type" : "SF", "position" : 232, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "2009/SN+년/NNB+의/JKG", "target" : "2009년의", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "코믹/NNG", "target" : "코믹", "word_id" : 1, "m_begin" : 3, "m_end" : 3},
		{"id" : 2, "result" : "마켓/NNG", "target" : "마켓", "word_id" : 2, "m_begin" : 4, "m_end" : 4},
		{"id" : 3, "result" : "77/SN+에서/JKB", "target" : "77에서", "word_id" : 3, "m_begin" : 5, "m_end" : 6},
		{"id" : 4, "result" : "'/SS+StrawberryPink/SL+'/SS", "target" : "'StrawberryPink'", "word_id" : 4, "m_begin" : 7, "m_end" : 9},
		{"id" : 5, "result" : "명의/NNG+로/JKB+,/SP", "target" : "명의로,", "word_id" : 5, "m_begin" : 10, "m_end" : 12},
		{"id" : 6, "result" : "2010/SN+년/NNB+의/JKG", "target" : "2010년의", "word_id" : 6, "m_begin" : 13, "m_end" : 15},
		{"id" : 7, "result" : "선샤/NNG+이/VCP+ㄴ/ETM", "target" : "선샤인", "word_id" : 7, "m_begin" : 16, "m_end" : 18},
		{"id" : 8, "result" : "크리에이션/NNG+에서/JKB", "target" : "크리에이션에서", "word_id" : 8, "m_begin" : 19, "m_end" : 20},
		{"id" : 9, "result" : "'/SS+ONIGIRIズ/SL+'/SS", "target" : "'ONIGIRIズ'", "word_id" : 9, "m_begin" : 21, "m_end" : 23},
		{"id" : 10, "result" : "명의/NNG+로/JKB", "target" : "명의로", "word_id" : 10, "m_begin" : 24, "m_end" : 25},
		{"id" : 11, "result" : "직접/MAG", "target" : "직접", "word_id" : 11, "m_begin" : 26, "m_end" : 26},
		{"id" : 12, "result" : "참가하/VV+었/EP+다/EF+./SF", "target" : "참가했다.", "word_id" : 12, "m_begin" : 27, "m_end" : 31}
	],
	"WSD" : [
		{"id" : 0, "text" : "2009", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 85, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 89, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 92, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "코믹", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 96, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "마켓", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 103, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "77", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 110, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 112, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 119, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "StrawberryPink", "type" : "SL", "scode" : "00", "weight" : 1, "position" : 120, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 134, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "명의", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 136, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 142, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 145, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "2010", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 147, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 151, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 154, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "선샤", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 158, "begin" : 16, "end" : 16},
		{"id" : 17, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 164, "begin" : 17, "end" : 17},
		{"id" : 18, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 164, "begin" : 18, "end" : 18},
		{"id" : 19, "text" : "크리에이션", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 168, "begin" : 19, "end" : 19},
		{"id" : 20, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 183, "begin" : 20, "end" : 20},
		{"id" : 21, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 190, "begin" : 21, "end" : 21},
		{"id" : 22, "text" : "ONIGIRIズ", "type" : "SL", "scode" : "00", "weight" : 1, "position" : 191, "begin" : 22, "end" : 22},
		{"id" : 23, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 201, "begin" : 23, "end" : 23},
		{"id" : 24, "text" : "명의", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 203, "begin" : 24, "end" : 24},
		{"id" : 25, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 209, "begin" : 25, "end" : 25},
		{"id" : 26, "text" : "직접", "type" : "MAG", "scode" : "00", "weight" : 1, "position" : 213, "begin" : 26, "end" : 26},
		{"id" : 27, "text" : "참가하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 220, "begin" : 27, "end" : 28},
		{"id" : 28, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 226, "begin" : 29, "end" : 29},
		{"id" : 29, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 229, "begin" : 30, "end" : 30},
		{"id" : 30, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 232, "begin" : 31, "end" : 31}
	],
	"word" : [
		{"id" : 0, "text" : "2009년의", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "코믹", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 2, "text" : "마켓", "type" : "", "begin" : 4, "end" : 4},
		{"id" : 3, "text" : "77에서", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 4, "text" : "'StrawberryPink'", "type" : "", "begin" : 7, "end" : 9},
		{"id" : 5, "text" : "명의로,", "type" : "", "begin" : 10, "end" : 12},
		{"id" : 6, "text" : "2010년의", "type" : "", "begin" : 13, "end" : 15},
		{"id" : 7, "text" : "선샤인", "type" : "", "begin" : 16, "end" : 18},
		{"id" : 8, "text" : "크리에이션에서", "type" : "", "begin" : 19, "end" : 20},
		{"id" : 9, "text" : "'ONIGIRIズ'", "type" : "", "begin" : 21, "end" : 23},
		{"id" : 10, "text" : "명의로", "type" : "", "begin" : 24, "end" : 25},
		{"id" : 11, "text" : "직접", "type" : "", "begin" : 26, "end" : 26},
		{"id" : 12, "text" : "참가했다.", "type" : "", "begin" : 27, "end" : 31}
	],
	"NE" : [
		{"id" : 0, "text" : "2009년", "type" : "DT_YEAR", "begin" : 0, "end" : 1, "weight" : 0.700938, "common_noun" : 0},
		{"id" : 1, "text" : "77", "type" : "QT_COUNT", "begin" : 5, "end" : 5, "weight" : 0.223367, "common_noun" : 0},
		{"id" : 2, "text" : "2010년", "type" : "DT_YEAR", "begin" : 13, "end" : 14, "weight" : 0.715447, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "2009년의", "head" : 3, "label" : "NP_MOD", "mod" : [], "weight" : 0.68138 },
		{"id" : 1, "text" : "코믹", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.746789 },
		{"id" : 2, "text" : "마켓", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.682253 },
		{"id" : 3, "text" : "77에서", "head" : 5, "label" : "NP_AJT", "mod" : [0, 2], "weight" : 0.979342 },
		{"id" : 4, "text" : "'StrawberryPink'", "head" : 5, "label" : "NP", "mod" : [], "weight" : 0.512246 },
		{"id" : 5, "text" : "명의로,", "head" : 12, "label" : "NP_AJT", "mod" : [3, 4], "weight" : 0.527056 },
		{"id" : 6, "text" : "2010년의", "head" : 7, "label" : "NP_MOD", "mod" : [], "weight" : 0.620225 },
		{"id" : 7, "text" : "선샤인", "head" : 8, "label" : "VNP_MOD", "mod" : [6], "weight" : 0.996661 },
		{"id" : 8, "text" : "크리에이션에서", "head" : 12, "label" : "NP_AJT", "mod" : [7], "weight" : 0.546553 },
		{"id" : 9, "text" : "'ONIGIRIズ'", "head" : 10, "label" : "NP", "mod" : [], "weight" : 0.675459 },
		{"id" : 10, "text" : "명의로", "head" : 12, "label" : "NP_AJT", "mod" : [9], "weight" : 0.461105 },
		{"id" : 11, "text" : "직접", "head" : 12, "label" : "AP", "mod" : [], "weight" : 0.428112 },
		{"id" : 12, "text" : "참가했다.", "head" : -1, "label" : "VP", "mod" : [5, 8, 10, 11], "weight" : 0.00238584 }
	],
	"SRL" : [
		{"verb" : "참가", "sense" : 1, "word_id" : 12, "weight" : 0.0923164,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 8, "text" : "크리에이션에서", "weight" : 0.0935955 },
				{"type" : "ARG1", "word_id" : 10, "text" : "명의로", "weight" : 0.0788239 },
				{"type" : "ARGM-MNR", "word_id" : 11, "text" : "직접", "weight" : 0.10453 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 12, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.563539 }
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "코믹 트레져 15에는 위탁 참가했다.",
	"morp" : [
		{"id" : 0, "lemma" : "코믹", "type" : "NNG", "position" : 233, "weight" : 0.9 },
		{"id" : 1, "lemma" : "트레져", "type" : "NNP", "position" : 240, "weight" : 0.6 },
		{"id" : 2, "lemma" : "15", "type" : "SN", "position" : 250, "weight" : 1 },
		{"id" : 3, "lemma" : "에", "type" : "JKB", "position" : 252, "weight" : 0.00395698 },
		{"id" : 4, "lemma" : "는", "type" : "JX", "position" : 255, "weight" : 0.0387928 },
		{"id" : 5, "lemma" : "위탁", "type" : "NNG", "position" : 259, "weight" : 0.9 },
		{"id" : 6, "lemma" : "참가", "type" : "NNG", "position" : 266, "weight" : 0.9 },
		{"id" : 7, "lemma" : "하", "type" : "XSV", "position" : 272, "weight" : 0.0001 },
		{"id" : 8, "lemma" : "었", "type" : "EP", "position" : 272, "weight" : 0.9 },
		{"id" : 9, "lemma" : "다", "type" : "EF", "position" : 275, "weight" : 0.640954 },
		{"id" : 10, "lemma" : ".", "type" : "SF", "position" : 278, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "코믹/NNG", "target" : "코믹", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "트레져/NNG", "target" : "트레져", "word_id" : 1, "m_begin" : 1, "m_end" : 1},
		{"id" : 2, "result" : "15/SN+에/JKB+는/JX", "target" : "15에는", "word_id" : 2, "m_begin" : 2, "m_end" : 4},
		{"id" : 3, "result" : "위탁/NNG", "target" : "위탁", "word_id" : 3, "m_begin" : 5, "m_end" : 5},
		{"id" : 4, "result" : "참가하/VV+었/EP+다/EF+./SF", "target" : "참가했다.", "word_id" : 4, "m_begin" : 6, "m_end" : 10}
	],
	"WSD" : [
		{"id" : 0, "text" : "코믹", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 233, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "트레져", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 240, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "15", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 250, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 252, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 255, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "위탁", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 259, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "참가하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 266, "begin" : 6, "end" : 7},
		{"id" : 7, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 272, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 275, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 278, "begin" : 10, "end" : 10}
	],
	"word" : [
		{"id" : 0, "text" : "코믹", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "트레져", "type" : "", "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "15에는", "type" : "", "begin" : 2, "end" : 4},
		{"id" : 3, "text" : "위탁", "type" : "", "begin" : 5, "end" : 5},
		{"id" : 4, "text" : "참가했다.", "type" : "", "begin" : 6, "end" : 10}
	],
	"NE" : [
		{"id" : 0, "text" : "15", "type" : "QT_OTHERS", "begin" : 2, "end" : 2, "weight" : 0.229576, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "코믹", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.586878 },
		{"id" : 1, "text" : "트레져", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.770665 },
		{"id" : 2, "text" : "15에는", "head" : 4, "label" : "NP_AJT", "mod" : [0, 1], "weight" : 0.544047 },
		{"id" : 3, "text" : "위탁", "head" : 4, "label" : "NP", "mod" : [], "weight" : 0.24867 },
		{"id" : 4, "text" : "참가했다.", "head" : -1, "label" : "VP", "mod" : [2, 3], "weight" : 0.0360308 }
	],
	"SRL" : [
		{"verb" : "참가", "sense" : 1, "word_id" : 4, "weight" : 0.11797,
			"argument" : [
				{"type" : "ARG1", "word_id" : 2, "text" : "15에는", "weight" : 0.11797 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 4, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.504867 }
	]
	},
	{
	"id" : 3,
	"reserve_str" : "",
	"text" : "그 외에, 다른 서클에서도 게스트 등으로 일러스트를 그렸다.",
	"morp" : [
		{"id" : 0, "lemma" : "그", "type" : "MM", "position" : 279, "weight" : 0.026321 },
		{"id" : 1, "lemma" : "외", "type" : "NNB", "position" : 283, "weight" : 0.149991 },
		{"id" : 2, "lemma" : "에", "type" : "JKB", "position" : 286, "weight" : 0.135559 },
		{"id" : 3, "lemma" : ",", "type" : "SP", "position" : 289, "weight" : 1 },
		{"id" : 4, "lemma" : "다른", "type" : "MM", "position" : 291, "weight" : 0.9 },
		{"id" : 5, "lemma" : "서클", "type" : "NNG", "position" : 298, "weight" : 0.9 },
		{"id" : 6, "lemma" : "에서", "type" : "JKB", "position" : 304, "weight" : 0.153407 },
		{"id" : 7, "lemma" : "도", "type" : "JX", "position" : 310, "weight" : 0.0912297 },
		{"id" : 8, "lemma" : "게스트", "type" : "NNG", "position" : 314, "weight" : 0.9 },
		{"id" : 9, "lemma" : "등", "type" : "NNB", "position" : 324, "weight" : 0.0146757 },
		{"id" : 10, "lemma" : "으로", "type" : "JKB", "position" : 327, "weight" : 0.135596 },
		{"id" : 11, "lemma" : "일러스트", "type" : "NNG", "position" : 334, "weight" : 0.55 },
		{"id" : 12, "lemma" : "를", "type" : "JKO", "position" : 346, "weight" : 0.137686 },
		{"id" : 13, "lemma" : "그리", "type" : "VV", "position" : 350, "weight" : 0.604417 },
		{"id" : 14, "lemma" : "었", "type" : "EP", "position" : 353, "weight" : 0.9 },
		{"id" : 15, "lemma" : "다", "type" : "EF", "position" : 356, "weight" : 0.640954 },
		{"id" : 16, "lemma" : ".", "type" : "SF", "position" : 359, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "그/MM", "target" : "그", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "외/NNB+에/JKB+,/SP", "target" : "외에,", "word_id" : 1, "m_begin" : 1, "m_end" : 3},
		{"id" : 2, "result" : "다른/MM", "target" : "다른", "word_id" : 2, "m_begin" : 4, "m_end" : 4},
		{"id" : 3, "result" : "서클/NNG+에서/JKB+도/JX", "target" : "서클에서도", "word_id" : 3, "m_begin" : 5, "m_end" : 7},
		{"id" : 4, "result" : "게스트/NNG", "target" : "게스트", "word_id" : 4, "m_begin" : 8, "m_end" : 8},
		{"id" : 5, "result" : "등/NNB+으로/JKB", "target" : "등으로", "word_id" : 5, "m_begin" : 9, "m_end" : 10},
		{"id" : 6, "result" : "일러스트/NNG+를/JKO", "target" : "일러스트를", "word_id" : 6, "m_begin" : 11, "m_end" : 12},
		{"id" : 7, "result" : "그리/VV+었/EP+다/EF+./SF", "target" : "그렸다.", "word_id" : 7, "m_begin" : 13, "m_end" : 16}
	],
	"WSD" : [
		{"id" : 0, "text" : "그", "type" : "MM", "scode" : "01", "weight" : 1, "position" : 279, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "외", "type" : "NNB", "scode" : "04", "weight" : 1, "position" : 283, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 286, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 289, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "다른", "type" : "MM", "scode" : "00", "weight" : 1, "position" : 291, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "서클", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 298, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 304, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "도", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 310, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "게스트", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 314, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "등", "type" : "NNB", "scode" : "05", "weight" : 1, "position" : 324, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 327, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "일러스트", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 334, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 346, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "그리", "type" : "VV", "scode" : "02", "weight" : 1, "position" : 350, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 353, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 356, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 359, "begin" : 16, "end" : 16}
	],
	"word" : [
		{"id" : 0, "text" : "그", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "외에,", "type" : "", "begin" : 1, "end" : 3},
		{"id" : 2, "text" : "다른", "type" : "", "begin" : 4, "end" : 4},
		{"id" : 3, "text" : "서클에서도", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 4, "text" : "게스트", "type" : "", "begin" : 8, "end" : 8},
		{"id" : 5, "text" : "등으로", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 6, "text" : "일러스트를", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 7, "text" : "그렸다.", "type" : "", "begin" : 13, "end" : 16}
	],
	"NE" : [
		{"id" : 0, "text" : "게스트", "type" : "CV_POSITION", "begin" : 8, "end" : 8, "weight" : 0.278165, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "그", "head" : 1, "label" : "DP", "mod" : [], "weight" : 0.836037 },
		{"id" : 1, "text" : "외에,", "head" : 7, "label" : "NP_AJT", "mod" : [0], "weight" : 0.758888 },
		{"id" : 2, "text" : "다른", "head" : 3, "label" : "DP", "mod" : [], "weight" : 0.783153 },
		{"id" : 3, "text" : "서클에서도", "head" : 7, "label" : "NP_AJT", "mod" : [2], "weight" : 0.744533 },
		{"id" : 4, "text" : "게스트", "head" : 5, "label" : "NP", "mod" : [], "weight" : 0.831701 },
		{"id" : 5, "text" : "등으로", "head" : 7, "label" : "NP_AJT", "mod" : [4], "weight" : 0.741973 },
		{"id" : 6, "text" : "일러스트를", "head" : 7, "label" : "NP_OBJ", "mod" : [], "weight" : 0.533182 },
		{"id" : 7, "text" : "그렸다.", "head" : -1, "label" : "VP", "mod" : [1, 3, 5, 6], "weight" : 0.0900432 }
	],
	"SRL" : [
		{"verb" : "그리", "sense" : 1, "word_id" : 7, "weight" : 0.160579,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 3, "text" : "서클에서도", "weight" : 0.139189 },
				{"type" : "ARGM-INS", "word_id" : 5, "text" : "등으로", "weight" : 0.0589077 },
				{"type" : "ARG1", "word_id" : 6, "text" : "일러스트를", "weight" : 0.28364 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 7, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.75514 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 3, "sent_id" : 1, "start_eid" : 0, "end_eid" : 5, "ne_id" : -1, "text" : "2009년의 코믹 마켓 77에서 'StrawberryPink' 명의", "start_eid_short" : 4, "end_eid_short" : 5, "text_short" : "'StrawberryPink' 명의", "weight" : 0.006 },
		{"id" : 10, "sent_id" : 1, "start_eid" : 9, "end_eid" : 10, "ne_id" : -1, "text" : "'ONIGIRIズ' 명의", "start_eid_short" : 9, "end_eid_short" : 10, "text_short" : "'ONIGIRIズ' 명의", "weight" : 0.01 }
	] },
	{"id" : 1, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 7, "sent_id" : 1, "start_eid" : 1, "end_eid" : 1, "ne_id" : -1, "text" : "코믹", "start_eid_short" : 1, "end_eid_short" : 1, "text_short" : "코믹", "weight" : 0.01 },
		{"id" : 12, "sent_id" : 2, "start_eid" : 0, "end_eid" : 0, "ne_id" : -1, "text" : "코믹", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "코믹", "weight" : 0.016 }
	] },
	{"id" : 2, "type" : "QT_OTHERS", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 11, "sent_id" : 2, "start_eid" : 0, "end_eid" : 2, "ne_id" : 0, "text" : "코믹 트레져 15", "start_eid_short" : 0, "end_eid_short" : 2, "text_short" : "코믹 트레져 15", "weight" : 0.002 },
		{"id" : 15, "sent_id" : 3, "start_eid" : 0, "end_eid" : 1, "ne_id" : -1, "text" : "그 외", "start_eid_short" : 0, "end_eid_short" : 1, "text_short" : "그 외", "weight" : 0.006 }
	] }
 ]
}

