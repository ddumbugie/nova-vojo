{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "대통령 직선제 개헌을 결정한 6ㆍ29 민주화 선언이 이루어진 때는 언제일까?",
	"morp" : [
		{"id" : 0, "lemma" : "대통령", "type" : "NNG", "position" : 0, "weight" : 0.9 },
		{"id" : 1, "lemma" : "직선", "type" : "NNG", "position" : 10, "weight" : 0.9 },
		{"id" : 2, "lemma" : "제", "type" : "XSN", "position" : 16, "weight" : 0.00596315 },
		{"id" : 3, "lemma" : "개헌", "type" : "NNG", "position" : 20, "weight" : 0.9 },
		{"id" : 4, "lemma" : "을", "type" : "JKO", "position" : 26, "weight" : 0.129611 },
		{"id" : 5, "lemma" : "결정", "type" : "NNG", "position" : 30, "weight" : 0.9 },
		{"id" : 6, "lemma" : "하", "type" : "XSV", "position" : 36, "weight" : 0.0001 },
		{"id" : 7, "lemma" : "ㄴ", "type" : "ETM", "position" : 36, "weight" : 0.392321 },
		{"id" : 8, "lemma" : "6", "type" : "SN", "position" : 40, "weight" : 1 },
		{"id" : 9, "lemma" : "ㆍ", "type" : "SP", "position" : 41, "weight" : 1 },
		{"id" : 10, "lemma" : "29", "type" : "SN", "position" : 44, "weight" : 1 },
		{"id" : 11, "lemma" : "민주", "type" : "NNG", "position" : 47, "weight" : 0.0687007 },
		{"id" : 12, "lemma" : "화", "type" : "XSN", "position" : 53, "weight" : 0.0240371 },
		{"id" : 13, "lemma" : "선언", "type" : "NNG", "position" : 57, "weight" : 0.131142 },
		{"id" : 14, "lemma" : "이", "type" : "JKS", "position" : 63, "weight" : 0.0360723 },
		{"id" : 15, "lemma" : "이루어지", "type" : "VV", "position" : 67, "weight" : 0.9 },
		{"id" : 16, "lemma" : "ㄴ", "type" : "ETM", "position" : 76, "weight" : 0.304215 },
		{"id" : 17, "lemma" : "때", "type" : "NNG", "position" : 80, "weight" : 0.656522 },
		{"id" : 18, "lemma" : "는", "type" : "JX", "position" : 83, "weight" : 0.0287565 },
		{"id" : 19, "lemma" : "언제", "type" : "NP", "position" : 87, "weight" : 0.0235972 },
		{"id" : 20, "lemma" : "이", "type" : "VCP", "position" : 93, "weight" : 0.0175768 },
		{"id" : 21, "lemma" : "ㄹ까", "type" : "EF", "position" : 93, "weight" : 0.258243 },
		{"id" : 22, "lemma" : "?", "type" : "SF", "position" : 99, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "대통령/NNG", "target" : "대통령", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "직선제/NNG", "target" : "직선제", "word_id" : 1, "m_begin" : 1, "m_end" : 2},
		{"id" : 2, "result" : "개헌/NNG+을/JKO", "target" : "개헌을", "word_id" : 2, "m_begin" : 3, "m_end" : 4},
		{"id" : 3, "result" : "결정하/VV+ㄴ/ETM", "target" : "결정한", "word_id" : 3, "m_begin" : 5, "m_end" : 7},
		{"id" : 4, "result" : "6/SN+ㆍ/SP+29/SN", "target" : "6ㆍ29", "word_id" : 4, "m_begin" : 8, "m_end" : 10},
		{"id" : 5, "result" : "민주화/NNG", "target" : "민주화", "word_id" : 5, "m_begin" : 11, "m_end" : 12},
		{"id" : 6, "result" : "선언/NNG+이/JKS", "target" : "선언이", "word_id" : 6, "m_begin" : 13, "m_end" : 14},
		{"id" : 7, "result" : "이루어지/VV+ㄴ/ETM", "target" : "이루어진", "word_id" : 7, "m_begin" : 15, "m_end" : 16},
		{"id" : 8, "result" : "때/NNG+는/JX", "target" : "때는", "word_id" : 8, "m_begin" : 17, "m_end" : 18},
		{"id" : 9, "result" : "언제/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "언제일까?", "word_id" : 9, "m_begin" : 19, "m_end" : 22}
	],
	"WSD" : [
		{"id" : 0, "text" : "대통령", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "직선제", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "개헌", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 20, "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 26, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "결정하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 30, "begin" : 5, "end" : 6},
		{"id" : 5, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 36, "begin" : 7, "end" : 7},
		{"id" : 6, "text" : "6", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 8, "end" : 8},
		{"id" : 7, "text" : "ㆍ", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 41, "begin" : 9, "end" : 9},
		{"id" : 8, "text" : "29", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 44, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "민주화", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 47, "begin" : 11, "end" : 12},
		{"id" : 10, "text" : "선언", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 57, "begin" : 13, "end" : 13},
		{"id" : 11, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 63, "begin" : 14, "end" : 14},
		{"id" : 12, "text" : "이루어지", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 67, "begin" : 15, "end" : 15},
		{"id" : 13, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 76, "begin" : 16, "end" : 16},
		{"id" : 14, "text" : "때", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 80, "begin" : 17, "end" : 17},
		{"id" : 15, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 83, "begin" : 18, "end" : 18},
		{"id" : 16, "text" : "언제", "type" : "NP", "scode" : "01", "weight" : 1, "position" : 87, "begin" : 19, "end" : 19},
		{"id" : 17, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 93, "begin" : 20, "end" : 20},
		{"id" : 18, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 93, "begin" : 21, "end" : 21},
		{"id" : 19, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 99, "begin" : 22, "end" : 22}
	],
	"word" : [
		{"id" : 0, "text" : "대통령", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "직선제", "type" : "", "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "개헌을", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 3, "text" : "결정한", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 4, "text" : "6ㆍ29", "type" : "", "begin" : 8, "end" : 10},
		{"id" : 5, "text" : "민주화", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 6, "text" : "선언이", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 7, "text" : "이루어진", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 8, "text" : "때는", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 9, "text" : "언제일까?", "type" : "", "begin" : 19, "end" : 22}
	],
	"NE" : [
		{"id" : 0, "text" : "대통령 직선제", "type" : "CV_POLICY", "begin" : 0, "end" : 2, "weight" : 0.604223, "common_noun" : 0},
		{"id" : 1, "text" : "6ㆍ29 민주화 선언", "type" : "EV_ACTIVITY", "begin" : 8, "end" : 13, "weight" : 0.0855671, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "대통령", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.526337 },
		{"id" : 1, "text" : "직선제", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.63887 },
		{"id" : 2, "text" : "개헌을", "head" : 3, "label" : "NP_OBJ", "mod" : [1], "weight" : 0.789117 },
		{"id" : 3, "text" : "결정한", "head" : 6, "label" : "VP_MOD", "mod" : [2], "weight" : 0.748541 },
		{"id" : 4, "text" : "6ㆍ29", "head" : 6, "label" : "NP", "mod" : [], "weight" : 0.668053 },
		{"id" : 5, "text" : "민주화", "head" : 6, "label" : "NP", "mod" : [], "weight" : 0.591836 },
		{"id" : 6, "text" : "선언이", "head" : 7, "label" : "NP_SBJ", "mod" : [3, 4, 5], "weight" : 0.77502 },
		{"id" : 7, "text" : "이루어진", "head" : 8, "label" : "VP_MOD", "mod" : [6], "weight" : 0.76293 },
		{"id" : 8, "text" : "때는", "head" : 9, "label" : "NP_SBJ", "mod" : [7], "weight" : 0.658353 },
		{"id" : 9, "text" : "언제일까?", "head" : -1, "label" : "VNP", "mod" : [8], "weight" : 0.0269423 }
	],
	"SRL" : [
		{"verb" : "결정", "sense" : 1, "word_id" : 3, "weight" : 0.395728,
			"argument" : [
				{"type" : "ARG1", "word_id" : 2, "text" : "개헌을", "weight" : 0.2982 },
				{"type" : "ARG0", "word_id" : 6, "text" : "선언이", "weight" : 0.493257 }
			] },
		{"verb" : "이루어지", "sense" : 2, "word_id" : 7, "weight" : 0.278594,
			"argument" : [
				{"type" : "ARG1", "word_id" : 6, "text" : "선언이", "weight" : 0.278594 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "EV_ACTIVITY", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 8, "ne_id" : -1, "text" : "대통령 직선제 개헌을 결정한 6ㆍ29 민주화 선언이 이루어진 때", "start_eid_short" : 8, "end_eid_short" : 8, "text_short" : "때", "weight" : 0.002 },
		{"id" : 1, "sent_id" : 0, "start_eid" : 0, "end_eid" : 6, "ne_id" : 1, "text" : "대통령 직선제 개헌을 결정한 6ㆍ29 민주화 선언", "start_eid_short" : 4, "end_eid_short" : 6, "text_short" : "6ㆍ29 민주화 선언", "weight" : 0.008 },
		{"id" : 4, "sent_id" : 0, "start_eid" : 8, "end_eid" : 9, "ne_id" : -1, "text" : "언제", "start_eid_short" : 9, "end_eid_short" : 9, "text_short" : "언제이ㄹ까?", "weight" : 0.012 }
	] }
 ]
}
