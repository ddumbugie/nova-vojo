{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿조선시대 지배계층 신분인 '양반'의 '양'자를 한자로 바르게 쓴 것은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "조선", "type" : "NNP", "position" : 3, "weight" : 0.0796816 },
		{"id" : 2, "lemma" : "시대", "type" : "NNG", "position" : 9, "weight" : 0.9 },
		{"id" : 3, "lemma" : "지배", "type" : "NNG", "position" : 16, "weight" : 0.9 },
		{"id" : 4, "lemma" : "계층", "type" : "NNG", "position" : 22, "weight" : 0.9 },
		{"id" : 5, "lemma" : "신분", "type" : "NNG", "position" : 29, "weight" : 0.9 },
		{"id" : 6, "lemma" : "이", "type" : "VCP", "position" : 35, "weight" : 0.0177525 },
		{"id" : 7, "lemma" : "ㄴ", "type" : "ETM", "position" : 35, "weight" : 0.220712 },
		{"id" : 8, "lemma" : "'", "type" : "SS", "position" : 39, "weight" : 1 },
		{"id" : 9, "lemma" : "양반", "type" : "NNG", "position" : 40, "weight" : 0.9 },
		{"id" : 10, "lemma" : "'", "type" : "SS", "position" : 46, "weight" : 1 },
		{"id" : 11, "lemma" : "의", "type" : "JKG", "position" : 47, "weight" : 0.0878251 },
		{"id" : 12, "lemma" : "'", "type" : "SS", "position" : 51, "weight" : 1 },
		{"id" : 13, "lemma" : "양", "type" : "NNG", "position" : 52, "weight" : 0.111208 },
		{"id" : 14, "lemma" : "'", "type" : "SS", "position" : 55, "weight" : 1 },
		{"id" : 15, "lemma" : "자", "type" : "NNG", "position" : 56, "weight" : 0.0240063 },
		{"id" : 16, "lemma" : "를", "type" : "JKO", "position" : 59, "weight" : 0.137686 },
		{"id" : 17, "lemma" : "한자", "type" : "NNG", "position" : 63, "weight" : 0.100921 },
		{"id" : 18, "lemma" : "로", "type" : "JKB", "position" : 69, "weight" : 0.153229 },
		{"id" : 19, "lemma" : "바르", "type" : "VA", "position" : 73, "weight" : 0.0321609 },
		{"id" : 20, "lemma" : "게", "type" : "EC", "position" : 79, "weight" : 0.31879 },
		{"id" : 21, "lemma" : "쓰", "type" : "VV", "position" : 83, "weight" : 0.224832 },
		{"id" : 22, "lemma" : "ㄴ", "type" : "ETM", "position" : 83, "weight" : 0.304215 },
		{"id" : 23, "lemma" : "것", "type" : "NNB", "position" : 87, "weight" : 0.228788 },
		{"id" : 24, "lemma" : "은", "type" : "JX", "position" : 90, "weight" : 0.0688243 },
		{"id" : 25, "lemma" : "무엇", "type" : "NP", "position" : 94, "weight" : 0.9 },
		{"id" : 26, "lemma" : "이", "type" : "VCP", "position" : 100, "weight" : 0.0175768 },
		{"id" : 27, "lemma" : "ㄹ까", "type" : "EF", "position" : 100, "weight" : 0.258243 },
		{"id" : 28, "lemma" : "?", "type" : "SF", "position" : 106, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿조선시대/NNG", "target" : "﻿조선시대", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "지배계층/NNG", "target" : "지배계층", "word_id" : 1, "m_begin" : 3, "m_end" : 4},
		{"id" : 2, "result" : "신분/NNG+이/VCP+ㄴ/ETM", "target" : "신분인", "word_id" : 2, "m_begin" : 5, "m_end" : 7},
		{"id" : 3, "result" : "'/SS+양반/NNG+'/SS+의/JKG", "target" : "'양반'의", "word_id" : 3, "m_begin" : 8, "m_end" : 11},
		{"id" : 4, "result" : "'/SS+양/NNG+'/SS+자/NNG+를/JKO", "target" : "'양'자를", "word_id" : 4, "m_begin" : 12, "m_end" : 16},
		{"id" : 5, "result" : "한자/NNG+로/JKB", "target" : "한자로", "word_id" : 5, "m_begin" : 17, "m_end" : 18},
		{"id" : 6, "result" : "바르/VA+게/EC", "target" : "바르게", "word_id" : 6, "m_begin" : 19, "m_end" : 20},
		{"id" : 7, "result" : "쓰/VV+ㄴ/ETM", "target" : "쓴", "word_id" : 7, "m_begin" : 21, "m_end" : 22},
		{"id" : 8, "result" : "것/NNB+은/JX", "target" : "것은", "word_id" : 8, "m_begin" : 23, "m_end" : 24},
		{"id" : 9, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 9, "m_begin" : 25, "m_end" : 28}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "조선", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "시대", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 9, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "지배", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 16, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "계층", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 22, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "신분", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 29, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 35, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 35, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 39, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "양반", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 40, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 46, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 47, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 51, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "양", "type" : "NNG", "scode" : "20", "weight" : 1, "position" : 52, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 55, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "자", "type" : "NNG", "scode" : "14", "weight" : 1, "position" : 56, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 59, "begin" : 16, "end" : 16},
		{"id" : 17, "text" : "한자", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 63, "begin" : 17, "end" : 17},
		{"id" : 18, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 69, "begin" : 18, "end" : 18},
		{"id" : 19, "text" : "바르", "type" : "VA", "scode" : "03", "weight" : 1, "position" : 73, "begin" : 19, "end" : 19},
		{"id" : 20, "text" : "게", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 79, "begin" : 20, "end" : 20},
		{"id" : 21, "text" : "쓰", "type" : "VV", "scode" : "03", "weight" : 1, "position" : 83, "begin" : 21, "end" : 21},
		{"id" : 22, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 83, "begin" : 22, "end" : 22},
		{"id" : 23, "text" : "것", "type" : "NNB", "scode" : "01", "weight" : 1, "position" : 87, "begin" : 23, "end" : 23},
		{"id" : 24, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 90, "begin" : 24, "end" : 24},
		{"id" : 25, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 94, "begin" : 25, "end" : 25},
		{"id" : 26, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 100, "begin" : 26, "end" : 26},
		{"id" : 27, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 100, "begin" : 27, "end" : 27},
		{"id" : 28, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 106, "begin" : 28, "end" : 28}
	],
	"word" : [
		{"id" : 0, "text" : "﻿조선시대", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "지배계층", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 2, "text" : "신분인", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 3, "text" : "'양반'의", "type" : "", "begin" : 8, "end" : 11},
		{"id" : 4, "text" : "'양'자를", "type" : "", "begin" : 12, "end" : 16},
		{"id" : 5, "text" : "한자로", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 6, "text" : "바르게", "type" : "", "begin" : 19, "end" : 20},
		{"id" : 7, "text" : "쓴", "type" : "", "begin" : 21, "end" : 22},
		{"id" : 8, "text" : "것은", "type" : "", "begin" : 23, "end" : 24},
		{"id" : 9, "text" : "무엇일까?", "type" : "", "begin" : 25, "end" : 28}
	],
	"NE" : [
		{"id" : 0, "text" : "조선시대", "type" : "DT_DYNASTY", "begin" : 1, "end" : 2, "weight" : 0.661355, "common_noun" : 0},
		{"id" : 1, "text" : "양반", "type" : "CV_POSITION", "begin" : 9, "end" : 9, "weight" : 0.343491, "common_noun" : 0},
		{"id" : 2, "text" : "한자", "type" : "CV_LANGUAGE", "begin" : 17, "end" : 17, "weight" : 0.316453, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿조선시대", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.550696 },
		{"id" : 1, "text" : "지배계층", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.757888 },
		{"id" : 2, "text" : "신분인", "head" : 3, "label" : "VNP_MOD", "mod" : [1], "weight" : 0.762645 },
		{"id" : 3, "text" : "'양반'의", "head" : 4, "label" : "NP_MOD", "mod" : [2], "weight" : 0.811168 },
		{"id" : 4, "text" : "'양'자를", "head" : 7, "label" : "NP_OBJ", "mod" : [3], "weight" : 0.935777 },
		{"id" : 5, "text" : "한자로", "head" : 7, "label" : "NP_AJT", "mod" : [], "weight" : 0.802541 },
		{"id" : 6, "text" : "바르게", "head" : 7, "label" : "VP_AJT", "mod" : [], "weight" : 0.688258 },
		{"id" : 7, "text" : "쓴", "head" : 8, "label" : "VP_MOD", "mod" : [4, 5, 6], "weight" : 0.75718 },
		{"id" : 8, "text" : "것은", "head" : 9, "label" : "NP_SBJ", "mod" : [7], "weight" : 0.63629 },
		{"id" : 9, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [8], "weight" : 0.0433755 }
	],
	"SRL" : [
		{"verb" : "바르", "sense" : 1, "word_id" : 6, "weight" : 0,
			"argument" : [
			] },
		{"verb" : "쓰", "sense" : 1, "word_id" : 7, "weight" : 0.182487,
			"argument" : [
				{"type" : "ARG1", "word_id" : 4, "text" : "'양'자를", "weight" : 0.204843 },
				{"type" : "ARGM-INS", "word_id" : 5, "text" : "한자로", "weight" : 0.167493 },
				{"type" : "ARGM-MNR", "word_id" : 6, "text" : "바르게", "weight" : 0.175126 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
 ]
}

