{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "강하니(본명 강청광, 1986년7월 25일 ~ )는 대한민국의 가수이다.",
	"morp" : [
		{"id" : 0, "lemma" : "강", "type" : "NNG", "position" : 0, "weight" : 0.370675 },
		{"id" : 1, "lemma" : "하", "type" : "XSA", "position" : 3, "weight" : 0.0001 },
		{"id" : 2, "lemma" : "니", "type" : "EF", "position" : 6, "weight" : 0.0100615 },
		{"id" : 3, "lemma" : "(", "type" : "SS", "position" : 9, "weight" : 1 },
		{"id" : 4, "lemma" : "본명", "type" : "NNG", "position" : 10, "weight" : 0.9 },
		{"id" : 5, "lemma" : "강청광", "type" : "NNP", "position" : 17, "weight" : 0.6 },
		{"id" : 6, "lemma" : ",", "type" : "SP", "position" : 26, "weight" : 1 },
		{"id" : 7, "lemma" : "1986", "type" : "SN", "position" : 28, "weight" : 1 },
		{"id" : 8, "lemma" : "년", "type" : "NNB", "position" : 32, "weight" : 0.414343 },
		{"id" : 9, "lemma" : "7", "type" : "SN", "position" : 35, "weight" : 1 },
		{"id" : 10, "lemma" : "월", "type" : "NNB", "position" : 36, "weight" : 0.408539 },
		{"id" : 11, "lemma" : "25", "type" : "SN", "position" : 40, "weight" : 1 },
		{"id" : 12, "lemma" : "일", "type" : "NNB", "position" : 42, "weight" : 0.126777 },
		{"id" : 13, "lemma" : "~", "type" : "SO", "position" : 46, "weight" : 1 },
		{"id" : 14, "lemma" : ")", "type" : "SS", "position" : 48, "weight" : 1 },
		{"id" : 15, "lemma" : "는", "type" : "JX", "position" : 49, "weight" : 0.00823314 },
		{"id" : 16, "lemma" : "대한민국", "type" : "NNP", "position" : 53, "weight" : 0.0447775 },
		{"id" : 17, "lemma" : "의", "type" : "JKG", "position" : 65, "weight" : 0.0987295 },
		{"id" : 18, "lemma" : "가수", "type" : "NNG", "position" : 69, "weight" : 0.9 },
		{"id" : 19, "lemma" : "이", "type" : "VCP", "position" : 75, "weight" : 0.0177525 },
		{"id" : 20, "lemma" : "다", "type" : "EF", "position" : 78, "weight" : 0.353579 },
		{"id" : 21, "lemma" : ".", "type" : "SF", "position" : 81, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "강하/VA+니/EF+(/SS+본명/NNG", "target" : "강하니(본명", "word_id" : 0, "m_begin" : 0, "m_end" : 4},
		{"id" : 1, "result" : "강청광/NNG+,/SP", "target" : "강청광,", "word_id" : 1, "m_begin" : 5, "m_end" : 6},
		{"id" : 2, "result" : "1986/SN+년/NNB+7/SN+월/NNB", "target" : "1986년7월", "word_id" : 2, "m_begin" : 7, "m_end" : 10},
		{"id" : 3, "result" : "25/SN+일/NNB", "target" : "25일", "word_id" : 3, "m_begin" : 11, "m_end" : 12},
		{"id" : 4, "result" : "~/SO", "target" : "~", "word_id" : 4, "m_begin" : 13, "m_end" : 13},
		{"id" : 5, "result" : ")/SS+는/JX", "target" : ")는", "word_id" : 5, "m_begin" : 14, "m_end" : 15},
		{"id" : 6, "result" : "대한민국/NNG+의/JKG", "target" : "대한민국의", "word_id" : 6, "m_begin" : 16, "m_end" : 17},
		{"id" : 7, "result" : "가수/NNG+이/VCP+다/EF+./SF", "target" : "가수이다.", "word_id" : 7, "m_begin" : 18, "m_end" : 21}
	],
	"WSD" : [
		{"id" : 0, "text" : "강하", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 0, "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "니", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 6, "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "본명", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 10, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "강청광", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 17, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 26, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "1986", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 28, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 32, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "7", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 35, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "월", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 36, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "25", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "일", "type" : "NNB", "scode" : "07", "weight" : 1, "position" : 42, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "~", "type" : "SO", "scode" : "00", "weight" : 1, "position" : 46, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 48, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 49, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "대한민국", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 53, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 65, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "가수", "type" : "NNG", "scode" : "11", "weight" : 1, "position" : 69, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 75, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 78, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 81, "begin" : 21, "end" : 21}
	],
	"word" : [
		{"id" : 0, "text" : "강하니(본명", "type" : "", "begin" : 0, "end" : 4},
		{"id" : 1, "text" : "강청광,", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 2, "text" : "1986년7월", "type" : "", "begin" : 7, "end" : 10},
		{"id" : 3, "text" : "25일", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 4, "text" : "~", "type" : "", "begin" : 13, "end" : 13},
		{"id" : 5, "text" : ")는", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 6, "text" : "대한민국의", "type" : "", "begin" : 16, "end" : 17},
		{"id" : 7, "text" : "가수이다.", "type" : "", "begin" : 18, "end" : 21}
	],
	"NE" : [
		{"id" : 0, "text" : "강하니", "type" : "PS_NAME", "begin" : 0, "end" : 2, "weight" : 0.56163, "common_noun" : 0},
		{"id" : 1, "text" : "강청광", "type" : "PS_NAME", "begin" : 5, "end" : 5, "weight" : 0.363312, "common_noun" : 0},
		{"id" : 2, "text" : "1986년7월 25일 ~", "type" : "DT_OTHERS", "begin" : 7, "end" : 13, "weight" : 0.73192, "common_noun" : 0},
		{"id" : 3, "text" : "대한민국", "type" : "LCP_COUNTRY", "begin" : 16, "end" : 16, "weight" : 0.357756, "common_noun" : 0},
		{"id" : 4, "text" : "가수", "type" : "CV_OCCUPATION", "begin" : 18, "end" : 18, "weight" : 0.368643, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "강하니(본명", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.686163 },
		{"id" : 1, "text" : "강청광,", "head" : 5, "label" : "NP_CNJ", "mod" : [0], "weight" : 0.586508 },
		{"id" : 2, "text" : "1986년7월", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.561924 },
		{"id" : 3, "text" : "25일", "head" : 4, "label" : "NP", "mod" : [2], "weight" : 0.420727 },
		{"id" : 4, "text" : "~", "head" : 5, "label" : "X", "mod" : [3], "weight" : 0.78384 },
		{"id" : 5, "text" : ")는", "head" : 7, "label" : "NP_SBJ", "mod" : [1, 4], "weight" : 0.451719 },
		{"id" : 6, "text" : "대한민국의", "head" : 7, "label" : "NP_MOD", "mod" : [], "weight" : 0.435926 },
		{"id" : 7, "text" : "가수이다.", "head" : -1, "label" : "VNP", "mod" : [5, 6], "weight" : 0.00844977 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "2009년 디지털 싱글 《사랑따위가》로 데뷔했다.",
	"morp" : [
		{"id" : 0, "lemma" : "2009", "type" : "SN", "position" : 82, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 86, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "디지털", "type" : "NNG", "position" : 90, "weight" : 0.9 },
		{"id" : 3, "lemma" : "싱글", "type" : "NNG", "position" : 100, "weight" : 0.159957 },
		{"id" : 4, "lemma" : "《", "type" : "SS", "position" : 107, "weight" : 1 },
		{"id" : 5, "lemma" : "사랑", "type" : "NNG", "position" : 110, "weight" : 0.199786 },
		{"id" : 6, "lemma" : "따위", "type" : "NNB", "position" : 116, "weight" : 0.9 },
		{"id" : 7, "lemma" : "가", "type" : "JKS", "position" : 122, "weight" : 0.0691641 },
		{"id" : 8, "lemma" : "》", "type" : "SS", "position" : 125, "weight" : 1 },
		{"id" : 9, "lemma" : "로", "type" : "JKB", "position" : 128, "weight" : 0.0485504 },
		{"id" : 10, "lemma" : "데뷔", "type" : "NNG", "position" : 132, "weight" : 0.9 },
		{"id" : 11, "lemma" : "하", "type" : "XSV", "position" : 138, "weight" : 0.0001 },
		{"id" : 12, "lemma" : "었", "type" : "EP", "position" : 138, "weight" : 0.9 },
		{"id" : 13, "lemma" : "다", "type" : "EF", "position" : 141, "weight" : 0.640954 },
		{"id" : 14, "lemma" : ".", "type" : "SF", "position" : 144, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "2009/SN+년/NNB", "target" : "2009년", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "디지털/NNG", "target" : "디지털", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "싱글/NNG", "target" : "싱글", "word_id" : 2, "m_begin" : 3, "m_end" : 3},
		{"id" : 3, "result" : "《/SS+사랑/NNG+따위/NNB+가/JKS+》/SS+로/JKB", "target" : "《사랑따위가》로", "word_id" : 3, "m_begin" : 4, "m_end" : 9},
		{"id" : 4, "result" : "데뷔하/VV+었/EP+다/EF+./SF", "target" : "데뷔했다.", "word_id" : 4, "m_begin" : 10, "m_end" : 14}
	],
	"WSD" : [
		{"id" : 0, "text" : "2009", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 82, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 86, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "디지털", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 90, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "싱글", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 100, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "《", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 107, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "사랑", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 110, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "따위", "type" : "NNB", "scode" : "00", "weight" : 1, "position" : 116, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "가", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 122, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "》", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 125, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 128, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "데뷔하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 132, "begin" : 10, "end" : 11},
		{"id" : 11, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 138, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 141, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 144, "begin" : 14, "end" : 14}
	],
	"word" : [
		{"id" : 0, "text" : "2009년", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "디지털", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "싱글", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "《사랑따위가》로", "type" : "", "begin" : 4, "end" : 9},
		{"id" : 4, "text" : "데뷔했다.", "type" : "", "begin" : 10, "end" : 14}
	],
	"NE" : [
		{"id" : 0, "text" : "2009년", "type" : "DT_YEAR", "begin" : 0, "end" : 1, "weight" : 0.725304, "common_noun" : 0},
		{"id" : 1, "text" : "사랑따위가", "type" : "AFW_MUSIC", "begin" : 5, "end" : 7, "weight" : 0.973717, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "2009년", "head" : 4, "label" : "NP_AJT", "mod" : [], "weight" : 0.487775 },
		{"id" : 1, "text" : "디지털", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.620274 },
		{"id" : 2, "text" : "싱글", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.700007 },
		{"id" : 3, "text" : "《사랑따위가》로", "head" : 4, "label" : "NP_AJT", "mod" : [2], "weight" : 0.482309 },
		{"id" : 4, "text" : "데뷔했다.", "head" : -1, "label" : "VP", "mod" : [0, 3], "weight" : 0.0572013 }
	],
	"SRL" : [
		{"verb" : "데뷔", "sense" : 1, "word_id" : 4, "weight" : 0.0618461,
			"argument" : [
				{"type" : "ARG1", "word_id" : 3, "text" : "《사랑따위가》로", "weight" : 0.0618461 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 4, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.738907 }
	]
	}
 ],
 "entity" : [
 ]
}

