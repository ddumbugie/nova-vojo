{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿이것은 조선왕조시기에 중추부의 정삼품 당상관을 이르던 말이었다가 나중에 와서는 나이 많은 남자를 낮잡아 이르는 말로 쓰였다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "이것", "type" : "NP", "position" : 3, "weight" : 0.0020857 },
		{"id" : 2, "lemma" : "은", "type" : "JX", "position" : 9, "weight" : 0.191811 },
		{"id" : 3, "lemma" : "조선", "type" : "NNP", "position" : 13, "weight" : 0.0441558 },
		{"id" : 4, "lemma" : "왕조", "type" : "NNG", "position" : 19, "weight" : 0.9 },
		{"id" : 5, "lemma" : "시기", "type" : "NNG", "position" : 25, "weight" : 0.184726 },
		{"id" : 6, "lemma" : "에", "type" : "JKB", "position" : 31, "weight" : 0.153364 },
		{"id" : 7, "lemma" : "중추", "type" : "NNG", "position" : 35, "weight" : 0.9 },
		{"id" : 8, "lemma" : "부", "type" : "XSN", "position" : 41, "weight" : 0.0112289 },
		{"id" : 9, "lemma" : "의", "type" : "JKG", "position" : 44, "weight" : 0.120732 },
		{"id" : 10, "lemma" : "정", "type" : "NNG", "position" : 48, "weight" : 0.379192 },
		{"id" : 11, "lemma" : "삼품", "type" : "NNG", "position" : 51, "weight" : 0.9 },
		{"id" : 12, "lemma" : "당상", "type" : "NNG", "position" : 58, "weight" : 0.9 },
		{"id" : 13, "lemma" : "관", "type" : "XSN", "position" : 64, "weight" : 0.0123777 },
		{"id" : 14, "lemma" : "을", "type" : "JKO", "position" : 67, "weight" : 0.0819193 },
		{"id" : 15, "lemma" : "이르", "type" : "VV", "position" : 71, "weight" : 0.760305 },
		{"id" : 16, "lemma" : "던", "type" : "ETM", "position" : 77, "weight" : 0.317449 },
		{"id" : 17, "lemma" : "말", "type" : "NNG", "position" : 81, "weight" : 0.528926 },
		{"id" : 18, "lemma" : "이", "type" : "VCP", "position" : 84, "weight" : 0.0177525 },
		{"id" : 19, "lemma" : "었", "type" : "EP", "position" : 87, "weight" : 0.9 },
		{"id" : 20, "lemma" : "다가", "type" : "EC", "position" : 90, "weight" : 0.188685 },
		{"id" : 21, "lemma" : "나중", "type" : "NNG", "position" : 97, "weight" : 0.9 },
		{"id" : 22, "lemma" : "에", "type" : "JKB", "position" : 103, "weight" : 0.153364 },
		{"id" : 23, "lemma" : "오", "type" : "VV", "position" : 107, "weight" : 0.227595 },
		{"id" : 24, "lemma" : "아서", "type" : "EC", "position" : 107, "weight" : 0.428178 },
		{"id" : 25, "lemma" : "는", "type" : "JX", "position" : 113, "weight" : 0.0115393 },
		{"id" : 26, "lemma" : "나이", "type" : "NNG", "position" : 117, "weight" : 0.9 },
		{"id" : 27, "lemma" : "많", "type" : "VA", "position" : 124, "weight" : 0.00587367 },
		{"id" : 28, "lemma" : "은", "type" : "ETM", "position" : 127, "weight" : 0.155123 },
		{"id" : 29, "lemma" : "남자", "type" : "NNG", "position" : 131, "weight" : 0.9 },
		{"id" : 30, "lemma" : "를", "type" : "JKO", "position" : 137, "weight" : 0.137686 },
		{"id" : 31, "lemma" : "낮잡", "type" : "VV", "position" : 141, "weight" : 0.9 },
		{"id" : 32, "lemma" : "아", "type" : "EC", "position" : 147, "weight" : 0.398809 },
		{"id" : 33, "lemma" : "이르", "type" : "VV", "position" : 151, "weight" : 0.221266 },
		{"id" : 34, "lemma" : "는", "type" : "ETM", "position" : 157, "weight" : 0.184941 },
		{"id" : 35, "lemma" : "말", "type" : "NNG", "position" : 161, "weight" : 0.528926 },
		{"id" : 36, "lemma" : "로", "type" : "JKB", "position" : 164, "weight" : 0.153229 },
		{"id" : 37, "lemma" : "쓰이", "type" : "VV", "position" : 168, "weight" : 0.9 },
		{"id" : 38, "lemma" : "었", "type" : "EP", "position" : 171, "weight" : 0.9 },
		{"id" : 39, "lemma" : "다", "type" : "EF", "position" : 174, "weight" : 0.640954 },
		{"id" : 40, "lemma" : ".", "type" : "SF", "position" : 177, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿/NNG+이것/NP+은/JX", "target" : "﻿이것은", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "조선왕조시기/NNG+에/JKB", "target" : "조선왕조시기에", "word_id" : 1, "m_begin" : 3, "m_end" : 6},
		{"id" : 2, "result" : "중추부/NNG+의/JKG", "target" : "중추부의", "word_id" : 2, "m_begin" : 7, "m_end" : 9},
		{"id" : 3, "result" : "정삼품/NNG", "target" : "정삼품", "word_id" : 3, "m_begin" : 10, "m_end" : 11},
		{"id" : 4, "result" : "당상관/NNG+을/JKO", "target" : "당상관을", "word_id" : 4, "m_begin" : 12, "m_end" : 14},
		{"id" : 5, "result" : "이르/VV+던/ETM", "target" : "이르던", "word_id" : 5, "m_begin" : 15, "m_end" : 16},
		{"id" : 6, "result" : "말/NNG+이/VCP+었/EP+다가/EC", "target" : "말이었다가", "word_id" : 6, "m_begin" : 17, "m_end" : 20},
		{"id" : 7, "result" : "나중/NNG+에/JKB", "target" : "나중에", "word_id" : 7, "m_begin" : 21, "m_end" : 22},
		{"id" : 8, "result" : "오/VV+어서/EC+는/JX", "target" : "와서는", "word_id" : 8, "m_begin" : 23, "m_end" : 25},
		{"id" : 9, "result" : "나이/NNG", "target" : "나이", "word_id" : 9, "m_begin" : 26, "m_end" : 26},
		{"id" : 10, "result" : "많/VA+은/ETM", "target" : "많은", "word_id" : 10, "m_begin" : 27, "m_end" : 28},
		{"id" : 11, "result" : "남자/NNG+를/JKO", "target" : "남자를", "word_id" : 11, "m_begin" : 29, "m_end" : 30},
		{"id" : 12, "result" : "낮잡/VV+어/EC", "target" : "낮잡아", "word_id" : 12, "m_begin" : 31, "m_end" : 32},
		{"id" : 13, "result" : "이르/VV+는/ETM", "target" : "이르는", "word_id" : 13, "m_begin" : 33, "m_end" : 34},
		{"id" : 14, "result" : "말/NNG+로/JKB", "target" : "말로", "word_id" : 14, "m_begin" : 35, "m_end" : 36},
		{"id" : 15, "result" : "쓰이/VV+었/EP+다/EF+./SF", "target" : "쓰였다.", "word_id" : 15, "m_begin" : 37, "m_end" : 40}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "조선왕조", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 13, "begin" : 3, "end" : 4},
		{"id" : 4, "text" : "시기", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 25, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 31, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "중추부", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 35, "begin" : 7, "end" : 8},
		{"id" : 7, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 44, "begin" : 9, "end" : 9},
		{"id" : 8, "text" : "정삼품", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 48, "begin" : 10, "end" : 11},
		{"id" : 9, "text" : "당상관", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 58, "begin" : 12, "end" : 13},
		{"id" : 10, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 67, "begin" : 14, "end" : 14},
		{"id" : 11, "text" : "이르", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 71, "begin" : 15, "end" : 15},
		{"id" : 12, "text" : "던", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 77, "begin" : 16, "end" : 16},
		{"id" : 13, "text" : "말", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 81, "begin" : 17, "end" : 17},
		{"id" : 14, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 84, "begin" : 18, "end" : 18},
		{"id" : 15, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 87, "begin" : 19, "end" : 19},
		{"id" : 16, "text" : "다가", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 90, "begin" : 20, "end" : 20},
		{"id" : 17, "text" : "나중", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 97, "begin" : 21, "end" : 21},
		{"id" : 18, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 103, "begin" : 22, "end" : 22},
		{"id" : 19, "text" : "오", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 107, "begin" : 23, "end" : 23},
		{"id" : 20, "text" : "아서", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 107, "begin" : 24, "end" : 24},
		{"id" : 21, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 113, "begin" : 25, "end" : 25},
		{"id" : 22, "text" : "나이", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 117, "begin" : 26, "end" : 26},
		{"id" : 23, "text" : "많", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 124, "begin" : 27, "end" : 27},
		{"id" : 24, "text" : "은", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 127, "begin" : 28, "end" : 28},
		{"id" : 25, "text" : "남자", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 131, "begin" : 29, "end" : 29},
		{"id" : 26, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 137, "begin" : 30, "end" : 30},
		{"id" : 27, "text" : "낮잡", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 141, "begin" : 31, "end" : 31},
		{"id" : 28, "text" : "아", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 147, "begin" : 32, "end" : 32},
		{"id" : 29, "text" : "이르", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 151, "begin" : 33, "end" : 33},
		{"id" : 30, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 157, "begin" : 34, "end" : 34},
		{"id" : 31, "text" : "말", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 161, "begin" : 35, "end" : 35},
		{"id" : 32, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 164, "begin" : 36, "end" : 36},
		{"id" : 33, "text" : "쓰이", "type" : "VV", "scode" : "03", "weight" : 1, "position" : 168, "begin" : 37, "end" : 37},
		{"id" : 34, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 171, "begin" : 38, "end" : 38},
		{"id" : 35, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 174, "begin" : 39, "end" : 39},
		{"id" : 36, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 177, "begin" : 40, "end" : 40}
	],
	"word" : [
		{"id" : 0, "text" : "﻿이것은", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "조선왕조시기에", "type" : "", "begin" : 3, "end" : 6},
		{"id" : 2, "text" : "중추부의", "type" : "", "begin" : 7, "end" : 9},
		{"id" : 3, "text" : "정삼품", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 4, "text" : "당상관을", "type" : "", "begin" : 12, "end" : 14},
		{"id" : 5, "text" : "이르던", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 6, "text" : "말이었다가", "type" : "", "begin" : 17, "end" : 20},
		{"id" : 7, "text" : "나중에", "type" : "", "begin" : 21, "end" : 22},
		{"id" : 8, "text" : "와서는", "type" : "", "begin" : 23, "end" : 25},
		{"id" : 9, "text" : "나이", "type" : "", "begin" : 26, "end" : 26},
		{"id" : 10, "text" : "많은", "type" : "", "begin" : 27, "end" : 28},
		{"id" : 11, "text" : "남자를", "type" : "", "begin" : 29, "end" : 30},
		{"id" : 12, "text" : "낮잡아", "type" : "", "begin" : 31, "end" : 32},
		{"id" : 13, "text" : "이르는", "type" : "", "begin" : 33, "end" : 34},
		{"id" : 14, "text" : "말로", "type" : "", "begin" : 35, "end" : 36},
		{"id" : 15, "text" : "쓰였다.", "type" : "", "begin" : 37, "end" : 40}
	],
	"NE" : [
		{"id" : 0, "text" : "조선왕조", "type" : "DT_DYNASTY", "begin" : 3, "end" : 4, "weight" : 0.76985, "common_noun" : 0},
		{"id" : 1, "text" : "중추부", "type" : "OGG_POLITICS", "begin" : 7, "end" : 8, "weight" : 0.198118, "common_noun" : 0},
		{"id" : 2, "text" : "정삼품", "type" : "CV_POSITION", "begin" : 10, "end" : 11, "weight" : 0.296944, "common_noun" : 0},
		{"id" : 3, "text" : "당상관", "type" : "CV_POSITION", "begin" : 12, "end" : 13, "weight" : 0.540924, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿이것은", "head" : 5, "label" : "NP_SBJ", "mod" : [], "weight" : 0.609629 },
		{"id" : 1, "text" : "조선왕조시기에", "head" : 5, "label" : "NP_AJT", "mod" : [], "weight" : 0.761058 },
		{"id" : 2, "text" : "중추부의", "head" : 3, "label" : "NP_MOD", "mod" : [], "weight" : 0.642572 },
		{"id" : 3, "text" : "정삼품", "head" : 4, "label" : "NP", "mod" : [2], "weight" : 0.951024 },
		{"id" : 4, "text" : "당상관을", "head" : 5, "label" : "NP_OBJ", "mod" : [3], "weight" : 0.662873 },
		{"id" : 5, "text" : "이르던", "head" : 6, "label" : "VP_MOD", "mod" : [0, 1, 4], "weight" : 0.644243 },
		{"id" : 6, "text" : "말이었다가", "head" : 8, "label" : "VNP", "mod" : [5], "weight" : 0.968423 },
		{"id" : 7, "text" : "나중에", "head" : 8, "label" : "NP_AJT", "mod" : [], "weight" : 0.799331 },
		{"id" : 8, "text" : "와서는", "head" : 15, "label" : "VP", "mod" : [6, 7], "weight" : 0.733531 },
		{"id" : 9, "text" : "나이", "head" : 10, "label" : "NP_SBJ", "mod" : [], "weight" : 0.809228 },
		{"id" : 10, "text" : "많은", "head" : 11, "label" : "VP_MOD", "mod" : [9], "weight" : 0.883627 },
		{"id" : 11, "text" : "남자를", "head" : 12, "label" : "NP_OBJ", "mod" : [10], "weight" : 0.689872 },
		{"id" : 12, "text" : "낮잡아", "head" : 13, "label" : "VP", "mod" : [11], "weight" : 0.729262 },
		{"id" : 13, "text" : "이르는", "head" : 14, "label" : "VP_MOD", "mod" : [12], "weight" : 0.975595 },
		{"id" : 14, "text" : "말로", "head" : 15, "label" : "NP_AJT", "mod" : [13], "weight" : 0.571087 },
		{"id" : 15, "text" : "쓰였다.", "head" : -1, "label" : "VP", "mod" : [8, 14], "weight" : 0.010253 }
	],
	"SRL" : [
		{"verb" : "이르", "sense" : 1, "word_id" : 5, "weight" : 0.221477,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 1, "text" : "조선왕조시기에", "weight" : 0.179096 },
				{"type" : "ARG1", "word_id" : 4, "text" : "당상관을", "weight" : 0.351841 },
				{"type" : "ARG2", "word_id" : 6, "text" : "말이었다가", "weight" : 0.133493 }
			] },
		{"verb" : "오", "sense" : 1, "word_id" : 8, "weight" : 0.171043,
			"argument" : [
				{"type" : "ARG3", "word_id" : 7, "text" : "나중에", "weight" : 0.171043 }
			] },
		{"verb" : "많", "sense" : 1, "word_id" : 10, "weight" : 0.203118,
			"argument" : [
				{"type" : "ARG1", "word_id" : 9, "text" : "나이", "weight" : 0.179587 },
				{"type" : "ARG1", "word_id" : 11, "text" : "남자를", "weight" : 0.226648 }
			] },
		{"verb" : "낮잡", "sense" : 1, "word_id" : 12, "weight" : 0.185056,
			"argument" : [
				{"type" : "ARG1", "word_id" : 11, "text" : "남자를", "weight" : 0.185056 }
			] },
		{"verb" : "이르", "sense" : 1, "word_id" : 13, "weight" : 0.155968,
			"argument" : [
				{"type" : "ARGM-MNR", "word_id" : 12, "text" : "낮잡아", "weight" : 0.132038 },
				{"type" : "ARG2", "word_id" : 14, "text" : "말로", "weight" : 0.179897 }
			] },
		{"verb" : "쓰이", "sense" : 1, "word_id" : 15, "weight" : 0.165145,
			"argument" : [
				{"type" : "ARG2", "word_id" : 14, "text" : "말로", "weight" : 0.165145 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 8, "ant_sid" : 0, "ant_wid" : 0, "type" : "s", "istitle" : 0, "weight" : 0.489202 },
		{"id" : 1, "verb_wid" : 12, "ant_sid" : 0, "ant_wid" : 0, "type" : "s", "istitle" : 0, "weight" : 0.450331 },
		{"id" : 2, "verb_wid" : 15, "ant_sid" : 0, "ant_wid" : 0, "type" : "s", "istitle" : 0, "weight" : 0.287807 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "소설 운수좋은 날의 남자 주인공에게 쓰였던 이 호칭은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "소설", "type" : "NNG", "position" : 179, "weight" : 0.9 },
		{"id" : 1, "lemma" : "운수", "type" : "NNG", "position" : 186, "weight" : 0.9 },
		{"id" : 2, "lemma" : "좋", "type" : "VA", "position" : 192, "weight" : 0.00587172 },
		{"id" : 3, "lemma" : "은", "type" : "ETM", "position" : 195, "weight" : 0.155123 },
		{"id" : 4, "lemma" : "날", "type" : "NNG", "position" : 199, "weight" : 0.59791 },
		{"id" : 5, "lemma" : "의", "type" : "JKG", "position" : 202, "weight" : 0.0694213 },
		{"id" : 6, "lemma" : "남자", "type" : "NNG", "position" : 206, "weight" : 0.9 },
		{"id" : 7, "lemma" : "주인", "type" : "NNG", "position" : 213, "weight" : 0.9 },
		{"id" : 8, "lemma" : "공", "type" : "NNG", "position" : 219, "weight" : 0.175294 },
		{"id" : 9, "lemma" : "에게", "type" : "JKB", "position" : 222, "weight" : 0.153356 },
		{"id" : 10, "lemma" : "쓰이", "type" : "VV", "position" : 229, "weight" : 0.9 },
		{"id" : 11, "lemma" : "었", "type" : "EP", "position" : 232, "weight" : 0.9 },
		{"id" : 12, "lemma" : "던", "type" : "ETM", "position" : 235, "weight" : 0.107547 },
		{"id" : 13, "lemma" : "이", "type" : "MM", "position" : 239, "weight" : 0.00060084 },
		{"id" : 14, "lemma" : "호칭", "type" : "NNG", "position" : 243, "weight" : 0.9 },
		{"id" : 15, "lemma" : "은", "type" : "JX", "position" : 249, "weight" : 0.0449928 },
		{"id" : 16, "lemma" : "무엇", "type" : "NP", "position" : 253, "weight" : 0.9 },
		{"id" : 17, "lemma" : "이", "type" : "VCP", "position" : 259, "weight" : 0.0175768 },
		{"id" : 18, "lemma" : "ㄹ까", "type" : "EF", "position" : 259, "weight" : 0.258243 },
		{"id" : 19, "lemma" : "?", "type" : "SF", "position" : 265, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "소설/NNG", "target" : "소설", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "운수/NNG+좋/VA+은/ETM", "target" : "운수좋은", "word_id" : 1, "m_begin" : 1, "m_end" : 3},
		{"id" : 2, "result" : "날/NNG+의/JKG", "target" : "날의", "word_id" : 2, "m_begin" : 4, "m_end" : 5},
		{"id" : 3, "result" : "남자/NNG", "target" : "남자", "word_id" : 3, "m_begin" : 6, "m_end" : 6},
		{"id" : 4, "result" : "주인공/NNG+에게/JKB", "target" : "주인공에게", "word_id" : 4, "m_begin" : 7, "m_end" : 9},
		{"id" : 5, "result" : "쓰이/VV+었/EP+던/ETM", "target" : "쓰였던", "word_id" : 5, "m_begin" : 10, "m_end" : 12},
		{"id" : 6, "result" : "이/MM", "target" : "이", "word_id" : 6, "m_begin" : 13, "m_end" : 13},
		{"id" : 7, "result" : "호칭/NNG+은/JX", "target" : "호칭은", "word_id" : 7, "m_begin" : 14, "m_end" : 15},
		{"id" : 8, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 8, "m_begin" : 16, "m_end" : 19}
	],
	"WSD" : [
		{"id" : 0, "text" : "소설", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 179, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "운수", "type" : "NNG", "scode" : "05", "weight" : 1, "position" : 186, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "좋", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 192, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "은", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 195, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "날", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 199, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 202, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "남자", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 206, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "주인공", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 213, "begin" : 7, "end" : 8},
		{"id" : 8, "text" : "에게", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 222, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "쓰이", "type" : "VV", "scode" : "03", "weight" : 1, "position" : 229, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 232, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "던", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 235, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 239, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "호칭", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 243, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 249, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 253, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 259, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 259, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 265, "begin" : 19, "end" : 19}
	],
	"word" : [
		{"id" : 0, "text" : "소설", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "운수좋은", "type" : "", "begin" : 1, "end" : 3},
		{"id" : 2, "text" : "날의", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 3, "text" : "남자", "type" : "", "begin" : 6, "end" : 6},
		{"id" : 4, "text" : "주인공에게", "type" : "", "begin" : 7, "end" : 9},
		{"id" : 5, "text" : "쓰였던", "type" : "", "begin" : 10, "end" : 12},
		{"id" : 6, "text" : "이", "type" : "", "begin" : 13, "end" : 13},
		{"id" : 7, "text" : "호칭은", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 8, "text" : "무엇일까?", "type" : "", "begin" : 16, "end" : 19}
	],
	"NE" : [
		{"id" : 0, "text" : "소설", "type" : "FD_ART", "begin" : 0, "end" : 0, "weight" : 0.358425, "common_noun" : 0},
		{"id" : 1, "text" : "운수좋은 날", "type" : "AFW_DOCUMENT", "begin" : 1, "end" : 4, "weight" : 0.876047, "common_noun" : 0},
		{"id" : 2, "text" : "남자 주인공", "type" : "CV_POSITION", "begin" : 6, "end" : 8, "weight" : 0.238782, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "소설", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.664405 },
		{"id" : 1, "text" : "운수좋은", "head" : 2, "label" : "VP_MOD", "mod" : [], "weight" : 0.877602 },
		{"id" : 2, "text" : "날의", "head" : 4, "label" : "NP_MOD", "mod" : [0, 1], "weight" : 0.733664 },
		{"id" : 3, "text" : "남자", "head" : 4, "label" : "NP", "mod" : [], "weight" : 0.823881 },
		{"id" : 4, "text" : "주인공에게", "head" : 5, "label" : "NP_AJT", "mod" : [2, 3], "weight" : 0.939385 },
		{"id" : 5, "text" : "쓰였던", "head" : 7, "label" : "VP_MOD", "mod" : [4], "weight" : 0.98025 },
		{"id" : 6, "text" : "이", "head" : 7, "label" : "DP", "mod" : [], "weight" : 0.94062 },
		{"id" : 7, "text" : "호칭은", "head" : 8, "label" : "NP_SBJ", "mod" : [5, 6], "weight" : 0.784463 },
		{"id" : 8, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [7], "weight" : 0.168571 }
	],
	"SRL" : [
		{"verb" : "운수", "sense" : 1, "word_id" : 1, "weight" : 0.119965,
			"argument" : [
				{"type" : "ARG1", "word_id" : 2, "text" : "날의", "weight" : 0.119965 }
			] },
		{"verb" : "쓰이", "sense" : 1, "word_id" : 5, "weight" : 0.295023,
			"argument" : [
				{"type" : "ARG2", "word_id" : 4, "text" : "주인공에게", "weight" : 0.318956 },
				{"type" : "ARG1", "word_id" : 7, "text" : "호칭은", "weight" : 0.271091 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "AFW_DOCUMENT", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 3, "sent_id" : 0, "start_eid" : 2, "end_eid" : 3, "ne_id" : 2, "text" : "중추부의 정삼품", "start_eid_short" : 2, "end_eid_short" : 3, "text_short" : "중추부의 정삼품", "weight" : 0.005 },
		{"id" : 11, "sent_id" : 1, "start_eid" : 2, "end_eid" : 2, "ne_id" : 1, "text" : "날", "start_eid_short" : 2, "end_eid_short" : 2, "text_short" : "날", "weight" : 0.006 }
	] },
	{"id" : 1, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 7, "sent_id" : 0, "start_eid" : 9, "end_eid" : 11, "ne_id" : -1, "text" : "나이 많은 남자", "start_eid_short" : 11, "end_eid_short" : 11, "text_short" : "남자", "weight" : 0.002 },
		{"id" : 12, "sent_id" : 1, "start_eid" : 6, "end_eid" : 7, "ne_id" : -1, "text" : "이 호칭", "start_eid_short" : 6, "end_eid_short" : 7, "text_short" : "이 호칭", "weight" : 0.006 },
		{"id" : 14, "sent_id" : 1, "start_eid" : 8, "end_eid" : 8, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 8, "end_eid_short" : 8, "text_short" : "무엇이ㄹ까?", "weight" : 0.006 }
	] }
 ]
}

