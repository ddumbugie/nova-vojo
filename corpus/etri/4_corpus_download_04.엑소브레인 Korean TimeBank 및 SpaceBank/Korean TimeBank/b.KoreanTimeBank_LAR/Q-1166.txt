{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "봄과 가을에 3~4일의 간격을 두고 이동성 고기압의 형태로 한반도에 다가와 따뜻하고 건조한 날씨를 지속시키는 기단은 무엇일까?",
	"morp" : [
		{"id" : 0, "lemma" : "봄", "type" : "NNG", "position" : 0, "weight" : 0.432085 },
		{"id" : 1, "lemma" : "과", "type" : "JC", "position" : 3, "weight" : 0.017569 },
		{"id" : 2, "lemma" : "가을", "type" : "NNG", "position" : 7, "weight" : 0.9 },
		{"id" : 3, "lemma" : "에", "type" : "JKB", "position" : 13, "weight" : 0.153364 },
		{"id" : 4, "lemma" : "3", "type" : "SN", "position" : 17, "weight" : 1 },
		{"id" : 5, "lemma" : "~", "type" : "SO", "position" : 18, "weight" : 1 },
		{"id" : 6, "lemma" : "4", "type" : "SN", "position" : 19, "weight" : 1 },
		{"id" : 7, "lemma" : "일", "type" : "NNB", "position" : 20, "weight" : 0.126777 },
		{"id" : 8, "lemma" : "의", "type" : "JKG", "position" : 23, "weight" : 0.0520933 },
		{"id" : 9, "lemma" : "간격", "type" : "NNG", "position" : 27, "weight" : 0.9 },
		{"id" : 10, "lemma" : "을", "type" : "JKO", "position" : 33, "weight" : 0.129611 },
		{"id" : 11, "lemma" : "두", "type" : "VV", "position" : 37, "weight" : 0.192556 },
		{"id" : 12, "lemma" : "고", "type" : "EC", "position" : 40, "weight" : 0.416679 },
		{"id" : 13, "lemma" : "이동", "type" : "NNG", "position" : 44, "weight" : 0.217633 },
		{"id" : 14, "lemma" : "성", "type" : "XSN", "position" : 50, "weight" : 0.0161708 },
		{"id" : 15, "lemma" : "고", "type" : "XPN", "position" : 54, "weight" : 0.0001 },
		{"id" : 16, "lemma" : "기압", "type" : "NNG", "position" : 57, "weight" : 0.9 },
		{"id" : 17, "lemma" : "의", "type" : "JKG", "position" : 63, "weight" : 0.0694213 },
		{"id" : 18, "lemma" : "형태", "type" : "NNG", "position" : 67, "weight" : 0.869758 },
		{"id" : 19, "lemma" : "로", "type" : "JKB", "position" : 73, "weight" : 0.153229 },
		{"id" : 20, "lemma" : "한반도", "type" : "NNP", "position" : 77, "weight" : 0.0216994 },
		{"id" : 21, "lemma" : "에", "type" : "JKB", "position" : 86, "weight" : 0.0823628 },
		{"id" : 22, "lemma" : "다가오", "type" : "VV", "position" : 90, "weight" : 0.9 },
		{"id" : 23, "lemma" : "아", "type" : "EC", "position" : 96, "weight" : 0.398809 },
		{"id" : 24, "lemma" : "따뜻하", "type" : "VA", "position" : 100, "weight" : 0.9 },
		{"id" : 25, "lemma" : "고", "type" : "EC", "position" : 109, "weight" : 0.310376 },
		{"id" : 26, "lemma" : "건조", "type" : "NNG", "position" : 113, "weight" : 0.9 },
		{"id" : 27, "lemma" : "하", "type" : "XSA", "position" : 119, "weight" : 0.0001 },
		{"id" : 28, "lemma" : "ㄴ", "type" : "ETM", "position" : 119, "weight" : 0.488779 },
		{"id" : 29, "lemma" : "날씨", "type" : "NNG", "position" : 123, "weight" : 0.9 },
		{"id" : 30, "lemma" : "를", "type" : "JKO", "position" : 129, "weight" : 0.137686 },
		{"id" : 31, "lemma" : "지속", "type" : "NNG", "position" : 133, "weight" : 0.9 },
		{"id" : 32, "lemma" : "시키", "type" : "XSV", "position" : 139, "weight" : 0.00340357 },
		{"id" : 33, "lemma" : "는", "type" : "ETM", "position" : 145, "weight" : 0.238503 },
		{"id" : 34, "lemma" : "기단", "type" : "NNG", "position" : 149, "weight" : 0.9 },
		{"id" : 35, "lemma" : "은", "type" : "JX", "position" : 155, "weight" : 0.0449928 },
		{"id" : 36, "lemma" : "무엇", "type" : "NP", "position" : 159, "weight" : 0.9 },
		{"id" : 37, "lemma" : "이", "type" : "VCP", "position" : 165, "weight" : 0.0175768 },
		{"id" : 38, "lemma" : "ㄹ까", "type" : "EF", "position" : 165, "weight" : 0.258243 },
		{"id" : 39, "lemma" : "?", "type" : "SF", "position" : 171, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "봄/NNG+과/JC", "target" : "봄과", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "가을/NNG+에/JKB", "target" : "가을에", "word_id" : 1, "m_begin" : 2, "m_end" : 3},
		{"id" : 2, "result" : "3/SN+~/SO+4/SN+일/NNB+의/JKG", "target" : "3~4일의", "word_id" : 2, "m_begin" : 4, "m_end" : 8},
		{"id" : 3, "result" : "간격/NNG+을/JKO", "target" : "간격을", "word_id" : 3, "m_begin" : 9, "m_end" : 10},
		{"id" : 4, "result" : "두/VV+고/EC", "target" : "두고", "word_id" : 4, "m_begin" : 11, "m_end" : 12},
		{"id" : 5, "result" : "이동성/NNG", "target" : "이동성", "word_id" : 5, "m_begin" : 13, "m_end" : 14},
		{"id" : 6, "result" : "고기압/NNG+의/JKG", "target" : "고기압의", "word_id" : 6, "m_begin" : 15, "m_end" : 17},
		{"id" : 7, "result" : "형태/NNG+로/JKB", "target" : "형태로", "word_id" : 7, "m_begin" : 18, "m_end" : 19},
		{"id" : 8, "result" : "한반도/NNG+에/JKB", "target" : "한반도에", "word_id" : 8, "m_begin" : 20, "m_end" : 21},
		{"id" : 9, "result" : "다가오/VV+어/EC", "target" : "다가와", "word_id" : 9, "m_begin" : 22, "m_end" : 23},
		{"id" : 10, "result" : "따뜻하/VA+고/EC", "target" : "따뜻하고", "word_id" : 10, "m_begin" : 24, "m_end" : 25},
		{"id" : 11, "result" : "건조하/VA+ㄴ/ETM", "target" : "건조한", "word_id" : 11, "m_begin" : 26, "m_end" : 28},
		{"id" : 12, "result" : "날씨/NNG+를/JKO", "target" : "날씨를", "word_id" : 12, "m_begin" : 29, "m_end" : 30},
		{"id" : 13, "result" : "지속시키/VV+는/ETM", "target" : "지속시키는", "word_id" : 13, "m_begin" : 31, "m_end" : 33},
		{"id" : 14, "result" : "기단/NNG+은/JX", "target" : "기단은", "word_id" : 14, "m_begin" : 34, "m_end" : 35},
		{"id" : 15, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 15, "m_begin" : 36, "m_end" : 39}
	],
	"WSD" : [
		{"id" : 0, "text" : "봄", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "과", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "가을", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 7, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 13, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "3", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 17, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "~", "type" : "SO", "scode" : "00", "weight" : 1, "position" : 18, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "4", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 19, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "일", "type" : "NNB", "scode" : "07", "weight" : 1, "position" : 20, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 23, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "간격", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 27, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 33, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "두", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 37, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "이동성", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 44, "begin" : 13, "end" : 14},
		{"id" : 14, "text" : "고기압", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 54, "begin" : 15, "end" : 16},
		{"id" : 15, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 63, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : "형태", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 67, "begin" : 18, "end" : 18},
		{"id" : 17, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 73, "begin" : 19, "end" : 19},
		{"id" : 18, "text" : "한반도", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 77, "begin" : 20, "end" : 20},
		{"id" : 19, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 86, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : "다가오", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 90, "begin" : 22, "end" : 22},
		{"id" : 21, "text" : "아", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 96, "begin" : 23, "end" : 23},
		{"id" : 22, "text" : "따뜻하", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 100, "begin" : 24, "end" : 24},
		{"id" : 23, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 109, "begin" : 25, "end" : 25},
		{"id" : 24, "text" : "건조하", "type" : "VA", "scode" : "02", "weight" : 1, "position" : 113, "begin" : 26, "end" : 27},
		{"id" : 25, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 119, "begin" : 28, "end" : 28},
		{"id" : 26, "text" : "날씨", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 123, "begin" : 29, "end" : 29},
		{"id" : 27, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 129, "begin" : 30, "end" : 30},
		{"id" : 28, "text" : "지속", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 133, "begin" : 31, "end" : 31},
		{"id" : 29, "text" : "시키", "type" : "XSV", "scode" : "00", "weight" : 1, "position" : 139, "begin" : 32, "end" : 32},
		{"id" : 30, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 145, "begin" : 33, "end" : 33},
		{"id" : 31, "text" : "기단", "type" : "NNG", "scode" : "05", "weight" : 1, "position" : 149, "begin" : 34, "end" : 34},
		{"id" : 32, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 155, "begin" : 35, "end" : 35},
		{"id" : 33, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 159, "begin" : 36, "end" : 36},
		{"id" : 34, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 165, "begin" : 37, "end" : 37},
		{"id" : 35, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 165, "begin" : 38, "end" : 38},
		{"id" : 36, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 171, "begin" : 39, "end" : 39}
	],
	"word" : [
		{"id" : 0, "text" : "봄과", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "가을에", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 2, "text" : "3~4일의", "type" : "", "begin" : 4, "end" : 8},
		{"id" : 3, "text" : "간격을", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 4, "text" : "두고", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 5, "text" : "이동성", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 6, "text" : "고기압의", "type" : "", "begin" : 15, "end" : 17},
		{"id" : 7, "text" : "형태로", "type" : "", "begin" : 18, "end" : 19},
		{"id" : 8, "text" : "한반도에", "type" : "", "begin" : 20, "end" : 21},
		{"id" : 9, "text" : "다가와", "type" : "", "begin" : 22, "end" : 23},
		{"id" : 10, "text" : "따뜻하고", "type" : "", "begin" : 24, "end" : 25},
		{"id" : 11, "text" : "건조한", "type" : "", "begin" : 26, "end" : 28},
		{"id" : 12, "text" : "날씨를", "type" : "", "begin" : 29, "end" : 30},
		{"id" : 13, "text" : "지속시키는", "type" : "", "begin" : 31, "end" : 33},
		{"id" : 14, "text" : "기단은", "type" : "", "begin" : 34, "end" : 35},
		{"id" : 15, "text" : "무엇일까?", "type" : "", "begin" : 36, "end" : 39}
	],
	"NE" : [
		{"id" : 0, "text" : "봄", "type" : "DT_SEASON", "begin" : 0, "end" : 0, "weight" : 0.525499, "common_noun" : 0},
		{"id" : 1, "text" : "가을", "type" : "DT_SEASON", "begin" : 2, "end" : 2, "weight" : 0.639565, "common_noun" : 0},
		{"id" : 2, "text" : "3~4일", "type" : "DT_DURATION", "begin" : 4, "end" : 7, "weight" : 0.593946, "common_noun" : 0},
		{"id" : 3, "text" : "한반도", "type" : "LCG_BAY", "begin" : 20, "end" : 20, "weight" : 0.294091, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "봄과", "head" : 1, "label" : "NP_CNJ", "mod" : [], "weight" : 0.589078 },
		{"id" : 1, "text" : "가을에", "head" : 4, "label" : "NP_AJT", "mod" : [0], "weight" : 0.822706 },
		{"id" : 2, "text" : "3~4일의", "head" : 3, "label" : "NP_MOD", "mod" : [], "weight" : 0.575592 },
		{"id" : 3, "text" : "간격을", "head" : 4, "label" : "NP_OBJ", "mod" : [2], "weight" : 0.75929 },
		{"id" : 4, "text" : "두고", "head" : 9, "label" : "VP", "mod" : [1, 3], "weight" : 0.621888 },
		{"id" : 5, "text" : "이동성", "head" : 6, "label" : "NP", "mod" : [], "weight" : 0.727425 },
		{"id" : 6, "text" : "고기압의", "head" : 7, "label" : "NP_MOD", "mod" : [5], "weight" : 0.749428 },
		{"id" : 7, "text" : "형태로", "head" : 9, "label" : "NP_AJT", "mod" : [6], "weight" : 0.608149 },
		{"id" : 8, "text" : "한반도에", "head" : 9, "label" : "NP_AJT", "mod" : [], "weight" : 0.672656 },
		{"id" : 9, "text" : "다가와", "head" : 13, "label" : "VP", "mod" : [4, 7, 8], "weight" : 0.750264 },
		{"id" : 10, "text" : "따뜻하고", "head" : 11, "label" : "VP", "mod" : [], "weight" : 0.72667 },
		{"id" : 11, "text" : "건조한", "head" : 12, "label" : "VP_MOD", "mod" : [10], "weight" : 0.851265 },
		{"id" : 12, "text" : "날씨를", "head" : 13, "label" : "NP_OBJ", "mod" : [11], "weight" : 0.644616 },
		{"id" : 13, "text" : "지속시키는", "head" : 14, "label" : "VP_MOD", "mod" : [9, 12], "weight" : 0.788042 },
		{"id" : 14, "text" : "기단은", "head" : 15, "label" : "NP_SBJ", "mod" : [13], "weight" : 0.803888 },
		{"id" : 15, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [14], "weight" : 0.00486216 }
	],
	"SRL" : [
		{"verb" : "두", "sense" : 1, "word_id" : 4, "weight" : 0.329042,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 1, "text" : "가을에", "weight" : 0.293759 },
				{"type" : "ARG1", "word_id" : 3, "text" : "간격을", "weight" : 0.364325 }
			] },
		{"verb" : "다가오", "sense" : 1, "word_id" : 9, "weight" : 0.339306,
			"argument" : [
				{"type" : "ARGM-PRD", "word_id" : 7, "text" : "형태로", "weight" : 0.350049 },
				{"type" : "ARG3", "word_id" : 8, "text" : "한반도에", "weight" : 0.328562 }
			] },
		{"verb" : "따뜻하", "sense" : 1, "word_id" : 10, "weight" : 0.713599,
			"argument" : [
				{"type" : "ARG1", "word_id" : 12, "text" : "날씨를", "weight" : 0.713599 }
			] },
		{"verb" : "건조", "sense" : 1, "word_id" : 11, "weight" : 0.511833,
			"argument" : [
				{"type" : "ARG1", "word_id" : 12, "text" : "날씨를", "weight" : 0.511833 }
			] },
		{"verb" : "지속", "sense" : 1, "word_id" : 13, "weight" : 0.438086,
			"argument" : [
				{"type" : "ARG1", "word_id" : 12, "text" : "날씨를", "weight" : 0.538559 },
				{"type" : "ARG0", "word_id" : 14, "text" : "기단은", "weight" : 0.337614 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 4, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.999462 },
		{"id" : 1, "verb_wid" : 9, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.978511 },
		{"id" : 2, "verb_wid" : 10, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.975854 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 14, "ne_id" : -1, "text" : "봄과 가을에 3~4일의 간격을 두고 이동성 고기압의 형태로 한반도에 다가와 따뜻하고 건조한 날씨를 지속시키는 기단", "start_eid_short" : 14, "end_eid_short" : 14, "text_short" : "기단", "weight" : 0.002 },
		{"id" : 11, "sent_id" : 0, "start_eid" : 14, "end_eid" : 15, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 15, "end_eid_short" : 15, "text_short" : "무엇이ㄹ까?", "weight" : 0.006 }
	] }
 ]
}
