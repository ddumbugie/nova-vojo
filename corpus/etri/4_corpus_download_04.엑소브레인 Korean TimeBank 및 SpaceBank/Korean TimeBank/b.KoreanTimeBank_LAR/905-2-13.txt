{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "조선 왕조 시기의 수필 또는 가사 작품 중에서 기행을 소재로 한 작품이 아닌 것은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "조선", "type" : "NNP", "position" : 0, "weight" : 0.0878802 },
		{"id" : 1, "lemma" : "왕조", "type" : "NNG", "position" : 7, "weight" : 0.9 },
		{"id" : 2, "lemma" : "시기", "type" : "NNG", "position" : 14, "weight" : 0.184726 },
		{"id" : 3, "lemma" : "의", "type" : "JKG", "position" : 20, "weight" : 0.0694213 },
		{"id" : 4, "lemma" : "수필", "type" : "NNG", "position" : 24, "weight" : 0.865944 },
		{"id" : 5, "lemma" : "또는", "type" : "MAJ", "position" : 31, "weight" : 0.0017557 },
		{"id" : 6, "lemma" : "가사", "type" : "NNG", "position" : 38, "weight" : 0.413016 },
		{"id" : 7, "lemma" : "작품", "type" : "NNG", "position" : 45, "weight" : 0.18482 },
		{"id" : 8, "lemma" : "중", "type" : "NNB", "position" : 52, "weight" : 0.013531 },
		{"id" : 9, "lemma" : "에서", "type" : "JKB", "position" : 55, "weight" : 0.135597 },
		{"id" : 10, "lemma" : "기행", "type" : "NNG", "position" : 62, "weight" : 0.206945 },
		{"id" : 11, "lemma" : "을", "type" : "JKO", "position" : 68, "weight" : 0.129611 },
		{"id" : 12, "lemma" : "소재", "type" : "NNG", "position" : 72, "weight" : 0.10119 },
		{"id" : 13, "lemma" : "로", "type" : "JKB", "position" : 78, "weight" : 0.153229 },
		{"id" : 14, "lemma" : "하", "type" : "VV", "position" : 82, "weight" : 0.275522 },
		{"id" : 15, "lemma" : "ㄴ", "type" : "ETM", "position" : 82, "weight" : 0.304215 },
		{"id" : 16, "lemma" : "작품", "type" : "NNG", "position" : 86, "weight" : 0.65877 },
		{"id" : 17, "lemma" : "이", "type" : "JKC", "position" : 92, "weight" : 0.000287945 },
		{"id" : 18, "lemma" : "아니", "type" : "VCN", "position" : 96, "weight" : 0.354175 },
		{"id" : 19, "lemma" : "ㄴ", "type" : "ETM", "position" : 99, "weight" : 0.144018 },
		{"id" : 20, "lemma" : "것", "type" : "NNB", "position" : 103, "weight" : 0.228788 },
		{"id" : 21, "lemma" : "은", "type" : "JX", "position" : 106, "weight" : 0.0688243 },
		{"id" : 22, "lemma" : "무엇", "type" : "NP", "position" : 110, "weight" : 0.9 },
		{"id" : 23, "lemma" : "이", "type" : "VCP", "position" : 116, "weight" : 0.0175768 },
		{"id" : 24, "lemma" : "ㄹ까", "type" : "EF", "position" : 116, "weight" : 0.258243 },
		{"id" : 25, "lemma" : "?", "type" : "SF", "position" : 122, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "조선/NNG", "target" : "조선", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "왕조/NNG", "target" : "왕조", "word_id" : 1, "m_begin" : 1, "m_end" : 1},
		{"id" : 2, "result" : "시기/NNG+의/JKG", "target" : "시기의", "word_id" : 2, "m_begin" : 2, "m_end" : 3},
		{"id" : 3, "result" : "수필/NNG", "target" : "수필", "word_id" : 3, "m_begin" : 4, "m_end" : 4},
		{"id" : 4, "result" : "또는/MAJ", "target" : "또는", "word_id" : 4, "m_begin" : 5, "m_end" : 5},
		{"id" : 5, "result" : "가사/NNG", "target" : "가사", "word_id" : 5, "m_begin" : 6, "m_end" : 6},
		{"id" : 6, "result" : "작품/NNG", "target" : "작품", "word_id" : 6, "m_begin" : 7, "m_end" : 7},
		{"id" : 7, "result" : "중/NNB+에서/JKB", "target" : "중에서", "word_id" : 7, "m_begin" : 8, "m_end" : 9},
		{"id" : 8, "result" : "기행/NNG+을/JKO", "target" : "기행을", "word_id" : 8, "m_begin" : 10, "m_end" : 11},
		{"id" : 9, "result" : "소재/NNG+로/JKB", "target" : "소재로", "word_id" : 9, "m_begin" : 12, "m_end" : 13},
		{"id" : 10, "result" : "하/VV+ㄴ/ETM", "target" : "한", "word_id" : 10, "m_begin" : 14, "m_end" : 15},
		{"id" : 11, "result" : "작품/NNG+이/JKC", "target" : "작품이", "word_id" : 11, "m_begin" : 16, "m_end" : 17},
		{"id" : 12, "result" : "아니/VCN+ㄴ/ETM", "target" : "아닌", "word_id" : 12, "m_begin" : 18, "m_end" : 19},
		{"id" : 13, "result" : "것/NNB+은/JX", "target" : "것은", "word_id" : 13, "m_begin" : 20, "m_end" : 21},
		{"id" : 14, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 14, "m_begin" : 22, "m_end" : 25}
	],
	"WSD" : [
		{"id" : 0, "text" : "조선", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "왕조", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 7, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "시기", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 14, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 20, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "수필", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 24, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "또는", "type" : "MAJ", "scode" : "00", "weight" : 1, "position" : 31, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "가사", "type" : "NNG", "scode" : "09", "weight" : 1, "position" : 38, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "작품", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 45, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "중", "type" : "NNB", "scode" : "04", "weight" : 1, "position" : 52, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 55, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "기행", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 62, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 68, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "소재", "type" : "NNG", "scode" : "05", "weight" : 1, "position" : 72, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 78, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 82, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 82, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "작품", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 86, "begin" : 16, "end" : 16},
		{"id" : 17, "text" : "이", "type" : "JKC", "scode" : "00", "weight" : 1, "position" : 92, "begin" : 17, "end" : 17},
		{"id" : 18, "text" : "아니", "type" : "VCN", "scode" : "00", "weight" : 1, "position" : 96, "begin" : 18, "end" : 18},
		{"id" : 19, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 99, "begin" : 19, "end" : 19},
		{"id" : 20, "text" : "것", "type" : "NNB", "scode" : "01", "weight" : 1, "position" : 103, "begin" : 20, "end" : 20},
		{"id" : 21, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 106, "begin" : 21, "end" : 21},
		{"id" : 22, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 110, "begin" : 22, "end" : 22},
		{"id" : 23, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 116, "begin" : 23, "end" : 23},
		{"id" : 24, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 116, "begin" : 24, "end" : 24},
		{"id" : 25, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 122, "begin" : 25, "end" : 25}
	],
	"word" : [
		{"id" : 0, "text" : "조선", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "왕조", "type" : "", "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "시기의", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 3, "text" : "수필", "type" : "", "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "또는", "type" : "", "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "가사", "type" : "", "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "작품", "type" : "", "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "중에서", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 8, "text" : "기행을", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 9, "text" : "소재로", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 10, "text" : "한", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 11, "text" : "작품이", "type" : "", "begin" : 16, "end" : 17},
		{"id" : 12, "text" : "아닌", "type" : "", "begin" : 18, "end" : 19},
		{"id" : 13, "text" : "것은", "type" : "", "begin" : 20, "end" : 21},
		{"id" : 14, "text" : "무엇일까?", "type" : "", "begin" : 22, "end" : 25}
	],
	"NE" : [
		{"id" : 0, "text" : "조선 왕조", "type" : "DT_DYNASTY", "begin" : 0, "end" : 1, "weight" : 0.639682, "common_noun" : 0},
		{"id" : 1, "text" : "수필", "type" : "FD_ART", "begin" : 4, "end" : 4, "weight" : 0.427617, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "조선", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.693614 },
		{"id" : 1, "text" : "왕조", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.972273 },
		{"id" : 2, "text" : "시기의", "head" : 6, "label" : "NP_MOD", "mod" : [1], "weight" : 0.661921 },
		{"id" : 3, "text" : "수필", "head" : 6, "label" : "NP_CNJ", "mod" : [], "weight" : 0.881034 },
		{"id" : 4, "text" : "또는", "head" : 6, "label" : "AP", "mod" : [], "weight" : 0.693908 },
		{"id" : 5, "text" : "가사", "head" : 6, "label" : "NP", "mod" : [], "weight" : 0.921038 },
		{"id" : 6, "text" : "작품", "head" : 7, "label" : "NP", "mod" : [2, 3, 4, 5], "weight" : 0.757099 },
		{"id" : 7, "text" : "중에서", "head" : 10, "label" : "NP_AJT", "mod" : [6], "weight" : 0.622686 },
		{"id" : 8, "text" : "기행을", "head" : 10, "label" : "NP_OBJ", "mod" : [], "weight" : 0.78832 },
		{"id" : 9, "text" : "소재로", "head" : 10, "label" : "NP_AJT", "mod" : [], "weight" : 0.853522 },
		{"id" : 10, "text" : "한", "head" : 11, "label" : "VP_MOD", "mod" : [7, 8, 9], "weight" : 0.853721 },
		{"id" : 11, "text" : "작품이", "head" : 12, "label" : "NP_CMP", "mod" : [10], "weight" : 0.990742 },
		{"id" : 12, "text" : "아닌", "head" : 13, "label" : "VP_MOD", "mod" : [11], "weight" : 0.816221 },
		{"id" : 13, "text" : "것은", "head" : 14, "label" : "NP_SBJ", "mod" : [12], "weight" : 0.640851 },
		{"id" : 14, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [13], "weight" : 0.0229033 }
	],
	"SRL" : [
		{"verb" : "하", "sense" : 3, "word_id" : 10, "weight" : 0.272922,
			"argument" : [
				{"type" : "ARG1", "word_id" : 8, "text" : "기행을", "weight" : 0.271212 },
				{"type" : "ARG2", "word_id" : 9, "text" : "소재로", "weight" : 0.200351 },
				{"type" : "ARG0", "word_id" : 11, "text" : "작품이", "weight" : 0.347203 }
			] },
		{"verb" : "아니", "sense" : 1, "word_id" : 12, "weight" : 0.261724,
			"argument" : [
				{"type" : "ARG2", "word_id" : 11, "text" : "작품이", "weight" : 0.373785 },
				{"type" : "ARG1", "word_id" : 13, "text" : "것은", "weight" : 0.149663 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
 ]
}

