{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿토월회(土月會)는 1923년 5월 일제 강점기 일본에서 결성된 연극 공연 단체이다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿토월회", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "(", "type" : "SS", "position" : 12, "weight" : 1 },
		{"id" : 2, "lemma" : "土月會", "type" : "SH", "position" : 13, "weight" : 1 },
		{"id" : 3, "lemma" : ")", "type" : "SS", "position" : 22, "weight" : 1 },
		{"id" : 4, "lemma" : "는", "type" : "JX", "position" : 23, "weight" : 0.00823314 },
		{"id" : 5, "lemma" : "1923", "type" : "SN", "position" : 27, "weight" : 1 },
		{"id" : 6, "lemma" : "년", "type" : "NNB", "position" : 31, "weight" : 0.414343 },
		{"id" : 7, "lemma" : "5", "type" : "SN", "position" : 35, "weight" : 1 },
		{"id" : 8, "lemma" : "월", "type" : "NNB", "position" : 36, "weight" : 0.408539 },
		{"id" : 9, "lemma" : "일제", "type" : "NNG", "position" : 40, "weight" : 0.110549 },
		{"id" : 10, "lemma" : "강점", "type" : "NNG", "position" : 47, "weight" : 0.9 },
		{"id" : 11, "lemma" : "기", "type" : "XSN", "position" : 53, "weight" : 0.000294499 },
		{"id" : 12, "lemma" : "일본", "type" : "NNP", "position" : 57, "weight" : 0.00809622 },
		{"id" : 13, "lemma" : "에서", "type" : "JKB", "position" : 63, "weight" : 0.0823859 },
		{"id" : 14, "lemma" : "결성", "type" : "NNG", "position" : 70, "weight" : 0.9 },
		{"id" : 15, "lemma" : "되", "type" : "XSV", "position" : 76, "weight" : 0.000224177 },
		{"id" : 16, "lemma" : "ㄴ", "type" : "ETM", "position" : 76, "weight" : 0.392321 },
		{"id" : 17, "lemma" : "연극", "type" : "NNG", "position" : 80, "weight" : 0.658593 },
		{"id" : 18, "lemma" : "공연", "type" : "NNG", "position" : 87, "weight" : 0.9 },
		{"id" : 19, "lemma" : "단체", "type" : "NNG", "position" : 94, "weight" : 0.184808 },
		{"id" : 20, "lemma" : "이", "type" : "VCP", "position" : 100, "weight" : 0.0177525 },
		{"id" : 21, "lemma" : "다", "type" : "EF", "position" : 103, "weight" : 0.353579 },
		{"id" : 22, "lemma" : ".", "type" : "SF", "position" : 106, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿토월회/NNG+(/SS+土月會/SH+)/SS+는/JX", "target" : "﻿토월회(土月會)는", "word_id" : 0, "m_begin" : 0, "m_end" : 4},
		{"id" : 1, "result" : "1923/SN+년/NNB", "target" : "1923년", "word_id" : 1, "m_begin" : 5, "m_end" : 6},
		{"id" : 2, "result" : "5/SN+월/NNB", "target" : "5월", "word_id" : 2, "m_begin" : 7, "m_end" : 8},
		{"id" : 3, "result" : "일제/NNG", "target" : "일제", "word_id" : 3, "m_begin" : 9, "m_end" : 9},
		{"id" : 4, "result" : "강점기/NNG", "target" : "강점기", "word_id" : 4, "m_begin" : 10, "m_end" : 11},
		{"id" : 5, "result" : "일본/NNG+에서/JKB", "target" : "일본에서", "word_id" : 5, "m_begin" : 12, "m_end" : 13},
		{"id" : 6, "result" : "결성되/VV+ㄴ/ETM", "target" : "결성된", "word_id" : 6, "m_begin" : 14, "m_end" : 16},
		{"id" : 7, "result" : "연극/NNG", "target" : "연극", "word_id" : 7, "m_begin" : 17, "m_end" : 17},
		{"id" : 8, "result" : "공연/NNG", "target" : "공연", "word_id" : 8, "m_begin" : 18, "m_end" : 18},
		{"id" : 9, "result" : "단체/NNG+이/VCP+다/EF+./SF", "target" : "단체이다.", "word_id" : 9, "m_begin" : 19, "m_end" : 22}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿토월회", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 12, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "土月會", "type" : "SH", "scode" : "00", "weight" : 1, "position" : 13, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 22, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 23, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "1923", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 27, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 31, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "5", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 35, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "월", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 36, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "일제", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 40, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "강점기", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 47, "begin" : 10, "end" : 11},
		{"id" : 11, "text" : "일본", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 57, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 63, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "결성되", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 70, "begin" : 14, "end" : 15},
		{"id" : 14, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 76, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : "연극", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 80, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : "공연", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 87, "begin" : 18, "end" : 18},
		{"id" : 17, "text" : "단체", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 94, "begin" : 19, "end" : 19},
		{"id" : 18, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 100, "begin" : 20, "end" : 20},
		{"id" : 19, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 103, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 106, "begin" : 22, "end" : 22}
	],
	"word" : [
		{"id" : 0, "text" : "﻿토월회(土月會)는", "type" : "", "begin" : 0, "end" : 4},
		{"id" : 1, "text" : "1923년", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 2, "text" : "5월", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 3, "text" : "일제", "type" : "", "begin" : 9, "end" : 9},
		{"id" : 4, "text" : "강점기", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 5, "text" : "일본에서", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 6, "text" : "결성된", "type" : "", "begin" : 14, "end" : 16},
		{"id" : 7, "text" : "연극", "type" : "", "begin" : 17, "end" : 17},
		{"id" : 8, "text" : "공연", "type" : "", "begin" : 18, "end" : 18},
		{"id" : 9, "text" : "단체이다.", "type" : "", "begin" : 19, "end" : 22}
	],
	"NE" : [
		{"id" : 0, "text" : "﻿토월회", "type" : "OG_OTHERS", "begin" : 0, "end" : 0, "weight" : 0.123315, "common_noun" : 0},
		{"id" : 1, "text" : "1923년 5월", "type" : "DT_OTHERS", "begin" : 5, "end" : 8, "weight" : 0.665052, "common_noun" : 0},
		{"id" : 2, "text" : "일제 강점기", "type" : "DT_OTHERS", "begin" : 9, "end" : 11, "weight" : 0.684981, "common_noun" : 0},
		{"id" : 3, "text" : "일본", "type" : "LCP_COUNTRY", "begin" : 12, "end" : 12, "weight" : 0.646496, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿토월회(土月會)는", "head" : 9, "label" : "NP_SBJ", "mod" : [], "weight" : 0.79518 },
		{"id" : 1, "text" : "1923년", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.934652 },
		{"id" : 2, "text" : "5월", "head" : 6, "label" : "NP_AJT", "mod" : [1], "weight" : 0.882412 },
		{"id" : 3, "text" : "일제", "head" : 4, "label" : "NP", "mod" : [], "weight" : 0.647871 },
		{"id" : 4, "text" : "강점기", "head" : 5, "label" : "NP", "mod" : [3], "weight" : 0.841419 },
		{"id" : 5, "text" : "일본에서", "head" : 6, "label" : "NP_AJT", "mod" : [4], "weight" : 0.766449 },
		{"id" : 6, "text" : "결성된", "head" : 9, "label" : "VP_MOD", "mod" : [2, 5], "weight" : 0.566696 },
		{"id" : 7, "text" : "연극", "head" : 8, "label" : "NP", "mod" : [], "weight" : 0.685389 },
		{"id" : 8, "text" : "공연", "head" : 9, "label" : "NP", "mod" : [7], "weight" : 0.428492 },
		{"id" : 9, "text" : "단체이다.", "head" : -1, "label" : "VNP", "mod" : [0, 6, 8], "weight" : 0.0276669 }
	],
	"SRL" : [
		{"verb" : "결성", "sense" : 1, "word_id" : 6, "weight" : 0.267594,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 2, "text" : "5월", "weight" : 0.381781 },
				{"type" : "ARGM-LOC", "word_id" : 5, "text" : "일본에서", "weight" : 0.153407 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "극예술연구회와 더불어 신극 운동을 표방했다. ",
	"morp" : [
		{"id" : 0, "lemma" : "극", "type" : "NNG", "position" : 108, "weight" : 0.429964 },
		{"id" : 1, "lemma" : "예술", "type" : "NNG", "position" : 111, "weight" : 0.9 },
		{"id" : 2, "lemma" : "연구", "type" : "NNG", "position" : 117, "weight" : 0.9 },
		{"id" : 3, "lemma" : "회", "type" : "XSN", "position" : 123, "weight" : 0.010766 },
		{"id" : 4, "lemma" : "와", "type" : "JKB", "position" : 126, "weight" : 0.0450559 },
		{"id" : 5, "lemma" : "더불", "type" : "VV", "position" : 130, "weight" : 0.9 },
		{"id" : 6, "lemma" : "어", "type" : "EC", "position" : 136, "weight" : 0.41831 },
		{"id" : 7, "lemma" : "신극", "type" : "NNG", "position" : 140, "weight" : 0.9 },
		{"id" : 8, "lemma" : "운동", "type" : "NNG", "position" : 147, "weight" : 0.9 },
		{"id" : 9, "lemma" : "을", "type" : "JKO", "position" : 153, "weight" : 0.129611 },
		{"id" : 10, "lemma" : "표방", "type" : "NNG", "position" : 157, "weight" : 0.85 },
		{"id" : 11, "lemma" : "하", "type" : "XSV", "position" : 163, "weight" : 0.0001 },
		{"id" : 12, "lemma" : "었", "type" : "EP", "position" : 163, "weight" : 0.9 },
		{"id" : 13, "lemma" : "다", "type" : "EF", "position" : 166, "weight" : 0.640954 },
		{"id" : 14, "lemma" : ".", "type" : "SF", "position" : 169, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "극예술연구회/NNG+와/JKB", "target" : "극예술연구회와", "word_id" : 0, "m_begin" : 0, "m_end" : 4},
		{"id" : 1, "result" : "더불/VV+어/EC", "target" : "더불어", "word_id" : 1, "m_begin" : 5, "m_end" : 6},
		{"id" : 2, "result" : "신극/NNG", "target" : "신극", "word_id" : 2, "m_begin" : 7, "m_end" : 7},
		{"id" : 3, "result" : "운동/NNG+을/JKO", "target" : "운동을", "word_id" : 3, "m_begin" : 8, "m_end" : 9},
		{"id" : 4, "result" : "표방하/VV+었/EP+다/EF+./SF", "target" : "표방했다.", "word_id" : 4, "m_begin" : 10, "m_end" : 14}
	],
	"WSD" : [
		{"id" : 0, "text" : "극예술연구회", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 108, "begin" : 0, "end" : 3},
		{"id" : 1, "text" : "와", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 126, "begin" : 4, "end" : 4},
		{"id" : 2, "text" : "더불", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 130, "begin" : 5, "end" : 5},
		{"id" : 3, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 136, "begin" : 6, "end" : 6},
		{"id" : 4, "text" : "신극", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 140, "begin" : 7, "end" : 7},
		{"id" : 5, "text" : "운동", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 147, "begin" : 8, "end" : 8},
		{"id" : 6, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 153, "begin" : 9, "end" : 9},
		{"id" : 7, "text" : "표방하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 157, "begin" : 10, "end" : 11},
		{"id" : 8, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 163, "begin" : 12, "end" : 12},
		{"id" : 9, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 166, "begin" : 13, "end" : 13},
		{"id" : 10, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 169, "begin" : 14, "end" : 14}
	],
	"word" : [
		{"id" : 0, "text" : "극예술연구회와", "type" : "", "begin" : 0, "end" : 4},
		{"id" : 1, "text" : "더불어", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 2, "text" : "신극", "type" : "", "begin" : 7, "end" : 7},
		{"id" : 3, "text" : "운동을", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 4, "text" : "표방했다.", "type" : "", "begin" : 10, "end" : 14}
	],
	"NE" : [
		{"id" : 0, "text" : "극예술연구회", "type" : "OGG_ART", "begin" : 0, "end" : 3, "weight" : 0.8951, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "극예술연구회와", "head" : 1, "label" : "NP_AJT", "mod" : [], "weight" : 0.738561 },
		{"id" : 1, "text" : "더불어", "head" : 4, "label" : "VP", "mod" : [0], "weight" : 0.695432 },
		{"id" : 2, "text" : "신극", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.992338 },
		{"id" : 3, "text" : "운동을", "head" : 4, "label" : "NP_OBJ", "mod" : [2], "weight" : 0.572673 },
		{"id" : 4, "text" : "표방했다.", "head" : -1, "label" : "VP", "mod" : [1, 3], "weight" : 0.177668 }
	],
	"SRL" : [
		{"verb" : "표방", "sense" : 1, "word_id" : 4, "weight" : 0.166171,
			"argument" : [
				{"type" : "ARG0", "word_id" : 0, "text" : "극예술연구회와", "weight" : 0.179955 },
				{"type" : "ARGM-ADV", "word_id" : 1, "text" : "더불어", "weight" : 0.143314 },
				{"type" : "ARG1", "word_id" : 3, "text" : "운동을", "weight" : 0.175245 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 1, "ant_sid" : 0, "ant_wid" : 0, "type" : "s", "istitle" : 0, "weight" : 0.551885 },
		{"id" : 1, "verb_wid" : 4, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.479298 }
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "당시 동경에서 유학 중이던 박승희•김복진•김기진•이서구•김을한•박승목•이제창이 주축이 되어 신월회라는 이름의 문예 동아리로 출발하였다. ",
	"morp" : [
		{"id" : 0, "lemma" : "당시", "type" : "NNG", "position" : 171, "weight" : 0.9 },
		{"id" : 1, "lemma" : "동경", "type" : "NNP", "position" : 178, "weight" : 0.00457461 },
		{"id" : 2, "lemma" : "에서", "type" : "JKB", "position" : 184, "weight" : 0.0823859 },
		{"id" : 3, "lemma" : "유학", "type" : "NNG", "position" : 191, "weight" : 0.9 },
		{"id" : 4, "lemma" : "중", "type" : "NNB", "position" : 198, "weight" : 0.013531 },
		{"id" : 5, "lemma" : "이", "type" : "VCP", "position" : 201, "weight" : 0.0607843 },
		{"id" : 6, "lemma" : "던", "type" : "ETM", "position" : 204, "weight" : 0.230313 },
		{"id" : 7, "lemma" : "박승희", "type" : "NNP", "position" : 208, "weight" : 0.35 },
		{"id" : 8, "lemma" : "•", "type" : "SW", "position" : 217, "weight" : 1 },
		{"id" : 9, "lemma" : "김복진", "type" : "NNP", "position" : 220, "weight" : 0.1 },
		{"id" : 10, "lemma" : "•", "type" : "SW", "position" : 229, "weight" : 1 },
		{"id" : 11, "lemma" : "김기진", "type" : "NNP", "position" : 232, "weight" : 0.5 },
		{"id" : 12, "lemma" : "•", "type" : "SW", "position" : 241, "weight" : 1 },
		{"id" : 13, "lemma" : "이서구", "type" : "NNP", "position" : 244, "weight" : 0.7 },
		{"id" : 14, "lemma" : "•", "type" : "SW", "position" : 253, "weight" : 1 },
		{"id" : 15, "lemma" : "김을한", "type" : "NNP", "position" : 256, "weight" : 0.05 },
		{"id" : 16, "lemma" : "•", "type" : "SW", "position" : 265, "weight" : 1 },
		{"id" : 17, "lemma" : "박승목", "type" : "NNP", "position" : 268, "weight" : 0.6 },
		{"id" : 18, "lemma" : "•", "type" : "SW", "position" : 277, "weight" : 1 },
		{"id" : 19, "lemma" : "이제창", "type" : "NNP", "position" : 280, "weight" : 0.6 },
		{"id" : 20, "lemma" : "이", "type" : "JKS", "position" : 289, "weight" : 0.0234517 },
		{"id" : 21, "lemma" : "주축", "type" : "NNG", "position" : 293, "weight" : 0.9 },
		{"id" : 22, "lemma" : "이", "type" : "JKC", "position" : 299, "weight" : 0.000287945 },
		{"id" : 23, "lemma" : "되", "type" : "VV", "position" : 303, "weight" : 0.542406 },
		{"id" : 24, "lemma" : "어", "type" : "EC", "position" : 306, "weight" : 0.41831 },
		{"id" : 25, "lemma" : "신월회", "type" : "NNP", "position" : 310, "weight" : 0.6 },
		{"id" : 26, "lemma" : "이", "type" : "VCP", "position" : 316, "weight" : 0.00359898 },
		{"id" : 27, "lemma" : "라는", "type" : "ETM", "position" : 319, "weight" : 0.230361 },
		{"id" : 28, "lemma" : "이름", "type" : "NNG", "position" : 326, "weight" : 0.9 },
		{"id" : 29, "lemma" : "의", "type" : "JKG", "position" : 332, "weight" : 0.0694213 },
		{"id" : 30, "lemma" : "문예", "type" : "NNG", "position" : 336, "weight" : 0.861141 },
		{"id" : 31, "lemma" : "동아리", "type" : "NNG", "position" : 343, "weight" : 0.9 },
		{"id" : 32, "lemma" : "로", "type" : "JKB", "position" : 352, "weight" : 0.153229 },
		{"id" : 33, "lemma" : "출발", "type" : "NNG", "position" : 356, "weight" : 0.9 },
		{"id" : 34, "lemma" : "하", "type" : "XSV", "position" : 362, "weight" : 0.0001 },
		{"id" : 35, "lemma" : "었", "type" : "EP", "position" : 365, "weight" : 0.9 },
		{"id" : 36, "lemma" : "다", "type" : "EF", "position" : 368, "weight" : 0.640954 },
		{"id" : 37, "lemma" : ".", "type" : "SF", "position" : 371, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "당시/NNG", "target" : "당시", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "동경/NNG+에서/JKB", "target" : "동경에서", "word_id" : 1, "m_begin" : 1, "m_end" : 2},
		{"id" : 2, "result" : "유학/NNG", "target" : "유학", "word_id" : 2, "m_begin" : 3, "m_end" : 3},
		{"id" : 3, "result" : "중/NNB+이/VCP+던/ETM", "target" : "중이던", "word_id" : 3, "m_begin" : 4, "m_end" : 6},
		{"id" : 4, "result" : "박승희/NNG+•/SW+김복진/NNG+•/SW+김기진/NNG+•/SW+이서구/NNG+•/SW+김을한/NNG+•/SW+박승목/NNG+•/SW+이제창/NNG+이/JKS", "target" : "박승희•김복진•김기진•이서구•김을한•박승목•이제창이", "word_id" : 4, "m_begin" : 7, "m_end" : 20},
		{"id" : 5, "result" : "주축/NNG+이/JKC", "target" : "주축이", "word_id" : 5, "m_begin" : 21, "m_end" : 22},
		{"id" : 6, "result" : "되/VV+어/EC", "target" : "되어", "word_id" : 6, "m_begin" : 23, "m_end" : 24},
		{"id" : 7, "result" : "신월회/NNG+이/VCP+라는/ETM", "target" : "신월회라는", "word_id" : 7, "m_begin" : 25, "m_end" : 27},
		{"id" : 8, "result" : "이름/NNG+의/JKG", "target" : "이름의", "word_id" : 8, "m_begin" : 28, "m_end" : 29},
		{"id" : 9, "result" : "문예/NNG", "target" : "문예", "word_id" : 9, "m_begin" : 30, "m_end" : 30},
		{"id" : 10, "result" : "동아리/NNG+로/JKB", "target" : "동아리로", "word_id" : 10, "m_begin" : 31, "m_end" : 32},
		{"id" : 11, "result" : "출발하/VV+었/EP+다/EF+./SF", "target" : "출발하였다.", "word_id" : 11, "m_begin" : 33, "m_end" : 37}
	],
	"WSD" : [
		{"id" : 0, "text" : "당시", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 171, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "동경", "type" : "NNP", "scode" : "04", "weight" : 1, "position" : 178, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 184, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "유학", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 191, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "중", "type" : "NNB", "scode" : "04", "weight" : 1, "position" : 198, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 201, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "던", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 204, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "박승희", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 208, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "•", "type" : "SW", "scode" : "00", "weight" : 1, "position" : 217, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "김복진", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 220, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "•", "type" : "SW", "scode" : "00", "weight" : 1, "position" : 229, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "김기진", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 232, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "•", "type" : "SW", "scode" : "00", "weight" : 1, "position" : 241, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "이서구", "type" : "NNP", "scode" : "99", "weight" : 1, "position" : 244, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "•", "type" : "SW", "scode" : "00", "weight" : 1, "position" : 253, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "김을한", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 256, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "•", "type" : "SW", "scode" : "00", "weight" : 1, "position" : 265, "begin" : 16, "end" : 16},
		{"id" : 17, "text" : "박승목", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 268, "begin" : 17, "end" : 17},
		{"id" : 18, "text" : "•", "type" : "SW", "scode" : "00", "weight" : 1, "position" : 277, "begin" : 18, "end" : 18},
		{"id" : 19, "text" : "이제창", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 280, "begin" : 19, "end" : 19},
		{"id" : 20, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 289, "begin" : 20, "end" : 20},
		{"id" : 21, "text" : "주축", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 293, "begin" : 21, "end" : 21},
		{"id" : 22, "text" : "이", "type" : "JKC", "scode" : "00", "weight" : 1, "position" : 299, "begin" : 22, "end" : 22},
		{"id" : 23, "text" : "되", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 303, "begin" : 23, "end" : 23},
		{"id" : 24, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 306, "begin" : 24, "end" : 24},
		{"id" : 25, "text" : "신월회", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 310, "begin" : 25, "end" : 25},
		{"id" : 26, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 316, "begin" : 26, "end" : 26},
		{"id" : 27, "text" : "라는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 319, "begin" : 27, "end" : 27},
		{"id" : 28, "text" : "이름", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 326, "begin" : 28, "end" : 28},
		{"id" : 29, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 332, "begin" : 29, "end" : 29},
		{"id" : 30, "text" : "문예", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 336, "begin" : 30, "end" : 30},
		{"id" : 31, "text" : "동아리", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 343, "begin" : 31, "end" : 31},
		{"id" : 32, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 352, "begin" : 32, "end" : 32},
		{"id" : 33, "text" : "출발하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 356, "begin" : 33, "end" : 34},
		{"id" : 34, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 365, "begin" : 35, "end" : 35},
		{"id" : 35, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 368, "begin" : 36, "end" : 36},
		{"id" : 36, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 371, "begin" : 37, "end" : 37}
	],
	"word" : [
		{"id" : 0, "text" : "당시", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "동경에서", "type" : "", "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "유학", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "중이던", "type" : "", "begin" : 4, "end" : 6},
		{"id" : 4, "text" : "박승희•김복진•김기진•이서구•김을한•박승목•이제창이", "type" : "", "begin" : 7, "end" : 20},
		{"id" : 5, "text" : "주축이", "type" : "", "begin" : 21, "end" : 22},
		{"id" : 6, "text" : "되어", "type" : "", "begin" : 23, "end" : 24},
		{"id" : 7, "text" : "신월회라는", "type" : "", "begin" : 25, "end" : 27},
		{"id" : 8, "text" : "이름의", "type" : "", "begin" : 28, "end" : 29},
		{"id" : 9, "text" : "문예", "type" : "", "begin" : 30, "end" : 30},
		{"id" : 10, "text" : "동아리로", "type" : "", "begin" : 31, "end" : 32},
		{"id" : 11, "text" : "출발하였다.", "type" : "", "begin" : 33, "end" : 37}
	],
	"NE" : [
		{"id" : 0, "text" : "동경", "type" : "LCP_CAPITALCITY", "begin" : 1, "end" : 1, "weight" : 0.250046, "common_noun" : 0},
		{"id" : 1, "text" : "박승희", "type" : "PS_NAME", "begin" : 7, "end" : 7, "weight" : 0.541735, "common_noun" : 0},
		{"id" : 2, "text" : "김복진", "type" : "PS_NAME", "begin" : 9, "end" : 9, "weight" : 0.411597, "common_noun" : 0},
		{"id" : 3, "text" : "김기진", "type" : "PS_NAME", "begin" : 11, "end" : 11, "weight" : 0.720108, "common_noun" : 0},
		{"id" : 4, "text" : "이서구", "type" : "PS_NAME", "begin" : 13, "end" : 13, "weight" : 0.274141, "common_noun" : 0},
		{"id" : 5, "text" : "김을한", "type" : "PS_NAME", "begin" : 15, "end" : 15, "weight" : 0.306441, "common_noun" : 0},
		{"id" : 6, "text" : "박승목", "type" : "PS_NAME", "begin" : 17, "end" : 17, "weight" : 0.328462, "common_noun" : 0},
		{"id" : 7, "text" : "이제창", "type" : "PS_NAME", "begin" : 19, "end" : 19, "weight" : 0.191897, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "당시", "head" : 3, "label" : "NP_AJT", "mod" : [], "weight" : 0.581747 },
		{"id" : 1, "text" : "동경에서", "head" : 3, "label" : "NP_AJT", "mod" : [], "weight" : 0.572274 },
		{"id" : 2, "text" : "유학", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.679149 },
		{"id" : 3, "text" : "중이던", "head" : 4, "label" : "VNP_MOD", "mod" : [0, 1, 2], "weight" : 0.769729 },
		{"id" : 4, "text" : "박승희•김복진•김기진•이서구•김을한•박승목•이제창이", "head" : 6, "label" : "NP_SBJ", "mod" : [3], "weight" : 0.794403 },
		{"id" : 5, "text" : "주축이", "head" : 6, "label" : "NP_CMP", "mod" : [], "weight" : 0.590103 },
		{"id" : 6, "text" : "되어", "head" : 11, "label" : "VP", "mod" : [4, 5], "weight" : 0.565284 },
		{"id" : 7, "text" : "신월회라는", "head" : 8, "label" : "VNP_MOD", "mod" : [], "weight" : 0.946484 },
		{"id" : 8, "text" : "이름의", "head" : 10, "label" : "NP_MOD", "mod" : [7], "weight" : 0.762611 },
		{"id" : 9, "text" : "문예", "head" : 10, "label" : "NP", "mod" : [], "weight" : 0.830178 },
		{"id" : 10, "text" : "동아리로", "head" : 11, "label" : "NP_AJT", "mod" : [8, 9], "weight" : 0.520883 },
		{"id" : 11, "text" : "출발하였다.", "head" : -1, "label" : "VP", "mod" : [6, 10], "weight" : 0.00813163 }
	],
	"SRL" : [
		{"verb" : "되", "sense" : 4, "word_id" : 6, "weight" : 0.222016,
			"argument" : [
				{"type" : "ARG1", "word_id" : 4, "text" : "박승희•김복진•김기진•이서구•김을한•박승목•이제창이", "weight" : 0.293781 },
				{"type" : "ARG2", "word_id" : 5, "text" : "주축이", "weight" : 0.150252 }
			] },
		{"verb" : "출발", "sense" : 1, "word_id" : 11, "weight" : 0.130739,
			"argument" : [
				{"type" : "ARGM-MNR", "word_id" : 6, "text" : "되어", "weight" : 0.108511 },
				{"type" : "ARG2", "word_id" : 10, "text" : "동아리로", "weight" : 0.152966 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 11, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.352603 }
	]
	},
	{
	"id" : 3,
	"reserve_str" : "",
	"text" : "현실(土)을 지키며 이상(月)을 좇는다는 뜻에서 토월회로 바꾸었다. ",
	"morp" : [
		{"id" : 0, "lemma" : "현실", "type" : "NNG", "position" : 373, "weight" : 0.9 },
		{"id" : 1, "lemma" : "(", "type" : "SS", "position" : 379, "weight" : 1 },
		{"id" : 2, "lemma" : "土", "type" : "SH", "position" : 380, "weight" : 1 },
		{"id" : 3, "lemma" : ")", "type" : "SS", "position" : 383, "weight" : 1 },
		{"id" : 4, "lemma" : "을", "type" : "JKO", "position" : 384, "weight" : 0.0336085 },
		{"id" : 5, "lemma" : "지키", "type" : "VV", "position" : 388, "weight" : 0.9 },
		{"id" : 6, "lemma" : "며", "type" : "EC", "position" : 394, "weight" : 0.429701 },
		{"id" : 7, "lemma" : "이상", "type" : "NNG", "position" : 398, "weight" : 0.218107 },
		{"id" : 8, "lemma" : "(", "type" : "SS", "position" : 404, "weight" : 1 },
		{"id" : 9, "lemma" : "月", "type" : "SH", "position" : 405, "weight" : 1 },
		{"id" : 10, "lemma" : ")", "type" : "SS", "position" : 408, "weight" : 1 },
		{"id" : 11, "lemma" : "을", "type" : "JKO", "position" : 409, "weight" : 0.0336085 },
		{"id" : 12, "lemma" : "좇", "type" : "VV", "position" : 413, "weight" : 0.772522 },
		{"id" : 13, "lemma" : "는다는", "type" : "ETM", "position" : 416, "weight" : 0.317035 },
		{"id" : 14, "lemma" : "뜻", "type" : "NNG", "position" : 426, "weight" : 0.9 },
		{"id" : 15, "lemma" : "에서", "type" : "JKB", "position" : 429, "weight" : 0.153407 },
		{"id" : 16, "lemma" : "토월", "type" : "NNP", "position" : 436, "weight" : 0.15 },
		{"id" : 17, "lemma" : "회", "type" : "XSN", "position" : 442, "weight" : 0.00497171 },
		{"id" : 18, "lemma" : "로", "type" : "JKB", "position" : 445, "weight" : 0.121577 },
		{"id" : 19, "lemma" : "바꾸", "type" : "VV", "position" : 449, "weight" : 0.9 },
		{"id" : 20, "lemma" : "었", "type" : "EP", "position" : 455, "weight" : 0.9 },
		{"id" : 21, "lemma" : "다", "type" : "EF", "position" : 458, "weight" : 0.640954 },
		{"id" : 22, "lemma" : ".", "type" : "SF", "position" : 461, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "현실/NNG+(/SS+土/SH+)/SS+을/JKO", "target" : "현실(土)을", "word_id" : 0, "m_begin" : 0, "m_end" : 4},
		{"id" : 1, "result" : "지키/VV+며/EC", "target" : "지키며", "word_id" : 1, "m_begin" : 5, "m_end" : 6},
		{"id" : 2, "result" : "이상/NNG+(/SS+月/SH+)/SS+을/JKO", "target" : "이상(月)을", "word_id" : 2, "m_begin" : 7, "m_end" : 11},
		{"id" : 3, "result" : "좇/VV+는다는/ETM", "target" : "좇는다는", "word_id" : 3, "m_begin" : 12, "m_end" : 13},
		{"id" : 4, "result" : "뜻/NNG+에서/JKB", "target" : "뜻에서", "word_id" : 4, "m_begin" : 14, "m_end" : 15},
		{"id" : 5, "result" : "토월회/NNG+로/JKB", "target" : "토월회로", "word_id" : 5, "m_begin" : 16, "m_end" : 18},
		{"id" : 6, "result" : "바꾸/VV+었/EP+다/EF+./SF", "target" : "바꾸었다.", "word_id" : 6, "m_begin" : 19, "m_end" : 22}
	],
	"WSD" : [
		{"id" : 0, "text" : "현실", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 373, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 379, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "土", "type" : "SH", "scode" : "00", "weight" : 1, "position" : 380, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 383, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 384, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "지키", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 388, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "며", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 394, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "이상", "type" : "NNG", "scode" : "05", "weight" : 1, "position" : 398, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 404, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "月", "type" : "SH", "scode" : "00", "weight" : 1, "position" : 405, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 408, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 409, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "좇", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 413, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "는다는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 416, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "뜻", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 426, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 429, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "토월회", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 436, "begin" : 16, "end" : 17},
		{"id" : 17, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 445, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "바꾸", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 449, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 455, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 458, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 461, "begin" : 22, "end" : 22}
	],
	"word" : [
		{"id" : 0, "text" : "현실(土)을", "type" : "", "begin" : 0, "end" : 4},
		{"id" : 1, "text" : "지키며", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 2, "text" : "이상(月)을", "type" : "", "begin" : 7, "end" : 11},
		{"id" : 3, "text" : "좇는다는", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 4, "text" : "뜻에서", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 5, "text" : "토월회로", "type" : "", "begin" : 16, "end" : 18},
		{"id" : 6, "text" : "바꾸었다.", "type" : "", "begin" : 19, "end" : 22}
	],
	"NE" : [
		{"id" : 0, "text" : "토월회", "type" : "OGG_ART", "begin" : 16, "end" : 17, "weight" : 0.372181, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "현실(土)을", "head" : 1, "label" : "NP_OBJ", "mod" : [], "weight" : 0.800985 },
		{"id" : 1, "text" : "지키며", "head" : 3, "label" : "VP", "mod" : [0], "weight" : 0.623316 },
		{"id" : 2, "text" : "이상(月)을", "head" : 3, "label" : "NP_OBJ", "mod" : [], "weight" : 0.867626 },
		{"id" : 3, "text" : "좇는다는", "head" : 4, "label" : "VP_MOD", "mod" : [1, 2], "weight" : 0.801854 },
		{"id" : 4, "text" : "뜻에서", "head" : 6, "label" : "NP_AJT", "mod" : [3], "weight" : 0.778259 },
		{"id" : 5, "text" : "토월회로", "head" : 6, "label" : "NP_AJT", "mod" : [], "weight" : 0.511319 },
		{"id" : 6, "text" : "바꾸었다.", "head" : -1, "label" : "VP", "mod" : [4, 5], "weight" : 0.085098 }
	],
	"SRL" : [
		{"verb" : "지키", "sense" : 1, "word_id" : 1, "weight" : 0.257669,
			"argument" : [
				{"type" : "ARG1", "word_id" : 0, "text" : "현실(土)을", "weight" : 0.257669 }
			] },
		{"verb" : "좇", "sense" : 1, "word_id" : 3, "weight" : 0.333941,
			"argument" : [
				{"type" : "ARG1", "word_id" : 2, "text" : "이상(月)을", "weight" : 0.333941 }
			] },
		{"verb" : "바꾸", "sense" : 1, "word_id" : 6, "weight" : 0.161865,
			"argument" : [
				{"type" : "ARGM-CAU", "word_id" : 4, "text" : "뜻에서", "weight" : 0.124165 },
				{"type" : "ARG3", "word_id" : 5, "text" : "토월회로", "weight" : 0.199564 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 1, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.823522 },
		{"id" : 1, "verb_wid" : 6, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.576504 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "OG_OTHERS", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 0, "ne_id" : 0, "text" : "﻿토월회(土月會)", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "﻿토월회(土月會)", "weight" : 0.002 },
		{"id" : 1, "sent_id" : 0, "start_eid" : 1, "end_eid" : 9, "ne_id" : -1, "text" : "1923년 5월 일제 강점기 일본에서 결성된 연극 공연 단체이다.", "start_eid_short" : 7, "end_eid_short" : 9, "text_short" : "연극 공연 단체이다.", "weight" : 0.007 }
	] }
 ]
}

