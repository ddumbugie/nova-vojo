{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "임해연(5월 8일 ~ )은 대한민국의 만화가이다.",
	"morp" : [
		{"id" : 0, "lemma" : "임해연", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "(", "type" : "SS", "position" : 9, "weight" : 1 },
		{"id" : 2, "lemma" : "5", "type" : "SN", "position" : 10, "weight" : 1 },
		{"id" : 3, "lemma" : "월", "type" : "NNB", "position" : 11, "weight" : 0.408539 },
		{"id" : 4, "lemma" : "8", "type" : "SN", "position" : 15, "weight" : 1 },
		{"id" : 5, "lemma" : "일", "type" : "NNB", "position" : 16, "weight" : 0.126777 },
		{"id" : 6, "lemma" : "~", "type" : "SO", "position" : 20, "weight" : 1 },
		{"id" : 7, "lemma" : ")", "type" : "SS", "position" : 22, "weight" : 1 },
		{"id" : 8, "lemma" : "은", "type" : "JX", "position" : 23, "weight" : 0.0128817 },
		{"id" : 9, "lemma" : "대한민국", "type" : "NNP", "position" : 27, "weight" : 0.0447775 },
		{"id" : 10, "lemma" : "의", "type" : "JKG", "position" : 39, "weight" : 0.0987295 },
		{"id" : 11, "lemma" : "만화", "type" : "NNG", "position" : 43, "weight" : 0.83848 },
		{"id" : 12, "lemma" : "가", "type" : "XSN", "position" : 49, "weight" : 0.000115417 },
		{"id" : 13, "lemma" : "이", "type" : "VCP", "position" : 52, "weight" : 0.0165001 },
		{"id" : 14, "lemma" : "다", "type" : "EF", "position" : 55, "weight" : 0.353579 },
		{"id" : 15, "lemma" : ".", "type" : "SF", "position" : 58, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "임해연/NNG+(/SS+5/SN+월/NNB", "target" : "임해연(5월", "word_id" : 0, "m_begin" : 0, "m_end" : 3},
		{"id" : 1, "result" : "8/SN+일/NNB", "target" : "8일", "word_id" : 1, "m_begin" : 4, "m_end" : 5},
		{"id" : 2, "result" : "~/SO", "target" : "~", "word_id" : 2, "m_begin" : 6, "m_end" : 6},
		{"id" : 3, "result" : ")/SS+은/JX", "target" : ")은", "word_id" : 3, "m_begin" : 7, "m_end" : 8},
		{"id" : 4, "result" : "대한민국/NNG+의/JKG", "target" : "대한민국의", "word_id" : 4, "m_begin" : 9, "m_end" : 10},
		{"id" : 5, "result" : "만화가/NNG+이/VCP+다/EF+./SF", "target" : "만화가이다.", "word_id" : 5, "m_begin" : 11, "m_end" : 15}
	],
	"WSD" : [
		{"id" : 0, "text" : "임해연", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "5", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "월", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 11, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "8", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 15, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "일", "type" : "NNB", "scode" : "07", "weight" : 1, "position" : 16, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "~", "type" : "SO", "scode" : "00", "weight" : 1, "position" : 20, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 22, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 23, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "대한민국", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 27, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 39, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "만화가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 43, "begin" : 11, "end" : 12},
		{"id" : 12, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 52, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 55, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 58, "begin" : 15, "end" : 15}
	],
	"word" : [
		{"id" : 0, "text" : "임해연(5월", "type" : "", "begin" : 0, "end" : 3},
		{"id" : 1, "text" : "8일", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 2, "text" : "~", "type" : "", "begin" : 6, "end" : 6},
		{"id" : 3, "text" : ")은", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 4, "text" : "대한민국의", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 5, "text" : "만화가이다.", "type" : "", "begin" : 11, "end" : 15}
	],
	"NE" : [
		{"id" : 0, "text" : "임해연", "type" : "PS_NAME", "begin" : 0, "end" : 0, "weight" : 0.279416, "common_noun" : 0},
		{"id" : 1, "text" : "5월 8일 ~", "type" : "DT_OTHERS", "begin" : 2, "end" : 6, "weight" : 0.595252, "common_noun" : 0},
		{"id" : 2, "text" : "대한민국", "type" : "LCP_COUNTRY", "begin" : 9, "end" : 9, "weight" : 0.18423, "common_noun" : 0},
		{"id" : 3, "text" : "만화가", "type" : "CV_OCCUPATION", "begin" : 11, "end" : 12, "weight" : 0.285657, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "임해연(5월", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.324981 },
		{"id" : 1, "text" : "8일", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.435159 },
		{"id" : 2, "text" : "~", "head" : 3, "label" : "X", "mod" : [0, 1], "weight" : 0.823191 },
		{"id" : 3, "text" : ")은", "head" : 5, "label" : "NP_SBJ", "mod" : [2], "weight" : 0.448183 },
		{"id" : 4, "text" : "대한민국의", "head" : 5, "label" : "NP_MOD", "mod" : [], "weight" : 0.431135 },
		{"id" : 5, "text" : "만화가이다.", "head" : -1, "label" : "VNP", "mod" : [3, 4], "weight" : 0.0130586 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "11회 파티 공모전 동상 수상으로 데뷔하였다.",
	"morp" : [
		{"id" : 0, "lemma" : "11", "type" : "SN", "position" : 59, "weight" : 1 },
		{"id" : 1, "lemma" : "회", "type" : "NNB", "position" : 61, "weight" : 0.258682 },
		{"id" : 2, "lemma" : "파티", "type" : "NNG", "position" : 65, "weight" : 0.110228 },
		{"id" : 3, "lemma" : "공모", "type" : "NNG", "position" : 72, "weight" : 0.9 },
		{"id" : 4, "lemma" : "전", "type" : "XSN", "position" : 78, "weight" : 0.00343717 },
		{"id" : 5, "lemma" : "동상", "type" : "NNG", "position" : 82, "weight" : 0.9 },
		{"id" : 6, "lemma" : "수상", "type" : "NNG", "position" : 89, "weight" : 0.9 },
		{"id" : 7, "lemma" : "으로", "type" : "JKB", "position" : 95, "weight" : 0.153406 },
		{"id" : 8, "lemma" : "데뷔", "type" : "NNG", "position" : 102, "weight" : 0.9 },
		{"id" : 9, "lemma" : "하", "type" : "XSV", "position" : 108, "weight" : 0.0001 },
		{"id" : 10, "lemma" : "었", "type" : "EP", "position" : 111, "weight" : 0.9 },
		{"id" : 11, "lemma" : "다", "type" : "EF", "position" : 114, "weight" : 0.640954 },
		{"id" : 12, "lemma" : ".", "type" : "SF", "position" : 117, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "11/SN+회/NNB", "target" : "11회", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "파티/NNG", "target" : "파티", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "공모전/NNG", "target" : "공모전", "word_id" : 2, "m_begin" : 3, "m_end" : 4},
		{"id" : 3, "result" : "동상/NNG", "target" : "동상", "word_id" : 3, "m_begin" : 5, "m_end" : 5},
		{"id" : 4, "result" : "수상/NNG+으로/JKB", "target" : "수상으로", "word_id" : 4, "m_begin" : 6, "m_end" : 7},
		{"id" : 5, "result" : "데뷔하/VV+었/EP+다/EF+./SF", "target" : "데뷔하였다.", "word_id" : 5, "m_begin" : 8, "m_end" : 12}
	],
	"WSD" : [
		{"id" : 0, "text" : "11", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 59, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "회", "type" : "NNB", "scode" : "08", "weight" : 1, "position" : 61, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "파티", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 65, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "공모전", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 72, "begin" : 3, "end" : 4},
		{"id" : 4, "text" : "동상", "type" : "NNG", "scode" : "09", "weight" : 1, "position" : 82, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "수상", "type" : "NNG", "scode" : "07", "weight" : 1, "position" : 89, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 95, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "데뷔하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 102, "begin" : 8, "end" : 9},
		{"id" : 8, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 111, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 114, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 117, "begin" : 12, "end" : 12}
	],
	"word" : [
		{"id" : 0, "text" : "11회", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "파티", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "공모전", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 3, "text" : "동상", "type" : "", "begin" : 5, "end" : 5},
		{"id" : 4, "text" : "수상으로", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 5, "text" : "데뷔하였다.", "type" : "", "begin" : 8, "end" : 12}
	],
	"NE" : [
		{"id" : 0, "text" : "11회", "type" : "QT_COUNT", "begin" : 0, "end" : 1, "weight" : 0.571354, "common_noun" : 0},
		{"id" : 1, "text" : "파티 공모전", "type" : "EV_OTHERS", "begin" : 2, "end" : 4, "weight" : 0.25468, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "11회", "head" : 4, "label" : "NP", "mod" : [], "weight" : 0.610446 },
		{"id" : 1, "text" : "파티", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.477198 },
		{"id" : 2, "text" : "공모전", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.627289 },
		{"id" : 3, "text" : "동상", "head" : 4, "label" : "NP", "mod" : [2], "weight" : 0.884333 },
		{"id" : 4, "text" : "수상으로", "head" : 5, "label" : "NP_AJT", "mod" : [0, 3], "weight" : 0.549962 },
		{"id" : 5, "text" : "데뷔하였다.", "head" : -1, "label" : "VP", "mod" : [4], "weight" : 0.0506695 }
	],
	"SRL" : [
		{"verb" : "데뷔", "sense" : 1, "word_id" : 5, "weight" : 0.0734019,
			"argument" : [
				{"type" : "ARG2", "word_id" : 4, "text" : "수상으로", "weight" : 0.0734019 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 5, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.71393 }
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "데뷔 이래 만화잡지 《파티》에서 주로 작품활동을 하고 있으며 현재 2010년 3월호부터 《호랑님의 식탁》을 연재중이다.",
	"morp" : [
		{"id" : 0, "lemma" : "데뷔", "type" : "NNG", "position" : 118, "weight" : 0.9 },
		{"id" : 1, "lemma" : "이래", "type" : "NNB", "position" : 125, "weight" : 0.0156611 },
		{"id" : 2, "lemma" : "만화", "type" : "NNG", "position" : 132, "weight" : 0.106433 },
		{"id" : 3, "lemma" : "잡지", "type" : "NNG", "position" : 138, "weight" : 0.9 },
		{"id" : 4, "lemma" : "《", "type" : "SS", "position" : 145, "weight" : 1 },
		{"id" : 5, "lemma" : "파티", "type" : "NNP", "position" : 148, "weight" : 0.000237525 },
		{"id" : 6, "lemma" : "》", "type" : "SS", "position" : 154, "weight" : 1 },
		{"id" : 7, "lemma" : "에서", "type" : "JKB", "position" : 157, "weight" : 0.0486066 },
		{"id" : 8, "lemma" : "주로", "type" : "MAG", "position" : 164, "weight" : 0.0557734 },
		{"id" : 9, "lemma" : "작품", "type" : "NNG", "position" : 171, "weight" : 0.261468 },
		{"id" : 10, "lemma" : "활동", "type" : "NNG", "position" : 177, "weight" : 0.9 },
		{"id" : 11, "lemma" : "을", "type" : "JKO", "position" : 183, "weight" : 0.129611 },
		{"id" : 12, "lemma" : "하", "type" : "VV", "position" : 187, "weight" : 0.48169 },
		{"id" : 13, "lemma" : "고", "type" : "EC", "position" : 190, "weight" : 0.416679 },
		{"id" : 14, "lemma" : "있", "type" : "VX", "position" : 194, "weight" : 0.125953 },
		{"id" : 15, "lemma" : "으며", "type" : "EC", "position" : 197, "weight" : 0.265281 },
		{"id" : 16, "lemma" : "현재", "type" : "MAG", "position" : 204, "weight" : 0.0283089 },
		{"id" : 17, "lemma" : "2010", "type" : "SN", "position" : 211, "weight" : 1 },
		{"id" : 18, "lemma" : "년", "type" : "NNB", "position" : 215, "weight" : 0.414343 },
		{"id" : 19, "lemma" : "3", "type" : "SN", "position" : 219, "weight" : 1 },
		{"id" : 20, "lemma" : "월", "type" : "NNB", "position" : 220, "weight" : 0.408539 },
		{"id" : 21, "lemma" : "호", "type" : "NNB", "position" : 223, "weight" : 0.00521451 },
		{"id" : 22, "lemma" : "부터", "type" : "JX", "position" : 226, "weight" : 0.105319 },
		{"id" : 23, "lemma" : "《", "type" : "SS", "position" : 233, "weight" : 1 },
		{"id" : 24, "lemma" : "호랑님", "type" : "NNP", "position" : 236, "weight" : 0.6 },
		{"id" : 25, "lemma" : "의", "type" : "JKG", "position" : 245, "weight" : 0.0987295 },
		{"id" : 26, "lemma" : "식탁", "type" : "NNG", "position" : 249, "weight" : 0.9 },
		{"id" : 27, "lemma" : "》", "type" : "SS", "position" : 255, "weight" : 1 },
		{"id" : 28, "lemma" : "을", "type" : "JKO", "position" : 258, "weight" : 0.0336085 },
		{"id" : 29, "lemma" : "연재", "type" : "NNG", "position" : 262, "weight" : 0.0975025 },
		{"id" : 30, "lemma" : "중", "type" : "NNB", "position" : 268, "weight" : 0.013531 },
		{"id" : 31, "lemma" : "이", "type" : "VCP", "position" : 271, "weight" : 0.0607843 },
		{"id" : 32, "lemma" : "다", "type" : "EF", "position" : 274, "weight" : 0.353579 },
		{"id" : 33, "lemma" : ".", "type" : "SF", "position" : 277, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "데뷔/NNG", "target" : "데뷔", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "이래/NNB", "target" : "이래", "word_id" : 1, "m_begin" : 1, "m_end" : 1},
		{"id" : 2, "result" : "만화잡지/NNG", "target" : "만화잡지", "word_id" : 2, "m_begin" : 2, "m_end" : 3},
		{"id" : 3, "result" : "《/SS+파티/NNG+》/SS+에서/JKB", "target" : "《파티》에서", "word_id" : 3, "m_begin" : 4, "m_end" : 7},
		{"id" : 4, "result" : "주로/MAG", "target" : "주로", "word_id" : 4, "m_begin" : 8, "m_end" : 8},
		{"id" : 5, "result" : "작품활동/NNG+을/JKO", "target" : "작품활동을", "word_id" : 5, "m_begin" : 9, "m_end" : 11},
		{"id" : 6, "result" : "하/VV+고/EC", "target" : "하고", "word_id" : 6, "m_begin" : 12, "m_end" : 13},
		{"id" : 7, "result" : "있/VX+으며/EC", "target" : "있으며", "word_id" : 7, "m_begin" : 14, "m_end" : 15},
		{"id" : 8, "result" : "현재/MAG", "target" : "현재", "word_id" : 8, "m_begin" : 16, "m_end" : 16},
		{"id" : 9, "result" : "2010/SN+년/NNB", "target" : "2010년", "word_id" : 9, "m_begin" : 17, "m_end" : 18},
		{"id" : 10, "result" : "3/SN+월/NNB+호/NNB+부터/JX", "target" : "3월호부터", "word_id" : 10, "m_begin" : 19, "m_end" : 22},
		{"id" : 11, "result" : "《/SS+호랑님/NNG+의/JKG", "target" : "《호랑님의", "word_id" : 11, "m_begin" : 23, "m_end" : 25},
		{"id" : 12, "result" : "식탁/NNG+》/SS+을/JKO", "target" : "식탁》을", "word_id" : 12, "m_begin" : 26, "m_end" : 28},
		{"id" : 13, "result" : "연재/NNG+중/NNB+이/VCP+다/EF+./SF", "target" : "연재중이다.", "word_id" : 13, "m_begin" : 29, "m_end" : 33}
	],
	"WSD" : [
		{"id" : 0, "text" : "데뷔", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 118, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "이래", "type" : "NNB", "scode" : "03", "weight" : 1, "position" : 125, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "만화", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 132, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "잡지", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 138, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "《", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 145, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "파티", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 148, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "》", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 154, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 157, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "주로", "type" : "MAG", "scode" : "01", "weight" : 1, "position" : 164, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "작품", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 171, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "활동", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 177, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 183, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 187, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 190, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "있", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 194, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "으며", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 197, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "현재", "type" : "MAG", "scode" : "02", "weight" : 1, "position" : 204, "begin" : 16, "end" : 16},
		{"id" : 17, "text" : "2010", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 211, "begin" : 17, "end" : 17},
		{"id" : 18, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 215, "begin" : 18, "end" : 18},
		{"id" : 19, "text" : "3", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 219, "begin" : 19, "end" : 19},
		{"id" : 20, "text" : "월", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 220, "begin" : 20, "end" : 20},
		{"id" : 21, "text" : "호", "type" : "NNB", "scode" : "14", "weight" : 1, "position" : 223, "begin" : 21, "end" : 21},
		{"id" : 22, "text" : "부터", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 226, "begin" : 22, "end" : 22},
		{"id" : 23, "text" : "《", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 233, "begin" : 23, "end" : 23},
		{"id" : 24, "text" : "호랑님", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 236, "begin" : 24, "end" : 24},
		{"id" : 25, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 245, "begin" : 25, "end" : 25},
		{"id" : 26, "text" : "식탁", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 249, "begin" : 26, "end" : 26},
		{"id" : 27, "text" : "》", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 255, "begin" : 27, "end" : 27},
		{"id" : 28, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 258, "begin" : 28, "end" : 28},
		{"id" : 29, "text" : "연재", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 262, "begin" : 29, "end" : 29},
		{"id" : 30, "text" : "중", "type" : "NNB", "scode" : "04", "weight" : 1, "position" : 268, "begin" : 30, "end" : 30},
		{"id" : 31, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 271, "begin" : 31, "end" : 31},
		{"id" : 32, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 274, "begin" : 32, "end" : 32},
		{"id" : 33, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 277, "begin" : 33, "end" : 33}
	],
	"word" : [
		{"id" : 0, "text" : "데뷔", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "이래", "type" : "", "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "만화잡지", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 3, "text" : "《파티》에서", "type" : "", "begin" : 4, "end" : 7},
		{"id" : 4, "text" : "주로", "type" : "", "begin" : 8, "end" : 8},
		{"id" : 5, "text" : "작품활동을", "type" : "", "begin" : 9, "end" : 11},
		{"id" : 6, "text" : "하고", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 7, "text" : "있으며", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 8, "text" : "현재", "type" : "", "begin" : 16, "end" : 16},
		{"id" : 9, "text" : "2010년", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 10, "text" : "3월호부터", "type" : "", "begin" : 19, "end" : 22},
		{"id" : 11, "text" : "《호랑님의", "type" : "", "begin" : 23, "end" : 25},
		{"id" : 12, "text" : "식탁》을", "type" : "", "begin" : 26, "end" : 28},
		{"id" : 13, "text" : "연재중이다.", "type" : "", "begin" : 29, "end" : 33}
	],
	"NE" : [
		{"id" : 0, "text" : "만화", "type" : "FD_ART", "begin" : 2, "end" : 2, "weight" : 0.363404, "common_noun" : 0},
		{"id" : 1, "text" : "파티", "type" : "AF_WORKS", "begin" : 5, "end" : 5, "weight" : 0.592977, "common_noun" : 0},
		{"id" : 2, "text" : "2010년 3월", "type" : "DT_OTHERS", "begin" : 17, "end" : 20, "weight" : 0.820752, "common_noun" : 0},
		{"id" : 3, "text" : "호랑님의 식탁", "type" : "AFW_DOCUMENT", "begin" : 24, "end" : 26, "weight" : 0.885006, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "데뷔", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.914143 },
		{"id" : 1, "text" : "이래", "head" : 3, "label" : "NP", "mod" : [0], "weight" : 0.567309 },
		{"id" : 2, "text" : "만화잡지", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.887494 },
		{"id" : 3, "text" : "《파티》에서", "head" : 6, "label" : "NP_AJT", "mod" : [1, 2], "weight" : 0.978143 },
		{"id" : 4, "text" : "주로", "head" : 6, "label" : "AP", "mod" : [], "weight" : 0.672583 },
		{"id" : 5, "text" : "작품활동을", "head" : 6, "label" : "NP_OBJ", "mod" : [], "weight" : 0.711565 },
		{"id" : 6, "text" : "하고", "head" : 7, "label" : "VP", "mod" : [3, 4, 5], "weight" : 0.716632 },
		{"id" : 7, "text" : "있으며", "head" : 13, "label" : "VP", "mod" : [6], "weight" : 0.686803 },
		{"id" : 8, "text" : "현재", "head" : 13, "label" : "AP", "mod" : [], "weight" : 0.622425 },
		{"id" : 9, "text" : "2010년", "head" : 10, "label" : "NP", "mod" : [], "weight" : 0.801568 },
		{"id" : 10, "text" : "3월호부터", "head" : 13, "label" : "NP_AJT", "mod" : [9], "weight" : 0.584493 },
		{"id" : 11, "text" : "《호랑님의", "head" : 12, "label" : "NP_MOD", "mod" : [], "weight" : 0.813446 },
		{"id" : 12, "text" : "식탁》을", "head" : 13, "label" : "NP_OBJ", "mod" : [11], "weight" : 0.415484 },
		{"id" : 13, "text" : "연재중이다.", "head" : -1, "label" : "VNP", "mod" : [7, 8, 10, 12], "weight" : 0.00529831 }
	],
	"SRL" : [
		{"verb" : "하", "sense" : 1, "word_id" : 6, "weight" : 0.336485,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 3, "text" : "《파티》에서", "weight" : 0.195661 },
				{"type" : "ARGM-ADV", "word_id" : 4, "text" : "주로", "weight" : 0.193776 },
				{"type" : "ARG1", "word_id" : 5, "text" : "작품활동을", "weight" : 0.378831 },
				{"type" : "AUX", "word_id" : 7, "text" : "있으며", "weight" : 0.577672 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 6, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.579778 },
		{"id" : 1, "verb_wid" : 13, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.475499 }
	]
	}
 ],
 "entity" : [
 ]
}

