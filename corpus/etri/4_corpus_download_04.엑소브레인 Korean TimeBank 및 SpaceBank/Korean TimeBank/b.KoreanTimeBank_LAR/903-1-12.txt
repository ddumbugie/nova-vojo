{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "이것은 보통 텔레비전 리모컨에서 이용하는 전자기파의 한 종류다.  ",
	"morp" : [
		{"id" : 0, "lemma" : "이것", "type" : "NP", "position" : 0, "weight" : 0.0733657 },
		{"id" : 1, "lemma" : "은", "type" : "JX", "position" : 6, "weight" : 0.191811 },
		{"id" : 2, "lemma" : "보통", "type" : "NNG", "position" : 10, "weight" : 0.219649 },
		{"id" : 3, "lemma" : "텔레비전", "type" : "NNG", "position" : 17, "weight" : 0.184743 },
		{"id" : 4, "lemma" : "리모컨", "type" : "NNG", "position" : 30, "weight" : 0.9 },
		{"id" : 5, "lemma" : "에서", "type" : "JKB", "position" : 39, "weight" : 0.153407 },
		{"id" : 6, "lemma" : "이용", "type" : "NNG", "position" : 46, "weight" : 0.207679 },
		{"id" : 7, "lemma" : "하", "type" : "XSV", "position" : 52, "weight" : 0.0001 },
		{"id" : 8, "lemma" : "는", "type" : "ETM", "position" : 55, "weight" : 0.238503 },
		{"id" : 9, "lemma" : "전자기", "type" : "NNG", "position" : 59, "weight" : 0.9 },
		{"id" : 10, "lemma" : "파", "type" : "XSN", "position" : 68, "weight" : 0.000623483 },
		{"id" : 11, "lemma" : "의", "type" : "JKG", "position" : 71, "weight" : 0.120732 },
		{"id" : 12, "lemma" : "한", "type" : "MM", "position" : 75, "weight" : 0.0231003 },
		{"id" : 13, "lemma" : "종류", "type" : "NNG", "position" : 79, "weight" : 0.9 },
		{"id" : 14, "lemma" : "이", "type" : "VCP", "position" : 82, "weight" : 0.0177525 },
		{"id" : 15, "lemma" : "다", "type" : "EF", "position" : 85, "weight" : 0.353579 },
		{"id" : 16, "lemma" : ".", "type" : "SF", "position" : 88, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이것/NP+은/JX", "target" : "이것은", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "보통/NNG", "target" : "보통", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "텔레비전/NNG", "target" : "텔레비전", "word_id" : 2, "m_begin" : 3, "m_end" : 3},
		{"id" : 3, "result" : "리모컨/NNG+에서/JKB", "target" : "리모컨에서", "word_id" : 3, "m_begin" : 4, "m_end" : 5},
		{"id" : 4, "result" : "이용하/VV+는/ETM", "target" : "이용하는", "word_id" : 4, "m_begin" : 6, "m_end" : 8},
		{"id" : 5, "result" : "전자기파/NNG+의/JKG", "target" : "전자기파의", "word_id" : 5, "m_begin" : 9, "m_end" : 11},
		{"id" : 6, "result" : "한/MM", "target" : "한", "word_id" : 6, "m_begin" : 12, "m_end" : 12},
		{"id" : 7, "result" : "종류/NNG+이/VCP+다/EF+./SF", "target" : "종류다.", "word_id" : 7, "m_begin" : 13, "m_end" : 16}
	],
	"WSD" : [
		{"id" : 0, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 6, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "보통", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "텔레비전", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 17, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "리모컨", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 30, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 39, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "이용하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 46, "begin" : 6, "end" : 7},
		{"id" : 7, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 55, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "전자기파", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 59, "begin" : 9, "end" : 10},
		{"id" : 9, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 71, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "한", "type" : "MM", "scode" : "01", "weight" : 1, "position" : 75, "begin" : 12, "end" : 12},
		{"id" : 11, "text" : "종류", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 79, "begin" : 13, "end" : 13},
		{"id" : 12, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 82, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 85, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 88, "begin" : 16, "end" : 16}
	],
	"word" : [
		{"id" : 0, "text" : "이것은", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "보통", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "텔레비전", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "리모컨에서", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 4, "text" : "이용하는", "type" : "", "begin" : 6, "end" : 8},
		{"id" : 5, "text" : "전자기파의", "type" : "", "begin" : 9, "end" : 11},
		{"id" : 6, "text" : "한", "type" : "", "begin" : 12, "end" : 12},
		{"id" : 7, "text" : "종류다.", "type" : "", "begin" : 13, "end" : 16}
	],
	"NE" : [
		{"id" : 0, "text" : "텔레비전", "type" : "TMI_HW", "begin" : 3, "end" : 3, "weight" : 0.21532, "common_noun" : 0},
		{"id" : 1, "text" : "리모컨", "type" : "TMI_HW", "begin" : 4, "end" : 4, "weight" : 0.358264, "common_noun" : 0},
		{"id" : 2, "text" : "한 종류", "type" : "QT_COUNT", "begin" : 12, "end" : 13, "weight" : 0.350116, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이것은", "head" : 7, "label" : "NP_SBJ", "mod" : [], "weight" : 0.899059 },
		{"id" : 1, "text" : "보통", "head" : 7, "label" : "AP", "mod" : [], "weight" : 0.527468 },
		{"id" : 2, "text" : "텔레비전", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.72958 },
		{"id" : 3, "text" : "리모컨에서", "head" : 4, "label" : "NP_AJT", "mod" : [2], "weight" : 0.766371 },
		{"id" : 4, "text" : "이용하는", "head" : 5, "label" : "VP_MOD", "mod" : [3], "weight" : 0.717589 },
		{"id" : 5, "text" : "전자기파의", "head" : 7, "label" : "NP_MOD", "mod" : [4], "weight" : 0.468269 },
		{"id" : 6, "text" : "한", "head" : 7, "label" : "DP", "mod" : [], "weight" : 0.44326 },
		{"id" : 7, "text" : "종류다.", "head" : -1, "label" : "VNP", "mod" : [0, 1, 5, 6], "weight" : 0.0251035 }
	],
	"SRL" : [
		{"verb" : "이용", "sense" : 1, "word_id" : 4, "weight" : 0.188273,
			"argument" : [
				{"type" : "ARG2", "word_id" : 3, "text" : "리모컨에서", "weight" : 0.169945 },
				{"type" : "ARG1", "word_id" : 5, "text" : "전자기파의", "weight" : 0.206601 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "눈으로는 볼 수 없고 일반적으로 공기 분자에 산란되기 어려워 대기를 잘 통과한다.  ",
	"morp" : [
		{"id" : 0, "lemma" : "눈", "type" : "NNG", "position" : 91, "weight" : 0.437905 },
		{"id" : 1, "lemma" : "으로", "type" : "JKB", "position" : 94, "weight" : 0.153406 },
		{"id" : 2, "lemma" : "는", "type" : "JX", "position" : 100, "weight" : 0.0387928 },
		{"id" : 3, "lemma" : "보", "type" : "VV", "position" : 104, "weight" : 0.0898222 },
		{"id" : 4, "lemma" : "ㄹ", "type" : "ETM", "position" : 104, "weight" : 0.300746 },
		{"id" : 5, "lemma" : "수", "type" : "NNB", "position" : 108, "weight" : 0.215617 },
		{"id" : 6, "lemma" : "없", "type" : "VA", "position" : 112, "weight" : 0.101269 },
		{"id" : 7, "lemma" : "고", "type" : "EC", "position" : 115, "weight" : 0.310376 },
		{"id" : 8, "lemma" : "일반", "type" : "NNG", "position" : 119, "weight" : 0.9 },
		{"id" : 9, "lemma" : "적", "type" : "XSN", "position" : 125, "weight" : 0.0168756 },
		{"id" : 10, "lemma" : "으로", "type" : "JKB", "position" : 128, "weight" : 0.121717 },
		{"id" : 11, "lemma" : "공기", "type" : "NNG", "position" : 135, "weight" : 0.207969 },
		{"id" : 12, "lemma" : "분자", "type" : "NNG", "position" : 142, "weight" : 0.184663 },
		{"id" : 13, "lemma" : "에", "type" : "JKB", "position" : 148, "weight" : 0.153364 },
		{"id" : 14, "lemma" : "산란", "type" : "NNG", "position" : 152, "weight" : 0.9 },
		{"id" : 15, "lemma" : "되", "type" : "XSV", "position" : 158, "weight" : 0.000224177 },
		{"id" : 16, "lemma" : "기", "type" : "ETN", "position" : 161, "weight" : 0.0556099 },
		{"id" : 17, "lemma" : "어렵", "type" : "VA", "position" : 165, "weight" : 0.0417172 },
		{"id" : 18, "lemma" : "어", "type" : "EC", "position" : 168, "weight" : 0.311591 },
		{"id" : 19, "lemma" : "대기", "type" : "NNG", "position" : 175, "weight" : 0.218609 },
		{"id" : 20, "lemma" : "를", "type" : "JKO", "position" : 181, "weight" : 0.137686 },
		{"id" : 21, "lemma" : "잘", "type" : "MAG", "position" : 185, "weight" : 0.0646785 },
		{"id" : 22, "lemma" : "통과", "type" : "NNG", "position" : 189, "weight" : 0.9 },
		{"id" : 23, "lemma" : "하", "type" : "XSV", "position" : 195, "weight" : 0.0001 },
		{"id" : 24, "lemma" : "ㄴ다", "type" : "EF", "position" : 195, "weight" : 0.0449045 },
		{"id" : 25, "lemma" : ".", "type" : "SF", "position" : 201, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "눈/NNG+으로/JKB+는/JX", "target" : "눈으로는", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "보/VV+ㄹ/ETM", "target" : "볼", "word_id" : 1, "m_begin" : 3, "m_end" : 4},
		{"id" : 2, "result" : "수/NNB", "target" : "수", "word_id" : 2, "m_begin" : 5, "m_end" : 5},
		{"id" : 3, "result" : "없/VA+고/EC", "target" : "없고", "word_id" : 3, "m_begin" : 6, "m_end" : 7},
		{"id" : 4, "result" : "일반적/NNG+으로/JKB", "target" : "일반적으로", "word_id" : 4, "m_begin" : 8, "m_end" : 10},
		{"id" : 5, "result" : "공기/NNG", "target" : "공기", "word_id" : 5, "m_begin" : 11, "m_end" : 11},
		{"id" : 6, "result" : "분자/NNG+에/JKB", "target" : "분자에", "word_id" : 6, "m_begin" : 12, "m_end" : 13},
		{"id" : 7, "result" : "산란되/VV+기/ETN", "target" : "산란되기", "word_id" : 7, "m_begin" : 14, "m_end" : 16},
		{"id" : 8, "result" : "어렵/VA+어/EC", "target" : "어려워", "word_id" : 8, "m_begin" : 17, "m_end" : 18},
		{"id" : 9, "result" : "대기/NNG+를/JKO", "target" : "대기를", "word_id" : 9, "m_begin" : 19, "m_end" : 20},
		{"id" : 10, "result" : "잘/MAG", "target" : "잘", "word_id" : 10, "m_begin" : 21, "m_end" : 21},
		{"id" : 11, "result" : "통과하/VV+ㄴ다/EF+./SF", "target" : "통과한다.", "word_id" : 11, "m_begin" : 22, "m_end" : 25}
	],
	"WSD" : [
		{"id" : 0, "text" : "눈", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 91, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 94, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 100, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "보", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 104, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "ㄹ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 104, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "수", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 108, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "없", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 112, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 115, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "일반적", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 119, "begin" : 8, "end" : 9},
		{"id" : 9, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 128, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "공기", "type" : "NNG", "scode" : "06", "weight" : 1, "position" : 135, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "분자", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 142, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 148, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "산란", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 152, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "되", "type" : "XSV", "scode" : "00", "weight" : 1, "position" : 158, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "기", "type" : "ETN", "scode" : "00", "weight" : 1, "position" : 161, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "어렵", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 165, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 168, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "대기", "type" : "NNG", "scode" : "07", "weight" : 1, "position" : 175, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 181, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "잘", "type" : "MAG", "scode" : "02", "weight" : 1, "position" : 185, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : "통과하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 189, "begin" : 22, "end" : 23},
		{"id" : 22, "text" : "ㄴ다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 195, "begin" : 24, "end" : 24},
		{"id" : 23, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 201, "begin" : 25, "end" : 25}
	],
	"word" : [
		{"id" : 0, "text" : "눈으로는", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "볼", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 2, "text" : "수", "type" : "", "begin" : 5, "end" : 5},
		{"id" : 3, "text" : "없고", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 4, "text" : "일반적으로", "type" : "", "begin" : 8, "end" : 10},
		{"id" : 5, "text" : "공기", "type" : "", "begin" : 11, "end" : 11},
		{"id" : 6, "text" : "분자에", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 7, "text" : "산란되기", "type" : "", "begin" : 14, "end" : 16},
		{"id" : 8, "text" : "어려워", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 9, "text" : "대기를", "type" : "", "begin" : 19, "end" : 20},
		{"id" : 10, "text" : "잘", "type" : "", "begin" : 21, "end" : 21},
		{"id" : 11, "text" : "통과한다.", "type" : "", "begin" : 22, "end" : 25}
	],
	"NE" : [
		{"id" : 0, "text" : "눈", "type" : "AM_PART", "begin" : 0, "end" : 0, "weight" : 0.2678, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "눈으로는", "head" : 1, "label" : "NP_AJT", "mod" : [], "weight" : 0.935998 },
		{"id" : 1, "text" : "볼", "head" : 2, "label" : "VP_MOD", "mod" : [0], "weight" : 0.962547 },
		{"id" : 2, "text" : "수", "head" : 3, "label" : "NP_SBJ", "mod" : [1], "weight" : 0.886444 },
		{"id" : 3, "text" : "없고", "head" : 8, "label" : "VP", "mod" : [2], "weight" : 0.657334 },
		{"id" : 4, "text" : "일반적으로", "head" : 7, "label" : "NP_AJT", "mod" : [], "weight" : 0.755103 },
		{"id" : 5, "text" : "공기", "head" : 6, "label" : "NP", "mod" : [], "weight" : 0.884073 },
		{"id" : 6, "text" : "분자에", "head" : 7, "label" : "NP_AJT", "mod" : [5], "weight" : 0.957504 },
		{"id" : 7, "text" : "산란되기", "head" : 8, "label" : "VP_SBJ", "mod" : [4, 6], "weight" : 0.745783 },
		{"id" : 8, "text" : "어려워", "head" : 11, "label" : "VP", "mod" : [3, 7], "weight" : 0.579176 },
		{"id" : 9, "text" : "대기를", "head" : 11, "label" : "NP_OBJ", "mod" : [], "weight" : 0.553593 },
		{"id" : 10, "text" : "잘", "head" : 11, "label" : "AP", "mod" : [], "weight" : 0.427719 },
		{"id" : 11, "text" : "통과한다.", "head" : -1, "label" : "VP", "mod" : [8, 9, 10], "weight" : 0.0203571 }
	],
	"SRL" : [
		{"verb" : "보", "sense" : 1, "word_id" : 1, "weight" : 0.292479,
			"argument" : [
				{"type" : "ARGM-INS", "word_id" : 0, "text" : "눈으로는", "weight" : 0.151665 },
				{"type" : "ARGM-NEG", "word_id" : 3, "text" : "없고", "weight" : 0.433294 }
			] },
		{"verb" : "산란", "sense" : 1, "word_id" : 7, "weight" : 0.135613,
			"argument" : [
				{"type" : "ARG3", "word_id" : 6, "text" : "분자에", "weight" : 0.135613 }
			] },
		{"verb" : "어렵", "sense" : 1, "word_id" : 8, "weight" : 0.242866,
			"argument" : [
				{"type" : "ARG1", "word_id" : 7, "text" : "산란되기", "weight" : 0.242866 }
			] },
		{"verb" : "통과", "sense" : 1, "word_id" : 11, "weight" : 0.191088,
			"argument" : [
				{"type" : "ARGM-CAU", "word_id" : 8, "text" : "어려워", "weight" : 0.221495 },
				{"type" : "ARG1", "word_id" : 9, "text" : "대기를", "weight" : 0.166152 },
				{"type" : "ARGM-ADV", "word_id" : 10, "text" : "잘", "weight" : 0.185616 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 1, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.750074 },
		{"id" : 1, "verb_wid" : 7, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.641765 },
		{"id" : 2, "verb_wid" : 11, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.380464 }
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "1800년경 영국의 천문학자 허셜이 발견한 이것은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "1800", "type" : "SN", "position" : 204, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 208, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "경", "type" : "XSN", "position" : 211, "weight" : 0.0160459 },
		{"id" : 3, "lemma" : "영국", "type" : "NNP", "position" : 215, "weight" : 0.9 },
		{"id" : 4, "lemma" : "의", "type" : "JKG", "position" : 221, "weight" : 0.0987295 },
		{"id" : 5, "lemma" : "천문학", "type" : "NNG", "position" : 225, "weight" : 0.9 },
		{"id" : 6, "lemma" : "자", "type" : "XSN", "position" : 234, "weight" : 0.00398741 },
		{"id" : 7, "lemma" : "허셜", "type" : "NNP", "position" : 238, "weight" : 0.25 },
		{"id" : 8, "lemma" : "이", "type" : "JKS", "position" : 244, "weight" : 0.0234517 },
		{"id" : 9, "lemma" : "발견", "type" : "NNG", "position" : 248, "weight" : 0.9 },
		{"id" : 10, "lemma" : "하", "type" : "XSV", "position" : 254, "weight" : 0.0001 },
		{"id" : 11, "lemma" : "ㄴ", "type" : "ETM", "position" : 254, "weight" : 0.392321 },
		{"id" : 12, "lemma" : "이것", "type" : "NP", "position" : 258, "weight" : 0.0102992 },
		{"id" : 13, "lemma" : "은", "type" : "JX", "position" : 264, "weight" : 0.191811 },
		{"id" : 14, "lemma" : "무엇", "type" : "NP", "position" : 268, "weight" : 0.9 },
		{"id" : 15, "lemma" : "이", "type" : "VCP", "position" : 274, "weight" : 0.0175768 },
		{"id" : 16, "lemma" : "ㄹ까", "type" : "EF", "position" : 274, "weight" : 0.258243 },
		{"id" : 17, "lemma" : "?", "type" : "SF", "position" : 280, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "1800/SN+년경/NNB", "target" : "1800년경", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "영국/NNG+의/JKG", "target" : "영국의", "word_id" : 1, "m_begin" : 3, "m_end" : 4},
		{"id" : 2, "result" : "천문학자/NNG", "target" : "천문학자", "word_id" : 2, "m_begin" : 5, "m_end" : 6},
		{"id" : 3, "result" : "허셜/NNG+이/JKS", "target" : "허셜이", "word_id" : 3, "m_begin" : 7, "m_end" : 8},
		{"id" : 4, "result" : "발견하/VV+ㄴ/ETM", "target" : "발견한", "word_id" : 4, "m_begin" : 9, "m_end" : 11},
		{"id" : 5, "result" : "이것/NP+은/JX", "target" : "이것은", "word_id" : 5, "m_begin" : 12, "m_end" : 13},
		{"id" : 6, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 6, "m_begin" : 14, "m_end" : 17}
	],
	"WSD" : [
		{"id" : 0, "text" : "1800", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 204, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 208, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "경", "type" : "XSN", "scode" : "25", "weight" : 1, "position" : 211, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "영국", "type" : "NNP", "scode" : "01", "weight" : 1, "position" : 215, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 221, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "천문학자", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 225, "begin" : 5, "end" : 6},
		{"id" : 6, "text" : "허셜", "type" : "NNP", "scode" : "01", "weight" : 1, "position" : 238, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 244, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "발견하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 248, "begin" : 9, "end" : 10},
		{"id" : 9, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 254, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 258, "begin" : 12, "end" : 12},
		{"id" : 11, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 264, "begin" : 13, "end" : 13},
		{"id" : 12, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 268, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 274, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 274, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 280, "begin" : 17, "end" : 17}
	],
	"word" : [
		{"id" : 0, "text" : "1800년경", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "영국의", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 2, "text" : "천문학자", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 3, "text" : "허셜이", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 4, "text" : "발견한", "type" : "", "begin" : 9, "end" : 11},
		{"id" : 5, "text" : "이것은", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 6, "text" : "무엇일까?", "type" : "", "begin" : 14, "end" : 17}
	],
	"NE" : [
		{"id" : 0, "text" : "1800년경", "type" : "DT_YEAR", "begin" : 0, "end" : 2, "weight" : 0.529663, "common_noun" : 0},
		{"id" : 1, "text" : "영국", "type" : "LCP_COUNTRY", "begin" : 3, "end" : 3, "weight" : 0.60806, "common_noun" : 0},
		{"id" : 2, "text" : "천문학자", "type" : "CV_OCCUPATION", "begin" : 5, "end" : 6, "weight" : 0.445463, "common_noun" : 0},
		{"id" : 3, "text" : "허셜", "type" : "PS_NAME", "begin" : 7, "end" : 7, "weight" : 0.307048, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "1800년경", "head" : 4, "label" : "NP_AJT", "mod" : [], "weight" : 0.866665 },
		{"id" : 1, "text" : "영국의", "head" : 2, "label" : "NP_MOD", "mod" : [], "weight" : 0.720939 },
		{"id" : 2, "text" : "천문학자", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.921736 },
		{"id" : 3, "text" : "허셜이", "head" : 4, "label" : "NP_SBJ", "mod" : [2], "weight" : 0.62546 },
		{"id" : 4, "text" : "발견한", "head" : 5, "label" : "VP_MOD", "mod" : [0, 3], "weight" : 0.858834 },
		{"id" : 5, "text" : "이것은", "head" : 6, "label" : "NP_SBJ", "mod" : [4], "weight" : 0.638849 },
		{"id" : 6, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [5], "weight" : 0.137508 }
	],
	"SRL" : [
		{"verb" : "발견", "sense" : 1, "word_id" : 4, "weight" : 0.370896,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 0, "text" : "1800년경", "weight" : 0.375945 },
				{"type" : "ARG0", "word_id" : 3, "text" : "허셜이", "weight" : 0.493284 },
				{"type" : "ARG1", "word_id" : 5, "text" : "이것은", "weight" : 0.243459 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "TMI_HW", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 3, "sent_id" : 0, "start_eid" : 2, "end_eid" : 3, "ne_id" : 1, "text" : "텔레비전 리모컨", "start_eid_short" : 2, "end_eid_short" : 3, "text_short" : "텔레비전 리모컨", "weight" : 0.002 },
		{"id" : 4, "sent_id" : 0, "start_eid" : 2, "end_eid" : 2, "ne_id" : 0, "text" : "텔레비전", "start_eid_short" : 2, "end_eid_short" : 2, "text_short" : "텔레비전", "weight" : 0.003 }
	] },
	{"id" : 1, "type" : "CV_OCCUPATION", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "영국", 
	 "mention" : [
		{"id" : 13, "sent_id" : 2, "start_eid" : 1, "end_eid" : 3, "ne_id" : 3, "text" : "영국의 천문학자 허셜", "start_eid_short" : 1, "end_eid_short" : 3, "text_short" : "영국의 천문학자 허셜", "weight" : 0.01 },
		{"id" : 14, "sent_id" : 2, "start_eid" : 1, "end_eid" : 2, "ne_id" : 2, "text" : "영국의 천문학자", "start_eid_short" : 1, "end_eid_short" : 2, "text_short" : "영국의 천문학자", "weight" : 0.015 },
		{"id" : 16, "sent_id" : 2, "start_eid" : 5, "end_eid" : 6, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 6, "end_eid_short" : 6, "text_short" : "무엇이ㄹ까?", "weight" : 0.006 }
	] },
	{"id" : 2, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 0, "ne_id" : -1, "text" : "이것", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "이것", "weight" : 0.03 },
		{"id" : 17, "sent_id" : 2, "start_eid" : 5, "end_eid" : 5, "ne_id" : -1, "text" : "이것", "start_eid_short" : 5, "end_eid_short" : 5, "text_short" : "이것", "weight" : 0.018 }
	] }
 ]
}

