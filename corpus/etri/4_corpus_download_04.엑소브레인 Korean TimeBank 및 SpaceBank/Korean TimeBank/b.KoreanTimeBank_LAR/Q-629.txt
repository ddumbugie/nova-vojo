{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿이곳은 충북 단양의 8경 중 하나로 알려져 있는 도담삼봉이다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "이곳", "type" : "NP", "position" : 3, "weight" : 0.9 },
		{"id" : 2, "lemma" : "은", "type" : "JX", "position" : 9, "weight" : 0.191811 },
		{"id" : 3, "lemma" : "충북", "type" : "NNP", "position" : 13, "weight" : 0.0448113 },
		{"id" : 4, "lemma" : "단양", "type" : "NNP", "position" : 20, "weight" : 0.0795478 },
		{"id" : 5, "lemma" : "의", "type" : "JKG", "position" : 26, "weight" : 0.0987295 },
		{"id" : 6, "lemma" : "8", "type" : "SN", "position" : 30, "weight" : 1 },
		{"id" : 7, "lemma" : "경", "type" : "NNB", "position" : 31, "weight" : 0.000268967 },
		{"id" : 8, "lemma" : "중", "type" : "NNB", "position" : 35, "weight" : 0.00859691 },
		{"id" : 9, "lemma" : "하나", "type" : "NNG", "position" : 39, "weight" : 0.0787416 },
		{"id" : 10, "lemma" : "로", "type" : "JKB", "position" : 45, "weight" : 0.153229 },
		{"id" : 11, "lemma" : "알려지", "type" : "VV", "position" : 49, "weight" : 0.9 },
		{"id" : 12, "lemma" : "어", "type" : "EC", "position" : 55, "weight" : 0.41831 },
		{"id" : 13, "lemma" : "있", "type" : "VX", "position" : 59, "weight" : 0.125953 },
		{"id" : 14, "lemma" : "는", "type" : "ETM", "position" : 62, "weight" : 0.183966 },
		{"id" : 15, "lemma" : "도담", "type" : "NNG", "position" : 66, "weight" : 0.1 },
		{"id" : 16, "lemma" : "삼봉", "type" : "NNG", "position" : 72, "weight" : 0.0924199 },
		{"id" : 17, "lemma" : "이", "type" : "VCP", "position" : 78, "weight" : 0.0177525 },
		{"id" : 18, "lemma" : "다", "type" : "EF", "position" : 81, "weight" : 0.353579 },
		{"id" : 19, "lemma" : ".", "type" : "SF", "position" : 84, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿/NNG+이곳/NP+은/JX", "target" : "﻿이곳은", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "충북/NNG", "target" : "충북", "word_id" : 1, "m_begin" : 3, "m_end" : 3},
		{"id" : 2, "result" : "단양/NNG+의/JKG", "target" : "단양의", "word_id" : 2, "m_begin" : 4, "m_end" : 5},
		{"id" : 3, "result" : "8/SN+경/NNB", "target" : "8경", "word_id" : 3, "m_begin" : 6, "m_end" : 7},
		{"id" : 4, "result" : "중/NNB", "target" : "중", "word_id" : 4, "m_begin" : 8, "m_end" : 8},
		{"id" : 5, "result" : "하나/NNG+로/JKB", "target" : "하나로", "word_id" : 5, "m_begin" : 9, "m_end" : 10},
		{"id" : 6, "result" : "알려지/VV+어/EC", "target" : "알려져", "word_id" : 6, "m_begin" : 11, "m_end" : 12},
		{"id" : 7, "result" : "있/VX+는/ETM", "target" : "있는", "word_id" : 7, "m_begin" : 13, "m_end" : 14},
		{"id" : 8, "result" : "도담삼봉/NNG+이/VCP+다/EF+./SF", "target" : "도담삼봉이다.", "word_id" : 8, "m_begin" : 15, "m_end" : 19}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "이곳", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "충북", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 13, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "단양", "type" : "NNP", "scode" : "01", "weight" : 1, "position" : 20, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 26, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "8", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 30, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "경", "type" : "NNB", "scode" : "00", "weight" : 0, "position" : 31, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "중", "type" : "NNB", "scode" : "04", "weight" : 1, "position" : 35, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "하나", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 39, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 45, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "알려지", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 49, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 55, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "있", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 59, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 62, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "도담삼봉", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 66, "begin" : 15, "end" : 16},
		{"id" : 16, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 78, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 81, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 84, "begin" : 19, "end" : 19}
	],
	"word" : [
		{"id" : 0, "text" : "﻿이곳은", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "충북", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 2, "text" : "단양의", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 3, "text" : "8경", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 4, "text" : "중", "type" : "", "begin" : 8, "end" : 8},
		{"id" : 5, "text" : "하나로", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 6, "text" : "알려져", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 7, "text" : "있는", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 8, "text" : "도담삼봉이다.", "type" : "", "begin" : 15, "end" : 19}
	],
	"NE" : [
		{"id" : 0, "text" : "충북", "type" : "LCP_PROVINCE", "begin" : 3, "end" : 3, "weight" : 0.371222, "common_noun" : 0},
		{"id" : 1, "text" : "단양", "type" : "LCP_COUNTY", "begin" : 4, "end" : 4, "weight" : 0.365797, "common_noun" : 0},
		{"id" : 2, "text" : "8경", "type" : "QT_COUNT", "begin" : 6, "end" : 7, "weight" : 0.308021, "common_noun" : 0},
		{"id" : 3, "text" : "하나", "type" : "QT_COUNT", "begin" : 9, "end" : 9, "weight" : 0.48848, "common_noun" : 0},
		{"id" : 4, "text" : "도담삼봉", "type" : "LCG_MOUNTAIN", "begin" : 15, "end" : 16, "weight" : 0.104866, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿이곳은", "head" : 6, "label" : "NP_SBJ", "mod" : [], "weight" : 0.516623 },
		{"id" : 1, "text" : "충북", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.878913 },
		{"id" : 2, "text" : "단양의", "head" : 3, "label" : "NP_MOD", "mod" : [1], "weight" : 0.644332 },
		{"id" : 3, "text" : "8경", "head" : 4, "label" : "NP", "mod" : [2], "weight" : 0.895532 },
		{"id" : 4, "text" : "중", "head" : 5, "label" : "NP", "mod" : [3], "weight" : 0.693662 },
		{"id" : 5, "text" : "하나로", "head" : 6, "label" : "NP_AJT", "mod" : [4], "weight" : 0.923509 },
		{"id" : 6, "text" : "알려져", "head" : 7, "label" : "VP", "mod" : [0, 5], "weight" : 0.65647 },
		{"id" : 7, "text" : "있는", "head" : 8, "label" : "VP_MOD", "mod" : [6], "weight" : 0.507261 },
		{"id" : 8, "text" : "도담삼봉이다.", "head" : -1, "label" : "VNP", "mod" : [7], "weight" : 0.0346227 }
	],
	"SRL" : [
		{"verb" : "알려지", "sense" : 1, "word_id" : 6, "weight" : 0.207687,
			"argument" : [
				{"type" : "ARG2", "word_id" : 5, "text" : "하나로", "weight" : 0.124025 },
				{"type" : "AUX", "word_id" : 7, "text" : "있는", "weight" : 0.36615 },
				{"type" : "ARG1", "word_id" : 8, "text" : "도담삼봉이다.", "weight" : 0.132887 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 8, "ant_sid" : 0, "ant_wid" : 0, "type" : "s", "istitle" : 0, "weight" : 0.785106 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "3개의 바위로 이루어진 섬으로 많은 위인들이 시와 그림을 통해 그 아름다움을 예찬하기도 했다. ",
	"morp" : [
		{"id" : 0, "lemma" : "3", "type" : "SN", "position" : 86, "weight" : 1 },
		{"id" : 1, "lemma" : "개", "type" : "NNB", "position" : 87, "weight" : 0.363274 },
		{"id" : 2, "lemma" : "의", "type" : "JKG", "position" : 90, "weight" : 0.0520933 },
		{"id" : 3, "lemma" : "바위", "type" : "NNG", "position" : 94, "weight" : 0.870994 },
		{"id" : 4, "lemma" : "로", "type" : "JKB", "position" : 100, "weight" : 0.153229 },
		{"id" : 5, "lemma" : "이루어지", "type" : "VV", "position" : 104, "weight" : 0.9 },
		{"id" : 6, "lemma" : "ㄴ", "type" : "ETM", "position" : 113, "weight" : 0.304215 },
		{"id" : 7, "lemma" : "섬", "type" : "NNG", "position" : 117, "weight" : 0.588632 },
		{"id" : 8, "lemma" : "으로", "type" : "JKB", "position" : 120, "weight" : 0.153406 },
		{"id" : 9, "lemma" : "많", "type" : "VA", "position" : 127, "weight" : 0.0728426 },
		{"id" : 10, "lemma" : "은", "type" : "ETM", "position" : 130, "weight" : 0.155123 },
		{"id" : 11, "lemma" : "위인", "type" : "NNG", "position" : 134, "weight" : 0.9 },
		{"id" : 12, "lemma" : "들", "type" : "XSN", "position" : 140, "weight" : 0.0299841 },
		{"id" : 13, "lemma" : "이", "type" : "JKS", "position" : 143, "weight" : 0.0731805 },
		{"id" : 14, "lemma" : "시", "type" : "NNG", "position" : 147, "weight" : 0.0392668 },
		{"id" : 15, "lemma" : "와", "type" : "JC", "position" : 150, "weight" : 0.0169714 },
		{"id" : 16, "lemma" : "그림", "type" : "NNG", "position" : 154, "weight" : 0.800345 },
		{"id" : 17, "lemma" : "을", "type" : "JKO", "position" : 160, "weight" : 0.129611 },
		{"id" : 18, "lemma" : "통하", "type" : "VV", "position" : 164, "weight" : 0.9 },
		{"id" : 19, "lemma" : "어", "type" : "EC", "position" : 167, "weight" : 0.41831 },
		{"id" : 20, "lemma" : "그", "type" : "MM", "position" : 171, "weight" : 0.0123347 },
		{"id" : 21, "lemma" : "아름", "type" : "NNG", "position" : 175, "weight" : 0.0899333 },
		{"id" : 22, "lemma" : "답", "type" : "XSA", "position" : 181, "weight" : 0.00029128 },
		{"id" : 23, "lemma" : "ㅁ", "type" : "ETN", "position" : 181, "weight" : 0.0422948 },
		{"id" : 24, "lemma" : "을", "type" : "JKO", "position" : 187, "weight" : 0.0925684 },
		{"id" : 25, "lemma" : "예찬", "type" : "NNG", "position" : 191, "weight" : 0.100086 },
		{"id" : 26, "lemma" : "하", "type" : "XSV", "position" : 197, "weight" : 0.0001 },
		{"id" : 27, "lemma" : "기", "type" : "ETN", "position" : 200, "weight" : 0.0556099 },
		{"id" : 28, "lemma" : "도", "type" : "JX", "position" : 203, "weight" : 0.147715 },
		{"id" : 29, "lemma" : "하", "type" : "VX", "position" : 207, "weight" : 0.00978548 },
		{"id" : 30, "lemma" : "었", "type" : "EP", "position" : 207, "weight" : 0.9 },
		{"id" : 31, "lemma" : "다", "type" : "EF", "position" : 210, "weight" : 0.640954 },
		{"id" : 32, "lemma" : ".", "type" : "SF", "position" : 213, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "3/SN+개/NNB+의/JKG", "target" : "3개의", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "바위/NNG+로/JKB", "target" : "바위로", "word_id" : 1, "m_begin" : 3, "m_end" : 4},
		{"id" : 2, "result" : "이루어지/VV+ㄴ/ETM", "target" : "이루어진", "word_id" : 2, "m_begin" : 5, "m_end" : 6},
		{"id" : 3, "result" : "섬/NNG+으로/JKB", "target" : "섬으로", "word_id" : 3, "m_begin" : 7, "m_end" : 8},
		{"id" : 4, "result" : "많/VA+은/ETM", "target" : "많은", "word_id" : 4, "m_begin" : 9, "m_end" : 10},
		{"id" : 5, "result" : "위인들/NNG+이/JKS", "target" : "위인들이", "word_id" : 5, "m_begin" : 11, "m_end" : 13},
		{"id" : 6, "result" : "시/NNG+와/JC", "target" : "시와", "word_id" : 6, "m_begin" : 14, "m_end" : 15},
		{"id" : 7, "result" : "그림/NNG+을/JKO", "target" : "그림을", "word_id" : 7, "m_begin" : 16, "m_end" : 17},
		{"id" : 8, "result" : "통하/VV+어/EC", "target" : "통해", "word_id" : 8, "m_begin" : 18, "m_end" : 19},
		{"id" : 9, "result" : "그/MM", "target" : "그", "word_id" : 9, "m_begin" : 20, "m_end" : 20},
		{"id" : 10, "result" : "아름답/VA+ㅁ/ETN+을/JKO", "target" : "아름다움을", "word_id" : 10, "m_begin" : 21, "m_end" : 24},
		{"id" : 11, "result" : "예찬하/VV+기/ETN+도/JX", "target" : "예찬하기도", "word_id" : 11, "m_begin" : 25, "m_end" : 28},
		{"id" : 12, "result" : "하/VX+었/EP+다/EF+./SF", "target" : "했다.", "word_id" : 12, "m_begin" : 29, "m_end" : 32}
	],
	"WSD" : [
		{"id" : 0, "text" : "3", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 86, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "개", "type" : "NNB", "scode" : "10", "weight" : 1, "position" : 87, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 90, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "바위", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 94, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 100, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "이루어지", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 104, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 113, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "섬", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 117, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 120, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "많", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 127, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "은", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 130, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "위인", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 134, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "들", "type" : "XSN", "scode" : "09", "weight" : 1, "position" : 140, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 143, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "시", "type" : "NNG", "scode" : "13", "weight" : 1, "position" : 147, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "와", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 150, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "그림", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 154, "begin" : 16, "end" : 16},
		{"id" : 17, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 160, "begin" : 17, "end" : 17},
		{"id" : 18, "text" : "통하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 164, "begin" : 18, "end" : 18},
		{"id" : 19, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 167, "begin" : 19, "end" : 19},
		{"id" : 20, "text" : "그", "type" : "MM", "scode" : "01", "weight" : 1, "position" : 171, "begin" : 20, "end" : 20},
		{"id" : 21, "text" : "아름답", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 175, "begin" : 21, "end" : 22},
		{"id" : 22, "text" : "ㅁ", "type" : "ETN", "scode" : "00", "weight" : 1, "position" : 181, "begin" : 23, "end" : 23},
		{"id" : 23, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 187, "begin" : 24, "end" : 24},
		{"id" : 24, "text" : "예찬하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 191, "begin" : 25, "end" : 26},
		{"id" : 25, "text" : "기", "type" : "ETN", "scode" : "00", "weight" : 1, "position" : 200, "begin" : 27, "end" : 27},
		{"id" : 26, "text" : "도", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 203, "begin" : 28, "end" : 28},
		{"id" : 27, "text" : "하", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 207, "begin" : 29, "end" : 29},
		{"id" : 28, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 207, "begin" : 30, "end" : 30},
		{"id" : 29, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 210, "begin" : 31, "end" : 31},
		{"id" : 30, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 213, "begin" : 32, "end" : 32}
	],
	"word" : [
		{"id" : 0, "text" : "3개의", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "바위로", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 2, "text" : "이루어진", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 3, "text" : "섬으로", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 4, "text" : "많은", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 5, "text" : "위인들이", "type" : "", "begin" : 11, "end" : 13},
		{"id" : 6, "text" : "시와", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 7, "text" : "그림을", "type" : "", "begin" : 16, "end" : 17},
		{"id" : 8, "text" : "통해", "type" : "", "begin" : 18, "end" : 19},
		{"id" : 9, "text" : "그", "type" : "", "begin" : 20, "end" : 20},
		{"id" : 10, "text" : "아름다움을", "type" : "", "begin" : 21, "end" : 24},
		{"id" : 11, "text" : "예찬하기도", "type" : "", "begin" : 25, "end" : 28},
		{"id" : 12, "text" : "했다.", "type" : "", "begin" : 29, "end" : 32}
	],
	"NE" : [
		{"id" : 0, "text" : "3개", "type" : "QT_COUNT", "begin" : 0, "end" : 1, "weight" : 0.544881, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "3개의", "head" : 1, "label" : "NP_MOD", "mod" : [], "weight" : 0.941167 },
		{"id" : 1, "text" : "바위로", "head" : 2, "label" : "NP_AJT", "mod" : [0], "weight" : 0.715082 },
		{"id" : 2, "text" : "이루어진", "head" : 3, "label" : "VP_MOD", "mod" : [1], "weight" : 0.897777 },
		{"id" : 3, "text" : "섬으로", "head" : 8, "label" : "NP_AJT", "mod" : [2], "weight" : 0.789226 },
		{"id" : 4, "text" : "많은", "head" : 5, "label" : "VP_MOD", "mod" : [], "weight" : 0.838312 },
		{"id" : 5, "text" : "위인들이", "head" : 8, "label" : "NP_SBJ", "mod" : [4], "weight" : 0.816823 },
		{"id" : 6, "text" : "시와", "head" : 7, "label" : "NP_CNJ", "mod" : [], "weight" : 0.936251 },
		{"id" : 7, "text" : "그림을", "head" : 8, "label" : "NP_OBJ", "mod" : [6], "weight" : 0.747193 },
		{"id" : 8, "text" : "통해", "head" : 11, "label" : "VP", "mod" : [3, 5, 7], "weight" : 0.951481 },
		{"id" : 9, "text" : "그", "head" : 10, "label" : "DP", "mod" : [], "weight" : 0.941863 },
		{"id" : 10, "text" : "아름다움을", "head" : 11, "label" : "NP_OBJ", "mod" : [9], "weight" : 0.865604 },
		{"id" : 11, "text" : "예찬하기도", "head" : 12, "label" : "VP", "mod" : [8, 10], "weight" : 0.413675 },
		{"id" : 12, "text" : "했다.", "head" : -1, "label" : "VP", "mod" : [11], "weight" : 0.0587039 }
	],
	"SRL" : [
		{"verb" : "이루어지", "sense" : 1, "word_id" : 2, "weight" : 0.235651,
			"argument" : [
				{"type" : "ARGM-INS", "word_id" : 1, "text" : "바위로", "weight" : 0.0911004 },
				{"type" : "ARG1", "word_id" : 3, "text" : "섬으로", "weight" : 0.380201 }
			] },
		{"verb" : "많", "sense" : 1, "word_id" : 4, "weight" : 0.379303,
			"argument" : [
				{"type" : "ARG1", "word_id" : 5, "text" : "위인들이", "weight" : 0.379303 }
			] },
		{"verb" : "통하", "sense" : 1, "word_id" : 8, "weight" : 0.305991,
			"argument" : [
				{"type" : "ARG1", "word_id" : 5, "text" : "위인들이", "weight" : 0.19142 },
				{"type" : "ARG2", "word_id" : 6, "text" : "시와", "weight" : 0.281815 },
				{"type" : "ARG2", "word_id" : 7, "text" : "그림을", "weight" : 0.444739 }
			] },
		{"verb" : "예찬", "sense" : 1, "word_id" : 11, "weight" : 0.319866,
			"argument" : [
				{"type" : "ARG0", "word_id" : 5, "text" : "위인들이", "weight" : 0.277739 },
				{"type" : "ARG1", "word_id" : 10, "text" : "아름다움을", "weight" : 0.250306 },
				{"type" : "AUX", "word_id" : 12, "text" : "했다.", "weight" : 0.431554 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 11, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.341274 }
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "단양의 군수로 부임하면서 도담삼봉의 절경을 예찬하는 시를 남겼으며 <성학십도>, <주자서절요>등을 남긴 조선 중기의 학자는 누구일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "단양", "type" : "NNP", "position" : 215, "weight" : 0.0877327 },
		{"id" : 1, "lemma" : "의", "type" : "JKG", "position" : 221, "weight" : 0.0987295 },
		{"id" : 2, "lemma" : "군수", "type" : "NNG", "position" : 225, "weight" : 0.866636 },
		{"id" : 3, "lemma" : "로", "type" : "JKB", "position" : 231, "weight" : 0.153229 },
		{"id" : 4, "lemma" : "부임", "type" : "NNG", "position" : 235, "weight" : 0.9 },
		{"id" : 5, "lemma" : "하", "type" : "XSV", "position" : 241, "weight" : 0.0001 },
		{"id" : 6, "lemma" : "면서", "type" : "EC", "position" : 244, "weight" : 0.370489 },
		{"id" : 7, "lemma" : "도담", "type" : "NNG", "position" : 251, "weight" : 0.1 },
		{"id" : 8, "lemma" : "삼봉", "type" : "NNG", "position" : 257, "weight" : 0.0924199 },
		{"id" : 9, "lemma" : "의", "type" : "JKG", "position" : 263, "weight" : 0.0694213 },
		{"id" : 10, "lemma" : "절경", "type" : "NNG", "position" : 267, "weight" : 0.9 },
		{"id" : 11, "lemma" : "을", "type" : "JKO", "position" : 273, "weight" : 0.129611 },
		{"id" : 12, "lemma" : "예찬", "type" : "NNG", "position" : 277, "weight" : 0.100086 },
		{"id" : 13, "lemma" : "하", "type" : "XSV", "position" : 283, "weight" : 0.0001 },
		{"id" : 14, "lemma" : "는", "type" : "ETM", "position" : 286, "weight" : 0.238503 },
		{"id" : 15, "lemma" : "시", "type" : "NNG", "position" : 290, "weight" : 0.123639 },
		{"id" : 16, "lemma" : "를", "type" : "JKO", "position" : 293, "weight" : 0.137686 },
		{"id" : 17, "lemma" : "남기", "type" : "VV", "position" : 297, "weight" : 0.778316 },
		{"id" : 18, "lemma" : "었", "type" : "EP", "position" : 300, "weight" : 0.9 },
		{"id" : 19, "lemma" : "으며", "type" : "EC", "position" : 303, "weight" : 0.197482 },
		{"id" : 20, "lemma" : "<", "type" : "SS", "position" : 310, "weight" : 1 },
		{"id" : 21, "lemma" : "성학십도", "type" : "NNP", "position" : 311, "weight" : 0.15 },
		{"id" : 22, "lemma" : ">", "type" : "SS", "position" : 323, "weight" : 1 },
		{"id" : 23, "lemma" : ",", "type" : "SP", "position" : 324, "weight" : 1 },
		{"id" : 24, "lemma" : "<", "type" : "SS", "position" : 326, "weight" : 1 },
		{"id" : 25, "lemma" : "주자서", "type" : "NNP", "position" : 327, "weight" : 0.6 },
		{"id" : 26, "lemma" : "절요", "type" : "NNG", "position" : 336, "weight" : 0.1 },
		{"id" : 27, "lemma" : ">", "type" : "SS", "position" : 342, "weight" : 1 },
		{"id" : 28, "lemma" : "등", "type" : "NNB", "position" : 343, "weight" : 0.0121753 },
		{"id" : 29, "lemma" : "을", "type" : "JKO", "position" : 346, "weight" : 0.0630104 },
		{"id" : 30, "lemma" : "남기", "type" : "VV", "position" : 350, "weight" : 0.778316 },
		{"id" : 31, "lemma" : "ㄴ", "type" : "ETM", "position" : 353, "weight" : 0.304215 },
		{"id" : 32, "lemma" : "조선", "type" : "NNP", "position" : 357, "weight" : 0.027614 },
		{"id" : 33, "lemma" : "중기", "type" : "NNG", "position" : 364, "weight" : 0.279669 },
		{"id" : 34, "lemma" : "의", "type" : "JKG", "position" : 370, "weight" : 0.0694213 },
		{"id" : 35, "lemma" : "학자", "type" : "NNG", "position" : 374, "weight" : 0.9 },
		{"id" : 36, "lemma" : "는", "type" : "JX", "position" : 380, "weight" : 0.0287565 },
		{"id" : 37, "lemma" : "누구", "type" : "NP", "position" : 384, "weight" : 0.9 },
		{"id" : 38, "lemma" : "이", "type" : "VCP", "position" : 390, "weight" : 0.0175768 },
		{"id" : 39, "lemma" : "ㄹ까", "type" : "EF", "position" : 390, "weight" : 0.258243 },
		{"id" : 40, "lemma" : "?", "type" : "SF", "position" : 396, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "단양/NNG+의/JKG", "target" : "단양의", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "군수/NNG+로/JKB", "target" : "군수로", "word_id" : 1, "m_begin" : 2, "m_end" : 3},
		{"id" : 2, "result" : "부임하/VV+면서/EC", "target" : "부임하면서", "word_id" : 2, "m_begin" : 4, "m_end" : 6},
		{"id" : 3, "result" : "도담삼봉/NNG+의/JKG", "target" : "도담삼봉의", "word_id" : 3, "m_begin" : 7, "m_end" : 9},
		{"id" : 4, "result" : "절경/NNG+을/JKO", "target" : "절경을", "word_id" : 4, "m_begin" : 10, "m_end" : 11},
		{"id" : 5, "result" : "예찬하/VV+는/ETM", "target" : "예찬하는", "word_id" : 5, "m_begin" : 12, "m_end" : 14},
		{"id" : 6, "result" : "시/NNG+를/JKO", "target" : "시를", "word_id" : 6, "m_begin" : 15, "m_end" : 16},
		{"id" : 7, "result" : "남기/VV+었/EP+으며/EC", "target" : "남겼으며", "word_id" : 7, "m_begin" : 17, "m_end" : 19},
		{"id" : 8, "result" : "</SS+성학십도/NNG+>/SS+,/SP", "target" : "<성학십도>,", "word_id" : 8, "m_begin" : 20, "m_end" : 23},
		{"id" : 9, "result" : "</SS+주자서절요/NNG+>/SS+등/NNB+을/JKO", "target" : "<주자서절요>등을", "word_id" : 9, "m_begin" : 24, "m_end" : 29},
		{"id" : 10, "result" : "남기/VV+ㄴ/ETM", "target" : "남긴", "word_id" : 10, "m_begin" : 30, "m_end" : 31},
		{"id" : 11, "result" : "조선/NNG", "target" : "조선", "word_id" : 11, "m_begin" : 32, "m_end" : 32},
		{"id" : 12, "result" : "중기/NNG+의/JKG", "target" : "중기의", "word_id" : 12, "m_begin" : 33, "m_end" : 34},
		{"id" : 13, "result" : "학자/NNG+는/JX", "target" : "학자는", "word_id" : 13, "m_begin" : 35, "m_end" : 36},
		{"id" : 14, "result" : "누구/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "누구일까?", "word_id" : 14, "m_begin" : 37, "m_end" : 40}
	],
	"WSD" : [
		{"id" : 0, "text" : "단양", "type" : "NNP", "scode" : "01", "weight" : 1, "position" : 215, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 221, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "군수", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 225, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 231, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "부임하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 235, "begin" : 4, "end" : 5},
		{"id" : 5, "text" : "면서", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 244, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "도담삼봉", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 251, "begin" : 7, "end" : 8},
		{"id" : 7, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 263, "begin" : 9, "end" : 9},
		{"id" : 8, "text" : "절경", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 267, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 273, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "예찬하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 277, "begin" : 12, "end" : 13},
		{"id" : 11, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 286, "begin" : 14, "end" : 14},
		{"id" : 12, "text" : "시", "type" : "NNG", "scode" : "13", "weight" : 1, "position" : 290, "begin" : 15, "end" : 15},
		{"id" : 13, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 293, "begin" : 16, "end" : 16},
		{"id" : 14, "text" : "남기", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 297, "begin" : 17, "end" : 17},
		{"id" : 15, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 300, "begin" : 18, "end" : 18},
		{"id" : 16, "text" : "으며", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 303, "begin" : 19, "end" : 19},
		{"id" : 17, "text" : "<", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 310, "begin" : 20, "end" : 20},
		{"id" : 18, "text" : "성학십도", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 311, "begin" : 21, "end" : 21},
		{"id" : 19, "text" : ">", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 323, "begin" : 22, "end" : 22},
		{"id" : 20, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 324, "begin" : 23, "end" : 23},
		{"id" : 21, "text" : "<", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 326, "begin" : 24, "end" : 24},
		{"id" : 22, "text" : "주자서절요", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 327, "begin" : 25, "end" : 26},
		{"id" : 23, "text" : ">", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 342, "begin" : 27, "end" : 27},
		{"id" : 24, "text" : "등", "type" : "NNB", "scode" : "05", "weight" : 1, "position" : 343, "begin" : 28, "end" : 28},
		{"id" : 25, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 346, "begin" : 29, "end" : 29},
		{"id" : 26, "text" : "남기", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 350, "begin" : 30, "end" : 30},
		{"id" : 27, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 353, "begin" : 31, "end" : 31},
		{"id" : 28, "text" : "조선", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 357, "begin" : 32, "end" : 32},
		{"id" : 29, "text" : "중기", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 364, "begin" : 33, "end" : 33},
		{"id" : 30, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 370, "begin" : 34, "end" : 34},
		{"id" : 31, "text" : "학자", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 374, "begin" : 35, "end" : 35},
		{"id" : 32, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 380, "begin" : 36, "end" : 36},
		{"id" : 33, "text" : "누구", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 384, "begin" : 37, "end" : 37},
		{"id" : 34, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 390, "begin" : 38, "end" : 38},
		{"id" : 35, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 390, "begin" : 39, "end" : 39},
		{"id" : 36, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 396, "begin" : 40, "end" : 40}
	],
	"word" : [
		{"id" : 0, "text" : "단양의", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "군수로", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 2, "text" : "부임하면서", "type" : "", "begin" : 4, "end" : 6},
		{"id" : 3, "text" : "도담삼봉의", "type" : "", "begin" : 7, "end" : 9},
		{"id" : 4, "text" : "절경을", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 5, "text" : "예찬하는", "type" : "", "begin" : 12, "end" : 14},
		{"id" : 6, "text" : "시를", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 7, "text" : "남겼으며", "type" : "", "begin" : 17, "end" : 19},
		{"id" : 8, "text" : "<성학십도>,", "type" : "", "begin" : 20, "end" : 23},
		{"id" : 9, "text" : "<주자서절요>등을", "type" : "", "begin" : 24, "end" : 29},
		{"id" : 10, "text" : "남긴", "type" : "", "begin" : 30, "end" : 31},
		{"id" : 11, "text" : "조선", "type" : "", "begin" : 32, "end" : 32},
		{"id" : 12, "text" : "중기의", "type" : "", "begin" : 33, "end" : 34},
		{"id" : 13, "text" : "학자는", "type" : "", "begin" : 35, "end" : 36},
		{"id" : 14, "text" : "누구일까?", "type" : "", "begin" : 37, "end" : 40}
	],
	"NE" : [
		{"id" : 0, "text" : "단양", "type" : "LCP_COUNTY", "begin" : 0, "end" : 0, "weight" : 0.272192, "common_noun" : 0},
		{"id" : 1, "text" : "군수", "type" : "CV_POSITION", "begin" : 2, "end" : 2, "weight" : 0.432208, "common_noun" : 0},
		{"id" : 2, "text" : "도담삼봉", "type" : "LCG_MOUNTAIN", "begin" : 7, "end" : 8, "weight" : 0.105663, "common_noun" : 0},
		{"id" : 3, "text" : "성학십도", "type" : "AFW_ART_CRAFT", "begin" : 21, "end" : 21, "weight" : 0.207838, "common_noun" : 0},
		{"id" : 4, "text" : "주자서절요", "type" : "AF_WORKS", "begin" : 25, "end" : 26, "weight" : 0.57905, "common_noun" : 0},
		{"id" : 5, "text" : "조선 중기", "type" : "DT_DYNASTY", "begin" : 32, "end" : 33, "weight" : 0.725209, "common_noun" : 0},
		{"id" : 6, "text" : "학자", "type" : "CV_OCCUPATION", "begin" : 35, "end" : 35, "weight" : 0.429343, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "단양의", "head" : 1, "label" : "NP_MOD", "mod" : [], "weight" : 0.763593 },
		{"id" : 1, "text" : "군수로", "head" : 2, "label" : "NP_AJT", "mod" : [0], "weight" : 0.955275 },
		{"id" : 2, "text" : "부임하면서", "head" : 5, "label" : "VP", "mod" : [1], "weight" : 0.578169 },
		{"id" : 3, "text" : "도담삼봉의", "head" : 4, "label" : "NP_MOD", "mod" : [], "weight" : 0.764514 },
		{"id" : 4, "text" : "절경을", "head" : 5, "label" : "NP_OBJ", "mod" : [3], "weight" : 0.78241 },
		{"id" : 5, "text" : "예찬하는", "head" : 6, "label" : "VP_MOD", "mod" : [2, 4], "weight" : 0.765156 },
		{"id" : 6, "text" : "시를", "head" : 7, "label" : "NP_OBJ", "mod" : [5], "weight" : 0.931756 },
		{"id" : 7, "text" : "남겼으며", "head" : 10, "label" : "VP", "mod" : [6], "weight" : 0.652043 },
		{"id" : 8, "text" : "<성학십도>,", "head" : 9, "label" : "NP_CNJ", "mod" : [], "weight" : 0.779356 },
		{"id" : 9, "text" : "<주자서절요>등을", "head" : 10, "label" : "NP_OBJ", "mod" : [8], "weight" : 0.810585 },
		{"id" : 10, "text" : "남긴", "head" : 13, "label" : "VP_MOD", "mod" : [7, 9], "weight" : 0.9264 },
		{"id" : 11, "text" : "조선", "head" : 12, "label" : "NP", "mod" : [], "weight" : 0.978068 },
		{"id" : 12, "text" : "중기의", "head" : 13, "label" : "NP_MOD", "mod" : [11], "weight" : 0.763541 },
		{"id" : 13, "text" : "학자는", "head" : 14, "label" : "NP_SBJ", "mod" : [10, 12], "weight" : 0.711916 },
		{"id" : 14, "text" : "누구일까?", "head" : -1, "label" : "VNP", "mod" : [13], "weight" : 0.0257724 }
	],
	"SRL" : [
		{"verb" : "부임", "sense" : 1, "word_id" : 2, "weight" : 0.136429,
			"argument" : [
				{"type" : "ARG1", "word_id" : 1, "text" : "군수로", "weight" : 0.136429 }
			] },
		{"verb" : "예찬", "sense" : 1, "word_id" : 5, "weight" : 0.233596,
			"argument" : [
				{"type" : "ARG1", "word_id" : 4, "text" : "절경을", "weight" : 0.233596 }
			] },
		{"verb" : "남기", "sense" : 1, "word_id" : 7, "weight" : 0.254853,
			"argument" : [
				{"type" : "ARGM-MNR", "word_id" : 2, "text" : "부임하면서", "weight" : 0.104699 },
				{"type" : "ARG1", "word_id" : 6, "text" : "시를", "weight" : 0.331045 },
				{"type" : "ARG0", "word_id" : 13, "text" : "학자는", "weight" : 0.328814 }
			] },
		{"verb" : "남기", "sense" : 1, "word_id" : 10, "weight" : 0.295098,
			"argument" : [
				{"type" : "ARG1", "word_id" : 9, "text" : "<주자서절요>등을", "weight" : 0.259572 },
				{"type" : "ARG0", "word_id" : 13, "text" : "학자는", "weight" : 0.330624 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 2, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.499275 },
		{"id" : 1, "verb_wid" : 7, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.137783 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "QT_COUNT", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "단양", 
	 "mention" : [
		{"id" : 3, "sent_id" : 0, "start_eid" : 1, "end_eid" : 3, "ne_id" : 2, "text" : "충북 단양의 8경", "start_eid_short" : 1, "end_eid_short" : 3, "text_short" : "충북 단양의 8경", "weight" : 0.002 },
		{"id" : 14, "sent_id" : 1, "start_eid" : 9, "end_eid" : 10, "ne_id" : -1, "text" : "그 아름답ㅁ", "start_eid_short" : 9, "end_eid_short" : 10, "text_short" : "그 아름답ㅁ", "weight" : 0.006 }
	] },
	{"id" : 1, "type" : "LCP_COUNTY", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "단양", 
	 "mention" : [
		{"id" : 4, "sent_id" : 0, "start_eid" : 1, "end_eid" : 2, "ne_id" : 1, "text" : "충북 단양", "start_eid_short" : 1, "end_eid_short" : 2, "text_short" : "충북 단양", "weight" : 0.006 },
		{"id" : 19, "sent_id" : 2, "start_eid" : 0, "end_eid" : 0, "ne_id" : 0, "text" : "단양", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "단양", "weight" : 0.01 }
	] },
	{"id" : 2, "type" : "CV_OCCUPATION", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "단양", 
	 "mention" : [
		{"id" : 15, "sent_id" : 2, "start_eid" : 0, "end_eid" : 13, "ne_id" : 6, "text" : "단양의 군수로 부임하면서 도담삼봉의 절경을 예찬하는 시를 남겼으며 <성학십도>, <주자서절요>등을 남긴 조선 중기의 학자", "start_eid_short" : 11, "end_eid_short" : 13, "text_short" : "조선 중기의 학자", "weight" : 0.005 },
		{"id" : 25, "sent_id" : 2, "start_eid" : 14, "end_eid" : 14, "ne_id" : -1, "text" : "누구", "start_eid_short" : 14, "end_eid_short" : 14, "text_short" : "누구이ㄹ까?", "weight" : 0.006 }
	] }
 ]
}

