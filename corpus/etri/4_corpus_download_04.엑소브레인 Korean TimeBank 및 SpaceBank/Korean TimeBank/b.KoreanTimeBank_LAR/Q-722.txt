{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿이것은 지중해와 홍해를 연결하는 세계 최대의 인공 수로로 1869년에 개통됐다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "이것", "type" : "NP", "position" : 3, "weight" : 0.0020857 },
		{"id" : 2, "lemma" : "은", "type" : "JX", "position" : 9, "weight" : 0.191811 },
		{"id" : 3, "lemma" : "지중해", "type" : "NNP", "position" : 13, "weight" : 0.0433929 },
		{"id" : 4, "lemma" : "와", "type" : "JC", "position" : 22, "weight" : 0.0104534 },
		{"id" : 5, "lemma" : "홍해", "type" : "NNP", "position" : 26, "weight" : 0.9 },
		{"id" : 6, "lemma" : "를", "type" : "JKO", "position" : 32, "weight" : 0.0283046 },
		{"id" : 7, "lemma" : "연결", "type" : "NNG", "position" : 36, "weight" : 0.9 },
		{"id" : 8, "lemma" : "하", "type" : "XSV", "position" : 42, "weight" : 0.0001 },
		{"id" : 9, "lemma" : "는", "type" : "ETM", "position" : 45, "weight" : 0.238503 },
		{"id" : 10, "lemma" : "세계", "type" : "NNG", "position" : 49, "weight" : 0.9 },
		{"id" : 11, "lemma" : "최대", "type" : "NNG", "position" : 56, "weight" : 0.9 },
		{"id" : 12, "lemma" : "의", "type" : "JKG", "position" : 62, "weight" : 0.0694213 },
		{"id" : 13, "lemma" : "인공", "type" : "NNG", "position" : 66, "weight" : 0.9 },
		{"id" : 14, "lemma" : "수로", "type" : "NNG", "position" : 73, "weight" : 0.171587 },
		{"id" : 15, "lemma" : "로", "type" : "JKB", "position" : 79, "weight" : 0.153229 },
		{"id" : 16, "lemma" : "1869", "type" : "SN", "position" : 83, "weight" : 1 },
		{"id" : 17, "lemma" : "년", "type" : "NNB", "position" : 87, "weight" : 0.414343 },
		{"id" : 18, "lemma" : "에", "type" : "JKB", "position" : 90, "weight" : 0.135559 },
		{"id" : 19, "lemma" : "개통", "type" : "NNG", "position" : 94, "weight" : 0.9 },
		{"id" : 20, "lemma" : "되", "type" : "XSV", "position" : 100, "weight" : 0.000224177 },
		{"id" : 21, "lemma" : "었", "type" : "EP", "position" : 100, "weight" : 0.9 },
		{"id" : 22, "lemma" : "다", "type" : "EF", "position" : 103, "weight" : 0.640954 },
		{"id" : 23, "lemma" : ".", "type" : "SF", "position" : 106, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿/NNG+이것/NP+은/JX", "target" : "﻿이것은", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "지중해/NNG+와/JC", "target" : "지중해와", "word_id" : 1, "m_begin" : 3, "m_end" : 4},
		{"id" : 2, "result" : "홍해/NNG+를/JKO", "target" : "홍해를", "word_id" : 2, "m_begin" : 5, "m_end" : 6},
		{"id" : 3, "result" : "연결하/VV+는/ETM", "target" : "연결하는", "word_id" : 3, "m_begin" : 7, "m_end" : 9},
		{"id" : 4, "result" : "세계/NNG", "target" : "세계", "word_id" : 4, "m_begin" : 10, "m_end" : 10},
		{"id" : 5, "result" : "최대/NNG+의/JKG", "target" : "최대의", "word_id" : 5, "m_begin" : 11, "m_end" : 12},
		{"id" : 6, "result" : "인공/NNG", "target" : "인공", "word_id" : 6, "m_begin" : 13, "m_end" : 13},
		{"id" : 7, "result" : "수로/NNG+로/JKB", "target" : "수로로", "word_id" : 7, "m_begin" : 14, "m_end" : 15},
		{"id" : 8, "result" : "1869/SN+년/NNB+에/JKB", "target" : "1869년에", "word_id" : 8, "m_begin" : 16, "m_end" : 18},
		{"id" : 9, "result" : "개통되/VV+었/EP+다/EF+./SF", "target" : "개통됐다.", "word_id" : 9, "m_begin" : 19, "m_end" : 23}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "지중해", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 13, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "와", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 22, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "홍해", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 26, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 32, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "연결하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 36, "begin" : 7, "end" : 8},
		{"id" : 8, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 45, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "세계", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 49, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "최대", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 56, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 62, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "인공", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 66, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "수로", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 73, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 79, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "1869", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 83, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 87, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 90, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "개통되", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 94, "begin" : 19, "end" : 20},
		{"id" : 19, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 100, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 103, "begin" : 22, "end" : 22},
		{"id" : 21, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 106, "begin" : 23, "end" : 23}
	],
	"word" : [
		{"id" : 0, "text" : "﻿이것은", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "지중해와", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 2, "text" : "홍해를", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 3, "text" : "연결하는", "type" : "", "begin" : 7, "end" : 9},
		{"id" : 4, "text" : "세계", "type" : "", "begin" : 10, "end" : 10},
		{"id" : 5, "text" : "최대의", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 6, "text" : "인공", "type" : "", "begin" : 13, "end" : 13},
		{"id" : 7, "text" : "수로로", "type" : "", "begin" : 14, "end" : 15},
		{"id" : 8, "text" : "1869년에", "type" : "", "begin" : 16, "end" : 18},
		{"id" : 9, "text" : "개통됐다.", "type" : "", "begin" : 19, "end" : 23}
	],
	"NE" : [
		{"id" : 0, "text" : "지중해", "type" : "LCG_OCEAN", "begin" : 3, "end" : 3, "weight" : 0.234954, "common_noun" : 0},
		{"id" : 1, "text" : "홍해", "type" : "LCG_OCEAN", "begin" : 5, "end" : 5, "weight" : 0.206269, "common_noun" : 0},
		{"id" : 2, "text" : "1869년", "type" : "DT_YEAR", "begin" : 16, "end" : 17, "weight" : 0.406476, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿이것은", "head" : 9, "label" : "NP_SBJ", "mod" : [], "weight" : 0.614795 },
		{"id" : 1, "text" : "지중해와", "head" : 2, "label" : "NP_CNJ", "mod" : [], "weight" : 0.728741 },
		{"id" : 2, "text" : "홍해를", "head" : 3, "label" : "NP_OBJ", "mod" : [1], "weight" : 0.935257 },
		{"id" : 3, "text" : "연결하는", "head" : 7, "label" : "VP_MOD", "mod" : [2], "weight" : 0.942914 },
		{"id" : 4, "text" : "세계", "head" : 5, "label" : "NP", "mod" : [], "weight" : 0.78371 },
		{"id" : 5, "text" : "최대의", "head" : 7, "label" : "NP_MOD", "mod" : [4], "weight" : 0.922313 },
		{"id" : 6, "text" : "인공", "head" : 7, "label" : "NP", "mod" : [], "weight" : 0.826373 },
		{"id" : 7, "text" : "수로로", "head" : 9, "label" : "NP_AJT", "mod" : [3, 5, 6], "weight" : 0.599195 },
		{"id" : 8, "text" : "1869년에", "head" : 9, "label" : "NP_AJT", "mod" : [], "weight" : 0.535939 },
		{"id" : 9, "text" : "개통됐다.", "head" : -1, "label" : "VP", "mod" : [0, 7, 8], "weight" : 0.044434 }
	],
	"SRL" : [
		{"verb" : "연결", "sense" : 1, "word_id" : 3, "weight" : 0.251889,
			"argument" : [
				{"type" : "ARG2", "word_id" : 1, "text" : "지중해와", "weight" : 0.24469 },
				{"type" : "ARG1", "word_id" : 2, "text" : "홍해를", "weight" : 0.228222 },
				{"type" : "ARG0", "word_id" : 7, "text" : "수로로", "weight" : 0.282755 }
			] },
		{"verb" : "개통", "sense" : 1, "word_id" : 9, "weight" : 0.127502,
			"argument" : [
				{"type" : "ARG1", "word_id" : 7, "text" : "수로로", "weight" : 0.0657356 },
				{"type" : "ARGM-TMP", "word_id" : 8, "text" : "1869년에", "weight" : 0.189269 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "유럽과 인도를 오가는 항로를 약 3분의 1 단축한 이 수로는 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "유럽", "type" : "NNP", "position" : 108, "weight" : 0.9 },
		{"id" : 1, "lemma" : "과", "type" : "JC", "position" : 114, "weight" : 0.0108215 },
		{"id" : 2, "lemma" : "인도", "type" : "NNP", "position" : 118, "weight" : 0.0614744 },
		{"id" : 3, "lemma" : "를", "type" : "JKO", "position" : 124, "weight" : 0.0283046 },
		{"id" : 4, "lemma" : "오가", "type" : "VV", "position" : 128, "weight" : 0.769858 },
		{"id" : 5, "lemma" : "는", "type" : "ETM", "position" : 134, "weight" : 0.184941 },
		{"id" : 6, "lemma" : "항로", "type" : "NNG", "position" : 138, "weight" : 0.9 },
		{"id" : 7, "lemma" : "를", "type" : "JKO", "position" : 144, "weight" : 0.137686 },
		{"id" : 8, "lemma" : "약", "type" : "MM", "position" : 148, "weight" : 0.00377349 },
		{"id" : 9, "lemma" : "3", "type" : "SN", "position" : 152, "weight" : 1 },
		{"id" : 10, "lemma" : "분", "type" : "XSN", "position" : 153, "weight" : 0.00279557 },
		{"id" : 11, "lemma" : "의", "type" : "JKG", "position" : 156, "weight" : 0.120732 },
		{"id" : 12, "lemma" : "1", "type" : "SN", "position" : 160, "weight" : 1 },
		{"id" : 13, "lemma" : "단축", "type" : "NNG", "position" : 162, "weight" : 0.9 },
		{"id" : 14, "lemma" : "하", "type" : "XSV", "position" : 168, "weight" : 0.0001 },
		{"id" : 15, "lemma" : "ㄴ", "type" : "ETM", "position" : 168, "weight" : 0.392321 },
		{"id" : 16, "lemma" : "이", "type" : "MM", "position" : 172, "weight" : 0.00060084 },
		{"id" : 17, "lemma" : "수", "type" : "NNG", "position" : 176, "weight" : 0.0372362 },
		{"id" : 18, "lemma" : "로", "type" : "JKB", "position" : 179, "weight" : 0.153229 },
		{"id" : 19, "lemma" : "는", "type" : "JX", "position" : 182, "weight" : 0.0387928 },
		{"id" : 20, "lemma" : "무엇", "type" : "NP", "position" : 186, "weight" : 0.9 },
		{"id" : 21, "lemma" : "이", "type" : "VCP", "position" : 192, "weight" : 0.0175768 },
		{"id" : 22, "lemma" : "ㄹ까", "type" : "EF", "position" : 192, "weight" : 0.258243 },
		{"id" : 23, "lemma" : "?", "type" : "SF", "position" : 198, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "유럽/NNG+과/JC", "target" : "유럽과", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "인도/NNG+를/JKO", "target" : "인도를", "word_id" : 1, "m_begin" : 2, "m_end" : 3},
		{"id" : 2, "result" : "오가/VV+는/ETM", "target" : "오가는", "word_id" : 2, "m_begin" : 4, "m_end" : 5},
		{"id" : 3, "result" : "항로/NNG+를/JKO", "target" : "항로를", "word_id" : 3, "m_begin" : 6, "m_end" : 7},
		{"id" : 4, "result" : "약/MM", "target" : "약", "word_id" : 4, "m_begin" : 8, "m_end" : 8},
		{"id" : 5, "result" : "3/SN+분/XSN+의/JKG", "target" : "3분의", "word_id" : 5, "m_begin" : 9, "m_end" : 11},
		{"id" : 6, "result" : "1/SN", "target" : "1", "word_id" : 6, "m_begin" : 12, "m_end" : 12},
		{"id" : 7, "result" : "단축하/VV+ㄴ/ETM", "target" : "단축한", "word_id" : 7, "m_begin" : 13, "m_end" : 15},
		{"id" : 8, "result" : "이/MM", "target" : "이", "word_id" : 8, "m_begin" : 16, "m_end" : 16},
		{"id" : 9, "result" : "수/NNG+로/JKB+는/JX", "target" : "수로는", "word_id" : 9, "m_begin" : 17, "m_end" : 19},
		{"id" : 10, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 10, "m_begin" : 20, "m_end" : 23}
	],
	"WSD" : [
		{"id" : 0, "text" : "유럽", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 108, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "과", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 114, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "인도", "type" : "NNP", "scode" : "08", "weight" : 1, "position" : 118, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 124, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "오가", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 128, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 134, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "항로", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 138, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 144, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "약", "type" : "MM", "scode" : "03", "weight" : 1, "position" : 148, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "3", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 152, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "분", "type" : "XSN", "scode" : "15", "weight" : 1, "position" : 153, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 156, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "1", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 160, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "단축하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 162, "begin" : 13, "end" : 14},
		{"id" : 14, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 168, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 172, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "수", "type" : "NNG", "scode" : "26", "weight" : 1, "position" : 176, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 179, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 182, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 186, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 192, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 192, "begin" : 22, "end" : 22},
		{"id" : 22, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 198, "begin" : 23, "end" : 23}
	],
	"word" : [
		{"id" : 0, "text" : "유럽과", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "인도를", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 2, "text" : "오가는", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 3, "text" : "항로를", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 4, "text" : "약", "type" : "", "begin" : 8, "end" : 8},
		{"id" : 5, "text" : "3분의", "type" : "", "begin" : 9, "end" : 11},
		{"id" : 6, "text" : "1", "type" : "", "begin" : 12, "end" : 12},
		{"id" : 7, "text" : "단축한", "type" : "", "begin" : 13, "end" : 15},
		{"id" : 8, "text" : "이", "type" : "", "begin" : 16, "end" : 16},
		{"id" : 9, "text" : "수로는", "type" : "", "begin" : 17, "end" : 19},
		{"id" : 10, "text" : "무엇일까?", "type" : "", "begin" : 20, "end" : 23}
	],
	"NE" : [
		{"id" : 0, "text" : "유럽", "type" : "LCG_CONTINENT", "begin" : 0, "end" : 0, "weight" : 0.404473, "common_noun" : 0},
		{"id" : 1, "text" : "인도", "type" : "LCP_COUNTRY", "begin" : 2, "end" : 2, "weight" : 0.46984, "common_noun" : 0},
		{"id" : 2, "text" : "약 3분의 1", "type" : "QT_PERCENTAGE", "begin" : 8, "end" : 12, "weight" : 0.371469, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "유럽과", "head" : 1, "label" : "NP_CNJ", "mod" : [], "weight" : 0.722048 },
		{"id" : 1, "text" : "인도를", "head" : 2, "label" : "NP_OBJ", "mod" : [0], "weight" : 0.69489 },
		{"id" : 2, "text" : "오가는", "head" : 3, "label" : "VP_MOD", "mod" : [1], "weight" : 0.972378 },
		{"id" : 3, "text" : "항로를", "head" : 7, "label" : "NP_OBJ", "mod" : [2], "weight" : 0.777576 },
		{"id" : 4, "text" : "약", "head" : 6, "label" : "DP", "mod" : [], "weight" : 0.835341 },
		{"id" : 5, "text" : "3분의", "head" : 6, "label" : "NP_MOD", "mod" : [], "weight" : 0.747678 },
		{"id" : 6, "text" : "1", "head" : 7, "label" : "NP", "mod" : [4, 5], "weight" : 0.615225 },
		{"id" : 7, "text" : "단축한", "head" : 9, "label" : "VP_MOD", "mod" : [3, 6], "weight" : 0.964108 },
		{"id" : 8, "text" : "이", "head" : 9, "label" : "DP", "mod" : [], "weight" : 0.97846 },
		{"id" : 9, "text" : "수로는", "head" : 10, "label" : "NP_AJT", "mod" : [7, 8], "weight" : 0.725221 },
		{"id" : 10, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [9], "weight" : 0.0655848 }
	],
	"SRL" : [
		{"verb" : "오가", "sense" : 1, "word_id" : 2, "weight" : 0.224711,
			"argument" : [
				{"type" : "ARG2", "word_id" : 0, "text" : "유럽과", "weight" : 0.243027 },
				{"type" : "ARG2", "word_id" : 1, "text" : "인도를", "weight" : 0.206396 }
			] },
		{"verb" : "단축", "sense" : 1, "word_id" : 7, "weight" : 0.223729,
			"argument" : [
				{"type" : "ARG1", "word_id" : 3, "text" : "항로를", "weight" : 0.24619 },
				{"type" : "ARG0", "word_id" : 9, "text" : "수로는", "weight" : 0.201269 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 10, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.628239 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 1, "sent_id" : 0, "start_eid" : 1, "end_eid" : 7, "ne_id" : -1, "text" : "지중해와 홍해를 연결하는 세계 최대의 인공 수로", "start_eid_short" : 4, "end_eid_short" : 7, "text_short" : "세계 최대의 인공 수로", "weight" : 0.002 },
		{"id" : 14, "sent_id" : 1, "start_eid" : 8, "end_eid" : 9, "ne_id" : -1, "text" : "이 수", "start_eid_short" : 8, "end_eid_short" : 9, "text_short" : "이 수", "weight" : 0.006 }
	] }
 ]
}

