{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿1944년 이 사람은 조선건국동맹을 조직해 일본의 패망을 예견하고 해방을 준비했다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "XPN", "position" : 0, "weight" : 0 },
		{"id" : 1, "lemma" : "1944", "type" : "SN", "position" : 3, "weight" : 1 },
		{"id" : 2, "lemma" : "년", "type" : "NNB", "position" : 7, "weight" : 0.414343 },
		{"id" : 3, "lemma" : "이", "type" : "MM", "position" : 11, "weight" : 0.000222516 },
		{"id" : 4, "lemma" : "사람", "type" : "NNG", "position" : 15, "weight" : 0.737436 },
		{"id" : 5, "lemma" : "은", "type" : "JX", "position" : 21, "weight" : 0.0449928 },
		{"id" : 6, "lemma" : "조선", "type" : "NNG", "position" : 25, "weight" : 0.00516349 },
		{"id" : 7, "lemma" : "건국", "type" : "NNG", "position" : 31, "weight" : 0.184155 },
		{"id" : 8, "lemma" : "동맹", "type" : "NNG", "position" : 37, "weight" : 0.9 },
		{"id" : 9, "lemma" : "을", "type" : "JKO", "position" : 43, "weight" : 0.129611 },
		{"id" : 10, "lemma" : "조직", "type" : "NNG", "position" : 47, "weight" : 0.101381 },
		{"id" : 11, "lemma" : "하", "type" : "XSV", "position" : 53, "weight" : 0.0001 },
		{"id" : 12, "lemma" : "어", "type" : "EC", "position" : 53, "weight" : 0.361326 },
		{"id" : 13, "lemma" : "일본", "type" : "NNP", "position" : 57, "weight" : 0.0219717 },
		{"id" : 14, "lemma" : "의", "type" : "JKG", "position" : 63, "weight" : 0.0987295 },
		{"id" : 15, "lemma" : "패망", "type" : "NNG", "position" : 67, "weight" : 0.9 },
		{"id" : 16, "lemma" : "을", "type" : "JKO", "position" : 73, "weight" : 0.129611 },
		{"id" : 17, "lemma" : "예견", "type" : "NNG", "position" : 77, "weight" : 0.9 },
		{"id" : 18, "lemma" : "하", "type" : "XSV", "position" : 83, "weight" : 0.0001 },
		{"id" : 19, "lemma" : "고", "type" : "EC", "position" : 86, "weight" : 0.359917 },
		{"id" : 20, "lemma" : "해방", "type" : "NNG", "position" : 90, "weight" : 0.9 },
		{"id" : 21, "lemma" : "을", "type" : "JKO", "position" : 96, "weight" : 0.129611 },
		{"id" : 22, "lemma" : "준비", "type" : "NNG", "position" : 100, "weight" : 0.9 },
		{"id" : 23, "lemma" : "하", "type" : "XSV", "position" : 106, "weight" : 0.0001 },
		{"id" : 24, "lemma" : "었", "type" : "EP", "position" : 106, "weight" : 0.9 },
		{"id" : 25, "lemma" : "다", "type" : "EF", "position" : 109, "weight" : 0.640954 },
		{"id" : 26, "lemma" : ".", "type" : "SF", "position" : 112, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿/XPN+1944/SN+년/NNB", "target" : "﻿1944년", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "이/MM", "target" : "이", "word_id" : 1, "m_begin" : 3, "m_end" : 3},
		{"id" : 2, "result" : "사람/NNG+은/JX", "target" : "사람은", "word_id" : 2, "m_begin" : 4, "m_end" : 5},
		{"id" : 3, "result" : "조선건국동맹/NNG+을/JKO", "target" : "조선건국동맹을", "word_id" : 3, "m_begin" : 6, "m_end" : 9},
		{"id" : 4, "result" : "조직하/VV+어/EC", "target" : "조직해", "word_id" : 4, "m_begin" : 10, "m_end" : 12},
		{"id" : 5, "result" : "일본/NNG+의/JKG", "target" : "일본의", "word_id" : 5, "m_begin" : 13, "m_end" : 14},
		{"id" : 6, "result" : "패망/NNG+을/JKO", "target" : "패망을", "word_id" : 6, "m_begin" : 15, "m_end" : 16},
		{"id" : 7, "result" : "예견하/VV+고/EC", "target" : "예견하고", "word_id" : 7, "m_begin" : 17, "m_end" : 19},
		{"id" : 8, "result" : "해방/NNG+을/JKO", "target" : "해방을", "word_id" : 8, "m_begin" : 20, "m_end" : 21},
		{"id" : 9, "result" : "준비하/VV+었/EP+다/EF+./SF", "target" : "준비했다.", "word_id" : 9, "m_begin" : 22, "m_end" : 26}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "XPN", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "1944", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 7, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 11, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "사람", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 15, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 21, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "조선", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 25, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "건국동맹", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 31, "begin" : 7, "end" : 8},
		{"id" : 8, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 43, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "조직하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 47, "begin" : 10, "end" : 11},
		{"id" : 10, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 53, "begin" : 12, "end" : 12},
		{"id" : 11, "text" : "일본", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 57, "begin" : 13, "end" : 13},
		{"id" : 12, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 63, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "패망", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 67, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 73, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : "예견하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 77, "begin" : 17, "end" : 18},
		{"id" : 16, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 86, "begin" : 19, "end" : 19},
		{"id" : 17, "text" : "해방", "type" : "NNG", "scode" : "05", "weight" : 1, "position" : 90, "begin" : 20, "end" : 20},
		{"id" : 18, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 96, "begin" : 21, "end" : 21},
		{"id" : 19, "text" : "준비하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 100, "begin" : 22, "end" : 23},
		{"id" : 20, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 106, "begin" : 24, "end" : 24},
		{"id" : 21, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 109, "begin" : 25, "end" : 25},
		{"id" : 22, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 112, "begin" : 26, "end" : 26}
	],
	"word" : [
		{"id" : 0, "text" : "﻿1944년", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "이", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 2, "text" : "사람은", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 3, "text" : "조선건국동맹을", "type" : "", "begin" : 6, "end" : 9},
		{"id" : 4, "text" : "조직해", "type" : "", "begin" : 10, "end" : 12},
		{"id" : 5, "text" : "일본의", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 6, "text" : "패망을", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 7, "text" : "예견하고", "type" : "", "begin" : 17, "end" : 19},
		{"id" : 8, "text" : "해방을", "type" : "", "begin" : 20, "end" : 21},
		{"id" : 9, "text" : "준비했다.", "type" : "", "begin" : 22, "end" : 26}
	],
	"NE" : [
		{"id" : 0, "text" : "﻿1944년", "type" : "QT_ORDER", "begin" : 0, "end" : 2, "weight" : 0.20368, "common_noun" : 0},
		{"id" : 1, "text" : "조선건국동맹", "type" : "OGG_POLITICS", "begin" : 6, "end" : 8, "weight" : 0.377499, "common_noun" : 0},
		{"id" : 2, "text" : "일본", "type" : "LCP_COUNTRY", "begin" : 13, "end" : 13, "weight" : 0.54077, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿1944년", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.653059 },
		{"id" : 1, "text" : "이", "head" : 2, "label" : "DP", "mod" : [], "weight" : 0.981937 },
		{"id" : 2, "text" : "사람은", "head" : 4, "label" : "NP_SBJ", "mod" : [0, 1], "weight" : 0.698086 },
		{"id" : 3, "text" : "조선건국동맹을", "head" : 4, "label" : "NP_OBJ", "mod" : [], "weight" : 0.787814 },
		{"id" : 4, "text" : "조직해", "head" : 7, "label" : "VP", "mod" : [2, 3], "weight" : 0.856948 },
		{"id" : 5, "text" : "일본의", "head" : 6, "label" : "NP_MOD", "mod" : [], "weight" : 0.94938 },
		{"id" : 6, "text" : "패망을", "head" : 7, "label" : "NP_OBJ", "mod" : [5], "weight" : 0.869593 },
		{"id" : 7, "text" : "예견하고", "head" : 9, "label" : "VP", "mod" : [4, 6], "weight" : 0.676001 },
		{"id" : 8, "text" : "해방을", "head" : 9, "label" : "NP_OBJ", "mod" : [], "weight" : 0.523784 },
		{"id" : 9, "text" : "준비했다.", "head" : -1, "label" : "VP", "mod" : [7, 8], "weight" : 0.0503956 }
	],
	"SRL" : [
		{"verb" : "조직", "sense" : 1, "word_id" : 4, "weight" : 0.197736,
			"argument" : [
				{"type" : "ARG0", "word_id" : 2, "text" : "사람은", "weight" : 0.154533 },
				{"type" : "ARG1", "word_id" : 3, "text" : "조선건국동맹을", "weight" : 0.240939 }
			] },
		{"verb" : "예견", "sense" : 1, "word_id" : 7, "weight" : 0.354357,
			"argument" : [
				{"type" : "ARG1", "word_id" : 6, "text" : "패망을", "weight" : 0.354357 }
			] },
		{"verb" : "준비", "sense" : 1, "word_id" : 9, "weight" : 0.202334,
			"argument" : [
				{"type" : "ARGM-MNR", "word_id" : 4, "text" : "조직해", "weight" : 0.161903 },
				{"type" : "ARG1", "word_id" : 8, "text" : "해방을", "weight" : 0.242765 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 7, "ant_sid" : 0, "ant_wid" : 2, "type" : "s", "istitle" : 0, "weight" : 0.781996 },
		{"id" : 1, "verb_wid" : 9, "ant_sid" : 0, "ant_wid" : 2, "type" : "s", "istitle" : 0, "weight" : 0.709878 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "또한 해방 이후 건국준비위원회를 조직했으며 김규식과 함께 좌우합작운동을 이끌었는데 이 사람은 누구일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "또한", "type" : "MAG", "position" : 114, "weight" : 0.0653106 },
		{"id" : 1, "lemma" : "해방", "type" : "NNG", "position" : 121, "weight" : 0.9 },
		{"id" : 2, "lemma" : "이후", "type" : "NNG", "position" : 128, "weight" : 0.184814 },
		{"id" : 3, "lemma" : "건국", "type" : "NNG", "position" : 135, "weight" : 0.184155 },
		{"id" : 4, "lemma" : "준비", "type" : "NNG", "position" : 141, "weight" : 0.9 },
		{"id" : 5, "lemma" : "위원", "type" : "NNG", "position" : 147, "weight" : 0.184216 },
		{"id" : 6, "lemma" : "회", "type" : "XSN", "position" : 153, "weight" : 0.010766 },
		{"id" : 7, "lemma" : "를", "type" : "JKO", "position" : 156, "weight" : 0.0870227 },
		{"id" : 8, "lemma" : "조직", "type" : "NNG", "position" : 160, "weight" : 0.101381 },
		{"id" : 9, "lemma" : "하", "type" : "XSV", "position" : 166, "weight" : 0.0001 },
		{"id" : 10, "lemma" : "었", "type" : "EP", "position" : 166, "weight" : 0.9 },
		{"id" : 11, "lemma" : "으며", "type" : "EC", "position" : 169, "weight" : 0.197482 },
		{"id" : 12, "lemma" : "김규식", "type" : "NNP", "position" : 176, "weight" : 0.7 },
		{"id" : 13, "lemma" : "과", "type" : "JKB", "position" : 185, "weight" : 0.0270179 },
		{"id" : 14, "lemma" : "함께", "type" : "MAG", "position" : 189, "weight" : 0.9 },
		{"id" : 15, "lemma" : "좌우", "type" : "NNG", "position" : 196, "weight" : 0.9 },
		{"id" : 16, "lemma" : "합작", "type" : "NNG", "position" : 202, "weight" : 0.9 },
		{"id" : 17, "lemma" : "운동", "type" : "NNG", "position" : 208, "weight" : 0.9 },
		{"id" : 18, "lemma" : "을", "type" : "JKO", "position" : 214, "weight" : 0.129611 },
		{"id" : 19, "lemma" : "이끌", "type" : "VV", "position" : 218, "weight" : 0.9 },
		{"id" : 20, "lemma" : "었", "type" : "EP", "position" : 224, "weight" : 0.9 },
		{"id" : 21, "lemma" : "는데", "type" : "EC", "position" : 227, "weight" : 0.188154 },
		{"id" : 22, "lemma" : "이", "type" : "MM", "position" : 234, "weight" : 0.000947656 },
		{"id" : 23, "lemma" : "사람", "type" : "NNG", "position" : 238, "weight" : 0.737436 },
		{"id" : 24, "lemma" : "은", "type" : "JX", "position" : 244, "weight" : 0.0449928 },
		{"id" : 25, "lemma" : "누구", "type" : "NP", "position" : 248, "weight" : 0.9 },
		{"id" : 26, "lemma" : "이", "type" : "VCP", "position" : 254, "weight" : 0.0175768 },
		{"id" : 27, "lemma" : "ㄹ까", "type" : "EF", "position" : 254, "weight" : 0.258243 },
		{"id" : 28, "lemma" : "?", "type" : "SF", "position" : 260, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "또한/MAG", "target" : "또한", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "해방/NNG", "target" : "해방", "word_id" : 1, "m_begin" : 1, "m_end" : 1},
		{"id" : 2, "result" : "이후/NNG", "target" : "이후", "word_id" : 2, "m_begin" : 2, "m_end" : 2},
		{"id" : 3, "result" : "건국준비위원회/NNG+를/JKO", "target" : "건국준비위원회를", "word_id" : 3, "m_begin" : 3, "m_end" : 7},
		{"id" : 4, "result" : "조직하/VV+었/EP+으며/EC", "target" : "조직했으며", "word_id" : 4, "m_begin" : 8, "m_end" : 11},
		{"id" : 5, "result" : "김규식/NNG+과/JKB", "target" : "김규식과", "word_id" : 5, "m_begin" : 12, "m_end" : 13},
		{"id" : 6, "result" : "함께/MAG", "target" : "함께", "word_id" : 6, "m_begin" : 14, "m_end" : 14},
		{"id" : 7, "result" : "좌우합작운동/NNG+을/JKO", "target" : "좌우합작운동을", "word_id" : 7, "m_begin" : 15, "m_end" : 18},
		{"id" : 8, "result" : "이끌/VV+었/EP+는데/EC", "target" : "이끌었는데", "word_id" : 8, "m_begin" : 19, "m_end" : 21},
		{"id" : 9, "result" : "이/MM", "target" : "이", "word_id" : 9, "m_begin" : 22, "m_end" : 22},
		{"id" : 10, "result" : "사람/NNG+은/JX", "target" : "사람은", "word_id" : 10, "m_begin" : 23, "m_end" : 24},
		{"id" : 11, "result" : "누구/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "누구일까?", "word_id" : 11, "m_begin" : 25, "m_end" : 28}
	],
	"WSD" : [
		{"id" : 0, "text" : "또한", "type" : "MAG", "scode" : "00", "weight" : 1, "position" : 114, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "해방", "type" : "NNG", "scode" : "05", "weight" : 1, "position" : 121, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "이후", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 128, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "건국준비위원회", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 135, "begin" : 3, "end" : 6},
		{"id" : 4, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 156, "begin" : 7, "end" : 7},
		{"id" : 5, "text" : "조직하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 160, "begin" : 8, "end" : 9},
		{"id" : 6, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 166, "begin" : 10, "end" : 10},
		{"id" : 7, "text" : "으며", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 169, "begin" : 11, "end" : 11},
		{"id" : 8, "text" : "김규식", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 176, "begin" : 12, "end" : 12},
		{"id" : 9, "text" : "과", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 185, "begin" : 13, "end" : 13},
		{"id" : 10, "text" : "함께", "type" : "MAG", "scode" : "00", "weight" : 1, "position" : 189, "begin" : 14, "end" : 14},
		{"id" : 11, "text" : "좌우합작", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 196, "begin" : 15, "end" : 16},
		{"id" : 12, "text" : "운동", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 208, "begin" : 17, "end" : 17},
		{"id" : 13, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 214, "begin" : 18, "end" : 18},
		{"id" : 14, "text" : "이끌", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 218, "begin" : 19, "end" : 19},
		{"id" : 15, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 224, "begin" : 20, "end" : 20},
		{"id" : 16, "text" : "는데", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 227, "begin" : 21, "end" : 21},
		{"id" : 17, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 234, "begin" : 22, "end" : 22},
		{"id" : 18, "text" : "사람", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 238, "begin" : 23, "end" : 23},
		{"id" : 19, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 244, "begin" : 24, "end" : 24},
		{"id" : 20, "text" : "누구", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 248, "begin" : 25, "end" : 25},
		{"id" : 21, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 254, "begin" : 26, "end" : 26},
		{"id" : 22, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 254, "begin" : 27, "end" : 27},
		{"id" : 23, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 260, "begin" : 28, "end" : 28}
	],
	"word" : [
		{"id" : 0, "text" : "또한", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "해방", "type" : "", "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "이후", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "건국준비위원회를", "type" : "", "begin" : 3, "end" : 7},
		{"id" : 4, "text" : "조직했으며", "type" : "", "begin" : 8, "end" : 11},
		{"id" : 5, "text" : "김규식과", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 6, "text" : "함께", "type" : "", "begin" : 14, "end" : 14},
		{"id" : 7, "text" : "좌우합작운동을", "type" : "", "begin" : 15, "end" : 18},
		{"id" : 8, "text" : "이끌었는데", "type" : "", "begin" : 19, "end" : 21},
		{"id" : 9, "text" : "이", "type" : "", "begin" : 22, "end" : 22},
		{"id" : 10, "text" : "사람은", "type" : "", "begin" : 23, "end" : 24},
		{"id" : 11, "text" : "누구일까?", "type" : "", "begin" : 25, "end" : 28}
	],
	"NE" : [
		{"id" : 0, "text" : "건국준비위원회", "type" : "OGG_POLITICS", "begin" : 3, "end" : 6, "weight" : 0.934211, "common_noun" : 0},
		{"id" : 1, "text" : "김규식", "type" : "PS_NAME", "begin" : 12, "end" : 12, "weight" : 0.68434, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "또한", "head" : 4, "label" : "AP", "mod" : [], "weight" : 0.827666 },
		{"id" : 1, "text" : "해방", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.893598 },
		{"id" : 2, "text" : "이후", "head" : 4, "label" : "NP_AJT", "mod" : [1], "weight" : 0.803894 },
		{"id" : 3, "text" : "건국준비위원회를", "head" : 4, "label" : "NP_OBJ", "mod" : [], "weight" : 0.614883 },
		{"id" : 4, "text" : "조직했으며", "head" : 8, "label" : "VP", "mod" : [0, 2, 3], "weight" : 0.941106 },
		{"id" : 5, "text" : "김규식과", "head" : 6, "label" : "NP_AJT", "mod" : [], "weight" : 0.8485 },
		{"id" : 6, "text" : "함께", "head" : 8, "label" : "AP", "mod" : [5], "weight" : 0.692313 },
		{"id" : 7, "text" : "좌우합작운동을", "head" : 8, "label" : "NP_OBJ", "mod" : [], "weight" : 0.667389 },
		{"id" : 8, "text" : "이끌었는데", "head" : 11, "label" : "VP", "mod" : [4, 6, 7], "weight" : 0.885091 },
		{"id" : 9, "text" : "이", "head" : 10, "label" : "DP", "mod" : [], "weight" : 0.860789 },
		{"id" : 10, "text" : "사람은", "head" : 11, "label" : "NP_SBJ", "mod" : [9], "weight" : 0.729832 },
		{"id" : 11, "text" : "누구일까?", "head" : -1, "label" : "VNP", "mod" : [8, 10], "weight" : 0.0503154 }
	],
	"SRL" : [
		{"verb" : "조직", "sense" : 1, "word_id" : 4, "weight" : 0.290023,
			"argument" : [
				{"type" : "ARGM-DIS", "word_id" : 0, "text" : "또한", "weight" : 0.204396 },
				{"type" : "ARGM-TMP", "word_id" : 2, "text" : "이후", "weight" : 0.238435 },
				{"type" : "ARG1", "word_id" : 3, "text" : "건국준비위원회를", "weight" : 0.427237 }
			] },
		{"verb" : "이끌", "sense" : 1, "word_id" : 8, "weight" : 0.219751,
			"argument" : [
				{"type" : "ARGM-MNR", "word_id" : 6, "text" : "함께", "weight" : 0.186325 },
				{"type" : "ARG1", "word_id" : 7, "text" : "좌우합작운동을", "weight" : 0.253176 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 4, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.378677 },
		{"id" : 1, "verb_wid" : 8, "ant_sid" : 0, "ant_wid" : 2, "type" : "s", "istitle" : 0, "weight" : 0.136566 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 1, "sent_id" : 0, "start_eid" : 1, "end_eid" : 2, "ne_id" : -1, "text" : "이 사람", "start_eid_short" : 1, "end_eid_short" : 2, "text_short" : "이 사람", "weight" : 0 },
		{"id" : 12, "sent_id" : 1, "start_eid" : 9, "end_eid" : 10, "ne_id" : -1, "text" : "이 사람", "start_eid_short" : 9, "end_eid_short" : 10, "text_short" : "이 사람", "weight" : 0.006 },
		{"id" : 13, "sent_id" : 1, "start_eid" : 11, "end_eid" : 11, "ne_id" : -1, "text" : "누구", "start_eid_short" : 11, "end_eid_short" : 11, "text_short" : "누구이ㄹ까?", "weight" : 0.006 }
	] },
	{"id" : 1, "type" : "OGG_POLITICS", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 3, "sent_id" : 0, "start_eid" : 3, "end_eid" : 3, "ne_id" : 1, "text" : "조선건국동맹", "start_eid_short" : 3, "end_eid_short" : 3, "text_short" : "조선건국동맹", "weight" : 0.002 },
		{"id" : 9, "sent_id" : 1, "start_eid" : 3, "end_eid" : 3, "ne_id" : 0, "text" : "건국준비위원회", "start_eid_short" : 3, "end_eid_short" : 3, "text_short" : "건국준비위원회", "weight" : 0.003 }
	] },
	{"id" : 2, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 8, "sent_id" : 1, "start_eid" : 1, "end_eid" : 1, "ne_id" : -1, "text" : "해방", "start_eid_short" : 1, "end_eid_short" : 1, "text_short" : "해방", "weight" : 0.016 },
		{"id" : 6, "sent_id" : 0, "start_eid" : 8, "end_eid" : 8, "ne_id" : -1, "text" : "해방", "start_eid_short" : 8, "end_eid_short" : 8, "text_short" : "해방", "weight" : 0.01 }
	] }
 ]
}

