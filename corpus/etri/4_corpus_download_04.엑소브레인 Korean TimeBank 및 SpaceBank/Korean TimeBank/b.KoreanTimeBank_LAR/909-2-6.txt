{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "19세기 영국의 '토머스 칼라일'은 '인도를 다 주어도 결코 이 사람과 바꾸지 않겠다'고 말했다.",
	"morp" : [
		{"id" : 0, "lemma" : "19", "type" : "SN", "position" : 0, "weight" : 1 },
		{"id" : 1, "lemma" : "세기", "type" : "NNG", "position" : 2, "weight" : 0.100259 },
		{"id" : 2, "lemma" : "영국", "type" : "NNP", "position" : 9, "weight" : 0.9 },
		{"id" : 3, "lemma" : "의", "type" : "JKG", "position" : 15, "weight" : 0.0987295 },
		{"id" : 4, "lemma" : "'", "type" : "SS", "position" : 19, "weight" : 1 },
		{"id" : 5, "lemma" : "토머스", "type" : "NNP", "position" : 20, "weight" : 0.9 },
		{"id" : 6, "lemma" : "칼라일", "type" : "NNP", "position" : 30, "weight" : 0.85 },
		{"id" : 7, "lemma" : "'", "type" : "SS", "position" : 39, "weight" : 1 },
		{"id" : 8, "lemma" : "은", "type" : "JX", "position" : 40, "weight" : 0.0128817 },
		{"id" : 9, "lemma" : "'", "type" : "SS", "position" : 44, "weight" : 1 },
		{"id" : 10, "lemma" : "인도", "type" : "NNG", "position" : 45, "weight" : 0.025633 },
		{"id" : 11, "lemma" : "를", "type" : "JKO", "position" : 51, "weight" : 0.137686 },
		{"id" : 12, "lemma" : "다", "type" : "MAG", "position" : 55, "weight" : 0.00102922 },
		{"id" : 13, "lemma" : "주", "type" : "VV", "position" : 59, "weight" : 0.112354 },
		{"id" : 14, "lemma" : "어도", "type" : "EC", "position" : 62, "weight" : 0.430707 },
		{"id" : 15, "lemma" : "결코", "type" : "MAG", "position" : 69, "weight" : 0.9 },
		{"id" : 16, "lemma" : "이", "type" : "MM", "position" : 76, "weight" : 0.00146884 },
		{"id" : 17, "lemma" : "사람", "type" : "NNG", "position" : 80, "weight" : 0.737436 },
		{"id" : 18, "lemma" : "과", "type" : "JKB", "position" : 86, "weight" : 0.0503087 },
		{"id" : 19, "lemma" : "바꾸", "type" : "VV", "position" : 90, "weight" : 0.9 },
		{"id" : 20, "lemma" : "지", "type" : "EC", "position" : 96, "weight" : 0.301842 },
		{"id" : 21, "lemma" : "않", "type" : "VX", "position" : 100, "weight" : 0.256301 },
		{"id" : 22, "lemma" : "겠", "type" : "EP", "position" : 103, "weight" : 0.9 },
		{"id" : 23, "lemma" : "다", "type" : "EF", "position" : 106, "weight" : 0.640954 },
		{"id" : 24, "lemma" : "'", "type" : "SS", "position" : 109, "weight" : 1 },
		{"id" : 25, "lemma" : "고", "type" : "JKQ", "position" : 110, "weight" : 0.000541335 },
		{"id" : 26, "lemma" : "말", "type" : "NNG", "position" : 114, "weight" : 0.0618649 },
		{"id" : 27, "lemma" : "하", "type" : "XSV", "position" : 117, "weight" : 0.0001 },
		{"id" : 28, "lemma" : "었", "type" : "EP", "position" : 117, "weight" : 0.9 },
		{"id" : 29, "lemma" : "다", "type" : "EF", "position" : 120, "weight" : 0.640954 },
		{"id" : 30, "lemma" : ".", "type" : "SF", "position" : 123, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "19/SN+세기/NNG", "target" : "19세기", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "영국/NNG+의/JKG", "target" : "영국의", "word_id" : 1, "m_begin" : 2, "m_end" : 3},
		{"id" : 2, "result" : "'/SS+토머스/NNG", "target" : "'토머스", "word_id" : 2, "m_begin" : 4, "m_end" : 5},
		{"id" : 3, "result" : "칼라일/NNG+'/SS+은/JX", "target" : "칼라일'은", "word_id" : 3, "m_begin" : 6, "m_end" : 8},
		{"id" : 4, "result" : "'/SS+인도/NNG+를/JKO", "target" : "'인도를", "word_id" : 4, "m_begin" : 9, "m_end" : 11},
		{"id" : 5, "result" : "다/MAG", "target" : "다", "word_id" : 5, "m_begin" : 12, "m_end" : 12},
		{"id" : 6, "result" : "주/VV+어도/EC", "target" : "주어도", "word_id" : 6, "m_begin" : 13, "m_end" : 14},
		{"id" : 7, "result" : "결코/MAG", "target" : "결코", "word_id" : 7, "m_begin" : 15, "m_end" : 15},
		{"id" : 8, "result" : "이/MM", "target" : "이", "word_id" : 8, "m_begin" : 16, "m_end" : 16},
		{"id" : 9, "result" : "사람/NNG+과/JKB", "target" : "사람과", "word_id" : 9, "m_begin" : 17, "m_end" : 18},
		{"id" : 10, "result" : "바꾸/VV+지/EC", "target" : "바꾸지", "word_id" : 10, "m_begin" : 19, "m_end" : 20},
		{"id" : 11, "result" : "않/VX+겠/EP+다/EF+'/SS+고/JKQ", "target" : "않겠다'고", "word_id" : 11, "m_begin" : 21, "m_end" : 25},
		{"id" : 12, "result" : "말하/VV+었/EP+다/EF+./SF", "target" : "말했다.", "word_id" : 12, "m_begin" : 26, "m_end" : 30}
	],
	"WSD" : [
		{"id" : 0, "text" : "19", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "세기", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 2, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "영국", "type" : "NNP", "scode" : "01", "weight" : 1, "position" : 9, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 15, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 19, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "토머스", "type" : "NNP", "scode" : "99", "weight" : 1, "position" : 20, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "칼라일", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 30, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 39, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 44, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "인도", "type" : "NNG", "scode" : "05", "weight" : 1, "position" : 45, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 51, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "다", "type" : "MAG", "scode" : "03", "weight" : 1, "position" : 55, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "주", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 59, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "어도", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 62, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "결코", "type" : "MAG", "scode" : "00", "weight" : 1, "position" : 69, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 76, "begin" : 16, "end" : 16},
		{"id" : 17, "text" : "사람", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 80, "begin" : 17, "end" : 17},
		{"id" : 18, "text" : "과", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 86, "begin" : 18, "end" : 18},
		{"id" : 19, "text" : "바꾸", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 90, "begin" : 19, "end" : 19},
		{"id" : 20, "text" : "지", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 96, "begin" : 20, "end" : 20},
		{"id" : 21, "text" : "않", "type" : "VX", "scode" : "00", "weight" : 1, "position" : 100, "begin" : 21, "end" : 21},
		{"id" : 22, "text" : "겠", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 103, "begin" : 22, "end" : 22},
		{"id" : 23, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 106, "begin" : 23, "end" : 23},
		{"id" : 24, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 109, "begin" : 24, "end" : 24},
		{"id" : 25, "text" : "고", "type" : "JKQ", "scode" : "00", "weight" : 1, "position" : 110, "begin" : 25, "end" : 25},
		{"id" : 26, "text" : "말하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 114, "begin" : 26, "end" : 27},
		{"id" : 27, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 117, "begin" : 28, "end" : 28},
		{"id" : 28, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 120, "begin" : 29, "end" : 29},
		{"id" : 29, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 123, "begin" : 30, "end" : 30}
	],
	"word" : [
		{"id" : 0, "text" : "19세기", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "영국의", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 2, "text" : "'토머스", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 3, "text" : "칼라일'은", "type" : "", "begin" : 6, "end" : 8},
		{"id" : 4, "text" : "'인도를", "type" : "", "begin" : 9, "end" : 11},
		{"id" : 5, "text" : "다", "type" : "", "begin" : 12, "end" : 12},
		{"id" : 6, "text" : "주어도", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 7, "text" : "결코", "type" : "", "begin" : 15, "end" : 15},
		{"id" : 8, "text" : "이", "type" : "", "begin" : 16, "end" : 16},
		{"id" : 9, "text" : "사람과", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 10, "text" : "바꾸지", "type" : "", "begin" : 19, "end" : 20},
		{"id" : 11, "text" : "않겠다'고", "type" : "", "begin" : 21, "end" : 25},
		{"id" : 12, "text" : "말했다.", "type" : "", "begin" : 26, "end" : 30}
	],
	"NE" : [
		{"id" : 0, "text" : "19세기", "type" : "DT_OTHERS", "begin" : 0, "end" : 1, "weight" : 0.635985, "common_noun" : 0},
		{"id" : 1, "text" : "영국", "type" : "LCP_COUNTRY", "begin" : 2, "end" : 2, "weight" : 0.440466, "common_noun" : 0},
		{"id" : 2, "text" : "토머스 칼라일", "type" : "PS_NAME", "begin" : 5, "end" : 6, "weight" : 0.334384, "common_noun" : 0},
		{"id" : 3, "text" : "인도", "type" : "LCP_COUNTRY", "begin" : 10, "end" : 10, "weight" : 0.283115, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "19세기", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.660992 },
		{"id" : 1, "text" : "영국의", "head" : 3, "label" : "NP_MOD", "mod" : [0], "weight" : 0.874755 },
		{"id" : 2, "text" : "'토머스", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.726321 },
		{"id" : 3, "text" : "칼라일'은", "head" : 12, "label" : "NP_SBJ", "mod" : [1, 2], "weight" : 0.883729 },
		{"id" : 4, "text" : "'인도를", "head" : 6, "label" : "NP_OBJ", "mod" : [], "weight" : 0.774621 },
		{"id" : 5, "text" : "다", "head" : 6, "label" : "AP", "mod" : [], "weight" : 0.664146 },
		{"id" : 6, "text" : "주어도", "head" : 10, "label" : "VP", "mod" : [4, 5], "weight" : 0.869195 },
		{"id" : 7, "text" : "결코", "head" : 10, "label" : "AP", "mod" : [], "weight" : 0.956024 },
		{"id" : 8, "text" : "이", "head" : 9, "label" : "DP", "mod" : [], "weight" : 0.796675 },
		{"id" : 9, "text" : "사람과", "head" : 10, "label" : "NP_AJT", "mod" : [8], "weight" : 0.878763 },
		{"id" : 10, "text" : "바꾸지", "head" : 11, "label" : "VP", "mod" : [6, 7, 9], "weight" : 0.827631 },
		{"id" : 11, "text" : "않겠다'고", "head" : 12, "label" : "VP_CMP", "mod" : [10], "weight" : 0.47266 },
		{"id" : 12, "text" : "말했다.", "head" : -1, "label" : "VP", "mod" : [3, 11], "weight" : 0.0251178 }
	],
	"SRL" : [
		{"verb" : "주", "sense" : 1, "word_id" : 6, "weight" : 0.18734,
			"argument" : [
				{"type" : "ARG1", "word_id" : 4, "text" : "'인도를", "weight" : 0.217269 },
				{"type" : "ARGM-EXT", "word_id" : 5, "text" : "다", "weight" : 0.157411 }
			] },
		{"verb" : "바꾸", "sense" : 1, "word_id" : 10, "weight" : 0.219137,
			"argument" : [
				{"type" : "ARGM-CND", "word_id" : 6, "text" : "주어도", "weight" : 0.147588 },
				{"type" : "ARGM-ADV", "word_id" : 7, "text" : "결코", "weight" : 0.184874 },
				{"type" : "ARG2", "word_id" : 9, "text" : "사람과", "weight" : 0.159272 },
				{"type" : "ARGM-NEG", "word_id" : 11, "text" : "않겠다'고", "weight" : 0.384813 }
			] },
		{"verb" : "말", "sense" : 1, "word_id" : 12, "weight" : 0.195278,
			"argument" : [
				{"type" : "ARG0", "word_id" : 3, "text" : "칼라일'은", "weight" : 0.222382 },
				{"type" : "ARGM-NEG", "word_id" : 11, "text" : "않겠다'고", "weight" : 0.168175 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 6, "ant_sid" : 0, "ant_wid" : 3, "type" : "s", "istitle" : 0, "weight" : 0.656508 },
		{"id" : 1, "verb_wid" : 10, "ant_sid" : 0, "ant_wid" : 3, "type" : "s", "istitle" : 0, "weight" : 0.522243 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "영국인의 자존심이라고 일컬어지는 세계적인 작가인 이 사람은 누구일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "영국", "type" : "NNP", "position" : 124, "weight" : 0.9 },
		{"id" : 1, "lemma" : "인", "type" : "XSN", "position" : 130, "weight" : 0.00915919 },
		{"id" : 2, "lemma" : "의", "type" : "JKG", "position" : 133, "weight" : 0.120732 },
		{"id" : 3, "lemma" : "자존", "type" : "NNG", "position" : 137, "weight" : 0.9 },
		{"id" : 4, "lemma" : "심", "type" : "XSN", "position" : 143, "weight" : 0.00108485 },
		{"id" : 5, "lemma" : "이", "type" : "VCP", "position" : 146, "weight" : 0.0165001 },
		{"id" : 6, "lemma" : "라고", "type" : "EC", "position" : 149, "weight" : 0.204147 },
		{"id" : 7, "lemma" : "일컫", "type" : "VV", "position" : 156, "weight" : 0.9 },
		{"id" : 8, "lemma" : "어", "type" : "EC", "position" : 162, "weight" : 0.41831 },
		{"id" : 9, "lemma" : "지", "type" : "VX", "position" : 165, "weight" : 0.0539764 },
		{"id" : 10, "lemma" : "는", "type" : "ETM", "position" : 168, "weight" : 0.183966 },
		{"id" : 11, "lemma" : "세계", "type" : "NNG", "position" : 172, "weight" : 0.9 },
		{"id" : 12, "lemma" : "적", "type" : "XSN", "position" : 178, "weight" : 0.0168756 },
		{"id" : 13, "lemma" : "이", "type" : "VCP", "position" : 181, "weight" : 0.0165001 },
		{"id" : 14, "lemma" : "ㄴ", "type" : "ETM", "position" : 181, "weight" : 0.220712 },
		{"id" : 15, "lemma" : "작가", "type" : "NNG", "position" : 185, "weight" : 0.658693 },
		{"id" : 16, "lemma" : "이", "type" : "VCP", "position" : 191, "weight" : 0.0177525 },
		{"id" : 17, "lemma" : "ㄴ", "type" : "ETM", "position" : 191, "weight" : 0.220712 },
		{"id" : 18, "lemma" : "이", "type" : "MM", "position" : 195, "weight" : 0.00060084 },
		{"id" : 19, "lemma" : "사람", "type" : "NNG", "position" : 199, "weight" : 0.737436 },
		{"id" : 20, "lemma" : "은", "type" : "JX", "position" : 205, "weight" : 0.0449928 },
		{"id" : 21, "lemma" : "누구", "type" : "NP", "position" : 209, "weight" : 0.9 },
		{"id" : 22, "lemma" : "이", "type" : "VCP", "position" : 215, "weight" : 0.0175768 },
		{"id" : 23, "lemma" : "ㄹ까", "type" : "EF", "position" : 215, "weight" : 0.258243 },
		{"id" : 24, "lemma" : "?", "type" : "SF", "position" : 221, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "영국인/NNG+의/JKG", "target" : "영국인의", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "자존심/NNG+이/VCP+라고/EC", "target" : "자존심이라고", "word_id" : 1, "m_begin" : 3, "m_end" : 6},
		{"id" : 2, "result" : "일컫/VV+어/EC+지/VX+는/ETM", "target" : "일컬어지는", "word_id" : 2, "m_begin" : 7, "m_end" : 10},
		{"id" : 3, "result" : "세계적/NNG+이/VCP+ㄴ/ETM", "target" : "세계적인", "word_id" : 3, "m_begin" : 11, "m_end" : 14},
		{"id" : 4, "result" : "작가/NNG+이/VCP+ㄴ/ETM", "target" : "작가인", "word_id" : 4, "m_begin" : 15, "m_end" : 17},
		{"id" : 5, "result" : "이/MM", "target" : "이", "word_id" : 5, "m_begin" : 18, "m_end" : 18},
		{"id" : 6, "result" : "사람/NNG+은/JX", "target" : "사람은", "word_id" : 6, "m_begin" : 19, "m_end" : 20},
		{"id" : 7, "result" : "누구/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "누구일까?", "word_id" : 7, "m_begin" : 21, "m_end" : 24}
	],
	"WSD" : [
		{"id" : 0, "text" : "영국인", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 124, "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 133, "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "자존심", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 137, "begin" : 3, "end" : 4},
		{"id" : 3, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 146, "begin" : 5, "end" : 5},
		{"id" : 4, "text" : "라고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 149, "begin" : 6, "end" : 6},
		{"id" : 5, "text" : "일컫", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 156, "begin" : 7, "end" : 7},
		{"id" : 6, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 162, "begin" : 8, "end" : 8},
		{"id" : 7, "text" : "지", "type" : "VX", "scode" : "04", "weight" : 1, "position" : 165, "begin" : 9, "end" : 9},
		{"id" : 8, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 168, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "세계적", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 172, "begin" : 11, "end" : 12},
		{"id" : 10, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 181, "begin" : 13, "end" : 13},
		{"id" : 11, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 181, "begin" : 14, "end" : 14},
		{"id" : 12, "text" : "작가", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 185, "begin" : 15, "end" : 15},
		{"id" : 13, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 191, "begin" : 16, "end" : 16},
		{"id" : 14, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 191, "begin" : 17, "end" : 17},
		{"id" : 15, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 195, "begin" : 18, "end" : 18},
		{"id" : 16, "text" : "사람", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 199, "begin" : 19, "end" : 19},
		{"id" : 17, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 205, "begin" : 20, "end" : 20},
		{"id" : 18, "text" : "누구", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 209, "begin" : 21, "end" : 21},
		{"id" : 19, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 215, "begin" : 22, "end" : 22},
		{"id" : 20, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 215, "begin" : 23, "end" : 23},
		{"id" : 21, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 221, "begin" : 24, "end" : 24}
	],
	"word" : [
		{"id" : 0, "text" : "영국인의", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "자존심이라고", "type" : "", "begin" : 3, "end" : 6},
		{"id" : 2, "text" : "일컬어지는", "type" : "", "begin" : 7, "end" : 10},
		{"id" : 3, "text" : "세계적인", "type" : "", "begin" : 11, "end" : 14},
		{"id" : 4, "text" : "작가인", "type" : "", "begin" : 15, "end" : 17},
		{"id" : 5, "text" : "이", "type" : "", "begin" : 18, "end" : 18},
		{"id" : 6, "text" : "사람은", "type" : "", "begin" : 19, "end" : 20},
		{"id" : 7, "text" : "누구일까?", "type" : "", "begin" : 21, "end" : 24}
	],
	"NE" : [
		{"id" : 0, "text" : "영국인", "type" : "CV_TRIBE", "begin" : 0, "end" : 1, "weight" : 0.355919, "common_noun" : 0},
		{"id" : 1, "text" : "작가", "type" : "CV_OCCUPATION", "begin" : 15, "end" : 15, "weight" : 0.445076, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "영국인의", "head" : 1, "label" : "NP_MOD", "mod" : [], "weight" : 0.885716 },
		{"id" : 1, "text" : "자존심이라고", "head" : 2, "label" : "VNP", "mod" : [0], "weight" : 0.750734 },
		{"id" : 2, "text" : "일컬어지는", "head" : 4, "label" : "VP_MOD", "mod" : [1], "weight" : 0.674861 },
		{"id" : 3, "text" : "세계적인", "head" : 4, "label" : "VNP_MOD", "mod" : [], "weight" : 0.924771 },
		{"id" : 4, "text" : "작가인", "head" : 6, "label" : "VNP_MOD", "mod" : [2, 3], "weight" : 0.934747 },
		{"id" : 5, "text" : "이", "head" : 6, "label" : "DP", "mod" : [], "weight" : 0.932813 },
		{"id" : 6, "text" : "사람은", "head" : 7, "label" : "NP_SBJ", "mod" : [4, 5], "weight" : 0.7929 },
		{"id" : 7, "text" : "누구일까?", "head" : -1, "label" : "VNP", "mod" : [6], "weight" : 0.192473 }
	],
	"SRL" : [
		{"verb" : "일컫", "sense" : 1, "word_id" : 2, "weight" : 0.160046,
			"argument" : [
				{"type" : "ARG2", "word_id" : 1, "text" : "자존심이라고", "weight" : 0.169578 },
				{"type" : "ARG1", "word_id" : 4, "text" : "작가인", "weight" : 0.150513 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "PS_NAME", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 2, "sent_id" : 0, "start_eid" : 2, "end_eid" : 3, "ne_id" : 2, "text" : "'토머스 칼라일'", "start_eid_short" : 2, "end_eid_short" : 3, "text_short" : "'토머스 칼라일'", "weight" : 0.002 },
		{"id" : 4, "sent_id" : 0, "start_eid" : 8, "end_eid" : 9, "ne_id" : -1, "text" : "이 사람", "start_eid_short" : 8, "end_eid_short" : 9, "text_short" : "이 사람", "weight" : 0.006 },
		{"id" : 7, "sent_id" : 1, "start_eid" : 5, "end_eid" : 6, "ne_id" : -1, "text" : "이 사람", "start_eid_short" : 5, "end_eid_short" : 6, "text_short" : "이 사람", "weight" : 0.012 },
		{"id" : 9, "sent_id" : 1, "start_eid" : 7, "end_eid" : 7, "ne_id" : -1, "text" : "누구", "start_eid_short" : 7, "end_eid_short" : 7, "text_short" : "누구이ㄹ까?", "weight" : 0.012 }
	] }
 ]
}

