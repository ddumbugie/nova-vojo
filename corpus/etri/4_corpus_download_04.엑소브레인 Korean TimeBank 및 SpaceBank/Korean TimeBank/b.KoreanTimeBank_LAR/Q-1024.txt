{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "이들은 중세 시대 교회에서 여성 성악가를 쓸 수 없었기 때문에 생겨났다.",
	"morp" : [
		{"id" : 0, "lemma" : "이", "type" : "NP", "position" : 0, "weight" : 0.00185676 },
		{"id" : 1, "lemma" : "들", "type" : "XSN", "position" : 3, "weight" : 0.055005 },
		{"id" : 2, "lemma" : "은", "type" : "JX", "position" : 6, "weight" : 0.100393 },
		{"id" : 3, "lemma" : "중세", "type" : "NNG", "position" : 10, "weight" : 0.9 },
		{"id" : 4, "lemma" : "시대", "type" : "NNG", "position" : 17, "weight" : 0.9 },
		{"id" : 5, "lemma" : "교회", "type" : "NNG", "position" : 24, "weight" : 0.9 },
		{"id" : 6, "lemma" : "에서", "type" : "JKB", "position" : 30, "weight" : 0.153407 },
		{"id" : 7, "lemma" : "여성", "type" : "NNG", "position" : 37, "weight" : 0.207985 },
		{"id" : 8, "lemma" : "성악", "type" : "NNG", "position" : 44, "weight" : 0.182873 },
		{"id" : 9, "lemma" : "가", "type" : "XSN", "position" : 50, "weight" : 0.000115417 },
		{"id" : 10, "lemma" : "를", "type" : "JKO", "position" : 53, "weight" : 0.0870227 },
		{"id" : 11, "lemma" : "쓰", "type" : "VV", "position" : 57, "weight" : 0.772556 },
		{"id" : 12, "lemma" : "ㄹ", "type" : "ETM", "position" : 57, "weight" : 0.300746 },
		{"id" : 13, "lemma" : "수", "type" : "NNB", "position" : 61, "weight" : 0.215617 },
		{"id" : 14, "lemma" : "없", "type" : "VA", "position" : 65, "weight" : 0.101269 },
		{"id" : 15, "lemma" : "었", "type" : "EP", "position" : 68, "weight" : 0.9 },
		{"id" : 16, "lemma" : "기", "type" : "ETN", "position" : 71, "weight" : 0.0127802 },
		{"id" : 17, "lemma" : "때문", "type" : "NNB", "position" : 75, "weight" : 0.9 },
		{"id" : 18, "lemma" : "에", "type" : "JKB", "position" : 81, "weight" : 0.135559 },
		{"id" : 19, "lemma" : "생겨나", "type" : "VV", "position" : 85, "weight" : 0.9 },
		{"id" : 20, "lemma" : "았", "type" : "EP", "position" : 91, "weight" : 0.9 },
		{"id" : 21, "lemma" : "다", "type" : "EF", "position" : 94, "weight" : 0.640954 },
		{"id" : 22, "lemma" : ".", "type" : "SF", "position" : 97, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이/NP+들/XSN+은/JX", "target" : "이들은", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "중세/NNG", "target" : "중세", "word_id" : 1, "m_begin" : 3, "m_end" : 3},
		{"id" : 2, "result" : "시대/NNG", "target" : "시대", "word_id" : 2, "m_begin" : 4, "m_end" : 4},
		{"id" : 3, "result" : "교회/NNG+에서/JKB", "target" : "교회에서", "word_id" : 3, "m_begin" : 5, "m_end" : 6},
		{"id" : 4, "result" : "여성/NNG", "target" : "여성", "word_id" : 4, "m_begin" : 7, "m_end" : 7},
		{"id" : 5, "result" : "성악가/NNG+를/JKO", "target" : "성악가를", "word_id" : 5, "m_begin" : 8, "m_end" : 10},
		{"id" : 6, "result" : "쓰/VV+ㄹ/ETM", "target" : "쓸", "word_id" : 6, "m_begin" : 11, "m_end" : 12},
		{"id" : 7, "result" : "수/NNB", "target" : "수", "word_id" : 7, "m_begin" : 13, "m_end" : 13},
		{"id" : 8, "result" : "없/VA+었/EP+기/ETN", "target" : "없었기", "word_id" : 8, "m_begin" : 14, "m_end" : 16},
		{"id" : 9, "result" : "때문/NNB+에/JKB", "target" : "때문에", "word_id" : 9, "m_begin" : 17, "m_end" : 18},
		{"id" : 10, "result" : "생겨나/VV+었/EP+다/EF+./SF", "target" : "생겨났다.", "word_id" : 10, "m_begin" : 19, "m_end" : 22}
	],
	"WSD" : [
		{"id" : 0, "text" : "이", "type" : "NP", "scode" : "05", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "들", "type" : "XSN", "scode" : "09", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 6, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "중세", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 10, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "시대", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 17, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "교회", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 24, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 30, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "여성", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 37, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "성악가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 44, "begin" : 8, "end" : 9},
		{"id" : 9, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 53, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "쓰", "type" : "VV", "scode" : "03", "weight" : 1, "position" : 57, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "ㄹ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 57, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "수", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 61, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "없", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 65, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 68, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "기", "type" : "ETN", "scode" : "00", "weight" : 1, "position" : 71, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "때문", "type" : "NNB", "scode" : "00", "weight" : 1, "position" : 75, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 81, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "생겨나", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 85, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "았", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 91, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 94, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 97, "begin" : 22, "end" : 22}
	],
	"word" : [
		{"id" : 0, "text" : "이들은", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "중세", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 2, "text" : "시대", "type" : "", "begin" : 4, "end" : 4},
		{"id" : 3, "text" : "교회에서", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 4, "text" : "여성", "type" : "", "begin" : 7, "end" : 7},
		{"id" : 5, "text" : "성악가를", "type" : "", "begin" : 8, "end" : 10},
		{"id" : 6, "text" : "쓸", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 7, "text" : "수", "type" : "", "begin" : 13, "end" : 13},
		{"id" : 8, "text" : "없었기", "type" : "", "begin" : 14, "end" : 16},
		{"id" : 9, "text" : "때문에", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 10, "text" : "생겨났다.", "type" : "", "begin" : 19, "end" : 22}
	],
	"NE" : [
		{"id" : 0, "text" : "중세 시대", "type" : "DT_DYNASTY", "begin" : 3, "end" : 4, "weight" : 0.877504, "common_noun" : 0},
		{"id" : 1, "text" : "성악가", "type" : "CV_OCCUPATION", "begin" : 8, "end" : 9, "weight" : 0.560497, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이들은", "head" : 6, "label" : "NP_SBJ", "mod" : [], "weight" : 0.290939 },
		{"id" : 1, "text" : "중세", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.721066 },
		{"id" : 2, "text" : "시대", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.71004 },
		{"id" : 3, "text" : "교회에서", "head" : 6, "label" : "NP_AJT", "mod" : [2], "weight" : 0.820025 },
		{"id" : 4, "text" : "여성", "head" : 5, "label" : "NP", "mod" : [], "weight" : 0.728506 },
		{"id" : 5, "text" : "성악가를", "head" : 6, "label" : "NP_OBJ", "mod" : [4], "weight" : 0.812485 },
		{"id" : 6, "text" : "쓸", "head" : 7, "label" : "VP_MOD", "mod" : [0, 3, 5], "weight" : 0.779063 },
		{"id" : 7, "text" : "수", "head" : 8, "label" : "NP_SBJ", "mod" : [6], "weight" : 0.812294 },
		{"id" : 8, "text" : "없었기", "head" : 9, "label" : "VP", "mod" : [7], "weight" : 0.701665 },
		{"id" : 9, "text" : "때문에", "head" : 10, "label" : "NP_AJT", "mod" : [8], "weight" : 0.76546 },
		{"id" : 10, "text" : "생겨났다.", "head" : -1, "label" : "VP", "mod" : [9], "weight" : 0.017961 }
	],
	"SRL" : [
		{"verb" : "쓰", "sense" : 1, "word_id" : 6, "weight" : 0.428955,
			"argument" : [
				{"type" : "ARG0", "word_id" : 3, "text" : "교회에서", "weight" : 0.34927 },
				{"type" : "ARG1", "word_id" : 5, "text" : "성악가를", "weight" : 0.365038 },
				{"type" : "ARGM-NEG", "word_id" : 8, "text" : "없었기", "weight" : 0.572555 }
			] },
		{"verb" : "없", "sense" : 1, "word_id" : 8, "weight" : 0.311056,
			"argument" : [
				{"type" : "ARG1", "word_id" : 5, "text" : "성악가를", "weight" : 0.365657 },
				{"type" : "ARGM-MNR", "word_id" : 6, "text" : "쓸", "weight" : 0.256455 }
			] },
		{"verb" : "생겨나", "sense" : 1, "word_id" : 10, "weight" : 0.470328,
			"argument" : [
				{"type" : "ARG1", "word_id" : 0, "text" : "이들은", "weight" : 0.554928 },
				{"type" : "ARGM-CAU", "word_id" : 9, "text" : "때문에", "weight" : 0.385729 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 10, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.661752 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "여성과 비슷한 성질의 목소리를 가진 남성 성악가를 말하는 이 용어는 무엇일까?",
	"morp" : [
		{"id" : 0, "lemma" : "여성", "type" : "NNG", "position" : 98, "weight" : 0.43782 },
		{"id" : 1, "lemma" : "과", "type" : "JKB", "position" : 104, "weight" : 0.0503087 },
		{"id" : 2, "lemma" : "비슷하", "type" : "VA", "position" : 108, "weight" : 0.9 },
		{"id" : 3, "lemma" : "ㄴ", "type" : "ETM", "position" : 114, "weight" : 0.430446 },
		{"id" : 4, "lemma" : "성질", "type" : "NNG", "position" : 118, "weight" : 0.9 },
		{"id" : 5, "lemma" : "의", "type" : "JKG", "position" : 124, "weight" : 0.0694213 },
		{"id" : 6, "lemma" : "목소", "type" : "NNP", "position" : 128, "weight" : 0.6 },
		{"id" : 7, "lemma" : "리", "type" : "NNG", "position" : 134, "weight" : 0.0211918 },
		{"id" : 8, "lemma" : "를", "type" : "JKO", "position" : 137, "weight" : 0.137686 },
		{"id" : 9, "lemma" : "가지", "type" : "VV", "position" : 141, "weight" : 0.401357 },
		{"id" : 10, "lemma" : "ㄴ", "type" : "ETM", "position" : 144, "weight" : 0.304215 },
		{"id" : 11, "lemma" : "남성", "type" : "NNG", "position" : 148, "weight" : 0.657519 },
		{"id" : 12, "lemma" : "성악", "type" : "NNG", "position" : 155, "weight" : 0.182873 },
		{"id" : 13, "lemma" : "가", "type" : "XSN", "position" : 161, "weight" : 0.000115417 },
		{"id" : 14, "lemma" : "를", "type" : "JKO", "position" : 164, "weight" : 0.0870227 },
		{"id" : 15, "lemma" : "말", "type" : "NNG", "position" : 168, "weight" : 0.0814073 },
		{"id" : 16, "lemma" : "하", "type" : "XSV", "position" : 171, "weight" : 0.0001 },
		{"id" : 17, "lemma" : "는", "type" : "ETM", "position" : 174, "weight" : 0.238503 },
		{"id" : 18, "lemma" : "이", "type" : "MM", "position" : 178, "weight" : 0.00060084 },
		{"id" : 19, "lemma" : "용어", "type" : "NNG", "position" : 182, "weight" : 0.9 },
		{"id" : 20, "lemma" : "는", "type" : "JX", "position" : 188, "weight" : 0.0287565 },
		{"id" : 21, "lemma" : "무엇", "type" : "NP", "position" : 192, "weight" : 0.9 },
		{"id" : 22, "lemma" : "이", "type" : "VCP", "position" : 198, "weight" : 0.0175768 },
		{"id" : 23, "lemma" : "ㄹ까", "type" : "EF", "position" : 198, "weight" : 0.258243 },
		{"id" : 24, "lemma" : "?", "type" : "SF", "position" : 204, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "여성/NNG+과/JKB", "target" : "여성과", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "비슷하/VA+ㄴ/ETM", "target" : "비슷한", "word_id" : 1, "m_begin" : 2, "m_end" : 3},
		{"id" : 2, "result" : "성질/NNG+의/JKG", "target" : "성질의", "word_id" : 2, "m_begin" : 4, "m_end" : 5},
		{"id" : 3, "result" : "목소리/NNG+를/JKO", "target" : "목소리를", "word_id" : 3, "m_begin" : 6, "m_end" : 8},
		{"id" : 4, "result" : "가지/VV+ㄴ/ETM", "target" : "가진", "word_id" : 4, "m_begin" : 9, "m_end" : 10},
		{"id" : 5, "result" : "남성/NNG", "target" : "남성", "word_id" : 5, "m_begin" : 11, "m_end" : 11},
		{"id" : 6, "result" : "성악가/NNG+를/JKO", "target" : "성악가를", "word_id" : 6, "m_begin" : 12, "m_end" : 14},
		{"id" : 7, "result" : "말하/VV+는/ETM", "target" : "말하는", "word_id" : 7, "m_begin" : 15, "m_end" : 17},
		{"id" : 8, "result" : "이/MM", "target" : "이", "word_id" : 8, "m_begin" : 18, "m_end" : 18},
		{"id" : 9, "result" : "용어/NNG+는/JX", "target" : "용어는", "word_id" : 9, "m_begin" : 19, "m_end" : 20},
		{"id" : 10, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 10, "m_begin" : 21, "m_end" : 24}
	],
	"WSD" : [
		{"id" : 0, "text" : "여성", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 98, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "과", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 104, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "비슷하", "type" : "VA", "scode" : "02", "weight" : 1, "position" : 108, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 114, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "성질", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 118, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 124, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "목소리", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 128, "begin" : 6, "end" : 7},
		{"id" : 7, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 137, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "가지", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 141, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 144, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "남성", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 148, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "성악가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 155, "begin" : 12, "end" : 13},
		{"id" : 12, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 164, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "말하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 168, "begin" : 15, "end" : 16},
		{"id" : 14, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 174, "begin" : 17, "end" : 17},
		{"id" : 15, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 178, "begin" : 18, "end" : 18},
		{"id" : 16, "text" : "용어", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 182, "begin" : 19, "end" : 19},
		{"id" : 17, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 188, "begin" : 20, "end" : 20},
		{"id" : 18, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 192, "begin" : 21, "end" : 21},
		{"id" : 19, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 198, "begin" : 22, "end" : 22},
		{"id" : 20, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 198, "begin" : 23, "end" : 23},
		{"id" : 21, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 204, "begin" : 24, "end" : 24}
	],
	"word" : [
		{"id" : 0, "text" : "여성과", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "비슷한", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 2, "text" : "성질의", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 3, "text" : "목소리를", "type" : "", "begin" : 6, "end" : 8},
		{"id" : 4, "text" : "가진", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 5, "text" : "남성", "type" : "", "begin" : 11, "end" : 11},
		{"id" : 6, "text" : "성악가를", "type" : "", "begin" : 12, "end" : 14},
		{"id" : 7, "text" : "말하는", "type" : "", "begin" : 15, "end" : 17},
		{"id" : 8, "text" : "이", "type" : "", "begin" : 18, "end" : 18},
		{"id" : 9, "text" : "용어는", "type" : "", "begin" : 19, "end" : 20},
		{"id" : 10, "text" : "무엇일까?", "type" : "", "begin" : 21, "end" : 24}
	],
	"NE" : [
		{"id" : 0, "text" : "성악가", "type" : "CV_OCCUPATION", "begin" : 12, "end" : 13, "weight" : 0.722164, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "여성과", "head" : 1, "label" : "NP_AJT", "mod" : [], "weight" : 0.484368 },
		{"id" : 1, "text" : "비슷한", "head" : 2, "label" : "VP_MOD", "mod" : [0], "weight" : 0.219104 },
		{"id" : 2, "text" : "성질의", "head" : 3, "label" : "NP_MOD", "mod" : [1], "weight" : 0.37426 },
		{"id" : 3, "text" : "목소리를", "head" : 4, "label" : "NP_OBJ", "mod" : [2], "weight" : 0.571413 },
		{"id" : 4, "text" : "가진", "head" : 6, "label" : "VP_MOD", "mod" : [3], "weight" : 0.555373 },
		{"id" : 5, "text" : "남성", "head" : 6, "label" : "NP", "mod" : [], "weight" : 0.621192 },
		{"id" : 6, "text" : "성악가를", "head" : 7, "label" : "NP_OBJ", "mod" : [4, 5], "weight" : 0.55978 },
		{"id" : 7, "text" : "말하는", "head" : 9, "label" : "VP_MOD", "mod" : [6], "weight" : 0.791771 },
		{"id" : 8, "text" : "이", "head" : 9, "label" : "DP", "mod" : [], "weight" : 0.59192 },
		{"id" : 9, "text" : "용어는", "head" : 10, "label" : "NP_SBJ", "mod" : [7, 8], "weight" : 0.887581 },
		{"id" : 10, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [9], "weight" : 0.001604 }
	],
	"SRL" : [
		{"verb" : "비슷하", "sense" : 1, "word_id" : 1, "weight" : 0.498806,
			"argument" : [
				{"type" : "ARG2", "word_id" : 0, "text" : "여성과", "weight" : 0.52965 },
				{"type" : "ARG1", "word_id" : 2, "text" : "성질의", "weight" : 0.467962 }
			] },
		{"verb" : "가지", "sense" : 1, "word_id" : 4, "weight" : 0.496541,
			"argument" : [
				{"type" : "ARG1", "word_id" : 3, "text" : "목소리를", "weight" : 0.323963 },
				{"type" : "ARG0", "word_id" : 6, "text" : "성악가를", "weight" : 0.669118 }
			] },
		{"verb" : "말", "sense" : 1, "word_id" : 7, "weight" : 0.319003,
			"argument" : [
				{"type" : "ARG1", "word_id" : 6, "text" : "성악가를", "weight" : 0.387709 },
				{"type" : "ARG2", "word_id" : 9, "text" : "용어는", "weight" : 0.250297 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 1, "sent_id" : 0, "start_eid" : 1, "end_eid" : 3, "ne_id" : -1, "text" : "중세 시대 교회", "start_eid_short" : 1, "end_eid_short" : 3, "text_short" : "중세 시대 교회", "weight" : 0.002 },
		{"id" : 12, "sent_id" : 1, "start_eid" : 8, "end_eid" : 9, "ne_id" : -1, "text" : "이 용어", "start_eid_short" : 8, "end_eid_short" : 9, "text_short" : "이 용어", "weight" : 0.006 },
		{"id" : 13, "sent_id" : 1, "start_eid" : 10, "end_eid" : 10, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 10, "end_eid_short" : 10, "text_short" : "무엇이ㄹ까?", "weight" : 0.006 }
	] },
	{"id" : 1, "type" : "CV_OCCUPATION", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 3, "sent_id" : 0, "start_eid" : 4, "end_eid" : 5, "ne_id" : 1, "text" : "여성 성악가", "start_eid_short" : 4, "end_eid_short" : 5, "text_short" : "여성 성악가", "weight" : 0.005 },
		{"id" : 7, "sent_id" : 1, "start_eid" : 0, "end_eid" : 6, "ne_id" : 0, "text" : "여성과 비슷한 성질의 목소리를 가진 남성 성악가", "start_eid_short" : 5, "end_eid_short" : 6, "text_short" : "남성 성악가", "weight" : 0.0065 }
	] },
	{"id" : 2, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 4, "sent_id" : 0, "start_eid" : 4, "end_eid" : 4, "ne_id" : -1, "text" : "여성", "start_eid_short" : 4, "end_eid_short" : 4, "text_short" : "여성", "weight" : 0.01 },
		{"id" : 9, "sent_id" : 1, "start_eid" : 0, "end_eid" : 0, "ne_id" : -1, "text" : "여성", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "여성", "weight" : 0.016 }
	] }
 ]
}
