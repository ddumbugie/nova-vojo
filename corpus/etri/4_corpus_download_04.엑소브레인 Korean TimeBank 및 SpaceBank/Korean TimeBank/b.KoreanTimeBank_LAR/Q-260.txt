{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿필립K.딕은 20세기를 대표하는 SF작가중 한명으로 그의 작품은 영화로 많이 만들어 졌다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿필립", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "K", "type" : "SL", "position" : 9, "weight" : 1 },
		{"id" : 2, "lemma" : ".", "type" : "SF", "position" : 10, "weight" : 1 },
		{"id" : 3, "lemma" : "딕", "type" : "NNP", "position" : 11, "weight" : 0.9 },
		{"id" : 4, "lemma" : "은", "type" : "JX", "position" : 14, "weight" : 0.0520511 },
		{"id" : 5, "lemma" : "20", "type" : "SN", "position" : 18, "weight" : 1 },
		{"id" : 6, "lemma" : "세기", "type" : "NNG", "position" : 20, "weight" : 0.100259 },
		{"id" : 7, "lemma" : "를", "type" : "JKO", "position" : 26, "weight" : 0.137686 },
		{"id" : 8, "lemma" : "대표", "type" : "NNG", "position" : 30, "weight" : 0.9 },
		{"id" : 9, "lemma" : "하", "type" : "XSV", "position" : 36, "weight" : 0.0001 },
		{"id" : 10, "lemma" : "는", "type" : "ETM", "position" : 39, "weight" : 0.238503 },
		{"id" : 11, "lemma" : "SF", "type" : "SL", "position" : 43, "weight" : 1 },
		{"id" : 12, "lemma" : "작가", "type" : "NNG", "position" : 45, "weight" : 0.173404 },
		{"id" : 13, "lemma" : "중", "type" : "NNB", "position" : 51, "weight" : 0.013531 },
		{"id" : 14, "lemma" : "한", "type" : "MM", "position" : 55, "weight" : 0.00485068 },
		{"id" : 15, "lemma" : "명", "type" : "NNB", "position" : 58, "weight" : 0.173745 },
		{"id" : 16, "lemma" : "으로", "type" : "JKB", "position" : 61, "weight" : 0.135596 },
		{"id" : 17, "lemma" : "그", "type" : "NP", "position" : 68, "weight" : 0.00637967 },
		{"id" : 18, "lemma" : "의", "type" : "JKG", "position" : 71, "weight" : 0.134696 },
		{"id" : 19, "lemma" : "작품", "type" : "NNG", "position" : 75, "weight" : 0.871348 },
		{"id" : 20, "lemma" : "은", "type" : "JX", "position" : 81, "weight" : 0.0449928 },
		{"id" : 21, "lemma" : "영화", "type" : "NNG", "position" : 85, "weight" : 0.322326 },
		{"id" : 22, "lemma" : "로", "type" : "JKB", "position" : 91, "weight" : 0.153229 },
		{"id" : 23, "lemma" : "많이", "type" : "MAG", "position" : 95, "weight" : 0.9 },
		{"id" : 24, "lemma" : "만들", "type" : "VV", "position" : 102, "weight" : 0.9 },
		{"id" : 25, "lemma" : "어", "type" : "EC", "position" : 108, "weight" : 0.41831 },
		{"id" : 26, "lemma" : "지", "type" : "VX", "position" : 112, "weight" : 0.0539764 },
		{"id" : 27, "lemma" : "었", "type" : "EP", "position" : 112, "weight" : 0.9 },
		{"id" : 28, "lemma" : "다", "type" : "EF", "position" : 115, "weight" : 0.640954 },
		{"id" : 29, "lemma" : ".", "type" : "SF", "position" : 118, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿필립/NNG+K/SL+./SF+딕/NNG+은/JX", "target" : "﻿필립K.딕은", "word_id" : 0, "m_begin" : 0, "m_end" : 4},
		{"id" : 1, "result" : "20/SN+세기/NNG+를/JKO", "target" : "20세기를", "word_id" : 1, "m_begin" : 5, "m_end" : 7},
		{"id" : 2, "result" : "대표하/VV+는/ETM", "target" : "대표하는", "word_id" : 2, "m_begin" : 8, "m_end" : 10},
		{"id" : 3, "result" : "SF/SL+작가/NNG+중/NNB", "target" : "SF작가중", "word_id" : 3, "m_begin" : 11, "m_end" : 13},
		{"id" : 4, "result" : "한/MM+명/NNB+으로/JKB", "target" : "한명으로", "word_id" : 4, "m_begin" : 14, "m_end" : 16},
		{"id" : 5, "result" : "그/NP+의/JKG", "target" : "그의", "word_id" : 5, "m_begin" : 17, "m_end" : 18},
		{"id" : 6, "result" : "작품/NNG+은/JX", "target" : "작품은", "word_id" : 6, "m_begin" : 19, "m_end" : 20},
		{"id" : 7, "result" : "영화/NNG+로/JKB", "target" : "영화로", "word_id" : 7, "m_begin" : 21, "m_end" : 22},
		{"id" : 8, "result" : "많이/MAG", "target" : "많이", "word_id" : 8, "m_begin" : 23, "m_end" : 23},
		{"id" : 9, "result" : "만들/VV+어/EC", "target" : "만들어", "word_id" : 9, "m_begin" : 24, "m_end" : 25},
		{"id" : 10, "result" : "지/VX+었/EP+다/EF+./SF", "target" : "졌다.", "word_id" : 10, "m_begin" : 26, "m_end" : 29}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿필립", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "K", "type" : "SL", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "딕", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 11, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 14, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "20", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 18, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "세기", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 20, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 26, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "대표하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 30, "begin" : 8, "end" : 9},
		{"id" : 9, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 39, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "SF", "type" : "SL", "scode" : "00", "weight" : 1, "position" : 43, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "작가", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 45, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "중", "type" : "NNB", "scode" : "04", "weight" : 1, "position" : 51, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "한", "type" : "MM", "scode" : "01", "weight" : 1, "position" : 55, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "명", "type" : "NNB", "scode" : "03", "weight" : 1, "position" : 58, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 61, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "그", "type" : "NP", "scode" : "01", "weight" : 1, "position" : 68, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 71, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "작품", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 75, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 81, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "영화", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 85, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 91, "begin" : 22, "end" : 22},
		{"id" : 22, "text" : "많이", "type" : "MAG", "scode" : "00", "weight" : 1, "position" : 95, "begin" : 23, "end" : 23},
		{"id" : 23, "text" : "만들", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 102, "begin" : 24, "end" : 24},
		{"id" : 24, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 108, "begin" : 25, "end" : 25},
		{"id" : 25, "text" : "지", "type" : "VX", "scode" : "04", "weight" : 1, "position" : 112, "begin" : 26, "end" : 26},
		{"id" : 26, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 112, "begin" : 27, "end" : 27},
		{"id" : 27, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 115, "begin" : 28, "end" : 28},
		{"id" : 28, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 118, "begin" : 29, "end" : 29}
	],
	"word" : [
		{"id" : 0, "text" : "﻿필립K.딕은", "type" : "", "begin" : 0, "end" : 4},
		{"id" : 1, "text" : "20세기를", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 2, "text" : "대표하는", "type" : "", "begin" : 8, "end" : 10},
		{"id" : 3, "text" : "SF작가중", "type" : "", "begin" : 11, "end" : 13},
		{"id" : 4, "text" : "한명으로", "type" : "", "begin" : 14, "end" : 16},
		{"id" : 5, "text" : "그의", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 6, "text" : "작품은", "type" : "", "begin" : 19, "end" : 20},
		{"id" : 7, "text" : "영화로", "type" : "", "begin" : 21, "end" : 22},
		{"id" : 8, "text" : "많이", "type" : "", "begin" : 23, "end" : 23},
		{"id" : 9, "text" : "만들어", "type" : "", "begin" : 24, "end" : 25},
		{"id" : 10, "text" : "졌다.", "type" : "", "begin" : 26, "end" : 29}
	],
	"NE" : [
		{"id" : 0, "text" : "﻿필립K.딕", "type" : "PS_NAME", "begin" : 0, "end" : 3, "weight" : 0.110095, "common_noun" : 0},
		{"id" : 1, "text" : "20세기", "type" : "DT_OTHERS", "begin" : 5, "end" : 6, "weight" : 0.490766, "common_noun" : 0},
		{"id" : 2, "text" : "SF작가", "type" : "CV_OCCUPATION", "begin" : 11, "end" : 12, "weight" : 0.47687, "common_noun" : 0},
		{"id" : 3, "text" : "한명", "type" : "QT_MAN_COUNT", "begin" : 14, "end" : 15, "weight" : 0.853921, "common_noun" : 0},
		{"id" : 4, "text" : "영화", "type" : "FD_ART", "begin" : 21, "end" : 21, "weight" : 0.339083, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿필립K.딕은", "head" : 4, "label" : "NP_SBJ", "mod" : [], "weight" : 0.654009 },
		{"id" : 1, "text" : "20세기를", "head" : 2, "label" : "NP_OBJ", "mod" : [], "weight" : 0.979978 },
		{"id" : 2, "text" : "대표하는", "head" : 4, "label" : "VP_MOD", "mod" : [1], "weight" : 0.955379 },
		{"id" : 3, "text" : "SF작가중", "head" : 4, "label" : "NP", "mod" : [], "weight" : 0.799008 },
		{"id" : 4, "text" : "한명으로", "head" : 9, "label" : "NP_AJT", "mod" : [0, 2, 3], "weight" : 0.823566 },
		{"id" : 5, "text" : "그의", "head" : 6, "label" : "NP_MOD", "mod" : [], "weight" : 0.97273 },
		{"id" : 6, "text" : "작품은", "head" : 9, "label" : "NP_SBJ", "mod" : [5], "weight" : 0.952165 },
		{"id" : 7, "text" : "영화로", "head" : 9, "label" : "NP_AJT", "mod" : [], "weight" : 0.635162 },
		{"id" : 8, "text" : "많이", "head" : 9, "label" : "AP", "mod" : [], "weight" : 0.553471 },
		{"id" : 9, "text" : "만들어", "head" : 10, "label" : "VP", "mod" : [4, 6, 7, 8], "weight" : 0.477408 },
		{"id" : 10, "text" : "졌다.", "head" : -1, "label" : "VP", "mod" : [9], "weight" : 0.0479152 }
	],
	"SRL" : [
		{"verb" : "대표", "sense" : 1, "word_id" : 2, "weight" : 0.226556,
			"argument" : [
				{"type" : "ARG0", "word_id" : 0, "text" : "﻿필립K.딕은", "weight" : 0.263662 },
				{"type" : "ARG1", "word_id" : 1, "text" : "20세기를", "weight" : 0.189451 }
			] },
		{"verb" : "만들", "sense" : 1, "word_id" : 9, "weight" : 0.214927,
			"argument" : [
				{"type" : "ARGM-PRD", "word_id" : 4, "text" : "한명으로", "weight" : 0.149192 },
				{"type" : "ARG1", "word_id" : 6, "text" : "작품은", "weight" : 0.138772 },
				{"type" : "ARG2", "word_id" : 7, "text" : "영화로", "weight" : 0.0805967 },
				{"type" : "ARGM-EXT", "word_id" : 8, "text" : "많이", "weight" : 0.238295 },
				{"type" : "AUX", "word_id" : 10, "text" : "졌다.", "weight" : 0.467778 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "필립K.딕의 작품이 원작이 아닌 것은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "필립", "type" : "NNP", "position" : 120, "weight" : 0.9 },
		{"id" : 1, "lemma" : "K", "type" : "SL", "position" : 126, "weight" : 1 },
		{"id" : 2, "lemma" : ".", "type" : "SF", "position" : 127, "weight" : 1 },
		{"id" : 3, "lemma" : "딕", "type" : "NNP", "position" : 128, "weight" : 0.9 },
		{"id" : 4, "lemma" : "의", "type" : "JKG", "position" : 131, "weight" : 0.0987295 },
		{"id" : 5, "lemma" : "작품", "type" : "NNG", "position" : 135, "weight" : 0.871348 },
		{"id" : 6, "lemma" : "이", "type" : "JKS", "position" : 141, "weight" : 0.0360723 },
		{"id" : 7, "lemma" : "원작", "type" : "NNG", "position" : 145, "weight" : 0.9 },
		{"id" : 8, "lemma" : "이", "type" : "JKC", "position" : 151, "weight" : 0.000287945 },
		{"id" : 9, "lemma" : "아니", "type" : "VCN", "position" : 155, "weight" : 0.354175 },
		{"id" : 10, "lemma" : "ㄴ", "type" : "ETM", "position" : 158, "weight" : 0.144018 },
		{"id" : 11, "lemma" : "것", "type" : "NNB", "position" : 162, "weight" : 0.228788 },
		{"id" : 12, "lemma" : "은", "type" : "JX", "position" : 165, "weight" : 0.0688243 },
		{"id" : 13, "lemma" : "무엇", "type" : "NP", "position" : 169, "weight" : 0.9 },
		{"id" : 14, "lemma" : "이", "type" : "VCP", "position" : 175, "weight" : 0.0175768 },
		{"id" : 15, "lemma" : "ㄹ까", "type" : "EF", "position" : 175, "weight" : 0.258243 },
		{"id" : 16, "lemma" : "?", "type" : "SF", "position" : 181, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "필립/NNG+K/SL+./SF+딕/NNG+의/JKG", "target" : "필립K.딕의", "word_id" : 0, "m_begin" : 0, "m_end" : 4},
		{"id" : 1, "result" : "작품/NNG+이/JKS", "target" : "작품이", "word_id" : 1, "m_begin" : 5, "m_end" : 6},
		{"id" : 2, "result" : "원작/NNG+이/JKC", "target" : "원작이", "word_id" : 2, "m_begin" : 7, "m_end" : 8},
		{"id" : 3, "result" : "아니/VCN+ㄴ/ETM", "target" : "아닌", "word_id" : 3, "m_begin" : 9, "m_end" : 10},
		{"id" : 4, "result" : "것/NNB+은/JX", "target" : "것은", "word_id" : 4, "m_begin" : 11, "m_end" : 12},
		{"id" : 5, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 5, "m_begin" : 13, "m_end" : 16}
	],
	"WSD" : [
		{"id" : 0, "text" : "필립", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 120, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "K", "type" : "SL", "scode" : "00", "weight" : 1, "position" : 126, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 127, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "딕", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 128, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 131, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "작품", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 135, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 141, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "원작", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 145, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "이", "type" : "JKC", "scode" : "00", "weight" : 1, "position" : 151, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "아니", "type" : "VCN", "scode" : "00", "weight" : 1, "position" : 155, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 158, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "것", "type" : "NNB", "scode" : "01", "weight" : 1, "position" : 162, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 165, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 169, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 175, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 175, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 181, "begin" : 16, "end" : 16}
	],
	"word" : [
		{"id" : 0, "text" : "필립K.딕의", "type" : "", "begin" : 0, "end" : 4},
		{"id" : 1, "text" : "작품이", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 2, "text" : "원작이", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 3, "text" : "아닌", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 4, "text" : "것은", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 5, "text" : "무엇일까?", "type" : "", "begin" : 13, "end" : 16}
	],
	"NE" : [
		{"id" : 0, "text" : "필립K.딕", "type" : "PS_NAME", "begin" : 0, "end" : 3, "weight" : 0.43445, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "필립K.딕의", "head" : 1, "label" : "NP_MOD", "mod" : [], "weight" : 0.770034 },
		{"id" : 1, "text" : "작품이", "head" : 3, "label" : "NP_SBJ", "mod" : [0], "weight" : 0.793541 },
		{"id" : 2, "text" : "원작이", "head" : 3, "label" : "NP_CMP", "mod" : [], "weight" : 0.78126 },
		{"id" : 3, "text" : "아닌", "head" : 4, "label" : "VP_MOD", "mod" : [1, 2], "weight" : 0.810586 },
		{"id" : 4, "text" : "것은", "head" : 5, "label" : "NP_SBJ", "mod" : [3], "weight" : 0.638354 },
		{"id" : 5, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [4], "weight" : 0.160386 }
	],
	"SRL" : [
		{"verb" : "아니", "sense" : 1, "word_id" : 3, "weight" : 0.203709,
			"argument" : [
				{"type" : "ARG1", "word_id" : 1, "text" : "작품이", "weight" : 0.199311 },
				{"type" : "ARG2", "word_id" : 2, "text" : "원작이", "weight" : 0.303604 },
				{"type" : "ARG1", "word_id" : 4, "text" : "것은", "weight" : 0.108212 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "CV_OCCUPATION", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 1, "sent_id" : 0, "start_eid" : 0, "end_eid" : 0, "ne_id" : 0, "text" : "﻿필립K.딕", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "﻿필립K.딕", "weight" : 0.004 },
		{"id" : 3, "sent_id" : 0, "start_eid" : 3, "end_eid" : 3, "ne_id" : 2, "text" : "SF작가중", "start_eid_short" : 3, "end_eid_short" : 3, "text_short" : "SF작가중", "weight" : 0.009 }
	] },
	{"id" : 1, "type" : "FD_ART", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 4, "sent_id" : 0, "start_eid" : 5, "end_eid" : 6, "ne_id" : -1, "text" : "그의 작품", "start_eid_short" : 5, "end_eid_short" : 6, "text_short" : "그의 작품", "weight" : 0.012 },
		{"id" : 6, "sent_id" : 0, "start_eid" : 7, "end_eid" : 7, "ne_id" : 4, "text" : "영화", "start_eid_short" : 7, "end_eid_short" : 7, "text_short" : "영화", "weight" : 0.002 },
		{"id" : 7, "sent_id" : 1, "start_eid" : 0, "end_eid" : 1, "ne_id" : -1, "text" : "필립K.딕의 작품", "start_eid_short" : 0, "end_eid_short" : 1, "text_short" : "필립K.딕의 작품", "weight" : 0.016 },
		{"id" : 10, "sent_id" : 1, "start_eid" : 4, "end_eid" : 5, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 5, "end_eid_short" : 5, "text_short" : "무엇이ㄹ까?", "weight" : 0.012 }
	] }
 ]
}

