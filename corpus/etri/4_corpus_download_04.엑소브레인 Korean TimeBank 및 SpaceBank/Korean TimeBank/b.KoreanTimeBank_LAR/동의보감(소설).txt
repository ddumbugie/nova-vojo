{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "동의보감(東醫寶鑑)은 작가 이은성이 1990년에 집필한 책으로, 중학교 1학년 국어 교과서에 일부 수록 되어있다.",
	"morp" : [
		{"id" : 0, "lemma" : "동의", "type" : "NNG", "position" : 0, "weight" : 0.9 },
		{"id" : 1, "lemma" : "보감", "type" : "NNG", "position" : 6, "weight" : 0.4 },
		{"id" : 2, "lemma" : "(", "type" : "SS", "position" : 12, "weight" : 1 },
		{"id" : 3, "lemma" : "東醫寶鑑", "type" : "SH", "position" : 13, "weight" : 1 },
		{"id" : 4, "lemma" : ")", "type" : "SS", "position" : 25, "weight" : 1 },
		{"id" : 5, "lemma" : "은", "type" : "JX", "position" : 26, "weight" : 0.0128817 },
		{"id" : 6, "lemma" : "작가", "type" : "NNG", "position" : 30, "weight" : 0.324849 },
		{"id" : 7, "lemma" : "이은성", "type" : "NNP", "position" : 37, "weight" : 0.3 },
		{"id" : 8, "lemma" : "이", "type" : "JKS", "position" : 46, "weight" : 0.0234517 },
		{"id" : 9, "lemma" : "1990", "type" : "SN", "position" : 50, "weight" : 1 },
		{"id" : 10, "lemma" : "년", "type" : "NNB", "position" : 54, "weight" : 0.414343 },
		{"id" : 11, "lemma" : "에", "type" : "JKB", "position" : 57, "weight" : 0.135559 },
		{"id" : 12, "lemma" : "집필", "type" : "NNG", "position" : 61, "weight" : 0.9 },
		{"id" : 13, "lemma" : "하", "type" : "XSV", "position" : 67, "weight" : 0.0001 },
		{"id" : 14, "lemma" : "ㄴ", "type" : "ETM", "position" : 67, "weight" : 0.392321 },
		{"id" : 15, "lemma" : "책", "type" : "NNG", "position" : 71, "weight" : 0.642125 },
		{"id" : 16, "lemma" : "으로", "type" : "JKB", "position" : 74, "weight" : 0.153406 },
		{"id" : 17, "lemma" : ",", "type" : "SP", "position" : 80, "weight" : 1 },
		{"id" : 18, "lemma" : "중", "type" : "NNG", "position" : 82, "weight" : 0.0650981 },
		{"id" : 19, "lemma" : "학교", "type" : "NNG", "position" : 85, "weight" : 0.184769 },
		{"id" : 20, "lemma" : "1", "type" : "SN", "position" : 92, "weight" : 1 },
		{"id" : 21, "lemma" : "학년", "type" : "NNG", "position" : 93, "weight" : 0.9 },
		{"id" : 22, "lemma" : "국어", "type" : "NNG", "position" : 100, "weight" : 0.179559 },
		{"id" : 23, "lemma" : "교과", "type" : "NNG", "position" : 107, "weight" : 0.9 },
		{"id" : 24, "lemma" : "서", "type" : "NNG", "position" : 113, "weight" : 0.00374413 },
		{"id" : 25, "lemma" : "에", "type" : "JKB", "position" : 116, "weight" : 0.153364 },
		{"id" : 26, "lemma" : "일부", "type" : "NNG", "position" : 120, "weight" : 0.208008 },
		{"id" : 27, "lemma" : "수록", "type" : "NNG", "position" : 127, "weight" : 0.182236 },
		{"id" : 28, "lemma" : "되", "type" : "VV", "position" : 134, "weight" : 0.0139267 },
		{"id" : 29, "lemma" : "어", "type" : "EC", "position" : 137, "weight" : 0.41831 },
		{"id" : 30, "lemma" : "있", "type" : "VX", "position" : 140, "weight" : 0.125953 },
		{"id" : 31, "lemma" : "다", "type" : "EF", "position" : 143, "weight" : 0.180366 },
		{"id" : 32, "lemma" : ".", "type" : "SF", "position" : 146, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "동의보감/NNG+(/SS+東醫寶鑑/SH+)/SS+은/JX", "target" : "동의보감(東醫寶鑑)은", "word_id" : 0, "m_begin" : 0, "m_end" : 5},
		{"id" : 1, "result" : "작가/NNG", "target" : "작가", "word_id" : 1, "m_begin" : 6, "m_end" : 6},
		{"id" : 2, "result" : "이은성/NNG+이/JKS", "target" : "이은성이", "word_id" : 2, "m_begin" : 7, "m_end" : 8},
		{"id" : 3, "result" : "1990/SN+년/NNB+에/JKB", "target" : "1990년에", "word_id" : 3, "m_begin" : 9, "m_end" : 11},
		{"id" : 4, "result" : "집필하/VV+ㄴ/ETM", "target" : "집필한", "word_id" : 4, "m_begin" : 12, "m_end" : 14},
		{"id" : 5, "result" : "책/NNG+으로/JKB+,/SP", "target" : "책으로,", "word_id" : 5, "m_begin" : 15, "m_end" : 17},
		{"id" : 6, "result" : "중학교/NNG", "target" : "중학교", "word_id" : 6, "m_begin" : 18, "m_end" : 19},
		{"id" : 7, "result" : "1/SN+학년/NNG", "target" : "1학년", "word_id" : 7, "m_begin" : 20, "m_end" : 21},
		{"id" : 8, "result" : "국어/NNG", "target" : "국어", "word_id" : 8, "m_begin" : 22, "m_end" : 22},
		{"id" : 9, "result" : "교과서/NNG+에/JKB", "target" : "교과서에", "word_id" : 9, "m_begin" : 23, "m_end" : 25},
		{"id" : 10, "result" : "일부/NNG", "target" : "일부", "word_id" : 10, "m_begin" : 26, "m_end" : 26},
		{"id" : 11, "result" : "수록/NNG", "target" : "수록", "word_id" : 11, "m_begin" : 27, "m_end" : 27},
		{"id" : 12, "result" : "되/VV+어/EC+있/VX+다/EF+./SF", "target" : "되어있다.", "word_id" : 12, "m_begin" : 28, "m_end" : 32}
	],
	"WSD" : [
		{"id" : 0, "text" : "동의보감", "type" : "NNG", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 12, "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "東醫寶鑑", "type" : "SH", "scode" : "00", "weight" : 1, "position" : 13, "begin" : 3, "end" : 3},
		{"id" : 3, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 25, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 26, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "작가", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 30, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "이은성", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 37, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 46, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "1990", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 50, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 54, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 57, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "집필하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 61, "begin" : 12, "end" : 13},
		{"id" : 12, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 67, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "책", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 71, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 74, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 80, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : "중학교", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 82, "begin" : 18, "end" : 19},
		{"id" : 17, "text" : "1", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 92, "begin" : 20, "end" : 20},
		{"id" : 18, "text" : "학년", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 93, "begin" : 21, "end" : 21},
		{"id" : 19, "text" : "국어", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 100, "begin" : 22, "end" : 22},
		{"id" : 20, "text" : "교과서", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 107, "begin" : 23, "end" : 24},
		{"id" : 21, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 116, "begin" : 25, "end" : 25},
		{"id" : 22, "text" : "일부", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 120, "begin" : 26, "end" : 26},
		{"id" : 23, "text" : "수록", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 127, "begin" : 27, "end" : 27},
		{"id" : 24, "text" : "되", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 134, "begin" : 28, "end" : 28},
		{"id" : 25, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 137, "begin" : 29, "end" : 29},
		{"id" : 26, "text" : "있", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 140, "begin" : 30, "end" : 30},
		{"id" : 27, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 143, "begin" : 31, "end" : 31},
		{"id" : 28, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 146, "begin" : 32, "end" : 32}
	],
	"word" : [
		{"id" : 0, "text" : "동의보감(東醫寶鑑)은", "type" : "", "begin" : 0, "end" : 5},
		{"id" : 1, "text" : "작가", "type" : "", "begin" : 6, "end" : 6},
		{"id" : 2, "text" : "이은성이", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 3, "text" : "1990년에", "type" : "", "begin" : 9, "end" : 11},
		{"id" : 4, "text" : "집필한", "type" : "", "begin" : 12, "end" : 14},
		{"id" : 5, "text" : "책으로,", "type" : "", "begin" : 15, "end" : 17},
		{"id" : 6, "text" : "중학교", "type" : "", "begin" : 18, "end" : 19},
		{"id" : 7, "text" : "1학년", "type" : "", "begin" : 20, "end" : 21},
		{"id" : 8, "text" : "국어", "type" : "", "begin" : 22, "end" : 22},
		{"id" : 9, "text" : "교과서에", "type" : "", "begin" : 23, "end" : 25},
		{"id" : 10, "text" : "일부", "type" : "", "begin" : 26, "end" : 26},
		{"id" : 11, "text" : "수록", "type" : "", "begin" : 27, "end" : 27},
		{"id" : 12, "text" : "되어있다.", "type" : "", "begin" : 28, "end" : 32}
	],
	"NE" : [
		{"id" : 0, "text" : "동의보감", "type" : "AFW_DOCUMENT", "begin" : 0, "end" : 1, "weight" : 0.267649, "common_noun" : 0},
		{"id" : 1, "text" : "작가", "type" : "CV_OCCUPATION", "begin" : 6, "end" : 6, "weight" : 0.439675, "common_noun" : 0},
		{"id" : 2, "text" : "이은성", "type" : "PS_NAME", "begin" : 7, "end" : 7, "weight" : 0.529799, "common_noun" : 0},
		{"id" : 3, "text" : "1990년", "type" : "DT_YEAR", "begin" : 9, "end" : 10, "weight" : 0.723454, "common_noun" : 0},
		{"id" : 4, "text" : "1학년", "type" : "QT_ORDER", "begin" : 20, "end" : 21, "weight" : 0.285162, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "동의보감(東醫寶鑑)은", "head" : 5, "label" : "NP_SBJ", "mod" : [], "weight" : 0.709992 },
		{"id" : 1, "text" : "작가", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.548044 },
		{"id" : 2, "text" : "이은성이", "head" : 4, "label" : "NP_SBJ", "mod" : [1], "weight" : 0.653368 },
		{"id" : 3, "text" : "1990년에", "head" : 4, "label" : "NP_AJT", "mod" : [], "weight" : 0.90739 },
		{"id" : 4, "text" : "집필한", "head" : 5, "label" : "VP_MOD", "mod" : [2, 3], "weight" : 0.702623 },
		{"id" : 5, "text" : "책으로,", "head" : 12, "label" : "NP_AJT", "mod" : [0, 4], "weight" : 0.953161 },
		{"id" : 6, "text" : "중학교", "head" : 7, "label" : "NP", "mod" : [], "weight" : 0.621525 },
		{"id" : 7, "text" : "1학년", "head" : 8, "label" : "NP", "mod" : [6], "weight" : 0.643444 },
		{"id" : 8, "text" : "국어", "head" : 9, "label" : "NP", "mod" : [7], "weight" : 0.972107 },
		{"id" : 9, "text" : "교과서에", "head" : 12, "label" : "NP_AJT", "mod" : [8], "weight" : 0.783722 },
		{"id" : 10, "text" : "일부", "head" : 11, "label" : "NP", "mod" : [], "weight" : 0.653219 },
		{"id" : 11, "text" : "수록", "head" : 12, "label" : "NP_CMP", "mod" : [10], "weight" : 0.332601 },
		{"id" : 12, "text" : "되어있다.", "head" : -1, "label" : "VP", "mod" : [5, 9, 11], "weight" : 0.00740485 }
	],
	"SRL" : [
		{"verb" : "집필", "sense" : 1, "word_id" : 4, "weight" : 0.25335,
			"argument" : [
				{"type" : "ARG0", "word_id" : 2, "text" : "이은성이", "weight" : 0.339154 },
				{"type" : "ARGM-TMP", "word_id" : 3, "text" : "1990년에", "weight" : 0.153718 },
				{"type" : "ARG1", "word_id" : 5, "text" : "책으로,", "weight" : 0.267177 }
			] },
		{"verb" : "되", "sense" : 1, "word_id" : 12, "weight" : 0.19178,
			"argument" : [
				{"type" : "ARG1", "word_id" : 0, "text" : "동의보감(東醫寶鑑)은", "weight" : 0.209392 },
				{"type" : "ARG2", "word_id" : 9, "text" : "교과서에", "weight" : 0.127137 },
				{"type" : "ARG2", "word_id" : 11, "text" : "수록", "weight" : 0.23881 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 12, "ant_sid" : 0, "ant_wid" : 0, "type" : "s", "istitle" : 0, "weight" : 0.358763 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "이 책은 이은성이 별세한 후에 춘하추동으로 내려고 했던 것을 상중하로 정리해서 엮었다.",
	"morp" : [
		{"id" : 0, "lemma" : "이", "type" : "MM", "position" : 147, "weight" : 0.0020222 },
		{"id" : 1, "lemma" : "책", "type" : "NNG", "position" : 151, "weight" : 0.718743 },
		{"id" : 2, "lemma" : "은", "type" : "JX", "position" : 154, "weight" : 0.0449928 },
		{"id" : 3, "lemma" : "이은성", "type" : "NNP", "position" : 158, "weight" : 0.3 },
		{"id" : 4, "lemma" : "이", "type" : "JKS", "position" : 167, "weight" : 0.0234517 },
		{"id" : 5, "lemma" : "별세", "type" : "NNG", "position" : 171, "weight" : 0.9 },
		{"id" : 6, "lemma" : "하", "type" : "XSV", "position" : 177, "weight" : 0.0001 },
		{"id" : 7, "lemma" : "ㄴ", "type" : "ETM", "position" : 177, "weight" : 0.392321 },
		{"id" : 8, "lemma" : "후", "type" : "NNG", "position" : 181, "weight" : 0.655812 },
		{"id" : 9, "lemma" : "에", "type" : "JKB", "position" : 184, "weight" : 0.153364 },
		{"id" : 10, "lemma" : "춘", "type" : "NNG", "position" : 188, "weight" : 0.18493 },
		{"id" : 11, "lemma" : "하", "type" : "NNG", "position" : 191, "weight" : 0.000123752 },
		{"id" : 12, "lemma" : "추", "type" : "NNG", "position" : 194, "weight" : 0.0155139 },
		{"id" : 13, "lemma" : "동", "type" : "NNG", "position" : 197, "weight" : 0.145566 },
		{"id" : 14, "lemma" : "으로", "type" : "JKB", "position" : 200, "weight" : 0.153406 },
		{"id" : 15, "lemma" : "내", "type" : "VV", "position" : 207, "weight" : 0.145454 },
		{"id" : 16, "lemma" : "려고", "type" : "EC", "position" : 210, "weight" : 0.424175 },
		{"id" : 17, "lemma" : "하", "type" : "VV", "position" : 217, "weight" : 0.140183 },
		{"id" : 18, "lemma" : "었", "type" : "EP", "position" : 217, "weight" : 0.9 },
		{"id" : 19, "lemma" : "던", "type" : "ETM", "position" : 220, "weight" : 0.107547 },
		{"id" : 20, "lemma" : "것", "type" : "NNB", "position" : 224, "weight" : 0.228788 },
		{"id" : 21, "lemma" : "을", "type" : "JKO", "position" : 227, "weight" : 0.0630104 },
		{"id" : 22, "lemma" : "상", "type" : "NNG", "position" : 231, "weight" : 0.0457572 },
		{"id" : 23, "lemma" : "중", "type" : "NNG", "position" : 234, "weight" : 0.0226349 },
		{"id" : 24, "lemma" : "하", "type" : "NNG", "position" : 237, "weight" : 0.000123752 },
		{"id" : 25, "lemma" : "로", "type" : "JKB", "position" : 240, "weight" : 0.153229 },
		{"id" : 26, "lemma" : "정리", "type" : "NNG", "position" : 244, "weight" : 0.9 },
		{"id" : 27, "lemma" : "하", "type" : "XSV", "position" : 250, "weight" : 0.0001 },
		{"id" : 28, "lemma" : "어서", "type" : "EC", "position" : 250, "weight" : 0.364511 },
		{"id" : 29, "lemma" : "엮", "type" : "VV", "position" : 257, "weight" : 0.9 },
		{"id" : 30, "lemma" : "었", "type" : "EP", "position" : 260, "weight" : 0.9 },
		{"id" : 31, "lemma" : "다", "type" : "EF", "position" : 263, "weight" : 0.640954 },
		{"id" : 32, "lemma" : ".", "type" : "SF", "position" : 266, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이/MM", "target" : "이", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "책/NNG+은/JX", "target" : "책은", "word_id" : 1, "m_begin" : 1, "m_end" : 2},
		{"id" : 2, "result" : "이은성/NNG+이/JKS", "target" : "이은성이", "word_id" : 2, "m_begin" : 3, "m_end" : 4},
		{"id" : 3, "result" : "별세하/VV+ㄴ/ETM", "target" : "별세한", "word_id" : 3, "m_begin" : 5, "m_end" : 7},
		{"id" : 4, "result" : "후/NNG+에/JKB", "target" : "후에", "word_id" : 4, "m_begin" : 8, "m_end" : 9},
		{"id" : 5, "result" : "춘하추동/NNG+으로/JKB", "target" : "춘하추동으로", "word_id" : 5, "m_begin" : 10, "m_end" : 14},
		{"id" : 6, "result" : "내/VV+려고/EC", "target" : "내려고", "word_id" : 6, "m_begin" : 15, "m_end" : 16},
		{"id" : 7, "result" : "하/VV+었/EP+던/ETM", "target" : "했던", "word_id" : 7, "m_begin" : 17, "m_end" : 19},
		{"id" : 8, "result" : "것/NNB+을/JKO", "target" : "것을", "word_id" : 8, "m_begin" : 20, "m_end" : 21},
		{"id" : 9, "result" : "상중하/NNG+로/JKB", "target" : "상중하로", "word_id" : 9, "m_begin" : 22, "m_end" : 25},
		{"id" : 10, "result" : "정리하/VV+어서/EC", "target" : "정리해서", "word_id" : 10, "m_begin" : 26, "m_end" : 28},
		{"id" : 11, "result" : "엮/VV+었/EP+다/EF+./SF", "target" : "엮었다.", "word_id" : 11, "m_begin" : 29, "m_end" : 32}
	],
	"WSD" : [
		{"id" : 0, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 147, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "책", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 151, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 154, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "이은성", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 158, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "이", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 167, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "별세하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 171, "begin" : 5, "end" : 6},
		{"id" : 6, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 177, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "후", "type" : "NNG", "scode" : "08", "weight" : 1, "position" : 181, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 184, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "춘하추동", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 188, "begin" : 10, "end" : 13},
		{"id" : 10, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 200, "begin" : 14, "end" : 14},
		{"id" : 11, "text" : "내", "type" : "VV", "scode" : "02", "weight" : 1, "position" : 207, "begin" : 15, "end" : 15},
		{"id" : 12, "text" : "려고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 210, "begin" : 16, "end" : 16},
		{"id" : 13, "text" : "하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 217, "begin" : 17, "end" : 17},
		{"id" : 14, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 217, "begin" : 18, "end" : 18},
		{"id" : 15, "text" : "던", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 220, "begin" : 19, "end" : 19},
		{"id" : 16, "text" : "것", "type" : "NNB", "scode" : "01", "weight" : 1, "position" : 224, "begin" : 20, "end" : 20},
		{"id" : 17, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 227, "begin" : 21, "end" : 21},
		{"id" : 18, "text" : "상중하", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 231, "begin" : 22, "end" : 24},
		{"id" : 19, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 240, "begin" : 25, "end" : 25},
		{"id" : 20, "text" : "정리하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 244, "begin" : 26, "end" : 27},
		{"id" : 21, "text" : "어서", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 250, "begin" : 28, "end" : 28},
		{"id" : 22, "text" : "엮", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 257, "begin" : 29, "end" : 29},
		{"id" : 23, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 260, "begin" : 30, "end" : 30},
		{"id" : 24, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 263, "begin" : 31, "end" : 31},
		{"id" : 25, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 266, "begin" : 32, "end" : 32}
	],
	"word" : [
		{"id" : 0, "text" : "이", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "책은", "type" : "", "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "이은성이", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 3, "text" : "별세한", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 4, "text" : "후에", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 5, "text" : "춘하추동으로", "type" : "", "begin" : 10, "end" : 14},
		{"id" : 6, "text" : "내려고", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 7, "text" : "했던", "type" : "", "begin" : 17, "end" : 19},
		{"id" : 8, "text" : "것을", "type" : "", "begin" : 20, "end" : 21},
		{"id" : 9, "text" : "상중하로", "type" : "", "begin" : 22, "end" : 25},
		{"id" : 10, "text" : "정리해서", "type" : "", "begin" : 26, "end" : 28},
		{"id" : 11, "text" : "엮었다.", "type" : "", "begin" : 29, "end" : 32}
	],
	"NE" : [
		{"id" : 0, "text" : "이은성", "type" : "PS_NAME", "begin" : 3, "end" : 3, "weight" : 0.53688, "common_noun" : 0},
		{"id" : 1, "text" : "춘하추동", "type" : "AFW_PERFORMANCE", "begin" : 10, "end" : 13, "weight" : 0.0655891, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이", "head" : 1, "label" : "DP", "mod" : [], "weight" : 0.915051 },
		{"id" : 1, "text" : "책은", "head" : 10, "label" : "NP_SBJ", "mod" : [0], "weight" : 0.783619 },
		{"id" : 2, "text" : "이은성이", "head" : 3, "label" : "NP_SBJ", "mod" : [], "weight" : 0.844443 },
		{"id" : 3, "text" : "별세한", "head" : 4, "label" : "VP_MOD", "mod" : [2], "weight" : 0.834734 },
		{"id" : 4, "text" : "후에", "head" : 6, "label" : "NP_AJT", "mod" : [3], "weight" : 0.686128 },
		{"id" : 5, "text" : "춘하추동으로", "head" : 6, "label" : "NP_AJT", "mod" : [], "weight" : 0.767383 },
		{"id" : 6, "text" : "내려고", "head" : 7, "label" : "VP", "mod" : [4, 5], "weight" : 0.943872 },
		{"id" : 7, "text" : "했던", "head" : 8, "label" : "VP_MOD", "mod" : [6], "weight" : 0.950549 },
		{"id" : 8, "text" : "것을", "head" : 10, "label" : "NP_OBJ", "mod" : [7], "weight" : 0.937411 },
		{"id" : 9, "text" : "상중하로", "head" : 10, "label" : "NP_AJT", "mod" : [], "weight" : 0.782037 },
		{"id" : 10, "text" : "정리해서", "head" : 11, "label" : "VP", "mod" : [1, 8, 9], "weight" : 0.509729 },
		{"id" : 11, "text" : "엮었다.", "head" : -1, "label" : "VP", "mod" : [10], "weight" : 0.0687646 }
	],
	"SRL" : [
		{"verb" : "별세", "sense" : 1, "word_id" : 3, "weight" : 0.233086,
			"argument" : [
				{"type" : "ARG0", "word_id" : 2, "text" : "이은성이", "weight" : 0.233086 }
			] },
		{"verb" : "내", "sense" : 1, "word_id" : 6, "weight" : 0.171652,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 4, "text" : "후에", "weight" : 0.213485 },
				{"type" : "ARG2", "word_id" : 5, "text" : "춘하추동으로", "weight" : 0.0682536 },
				{"type" : "AUX", "word_id" : 7, "text" : "했던", "weight" : 0.233218 }
			] },
		{"verb" : "정리", "sense" : 1, "word_id" : 10, "weight" : 0.176884,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 4, "text" : "후에", "weight" : 0.254271 },
				{"type" : "ARG1", "word_id" : 8, "text" : "것을", "weight" : 0.201705 },
				{"type" : "ARGM-MNR", "word_id" : 9, "text" : "상중하로", "weight" : 0.0746747 }
			] },
		{"verb" : "엮", "sense" : 1, "word_id" : 11, "weight" : 0.179205,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 4, "text" : "후에", "weight" : 0.221494 },
				{"type" : "ARGM-MNR", "word_id" : 10, "text" : "정리해서", "weight" : 0.136916 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 11, "ant_sid" : 1, "ant_wid" : 1, "type" : "s", "istitle" : 0, "weight" : 0.184814 },
		{"id" : 1, "verb_wid" : 11, "ant_sid" : 1, "ant_wid" : 1, "type" : "o", "istitle" : 0, "weight" : 0.155137 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 5, "ne_id" : -1, "text" : "동의보감(東醫寶鑑)은 작가 이은성이 1990년에 집필한 책", "start_eid_short" : 5, "end_eid_short" : 5, "text_short" : "책", "weight" : 0.002 },
		{"id" : 11, "sent_id" : 1, "start_eid" : 0, "end_eid" : 1, "ne_id" : -1, "text" : "이 책", "start_eid_short" : 0, "end_eid_short" : 1, "text_short" : "이 책", "weight" : 0.006 }
	] },
	{"id" : 1, "type" : "PS_NAME", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 3, "sent_id" : 0, "start_eid" : 1, "end_eid" : 1, "ne_id" : 1, "text" : "작가", "start_eid_short" : 1, "end_eid_short" : 1, "text_short" : "작가", "weight" : 0.016 },
		{"id" : 2, "sent_id" : 0, "start_eid" : 1, "end_eid" : 2, "ne_id" : 2, "text" : "작가 이은성", "start_eid_short" : 1, "end_eid_short" : 2, "text_short" : "작가 이은성", "weight" : 0.011 },
		{"id" : 13, "sent_id" : 1, "start_eid" : 2, "end_eid" : 2, "ne_id" : 0, "text" : "이은성", "start_eid_short" : 2, "end_eid_short" : 2, "text_short" : "이은성", "weight" : 0.015 }
	] }
 ]
}

