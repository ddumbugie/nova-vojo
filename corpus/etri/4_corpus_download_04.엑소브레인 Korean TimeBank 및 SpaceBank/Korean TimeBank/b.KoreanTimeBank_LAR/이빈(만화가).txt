{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "이빈(1971년 7월 3일~ )은 대한민국의 만화가이다.",
	"morp" : [
		{"id" : 0, "lemma" : "이빈", "type" : "NNP", "position" : 0, "weight" : 0.0803694 },
		{"id" : 1, "lemma" : "(", "type" : "SS", "position" : 6, "weight" : 1 },
		{"id" : 2, "lemma" : "1971", "type" : "SN", "position" : 7, "weight" : 1 },
		{"id" : 3, "lemma" : "년", "type" : "NNB", "position" : 11, "weight" : 0.414343 },
		{"id" : 4, "lemma" : "7", "type" : "SN", "position" : 15, "weight" : 1 },
		{"id" : 5, "lemma" : "월", "type" : "NNB", "position" : 16, "weight" : 0.408539 },
		{"id" : 6, "lemma" : "3", "type" : "SN", "position" : 20, "weight" : 1 },
		{"id" : 7, "lemma" : "일", "type" : "NNB", "position" : 21, "weight" : 0.126777 },
		{"id" : 8, "lemma" : "~", "type" : "SO", "position" : 24, "weight" : 1 },
		{"id" : 9, "lemma" : ")", "type" : "SS", "position" : 26, "weight" : 1 },
		{"id" : 10, "lemma" : "은", "type" : "JX", "position" : 27, "weight" : 0.0128817 },
		{"id" : 11, "lemma" : "대한민국", "type" : "NNP", "position" : 31, "weight" : 0.0447775 },
		{"id" : 12, "lemma" : "의", "type" : "JKG", "position" : 43, "weight" : 0.0987295 },
		{"id" : 13, "lemma" : "만화", "type" : "NNG", "position" : 47, "weight" : 0.83848 },
		{"id" : 14, "lemma" : "가", "type" : "XSN", "position" : 53, "weight" : 0.000115417 },
		{"id" : 15, "lemma" : "이", "type" : "VCP", "position" : 56, "weight" : 0.0165001 },
		{"id" : 16, "lemma" : "다", "type" : "EF", "position" : 59, "weight" : 0.353579 },
		{"id" : 17, "lemma" : ".", "type" : "SF", "position" : 62, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이빈/NNG+(/SS+1971/SN+년/NNB", "target" : "이빈(1971년", "word_id" : 0, "m_begin" : 0, "m_end" : 3},
		{"id" : 1, "result" : "7/SN+월/NNB", "target" : "7월", "word_id" : 1, "m_begin" : 4, "m_end" : 5},
		{"id" : 2, "result" : "3/SN+일/NNB+~/SO", "target" : "3일~", "word_id" : 2, "m_begin" : 6, "m_end" : 8},
		{"id" : 3, "result" : ")/SS+은/JX", "target" : ")은", "word_id" : 3, "m_begin" : 9, "m_end" : 10},
		{"id" : 4, "result" : "대한민국/NNG+의/JKG", "target" : "대한민국의", "word_id" : 4, "m_begin" : 11, "m_end" : 12},
		{"id" : 5, "result" : "만화가/NNG+이/VCP+다/EF+./SF", "target" : "만화가이다.", "word_id" : 5, "m_begin" : 13, "m_end" : 17}
	],
	"WSD" : [
		{"id" : 0, "text" : "이빈", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 6, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "1971", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 7, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 11, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "7", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 15, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "월", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 16, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "3", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 20, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "일", "type" : "NNB", "scode" : "07", "weight" : 1, "position" : 21, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "~", "type" : "SO", "scode" : "00", "weight" : 1, "position" : 24, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 26, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 27, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "대한민국", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 31, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 43, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "만화가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 47, "begin" : 13, "end" : 14},
		{"id" : 14, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 56, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 59, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 62, "begin" : 17, "end" : 17}
	],
	"word" : [
		{"id" : 0, "text" : "이빈(1971년", "type" : "", "begin" : 0, "end" : 3},
		{"id" : 1, "text" : "7월", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 2, "text" : "3일~", "type" : "", "begin" : 6, "end" : 8},
		{"id" : 3, "text" : ")은", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 4, "text" : "대한민국의", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 5, "text" : "만화가이다.", "type" : "", "begin" : 13, "end" : 17}
	],
	"NE" : [
		{"id" : 0, "text" : "이빈", "type" : "PS_NAME", "begin" : 0, "end" : 0, "weight" : 0.240564, "common_noun" : 0},
		{"id" : 1, "text" : "1971년 7월 3일~", "type" : "DT_OTHERS", "begin" : 2, "end" : 8, "weight" : 0.728679, "common_noun" : 0},
		{"id" : 2, "text" : "대한민국", "type" : "LCP_COUNTRY", "begin" : 11, "end" : 11, "weight" : 0.184149, "common_noun" : 0},
		{"id" : 3, "text" : "만화가", "type" : "CV_OCCUPATION", "begin" : 13, "end" : 14, "weight" : 0.285646, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이빈(1971년", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.571971 },
		{"id" : 1, "text" : "7월", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.931274 },
		{"id" : 2, "text" : "3일~", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.343149 },
		{"id" : 3, "text" : ")은", "head" : 5, "label" : "NP_SBJ", "mod" : [2], "weight" : 0.384185 },
		{"id" : 4, "text" : "대한민국의", "head" : 5, "label" : "NP_MOD", "mod" : [], "weight" : 0.446421 },
		{"id" : 5, "text" : "만화가이다.", "head" : -1, "label" : "VNP", "mod" : [3, 4], "weight" : 0.0181988 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "1991년 《르네상스》에서 〈나는 깍두기〉로 데뷔하였다.",
	"morp" : [
		{"id" : 0, "lemma" : "1991", "type" : "SN", "position" : 63, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 67, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "《", "type" : "SS", "position" : 71, "weight" : 1 },
		{"id" : 3, "lemma" : "르네상스", "type" : "NNG", "position" : 74, "weight" : 0.9 },
		{"id" : 4, "lemma" : "》", "type" : "SS", "position" : 86, "weight" : 1 },
		{"id" : 5, "lemma" : "에서", "type" : "JKB", "position" : 89, "weight" : 0.0486066 },
		{"id" : 6, "lemma" : "〈", "type" : "SS", "position" : 96, "weight" : 1 },
		{"id" : 7, "lemma" : "나", "type" : "NP", "position" : 99, "weight" : 0.0093447 },
		{"id" : 8, "lemma" : "는", "type" : "JX", "position" : 102, "weight" : 0.122593 },
		{"id" : 9, "lemma" : "깍두기", "type" : "NNG", "position" : 106, "weight" : 0.9 },
		{"id" : 10, "lemma" : "〉", "type" : "SS", "position" : 115, "weight" : 1 },
		{"id" : 11, "lemma" : "로", "type" : "JKB", "position" : 118, "weight" : 0.0485504 },
		{"id" : 12, "lemma" : "데뷔", "type" : "NNG", "position" : 122, "weight" : 0.9 },
		{"id" : 13, "lemma" : "하", "type" : "XSV", "position" : 128, "weight" : 0.0001 },
		{"id" : 14, "lemma" : "었", "type" : "EP", "position" : 131, "weight" : 0.9 },
		{"id" : 15, "lemma" : "다", "type" : "EF", "position" : 134, "weight" : 0.640954 },
		{"id" : 16, "lemma" : ".", "type" : "SF", "position" : 137, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "1991/SN+년/NNB", "target" : "1991년", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "《/SS+르네상스/NNG+》/SS+에서/JKB", "target" : "《르네상스》에서", "word_id" : 1, "m_begin" : 2, "m_end" : 5},
		{"id" : 2, "result" : "〈/SS+나/NP+는/JX", "target" : "〈나는", "word_id" : 2, "m_begin" : 6, "m_end" : 8},
		{"id" : 3, "result" : "깍두기/NNG+〉/SS+로/JKB", "target" : "깍두기〉로", "word_id" : 3, "m_begin" : 9, "m_end" : 11},
		{"id" : 4, "result" : "데뷔하/VV+었/EP+다/EF+./SF", "target" : "데뷔하였다.", "word_id" : 4, "m_begin" : 12, "m_end" : 16}
	],
	"WSD" : [
		{"id" : 0, "text" : "1991", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 63, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 67, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "《", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 71, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "르네상스", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 74, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "》", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 86, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 89, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "〈", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 96, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "나", "type" : "NP", "scode" : "03", "weight" : 1, "position" : 99, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 102, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "깍두기", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 106, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "〉", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 115, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 118, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "데뷔하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 122, "begin" : 12, "end" : 13},
		{"id" : 13, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 131, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 134, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 137, "begin" : 16, "end" : 16}
	],
	"word" : [
		{"id" : 0, "text" : "1991년", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "《르네상스》에서", "type" : "", "begin" : 2, "end" : 5},
		{"id" : 2, "text" : "〈나는", "type" : "", "begin" : 6, "end" : 8},
		{"id" : 3, "text" : "깍두기〉로", "type" : "", "begin" : 9, "end" : 11},
		{"id" : 4, "text" : "데뷔하였다.", "type" : "", "begin" : 12, "end" : 16}
	],
	"NE" : [
		{"id" : 0, "text" : "1991년", "type" : "DT_YEAR", "begin" : 0, "end" : 1, "weight" : 0.746536, "common_noun" : 0},
		{"id" : 1, "text" : "르네상스", "type" : "AF_WORKS", "begin" : 3, "end" : 3, "weight" : 0.670418, "common_noun" : 0},
		{"id" : 2, "text" : "나는 깍두기", "type" : "AF_WORKS", "begin" : 7, "end" : 9, "weight" : 0.458509, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "1991년", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.580991 },
		{"id" : 1, "text" : "《르네상스》에서", "head" : 4, "label" : "NP_AJT", "mod" : [0], "weight" : 0.638412 },
		{"id" : 2, "text" : "〈나는", "head" : 3, "label" : "VP_MOD", "mod" : [], "weight" : 0.408108 },
		{"id" : 3, "text" : "깍두기〉로", "head" : 4, "label" : "NP_AJT", "mod" : [2], "weight" : 0.432577 },
		{"id" : 4, "text" : "데뷔하였다.", "head" : -1, "label" : "VP", "mod" : [1, 3], "weight" : 0.036902 }
	],
	"SRL" : [
		{"verb" : "데뷔", "sense" : 1, "word_id" : 4, "weight" : 0.0755729,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 1, "text" : "《르네상스》에서", "weight" : 0.0872629 },
				{"type" : "ARG2", "word_id" : 3, "text" : "깍두기〉로", "weight" : 0.0638828 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 4, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.761424 }
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "아마추어 동호회 '결'에서 활동하였고 주요 작품으로는 《안녕?!자두야!!》가 있다.",
	"morp" : [
		{"id" : 0, "lemma" : "아마추어", "type" : "NNG", "position" : 138, "weight" : 0.9 },
		{"id" : 1, "lemma" : "동호", "type" : "NNG", "position" : 151, "weight" : 0.0193437 },
		{"id" : 2, "lemma" : "회", "type" : "XSN", "position" : 157, "weight" : 0.010766 },
		{"id" : 3, "lemma" : "'", "type" : "SS", "position" : 161, "weight" : 1 },
		{"id" : 4, "lemma" : "결", "type" : "NNG", "position" : 162, "weight" : 0.106667 },
		{"id" : 5, "lemma" : "'", "type" : "SS", "position" : 165, "weight" : 1 },
		{"id" : 6, "lemma" : "에서", "type" : "JKB", "position" : 166, "weight" : 0.0486066 },
		{"id" : 7, "lemma" : "활동", "type" : "NNG", "position" : 173, "weight" : 0.9 },
		{"id" : 8, "lemma" : "하", "type" : "XSV", "position" : 179, "weight" : 0.0001 },
		{"id" : 9, "lemma" : "었", "type" : "EP", "position" : 182, "weight" : 0.9 },
		{"id" : 10, "lemma" : "고", "type" : "EC", "position" : 185, "weight" : 0.190901 },
		{"id" : 11, "lemma" : "주요", "type" : "NNG", "position" : 189, "weight" : 0.9 },
		{"id" : 12, "lemma" : "작품", "type" : "NNG", "position" : 196, "weight" : 0.18482 },
		{"id" : 13, "lemma" : "으로", "type" : "JKB", "position" : 202, "weight" : 0.153406 },
		{"id" : 14, "lemma" : "는", "type" : "JX", "position" : 208, "weight" : 0.0387928 },
		{"id" : 15, "lemma" : "《", "type" : "SS", "position" : 212, "weight" : 1 },
		{"id" : 16, "lemma" : "안녕", "type" : "NNG", "position" : 215, "weight" : 0.0856035 },
		{"id" : 17, "lemma" : "?", "type" : "SF", "position" : 221, "weight" : 1 },
		{"id" : 18, "lemma" : "!", "type" : "SF", "position" : 222, "weight" : 1 },
		{"id" : 19, "lemma" : "자", "type" : "EC", "position" : 223, "weight" : 0 },
		{"id" : 20, "lemma" : "두", "type" : "VX", "position" : 226, "weight" : 0.0335364 },
		{"id" : 21, "lemma" : "야", "type" : "EF", "position" : 229, "weight" : 0.108646 },
		{"id" : 22, "lemma" : "!", "type" : "SF", "position" : 232, "weight" : 1 },
		{"id" : 23, "lemma" : "!", "type" : "SF", "position" : 233, "weight" : 1 },
		{"id" : 24, "lemma" : "》", "type" : "SS", "position" : 234, "weight" : 1 },
		{"id" : 25, "lemma" : "가", "type" : "JKS", "position" : 237, "weight" : 0.0144542 },
		{"id" : 26, "lemma" : "있", "type" : "VA", "position" : 241, "weight" : 0.12767 },
		{"id" : 27, "lemma" : "다", "type" : "EF", "position" : 244, "weight" : 0.132573 },
		{"id" : 28, "lemma" : ".", "type" : "SF", "position" : 247, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "아마추어/NNG", "target" : "아마추어", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "동호회/NNG", "target" : "동호회", "word_id" : 1, "m_begin" : 1, "m_end" : 2},
		{"id" : 2, "result" : "'/SS+결/NNG+'/SS+에서/JKB", "target" : "'결'에서", "word_id" : 2, "m_begin" : 3, "m_end" : 6},
		{"id" : 3, "result" : "활동하/VV+었/EP+고/EC", "target" : "활동하였고", "word_id" : 3, "m_begin" : 7, "m_end" : 10},
		{"id" : 4, "result" : "주요/NNG", "target" : "주요", "word_id" : 4, "m_begin" : 11, "m_end" : 11},
		{"id" : 5, "result" : "작품/NNG+으로/JKB+는/JX", "target" : "작품으로는", "word_id" : 5, "m_begin" : 12, "m_end" : 14},
		{"id" : 6, "result" : "《/SS+안녕/NNG+?/SF+!/SF+자/EC+두/VX+야/EF+!/SF+!/SF+》/SS+가/JKS", "target" : "《안녕?!자두야!!》가", "word_id" : 6, "m_begin" : 15, "m_end" : 25},
		{"id" : 7, "result" : "있/VA+다/EF+./SF", "target" : "있다.", "word_id" : 7, "m_begin" : 26, "m_end" : 28}
	],
	"WSD" : [
		{"id" : 0, "text" : "아마추어", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 138, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "동호회", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 151, "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 161, "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "결", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 162, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 165, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 166, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "활동하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 173, "begin" : 7, "end" : 8},
		{"id" : 7, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 182, "begin" : 9, "end" : 9},
		{"id" : 8, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 185, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "주요", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 189, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "작품", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 196, "begin" : 12, "end" : 12},
		{"id" : 11, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 202, "begin" : 13, "end" : 13},
		{"id" : 12, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 208, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "《", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 212, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : "안녕", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 215, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 221, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : "!", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 222, "begin" : 18, "end" : 18},
		{"id" : 17, "text" : "자", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 223, "begin" : 19, "end" : 19},
		{"id" : 18, "text" : "두", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 226, "begin" : 20, "end" : 20},
		{"id" : 19, "text" : "야", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 229, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : "!", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 232, "begin" : 22, "end" : 22},
		{"id" : 21, "text" : "!", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 233, "begin" : 23, "end" : 23},
		{"id" : 22, "text" : "》", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 234, "begin" : 24, "end" : 24},
		{"id" : 23, "text" : "가", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 237, "begin" : 25, "end" : 25},
		{"id" : 24, "text" : "있", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 241, "begin" : 26, "end" : 26},
		{"id" : 25, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 244, "begin" : 27, "end" : 27},
		{"id" : 26, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 247, "begin" : 28, "end" : 28}
	],
	"word" : [
		{"id" : 0, "text" : "아마추어", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "동호회", "type" : "", "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "'결'에서", "type" : "", "begin" : 3, "end" : 6},
		{"id" : 3, "text" : "활동하였고", "type" : "", "begin" : 7, "end" : 10},
		{"id" : 4, "text" : "주요", "type" : "", "begin" : 11, "end" : 11},
		{"id" : 5, "text" : "작품으로는", "type" : "", "begin" : 12, "end" : 14},
		{"id" : 6, "text" : "《안녕?!자두야!!》가", "type" : "", "begin" : 15, "end" : 25},
		{"id" : 7, "text" : "있다.", "type" : "", "begin" : 26, "end" : 28}
	],
	"NE" : [
		{"id" : 0, "text" : "아마추어", "type" : "CV_POSITION", "begin" : 0, "end" : 0, "weight" : 0.265099, "common_noun" : 0},
		{"id" : 1, "text" : "안녕?!자두야!!", "type" : "AFW_DOCUMENT", "begin" : 16, "end" : 23, "weight" : 0.848538, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "아마추어", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.678623 },
		{"id" : 1, "text" : "동호회", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.919064 },
		{"id" : 2, "text" : "'결'에서", "head" : 3, "label" : "NP_AJT", "mod" : [1], "weight" : 0.725577 },
		{"id" : 3, "text" : "활동하였고", "head" : 7, "label" : "VP", "mod" : [2], "weight" : 0.630214 },
		{"id" : 4, "text" : "주요", "head" : 5, "label" : "NP", "mod" : [], "weight" : 0.813615 },
		{"id" : 5, "text" : "작품으로는", "head" : 7, "label" : "NP_AJT", "mod" : [4], "weight" : 0.653614 },
		{"id" : 6, "text" : "《안녕?!자두야!!》가", "head" : 7, "label" : "NP_SBJ", "mod" : [], "weight" : 0.489399 },
		{"id" : 7, "text" : "있다.", "head" : -1, "label" : "VP", "mod" : [3, 5, 6], "weight" : 0.0532665 }
	],
	"SRL" : [
		{"verb" : "활동", "sense" : 1, "word_id" : 3, "weight" : 0.122823,
			"argument" : [
				{"type" : "ARGM-LOC", "word_id" : 2, "text" : "'결'에서", "weight" : 0.122823 }
			] },
		{"verb" : "있", "sense" : 1, "word_id" : 7, "weight" : 0.197592,
			"argument" : [
				{"type" : "ARG2", "word_id" : 5, "text" : "작품으로는", "weight" : 0.118845 },
				{"type" : "ARG1", "word_id" : 6, "text" : "《안녕?!자두야!!》가", "weight" : 0.276339 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 3, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.385842 }
	]
	}
 ],
 "entity" : [
 ]
}

