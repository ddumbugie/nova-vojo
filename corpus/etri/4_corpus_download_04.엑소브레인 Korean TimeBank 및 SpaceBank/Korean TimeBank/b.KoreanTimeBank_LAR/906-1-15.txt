{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "이것은 물 위를 빠른 속도로 치고 나가는 초고속 선박 기술과 수면에서 뜬 상태로 이동하는 항공 기술을 접목해 만든 첨단 선박이다.",
	"morp" : [
		{"id" : 0, "lemma" : "이것", "type" : "NP", "position" : 0, "weight" : 0.0733657 },
		{"id" : 1, "lemma" : "은", "type" : "JX", "position" : 6, "weight" : 0.191811 },
		{"id" : 2, "lemma" : "물", "type" : "NNG", "position" : 10, "weight" : 0.277882 },
		{"id" : 3, "lemma" : "위", "type" : "NNG", "position" : 14, "weight" : 0.165604 },
		{"id" : 4, "lemma" : "를", "type" : "JKO", "position" : 17, "weight" : 0.137686 },
		{"id" : 5, "lemma" : "빠르", "type" : "VA", "position" : 21, "weight" : 0.9 },
		{"id" : 6, "lemma" : "ㄴ", "type" : "ETM", "position" : 24, "weight" : 0.430446 },
		{"id" : 7, "lemma" : "속도", "type" : "NNG", "position" : 28, "weight" : 0.9 },
		{"id" : 8, "lemma" : "로", "type" : "JKB", "position" : 34, "weight" : 0.153229 },
		{"id" : 9, "lemma" : "치", "type" : "VV", "position" : 38, "weight" : 0.40589 },
		{"id" : 10, "lemma" : "고", "type" : "EC", "position" : 41, "weight" : 0.416679 },
		{"id" : 11, "lemma" : "나가", "type" : "VX", "position" : 45, "weight" : 0.103911 },
		{"id" : 12, "lemma" : "는", "type" : "ETM", "position" : 51, "weight" : 0.183966 },
		{"id" : 13, "lemma" : "초", "type" : "XPN", "position" : 55, "weight" : 0.000165226 },
		{"id" : 14, "lemma" : "고속", "type" : "NNG", "position" : 58, "weight" : 0.9 },
		{"id" : 15, "lemma" : "선박", "type" : "NNG", "position" : 65, "weight" : 0.9 },
		{"id" : 16, "lemma" : "기술", "type" : "NNG", "position" : 72, "weight" : 0.9 },
		{"id" : 17, "lemma" : "과", "type" : "JC", "position" : 78, "weight" : 0.017569 },
		{"id" : 18, "lemma" : "수면", "type" : "NNG", "position" : 82, "weight" : 0.9 },
		{"id" : 19, "lemma" : "에서", "type" : "JKB", "position" : 88, "weight" : 0.153407 },
		{"id" : 20, "lemma" : "뜨", "type" : "VV", "position" : 95, "weight" : 0.442858 },
		{"id" : 21, "lemma" : "ㄴ", "type" : "ETM", "position" : 95, "weight" : 0.304215 },
		{"id" : 22, "lemma" : "상태", "type" : "NNG", "position" : 99, "weight" : 0.658577 },
		{"id" : 23, "lemma" : "로", "type" : "JKB", "position" : 105, "weight" : 0.153229 },
		{"id" : 24, "lemma" : "이동", "type" : "NNG", "position" : 109, "weight" : 0.206348 },
		{"id" : 25, "lemma" : "하", "type" : "XSV", "position" : 115, "weight" : 0.0001 },
		{"id" : 26, "lemma" : "는", "type" : "ETM", "position" : 118, "weight" : 0.238503 },
		{"id" : 27, "lemma" : "항공", "type" : "NNG", "position" : 122, "weight" : 0.9 },
		{"id" : 28, "lemma" : "기술", "type" : "NNG", "position" : 129, "weight" : 0.9 },
		{"id" : 29, "lemma" : "을", "type" : "JKO", "position" : 135, "weight" : 0.129611 },
		{"id" : 30, "lemma" : "접목", "type" : "NNG", "position" : 139, "weight" : 0.9 },
		{"id" : 31, "lemma" : "하", "type" : "XSV", "position" : 145, "weight" : 0.0001 },
		{"id" : 32, "lemma" : "어", "type" : "EC", "position" : 145, "weight" : 0.361326 },
		{"id" : 33, "lemma" : "만들", "type" : "VV", "position" : 149, "weight" : 0.9 },
		{"id" : 34, "lemma" : "ㄴ", "type" : "ETM", "position" : 152, "weight" : 0.304215 },
		{"id" : 35, "lemma" : "첨단", "type" : "NNG", "position" : 156, "weight" : 0.9 },
		{"id" : 36, "lemma" : "선박", "type" : "NNG", "position" : 163, "weight" : 0.9 },
		{"id" : 37, "lemma" : "이", "type" : "VCP", "position" : 169, "weight" : 0.0177525 },
		{"id" : 38, "lemma" : "다", "type" : "EF", "position" : 172, "weight" : 0.353579 },
		{"id" : 39, "lemma" : ".", "type" : "SF", "position" : 175, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이것/NP+은/JX", "target" : "이것은", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "물/NNG", "target" : "물", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "위/NNG+를/JKO", "target" : "위를", "word_id" : 2, "m_begin" : 3, "m_end" : 4},
		{"id" : 3, "result" : "빠르/VA+ㄴ/ETM", "target" : "빠른", "word_id" : 3, "m_begin" : 5, "m_end" : 6},
		{"id" : 4, "result" : "속도/NNG+로/JKB", "target" : "속도로", "word_id" : 4, "m_begin" : 7, "m_end" : 8},
		{"id" : 5, "result" : "치/VV+고/EC", "target" : "치고", "word_id" : 5, "m_begin" : 9, "m_end" : 10},
		{"id" : 6, "result" : "나가/VX+는/ETM", "target" : "나가는", "word_id" : 6, "m_begin" : 11, "m_end" : 12},
		{"id" : 7, "result" : "초고속/NNG", "target" : "초고속", "word_id" : 7, "m_begin" : 13, "m_end" : 14},
		{"id" : 8, "result" : "선박/NNG", "target" : "선박", "word_id" : 8, "m_begin" : 15, "m_end" : 15},
		{"id" : 9, "result" : "기술/NNG+과/JC", "target" : "기술과", "word_id" : 9, "m_begin" : 16, "m_end" : 17},
		{"id" : 10, "result" : "수면/NNG+에서/JKB", "target" : "수면에서", "word_id" : 10, "m_begin" : 18, "m_end" : 19},
		{"id" : 11, "result" : "뜨/VV+ㄴ/ETM", "target" : "뜬", "word_id" : 11, "m_begin" : 20, "m_end" : 21},
		{"id" : 12, "result" : "상태/NNG+로/JKB", "target" : "상태로", "word_id" : 12, "m_begin" : 22, "m_end" : 23},
		{"id" : 13, "result" : "이동하/VV+는/ETM", "target" : "이동하는", "word_id" : 13, "m_begin" : 24, "m_end" : 26},
		{"id" : 14, "result" : "항공/NNG", "target" : "항공", "word_id" : 14, "m_begin" : 27, "m_end" : 27},
		{"id" : 15, "result" : "기술/NNG+을/JKO", "target" : "기술을", "word_id" : 15, "m_begin" : 28, "m_end" : 29},
		{"id" : 16, "result" : "접목하/VV+어/EC", "target" : "접목해", "word_id" : 16, "m_begin" : 30, "m_end" : 32},
		{"id" : 17, "result" : "만들/VV+ㄴ/ETM", "target" : "만든", "word_id" : 17, "m_begin" : 33, "m_end" : 34},
		{"id" : 18, "result" : "첨단/NNG", "target" : "첨단", "word_id" : 18, "m_begin" : 35, "m_end" : 35},
		{"id" : 19, "result" : "선박/NNG+이/VCP+다/EF+./SF", "target" : "선박이다.", "word_id" : 19, "m_begin" : 36, "m_end" : 39}
	],
	"WSD" : [
		{"id" : 0, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 6, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "물", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "위", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 14, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 17, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "빠르", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 21, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 24, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "속도", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 28, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 34, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "치", "type" : "VV", "scode" : "02", "weight" : 1, "position" : 38, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 41, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "나가", "type" : "VX", "scode" : "00", "weight" : 1, "position" : 45, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 51, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "초고속", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 55, "begin" : 13, "end" : 14},
		{"id" : 14, "text" : "선박", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 65, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "기술", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 72, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "과", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 78, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "수면", "type" : "NNG", "scode" : "07", "weight" : 1, "position" : 82, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 88, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "뜨", "type" : "VV", "scode" : "05", "weight" : 1, "position" : 95, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 95, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : "상태", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 99, "begin" : 22, "end" : 22},
		{"id" : 22, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 105, "begin" : 23, "end" : 23},
		{"id" : 23, "text" : "이동하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 109, "begin" : 24, "end" : 25},
		{"id" : 24, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 118, "begin" : 26, "end" : 26},
		{"id" : 25, "text" : "항공", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 122, "begin" : 27, "end" : 27},
		{"id" : 26, "text" : "기술", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 129, "begin" : 28, "end" : 28},
		{"id" : 27, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 135, "begin" : 29, "end" : 29},
		{"id" : 28, "text" : "접목하", "type" : "VV", "scode" : "02", "weight" : 1, "position" : 139, "begin" : 30, "end" : 31},
		{"id" : 29, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 145, "begin" : 32, "end" : 32},
		{"id" : 30, "text" : "만들", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 149, "begin" : 33, "end" : 33},
		{"id" : 31, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 152, "begin" : 34, "end" : 34},
		{"id" : 32, "text" : "첨단", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 156, "begin" : 35, "end" : 35},
		{"id" : 33, "text" : "선박", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 163, "begin" : 36, "end" : 36},
		{"id" : 34, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 169, "begin" : 37, "end" : 37},
		{"id" : 35, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 172, "begin" : 38, "end" : 38},
		{"id" : 36, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 175, "begin" : 39, "end" : 39}
	],
	"word" : [
		{"id" : 0, "text" : "이것은", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "물", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "위를", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 3, "text" : "빠른", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 4, "text" : "속도로", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 5, "text" : "치고", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 6, "text" : "나가는", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 7, "text" : "초고속", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 8, "text" : "선박", "type" : "", "begin" : 15, "end" : 15},
		{"id" : 9, "text" : "기술과", "type" : "", "begin" : 16, "end" : 17},
		{"id" : 10, "text" : "수면에서", "type" : "", "begin" : 18, "end" : 19},
		{"id" : 11, "text" : "뜬", "type" : "", "begin" : 20, "end" : 21},
		{"id" : 12, "text" : "상태로", "type" : "", "begin" : 22, "end" : 23},
		{"id" : 13, "text" : "이동하는", "type" : "", "begin" : 24, "end" : 26},
		{"id" : 14, "text" : "항공", "type" : "", "begin" : 27, "end" : 27},
		{"id" : 15, "text" : "기술을", "type" : "", "begin" : 28, "end" : 29},
		{"id" : 16, "text" : "접목해", "type" : "", "begin" : 30, "end" : 32},
		{"id" : 17, "text" : "만든", "type" : "", "begin" : 33, "end" : 34},
		{"id" : 18, "text" : "첨단", "type" : "", "begin" : 35, "end" : 35},
		{"id" : 19, "text" : "선박이다.", "type" : "", "begin" : 36, "end" : 39}
	],
	"NE" : [
		{"id" : 0, "text" : "선박", "type" : "AF_TRANSPORT", "begin" : 36, "end" : 36, "weight" : 0.254333, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이것은", "head" : 19, "label" : "NP_SBJ", "mod" : [], "weight" : 0.984895 },
		{"id" : 1, "text" : "물", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.828489 },
		{"id" : 2, "text" : "위를", "head" : 5, "label" : "NP_OBJ", "mod" : [1], "weight" : 0.677365 },
		{"id" : 3, "text" : "빠른", "head" : 4, "label" : "VP_MOD", "mod" : [], "weight" : 0.933209 },
		{"id" : 4, "text" : "속도로", "head" : 5, "label" : "NP_AJT", "mod" : [3], "weight" : 0.675727 },
		{"id" : 5, "text" : "치고", "head" : 6, "label" : "VP", "mod" : [2, 4], "weight" : 0.680537 },
		{"id" : 6, "text" : "나가는", "head" : 9, "label" : "VP_MOD", "mod" : [5], "weight" : 0.646255 },
		{"id" : 7, "text" : "초고속", "head" : 9, "label" : "NP", "mod" : [], "weight" : 0.683985 },
		{"id" : 8, "text" : "선박", "head" : 9, "label" : "NP", "mod" : [], "weight" : 0.745489 },
		{"id" : 9, "text" : "기술과", "head" : 15, "label" : "NP_CNJ", "mod" : [6, 7, 8], "weight" : 0.771655 },
		{"id" : 10, "text" : "수면에서", "head" : 11, "label" : "NP_AJT", "mod" : [], "weight" : 0.671214 },
		{"id" : 11, "text" : "뜬", "head" : 12, "label" : "VP_MOD", "mod" : [10], "weight" : 0.790915 },
		{"id" : 12, "text" : "상태로", "head" : 13, "label" : "NP_AJT", "mod" : [11], "weight" : 0.691191 },
		{"id" : 13, "text" : "이동하는", "head" : 15, "label" : "VP_MOD", "mod" : [12], "weight" : 0.694562 },
		{"id" : 14, "text" : "항공", "head" : 15, "label" : "NP", "mod" : [], "weight" : 0.676656 },
		{"id" : 15, "text" : "기술을", "head" : 16, "label" : "NP_OBJ", "mod" : [9, 13, 14], "weight" : 0.963949 },
		{"id" : 16, "text" : "접목해", "head" : 17, "label" : "VP", "mod" : [15], "weight" : 0.802444 },
		{"id" : 17, "text" : "만든", "head" : 19, "label" : "VP_MOD", "mod" : [16], "weight" : 0.532395 },
		{"id" : 18, "text" : "첨단", "head" : 19, "label" : "NP", "mod" : [], "weight" : 0.428348 },
		{"id" : 19, "text" : "선박이다.", "head" : -1, "label" : "VNP", "mod" : [0, 17, 18], "weight" : 0.00112907 }
	],
	"SRL" : [
		{"verb" : "빠르", "sense" : 1, "word_id" : 3, "weight" : 0.255979,
			"argument" : [
				{"type" : "ARG1", "word_id" : 4, "text" : "속도로", "weight" : 0.255979 }
			] },
		{"verb" : "치", "sense" : 1, "word_id" : 5, "weight" : 0.207215,
			"argument" : [
				{"type" : "ARG1", "word_id" : 2, "text" : "위를", "weight" : 0.272513 },
				{"type" : "ARGM-MNR", "word_id" : 4, "text" : "속도로", "weight" : 0.147664 },
				{"type" : "AUX", "word_id" : 6, "text" : "나가는", "weight" : 0.220175 },
				{"type" : "ARG0", "word_id" : 9, "text" : "기술과", "weight" : 0.188507 }
			] },
		{"verb" : "뜨", "sense" : 1, "word_id" : 11, "weight" : 0.159285,
			"argument" : [
				{"type" : "ARG2", "word_id" : 10, "text" : "수면에서", "weight" : 0.159285 }
			] },
		{"verb" : "이동", "sense" : 1, "word_id" : 13, "weight" : 0.188971,
			"argument" : [
				{"type" : "ARGM-PRD", "word_id" : 12, "text" : "상태로", "weight" : 0.12021 },
				{"type" : "ARG1", "word_id" : 15, "text" : "기술을", "weight" : 0.257731 }
			] },
		{"verb" : "접목", "sense" : 1, "word_id" : 16, "weight" : 0.271169,
			"argument" : [
				{"type" : "ARG1", "word_id" : 9, "text" : "기술과", "weight" : 0.140184 },
				{"type" : "ARG1", "word_id" : 15, "text" : "기술을", "weight" : 0.402154 }
			] },
		{"verb" : "만들", "sense" : 1, "word_id" : 17, "weight" : 0.215473,
			"argument" : [
				{"type" : "ARGM-MNR", "word_id" : 16, "text" : "접목해", "weight" : 0.19658 },
				{"type" : "ARG1", "word_id" : 19, "text" : "선박이다.", "weight" : 0.234366 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 16, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.38206 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "1976년에 처음 등장한 이것은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "1976", "type" : "SN", "position" : 176, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 180, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "에", "type" : "JKB", "position" : 183, "weight" : 0.135559 },
		{"id" : 3, "lemma" : "처음", "type" : "NNG", "position" : 187, "weight" : 0.9 },
		{"id" : 4, "lemma" : "등장", "type" : "NNG", "position" : 194, "weight" : 0.9 },
		{"id" : 5, "lemma" : "하", "type" : "XSV", "position" : 200, "weight" : 0.0001 },
		{"id" : 6, "lemma" : "ㄴ", "type" : "ETM", "position" : 200, "weight" : 0.392321 },
		{"id" : 7, "lemma" : "이것", "type" : "NP", "position" : 204, "weight" : 0.0102992 },
		{"id" : 8, "lemma" : "은", "type" : "JX", "position" : 210, "weight" : 0.191811 },
		{"id" : 9, "lemma" : "무엇", "type" : "NP", "position" : 214, "weight" : 0.9 },
		{"id" : 10, "lemma" : "이", "type" : "VCP", "position" : 220, "weight" : 0.0175768 },
		{"id" : 11, "lemma" : "ㄹ까", "type" : "EF", "position" : 220, "weight" : 0.258243 },
		{"id" : 12, "lemma" : "?", "type" : "SF", "position" : 226, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "1976/SN+년/NNB+에/JKB", "target" : "1976년에", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "처음/NNG", "target" : "처음", "word_id" : 1, "m_begin" : 3, "m_end" : 3},
		{"id" : 2, "result" : "등장하/VV+ㄴ/ETM", "target" : "등장한", "word_id" : 2, "m_begin" : 4, "m_end" : 6},
		{"id" : 3, "result" : "이것/NP+은/JX", "target" : "이것은", "word_id" : 3, "m_begin" : 7, "m_end" : 8},
		{"id" : 4, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 4, "m_begin" : 9, "m_end" : 12}
	],
	"WSD" : [
		{"id" : 0, "text" : "1976", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 176, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 180, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 183, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "처음", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 187, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "등장하", "type" : "VV", "scode" : "01", "weight" : 1, "position" : 194, "begin" : 4, "end" : 5},
		{"id" : 5, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 200, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 204, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 210, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 214, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 220, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 220, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 226, "begin" : 12, "end" : 12}
	],
	"word" : [
		{"id" : 0, "text" : "1976년에", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "처음", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 2, "text" : "등장한", "type" : "", "begin" : 4, "end" : 6},
		{"id" : 3, "text" : "이것은", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 4, "text" : "무엇일까?", "type" : "", "begin" : 9, "end" : 12}
	],
	"NE" : [
		{"id" : 0, "text" : "1976년", "type" : "DT_YEAR", "begin" : 0, "end" : 1, "weight" : 0.631853, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "1976년에", "head" : 2, "label" : "NP_AJT", "mod" : [], "weight" : 0.897136 },
		{"id" : 1, "text" : "처음", "head" : 2, "label" : "NP_AJT", "mod" : [], "weight" : 0.613131 },
		{"id" : 2, "text" : "등장한", "head" : 3, "label" : "VP_MOD", "mod" : [0, 1], "weight" : 0.817479 },
		{"id" : 3, "text" : "이것은", "head" : 4, "label" : "NP_SBJ", "mod" : [2], "weight" : 0.669636 },
		{"id" : 4, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [3], "weight" : 0.208023 }
	],
	"SRL" : [
		{"verb" : "등장", "sense" : 1, "word_id" : 2, "weight" : 0.254458,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 0, "text" : "1976년에", "weight" : 0.219103 },
				{"type" : "ARGM-ADV", "word_id" : 1, "text" : "처음", "weight" : 0.216526 },
				{"type" : "ARG1", "word_id" : 3, "text" : "이것은", "weight" : 0.327745 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 2, "sent_id" : 0, "start_eid" : 1, "end_eid" : 9, "ne_id" : -1, "text" : "물 위를 빠른 속도로 치고 나가는 초고속 선박 기술", "start_eid_short" : 7, "end_eid_short" : 9, "text_short" : "초고속 선박 기술", "weight" : 0.002 },
		{"id" : 6, "sent_id" : 0, "start_eid" : 7, "end_eid" : 15, "ne_id" : -1, "text" : "초고속 선박 기술과 수면에서 뜬 상태로 이동하는 항공 기술", "start_eid_short" : 14, "end_eid_short" : 15, "text_short" : "항공 기술", "weight" : 0.0035 }
	] },
	{"id" : 1, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 0, "ne_id" : -1, "text" : "이것", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "이것", "weight" : 0.03 },
		{"id" : 16, "sent_id" : 1, "start_eid" : 3, "end_eid" : 3, "ne_id" : -1, "text" : "이것", "start_eid_short" : 3, "end_eid_short" : 3, "text_short" : "이것", "weight" : 0.018 }
	] }
 ]
}

