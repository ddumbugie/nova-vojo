{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿조선 세종 때 우리 고유의 약재와 치료 방법을 정리해 편찬한 의서는 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "조선", "type" : "NNP", "position" : 3, "weight" : 0.0796816 },
		{"id" : 2, "lemma" : "세종", "type" : "NNP", "position" : 10, "weight" : 0.0807222 },
		{"id" : 3, "lemma" : "때", "type" : "NNG", "position" : 17, "weight" : 0.280999 },
		{"id" : 4, "lemma" : "우리", "type" : "NP", "position" : 21, "weight" : 0.00176155 },
		{"id" : 5, "lemma" : "고유", "type" : "NNG", "position" : 28, "weight" : 0.9 },
		{"id" : 6, "lemma" : "의", "type" : "JKG", "position" : 34, "weight" : 0.0694213 },
		{"id" : 7, "lemma" : "약재", "type" : "NNG", "position" : 38, "weight" : 0.869588 },
		{"id" : 8, "lemma" : "와", "type" : "JC", "position" : 44, "weight" : 0.0169714 },
		{"id" : 9, "lemma" : "치료", "type" : "NNG", "position" : 48, "weight" : 0.9 },
		{"id" : 10, "lemma" : "방법", "type" : "NNG", "position" : 55, "weight" : 0.9 },
		{"id" : 11, "lemma" : "을", "type" : "JKO", "position" : 61, "weight" : 0.129611 },
		{"id" : 12, "lemma" : "정리", "type" : "NNG", "position" : 65, "weight" : 0.9 },
		{"id" : 13, "lemma" : "하", "type" : "XSV", "position" : 71, "weight" : 0.0001 },
		{"id" : 14, "lemma" : "어", "type" : "EC", "position" : 71, "weight" : 0.361326 },
		{"id" : 15, "lemma" : "편찬", "type" : "NNG", "position" : 75, "weight" : 0.9 },
		{"id" : 16, "lemma" : "하", "type" : "XSV", "position" : 81, "weight" : 0.0001 },
		{"id" : 17, "lemma" : "ㄴ", "type" : "ETM", "position" : 81, "weight" : 0.392321 },
		{"id" : 18, "lemma" : "의서", "type" : "NNG", "position" : 85, "weight" : 0.636122 },
		{"id" : 19, "lemma" : "는", "type" : "JX", "position" : 91, "weight" : 0.0287565 },
		{"id" : 20, "lemma" : "무엇", "type" : "NP", "position" : 95, "weight" : 0.9 },
		{"id" : 21, "lemma" : "이", "type" : "VCP", "position" : 101, "weight" : 0.0175768 },
		{"id" : 22, "lemma" : "ㄹ까", "type" : "EF", "position" : 101, "weight" : 0.258243 },
		{"id" : 23, "lemma" : "?", "type" : "SF", "position" : 107, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿조선/NNG", "target" : "﻿조선", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "세종/NNG", "target" : "세종", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "때/NNG", "target" : "때", "word_id" : 2, "m_begin" : 3, "m_end" : 3},
		{"id" : 3, "result" : "우리/NP", "target" : "우리", "word_id" : 3, "m_begin" : 4, "m_end" : 4},
		{"id" : 4, "result" : "고유/NNG+의/JKG", "target" : "고유의", "word_id" : 4, "m_begin" : 5, "m_end" : 6},
		{"id" : 5, "result" : "약재/NNG+와/JC", "target" : "약재와", "word_id" : 5, "m_begin" : 7, "m_end" : 8},
		{"id" : 6, "result" : "치료/NNG", "target" : "치료", "word_id" : 6, "m_begin" : 9, "m_end" : 9},
		{"id" : 7, "result" : "방법/NNG+을/JKO", "target" : "방법을", "word_id" : 7, "m_begin" : 10, "m_end" : 11},
		{"id" : 8, "result" : "정리하/VV+어/EC", "target" : "정리해", "word_id" : 8, "m_begin" : 12, "m_end" : 14},
		{"id" : 9, "result" : "편찬하/VV+ㄴ/ETM", "target" : "편찬한", "word_id" : 9, "m_begin" : 15, "m_end" : 17},
		{"id" : 10, "result" : "의서/NNG+는/JX", "target" : "의서는", "word_id" : 10, "m_begin" : 18, "m_end" : 19},
		{"id" : 11, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 11, "m_begin" : 20, "m_end" : 23}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "조선", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "세종", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "때", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 17, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "우리", "type" : "NP", "scode" : "03", "weight" : 1, "position" : 21, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "고유", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 28, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 34, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "약재", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 38, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "와", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 44, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "치료", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 48, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "방법", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 55, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 61, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "정리하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 65, "begin" : 12, "end" : 13},
		{"id" : 13, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 71, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "편찬하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 75, "begin" : 15, "end" : 16},
		{"id" : 15, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 81, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : "의서", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 85, "begin" : 18, "end" : 18},
		{"id" : 17, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 91, "begin" : 19, "end" : 19},
		{"id" : 18, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 95, "begin" : 20, "end" : 20},
		{"id" : 19, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 101, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 101, "begin" : 22, "end" : 22},
		{"id" : 21, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 107, "begin" : 23, "end" : 23}
	],
	"word" : [
		{"id" : 0, "text" : "﻿조선", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "세종", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "때", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "우리", "type" : "", "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "고유의", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 5, "text" : "약재와", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 6, "text" : "치료", "type" : "", "begin" : 9, "end" : 9},
		{"id" : 7, "text" : "방법을", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 8, "text" : "정리해", "type" : "", "begin" : 12, "end" : 14},
		{"id" : 9, "text" : "편찬한", "type" : "", "begin" : 15, "end" : 17},
		{"id" : 10, "text" : "의서는", "type" : "", "begin" : 18, "end" : 19},
		{"id" : 11, "text" : "무엇일까?", "type" : "", "begin" : 20, "end" : 23}
	],
	"NE" : [
		{"id" : 0, "text" : "조선 세종 때", "type" : "DT_DYNASTY", "begin" : 1, "end" : 3, "weight" : 0.550583, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿조선", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.720772 },
		{"id" : 1, "text" : "세종", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.905862 },
		{"id" : 2, "text" : "때", "head" : 8, "label" : "NP_AJT", "mod" : [1], "weight" : 0.718481 },
		{"id" : 3, "text" : "우리", "head" : 4, "label" : "NP", "mod" : [], "weight" : 0.999382 },
		{"id" : 4, "text" : "고유의", "head" : 7, "label" : "NP_MOD", "mod" : [3], "weight" : 0.767869 },
		{"id" : 5, "text" : "약재와", "head" : 7, "label" : "NP_CNJ", "mod" : [], "weight" : 0.727442 },
		{"id" : 6, "text" : "치료", "head" : 7, "label" : "NP", "mod" : [], "weight" : 0.930667 },
		{"id" : 7, "text" : "방법을", "head" : 8, "label" : "NP_OBJ", "mod" : [4, 5, 6], "weight" : 0.817462 },
		{"id" : 8, "text" : "정리해", "head" : 9, "label" : "VP", "mod" : [2, 7], "weight" : 0.999602 },
		{"id" : 9, "text" : "편찬한", "head" : 10, "label" : "VP_MOD", "mod" : [8], "weight" : 0.754152 },
		{"id" : 10, "text" : "의서는", "head" : 11, "label" : "NP_SBJ", "mod" : [9], "weight" : 0.711911 },
		{"id" : 11, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [10], "weight" : 0.0744841 }
	],
	"SRL" : [
		{"verb" : "정리", "sense" : 1, "word_id" : 8, "weight" : 0.292213,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 2, "text" : "때", "weight" : 0.418072 },
				{"type" : "ARG1", "word_id" : 5, "text" : "약재와", "weight" : 0.20112 },
				{"type" : "ARG1", "word_id" : 7, "text" : "방법을", "weight" : 0.257448 }
			] },
		{"verb" : "편찬", "sense" : 1, "word_id" : 9, "weight" : 0.305065,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 2, "text" : "때", "weight" : 0.476016 },
				{"type" : "ARGM-MNR", "word_id" : 8, "text" : "정리해", "weight" : 0.137761 },
				{"type" : "ARG1", "word_id" : 10, "text" : "의서는", "weight" : 0.301418 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 8, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.67823 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 10, "ne_id" : -1, "text" : "﻿조선 세종 때 우리 고유의 약재와 치료 방법을 정리해 편찬한 의서", "start_eid_short" : 10, "end_eid_short" : 10, "text_short" : "의서", "weight" : 0.002 },
		{"id" : 8, "sent_id" : 0, "start_eid" : 10, "end_eid" : 11, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 11, "end_eid_short" : 11, "text_short" : "무엇이ㄹ까?", "weight" : 0.006 }
	] }
 ]
}

