{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "중국 삼국 시대 촉한 사람인 마량의 흰 눈썹에서 유래한 '이 말'은 사물이나 인물 가운데 가장 뛰어난 것을 뜻한다.",
	"morp" : [
		{"id" : 0, "lemma" : "중국", "type" : "NNP", "position" : 0, "weight" : 0.0892692 },
		{"id" : 1, "lemma" : "삼국", "type" : "NNG", "position" : 7, "weight" : 0.9 },
		{"id" : 2, "lemma" : "시대", "type" : "NNG", "position" : 14, "weight" : 0.9 },
		{"id" : 3, "lemma" : "촉하", "type" : "VA", "position" : 21, "weight" : 0.1 },
		{"id" : 4, "lemma" : "ㄴ", "type" : "ETM", "position" : 24, "weight" : 0.430446 },
		{"id" : 5, "lemma" : "사람", "type" : "NNG", "position" : 28, "weight" : 0.658826 },
		{"id" : 6, "lemma" : "이", "type" : "VCP", "position" : 34, "weight" : 0.0177525 },
		{"id" : 7, "lemma" : "ㄴ", "type" : "ETM", "position" : 34, "weight" : 0.220712 },
		{"id" : 8, "lemma" : "마량", "type" : "NNG", "position" : 38, "weight" : 0.25 },
		{"id" : 9, "lemma" : "의", "type" : "JKG", "position" : 44, "weight" : 0.0694213 },
		{"id" : 10, "lemma" : "희", "type" : "VA", "position" : 48, "weight" : 0.0345146 },
		{"id" : 11, "lemma" : "ㄴ", "type" : "ETM", "position" : 48, "weight" : 0.430446 },
		{"id" : 12, "lemma" : "눈썹", "type" : "NNG", "position" : 52, "weight" : 0.9 },
		{"id" : 13, "lemma" : "에서", "type" : "JKB", "position" : 58, "weight" : 0.153407 },
		{"id" : 14, "lemma" : "유래", "type" : "NNG", "position" : 65, "weight" : 0.9 },
		{"id" : 15, "lemma" : "하", "type" : "XSV", "position" : 71, "weight" : 0.0001 },
		{"id" : 16, "lemma" : "ㄴ", "type" : "ETM", "position" : 71, "weight" : 0.392321 },
		{"id" : 17, "lemma" : "'", "type" : "SS", "position" : 75, "weight" : 1 },
		{"id" : 18, "lemma" : "이", "type" : "MM", "position" : 76, "weight" : 0.000372718 },
		{"id" : 19, "lemma" : "말", "type" : "NNG", "position" : 80, "weight" : 0.592037 },
		{"id" : 20, "lemma" : "'", "type" : "SS", "position" : 83, "weight" : 1 },
		{"id" : 21, "lemma" : "은", "type" : "JX", "position" : 84, "weight" : 0.0128817 },
		{"id" : 22, "lemma" : "사물", "type" : "NNG", "position" : 88, "weight" : 0.9 },
		{"id" : 23, "lemma" : "이나", "type" : "JC", "position" : 94, "weight" : 0.0238189 },
		{"id" : 24, "lemma" : "인물", "type" : "NNG", "position" : 101, "weight" : 0.80287 },
		{"id" : 25, "lemma" : "가운데", "type" : "NNG", "position" : 108, "weight" : 0.9 },
		{"id" : 26, "lemma" : "가장", "type" : "MAG", "position" : 118, "weight" : 0.00496175 },
		{"id" : 27, "lemma" : "뛰어나", "type" : "VA", "position" : 125, "weight" : 0.9 },
		{"id" : 28, "lemma" : "ㄴ", "type" : "ETM", "position" : 131, "weight" : 0.430446 },
		{"id" : 29, "lemma" : "것", "type" : "NNB", "position" : 135, "weight" : 0.228788 },
		{"id" : 30, "lemma" : "을", "type" : "JKO", "position" : 138, "weight" : 0.0630104 },
		{"id" : 31, "lemma" : "뜻", "type" : "NNG", "position" : 142, "weight" : 0.9 },
		{"id" : 32, "lemma" : "하", "type" : "XSV", "position" : 145, "weight" : 0.0001 },
		{"id" : 33, "lemma" : "ㄴ다", "type" : "EF", "position" : 145, "weight" : 0.0449045 },
		{"id" : 34, "lemma" : ".", "type" : "SF", "position" : 151, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "중국/NNG", "target" : "중국", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "삼국/NNG", "target" : "삼국", "word_id" : 1, "m_begin" : 1, "m_end" : 1},
		{"id" : 2, "result" : "시대/NNG", "target" : "시대", "word_id" : 2, "m_begin" : 2, "m_end" : 2},
		{"id" : 3, "result" : "촉하/VA+ㄴ/ETM", "target" : "촉한", "word_id" : 3, "m_begin" : 3, "m_end" : 4},
		{"id" : 4, "result" : "사람/NNG+이/VCP+ㄴ/ETM", "target" : "사람인", "word_id" : 4, "m_begin" : 5, "m_end" : 7},
		{"id" : 5, "result" : "마량/NNG+의/JKG", "target" : "마량의", "word_id" : 5, "m_begin" : 8, "m_end" : 9},
		{"id" : 6, "result" : "희/VA+ㄴ/ETM", "target" : "흰", "word_id" : 6, "m_begin" : 10, "m_end" : 11},
		{"id" : 7, "result" : "눈썹/NNG+에서/JKB", "target" : "눈썹에서", "word_id" : 7, "m_begin" : 12, "m_end" : 13},
		{"id" : 8, "result" : "유래하/VV+ㄴ/ETM", "target" : "유래한", "word_id" : 8, "m_begin" : 14, "m_end" : 16},
		{"id" : 9, "result" : "'/SS+이/MM", "target" : "'이", "word_id" : 9, "m_begin" : 17, "m_end" : 18},
		{"id" : 10, "result" : "말/NNG+'/SS+은/JX", "target" : "말'은", "word_id" : 10, "m_begin" : 19, "m_end" : 21},
		{"id" : 11, "result" : "사물/NNG+이나/JC", "target" : "사물이나", "word_id" : 11, "m_begin" : 22, "m_end" : 23},
		{"id" : 12, "result" : "인물/NNG", "target" : "인물", "word_id" : 12, "m_begin" : 24, "m_end" : 24},
		{"id" : 13, "result" : "가운데/NNG", "target" : "가운데", "word_id" : 13, "m_begin" : 25, "m_end" : 25},
		{"id" : 14, "result" : "가장/MAG", "target" : "가장", "word_id" : 14, "m_begin" : 26, "m_end" : 26},
		{"id" : 15, "result" : "뛰어나/VA+ㄴ/ETM", "target" : "뛰어난", "word_id" : 15, "m_begin" : 27, "m_end" : 28},
		{"id" : 16, "result" : "것/NNB+을/JKO", "target" : "것을", "word_id" : 16, "m_begin" : 29, "m_end" : 30},
		{"id" : 17, "result" : "뜻하/VV+ㄴ다/EF+./SF", "target" : "뜻한다.", "word_id" : 17, "m_begin" : 31, "m_end" : 34}
	],
	"WSD" : [
		{"id" : 0, "text" : "중국", "type" : "NNP", "scode" : "01", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "삼국", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 7, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "시대", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 14, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "촉하", "type" : "VA", "scode" : "00", "weight" : 0, "position" : 21, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 24, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "사람", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 28, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 34, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 34, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "마량", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 38, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 44, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "희", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 48, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 48, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "눈썹", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 52, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 58, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "유래하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 65, "begin" : 14, "end" : 15},
		{"id" : 15, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 71, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 75, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 76, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "말", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 80, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "'", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 83, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 84, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : "사물", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 88, "begin" : 22, "end" : 22},
		{"id" : 22, "text" : "이나", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 94, "begin" : 23, "end" : 23},
		{"id" : 23, "text" : "인물", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 101, "begin" : 24, "end" : 24},
		{"id" : 24, "text" : "가운데", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 108, "begin" : 25, "end" : 25},
		{"id" : 25, "text" : "가장", "type" : "MAG", "scode" : "01", "weight" : 1, "position" : 118, "begin" : 26, "end" : 26},
		{"id" : 26, "text" : "뛰어나", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 125, "begin" : 27, "end" : 27},
		{"id" : 27, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 131, "begin" : 28, "end" : 28},
		{"id" : 28, "text" : "것", "type" : "NNB", "scode" : "01", "weight" : 1, "position" : 135, "begin" : 29, "end" : 29},
		{"id" : 29, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 138, "begin" : 30, "end" : 30},
		{"id" : 30, "text" : "뜻하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 142, "begin" : 31, "end" : 32},
		{"id" : 31, "text" : "ㄴ다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 145, "begin" : 33, "end" : 33},
		{"id" : 32, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 151, "begin" : 34, "end" : 34}
	],
	"word" : [
		{"id" : 0, "text" : "중국", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "삼국", "type" : "", "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "시대", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "촉한", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 4, "text" : "사람인", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 5, "text" : "마량의", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 6, "text" : "흰", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 7, "text" : "눈썹에서", "type" : "", "begin" : 12, "end" : 13},
		{"id" : 8, "text" : "유래한", "type" : "", "begin" : 14, "end" : 16},
		{"id" : 9, "text" : "'이", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 10, "text" : "말'은", "type" : "", "begin" : 19, "end" : 21},
		{"id" : 11, "text" : "사물이나", "type" : "", "begin" : 22, "end" : 23},
		{"id" : 12, "text" : "인물", "type" : "", "begin" : 24, "end" : 24},
		{"id" : 13, "text" : "가운데", "type" : "", "begin" : 25, "end" : 25},
		{"id" : 14, "text" : "가장", "type" : "", "begin" : 26, "end" : 26},
		{"id" : 15, "text" : "뛰어난", "type" : "", "begin" : 27, "end" : 28},
		{"id" : 16, "text" : "것을", "type" : "", "begin" : 29, "end" : 30},
		{"id" : 17, "text" : "뜻한다.", "type" : "", "begin" : 31, "end" : 34}
	],
	"NE" : [
		{"id" : 0, "text" : "중국", "type" : "LCP_COUNTRY", "begin" : 0, "end" : 0, "weight" : 0.592252, "common_noun" : 0},
		{"id" : 1, "text" : "삼국 시대", "type" : "DT_DYNASTY", "begin" : 1, "end" : 2, "weight" : 0.914812, "common_noun" : 0},
		{"id" : 2, "text" : "촉한", "type" : "LCP_COUNTRY", "begin" : 3, "end" : 4, "weight" : 0.425336, "common_noun" : 0},
		{"id" : 3, "text" : "마량", "type" : "PS_NAME", "begin" : 8, "end" : 8, "weight" : 0.451657, "common_noun" : 0},
		{"id" : 4, "text" : "눈썹", "type" : "AM_PART", "begin" : 12, "end" : 12, "weight" : 0.390012, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "중국", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.656308 },
		{"id" : 1, "text" : "삼국", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.662388 },
		{"id" : 2, "text" : "시대", "head" : 3, "label" : "NP_SBJ", "mod" : [1], "weight" : 0.184316 },
		{"id" : 3, "text" : "촉한", "head" : 4, "label" : "VP_MOD", "mod" : [2], "weight" : 0.617033 },
		{"id" : 4, "text" : "사람인", "head" : 5, "label" : "VNP_MOD", "mod" : [3], "weight" : 0.264893 },
		{"id" : 5, "text" : "마량의", "head" : 7, "label" : "NP_MOD", "mod" : [4], "weight" : 0.736352 },
		{"id" : 6, "text" : "흰", "head" : 7, "label" : "VP_MOD", "mod" : [], "weight" : 0.724007 },
		{"id" : 7, "text" : "눈썹에서", "head" : 8, "label" : "NP_AJT", "mod" : [5, 6], "weight" : 0.810918 },
		{"id" : 8, "text" : "유래한", "head" : 10, "label" : "VP_MOD", "mod" : [7], "weight" : 0.675633 },
		{"id" : 9, "text" : "'이", "head" : 10, "label" : "DP", "mod" : [], "weight" : 0.498008 },
		{"id" : 10, "text" : "말'은", "head" : 17, "label" : "NP_SBJ", "mod" : [8, 9], "weight" : 0.794416 },
		{"id" : 11, "text" : "사물이나", "head" : 12, "label" : "NP_CNJ", "mod" : [], "weight" : 0.582919 },
		{"id" : 12, "text" : "인물", "head" : 13, "label" : "NP", "mod" : [11], "weight" : 0.706776 },
		{"id" : 13, "text" : "가운데", "head" : 15, "label" : "NP_AJT", "mod" : [12], "weight" : 0.160345 },
		{"id" : 14, "text" : "가장", "head" : 15, "label" : "AP", "mod" : [], "weight" : 0.824507 },
		{"id" : 15, "text" : "뛰어난", "head" : 16, "label" : "VP_MOD", "mod" : [13, 14], "weight" : 0.81547 },
		{"id" : 16, "text" : "것을", "head" : 17, "label" : "NP_OBJ", "mod" : [15], "weight" : 0.745511 },
		{"id" : 17, "text" : "뜻한다.", "head" : -1, "label" : "VP", "mod" : [10, 16], "weight" : 4.57265e-05 }
	],
	"SRL" : [
		{"verb" : "촉하", "sense" : 1, "word_id" : 3, "weight" : 0.2108,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 2, "text" : "시대", "weight" : 0.2108 }
			] },
		{"verb" : "희", "sense" : 1, "word_id" : 6, "weight" : 0.628241,
			"argument" : [
				{"type" : "ARG1", "word_id" : 7, "text" : "눈썹에서", "weight" : 0.628241 }
			] },
		{"verb" : "유래", "sense" : 1, "word_id" : 8, "weight" : 0.392363,
			"argument" : [
				{"type" : "ARG2", "word_id" : 7, "text" : "눈썹에서", "weight" : 0.188393 },
				{"type" : "ARG1", "word_id" : 10, "text" : "말'은", "weight" : 0.596334 }
			] },
		{"verb" : "뛰어나", "sense" : 1, "word_id" : 15, "weight" : 0.381331,
			"argument" : [
				{"type" : "ARGM-EXT", "word_id" : 14, "text" : "가장", "weight" : 0.437751 },
				{"type" : "ARG1", "word_id" : 16, "text" : "것을", "weight" : 0.32491 }
			] },
		{"verb" : "뜻", "sense" : 1, "word_id" : 17, "weight" : 0.53454,
			"argument" : [
				{"type" : "ARG1", "word_id" : 10, "text" : "말'은", "weight" : 0.346267 },
				{"type" : "ARG2", "word_id" : 16, "text" : "것을", "weight" : 0.722813 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "이 말은 무엇일까?",
	"morp" : [
		{"id" : 0, "lemma" : "이", "type" : "MM", "position" : 152, "weight" : 0.0020222 },
		{"id" : 1, "lemma" : "말", "type" : "NNG", "position" : 156, "weight" : 0.592037 },
		{"id" : 2, "lemma" : "은", "type" : "JX", "position" : 159, "weight" : 0.0449928 },
		{"id" : 3, "lemma" : "무엇", "type" : "NP", "position" : 163, "weight" : 0.9 },
		{"id" : 4, "lemma" : "이", "type" : "VCP", "position" : 169, "weight" : 0.0175768 },
		{"id" : 5, "lemma" : "ㄹ까", "type" : "EF", "position" : 169, "weight" : 0.258243 },
		{"id" : 6, "lemma" : "?", "type" : "SF", "position" : 175, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이/MM", "target" : "이", "word_id" : 0, "m_begin" : 0, "m_end" : 0},
		{"id" : 1, "result" : "말/NNG+은/JX", "target" : "말은", "word_id" : 1, "m_begin" : 1, "m_end" : 2},
		{"id" : 2, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 2, "m_begin" : 3, "m_end" : 6}
	],
	"WSD" : [
		{"id" : 0, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 152, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "말", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 156, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 159, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 163, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 169, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 169, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 175, "begin" : 6, "end" : 6}
	],
	"word" : [
		{"id" : 0, "text" : "이", "type" : "", "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "말은", "type" : "", "begin" : 1, "end" : 2},
		{"id" : 2, "text" : "무엇일까?", "type" : "", "begin" : 3, "end" : 6}
	],
	"NE" : [
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이", "head" : 1, "label" : "DP", "mod" : [], "weight" : 0.634046 },
		{"id" : 1, "text" : "말은", "head" : 2, "label" : "NP_SBJ", "mod" : [0], "weight" : 0.778413 },
		{"id" : 2, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [1], "weight" : 0.43965 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 5, "sent_id" : 0, "start_eid" : 9, "end_eid" : 10, "ne_id" : -1, "text" : "'이 말'", "start_eid_short" : 9, "end_eid_short" : 10, "text_short" : "'이 말'", "weight" : 0 },
		{"id" : 10, "sent_id" : 1, "start_eid" : 0, "end_eid" : 1, "ne_id" : -1, "text" : "이 말", "start_eid_short" : 0, "end_eid_short" : 1, "text_short" : "이 말", "weight" : 0.006 },
		{"id" : 11, "sent_id" : 1, "start_eid" : 2, "end_eid" : 2, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 2, "end_eid_short" : 2, "text_short" : "무엇이ㄹ까?", "weight" : 0.006 }
	] }
 ]
}
