{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "﻿1909년에는 유길준의 <조선문전>, 광무 초년엔 한국 최초의 문법책 <조선문전(朝鮮文典)>을 내었다. ",
	"morp" : [
		{"id" : 0, "lemma" : "﻿", "type" : "XPN", "position" : 0, "weight" : 0 },
		{"id" : 1, "lemma" : "1909", "type" : "SN", "position" : 3, "weight" : 1 },
		{"id" : 2, "lemma" : "년", "type" : "NNB", "position" : 7, "weight" : 0.414343 },
		{"id" : 3, "lemma" : "에", "type" : "JKB", "position" : 10, "weight" : 0.135559 },
		{"id" : 4, "lemma" : "는", "type" : "JX", "position" : 13, "weight" : 0.0387928 },
		{"id" : 5, "lemma" : "유길준", "type" : "NNP", "position" : 17, "weight" : 0.45 },
		{"id" : 6, "lemma" : "의", "type" : "JKG", "position" : 26, "weight" : 0.0987295 },
		{"id" : 7, "lemma" : "<", "type" : "SS", "position" : 30, "weight" : 1 },
		{"id" : 8, "lemma" : "조선", "type" : "NNP", "position" : 31, "weight" : 0.0663851 },
		{"id" : 9, "lemma" : "문전", "type" : "NNG", "position" : 37, "weight" : 0.9 },
		{"id" : 10, "lemma" : ">", "type" : "SS", "position" : 43, "weight" : 1 },
		{"id" : 11, "lemma" : ",", "type" : "SP", "position" : 44, "weight" : 1 },
		{"id" : 12, "lemma" : "광무", "type" : "NNG", "position" : 46, "weight" : 0.529288 },
		{"id" : 13, "lemma" : "초년", "type" : "NNG", "position" : 53, "weight" : 0.9 },
		{"id" : 14, "lemma" : "에", "type" : "JKB", "position" : 59, "weight" : 0.153364 },
		{"id" : 15, "lemma" : "ㄴ", "type" : "JX", "position" : 59, "weight" : 0.00384955 },
		{"id" : 16, "lemma" : "한국", "type" : "NNP", "position" : 63, "weight" : 0.0448181 },
		{"id" : 17, "lemma" : "최초", "type" : "NNG", "position" : 70, "weight" : 0.9 },
		{"id" : 18, "lemma" : "의", "type" : "JKG", "position" : 76, "weight" : 0.0694213 },
		{"id" : 19, "lemma" : "문법", "type" : "NNG", "position" : 80, "weight" : 0.9 },
		{"id" : 20, "lemma" : "책", "type" : "NNG", "position" : 86, "weight" : 0.18015 },
		{"id" : 21, "lemma" : "<", "type" : "SS", "position" : 90, "weight" : 1 },
		{"id" : 22, "lemma" : "조선", "type" : "NNP", "position" : 91, "weight" : 0.0663851 },
		{"id" : 23, "lemma" : "문전", "type" : "NNG", "position" : 97, "weight" : 0.9 },
		{"id" : 24, "lemma" : "(", "type" : "SS", "position" : 103, "weight" : 1 },
		{"id" : 25, "lemma" : "朝鮮文典", "type" : "SH", "position" : 104, "weight" : 1 },
		{"id" : 26, "lemma" : ")", "type" : "SS", "position" : 116, "weight" : 1 },
		{"id" : 27, "lemma" : ">", "type" : "SS", "position" : 117, "weight" : 1 },
		{"id" : 28, "lemma" : "을", "type" : "JKO", "position" : 118, "weight" : 0.0336085 },
		{"id" : 29, "lemma" : "내", "type" : "VV", "position" : 122, "weight" : 0.254295 },
		{"id" : 30, "lemma" : "었", "type" : "EP", "position" : 125, "weight" : 0.9 },
		{"id" : 31, "lemma" : "다", "type" : "EF", "position" : 128, "weight" : 0.640954 },
		{"id" : 32, "lemma" : ".", "type" : "SF", "position" : 131, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "﻿/XPN+1909/SN+년/NNB+에/JKB+는/JX", "target" : "﻿1909년에는", "word_id" : 0, "m_begin" : 0, "m_end" : 4},
		{"id" : 1, "result" : "유길준/NNG+의/JKG", "target" : "유길준의", "word_id" : 1, "m_begin" : 5, "m_end" : 6},
		{"id" : 2, "result" : "</SS+조선문전/NNG+>/SS+,/SP", "target" : "<조선문전>,", "word_id" : 2, "m_begin" : 7, "m_end" : 11},
		{"id" : 3, "result" : "광무/NNG", "target" : "광무", "word_id" : 3, "m_begin" : 12, "m_end" : 12},
		{"id" : 4, "result" : "초년/NNG+에/JKB+ㄴ/JX", "target" : "초년엔", "word_id" : 4, "m_begin" : 13, "m_end" : 15},
		{"id" : 5, "result" : "한국/NNG", "target" : "한국", "word_id" : 5, "m_begin" : 16, "m_end" : 16},
		{"id" : 6, "result" : "최초/NNG+의/JKG", "target" : "최초의", "word_id" : 6, "m_begin" : 17, "m_end" : 18},
		{"id" : 7, "result" : "문법책/NNG", "target" : "문법책", "word_id" : 7, "m_begin" : 19, "m_end" : 20},
		{"id" : 8, "result" : "</SS+조선문전/NNG+(/SS+朝鮮文典/SH+)/SS+>/SS+을/JKO", "target" : "<조선문전(朝鮮文典)>을", "word_id" : 8, "m_begin" : 21, "m_end" : 28},
		{"id" : 9, "result" : "내/VV+었/EP+다/EF+./SF", "target" : "내었다.", "word_id" : 9, "m_begin" : 29, "m_end" : 32}
	],
	"WSD" : [
		{"id" : 0, "text" : "﻿", "type" : "XPN", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "1909", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 3, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 7, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 13, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "유길준", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 17, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 26, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "<", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 30, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "조선문전", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 31, "begin" : 8, "end" : 9},
		{"id" : 9, "text" : ">", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 43, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 44, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "광무", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 46, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "초년", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 53, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 59, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "ㄴ", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 59, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "한국", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 63, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "최초", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 70, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 76, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "문법", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 80, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "책", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 86, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "<", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 90, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : "조선문전", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 91, "begin" : 22, "end" : 23},
		{"id" : 22, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 103, "begin" : 24, "end" : 24},
		{"id" : 23, "text" : "朝鮮文典", "type" : "SH", "scode" : "00", "weight" : 1, "position" : 104, "begin" : 25, "end" : 25},
		{"id" : 24, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 116, "begin" : 26, "end" : 26},
		{"id" : 25, "text" : ">", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 117, "begin" : 27, "end" : 27},
		{"id" : 26, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 118, "begin" : 28, "end" : 28},
		{"id" : 27, "text" : "내", "type" : "VV", "scode" : "02", "weight" : 1, "position" : 122, "begin" : 29, "end" : 29},
		{"id" : 28, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 125, "begin" : 30, "end" : 30},
		{"id" : 29, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 128, "begin" : 31, "end" : 31},
		{"id" : 30, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 131, "begin" : 32, "end" : 32}
	],
	"word" : [
		{"id" : 0, "text" : "﻿1909년에는", "type" : "", "begin" : 0, "end" : 4},
		{"id" : 1, "text" : "유길준의", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 2, "text" : "<조선문전>,", "type" : "", "begin" : 7, "end" : 11},
		{"id" : 3, "text" : "광무", "type" : "", "begin" : 12, "end" : 12},
		{"id" : 4, "text" : "초년엔", "type" : "", "begin" : 13, "end" : 15},
		{"id" : 5, "text" : "한국", "type" : "", "begin" : 16, "end" : 16},
		{"id" : 6, "text" : "최초의", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 7, "text" : "문법책", "type" : "", "begin" : 19, "end" : 20},
		{"id" : 8, "text" : "<조선문전(朝鮮文典)>을", "type" : "", "begin" : 21, "end" : 28},
		{"id" : 9, "text" : "내었다.", "type" : "", "begin" : 29, "end" : 32}
	],
	"NE" : [
		{"id" : 0, "text" : "1909년", "type" : "DT_YEAR", "begin" : 1, "end" : 2, "weight" : 0.312243, "common_noun" : 0},
		{"id" : 1, "text" : "조선문전", "type" : "AF_WORKS", "begin" : 8, "end" : 9, "weight" : 0.445712, "common_noun" : 0},
		{"id" : 2, "text" : "광무 초년", "type" : "DT_DYNASTY", "begin" : 12, "end" : 13, "weight" : 0.368514, "common_noun" : 0},
		{"id" : 3, "text" : "한국", "type" : "LCP_COUNTRY", "begin" : 16, "end" : 16, "weight" : 0.468984, "common_noun" : 0},
		{"id" : 4, "text" : "조선문전", "type" : "AF_WORKS", "begin" : 22, "end" : 23, "weight" : 0.505882, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "﻿1909년에는", "head" : 2, "label" : "NP_AJT", "mod" : [], "weight" : 0.501926 },
		{"id" : 1, "text" : "유길준의", "head" : 2, "label" : "NP_MOD", "mod" : [], "weight" : 0.961575 },
		{"id" : 2, "text" : "<조선문전>,", "head" : 9, "label" : "NP", "mod" : [0, 1], "weight" : 0.481845 },
		{"id" : 3, "text" : "광무", "head" : 4, "label" : "NP", "mod" : [], "weight" : 0.9949 },
		{"id" : 4, "text" : "초년엔", "head" : 9, "label" : "NP_AJT", "mod" : [3], "weight" : 0.725722 },
		{"id" : 5, "text" : "한국", "head" : 6, "label" : "NP", "mod" : [], "weight" : 0.771197 },
		{"id" : 6, "text" : "최초의", "head" : 7, "label" : "NP_MOD", "mod" : [5], "weight" : 0.61993 },
		{"id" : 7, "text" : "문법책", "head" : 8, "label" : "NP", "mod" : [6], "weight" : 0.99395 },
		{"id" : 8, "text" : "<조선문전(朝鮮文典)>을", "head" : 9, "label" : "NP_OBJ", "mod" : [7], "weight" : 0.560554 },
		{"id" : 9, "text" : "내었다.", "head" : -1, "label" : "VP", "mod" : [2, 4, 8], "weight" : 0.0285935 }
	],
	"SRL" : [
		{"verb" : "내", "sense" : 1, "word_id" : 9, "weight" : 0.25957,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 4, "text" : "초년엔", "weight" : 0.244682 },
				{"type" : "ARG1", "word_id" : 8, "text" : "<조선문전(朝鮮文典)>을", "weight" : 0.274458 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 9, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.905828 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "AF_WORKS", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 2, "sent_id" : 0, "start_eid" : 2, "end_eid" : 2, "ne_id" : 1, "text" : "<조선문전>", "start_eid_short" : 2, "end_eid_short" : 2, "text_short" : "<조선문전>,", "weight" : 0.006 },
		{"id" : 7, "sent_id" : 0, "start_eid" : 8, "end_eid" : 8, "ne_id" : 4, "text" : "<조선문전(朝鮮文典)>", "start_eid_short" : 8, "end_eid_short" : 8, "text_short" : "<조선문전(朝鮮文典)>", "weight" : 0.01 }
	] }
 ]
}

