{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "11세기경 잉글랜드 셔우드 숲을 근거지로 활약한 의적이다.",
	"morp" : [
		{"id" : 0, "lemma" : "11", "type" : "SN", "position" : 0, "weight" : 1 },
		{"id" : 1, "lemma" : "세기", "type" : "NNG", "position" : 2, "weight" : 0.100259 },
		{"id" : 2, "lemma" : "경", "type" : "XSN", "position" : 8, "weight" : 0.0245919 },
		{"id" : 3, "lemma" : "잉글랜드", "type" : "NNP", "position" : 12, "weight" : 0.9 },
		{"id" : 4, "lemma" : "셔우드", "type" : "NNP", "position" : 25, "weight" : 0.15 },
		{"id" : 5, "lemma" : "숲", "type" : "NNG", "position" : 35, "weight" : 0.9 },
		{"id" : 6, "lemma" : "을", "type" : "JKO", "position" : 38, "weight" : 0.129611 },
		{"id" : 7, "lemma" : "근거", "type" : "NNG", "position" : 42, "weight" : 0.9 },
		{"id" : 8, "lemma" : "지", "type" : "XSN", "position" : 48, "weight" : 0.000168337 },
		{"id" : 9, "lemma" : "로", "type" : "JKB", "position" : 51, "weight" : 0.121577 },
		{"id" : 10, "lemma" : "활약", "type" : "NNG", "position" : 55, "weight" : 0.9 },
		{"id" : 11, "lemma" : "하", "type" : "XSV", "position" : 61, "weight" : 0.0001 },
		{"id" : 12, "lemma" : "ㄴ", "type" : "ETM", "position" : 61, "weight" : 0.392321 },
		{"id" : 13, "lemma" : "의적", "type" : "NNG", "position" : 65, "weight" : 0.560015 },
		{"id" : 14, "lemma" : "이", "type" : "VCP", "position" : 71, "weight" : 0.0177525 },
		{"id" : 15, "lemma" : "다", "type" : "EF", "position" : 74, "weight" : 0.353579 },
		{"id" : 16, "lemma" : ".", "type" : "SF", "position" : 77, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "11/SN+세기경/NNG", "target" : "11세기경", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "잉글랜드/NNG", "target" : "잉글랜드", "word_id" : 1, "m_begin" : 3, "m_end" : 3},
		{"id" : 2, "result" : "셔우드/NNG", "target" : "셔우드", "word_id" : 2, "m_begin" : 4, "m_end" : 4},
		{"id" : 3, "result" : "숲/NNG+을/JKO", "target" : "숲을", "word_id" : 3, "m_begin" : 5, "m_end" : 6},
		{"id" : 4, "result" : "근거지/NNG+로/JKB", "target" : "근거지로", "word_id" : 4, "m_begin" : 7, "m_end" : 9},
		{"id" : 5, "result" : "활약하/VV+ㄴ/ETM", "target" : "활약한", "word_id" : 5, "m_begin" : 10, "m_end" : 12},
		{"id" : 6, "result" : "의적/NNG+이/VCP+다/EF+./SF", "target" : "의적이다.", "word_id" : 6, "m_begin" : 13, "m_end" : 16}
	],
	"WSD" : [
		{"id" : 0, "text" : "11", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "세기", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 2, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "경", "type" : "XSN", "scode" : "25", "weight" : 1, "position" : 8, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "잉글랜드", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 12, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "셔우드", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 25, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "숲", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 35, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 38, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "근거지", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 42, "begin" : 7, "end" : 8},
		{"id" : 8, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 51, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "활약하", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 55, "begin" : 10, "end" : 11},
		{"id" : 10, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 61, "begin" : 12, "end" : 12},
		{"id" : 11, "text" : "의적", "type" : "NNG", "scode" : "03", "weight" : 1, "position" : 65, "begin" : 13, "end" : 13},
		{"id" : 12, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 71, "begin" : 14, "end" : 14},
		{"id" : 13, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 74, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 77, "begin" : 16, "end" : 16}
	],
	"word" : [
		{"id" : 0, "text" : "11세기경", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "잉글랜드", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 2, "text" : "셔우드", "type" : "", "begin" : 4, "end" : 4},
		{"id" : 3, "text" : "숲을", "type" : "", "begin" : 5, "end" : 6},
		{"id" : 4, "text" : "근거지로", "type" : "", "begin" : 7, "end" : 9},
		{"id" : 5, "text" : "활약한", "type" : "", "begin" : 10, "end" : 12},
		{"id" : 6, "text" : "의적이다.", "type" : "", "begin" : 13, "end" : 16}
	],
	"NE" : [
		{"id" : 0, "text" : "11세기경", "type" : "DT_OTHERS", "begin" : 0, "end" : 2, "weight" : 0.559475, "common_noun" : 0},
		{"id" : 1, "text" : "잉글랜드", "type" : "LCP_COUNTRY", "begin" : 3, "end" : 3, "weight" : 0.606346, "common_noun" : 0},
		{"id" : 2, "text" : "셔우드 숲", "type" : "LC_OTHERS", "begin" : 4, "end" : 5, "weight" : 0.257249, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "11세기경", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.0772797 },
		{"id" : 1, "text" : "잉글랜드", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.614379 },
		{"id" : 2, "text" : "셔우드", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.794192 },
		{"id" : 3, "text" : "숲을", "head" : 5, "label" : "NP_OBJ", "mod" : [0, 1, 2], "weight" : 0.850741 },
		{"id" : 4, "text" : "근거지로", "head" : 5, "label" : "NP_AJT", "mod" : [], "weight" : 0.653617 },
		{"id" : 5, "text" : "활약한", "head" : 6, "label" : "VP_MOD", "mod" : [3, 4], "weight" : 0.813621 },
		{"id" : 6, "text" : "의적이다.", "head" : -1, "label" : "VNP", "mod" : [5], "weight" : 0.0138275 }
	],
	"SRL" : [
		{"verb" : "활약", "sense" : 1, "word_id" : 5, "weight" : 0.197172,
			"argument" : [
				{"type" : "ARG1", "word_id" : 3, "text" : "숲을", "weight" : 0.301102 },
				{"type" : "ARGM-MNR", "word_id" : 4, "text" : "근거지로", "weight" : 0.104712 },
				{"type" : "ARG0", "word_id" : 6, "text" : "의적이다.", "weight" : 0.185703 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "포악한 귀족과 성직자의 재산을 빼앗고 가난한 사람을 도왔다고 알려진 이 인물은 누구일까?",
	"morp" : [
		{"id" : 0, "lemma" : "포악", "type" : "NNG", "position" : 78, "weight" : 0.9 },
		{"id" : 1, "lemma" : "하", "type" : "XSA", "position" : 84, "weight" : 0.0001 },
		{"id" : 2, "lemma" : "ㄴ", "type" : "ETM", "position" : 84, "weight" : 0.488779 },
		{"id" : 3, "lemma" : "귀족", "type" : "NNG", "position" : 88, "weight" : 0.9 },
		{"id" : 4, "lemma" : "과", "type" : "JC", "position" : 94, "weight" : 0.017569 },
		{"id" : 5, "lemma" : "성직", "type" : "NNG", "position" : 98, "weight" : 0.9 },
		{"id" : 6, "lemma" : "자", "type" : "XSN", "position" : 104, "weight" : 0.00398741 },
		{"id" : 7, "lemma" : "의", "type" : "JKG", "position" : 107, "weight" : 0.120732 },
		{"id" : 8, "lemma" : "재산", "type" : "NNG", "position" : 111, "weight" : 0.9 },
		{"id" : 9, "lemma" : "을", "type" : "JKO", "position" : 117, "weight" : 0.129611 },
		{"id" : 10, "lemma" : "빼앗", "type" : "VV", "position" : 121, "weight" : 0.9 },
		{"id" : 11, "lemma" : "고", "type" : "EC", "position" : 127, "weight" : 0.416679 },
		{"id" : 12, "lemma" : "가난", "type" : "NNG", "position" : 131, "weight" : 0.9 },
		{"id" : 13, "lemma" : "하", "type" : "XSA", "position" : 137, "weight" : 0.0001 },
		{"id" : 14, "lemma" : "ㄴ", "type" : "ETM", "position" : 137, "weight" : 0.488779 },
		{"id" : 15, "lemma" : "사람", "type" : "NNG", "position" : 141, "weight" : 0.658826 },
		{"id" : 16, "lemma" : "을", "type" : "JKO", "position" : 147, "weight" : 0.129611 },
		{"id" : 17, "lemma" : "돕", "type" : "VV", "position" : 151, "weight" : 0.777582 },
		{"id" : 18, "lemma" : "았", "type" : "EP", "position" : 151, "weight" : 0.9 },
		{"id" : 19, "lemma" : "다고", "type" : "EC", "position" : 157, "weight" : 0.193823 },
		{"id" : 20, "lemma" : "알려지", "type" : "VV", "position" : 164, "weight" : 0.9 },
		{"id" : 21, "lemma" : "ㄴ", "type" : "ETM", "position" : 170, "weight" : 0.304215 },
		{"id" : 22, "lemma" : "이", "type" : "MM", "position" : 174, "weight" : 0.00060084 },
		{"id" : 23, "lemma" : "인물", "type" : "NNG", "position" : 178, "weight" : 0.737262 },
		{"id" : 24, "lemma" : "은", "type" : "JX", "position" : 184, "weight" : 0.0449928 },
		{"id" : 25, "lemma" : "누구", "type" : "NP", "position" : 188, "weight" : 0.9 },
		{"id" : 26, "lemma" : "이", "type" : "VCP", "position" : 194, "weight" : 0.0175768 },
		{"id" : 27, "lemma" : "ㄹ까", "type" : "EF", "position" : 194, "weight" : 0.258243 },
		{"id" : 28, "lemma" : "?", "type" : "SF", "position" : 200, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "포악하/VA+ㄴ/ETM", "target" : "포악한", "word_id" : 0, "m_begin" : 0, "m_end" : 2},
		{"id" : 1, "result" : "귀족/NNG+과/JC", "target" : "귀족과", "word_id" : 1, "m_begin" : 3, "m_end" : 4},
		{"id" : 2, "result" : "성직자/NNG+의/JKG", "target" : "성직자의", "word_id" : 2, "m_begin" : 5, "m_end" : 7},
		{"id" : 3, "result" : "재산/NNG+을/JKO", "target" : "재산을", "word_id" : 3, "m_begin" : 8, "m_end" : 9},
		{"id" : 4, "result" : "빼앗/VV+고/EC", "target" : "빼앗고", "word_id" : 4, "m_begin" : 10, "m_end" : 11},
		{"id" : 5, "result" : "가난하/VA+ㄴ/ETM", "target" : "가난한", "word_id" : 5, "m_begin" : 12, "m_end" : 14},
		{"id" : 6, "result" : "사람/NNG+을/JKO", "target" : "사람을", "word_id" : 6, "m_begin" : 15, "m_end" : 16},
		{"id" : 7, "result" : "돕/VV+었/EP+다고/EC", "target" : "도왔다고", "word_id" : 7, "m_begin" : 17, "m_end" : 19},
		{"id" : 8, "result" : "알려지/VV+ㄴ/ETM", "target" : "알려진", "word_id" : 8, "m_begin" : 20, "m_end" : 21},
		{"id" : 9, "result" : "이/MM", "target" : "이", "word_id" : 9, "m_begin" : 22, "m_end" : 22},
		{"id" : 10, "result" : "인물/NNG+은/JX", "target" : "인물은", "word_id" : 10, "m_begin" : 23, "m_end" : 24},
		{"id" : 11, "result" : "누구/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "누구일까?", "word_id" : 11, "m_begin" : 25, "m_end" : 28}
	],
	"WSD" : [
		{"id" : 0, "text" : "포악하", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 78, "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 84, "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "귀족", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 88, "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "과", "type" : "JC", "scode" : "00", "weight" : 1, "position" : 94, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "성직자", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 98, "begin" : 5, "end" : 6},
		{"id" : 5, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 107, "begin" : 7, "end" : 7},
		{"id" : 6, "text" : "재산", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 111, "begin" : 8, "end" : 8},
		{"id" : 7, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 117, "begin" : 9, "end" : 9},
		{"id" : 8, "text" : "빼앗", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 121, "begin" : 10, "end" : 10},
		{"id" : 9, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 127, "begin" : 11, "end" : 11},
		{"id" : 10, "text" : "가난하", "type" : "VA", "scode" : "00", "weight" : 1, "position" : 131, "begin" : 12, "end" : 13},
		{"id" : 11, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 137, "begin" : 14, "end" : 14},
		{"id" : 12, "text" : "사람", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 141, "begin" : 15, "end" : 15},
		{"id" : 13, "text" : "을", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 147, "begin" : 16, "end" : 16},
		{"id" : 14, "text" : "돕", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 151, "begin" : 17, "end" : 17},
		{"id" : 15, "text" : "았", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 151, "begin" : 18, "end" : 18},
		{"id" : 16, "text" : "다고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 157, "begin" : 19, "end" : 19},
		{"id" : 17, "text" : "알려지", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 164, "begin" : 20, "end" : 20},
		{"id" : 18, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 170, "begin" : 21, "end" : 21},
		{"id" : 19, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 174, "begin" : 22, "end" : 22},
		{"id" : 20, "text" : "인물", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 178, "begin" : 23, "end" : 23},
		{"id" : 21, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 184, "begin" : 24, "end" : 24},
		{"id" : 22, "text" : "누구", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 188, "begin" : 25, "end" : 25},
		{"id" : 23, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 194, "begin" : 26, "end" : 26},
		{"id" : 24, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 194, "begin" : 27, "end" : 27},
		{"id" : 25, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 200, "begin" : 28, "end" : 28}
	],
	"word" : [
		{"id" : 0, "text" : "포악한", "type" : "", "begin" : 0, "end" : 2},
		{"id" : 1, "text" : "귀족과", "type" : "", "begin" : 3, "end" : 4},
		{"id" : 2, "text" : "성직자의", "type" : "", "begin" : 5, "end" : 7},
		{"id" : 3, "text" : "재산을", "type" : "", "begin" : 8, "end" : 9},
		{"id" : 4, "text" : "빼앗고", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 5, "text" : "가난한", "type" : "", "begin" : 12, "end" : 14},
		{"id" : 6, "text" : "사람을", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 7, "text" : "도왔다고", "type" : "", "begin" : 17, "end" : 19},
		{"id" : 8, "text" : "알려진", "type" : "", "begin" : 20, "end" : 21},
		{"id" : 9, "text" : "이", "type" : "", "begin" : 22, "end" : 22},
		{"id" : 10, "text" : "인물은", "type" : "", "begin" : 23, "end" : 24},
		{"id" : 11, "text" : "누구일까?", "type" : "", "begin" : 25, "end" : 28}
	],
	"NE" : [
		{"id" : 0, "text" : "귀족", "type" : "CV_POSITION", "begin" : 3, "end" : 3, "weight" : 0.365693, "common_noun" : 0},
		{"id" : 1, "text" : "성직자", "type" : "CV_OCCUPATION", "begin" : 5, "end" : 6, "weight" : 0.906073, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "포악한", "head" : 2, "label" : "VP_MOD", "mod" : [], "weight" : 0.42608 },
		{"id" : 1, "text" : "귀족과", "head" : 2, "label" : "NP_CNJ", "mod" : [], "weight" : 0.459402 },
		{"id" : 2, "text" : "성직자의", "head" : 3, "label" : "NP_MOD", "mod" : [0, 1], "weight" : 0.648707 },
		{"id" : 3, "text" : "재산을", "head" : 4, "label" : "NP_OBJ", "mod" : [2], "weight" : 0.788409 },
		{"id" : 4, "text" : "빼앗고", "head" : 7, "label" : "VP", "mod" : [3], "weight" : 0.745029 },
		{"id" : 5, "text" : "가난한", "head" : 6, "label" : "VP_MOD", "mod" : [], "weight" : 0.736182 },
		{"id" : 6, "text" : "사람을", "head" : 7, "label" : "NP_OBJ", "mod" : [5], "weight" : 0.824199 },
		{"id" : 7, "text" : "도왔다고", "head" : 8, "label" : "VP_CMP", "mod" : [4, 6], "weight" : 0.591166 },
		{"id" : 8, "text" : "알려진", "head" : 10, "label" : "VP_MOD", "mod" : [7], "weight" : 0.769877 },
		{"id" : 9, "text" : "이", "head" : 10, "label" : "DP", "mod" : [], "weight" : 0.695754 },
		{"id" : 10, "text" : "인물은", "head" : 11, "label" : "NP_SBJ", "mod" : [8, 9], "weight" : 0.808423 },
		{"id" : 11, "text" : "누구일까?", "head" : -1, "label" : "VNP", "mod" : [10], "weight" : 0.0101781 }
	],
	"SRL" : [
		{"verb" : "포악", "sense" : 1, "word_id" : 0, "weight" : 0,
			"argument" : [
			] },
		{"verb" : "빼앗", "sense" : 1, "word_id" : 4, "weight" : 0.248826,
			"argument" : [
				{"type" : "ARG1", "word_id" : 3, "text" : "재산을", "weight" : 0.248826 }
			] },
		{"verb" : "가난", "sense" : 1, "word_id" : 5, "weight" : 0.764364,
			"argument" : [
				{"type" : "ARG1", "word_id" : 6, "text" : "사람을", "weight" : 0.764364 }
			] },
		{"verb" : "돕", "sense" : 1, "word_id" : 7, "weight" : 0.607795,
			"argument" : [
				{"type" : "ARG1", "word_id" : 6, "text" : "사람을", "weight" : 0.607795 }
			] },
		{"verb" : "알려지", "sense" : 1, "word_id" : 8, "weight" : 0.243696,
			"argument" : [
				{"type" : "ARG1", "word_id" : 7, "text" : "도왔다고", "weight" : 0.227653 },
				{"type" : "ARG1", "word_id" : 10, "text" : "인물은", "weight" : 0.25974 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 4, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.998544 },
		{"id" : 1, "verb_wid" : 7, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.994194 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 6, "ne_id" : -1, "text" : "11세기경 잉글랜드 셔우드 숲을 근거지로 활약한 의적이다.", "start_eid_short" : 6, "end_eid_short" : 6, "text_short" : "의적이다.", "weight" : 0 },
		{"id" : 10, "sent_id" : 1, "start_eid" : 9, "end_eid" : 10, "ne_id" : -1, "text" : "이 인물", "start_eid_short" : 9, "end_eid_short" : 10, "text_short" : "이 인물", "weight" : 0.006 },
		{"id" : 11, "sent_id" : 1, "start_eid" : 11, "end_eid" : 11, "ne_id" : -1, "text" : "누구", "start_eid_short" : 11, "end_eid_short" : 11, "text_short" : "누구이ㄹ까?", "weight" : 0.006 }
	] }
 ]
}
