{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "지현곤(1961년8월 12일 - )는 대한민국의 만화가이다.",
	"morp" : [
		{"id" : 0, "lemma" : "지현곤", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "(", "type" : "SS", "position" : 9, "weight" : 1 },
		{"id" : 2, "lemma" : "1961", "type" : "SN", "position" : 10, "weight" : 1 },
		{"id" : 3, "lemma" : "년", "type" : "NNB", "position" : 14, "weight" : 0.414343 },
		{"id" : 4, "lemma" : "8", "type" : "SN", "position" : 17, "weight" : 1 },
		{"id" : 5, "lemma" : "월", "type" : "NNB", "position" : 18, "weight" : 0.408539 },
		{"id" : 6, "lemma" : "12", "type" : "SN", "position" : 22, "weight" : 1 },
		{"id" : 7, "lemma" : "일", "type" : "NNB", "position" : 24, "weight" : 0.126777 },
		{"id" : 8, "lemma" : "-", "type" : "SO", "position" : 28, "weight" : 1 },
		{"id" : 9, "lemma" : ")", "type" : "SS", "position" : 30, "weight" : 1 },
		{"id" : 10, "lemma" : "는", "type" : "JX", "position" : 31, "weight" : 0.00823314 },
		{"id" : 11, "lemma" : "대한민국", "type" : "NNP", "position" : 35, "weight" : 0.0447775 },
		{"id" : 12, "lemma" : "의", "type" : "JKG", "position" : 47, "weight" : 0.0987295 },
		{"id" : 13, "lemma" : "만화", "type" : "NNG", "position" : 51, "weight" : 0.83848 },
		{"id" : 14, "lemma" : "가", "type" : "XSN", "position" : 57, "weight" : 0.000115417 },
		{"id" : 15, "lemma" : "이", "type" : "VCP", "position" : 60, "weight" : 0.0165001 },
		{"id" : 16, "lemma" : "다", "type" : "EF", "position" : 63, "weight" : 0.353579 },
		{"id" : 17, "lemma" : ".", "type" : "SF", "position" : 66, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "지현곤/NNG+(/SS+1961/SN+년/NNB+8/SN+월/NNB", "target" : "지현곤(1961년8월", "word_id" : 0, "m_begin" : 0, "m_end" : 5},
		{"id" : 1, "result" : "12/SN+일/NNB", "target" : "12일", "word_id" : 1, "m_begin" : 6, "m_end" : 7},
		{"id" : 2, "result" : "-/SO", "target" : "-", "word_id" : 2, "m_begin" : 8, "m_end" : 8},
		{"id" : 3, "result" : ")/SS+는/JX", "target" : ")는", "word_id" : 3, "m_begin" : 9, "m_end" : 10},
		{"id" : 4, "result" : "대한민국/NNG+의/JKG", "target" : "대한민국의", "word_id" : 4, "m_begin" : 11, "m_end" : 12},
		{"id" : 5, "result" : "만화가/NNG+이/VCP+다/EF+./SF", "target" : "만화가이다.", "word_id" : 5, "m_begin" : 13, "m_end" : 17}
	],
	"WSD" : [
		{"id" : 0, "text" : "지현곤", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "1961", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 14, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "8", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 17, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "월", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 18, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "12", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 22, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "일", "type" : "NNB", "scode" : "07", "weight" : 1, "position" : 24, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "-", "type" : "SO", "scode" : "00", "weight" : 1, "position" : 28, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 30, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 31, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "대한민국", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 35, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 47, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : "만화가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 51, "begin" : 13, "end" : 14},
		{"id" : 14, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 60, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 63, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 66, "begin" : 17, "end" : 17}
	],
	"word" : [
		{"id" : 0, "text" : "지현곤(1961년8월", "type" : "", "begin" : 0, "end" : 5},
		{"id" : 1, "text" : "12일", "type" : "", "begin" : 6, "end" : 7},
		{"id" : 2, "text" : "-", "type" : "", "begin" : 8, "end" : 8},
		{"id" : 3, "text" : ")는", "type" : "", "begin" : 9, "end" : 10},
		{"id" : 4, "text" : "대한민국의", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 5, "text" : "만화가이다.", "type" : "", "begin" : 13, "end" : 17}
	],
	"NE" : [
		{"id" : 0, "text" : "지현곤", "type" : "PS_NAME", "begin" : 0, "end" : 0, "weight" : 0.315582, "common_noun" : 0},
		{"id" : 1, "text" : "1961년8월 12일 -", "type" : "DT_OTHERS", "begin" : 2, "end" : 8, "weight" : 0.732514, "common_noun" : 0},
		{"id" : 2, "text" : "대한민국", "type" : "LCP_COUNTRY", "begin" : 11, "end" : 11, "weight" : 0.174772, "common_noun" : 0},
		{"id" : 3, "text" : "만화가", "type" : "CV_OCCUPATION", "begin" : 13, "end" : 14, "weight" : 0.284924, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "지현곤(1961년8월", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.504624 },
		{"id" : 1, "text" : "12일", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.557772 },
		{"id" : 2, "text" : "-", "head" : 3, "label" : "X", "mod" : [1], "weight" : 0.649819 },
		{"id" : 3, "text" : ")는", "head" : 5, "label" : "NP_SBJ", "mod" : [2], "weight" : 0.449921 },
		{"id" : 4, "text" : "대한민국의", "head" : 5, "label" : "NP_MOD", "mod" : [], "weight" : 0.42139 },
		{"id" : 5, "text" : "만화가이다.", "head" : -1, "label" : "VNP", "mod" : [3, 4], "weight" : 0.0196441 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
 ]
}

