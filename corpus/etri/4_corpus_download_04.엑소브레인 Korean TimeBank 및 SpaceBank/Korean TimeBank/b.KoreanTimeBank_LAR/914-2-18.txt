{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "이것은 고려 왕조 시기에 만들어진 탑으로 일반적인 홀수 층의 탑이 아닌 짝수 층의 탑이다.",
	"morp" : [
		{"id" : 0, "lemma" : "이것", "type" : "NP", "position" : 0, "weight" : 0.0733657 },
		{"id" : 1, "lemma" : "은", "type" : "JX", "position" : 6, "weight" : 0.191811 },
		{"id" : 2, "lemma" : "고려", "type" : "NNP", "position" : 10, "weight" : 0.042748 },
		{"id" : 3, "lemma" : "왕조", "type" : "NNG", "position" : 17, "weight" : 0.9 },
		{"id" : 4, "lemma" : "시기", "type" : "NNG", "position" : 24, "weight" : 0.184726 },
		{"id" : 5, "lemma" : "에", "type" : "JKB", "position" : 30, "weight" : 0.153364 },
		{"id" : 6, "lemma" : "만들", "type" : "VV", "position" : 34, "weight" : 0.9 },
		{"id" : 7, "lemma" : "어", "type" : "EC", "position" : 40, "weight" : 0.41831 },
		{"id" : 8, "lemma" : "지", "type" : "VX", "position" : 43, "weight" : 0.0539764 },
		{"id" : 9, "lemma" : "ㄴ", "type" : "ETM", "position" : 43, "weight" : 0.302612 },
		{"id" : 10, "lemma" : "탑", "type" : "NNG", "position" : 47, "weight" : 0.9 },
		{"id" : 11, "lemma" : "으로", "type" : "JKB", "position" : 50, "weight" : 0.153406 },
		{"id" : 12, "lemma" : "일반", "type" : "NNG", "position" : 57, "weight" : 0.9 },
		{"id" : 13, "lemma" : "적", "type" : "XSN", "position" : 63, "weight" : 0.0168756 },
		{"id" : 14, "lemma" : "이", "type" : "VCP", "position" : 66, "weight" : 0.0165001 },
		{"id" : 15, "lemma" : "ㄴ", "type" : "ETM", "position" : 66, "weight" : 0.220712 },
		{"id" : 16, "lemma" : "홀수", "type" : "NNG", "position" : 70, "weight" : 0.9 },
		{"id" : 17, "lemma" : "층", "type" : "NNG", "position" : 77, "weight" : 0.141586 },
		{"id" : 18, "lemma" : "의", "type" : "JKG", "position" : 80, "weight" : 0.0694213 },
		{"id" : 19, "lemma" : "탑", "type" : "NNG", "position" : 84, "weight" : 0.9 },
		{"id" : 20, "lemma" : "이", "type" : "JKC", "position" : 87, "weight" : 0.000287945 },
		{"id" : 21, "lemma" : "아니", "type" : "VCN", "position" : 91, "weight" : 0.354175 },
		{"id" : 22, "lemma" : "ㄴ", "type" : "ETM", "position" : 94, "weight" : 0.144018 },
		{"id" : 23, "lemma" : "짝수", "type" : "NNG", "position" : 98, "weight" : 0.9 },
		{"id" : 24, "lemma" : "층", "type" : "NNG", "position" : 105, "weight" : 0.141586 },
		{"id" : 25, "lemma" : "의", "type" : "JKG", "position" : 108, "weight" : 0.0694213 },
		{"id" : 26, "lemma" : "탑", "type" : "NNG", "position" : 112, "weight" : 0.9 },
		{"id" : 27, "lemma" : "이", "type" : "VCP", "position" : 115, "weight" : 0.0177525 },
		{"id" : 28, "lemma" : "다", "type" : "EF", "position" : 118, "weight" : 0.353579 },
		{"id" : 29, "lemma" : ".", "type" : "SF", "position" : 121, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이것/NP+은/JX", "target" : "이것은", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "고려/NNG", "target" : "고려", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "왕조/NNG", "target" : "왕조", "word_id" : 2, "m_begin" : 3, "m_end" : 3},
		{"id" : 3, "result" : "시기/NNG+에/JKB", "target" : "시기에", "word_id" : 3, "m_begin" : 4, "m_end" : 5},
		{"id" : 4, "result" : "만들/VV+어/EC+지/VX+ㄴ/ETM", "target" : "만들어진", "word_id" : 4, "m_begin" : 6, "m_end" : 9},
		{"id" : 5, "result" : "탑/NNG+으로/JKB", "target" : "탑으로", "word_id" : 5, "m_begin" : 10, "m_end" : 11},
		{"id" : 6, "result" : "일반적/NNG+이/VCP+ㄴ/ETM", "target" : "일반적인", "word_id" : 6, "m_begin" : 12, "m_end" : 15},
		{"id" : 7, "result" : "홀수/NNG", "target" : "홀수", "word_id" : 7, "m_begin" : 16, "m_end" : 16},
		{"id" : 8, "result" : "층/NNG+의/JKG", "target" : "층의", "word_id" : 8, "m_begin" : 17, "m_end" : 18},
		{"id" : 9, "result" : "탑/NNG+이/JKC", "target" : "탑이", "word_id" : 9, "m_begin" : 19, "m_end" : 20},
		{"id" : 10, "result" : "아니/VCN+ㄴ/ETM", "target" : "아닌", "word_id" : 10, "m_begin" : 21, "m_end" : 22},
		{"id" : 11, "result" : "짝수/NNG", "target" : "짝수", "word_id" : 11, "m_begin" : 23, "m_end" : 23},
		{"id" : 12, "result" : "층/NNG+의/JKG", "target" : "층의", "word_id" : 12, "m_begin" : 24, "m_end" : 25},
		{"id" : 13, "result" : "탑/NNG+이/VCP+다/EF+./SF", "target" : "탑이다.", "word_id" : 13, "m_begin" : 26, "m_end" : 29}
	],
	"WSD" : [
		{"id" : 0, "text" : "이것", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 6, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "고려", "type" : "NNP", "scode" : "05", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "왕조", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 17, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "시기", "type" : "NNG", "scode" : "04", "weight" : 1, "position" : 24, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 30, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "만들", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 34, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "지", "type" : "VX", "scode" : "04", "weight" : 1, "position" : 43, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 43, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "탑", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 47, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 50, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "일반적", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 57, "begin" : 12, "end" : 13},
		{"id" : 13, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 66, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 66, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "홀수", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 70, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "층", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 77, "begin" : 17, "end" : 17},
		{"id" : 17, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 80, "begin" : 18, "end" : 18},
		{"id" : 18, "text" : "탑", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 84, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "이", "type" : "JKC", "scode" : "00", "weight" : 1, "position" : 87, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : "아니", "type" : "VCN", "scode" : "00", "weight" : 1, "position" : 91, "begin" : 21, "end" : 21},
		{"id" : 21, "text" : "ㄴ", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 94, "begin" : 22, "end" : 22},
		{"id" : 22, "text" : "짝수", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 98, "begin" : 23, "end" : 23},
		{"id" : 23, "text" : "층", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 105, "begin" : 24, "end" : 24},
		{"id" : 24, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 108, "begin" : 25, "end" : 25},
		{"id" : 25, "text" : "탑", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 112, "begin" : 26, "end" : 26},
		{"id" : 26, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 115, "begin" : 27, "end" : 27},
		{"id" : 27, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 118, "begin" : 28, "end" : 28},
		{"id" : 28, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 121, "begin" : 29, "end" : 29}
	],
	"word" : [
		{"id" : 0, "text" : "이것은", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "고려", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "왕조", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "시기에", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 4, "text" : "만들어진", "type" : "", "begin" : 6, "end" : 9},
		{"id" : 5, "text" : "탑으로", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 6, "text" : "일반적인", "type" : "", "begin" : 12, "end" : 15},
		{"id" : 7, "text" : "홀수", "type" : "", "begin" : 16, "end" : 16},
		{"id" : 8, "text" : "층의", "type" : "", "begin" : 17, "end" : 18},
		{"id" : 9, "text" : "탑이", "type" : "", "begin" : 19, "end" : 20},
		{"id" : 10, "text" : "아닌", "type" : "", "begin" : 21, "end" : 22},
		{"id" : 11, "text" : "짝수", "type" : "", "begin" : 23, "end" : 23},
		{"id" : 12, "text" : "층의", "type" : "", "begin" : 24, "end" : 25},
		{"id" : 13, "text" : "탑이다.", "type" : "", "begin" : 26, "end" : 29}
	],
	"NE" : [
		{"id" : 0, "text" : "고려 왕조", "type" : "DT_DYNASTY", "begin" : 2, "end" : 3, "weight" : 0.737976, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이것은", "head" : 5, "label" : "NP_SBJ", "mod" : [], "weight" : 0.564157 },
		{"id" : 1, "text" : "고려", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.684852 },
		{"id" : 2, "text" : "왕조", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.955816 },
		{"id" : 3, "text" : "시기에", "head" : 4, "label" : "NP_AJT", "mod" : [2], "weight" : 0.817799 },
		{"id" : 4, "text" : "만들어진", "head" : 5, "label" : "VP_MOD", "mod" : [3], "weight" : 0.881845 },
		{"id" : 5, "text" : "탑으로", "head" : 10, "label" : "NP_AJT", "mod" : [0, 4], "weight" : 0.580217 },
		{"id" : 6, "text" : "일반적인", "head" : 9, "label" : "VNP_MOD", "mod" : [], "weight" : 0.692017 },
		{"id" : 7, "text" : "홀수", "head" : 8, "label" : "NP", "mod" : [], "weight" : 0.68364 },
		{"id" : 8, "text" : "층의", "head" : 9, "label" : "NP_MOD", "mod" : [7], "weight" : 0.877293 },
		{"id" : 9, "text" : "탑이", "head" : 10, "label" : "NP_CMP", "mod" : [6, 8], "weight" : 0.75717 },
		{"id" : 10, "text" : "아닌", "head" : 13, "label" : "VP_MOD", "mod" : [5, 9], "weight" : 0.539355 },
		{"id" : 11, "text" : "짝수", "head" : 12, "label" : "NP", "mod" : [], "weight" : 0.761202 },
		{"id" : 12, "text" : "층의", "head" : 13, "label" : "NP_MOD", "mod" : [11], "weight" : 0.456327 },
		{"id" : 13, "text" : "탑이다.", "head" : -1, "label" : "VNP", "mod" : [10, 12], "weight" : 0.00546596 }
	],
	"SRL" : [
		{"verb" : "만들", "sense" : 1, "word_id" : 4, "weight" : 0.386083,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 3, "text" : "시기에", "weight" : 0.255629 },
				{"type" : "ARG1", "word_id" : 5, "text" : "탑으로", "weight" : 0.516538 }
			] },
		{"verb" : "아니", "sense" : 1, "word_id" : 10, "weight" : 0.296626,
			"argument" : [
				{"type" : "ARG2", "word_id" : 9, "text" : "탑이", "weight" : 0.263594 },
				{"type" : "ARG1", "word_id" : 13, "text" : "탑이다.", "weight" : 0.329658 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 13, "ant_sid" : 0, "ant_wid" : 0, "type" : "s", "istitle" : 0, "weight" : 0.54003 }
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "1900년대 초에 일본으로 반출됐다가 다시 돌려받았고 현재 국립중앙박물관에 전시돼 있는 이 탑은 무엇일까? ",
	"morp" : [
		{"id" : 0, "lemma" : "1900", "type" : "SN", "position" : 122, "weight" : 1 },
		{"id" : 1, "lemma" : "년대", "type" : "NNB", "position" : 126, "weight" : 0.9 },
		{"id" : 2, "lemma" : "초", "type" : "NNB", "position" : 133, "weight" : 0.00733198 },
		{"id" : 3, "lemma" : "에", "type" : "JKB", "position" : 136, "weight" : 0.135559 },
		{"id" : 4, "lemma" : "일본", "type" : "NNP", "position" : 140, "weight" : 0.0230184 },
		{"id" : 5, "lemma" : "으로", "type" : "JKB", "position" : 146, "weight" : 0.0823853 },
		{"id" : 6, "lemma" : "반출", "type" : "NNG", "position" : 153, "weight" : 0.9 },
		{"id" : 7, "lemma" : "되", "type" : "XSV", "position" : 159, "weight" : 0.000224177 },
		{"id" : 8, "lemma" : "었", "type" : "EP", "position" : 159, "weight" : 0.9 },
		{"id" : 9, "lemma" : "다가", "type" : "EC", "position" : 162, "weight" : 0.188685 },
		{"id" : 10, "lemma" : "다시", "type" : "MAG", "position" : 169, "weight" : 0.0469628 },
		{"id" : 11, "lemma" : "돌려받", "type" : "VV", "position" : 176, "weight" : 0.9 },
		{"id" : 12, "lemma" : "았", "type" : "EP", "position" : 185, "weight" : 0.9 },
		{"id" : 13, "lemma" : "고", "type" : "EC", "position" : 188, "weight" : 0.190901 },
		{"id" : 14, "lemma" : "현재", "type" : "MAG", "position" : 192, "weight" : 0.0283089 },
		{"id" : 15, "lemma" : "국립중앙박물관", "type" : "NNP", "position" : 199, "weight" : 0.9 },
		{"id" : 16, "lemma" : "에", "type" : "JKB", "position" : 220, "weight" : 0.0823628 },
		{"id" : 17, "lemma" : "전시", "type" : "NNG", "position" : 224, "weight" : 0.20775 },
		{"id" : 18, "lemma" : "되", "type" : "XSV", "position" : 230, "weight" : 0.000224177 },
		{"id" : 19, "lemma" : "어", "type" : "EC", "position" : 230, "weight" : 0.361326 },
		{"id" : 20, "lemma" : "있", "type" : "VX", "position" : 234, "weight" : 0.125953 },
		{"id" : 21, "lemma" : "는", "type" : "ETM", "position" : 237, "weight" : 0.183966 },
		{"id" : 22, "lemma" : "이", "type" : "MM", "position" : 241, "weight" : 0.00060084 },
		{"id" : 23, "lemma" : "탑", "type" : "NNG", "position" : 245, "weight" : 0.9 },
		{"id" : 24, "lemma" : "은", "type" : "JX", "position" : 248, "weight" : 0.0449928 },
		{"id" : 25, "lemma" : "무엇", "type" : "NP", "position" : 252, "weight" : 0.9 },
		{"id" : 26, "lemma" : "이", "type" : "VCP", "position" : 258, "weight" : 0.0175768 },
		{"id" : 27, "lemma" : "ㄹ까", "type" : "EF", "position" : 258, "weight" : 0.258243 },
		{"id" : 28, "lemma" : "?", "type" : "SF", "position" : 264, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "1900/SN+년대/NNB", "target" : "1900년대", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "초/NNB+에/JKB", "target" : "초에", "word_id" : 1, "m_begin" : 2, "m_end" : 3},
		{"id" : 2, "result" : "일본/NNG+으로/JKB", "target" : "일본으로", "word_id" : 2, "m_begin" : 4, "m_end" : 5},
		{"id" : 3, "result" : "반출되/VV+었/EP+다가/EC", "target" : "반출됐다가", "word_id" : 3, "m_begin" : 6, "m_end" : 9},
		{"id" : 4, "result" : "다시/MAG", "target" : "다시", "word_id" : 4, "m_begin" : 10, "m_end" : 10},
		{"id" : 5, "result" : "돌려받/VV+었/EP+고/EC", "target" : "돌려받았고", "word_id" : 5, "m_begin" : 11, "m_end" : 13},
		{"id" : 6, "result" : "현재/MAG", "target" : "현재", "word_id" : 6, "m_begin" : 14, "m_end" : 14},
		{"id" : 7, "result" : "국립중앙박물관/NNG+에/JKB", "target" : "국립중앙박물관에", "word_id" : 7, "m_begin" : 15, "m_end" : 16},
		{"id" : 8, "result" : "전시되/VV+어/EC", "target" : "전시돼", "word_id" : 8, "m_begin" : 17, "m_end" : 19},
		{"id" : 9, "result" : "있/VX+는/ETM", "target" : "있는", "word_id" : 9, "m_begin" : 20, "m_end" : 21},
		{"id" : 10, "result" : "이/MM", "target" : "이", "word_id" : 10, "m_begin" : 22, "m_end" : 22},
		{"id" : 11, "result" : "탑/NNG+은/JX", "target" : "탑은", "word_id" : 11, "m_begin" : 23, "m_end" : 24},
		{"id" : 12, "result" : "무엇/NP+이/VCP+ㄹ까/EF+?/SF", "target" : "무엇일까?", "word_id" : 12, "m_begin" : 25, "m_end" : 28}
	],
	"WSD" : [
		{"id" : 0, "text" : "1900", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 122, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년대", "type" : "NNB", "scode" : "00", "weight" : 1, "position" : 126, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "초", "type" : "NNB", "scode" : "03", "weight" : 1, "position" : 133, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 136, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "일본", "type" : "NNP", "scode" : "02", "weight" : 1, "position" : 140, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 146, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "반출되", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 153, "begin" : 6, "end" : 7},
		{"id" : 7, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 159, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "다가", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 162, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "다시", "type" : "MAG", "scode" : "01", "weight" : 1, "position" : 169, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "돌려받", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 176, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "았", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 185, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "고", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 188, "begin" : 13, "end" : 13},
		{"id" : 13, "text" : "현재", "type" : "MAG", "scode" : "02", "weight" : 1, "position" : 192, "begin" : 14, "end" : 14},
		{"id" : 14, "text" : "국립중앙박물관", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 199, "begin" : 15, "end" : 15},
		{"id" : 15, "text" : "에", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 220, "begin" : 16, "end" : 16},
		{"id" : 16, "text" : "전시되", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 224, "begin" : 17, "end" : 18},
		{"id" : 17, "text" : "어", "type" : "EC", "scode" : "00", "weight" : 1, "position" : 230, "begin" : 19, "end" : 19},
		{"id" : 18, "text" : "있", "type" : "VX", "scode" : "01", "weight" : 1, "position" : 234, "begin" : 20, "end" : 20},
		{"id" : 19, "text" : "는", "type" : "ETM", "scode" : "00", "weight" : 1, "position" : 237, "begin" : 21, "end" : 21},
		{"id" : 20, "text" : "이", "type" : "MM", "scode" : "05", "weight" : 1, "position" : 241, "begin" : 22, "end" : 22},
		{"id" : 21, "text" : "탑", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 245, "begin" : 23, "end" : 23},
		{"id" : 22, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 248, "begin" : 24, "end" : 24},
		{"id" : 23, "text" : "무엇", "type" : "NP", "scode" : "00", "weight" : 1, "position" : 252, "begin" : 25, "end" : 25},
		{"id" : 24, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 258, "begin" : 26, "end" : 26},
		{"id" : 25, "text" : "ㄹ까", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 258, "begin" : 27, "end" : 27},
		{"id" : 26, "text" : "?", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 264, "begin" : 28, "end" : 28}
	],
	"word" : [
		{"id" : 0, "text" : "1900년대", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "초에", "type" : "", "begin" : 2, "end" : 3},
		{"id" : 2, "text" : "일본으로", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 3, "text" : "반출됐다가", "type" : "", "begin" : 6, "end" : 9},
		{"id" : 4, "text" : "다시", "type" : "", "begin" : 10, "end" : 10},
		{"id" : 5, "text" : "돌려받았고", "type" : "", "begin" : 11, "end" : 13},
		{"id" : 6, "text" : "현재", "type" : "", "begin" : 14, "end" : 14},
		{"id" : 7, "text" : "국립중앙박물관에", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 8, "text" : "전시돼", "type" : "", "begin" : 17, "end" : 19},
		{"id" : 9, "text" : "있는", "type" : "", "begin" : 20, "end" : 21},
		{"id" : 10, "text" : "이", "type" : "", "begin" : 22, "end" : 22},
		{"id" : 11, "text" : "탑은", "type" : "", "begin" : 23, "end" : 24},
		{"id" : 12, "text" : "무엇일까?", "type" : "", "begin" : 25, "end" : 28}
	],
	"NE" : [
		{"id" : 0, "text" : "1900년대 초", "type" : "DT_OTHERS", "begin" : 0, "end" : 2, "weight" : 0.747977, "common_noun" : 0},
		{"id" : 1, "text" : "일본", "type" : "LCP_COUNTRY", "begin" : 4, "end" : 4, "weight" : 0.42051, "common_noun" : 0},
		{"id" : 2, "text" : "국립중앙박물관", "type" : "OGG_ART", "begin" : 15, "end" : 15, "weight" : 0.433958, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "1900년대", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.931073 },
		{"id" : 1, "text" : "초에", "head" : 3, "label" : "NP_AJT", "mod" : [0], "weight" : 0.853588 },
		{"id" : 2, "text" : "일본으로", "head" : 3, "label" : "NP_AJT", "mod" : [], "weight" : 0.67781 },
		{"id" : 3, "text" : "반출됐다가", "head" : 5, "label" : "VP", "mod" : [1, 2], "weight" : 0.776269 },
		{"id" : 4, "text" : "다시", "head" : 5, "label" : "AP", "mod" : [], "weight" : 0.813339 },
		{"id" : 5, "text" : "돌려받았고", "head" : 8, "label" : "VP", "mod" : [3, 4], "weight" : 0.632697 },
		{"id" : 6, "text" : "현재", "head" : 8, "label" : "AP", "mod" : [], "weight" : 0.900046 },
		{"id" : 7, "text" : "국립중앙박물관에", "head" : 8, "label" : "NP_AJT", "mod" : [], "weight" : 0.852891 },
		{"id" : 8, "text" : "전시돼", "head" : 9, "label" : "VP", "mod" : [5, 6, 7], "weight" : 0.816737 },
		{"id" : 9, "text" : "있는", "head" : 11, "label" : "VP_MOD", "mod" : [8], "weight" : 0.859359 },
		{"id" : 10, "text" : "이", "head" : 11, "label" : "DP", "mod" : [], "weight" : 0.930453 },
		{"id" : 11, "text" : "탑은", "head" : 12, "label" : "NP_SBJ", "mod" : [9, 10], "weight" : 0.747403 },
		{"id" : 12, "text" : "무엇일까?", "head" : -1, "label" : "VNP", "mod" : [11], "weight" : 0.0563216 }
	],
	"SRL" : [
		{"verb" : "반출", "sense" : 1, "word_id" : 3, "weight" : 0.205053,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 1, "text" : "초에", "weight" : 0.253973 },
				{"type" : "ARG2", "word_id" : 2, "text" : "일본으로", "weight" : 0.156132 }
			] },
		{"verb" : "돌려받", "sense" : 1, "word_id" : 5, "weight" : 0.277408,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 4, "text" : "다시", "weight" : 0.277408 }
			] },
		{"verb" : "전시", "sense" : 1, "word_id" : 8, "weight" : 0.3062,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 6, "text" : "현재", "weight" : 0.211686 },
				{"type" : "ARG2", "word_id" : 7, "text" : "국립중앙박물관에", "weight" : 0.292927 },
				{"type" : "AUX", "word_id" : 9, "text" : "있는", "weight" : 0.506131 },
				{"type" : "ARG1", "word_id" : 11, "text" : "탑은", "weight" : 0.214055 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 3, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.642043 },
		{"id" : 1, "verb_wid" : 5, "ant_sid" : 0, "ant_wid" : 0, "type" : "s", "istitle" : 0, "weight" : 0.297229 },
		{"id" : 2, "verb_wid" : 5, "ant_sid" : 0, "ant_wid" : 0, "type" : "o", "istitle" : 0, "weight" : 0.255119 }
	]
	}
 ],
 "entity" : [
	{"id" : 0, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 0, "sent_id" : 0, "start_eid" : 0, "end_eid" : 5, "ne_id" : -1, "text" : "이것은 고려 왕조 시기에 만들어진 탑", "start_eid_short" : 5, "end_eid_short" : 5, "text_short" : "탑", "weight" : 0.002 },
		{"id" : 1, "sent_id" : 0, "start_eid" : 0, "end_eid" : 0, "ne_id" : -1, "text" : "이것", "start_eid_short" : 0, "end_eid_short" : 0, "text_short" : "이것", "weight" : 0.018 },
		{"id" : 5, "sent_id" : 0, "start_eid" : 6, "end_eid" : 9, "ne_id" : -1, "text" : "일반적인 홀수 층의 탑", "start_eid_short" : 7, "end_eid_short" : 9, "text_short" : "홀수 층의 탑", "weight" : 0.0035 },
		{"id" : 13, "sent_id" : 1, "start_eid" : 10, "end_eid" : 11, "ne_id" : -1, "text" : "이 탑", "start_eid_short" : 10, "end_eid_short" : 11, "text_short" : "이 탑", "weight" : 0.006 },
		{"id" : 15, "sent_id" : 1, "start_eid" : 12, "end_eid" : 12, "ne_id" : -1, "text" : "무엇", "start_eid_short" : 12, "end_eid_short" : 12, "text_short" : "무엇이ㄹ까?", "weight" : 0.006 }
	] },
	{"id" : 1, "type" : "", "number" : "singular", "gender" : "", "person" : "", "animacy" : "", "loc" : "", 
	 "mention" : [
		{"id" : 6, "sent_id" : 0, "start_eid" : 7, "end_eid" : 8, "ne_id" : -1, "text" : "홀수 층", "start_eid_short" : 7, "end_eid_short" : 8, "text_short" : "홀수 층", "weight" : 0.002 },
		{"id" : 8, "sent_id" : 0, "start_eid" : 11, "end_eid" : 12, "ne_id" : -1, "text" : "짝수 층", "start_eid_short" : 11, "end_eid_short" : 12, "text_short" : "짝수 층", "weight" : 0.0035 }
	] }
 ]
}

