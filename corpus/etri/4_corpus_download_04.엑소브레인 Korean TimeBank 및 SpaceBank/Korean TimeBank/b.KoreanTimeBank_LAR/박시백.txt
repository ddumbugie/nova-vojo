{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "박시백(1964년 ~)은 대한민국의 만화가이다.",
	"morp" : [
		{"id" : 0, "lemma" : "박시백", "type" : "NNP", "position" : 0, "weight" : 0.6 },
		{"id" : 1, "lemma" : "(", "type" : "SS", "position" : 9, "weight" : 1 },
		{"id" : 2, "lemma" : "1964", "type" : "SN", "position" : 10, "weight" : 1 },
		{"id" : 3, "lemma" : "년", "type" : "NNB", "position" : 14, "weight" : 0.414343 },
		{"id" : 4, "lemma" : "~", "type" : "SO", "position" : 18, "weight" : 1 },
		{"id" : 5, "lemma" : ")", "type" : "SS", "position" : 19, "weight" : 1 },
		{"id" : 6, "lemma" : "은", "type" : "JX", "position" : 20, "weight" : 0.0128817 },
		{"id" : 7, "lemma" : "대한민국", "type" : "NNP", "position" : 24, "weight" : 0.0447775 },
		{"id" : 8, "lemma" : "의", "type" : "JKG", "position" : 36, "weight" : 0.0987295 },
		{"id" : 9, "lemma" : "만화", "type" : "NNG", "position" : 40, "weight" : 0.83848 },
		{"id" : 10, "lemma" : "가", "type" : "XSN", "position" : 46, "weight" : 0.000115417 },
		{"id" : 11, "lemma" : "이", "type" : "VCP", "position" : 49, "weight" : 0.0165001 },
		{"id" : 12, "lemma" : "다", "type" : "EF", "position" : 52, "weight" : 0.353579 },
		{"id" : 13, "lemma" : ".", "type" : "SF", "position" : 55, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "박시백/NNG+(/SS+1964/SN+년/NNB", "target" : "박시백(1964년", "word_id" : 0, "m_begin" : 0, "m_end" : 3},
		{"id" : 1, "result" : "~/SO+)/SS+은/JX", "target" : "~)은", "word_id" : 1, "m_begin" : 4, "m_end" : 6},
		{"id" : 2, "result" : "대한민국/NNG+의/JKG", "target" : "대한민국의", "word_id" : 2, "m_begin" : 7, "m_end" : 8},
		{"id" : 3, "result" : "만화가/NNG+이/VCP+다/EF+./SF", "target" : "만화가이다.", "word_id" : 3, "m_begin" : 9, "m_end" : 13}
	],
	"WSD" : [
		{"id" : 0, "text" : "박시백", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "1964", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 14, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "~", "type" : "SO", "scode" : "00", "weight" : 1, "position" : 18, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 19, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 20, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "대한민국", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 24, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 36, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "만화가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 40, "begin" : 9, "end" : 10},
		{"id" : 10, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 49, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 52, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 55, "begin" : 13, "end" : 13}
	],
	"word" : [
		{"id" : 0, "text" : "박시백(1964년", "type" : "", "begin" : 0, "end" : 3},
		{"id" : 1, "text" : "~)은", "type" : "", "begin" : 4, "end" : 6},
		{"id" : 2, "text" : "대한민국의", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 3, "text" : "만화가이다.", "type" : "", "begin" : 9, "end" : 13}
	],
	"NE" : [
		{"id" : 0, "text" : "박시백", "type" : "PS_NAME", "begin" : 0, "end" : 0, "weight" : 0.463764, "common_noun" : 0},
		{"id" : 1, "text" : "1964년", "type" : "DT_YEAR", "begin" : 2, "end" : 3, "weight" : 0.715946, "common_noun" : 0},
		{"id" : 2, "text" : "대한민국", "type" : "LCP_COUNTRY", "begin" : 7, "end" : 7, "weight" : 0.190467, "common_noun" : 0},
		{"id" : 3, "text" : "만화가", "type" : "CV_OCCUPATION", "begin" : 9, "end" : 10, "weight" : 0.285804, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "박시백(1964년", "head" : 1, "label" : "NP", "mod" : [], "weight" : 0.803018 },
		{"id" : 1, "text" : "~)은", "head" : 3, "label" : "NP_SBJ", "mod" : [0], "weight" : 0.561218 },
		{"id" : 2, "text" : "대한민국의", "head" : 3, "label" : "NP_MOD", "mod" : [], "weight" : 0.436551 },
		{"id" : 3, "text" : "만화가이다.", "head" : -1, "label" : "VNP", "mod" : [1, 2], "weight" : 0.114748 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "대표작으로는 《박시백의 조선왕조실록》 시리즈가 있다.",
	"morp" : [
		{"id" : 0, "lemma" : "대표", "type" : "NNG", "position" : 56, "weight" : 0.9 },
		{"id" : 1, "lemma" : "작", "type" : "XSN", "position" : 62, "weight" : 0.00128498 },
		{"id" : 2, "lemma" : "으로", "type" : "JKB", "position" : 65, "weight" : 0.121717 },
		{"id" : 3, "lemma" : "는", "type" : "JX", "position" : 71, "weight" : 0.0387928 },
		{"id" : 4, "lemma" : "《", "type" : "SS", "position" : 75, "weight" : 1 },
		{"id" : 5, "lemma" : "박시백", "type" : "NNP", "position" : 78, "weight" : 0.6 },
		{"id" : 6, "lemma" : "의", "type" : "JKG", "position" : 87, "weight" : 0.0987295 },
		{"id" : 7, "lemma" : "조선", "type" : "NNP", "position" : 91, "weight" : 0.0200051 },
		{"id" : 8, "lemma" : "왕조", "type" : "NNG", "position" : 97, "weight" : 0.9 },
		{"id" : 9, "lemma" : "실록", "type" : "NNG", "position" : 103, "weight" : 0.9 },
		{"id" : 10, "lemma" : "》", "type" : "SS", "position" : 109, "weight" : 1 },
		{"id" : 11, "lemma" : "시리즈", "type" : "NNG", "position" : 113, "weight" : 0.9 },
		{"id" : 12, "lemma" : "가", "type" : "JKS", "position" : 122, "weight" : 0.066324 },
		{"id" : 13, "lemma" : "있", "type" : "VA", "position" : 126, "weight" : 0.12767 },
		{"id" : 14, "lemma" : "다", "type" : "EF", "position" : 129, "weight" : 0.132573 },
		{"id" : 15, "lemma" : ".", "type" : "SF", "position" : 132, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "대표작/NNG+으로/JKB+는/JX", "target" : "대표작으로는", "word_id" : 0, "m_begin" : 0, "m_end" : 3},
		{"id" : 1, "result" : "《/SS+박시백/NNG+의/JKG", "target" : "《박시백의", "word_id" : 1, "m_begin" : 4, "m_end" : 6},
		{"id" : 2, "result" : "조선왕조실록/NNG+》/SS", "target" : "조선왕조실록》", "word_id" : 2, "m_begin" : 7, "m_end" : 10},
		{"id" : 3, "result" : "시리즈/NNG+가/JKS", "target" : "시리즈가", "word_id" : 3, "m_begin" : 11, "m_end" : 12},
		{"id" : 4, "result" : "있/VA+다/EF+./SF", "target" : "있다.", "word_id" : 4, "m_begin" : 13, "m_end" : 15}
	],
	"WSD" : [
		{"id" : 0, "text" : "대표작", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 56, "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 65, "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "는", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 71, "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "《", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 75, "begin" : 4, "end" : 4},
		{"id" : 4, "text" : "박시백", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 78, "begin" : 5, "end" : 5},
		{"id" : 5, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 87, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "조선왕조실록", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 91, "begin" : 7, "end" : 9},
		{"id" : 7, "text" : "》", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 109, "begin" : 10, "end" : 10},
		{"id" : 8, "text" : "시리즈", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 113, "begin" : 11, "end" : 11},
		{"id" : 9, "text" : "가", "type" : "JKS", "scode" : "00", "weight" : 1, "position" : 122, "begin" : 12, "end" : 12},
		{"id" : 10, "text" : "있", "type" : "VA", "scode" : "01", "weight" : 1, "position" : 126, "begin" : 13, "end" : 13},
		{"id" : 11, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 129, "begin" : 14, "end" : 14},
		{"id" : 12, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 132, "begin" : 15, "end" : 15}
	],
	"word" : [
		{"id" : 0, "text" : "대표작으로는", "type" : "", "begin" : 0, "end" : 3},
		{"id" : 1, "text" : "《박시백의", "type" : "", "begin" : 4, "end" : 6},
		{"id" : 2, "text" : "조선왕조실록》", "type" : "", "begin" : 7, "end" : 10},
		{"id" : 3, "text" : "시리즈가", "type" : "", "begin" : 11, "end" : 12},
		{"id" : 4, "text" : "있다.", "type" : "", "begin" : 13, "end" : 15}
	],
	"NE" : [
		{"id" : 0, "text" : "박시백의 조선왕조실록", "type" : "AF_WORKS", "begin" : 5, "end" : 9, "weight" : 0.828707, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "대표작으로는", "head" : 4, "label" : "NP_AJT", "mod" : [], "weight" : 0.740746 },
		{"id" : 1, "text" : "《박시백의", "head" : 2, "label" : "NP_MOD", "mod" : [], "weight" : 0.565662 },
		{"id" : 2, "text" : "조선왕조실록》", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.774741 },
		{"id" : 3, "text" : "시리즈가", "head" : 4, "label" : "NP_SBJ", "mod" : [2], "weight" : 0.516122 },
		{"id" : 4, "text" : "있다.", "head" : -1, "label" : "VP", "mod" : [0, 3], "weight" : 0.13361 }
	],
	"SRL" : [
		{"verb" : "있", "sense" : 1, "word_id" : 4, "weight" : 0.3281,
			"argument" : [
				{"type" : "ARG1", "word_id" : 3, "text" : "시리즈가", "weight" : 0.3281 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	}
 ],
 "entity" : [
 ]
}

