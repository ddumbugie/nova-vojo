{"doc_id" : "",
 "DCT" : "",
 "category" : "",
 "category_weight" : 0,
 "title" : {"text" : "", "NE" : ""},
 "metaInfo" : { },
 "sentence" : [
	{
	"id" : 0,
	"reserve_str" : "",
	"text" : "이진주(본명: 이세권, 1952년4월 2일 ~ )은 대한민국의 만화가이다.",
	"morp" : [
		{"id" : 0, "lemma" : "이진주", "type" : "NNP", "position" : 0, "weight" : 0.05 },
		{"id" : 1, "lemma" : "(", "type" : "SS", "position" : 9, "weight" : 1 },
		{"id" : 2, "lemma" : "본명", "type" : "NNG", "position" : 10, "weight" : 0.9 },
		{"id" : 3, "lemma" : ":", "type" : "SP", "position" : 16, "weight" : 1 },
		{"id" : 4, "lemma" : "이세권", "type" : "NNP", "position" : 18, "weight" : 0.6 },
		{"id" : 5, "lemma" : ",", "type" : "SP", "position" : 27, "weight" : 1 },
		{"id" : 6, "lemma" : "1952", "type" : "SN", "position" : 29, "weight" : 1 },
		{"id" : 7, "lemma" : "년", "type" : "NNB", "position" : 33, "weight" : 0.414343 },
		{"id" : 8, "lemma" : "4", "type" : "SN", "position" : 36, "weight" : 1 },
		{"id" : 9, "lemma" : "월", "type" : "NNB", "position" : 37, "weight" : 0.408539 },
		{"id" : 10, "lemma" : "2", "type" : "SN", "position" : 41, "weight" : 1 },
		{"id" : 11, "lemma" : "일", "type" : "NNB", "position" : 42, "weight" : 0.126777 },
		{"id" : 12, "lemma" : "~", "type" : "SO", "position" : 46, "weight" : 1 },
		{"id" : 13, "lemma" : ")", "type" : "SS", "position" : 48, "weight" : 1 },
		{"id" : 14, "lemma" : "은", "type" : "JX", "position" : 49, "weight" : 0.0128817 },
		{"id" : 15, "lemma" : "대한민국", "type" : "NNP", "position" : 53, "weight" : 0.0447775 },
		{"id" : 16, "lemma" : "의", "type" : "JKG", "position" : 65, "weight" : 0.0987295 },
		{"id" : 17, "lemma" : "만화", "type" : "NNG", "position" : 69, "weight" : 0.83848 },
		{"id" : 18, "lemma" : "가", "type" : "XSN", "position" : 75, "weight" : 0.000115417 },
		{"id" : 19, "lemma" : "이", "type" : "VCP", "position" : 78, "weight" : 0.0165001 },
		{"id" : 20, "lemma" : "다", "type" : "EF", "position" : 81, "weight" : 0.353579 },
		{"id" : 21, "lemma" : ".", "type" : "SF", "position" : 84, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "이진주/NNG+(/SS+본명/NNG+:/SP", "target" : "이진주(본명:", "word_id" : 0, "m_begin" : 0, "m_end" : 3},
		{"id" : 1, "result" : "이세권/NNG+,/SP", "target" : "이세권,", "word_id" : 1, "m_begin" : 4, "m_end" : 5},
		{"id" : 2, "result" : "1952/SN+년/NNB+4/SN+월/NNB", "target" : "1952년4월", "word_id" : 2, "m_begin" : 6, "m_end" : 9},
		{"id" : 3, "result" : "2/SN+일/NNB", "target" : "2일", "word_id" : 3, "m_begin" : 10, "m_end" : 11},
		{"id" : 4, "result" : "~/SO", "target" : "~", "word_id" : 4, "m_begin" : 12, "m_end" : 12},
		{"id" : 5, "result" : ")/SS+은/JX", "target" : ")은", "word_id" : 5, "m_begin" : 13, "m_end" : 14},
		{"id" : 6, "result" : "대한민국/NNG+의/JKG", "target" : "대한민국의", "word_id" : 6, "m_begin" : 15, "m_end" : 16},
		{"id" : 7, "result" : "만화가/NNG+이/VCP+다/EF+./SF", "target" : "만화가이다.", "word_id" : 7, "m_begin" : 17, "m_end" : 21}
	],
	"WSD" : [
		{"id" : 0, "text" : "이진주", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 0, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "(", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 9, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "본명", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 10, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : ":", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 16, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "이세권", "type" : "NNP", "scode" : "00", "weight" : 0, "position" : 18, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : ",", "type" : "SP", "scode" : "00", "weight" : 1, "position" : 27, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "1952", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 29, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 33, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "4", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 36, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "월", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 37, "begin" : 9, "end" : 9},
		{"id" : 10, "text" : "2", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 41, "begin" : 10, "end" : 10},
		{"id" : 11, "text" : "일", "type" : "NNB", "scode" : "07", "weight" : 1, "position" : 42, "begin" : 11, "end" : 11},
		{"id" : 12, "text" : "~", "type" : "SO", "scode" : "00", "weight" : 1, "position" : 46, "begin" : 12, "end" : 12},
		{"id" : 13, "text" : ")", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 48, "begin" : 13, "end" : 13},
		{"id" : 14, "text" : "은", "type" : "JX", "scode" : "00", "weight" : 1, "position" : 49, "begin" : 14, "end" : 14},
		{"id" : 15, "text" : "대한민국", "type" : "NNP", "scode" : "00", "weight" : 1, "position" : 53, "begin" : 15, "end" : 15},
		{"id" : 16, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 65, "begin" : 16, "end" : 16},
		{"id" : 17, "text" : "만화가", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 69, "begin" : 17, "end" : 18},
		{"id" : 18, "text" : "이", "type" : "VCP", "scode" : "00", "weight" : 1, "position" : 78, "begin" : 19, "end" : 19},
		{"id" : 19, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 81, "begin" : 20, "end" : 20},
		{"id" : 20, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 84, "begin" : 21, "end" : 21}
	],
	"word" : [
		{"id" : 0, "text" : "이진주(본명:", "type" : "", "begin" : 0, "end" : 3},
		{"id" : 1, "text" : "이세권,", "type" : "", "begin" : 4, "end" : 5},
		{"id" : 2, "text" : "1952년4월", "type" : "", "begin" : 6, "end" : 9},
		{"id" : 3, "text" : "2일", "type" : "", "begin" : 10, "end" : 11},
		{"id" : 4, "text" : "~", "type" : "", "begin" : 12, "end" : 12},
		{"id" : 5, "text" : ")은", "type" : "", "begin" : 13, "end" : 14},
		{"id" : 6, "text" : "대한민국의", "type" : "", "begin" : 15, "end" : 16},
		{"id" : 7, "text" : "만화가이다.", "type" : "", "begin" : 17, "end" : 21}
	],
	"NE" : [
		{"id" : 0, "text" : "이진주", "type" : "PS_NAME", "begin" : 0, "end" : 0, "weight" : 0.573547, "common_noun" : 0},
		{"id" : 1, "text" : "이세권", "type" : "PS_NAME", "begin" : 4, "end" : 4, "weight" : 0.54454, "common_noun" : 0},
		{"id" : 2, "text" : "1952년4월 2일 ~", "type" : "DT_OTHERS", "begin" : 6, "end" : 12, "weight" : 0.628875, "common_noun" : 0},
		{"id" : 3, "text" : "대한민국", "type" : "LCP_COUNTRY", "begin" : 15, "end" : 15, "weight" : 0.18423, "common_noun" : 0},
		{"id" : 4, "text" : "만화가", "type" : "CV_OCCUPATION", "begin" : 17, "end" : 18, "weight" : 0.285657, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "이진주(본명:", "head" : 5, "label" : "NP_CNJ", "mod" : [], "weight" : 0.508728 },
		{"id" : 1, "text" : "이세권,", "head" : 5, "label" : "NP_CNJ", "mod" : [], "weight" : 0.577708 },
		{"id" : 2, "text" : "1952년4월", "head" : 3, "label" : "NP", "mod" : [], "weight" : 0.542496 },
		{"id" : 3, "text" : "2일", "head" : 4, "label" : "NP", "mod" : [2], "weight" : 0.401348 },
		{"id" : 4, "text" : "~", "head" : 5, "label" : "X", "mod" : [3], "weight" : 0.805798 },
		{"id" : 5, "text" : ")은", "head" : 7, "label" : "NP_SBJ", "mod" : [0, 1, 4], "weight" : 0.448183 },
		{"id" : 6, "text" : "대한민국의", "head" : 7, "label" : "NP_MOD", "mod" : [], "weight" : 0.431135 },
		{"id" : 7, "text" : "만화가이다.", "head" : -1, "label" : "VNP", "mod" : [5, 6], "weight" : 0.00578398 }
	],
	"SRL" : [
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
	]
	},
	{
	"id" : 1,
	"reserve_str" : "",
	"text" : "딸의 이름 ‘진주’를 필명으로 사용하였다.",
	"morp" : [
		{"id" : 0, "lemma" : "딸", "type" : "NNG", "position" : 85, "weight" : 0.9 },
		{"id" : 1, "lemma" : "의", "type" : "JKG", "position" : 88, "weight" : 0.0694213 },
		{"id" : 2, "lemma" : "이름", "type" : "NNG", "position" : 92, "weight" : 0.9 },
		{"id" : 3, "lemma" : "‘", "type" : "SS", "position" : 99, "weight" : 1 },
		{"id" : 4, "lemma" : "진주", "type" : "NNP", "position" : 102, "weight" : 0.0364423 },
		{"id" : 5, "lemma" : "’", "type" : "SS", "position" : 108, "weight" : 1 },
		{"id" : 6, "lemma" : "를", "type" : "JKO", "position" : 111, "weight" : 0.0357023 },
		{"id" : 7, "lemma" : "필명", "type" : "NNG", "position" : 115, "weight" : 0.9 },
		{"id" : 8, "lemma" : "으로", "type" : "JKB", "position" : 121, "weight" : 0.153406 },
		{"id" : 9, "lemma" : "사용", "type" : "NNG", "position" : 128, "weight" : 0.9 },
		{"id" : 10, "lemma" : "하", "type" : "XSV", "position" : 134, "weight" : 0.0001 },
		{"id" : 11, "lemma" : "었", "type" : "EP", "position" : 137, "weight" : 0.9 },
		{"id" : 12, "lemma" : "다", "type" : "EF", "position" : 140, "weight" : 0.640954 },
		{"id" : 13, "lemma" : ".", "type" : "SF", "position" : 143, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "딸/NNG+의/JKG", "target" : "딸의", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "이름/NNG", "target" : "이름", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "‘/SS+진주/NNG+’/SS+를/JKO", "target" : "‘진주’를", "word_id" : 2, "m_begin" : 3, "m_end" : 6},
		{"id" : 3, "result" : "필명/NNG+으로/JKB", "target" : "필명으로", "word_id" : 3, "m_begin" : 7, "m_end" : 8},
		{"id" : 4, "result" : "사용하/VV+었/EP+다/EF+./SF", "target" : "사용하였다.", "word_id" : 4, "m_begin" : 9, "m_end" : 13}
	],
	"WSD" : [
		{"id" : 0, "text" : "딸", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 85, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "의", "type" : "JKG", "scode" : "00", "weight" : 1, "position" : 88, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "이름", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 92, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "‘", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 99, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "진주", "type" : "NNP", "scode" : "03", "weight" : 1, "position" : 102, "begin" : 4, "end" : 4},
		{"id" : 5, "text" : "’", "type" : "SS", "scode" : "00", "weight" : 1, "position" : 108, "begin" : 5, "end" : 5},
		{"id" : 6, "text" : "를", "type" : "JKO", "scode" : "00", "weight" : 1, "position" : 111, "begin" : 6, "end" : 6},
		{"id" : 7, "text" : "필명", "type" : "NNG", "scode" : "02", "weight" : 1, "position" : 115, "begin" : 7, "end" : 7},
		{"id" : 8, "text" : "으로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 121, "begin" : 8, "end" : 8},
		{"id" : 9, "text" : "사용하", "type" : "VV", "scode" : "03", "weight" : 1, "position" : 128, "begin" : 9, "end" : 10},
		{"id" : 10, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 137, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 140, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 143, "begin" : 13, "end" : 13}
	],
	"word" : [
		{"id" : 0, "text" : "딸의", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "이름", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "‘진주’를", "type" : "", "begin" : 3, "end" : 6},
		{"id" : 3, "text" : "필명으로", "type" : "", "begin" : 7, "end" : 8},
		{"id" : 4, "text" : "사용하였다.", "type" : "", "begin" : 9, "end" : 13}
	],
	"NE" : [
		{"id" : 0, "text" : "딸", "type" : "CV_RELATION", "begin" : 0, "end" : 0, "weight" : 0.302037, "common_noun" : 0},
		{"id" : 1, "text" : "진주", "type" : "MT_CHEMICAL", "begin" : 4, "end" : 4, "weight" : 0.128854, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "딸의", "head" : 1, "label" : "NP_MOD", "mod" : [], "weight" : 0.700692 },
		{"id" : 1, "text" : "이름", "head" : 2, "label" : "NP", "mod" : [0], "weight" : 0.854674 },
		{"id" : 2, "text" : "‘진주’를", "head" : 4, "label" : "NP_OBJ", "mod" : [1], "weight" : 0.49864 },
		{"id" : 3, "text" : "필명으로", "head" : 4, "label" : "NP_AJT", "mod" : [], "weight" : 0.495398 },
		{"id" : 4, "text" : "사용하였다.", "head" : -1, "label" : "VP", "mod" : [2, 3], "weight" : 0.0864002 }
	],
	"SRL" : [
		{"verb" : "사용", "sense" : 1, "word_id" : 4, "weight" : 0.149179,
			"argument" : [
				{"type" : "ARG1", "word_id" : 2, "text" : "‘진주’를", "weight" : 0.165713 },
				{"type" : "ARG2", "word_id" : 3, "text" : "필명으로", "weight" : 0.132646 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 4, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.82905 }
	]
	},
	{
	"id" : 2,
	"reserve_str" : "",
	"text" : "1988년 서울 YMCA 어린이만화모니터모임에서 제1회 어린이만화우수작가로 선정되었다.",
	"morp" : [
		{"id" : 0, "lemma" : "1988", "type" : "SN", "position" : 144, "weight" : 1 },
		{"id" : 1, "lemma" : "년", "type" : "NNB", "position" : 148, "weight" : 0.414343 },
		{"id" : 2, "lemma" : "서울", "type" : "NNP", "position" : 152, "weight" : 0.0151353 },
		{"id" : 3, "lemma" : "YMCA", "type" : "SL", "position" : 159, "weight" : 1 },
		{"id" : 4, "lemma" : "어린", "type" : "NNG", "position" : 164, "weight" : 0.05 },
		{"id" : 5, "lemma" : "이", "type" : "NNG", "position" : 170, "weight" : 0.000193221 },
		{"id" : 6, "lemma" : "만화", "type" : "NNG", "position" : 173, "weight" : 0.177848 },
		{"id" : 7, "lemma" : "모니터", "type" : "NNG", "position" : 179, "weight" : 0.9 },
		{"id" : 8, "lemma" : "모임", "type" : "NNG", "position" : 188, "weight" : 0.18476 },
		{"id" : 9, "lemma" : "에서", "type" : "JKB", "position" : 194, "weight" : 0.153407 },
		{"id" : 10, "lemma" : "제", "type" : "XPN", "position" : 201, "weight" : 0.000688271 },
		{"id" : 11, "lemma" : "1", "type" : "SN", "position" : 204, "weight" : 1 },
		{"id" : 12, "lemma" : "회", "type" : "NNB", "position" : 205, "weight" : 0.258682 },
		{"id" : 13, "lemma" : "어린", "type" : "NNG", "position" : 209, "weight" : 0.05 },
		{"id" : 14, "lemma" : "이", "type" : "NNG", "position" : 215, "weight" : 0.000193221 },
		{"id" : 15, "lemma" : "만화", "type" : "NNG", "position" : 218, "weight" : 0.177848 },
		{"id" : 16, "lemma" : "우수", "type" : "NNG", "position" : 224, "weight" : 0.184309 },
		{"id" : 17, "lemma" : "작가", "type" : "NNG", "position" : 230, "weight" : 0.184798 },
		{"id" : 18, "lemma" : "로", "type" : "JKB", "position" : 236, "weight" : 0.153229 },
		{"id" : 19, "lemma" : "선정", "type" : "NNG", "position" : 240, "weight" : 0.207767 },
		{"id" : 20, "lemma" : "되", "type" : "XSV", "position" : 246, "weight" : 0.000224177 },
		{"id" : 21, "lemma" : "었", "type" : "EP", "position" : 249, "weight" : 0.9 },
		{"id" : 22, "lemma" : "다", "type" : "EF", "position" : 252, "weight" : 0.640954 },
		{"id" : 23, "lemma" : ".", "type" : "SF", "position" : 255, "weight" : 1 }
	],
	"morp_eval" : [
		{"id" : 0, "result" : "1988/SN+년/NNB", "target" : "1988년", "word_id" : 0, "m_begin" : 0, "m_end" : 1},
		{"id" : 1, "result" : "서울/NNG", "target" : "서울", "word_id" : 1, "m_begin" : 2, "m_end" : 2},
		{"id" : 2, "result" : "YMCA/SL", "target" : "YMCA", "word_id" : 2, "m_begin" : 3, "m_end" : 3},
		{"id" : 3, "result" : "어린이만화모니터모임/NNG+에서/JKB", "target" : "어린이만화모니터모임에서", "word_id" : 3, "m_begin" : 4, "m_end" : 9},
		{"id" : 4, "result" : "제/XPN+1/SN+회/NNB", "target" : "제1회", "word_id" : 4, "m_begin" : 10, "m_end" : 12},
		{"id" : 5, "result" : "어린이만화우수작가/NNG+로/JKB", "target" : "어린이만화우수작가로", "word_id" : 5, "m_begin" : 13, "m_end" : 18},
		{"id" : 6, "result" : "선정되/VV+었/EP+다/EF+./SF", "target" : "선정되었다.", "word_id" : 6, "m_begin" : 19, "m_end" : 23}
	],
	"WSD" : [
		{"id" : 0, "text" : "1988", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 144, "begin" : 0, "end" : 0},
		{"id" : 1, "text" : "년", "type" : "NNB", "scode" : "02", "weight" : 1, "position" : 148, "begin" : 1, "end" : 1},
		{"id" : 2, "text" : "서울", "type" : "NNP", "scode" : "01", "weight" : 1, "position" : 152, "begin" : 2, "end" : 2},
		{"id" : 3, "text" : "YMCA", "type" : "SL", "scode" : "00", "weight" : 1, "position" : 159, "begin" : 3, "end" : 3},
		{"id" : 4, "text" : "어린이", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 164, "begin" : 4, "end" : 5},
		{"id" : 5, "text" : "만화", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 173, "begin" : 6, "end" : 6},
		{"id" : 6, "text" : "모니터", "type" : "NNG", "scode" : "00", "weight" : 1, "position" : 179, "begin" : 7, "end" : 7},
		{"id" : 7, "text" : "모임", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 188, "begin" : 8, "end" : 8},
		{"id" : 8, "text" : "에서", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 194, "begin" : 9, "end" : 9},
		{"id" : 9, "text" : "제", "type" : "XPN", "scode" : "21", "weight" : 1, "position" : 201, "begin" : 10, "end" : 10},
		{"id" : 10, "text" : "1", "type" : "SN", "scode" : "00", "weight" : 1, "position" : 204, "begin" : 11, "end" : 11},
		{"id" : 11, "text" : "회", "type" : "NNB", "scode" : "08", "weight" : 1, "position" : 205, "begin" : 12, "end" : 12},
		{"id" : 12, "text" : "어린이", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 209, "begin" : 13, "end" : 14},
		{"id" : 13, "text" : "만화", "type" : "NNG", "scode" : "10", "weight" : 1, "position" : 218, "begin" : 15, "end" : 15},
		{"id" : 14, "text" : "우수", "type" : "NNG", "scode" : "11", "weight" : 1, "position" : 224, "begin" : 16, "end" : 16},
		{"id" : 15, "text" : "작가", "type" : "NNG", "scode" : "01", "weight" : 1, "position" : 230, "begin" : 17, "end" : 17},
		{"id" : 16, "text" : "로", "type" : "JKB", "scode" : "00", "weight" : 1, "position" : 236, "begin" : 18, "end" : 18},
		{"id" : 17, "text" : "선정되", "type" : "VV", "scode" : "00", "weight" : 1, "position" : 240, "begin" : 19, "end" : 20},
		{"id" : 18, "text" : "었", "type" : "EP", "scode" : "00", "weight" : 1, "position" : 249, "begin" : 21, "end" : 21},
		{"id" : 19, "text" : "다", "type" : "EF", "scode" : "00", "weight" : 1, "position" : 252, "begin" : 22, "end" : 22},
		{"id" : 20, "text" : ".", "type" : "SF", "scode" : "00", "weight" : 1, "position" : 255, "begin" : 23, "end" : 23}
	],
	"word" : [
		{"id" : 0, "text" : "1988년", "type" : "", "begin" : 0, "end" : 1},
		{"id" : 1, "text" : "서울", "type" : "", "begin" : 2, "end" : 2},
		{"id" : 2, "text" : "YMCA", "type" : "", "begin" : 3, "end" : 3},
		{"id" : 3, "text" : "어린이만화모니터모임에서", "type" : "", "begin" : 4, "end" : 9},
		{"id" : 4, "text" : "제1회", "type" : "", "begin" : 10, "end" : 12},
		{"id" : 5, "text" : "어린이만화우수작가로", "type" : "", "begin" : 13, "end" : 18},
		{"id" : 6, "text" : "선정되었다.", "type" : "", "begin" : 19, "end" : 23}
	],
	"NE" : [
		{"id" : 0, "text" : "1988년", "type" : "DT_YEAR", "begin" : 0, "end" : 1, "weight" : 0.641237, "common_noun" : 0},
		{"id" : 1, "text" : "서울 YMCA 어린이만화모니터모임", "type" : "OG_OTHERS", "begin" : 2, "end" : 8, "weight" : 0.147598, "common_noun" : 0},
		{"id" : 2, "text" : "제1회", "type" : "QT_ORDER", "begin" : 10, "end" : 12, "weight" : 0.808183, "common_noun" : 0},
		{"id" : 3, "text" : "어린이만화우수작가", "type" : "EV_OTHERS", "begin" : 13, "end" : 17, "weight" : 0.166743, "common_noun" : 0}
	],
	"chunk" : [
	],
	"dependency" : [
		{"id" : 0, "text" : "1988년", "head" : 6, "label" : "NP_AJT", "mod" : [], "weight" : 0.529511 },
		{"id" : 1, "text" : "서울", "head" : 2, "label" : "NP", "mod" : [], "weight" : 0.633404 },
		{"id" : 2, "text" : "YMCA", "head" : 3, "label" : "NP", "mod" : [1], "weight" : 0.864992 },
		{"id" : 3, "text" : "어린이만화모니터모임에서", "head" : 6, "label" : "NP_AJT", "mod" : [2], "weight" : 0.594976 },
		{"id" : 4, "text" : "제1회", "head" : 5, "label" : "NP", "mod" : [], "weight" : 0.798403 },
		{"id" : 5, "text" : "어린이만화우수작가로", "head" : 6, "label" : "NP_AJT", "mod" : [4], "weight" : 0.504527 },
		{"id" : 6, "text" : "선정되었다.", "head" : -1, "label" : "VP", "mod" : [0, 3, 5], "weight" : 0.0384434 }
	],
	"SRL" : [
		{"verb" : "선정", "sense" : 1, "word_id" : 6, "weight" : 0.168753,
			"argument" : [
				{"type" : "ARGM-TMP", "word_id" : 0, "text" : "1988년", "weight" : 0.254737 },
				{"type" : "ARGM-LOC", "word_id" : 3, "text" : "어린이만화모니터모임에서", "weight" : 0.10031 },
				{"type" : "ARG2", "word_id" : 5, "text" : "어린이만화우수작가로", "weight" : 0.151213 }
			] }
	],
	"relation" : [
	],
	"SA" : [
	],
	"ZA" : [
		{"id" : 0, "verb_wid" : 6, "ant_sid" : -1, "ant_wid" : -1, "type" : "s", "istitle" : 0, "weight" : 0.665335 }
	]
	}
 ],
 "entity" : [
 ]
}

