《당신들의 천국》은 이청준의 소설로 한센병 환자 병원이 있는 소록도의 병원 안에서 벌어지는 병원장과 환자간의 갈등을 다룬 이야기다.
1976년 문학과지성사에서 간행했으며 총 3부로 구성되어 있다.
전직 군의관이자 현역 육군 대령이며 문둥병(나병) 환자 병원의 병원장이다.
소록도에 부임하여 좌절과 실의에 빠진 원생들을 위하여 오마도 간척 사업을 추진한다.
하지만 원생들은 일제 강점기 주정수 원장이 보인 위선과 배반의 역사를 떠올리며 고통과 배반을 떠올린다.
결국 조백헌 원장은 간척 사업의 결말을 보이지 못하고 섬을 떠난다.
그리고 7년 뒤 민간인의 신분으로 섬에 다시 들어와 섬사람들과 함께하는 삶을 살면서 사랑이 바탕이 된 진정한 천국을 꿈꾼다.
소록도의 병원에서 일하는 보건과장이며 작품 내에서는 섬사람의 하나로, 숨겨진 과거가 있다.
처음에는 조백헌에게 호의적이지 않은 모습을 보인다.
조백헌이 하는 많은 일들에 대해서 끊임없이 파고드는 정교한 면모를 지닌 인물이다.
취재기자이며 조백헌이라는 인물과 소록도에 깊은 관심을 갖고 있다.
이 작중인물은 당시 소록도 탐사 기사를 지면에 실었던 조선일보 기자 이규태를 모델로 하였다.
섬의 장로이며, 주민들에게 깊은 신뢰를 받고 있다.
가난과 병으로 인한 끔찍한 과거를 가지고 있는 인물.