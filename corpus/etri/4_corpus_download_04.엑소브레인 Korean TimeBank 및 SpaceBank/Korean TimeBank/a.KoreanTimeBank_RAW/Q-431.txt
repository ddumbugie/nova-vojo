﻿백제는 특유의 독창성과 창조성을 가진 문화로 유명하다.
특히 이것은 보통 4~5세기에 만들어진 것으로 추정하며 칼의 몸체 좌우에 엇갈려서 각각 3개의 칼날이 나뭇가지 모양으로 뻗어있다.
이 칼은 무엇일까?
