《대한제국 연대기》(大韓帝國年代記)는 대한민국의 소설가인 김경록이 2010년에 발표한 장편 대체역사소설이다.
22세기의 우주비행사로 훈련받은 고등 인재가 1399년 당시의 탐라에 표착하는 것으로 시작하고 19세기 대한제국 연해주의 준 자치지방의 독립투쟁을 묘사하는 것으로 끝난다.
원 역사라면 서유럽에서 일어났어야 할 대항해시대와 산업혁명이 극동의 대한제국에서 동시다발적으로 일어나고 또한 이에 따른 식민지개척과 사상투쟁까지 묘사되어있다.
소설 제목처럼 한 주인공의 이야기가 아닌, 시대의 핵심 사건을 시간 순서로 펼쳐놓은 소설이다.