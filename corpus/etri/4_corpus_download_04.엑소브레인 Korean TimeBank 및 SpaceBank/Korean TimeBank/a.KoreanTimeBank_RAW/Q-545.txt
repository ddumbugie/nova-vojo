﻿일반 거미의 경우 수명이 1년에서 2년인데 반해 이 거미는 20년까지 살기도 한다.
대형거미 중 하나로 털이 많이 나 있고 독성이 강한 것도 있으며 큰 곤충이나 쥐 등을 직접 사냥하기도 한다.
한때 유럽 늑대거미를 이 거미로 부르기도 했는데 이 거미는 무엇일까?
