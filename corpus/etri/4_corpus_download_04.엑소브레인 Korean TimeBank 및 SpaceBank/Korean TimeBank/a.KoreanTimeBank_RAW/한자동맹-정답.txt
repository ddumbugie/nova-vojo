﻿한자 동맹(독일어: die Hanse, 네덜란드어: de Hanze, 에스토니아어: hansa, 폴란드어: Hanza, 스웨덴어: Hansan, 영어: Hanseatic League)은 13~17세기에 독일 북쪽과 발트 해 연안에 있는 여러 도시 사이에서 이루어졌던 연맹이다.
주로 해상 교통의 안전을 보장하고 공동 방호와 상권 확장 등을 목적으로 했다.
뤼베크를 비롯하여 함부르크, 비스마르, 로스토크, 단치히 등 북해 연안과 엘베 강 동안에 있는 독일 여러 도시가 최초에 가맹하였고 후에 가맹한 여러 도시가 90개를 넘었다.
13~15세기에 독일 북부 연안과 발트 해 연안의 여러 도시 사이에 이루어진 도시 연맹.
해상 교통의 안전 보장, 공동 방호, 상권 확장 따위를 목적으로 하였다.
