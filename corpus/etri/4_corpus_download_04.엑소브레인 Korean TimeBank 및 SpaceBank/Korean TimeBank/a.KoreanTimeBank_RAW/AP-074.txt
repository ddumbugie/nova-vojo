﻿구운 밤 닷 되를 심습니다.
그 밤이 움이 돋아 싹이 나야만 옥으로 연꽃을 새기옵니다.
바위 위에 접을 붙이옵니다.
그 꽃이 세 묶음(혹은 한 겨울에) 피어야만 무쇠로 황소를 만들어다가 쇠나무산에 놓습니다.
그 소가 쇠풀을 먹어야
