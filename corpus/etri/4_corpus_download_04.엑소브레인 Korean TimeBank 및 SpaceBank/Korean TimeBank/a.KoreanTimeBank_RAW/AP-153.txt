﻿그의 대표작이기도 하며 또한 그를 콜롬비아의 세르반테스라고 일컫게 한 <백년 동안의 고독>은 마콘도(Ma­condo)라는 가공의 땅을 무대로 하여 부엔디아 일족의 역사를 그린 작품이다.
