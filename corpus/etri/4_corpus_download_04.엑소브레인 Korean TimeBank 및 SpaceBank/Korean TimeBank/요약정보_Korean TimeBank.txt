[폴더 구성]

a.KoreanTimeBank_Raw: 자연어 원본 문서들 (일반 텍스트)
b.KoreanTimeBank_LAR: ETRI 언어분석기 결과 문서들 (JSON)
c.KoreanTimeBank_XML: 시간 정보 태깅 문서들 (XML)


[전체 데이터 명세]

Total # of documents=  812
  └─ The # of documents including TIMEX3s=  812 (100.00%)
  └─ The # of documents including EVENTs=  812 (100.00%)
  └─ The # of documents including TLINKs=  693 (85.34%)

nSentences=  5467
  └─ The # of sentences including TIMEX3s=  2607 (47.69%)
  └─ The # of sentences including EVENTs=  5273 (96.45%)
  └─ The # of sentences including TLINKs=  2412 (44.12%)

Total # of TIMEX3s=  4509
Total # of EVENTs=  15653
Total # of MAKEINSTANCEs=  15700
Total # of TLINKs=  5182
