﻿* 폴더 설명
a.RawCorpus: 원시 말뭉치 문서들 
b.AnnotatedCorpus: 공간 정보 태깅 문서들 


* 공간 정보 주석 말뭉치 파일 형태
  XML형식으로 구성
  UNIX/UTF-8


* 전체 데이터 명세
Total # of documents = 87
Total # of sentences = 610
Total # of Place = 1005
Total # of Path = 100
Total # of Spatial Entity = 484
Total # of Motion = 273
Total # of Motion Signal = 345
Total # of Spatial Signal = 423
Total # of Measure = 131
Total # of QSLink = 235
Total # of OLink = 208
Total # of MoveLink = 294
Total # of MeasureLink = 133
