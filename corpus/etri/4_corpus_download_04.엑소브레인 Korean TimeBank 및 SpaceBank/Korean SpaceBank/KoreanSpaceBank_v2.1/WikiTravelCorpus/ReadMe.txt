﻿* 폴더 설명
a.RawCorpus: 원시 말뭉치 문서들 
b.AnnotatedCorpus: 공간 정보 태깅 문서들 


* 공간 정보 주석 말뭉치 파일 형태
  XML형식으로 구성
  UNIX/UTF-8


* 전체 데이터 명세
Total # of documents = 171
Total # of sentences = 1654
Total # of Place = 4920
Total # of Path = 281
Total # of Spatial Entity = 362
Total # of Motion = 238
Total # of Motion Signal = 247
Total # of Spatial Signal = 1248
Total # of Measure = 281
Total # of QSLink = 1096
Total # of OLink = 538
Total # of MoveLink = 293
Total # of MeasureLink = 347

